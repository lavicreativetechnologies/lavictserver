<?php
namespace Deployer;

require 'recipe/common.php';

add('recipes', ['shopware']);

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@gitlab.com:lavicreativetechnologies/lavictserver.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', [
  '.env',
    'install.lock',
    'public/.htaccess',
    'public/.user.ini',
]);

set('shared_dirs', [
 'config/jwt',
    'files',
    'var/log',
    'public/media',
    'public/thumbnail',
    'public/sitemap',
]);

// Writable dirs by web server 
set('writable_dirs', []);


// Hosts

// host('202.61.239.48')
//     ->user('root')
//     ->set('deploy_path', '/var/www/vhosts/rocdev.de/deptest.rocdev.de');

host('45.83.107.12')
    ->user('root')
    ->set('deploy_path','/var/www/deptest');

// host('localhost')
//     ->user('pcg')
//     ->set('deploy_path', '/var/www/html/test');


task('sw:build', static function () {
    run('cd {{release_path}} && bash bin/build-js.sh');
});

task('sw:cache:clear', static function () {
    run('cd {{release_path}} && bin/console cache:clear');
});

task('sw:deploy', [
    // 'sw:build',
    'sw:cache:clear',
]);


// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
