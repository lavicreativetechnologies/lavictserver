# 1.2.3
- fix product name not displaying correctly in list view and detail view

# 1.2.4
- add acl rule handling

# 1.2.5
- change event subscriber to subscribe new, not deprecated, Events for detailpage

# 1.2.6

- refactor acl rule

# 1.2.7

- refactor product extending

# 1.2.8

- add collective entry

# 1.2.9

- enable multiselect delete

# 1.3.0

- add import / export profile for the import / export module

# 1.3.1

- add own import for CSV files

# 1.3.2

- make 6.4 compatible

# 1.4.0

- add discount

# 1.4.1

- fix uninstall issue, on remove column

# 1.4.2

- fix uninstall issue, on remove profile

# 1.4.3

- fix uninstall issue, on remove profile

# 1.4.4

- add api action to delete all customer prices of one customer
