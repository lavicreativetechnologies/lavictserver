<?php declare(strict_types=1);

namespace VioCustomerPrice\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1614600908AddDiscount extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1614600908;
    }

    public function update(Connection $connection): void
    {
        $sql = <<< SQL
ALTER TABLE vio_customer_price ADD COLUMN `discount` DOUBLE NULL;
SQL;
        $connection->executeQuery($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
