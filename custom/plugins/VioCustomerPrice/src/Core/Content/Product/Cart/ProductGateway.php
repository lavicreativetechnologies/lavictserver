<?php


namespace VioCustomerPrice\Core\Content\Product\Cart;


use Shopware\Core\Content\Product\Cart\ProductGatewayInterface;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class ProductGateway implements ProductGatewayInterface
{
    /**
     * @var SalesChannelRepositoryInterface
     */
    private $repository;

    public function __construct(SalesChannelRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $ids
     * @param SalesChannelContext $context
     * @return ProductCollection
     * @throws InconsistentCriteriaIdsException
     */
    public function get(array $ids, SalesChannelContext $context): ProductCollection
    {
        $criteria = new Criteria($ids);
        $criteria->addAssociation('cover');
        $criteria->addAssociation('options.group');

        if($context->getCustomer()){
            $criteria->getAssociation('customerPrices')
                ->addFilter(new EqualsFilter('customerId', $context->getCustomer()->getId()))
                ->addSorting(new FieldSorting('quantityStart'));
        }

        /** @var ProductCollection $result */
        $result = $this->repository->search($criteria, $context)->getEntities();

        return $result;
    }
}