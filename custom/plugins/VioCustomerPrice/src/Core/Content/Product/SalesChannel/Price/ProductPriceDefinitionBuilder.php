<?php

declare(strict_types=1);


namespace VioCustomerPrice\Core\Content\Product\SalesChannel\Price;


use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\PriceDefinitionCollection;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\ReferencePriceDefinition;
use Shopware\Core\Checkout\Cart\Tax\TaxCalculator;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Price\ProductPriceDefinitionBuilderInterface;
use Shopware\Core\Content\Product\SalesChannel\Price\ProductPriceDefinitions;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\Tax\Exception\TaxNotFoundException;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use VioCustomerPrice\Entity\CustomerPriceCollection;
use VioCustomerPrice\Entity\CustomerPriceEntity;

class ProductPriceDefinitionBuilder implements ProductPriceDefinitionBuilderInterface
{
    /**
     * @var ProductPriceDefinitionBuilderInterface
     */
    private $productPriceDefinitionBuilder;
    /**
     * @var TaxCalculator
     */
    private $taxCalculator;
    /**
     * @var SystemConfigService
     */
    private $configService;


    public function __construct(
        ProductPriceDefinitionBuilderInterface $productPriceDefinitionBuilder,
        TaxCalculator $taxCalculator,
        SystemConfigService $configService
    )
    {
        $this->productPriceDefinitionBuilder = $productPriceDefinitionBuilder;
        $this->taxCalculator = $taxCalculator;
        $this->configService = $configService;
    }

    /**
     * @param ProductEntity $product
     * @param SalesChannelContext $context
     * @param int $quantity
     * @return ProductPriceDefinitions
     * @throws TaxNotFoundException
     */
    public function build(ProductEntity $product, SalesChannelContext $context, int $quantity = 1): ProductPriceDefinitions
    {
        $productPriceDefinitions = null;
        $coreProductPriceDefinitions = $this->productPriceDefinitionBuilder->build($product, $context, $quantity);
        $customer = $context->getCustomer();
        if($customer instanceof CustomerEntity) {
            $originalPrice = $coreProductPriceDefinitions->getPrice()->getPrice();
            $originalListPrice = $coreProductPriceDefinitions->getPrice()->getListPrice();
            // check if original price should be displayed as pseudo price
            if($originalListPrice === null && $this->configService->getBool('VioCustomerPrice.config.originalAsListPrice')) {
                $originalListPrice = $originalPrice;
            }
            // check to build a price rule
            /** @var CustomerPriceCollection $customerPricesCollection */
            $customerPricesCollection = $product->getExtension('customerPrices');

            if($customerPricesCollection instanceof CustomerPriceCollection && $customerPricesCollection->first() !== null) {
                /** @var QuantityPriceDefinition[] $prices */
                $prices = [];
                $taxRules = $context->buildTaxRules($product->getTaxId());
                $quantityCustomerPrice = null;

                /** @var CustomerPriceEntity $customerPrice */
                foreach ($customerPricesCollection as $customerPrice) {
                    $curQuantity = $customerPrice->getQuantityEnd();
                    if($curQuantity === 0 || $curQuantity === null){
                        $curQuantity = $customerPrice->getQuantityStart();
                    }

                    if($customerPrice->getQuantityStart() <= $quantity) {
                        $quantityCustomerPrice = $customerPrice;
                    }

                    $prices[] = $this->calcTax(new QuantityPriceDefinition(
                        $this->calcCustomerPrice($customerPrice, $originalPrice),
                        $taxRules,
                        $context->getContext()->getCurrencyPrecision(),
                        $curQuantity,
                        true,
                        $this->buildReferencePriceDefinition($product),
                        $originalListPrice
                    ), $context);
                }

                $quantityPrice = $this->calcTax(new QuantityPriceDefinition(
                    $this->calcCustomerPrice($quantityCustomerPrice, $originalPrice),
                    $taxRules,
                    $context->getContext()->getCurrencyPrecision(),
                    $quantity,
                    true,
                    $this->buildReferencePriceDefinition($product),
                    $originalListPrice
                ), $context);

                $productPriceDefinitions = new ProductPriceDefinitions(
                    $prices[0],
                    new PriceDefinitionCollection($prices),
                    $prices[count($prices) - 1],
                    $prices[0],
                    $quantityPrice
                );
            }
        }
        return $productPriceDefinitions ?? $coreProductPriceDefinitions;
    }

    private function calcTax(QuantityPriceDefinition $quantityPriceDefinition, SalesChannelContext $context) : QuantityPriceDefinition
    {
        if ($context->getTaxState() === CartPrice::TAX_STATE_GROSS) {
            // calc tax
            $grossPrice = $this->taxCalculator->calculateGross(
                $quantityPriceDefinition->getPrice(),
                $quantityPriceDefinition->getTaxRules()
            );
            $quantityPriceDefinition = new QuantityPriceDefinition(
                $grossPrice,
                $quantityPriceDefinition->getTaxRules(),
                $quantityPriceDefinition->getPrecision(),
                $quantityPriceDefinition->getQuantity(),
                $quantityPriceDefinition->isCalculated(),
                $quantityPriceDefinition->getReferencePriceDefinition(),
                $quantityPriceDefinition->getListPrice()
            );
        }
        return $quantityPriceDefinition;
    }

    private function buildReferencePriceDefinition(ProductEntity $product): ?ReferencePriceDefinition
    {
        $referencePrice = null;
        if (
            $product->getPurchaseUnit()
            && $product->getReferenceUnit()
            && $product->getUnit() !== null
            && $product->getPurchaseUnit() !== $product->getReferenceUnit()
        ) {
            $referencePrice = new ReferencePriceDefinition(
                $product->getPurchaseUnit(),
                $product->getReferenceUnit(),
                (string) $product->getUnit()->getTranslation('name')
            );
        }

        return $referencePrice;
    }

    private function calcCustomerPrice(CustomerPriceEntity $customerPrice, float $originalPrice): float
    {
        if($customerPrice->getPrice() > 0) {
            return $customerPrice->getPrice();
        }
        /** @noinspection NotOptimalIfConditionsInspection */
        if($customerPrice->getDiscount() !== null && $customerPrice->getDiscount() !== 0.0) {
            return $originalPrice * (1 - ($customerPrice->getDiscount() / 100));
        }
        return $originalPrice;
    }
}
