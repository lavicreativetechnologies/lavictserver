<?php

declare(strict_types=1);

namespace VioCustomerPrice\Subscriber;

use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Framework\Api\Context\SalesChannelApiSource;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductSubscriber implements EventSubscriberInterface
{

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var EntityRepositoryInterface
     */
    private $customerPriceRepository;
    /**
     * @var SalesChannelContextPersister
     */
    private $salesChannelContextPersister;

    /**
     * ProductSubscriber constructor.
     * @param RequestStack $requestStack
     * @param EntityRepositoryInterface $customerPriceRepository
     * @param SalesChannelContextPersister $salesChannelContextPersister
     */
    public function __construct(
        RequestStack $requestStack,
        EntityRepositoryInterface $customerPriceRepository,
        SalesChannelContextPersister $salesChannelContextPersister
    )
    {
        $this->requestStack = $requestStack;
        $this->customerPriceRepository = $customerPriceRepository;
        $this->salesChannelContextPersister = $salesChannelContextPersister;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            ProductEvents::PRODUCT_LOADED_EVENT => 'onProductLoaded'
        ];
    }

    /**
     * @param EntityLoadedEvent $event
     */
    public function onProductLoaded(EntityLoadedEvent $event): void
    {
        if($event->getDefinition() instanceof ProductDefinition){
            $source = $event->getContext()->getSource();

            if($source instanceof SalesChannelApiSource) {
                $token = $this->requestStack->getCurrentRequest()->headers->get(PlatformRequest::HEADER_CONTEXT_TOKEN);
                $salesChannelId = $source->getSalesChannelId();
                $payload = $this->salesChannelContextPersister->load($token, $salesChannelId);

                if(is_array($payload) && array_key_exists('customerId', $payload)) {
                    $customerPricesResult = $this->customerPriceRepository
                        ->search(
                            (new Criteria())
                            ->addFilter(
                                new EqualsFilter('customerId', $payload['customerId']),
                                new EqualsAnyFilter('productId', $event->getIds())
                            ),
                            $event->getContext()
                        );

                    /** @var ProductEntity $product */
                    foreach ($event->getEntities() as $product) {
                        $customerPrices = $customerPricesResult->filterByProperty('productId', $product->getId());
                        $product->addExtension('customerPrices', $customerPrices->getEntities());
                    }
                }
            }
        }
    }
}
