<?php


namespace VioCustomerPrice\Subscriber;


use Exception;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenContainerEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use VioCustomerPrice\Entity\CustomerPriceEntity;

class EntityWrittenContainerSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityRepositoryInterface
     */
    private $entityRepository;

    public function __construct(
        EntityRepositoryInterface $entityRepository
    )
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            EntityWrittenContainerEvent::class => "onEntityWrittenContainerEvent"
        ];
    }

    /**
     * @param EntityWrittenContainerEvent $event
     * @throws InconsistentCriteriaIdsException
     */
    public function onEntityWrittenContainerEvent(EntityWrittenContainerEvent $event)
    {
        foreach ($event->getEvents() as $nestedEvent){
            if($nestedEvent instanceof EntityWrittenEvent){
                foreach ($nestedEvent->getWriteResults() as $entityWriteResult){
                    if($entityWriteResult->getEntityName() === 'vio_customer_price'
                        && $entityWriteResult->getExistence() !== null
                        && ($entityWriteResult->getOperation() === 'update' || $entityWriteResult->getOperation() === 'create')
                        && $entityWriteResult->getExistence()->exists()
                        && !array_key_exists('quantityEnd', $entityWriteResult->getPayload())
                    ) {
                        $this->reorderPrices($entityWriteResult->getPrimaryKey(), $event->getContext());
                    }
                }
            }
        }
    }

    /**
     * reorder the related prices to the given price
     * @param string $priceId
     * @param Context $context
     * @throws InconsistentCriteriaIdsException
     * @throws Exception
     */
    private function reorderPrices(string $priceId, Context $context)
    {
        /** @var CustomerPriceEntity $customerPrice */
        $customerPrice = $this->entityRepository->search(new Criteria([$priceId]), $context)->first();
        if($customerPrice !== null){
            $criteria = new Criteria();
            $criteria
                ->addFilter(new EqualsFilter('customerId', $customerPrice->getCustomerId()))
                ->addFilter(new EqualsFilter('productId', $customerPrice->getProductId()))
                ->addSorting(new FieldSorting('quantityStart', FieldSorting::ASCENDING));
            $prices = $this->entityRepository->search($criteria, $context);
            if($prices->count() > 1){
                $iter = $prices->getIterator();
                /** @var CustomerPriceEntity|null $lastPrice */
                $lastPrice = null;
                $updateData = [];
                /** @var CustomerPriceEntity $curPrice */
                foreach ($prices as $curPrice){
                    if($lastPrice !== null) {
                        $updateData[] = ['id' => $lastPrice->getId(), 'quantityEnd' => $curPrice->getQuantityStart() - 1];
                    }
                    $lastPrice = $curPrice;
                }
                $this->entityRepository->update($updateData, $context);
            }
        }
    }
}
