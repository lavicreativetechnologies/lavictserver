const { Component } = Shopware;
import template from './sw-customer-detail.html.twig';

Component.override('sw-customer-detail', {
    template
});