import template from './vio-customer-price-detail.html.twig';

const { Component, Mixin } = Shopware;

Component.register('vio-customer-price-detail', {
    template,

    inject: [
        'repositoryFactory',
        'acl'
    ],

    mixins: [
        Mixin.getByName('notification')
    ],

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    data() {
        return {
            customerPrice: null,
            isLoading: false,
            processSuccess: false,
            repository: null
        };
    },

    created() {
        this.repository = this.repositoryFactory.create('vio_customer_price');
        this.getCustomerPrice();
    },

    methods: {
        getCustomerPrice() {
            this.repository
                .get(this.$route.params.id, Shopware.Context.api)
                .then((entity) => {
                    this.customerPrice = entity;
                });
        },

        onClickSave() {
            this.isLoading = true;

            this.repository
                .save(this.customerPrice, Shopware.Context.api)
                .then(() => {
                    this.getCustomerPrice();
                    this.isLoading = false;
                    this.processSuccess = true;
                }).catch((exception) => {
                this.isLoading = false;
                this.createNotificationError({
                    title: this.$t('vio-customer-price.detail.errorTitle'),
                    message: exception.message
                });
            });
        },

        saveFinish() {
            this.processSuccess = false;
        }
    }
});
