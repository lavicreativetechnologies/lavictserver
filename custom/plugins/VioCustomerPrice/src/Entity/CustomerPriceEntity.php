<?php declare(strict_types=1);

namespace VioCustomerPrice\Entity;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class CustomerPriceEntity extends Entity
{
    use EntityIdTrait;

    /** @var CustomerEntity */
    protected $customer;

    /** @var string */
    protected $customerId;

    /** @var ProductEntity */
    protected $product;

    /** @var string */
    protected $productId;

    /**  @var int */
    protected $quantityStart;

    /** @var int|null */
    protected $quantityEnd;

    /** @var ?float */
    protected $price;

    /** @var ?float */
    protected $discount;

    /**
     * @return CustomerEntity
     */
    public function getCustomer(): CustomerEntity
    {
        return $this->customer;
    }

    /**
     * @param CustomerEntity $customer
     * @return CustomerPriceEntity
     */
    public function setCustomer(CustomerEntity $customer): CustomerPriceEntity
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     * @return CustomerPriceEntity
     */
    public function setCustomerId(string $customerId): CustomerPriceEntity
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return ProductEntity
     */
    public function getProduct(): ProductEntity
    {
        return $this->product;
    }

    /**
     * @param ProductEntity $product
     * @return CustomerPriceEntity
     */
    public function setProduct(ProductEntity $product): CustomerPriceEntity
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     * @return CustomerPriceEntity
     */
    public function setProductId(string $productId): CustomerPriceEntity
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityStart(): int
    {
        return $this->quantityStart;
    }

    /**
     * @param int $quantityStart
     * @return CustomerPriceEntity
     */
    public function setQuantityStart(int $quantityStart): CustomerPriceEntity
    {
        $this->quantityStart = $quantityStart;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantityEnd(): ?int
    {
        return $this->quantityEnd;
    }

    /**
     * @param int|null $quantityEnd
     * @return CustomerPriceEntity
     */
    public function setQuantityEnd(?int $quantityEnd): CustomerPriceEntity
    {
        $this->quantityEnd = $quantityEnd;
        return $this;
    }

    /**
     * @return ?float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param ?float $price
     * @return CustomerPriceEntity
     */
    public function setPrice(?float $price): CustomerPriceEntity
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return ?float
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     * @return CustomerPriceEntity
     */
    public function setDiscount(?float $discount): CustomerPriceEntity
    {
        $this->discount = $discount;
        return $this;
    }

}
