<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Core\Note;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 *
 * @method void            add(NoteEntity $entity)
 * @method void            set(string $key, NoteEntity $entity)
 * @method NoteEntity[]    getIterator()
 * @method NoteEntity[]    getElements()
 * @method NoteEntity|null get(string $key)
 * @method NoteEntity|null first()
 * @method NoteEntity|null last()
 */
class NoteCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return NoteEntity::class;
    }
}
