<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Structs;

use Shopware\Core\Framework\Struct\Struct;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class UploadedFileStruct extends Struct
{
    protected string $originalFilename;

    protected File $file;

    public function __construct(string $originalFilename, File $file)
    {
        $this->originalFilename = $originalFilename;
        $this->file = $file;
    }

    public function getOriginalFilename(): string
    {
        return $this->originalFilename;
    }

    public function getFile(): File
    {
        return $this->file;
    }
}
