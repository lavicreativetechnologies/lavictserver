<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1587973796 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1587973796;
    }

    public function update(Connection $connection): void
    {
        $connection->executeQuery('
            CREATE TABLE IF NOT EXISTS `ies_delivery_note` (
                `id` BINARY(16) NOT NULL,
                `customer_id` BINARY(16) NOT NULL,
                `name` varchar(100) NOT NULL,
                `path` varchar(300) NOT NULL DEFAULT "",
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.ies_delivery_note.customer_id`
                    FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
