<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Controller\Storefront;

use Ies\DeliveryNote\Services\UploadService;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 *
 * @RouteScope(scopes={"storefront"})
 */
class DeliveryNoteController extends StorefrontController
{
    private UploadService $uploadService;

    private EntityRepositoryInterface $deliveryNoteRepository;

    public function __construct(
        UploadService $uploadService,
        EntityRepositoryInterface $deliveryNoteRepository
    ) {
        $this->uploadService = $uploadService;
        $this->deliveryNoteRepository = $deliveryNoteRepository;
    }

    /**
     * @Route("/delivery-note/upload", name="frontend.ies_delivery_note.upload", methods={"POST"}, defaults={"XmlHttpRequest"=true})
     */
    public function upload(Request $request, SalesChannelContext $context): Response
    {
        $redirectTo = $request->get('redirectTo', 'frontend.account.home.page');
        $customer = $context->getCustomer();

        if (!$customer) {
            return $this->redirectToRoute($redirectTo);
        }

        $uploadedFile = $this->uploadService->upload();

        if ($uploadedFile === null) {
            $this->addFlash('warning', $this->trans('delivery-note.upload.error'));

            return $this->redirectToRoute($redirectTo);
        }

        $noteId = Uuid::randomHex();
        $this->deliveryNoteRepository->create([
            [
                'id' => $noteId,
                'name' => $uploadedFile->getOriginalFilename(),
                'path' => $uploadedFile->getFile()->getPathname(),
                'customerId' => $customer->getId()
            ]
        ], $context->getContext());

        return $this->redirectToRoute($redirectTo);
    }

    /**
     * @Route("/delivery-note/remove", name="frontend.ies_delivery_note.remove", methods={"POST"}, defaults={"XmlHttpRequest"=true})
     */
    public function remove(Request $request, SalesChannelContext $context): Response
    {
        $redirectTo = $request->get('redirectTo', 'frontend.account.home.page');
        $customer = $context->getCustomer();

        if (!$customer) {
            return $this->redirectToRoute($redirectTo);
        }

        $data = [
            'id' => $request->get('id'),
            'customerId' => $customer->getId()
        ];

        $this->deliveryNoteRepository->delete([$data], $context->getContext());

        // @todo: delete from file system if not assined to an order

        return $this->redirectToRoute($redirectTo);
    }
}
