<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Services;

use Ies\DeliveryNote\Structs\UploadedFileStruct;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class UploadService
{
    public const UPLOAD_DIR = 'uploads/delivery';

    private FilesystemInterface $filesystem;

    public function __construct(FilesystemInterface $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function upload(string $newFilename = null): ?UploadedFileStruct
    {
        $uploadedFiles = $this->getUploadedFiles();
        if ($uploadedFiles->count() === 0) {
            return null;
        }

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $uploadedFiles->get('upload');

        if (!$this->isValidUpload($uploadedFile)) {
            return null;
        }

        $this->ensureUploadDir();

        $newFilename = $newFilename ? $newFilename : md5((string)time());

        try {
            $movedFile = $uploadedFile->move(
                self::UPLOAD_DIR,
                $newFilename . '.' . $uploadedFile->guessExtension()
            );

            return new UploadedFileStruct(
                $uploadedFile->getClientOriginalName(),
                $movedFile
            );
        } catch (FileException $ex) {
            return null;
        }
    }

    protected function ensureUploadDir(): void
    {
        if (!$this->filesystem->has(self::UPLOAD_DIR)) {
            $this->filesystem->createDir(self::UPLOAD_DIR);
        }
    }

    protected function getUploadedFiles(): FileBag
    {
        $request = Request::createFromGlobals();

        return $request->files;
    }

    protected function isValidUpload(UploadedFile $uploadedFile): bool
    {
        if ($uploadedFile === null) {
            return false;
        }

        if (($uploadedFile->getSize() < 1 && $uploadedFile->getError() == UPLOAD_ERR_INI_SIZE)) {
            return false;
        }

        $mimeType = $uploadedFile->getMimeType();

        return $mimeType == 'image/jpeg' || $mimeType == 'image/jpg' || $mimeType == 'application/pdf';
    }
}
