<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Decorators;

use League\Flysystem\FilesystemInterface;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Content\MailTemplate\Service\MailService;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelEntity;
use League\Flysystem\FileNotFoundException;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class MailServiceDecorator extends MailService
{
    private MailService $mailService;
    private FilesystemInterface $fileSystem;

    public function __construct(MailService $mailService, FilesystemInterface $filesystem)
    {
        $this->mailService = $mailService;
        $this->fileSystem = $filesystem;
    }

    public function send(array $data, Context $context, array $templateData = []): ?\Swift_Message
    {
        if (!isset($templateData['order'])) {
            return $this->mailService->send($data, $context, $templateData);
        }

        /** @var OrderEntity $order */
        $order = $templateData['order'];
        $customFields = $order->getCustomFields();

        if ($order && isset($customFields['deliveryNote'])) {
            $deliveryNote = $customFields['deliveryNote'];

            try {
                $attachmentData[] = $this->generateAttachmentData(
                    $order->getOrderNumber(),
                    $deliveryNote['path']
                );

                // add attachment to existing ones
                $data['binAttachments'] = ($data['binAttachments'] ?? []) + $attachmentData;
            } catch (\Exception $ex) {
            }
        }

        return $this->mailService->send($data, $context, $templateData);
    }

    public function buildContents(array $data, ?SalesChannelEntity $salesChannel): array
    {
        return $this->mailService->buildContents($data, $salesChannel);
    }

    /**
     * @param string $name
     * @param string $path
     *
     * @return array
     *
     * @throws FileNotFoundException
     * @throws \Swift_IoException
     */
    protected function generateAttachmentData(string $name, string $path): array
    {
        $extension = pathinfo($path)['extension'];
        $mimeType = $this->fileSystem->getMimetype($path);

        return [
            'content' => new \Swift_ByteStream_FileByteStream($path),
            'fileName' => $name . '.' . $extension,
            'mimeType' => $mimeType
        ];
    }
}
