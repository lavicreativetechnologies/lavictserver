<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Subscriber;

use Ies\DeliveryNote\Core\Note\NoteEntity;
use Psr\Container\ContainerInterface;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Order\CartConvertedEvent;
use Shopware\Core\Checkout\Cart\Order\Transformer\LineItemTransformer;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Tax\Struct\CalculatedTaxCollection;
use Shopware\Core\Checkout\Cart\Tax\Struct\TaxRuleCollection;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Storefront\Page\Checkout\Confirm\CheckoutConfirmPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class OrderSubscriber implements EventSubscriberInterface
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CartConvertedEvent::class => 'onCartConverted',
            CheckoutConfirmPageLoadedEvent::class => 'onConfirmPageLoaded',
        ];
    }

    /**
     * Adds the delivery note information to order object
     */
    public function onCartConverted(CartConvertedEvent $event): void
    {
        $convertedCart = $event->getConvertedCart();

        $salesChannelContext = $event->getSalesChannelContext();
        $customer = $salesChannelContext->getCustomer();

        /** @var NoteEntity $deliveryNote */
        $deliveryNote = $customer->getExtension('deliveryNote');

        if ($deliveryNote) {
            $convertedCart['customFields']['deliveryNote'] = [
                'id' => $deliveryNote->getId(),
                'name' => $deliveryNote->getName(),
                'path' => $deliveryNote->getPath()
            ];

            // add new item in beginning of order items
            $lineItem = $this->createDeliveryNoteLineItem($this->getDeliveryNoteTranslation($deliveryNote->getName()));
            array_unshift($convertedCart['lineItems'], array_values(LineItemTransformer::transform($lineItem, null, -1))[0]);
        }

        $event->setConvertedCart($convertedCart);
    }

    /**
     * Adds a new line item on confirm page, if delivery note was uploaded
     *
     * @throws \Shopware\Core\Checkout\Cart\Exception\InvalidQuantityException
     * @throws \Shopware\Core\Checkout\Cart\Exception\LineItemNotStackableException
     * @throws \Shopware\Core\Checkout\Cart\Exception\MixedLineItemTypeException
     */
    public function onConfirmPageLoaded(CheckoutConfirmPageLoadedEvent $event): void
    {
        $customer = $event->getSalesChannelContext()->getCustomer();

        /** @var NoteEntity $deliveryNote */
        if (!$deliveryNote = $customer->getExtension('deliveryNote')) {
            return;
        }

        $cart = $event->getPage()->getCart();

        $oldLineItemCollection = $cart->getLineItems();

        $lineItemCollection = new LineItemCollection([
            $this->createDeliveryNoteLineItem(
                $this->getDeliveryNoteTranslation($deliveryNote->getName())
            )
        ]);

        $cart->setLineItems($lineItemCollection);
        $cart->addLineItems($oldLineItemCollection);

        $event->getPage()->setCart($cart);
    }

    protected function createDeliveryNoteLineItem(string $label): LineItem
    {
        $lineItem = new LineItem(Uuid::randomHex(), LineItem::CUSTOM_LINE_ITEM_TYPE);

        return $lineItem
            ->setLabel($label)
            ->setGood(false)
            ->setPrice(
                new CalculatedPrice(
                    0,
                    0,
                    new CalculatedTaxCollection(),
                    new TaxRuleCollection()
                )
            );
    }

    private function getDeliveryNoteTranslation(string $name = ''): string
    {
        return $this->container->get('translator')->trans('delivery-note.line-item.name', [
            '%name%' => $name,
        ]);
    }
}
