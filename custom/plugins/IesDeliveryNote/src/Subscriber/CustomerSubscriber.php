<?php declare(strict_types=1);

namespace Ies\DeliveryNote\Subscriber;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Customer\CustomerEvents;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CustomerSubscriber implements EventSubscriberInterface
{
    private EntityRepositoryInterface $deliveryNoteRepository;

    public function __construct(EntityRepositoryInterface $deliveryNoteRepository)
    {
        $this->deliveryNoteRepository = $deliveryNoteRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CustomerEvents::CUSTOMER_LOADED_EVENT => 'onCustomerLoaded',
        ];
    }

    public function onCustomerLoaded(EntityLoadedEvent $event): void
    {
        /** @var CustomerEntity $customerEntity */
        foreach ($event->getEntities() as $customerEntity) {
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('customerId', $customerEntity->getId()));

            $result = $this->deliveryNoteRepository->search($criteria, $event->getContext());

            $customerEntity->addExtension('deliveryNote', $result->first());
        }
    }
}
