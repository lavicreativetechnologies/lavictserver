<?php declare(strict_types=1);

namespace Ies\DddViewer\Structs;

use Shopware\Core\Framework\Struct\Struct;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class ViewerStruct extends Struct
{
    public bool $hasViewer = false;

    public function __construct(bool $hasViewer)
    {
        $this->hasViewer = $hasViewer;
    }
}
