<?php declare(strict_types=1);

namespace Ies\DddViewer\Services;

use League\Flysystem\FilesystemInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DddViewerValidator
{
    private const DDD_VIEWER_PATH = 'mediafiles/singleproducts/3dviewer/';

    private FilesystemInterface $filesystem;

    public function __construct(FilesystemInterface $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function validate(string $name): bool
    {
        return $this->filesystem->has(self::DDD_VIEWER_PATH . $name);
    }
}
