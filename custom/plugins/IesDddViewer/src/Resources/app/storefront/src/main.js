import './vendors/viewer';
import DddViewerPlugin from './ddd-viewer/ddd-viewer.plugin';

window.PluginManager.register('DddViewer', DddViewerPlugin, '[data-ddd-viewer]');
