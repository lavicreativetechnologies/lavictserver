import Plugin from 'src/plugin-system/plugin.class';

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
export default class DddViewerPlugin extends Plugin {
    /**
     * Plugin settings
     */
    static options = {
        /**
         * selector for the ddd modal
         */
        modalSelector: '.js-ddd-modal',

        /**
         * selector for the container of the gallery and the zoom modal
         */
        galleryZoomModalContainerSelector: '.js-gallery-zoom-modal-container',

        /**
         * Folder name of loading viewer
         */
        articleNumber: ''
    };

    init() {
        this.el.addEventListener('click', this._onClick.bind(this));
    }

    _onClick(e) {
        e.preventDefault();
        e.stopPropagation();

        const galleryZoomModalContainer = this.el.closest(this.options.galleryZoomModalContainerSelector);
        const modal = galleryZoomModalContainer.querySelector(this.options.modalSelector);

        if (modal) {
            const $modal = $(modal);

            $modal.off('shown.bs.modal');
            $modal.on('shown.bs.modal', () => {
                this.init3dViewer();

                this.$emitter.publish('modalShow', { modal });
            });

            $modal.modal('show');
        }

        this.$emitter.publish('onClick', { modal });
    }

    /**
     * Inits the viewer
     */
    init3dViewer() {
        if (this.options.articleNumber === '') {
            return;
        }

        const nameOfDiv = 'dddviewer';
        const folderName = 'mediafiles/singleproducts/3dviewer/' + this.options.articleNumber;
        const viewPortWidth = 400;
        const viewPortHeight = 259;
        const backgroundColor = '#FFFFFF';
        const uMouseSensitivity = -0.05;
        const vMouseSensitivity = 0.05;
        const uStartIndex = 9;
        const vStartIndex = 0;
        const rotationDamping = 0.96;
        const showLoading = true;
        const loadingIcon = 'loader.png';

        new window.Viewer(
            nameOfDiv,
            folderName,
            viewPortWidth,
            viewPortHeight,
            backgroundColor,
            18,
            18,
            true,
            true,
            uMouseSensitivity,
            vMouseSensitivity,
            uStartIndex,
            vStartIndex,
            1, // min-zoom
            1, // max-zoom
            rotationDamping,
            true,
            false,
            false,
            'jpg',
            showLoading,
            loadingIcon,
            false,
            false,
            false,
            {}
        );
    }
}
