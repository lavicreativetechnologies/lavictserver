<?php declare(strict_types=1);

namespace Ies\DddViewer;

use Shopware\Core\Framework\Plugin;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DddViewer extends Plugin
{
}
