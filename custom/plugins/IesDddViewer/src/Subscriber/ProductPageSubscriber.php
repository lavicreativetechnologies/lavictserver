<?php declare(strict_types=1);

namespace Ies\DddViewer\Subscriber;

use Ies\DddViewer\Services\DddViewerValidator;
use Ies\DddViewer\Structs\ViewerStruct;
use Shopware\Storefront\Page\Product\ProductPage;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class ProductPageSubscriber implements EventSubscriberInterface
{
    private DddViewerValidator $validator;

    public function __construct(DddViewerValidator $validator)
    {
        $this->validator = $validator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductPageLoadedEvent::class => 'onProductPageLoaded',
        ];
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event): void
    {
        /** @var ProductPage $page */
        $page = $event->getPage();
        $product = $page->getProduct();

        $isValid = $this->validator->validate($product->getProductNumber());

        $product->addExtension('dddViewer', new ViewerStruct($isValid));

        $page->setProduct($product);
    }
}
