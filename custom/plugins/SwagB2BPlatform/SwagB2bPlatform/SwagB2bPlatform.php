<?php declare(strict_types=1);

namespace SwagB2bPlatform;

use Shopware\B2B\Common\B2BContainerBuilder;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use SwagB2bPlatform\Resources\DependencyInjection\SwagB2bPlatformAdministrationConfiguration;
use SwagB2bPlatform\Resources\DependencyInjection\SwagB2bPlatformApiConfiguration;
use SwagB2bPlatform\Resources\DependencyInjection\SwagB2bPlatformFrontendConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollectionBuilder;
use function file_exists;

class SwagB2bPlatform extends Plugin
{
    public static function isPackage(): bool
    {
        return file_exists(self::getPackageVendorAutoload());
    }

    public static function getPackageVendorAutoload(): string
    {
        return __DIR__ . '/../vendor/autoload.php';
    }

    /**
     * @return string[]
     */
    public static function getMigrationPaths(): array
    {
        if (self::isPackage()) {
            return [
                __DIR__ . '/Resources/Migration',
            ];
        }

        return [
            __DIR__ . '/../../components/SwagB2bPlatform/Resources/Migration',
        ];
    }

    public function install(InstallContext $context): void
    {
        (new SetUp($this->container))->setUp(self::getMigrationPaths());
    }

    public function update(UpdateContext $context): void
    {
        (new SetUp($this->container))->setUp(self::getMigrationPaths());
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $containerBuilder = B2BContainerBuilder::create();
        $containerBuilder->addConfiguration(new SwagB2bPlatformFrontendConfiguration());
        $containerBuilder->addConfiguration(new SwagB2bPlatformAdministrationConfiguration());
        $containerBuilder->addConfiguration(new SwagB2bPlatformApiConfiguration());

        $containerBuilder->registerConfigurations($container);
    }

    public function configureRoutes(RouteCollectionBuilder $routes, string $environment): void
    {
        parent::configureRoutes($routes, $environment);
        $routes->import('.', null, 'b2b');
    }

    protected function getServicesFilePath(): string
    {
        return 'Resources/config/services.xml';
    }
}

if (SwagB2bPlatform::isPackage()) {
    require_once SwagB2bPlatform::getPackageVendorAutoload();
}
