<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing;

use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use function mb_strpos;
use function mb_strtolower;
use function mb_substr;
use function preg_match;
use function preg_replace_callback;

class RouteLoader extends Loader
{
    private const PREFIX = 'frontend.b2b.';

    public const ROUTE_IS_B2B = '_b2b';

    public const ROUTE_TEMPLATE = '_b2b_template_name';

    public const ROUTE_CONTROLLER_ROUTE_NAME = '_b2b_controller_route_name';

    public const ROUTE_CONTROLLER_ACTION = '_b2b_controller_action';

    public const ROUTE_ARGUMENTS = '_b2b_arguments';

    /**
     * @var array
     */
    private $controllers;

    public function __construct(
        array $controllers
    ) {
        $this->controllers = $controllers;
    }

    public function load($resource, $type = null): RouteCollection
    {
        $routes = new RouteCollection();

        // index routes must a created deferred so the wildcard does not match first
        $indexRoutes = [];

        /**
         * @var string
         * @var RouteValue $definition
         */
        foreach ($this->controllers as $controllerRouteName => $definition) {
            $reflectionClass = new ReflectionClass($definition->className);

            $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);

            foreach ($methods as $method) {
                $actionName = $this->extractAction($method);

                if ($actionName === null) {
                    continue;
                }

                if ($actionName === 'index') {
                    $indexRoutes[self::PREFIX . $controllerRouteName . '.index'] = $this->createRoute($controllerRouteName, $actionName, $definition, $method);
                    continue;
                }

                $routes->add(
                    self::PREFIX . $controllerRouteName . '.' . $actionName,
                    $this->createRoute($controllerRouteName, $actionName, $definition, $method)
                );
            }
        }

        foreach ($indexRoutes as $key => $route) {
            $routes->add($key, $route);
        }

        return $routes;
    }

    /**
     * @internal
     */
    protected function extractAction(ReflectionMethod $reflectionMethod): ?string
    {
        if (mb_strpos($reflectionMethod->getName(), 'Action', -6) === false) {
            return null;
        }

        return mb_substr($reflectionMethod->getName(), 0, -6);
    }

    public function supports($resource, $type = null): bool
    {
        return $type === 'b2b';
    }

    /**
     * @internal
     */
    protected function createRoute(string $controllerRouteName, string $actionName, RouteValue $definition, ReflectionMethod $method): Route
    {
        $routePath = $this->determineRoutePath($controllerRouteName, $actionName);

        $route = new Route($routePath);
        $route->setMethods(['GET', 'POST']);
        $route->setDefault('_controller', $definition->serviceId . '::' . $method->getName());

        $this->setB2bInformation($controllerRouteName, $actionName, $route);
        $this->setArgumentWildcard($route);
        $this->setShopwareDefaults($route);

        return $route;
    }

    /**
     * @internal
     */
    protected function setShopwareDefaults(Route $route): void
    {
        $route->setDefault('XmlHttpRequest', true);
        $route->setOption('seo', false);
    }

    /**
     * @internal
     */
    protected function setB2bInformation(string $controllerRouteName, string $actionName, Route $route): void
    {
        $route->setDefault(self::ROUTE_IS_B2B, true);
        $route->setDefault(self::ROUTE_TEMPLATE, $controllerRouteName . '/' . $this->toDashCase($actionName));
        $route->setDefault(self::ROUTE_CONTROLLER_ROUTE_NAME, $controllerRouteName);
        $route->setDefault(self::ROUTE_CONTROLLER_ACTION, $actionName);
    }

    /**
     * @internal
     */
    protected function setArgumentWildcard(Route $route): void
    {
        $route->setDefault(self::ROUTE_ARGUMENTS, '__NONE_SET__');
        $route->setRequirement(self::ROUTE_ARGUMENTS, '.+');
    }

    /**
     * @internal
     */
    protected function determineRoutePath(string $controllerRouteName, string $actionName): string
    {
        $routePath = '/' . $controllerRouteName;

        if ($actionName !== 'index') {
            $routePath .= '/' . $actionName;
        }

        $routePath .= '/{' . self::ROUTE_ARGUMENTS . '}';

        return $routePath;
    }

    /**
     * @internal
     */
    protected function toDashCase(string $input): string
    {
        if (preg_match('/[A-Z]/', $input) === 0) {
            return $input;
        }

        $input = mb_strtolower(preg_replace_callback('/([a-z])([A-Z])/', function ($a) {
            return $a[1] . '_' . mb_strtolower($a[2]);
        }, $input));

        return $input;
    }
}
