<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing;

use Generator;
use Shopware\B2B\Common\MvcExtension\FileFactory;
use Shopware\B2B\Common\MvcExtension\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use function count;
use function end;
use function explode;

class MvcRequestResolver implements ArgumentValueResolverInterface
{
    public function supports(SymfonyRequest $request, ArgumentMetadata $argument): bool
    {
        return Request::class === $argument->getType();
    }

    public function resolve(SymfonyRequest $request, ArgumentMetadata $argument): Generator
    {
        $params = $this->extractAdditionalRequestArguments($request);

        $files = [];
        foreach ($request->files->all() as $key => $file) {
            try {
                $files[$key] = FileFactory::fromUploadedFile($file);
            } catch (FileNotFoundException $e) {
                // nth
            }
        }

        yield new SymfonyMvcRequest($request, $params, $files);
    }

    /**
     * @internal
     */
    protected function extractAdditionalRequestArguments(SymfonyRequest $request): array
    {
        $parameters = $request->attributes->get('_route_params');

        if (!isset($parameters[RouteLoader::ROUTE_ARGUMENTS])) {
            return [];
        }

        $arguments = explode('/', $parameters[RouteLoader::ROUTE_ARGUMENTS]);
        $argumentsCount = count($arguments);
        $params = [];

        for ($i = 1; $i < $argumentsCount; $i += 2) {
            $params[$arguments[$i - 1]] = $arguments[$i];
        }

        if ($argumentsCount % 2 !== 0) {
            $params[end($arguments)] = null;
        }

        return $params;
    }
}
