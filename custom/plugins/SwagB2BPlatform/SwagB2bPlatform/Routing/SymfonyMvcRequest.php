<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing;

use InvalidArgumentException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request as SymfonyHttpRequest;
use function array_key_exists;

class SymfonyMvcRequest implements Request
{
    /**
     * @var SymfonyHttpRequest
     */
    private $request;

    /**
     * @var array
     */
    private $additionalParams;

    /**
     * @var array
     */
    private $files;

    public function __construct(SymfonyHttpRequest $request, array $additionalParams, array $files = [])
    {
        $this->request = $request;
        $this->additionalParams = $additionalParams;
        $this->files = $files;
    }

    public function getParam($key, $default = null)
    {
        $result = $this->request->get($key);

        if ($result !== null && $result !== '') {
            return $result;
        }

        if (isset($this->additionalParams[$key])) {
            return $this->additionalParams[$key];
        }

        return $default;
    }

    public function getIdValue($key, $default = null): IdValue
    {
        return IdValue::create($this->getParam($key, $default));
    }

    public function requireFileParam($key): File
    {
        if (array_key_exists($key, $this->files)) {
            return $this->files[$key];
        }

        throw new InvalidArgumentException('Missing required file parameter "' . $key . '"');
    }

    public function requireParam($key)
    {
        $param = $this->getParam($key);

        if (!$param) {
            throw new InvalidArgumentException('Missing required parameter "' . $key . '"');
        }

        return $param;
    }

    public function requireIdValue($key): IdValue
    {
        return IdValue::create($this->requireParam($key));
    }

    public function isPost(): bool
    {
        return $this->request->getMethod() === SymfonyHttpRequest::METHOD_POST;
    }

    public function getPost(): array
    {
        return $this->request->request->all();
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function hasParam(string $key): bool
    {
        return $this->getParam($key) !== null;
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function checkPost(string $action = 'index', array $params = [], string $controller = null): void
    {
        if ($this->isPost()) {
            return;
        }

        throw new B2bControllerRedirectException($action, $controller, $params);
    }
}
