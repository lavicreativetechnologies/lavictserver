<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing\Response;

use Shopware\B2B\Common\Controller\GridHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use function array_merge;
use function ceil;
use function http_build_query;

class GridStateAdminVndApiJsonResponse extends JsonResponse
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request, array $gridState, int $status = 200, array $headers = [], bool $json = false)
    {
        $this->request = $request;

        $data = [
            'success' => true,
            'data' => $gridState['data'],
            'included' => [],
            'links' => $this->buildPaginationLinks($gridState['currentPage'], $gridState['total']),
            'meta' => [
                'total' => $gridState['total'],
                'totalCountMode' => 1,
            ],
        ];

        parent::__construct($data, $status, $headers, $json);
    }

    /**
     * @internal
     */
    protected function buildPaginationLinks(int $currentPage, int $total): array
    {
        $baseUrl = $this->getBaseUrl($this->request);
        $uri = $baseUrl . $this->request->getPathInfo();
        $parameters = [];
        $limit = GridHelper::PER_PAGE;

        $links = [
            'first' => $this->buildPaginationUrl($uri, $parameters, $limit, 1),
            'self' => $this->request->getUri(),
        ];

        if ($currentPage > 1) {
            $links['prev'] = $this->buildPaginationUrl($uri, $parameters, $limit, $currentPage - 1);
        }

        $lastPage = (int) ceil($total / $limit);
        $lastPage = $lastPage >= 1 ? $lastPage : 1;
        $links['last'] = $this->buildPaginationUrl($uri, $parameters, $limit, $lastPage);

        if ($currentPage < $lastPage) {
            $links['next'] = $this->buildPaginationUrl($uri, $parameters, $limit, $currentPage + 1);
        }

        return $links;
    }

    protected function getBaseUrl(Request $request): string
    {
        return $request->getSchemeAndHttpHost() . $request->getBasePath();
    }

    protected function buildPaginationUrl(string $uri, array $parameters, int $limit, int $page): string
    {
        $params = array_merge($parameters, ['limit' => $limit, 'page' => $page]);

        return $uri . '?' . http_build_query($params);
    }
}
