<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing;

use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Routing\KernelListenerPriorities;
use Shopware\Core\PlatformRequest;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RouteScopeResolver implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => ['resolveScope', KernelListenerPriorities::KERNEL_CONTROLLER_EVENT_PRIORITY_AUTH_VALIDATE_PRE],
        ];
    }

    public function resolveScope(ControllerEvent $event): void
    {
        $request = $event->getRequest();

        if (!$request->attributes->get(RouteLoader::ROUTE_IS_B2B)) {
            return;
        }

        $request->attributes->set(
            PlatformRequest::ATTRIBUTE_ROUTE_SCOPE,
            new RouteScope(['scopes' => ['storefront']])
        );
    }
}
