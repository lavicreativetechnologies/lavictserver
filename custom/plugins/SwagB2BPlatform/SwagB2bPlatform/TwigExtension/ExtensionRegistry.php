<?php declare(strict_types=1);

namespace SwagB2bPlatform\TwigExtension;

use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeIdentity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeIdentityInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ExtensionRegistry extends AbstractExtension implements GlobalsInterface
{
    /**
     * @var AuthenticationService
     */
    private $authService;

    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('b2b_acl_check', [RuntimeExtension::class, 'aclCheck']),
            new TwigFunction('b2b_acl', [RuntimeExtension::class, 'acl']),
            new TwigFunction('b2b_salutation', [SalutationExtension::class, 'b2bSalutation']),
            new TwigFunction('b2b_get_user_name', [UserInformationExtension::class, 'getUserName']),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('dateTime', [DateTimeExtension::class, 'formatDateTime']),
        ];
    }

    public function getGlobals(): array
    {
        return [
            'b2bSuite' => $this->authService->isB2b(),
            'isDebtor' => $this->authService->is(DebtorIdentity::class),
            'isSalesRep' => $this->authService->is(SalesRepresentativeIdentity::class),
            'isSalesRepRemoteControl' => (
                $this->authService->is(SalesRepresentativeIdentityInterface::class)
                && !$this->authService->is(SalesRepresentativeIdentity::class)
            ),
        ];
    }
}
