<?php declare(strict_types=1);

namespace SwagB2bPlatform\TwigExtension;

use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\Salutation\SalutationCollection;
use Twig\Extension\RuntimeExtensionInterface;

class SalutationExtension implements RuntimeExtensionInterface
{
    /**
     * @var EntityRepositoryInterface
     */
    private $salutationRepository;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    public function __construct(
        EntityRepositoryInterface $salutationRepository,
        ContextProvider $salesChannelContextProvider
    ) {
        $this->salutationRepository = $salutationRepository;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
    }

    /**
     * @return EntityCollection|SalutationCollection
     */
    public function b2bSalutation(): SalutationCollection
    {
        return $this->salutationRepository->search(
            new Criteria(),
            $this->salesChannelContextProvider->getContext()
        )->getEntities();
    }
}
