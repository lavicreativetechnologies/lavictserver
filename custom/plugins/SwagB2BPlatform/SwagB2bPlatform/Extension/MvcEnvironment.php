<?php declare(strict_types=1);

namespace SwagB2bPlatform\Extension;

use Shopware\B2B\Common\MvcExtension\MvcEnvironmentInterface;
use function getenv;

/**
 * @deprecated tag:v4.4.0 - Will be removed without replacement
 */
class MvcEnvironment implements MvcEnvironmentInterface
{
    public function getPathinfo(): string
    {
        return getenv('APP_URL');
    }
}
