<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\snippet\de_DE;

use Shopware\Core\System\Snippet\Files\SnippetFileInterface;

class B2bPlatform_de_DE implements SnippetFileInterface
{
    public function getName(): string
    {
        return 'b2bplatform.de-DE';
    }

    public function getPath(): string
    {
        return __DIR__ . '/b2bplatform.de-DE.json';
    }

    public function getIso(): string
    {
        return 'de-DE';
    }

    public function getAuthor(): string
    {
        return 'Shopware';
    }

    public function isBase(): bool
    {
        return false;
    }
}
