module.exports = {
  reporters: [
    'default', [
      'jest-junit',
      {
        'suiteName': 'platform administration jest',
        'outputDirectory': '../../../../../build/artifacts/',
        'outputName': 'platform-administration-jest.xml',
        'uniqueOutputName': 'false'
      }
    ]
  ],
  transform: {
    '^.+\\.js?$': 'babel-jest'
  },
  testTimeout: 10000,
  setupFilesAfterEnv: ['./tests/setupTests.js'],
  moduleNameMapper: {
      '^sesp/(.*)$': '<rootDir>/src/$1',
      '\\.twig$': '<rootDir>/tests/__mocks__/template.mock.js',
      '\\.(css|scss|less)$': '<rootDir>/tests/__mocks__/style.mock.js',
      '^module$': '<rootDir>/tests/__mocks__/module.mock',
      '^src/app/component/component$': '<rootDir>/tests/__mocks__/components.mock',
      '^__fixtures__/(.*)$': '<rootDir>/tests/__fixtures__/$1'
  },
  errorOnDeprecated: true,
  displayName: {
    name: 'B2B Platform Administration',
    color: 'lime'
  },
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.js'],
  moduleFileExtensions: ['js'],
  coverageDirectory: '../../../../../build/artifacts/platform-administration-jest',
  timers: 'fake',
  transformIgnorePatterns: ['<rootDir>/node_modules/']
};