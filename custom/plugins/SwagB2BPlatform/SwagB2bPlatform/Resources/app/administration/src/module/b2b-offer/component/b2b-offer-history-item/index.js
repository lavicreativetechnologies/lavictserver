import template from './b2b-offer-history-item.html.twig';
import './b2b-offer-history-item.scss';

const COMPONENT_NAME = 'b2b-offer-history-item';

const { Component, Mixin } = Shopware;

Component.register(COMPONENT_NAME, {
    template,
    mixins: [
        Mixin.getByName('b2b-date'),
        Mixin.getByName('b2b-currency')
    ],
    inject: [
        'logItemService',
    ],
    props: {
        item: {
            required: true,
            type: Object
        }
    },
    methods: {
        getLogComponent() {
            return this.logItemService.getLogItemByClass(this.item.logType);
        }
    }
});