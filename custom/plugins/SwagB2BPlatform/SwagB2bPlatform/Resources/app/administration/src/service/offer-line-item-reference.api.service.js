/* istanbul ignore file */

export default class OfferLineItemReferenceApiService extends Shopware.Classes.ApiService {
    constructor(httpClient, loginService, apiEndpoint = '_action/offer-line-item-reference') {
        super(httpClient, loginService, apiEndpoint);
    }

    static get name() {
        return 'offerLineItemReferenceApiService'
    }

    getAllPositions(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.get(`${this.getApiBasePath(offerId)}`, { params, headers })
          .then((response) => {
              const responseData = Shopware.Classes.ApiService.handleResponse(response);
              return responseData.data || [];
          });
    }
}