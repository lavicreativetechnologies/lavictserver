/* istanbul ignore file */

export default class SalesRepresentativeApiService extends Shopware.Classes.ApiService {
    constructor(httpClient, loginService, apiEndpoint = '_action/sales_representative') {
        super(httpClient, loginService, apiEndpoint);
    }

    static get name() {
        return 'salesRepresentativeApiService'
    }

    getClientList(salesRepId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath(salesRepId)}/list-clients`, params, {headers})
            .then((response) => {
                const data = Shopware.Classes.ApiService.handleResponse(response);

                return data.success ? data : {data: []};
            });
    }

    assignClient(salesRepId, additionalParams = {clientAuthId, assigned}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath(salesRepId)}/assign-client`, params, {headers})
            .then((response) => {
                return Shopware.Classes.ApiService.handleResponse(response);
            });
    }
}