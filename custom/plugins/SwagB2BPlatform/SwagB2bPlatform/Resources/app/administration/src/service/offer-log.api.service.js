/* istanbul ignore file */

export default class OfferLogApiService extends Shopware.Classes.ApiService {
    constructor(httpClient, loginService, apiEndpoint = '_action/offer-log') {
        super(httpClient, loginService, apiEndpoint);
    }

    static get name() {
        return 'offerLogApiService'
    }

    getOfferActivity(orderContextId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.get(`${this.getApiBasePath(orderContextId)}`, { params, headers })
          .then((response) => {
              const responseData = Shopware.Classes.ApiService.handleResponse(response);

              if (Array.isArray(responseData.data) && responseData.data.length === 0) {
                  return {};
              }

              return responseData.data || {};
          });
    }

    addComment(orderContextId, additionalParams = {}, additionalHeaders = {}) {
      const params = additionalParams;
      const headers = this.getBasicHeaders(additionalHeaders);

      return this.httpClient.post(`${this.getApiBasePath(orderContextId)}/comment`, params, { headers })
        .then((response) => {
          return response.status === 200;
        });
    }
}