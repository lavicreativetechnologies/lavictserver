/* istanbul ignore file */
import OfferService from '../service/offer.api.service';

Shopware.Application.addServiceProvider(OfferService.name, (container) => {
    const initContainer = Shopware.Application.getContainer('init');
    return new OfferService(initContainer.httpClient, container.loginService);
});