const { Component } = Shopware;
import template from './item-comment.html.twig';
import './item-comment.scss';

const NAME = 'b2b-offer-history-item-comment';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\AuditLog\\Framework\\AuditLogValueLineItemCommentEntity',
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});