const { Application, Entity, Component } = Shopware;

Application.addServiceProvider('logItemService', () => {
    return {
        registerLogItem,
        getLogItemByClass
    };
});

const logRegistryByClass = {};

function registerLogItem(logItemConfig) {
    if (!logItemConfig.name && !logItemConfig.logClass) {
        return false;
    }

    logRegistryByClass[logItemConfig.logClass] = logItemConfig.name;

    return true;
}


function getLogItemByClass(logClass) {
    return logRegistryByClass[logClass];
}
