Shopware.Mixin.register('b2b-date', {
    methods: {
        formatDateTime(obj) {
            if (!obj) {
                return '-';
            }

            const date = new Date(`${obj.date}`);
            const dateStr = this._formatToDateStr(date);
            const timeStr = this._formatToTimeStr(date);

            return `${dateStr}, ${timeStr}`;
        },
        formatDate(obj) {
            if (!obj) {
                return '-';
            }

            const date = new Date(`${obj.date}`);

            return this._formatToDateStr(date);
        },
        todayDate() {
            const date = new Date();
            return this._formatToDateStr(date);
        },
        _formatToDateStr(date) {
            const yyyy = date.getFullYear();
            const mm = ('0' + (date.getMonth() + 1)).slice(-2);
            const dd = ('0' + date.getDate()).slice(-2);

            return `${dd}/${mm}/${yyyy}`;
        },
        _formatToTimeStr(date) {
            const hh = ('0' + date.getHours()).slice(-2);
            const mm = ('0' + date.getMinutes()).slice(-2);

            return `${hh}:${mm}`;
        },
    }
});
