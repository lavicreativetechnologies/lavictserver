/* istanbul ignore file */
import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

export const ROUTE_MIDDLEWARE = (next, currentRoute) => {
    if (currentRoute.name === 'sw.customer.detail') {
        currentRoute.children.push({
            name: 'sw.customer.detail.salesRepresentativeClientSelection',
            path: '/sw/customer/detail/:id/sales-representative-client-selection',
            component: 'swag-b2b-sales-representative-client-selection',
            meta: {
                parentPath: 'sw.customer.index'
            }
        });
    }
    next(currentRoute);
};

Shopware.Module.register('b2b-salesrep-clients', {
    type: 'plugin',
    name: 'b2b-sales-representative-clients',
    title: 'b2b-salesrep.general.title',
    description: 'b2b-salesrep.general.description',
    color: '#b69880',
    icon: 'default-action-settings',
    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },
    routeMiddleware: ROUTE_MIDDLEWARE
});