/* istanbul ignore file */
import OfferLineItemReferenceService from '../service/offer-line-item-reference.api.service';

Shopware.Application.addServiceProvider(OfferLineItemReferenceService.name, (container) => {
    const initContainer = Shopware.Application.getContainer('init');
    return new OfferLineItemReferenceService(initContainer.httpClient, container.loginService);
});