import template from './b2b-offer-detail-discount.html.twig';
import './b2b-offer-detail-discount.scss';
import OfferApiService from '../../../../service/offer.api.service';

export const COMPONENT_NAME = 'b2b-offer-detail-discount';

const { Component, Mixin } = Shopware;

Component.register(COMPONENT_NAME, {
    template,
    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('b2b-date'),
        Mixin.getByName('b2b-currency'),
        Mixin.getByName('b2b-offer')
    ],
    inject: {
        offerService: OfferApiService.name
    },
    watch: {
        positions: {
            deep: true,
            immediate: true,
            handler() {
                this.updateSummary();
            }
        },
    },
    props: {
        editMode: {
            required: false,
            type: Boolean
        },
        showEditButton: {
            required: false,
            type: Boolean
        },
        offer: {
            required: true,
            type: Object
        },
        positions: {
            required: true,
            type: Array
        },
        isLoading: {
            required: false,
            type: Boolean
        }
    },
    data() {
        return {
            productSearch: null,
            comment: '',
            expirationDate: null,
            discountType: null,
            summary: {
                originalTotalAmountNet: 0,
                requestTotalAmountNet: 0,
                newTotalAmountNet: 0,
                originalTaxes: 0,
                requestTaxes: 0,
                newTaxes: 0,
                originalTotalWithTax: 0,
                requestTotalWithTax: 0,
                newTotalWithTax: 0,
                originalTotalDiscount: 0,
                requestTotalDiscount: 0,
                newTotalDiscount: 0
            }
        }
    },
    computed: {
        gridPositions() {
            let _positions = [];
            if (this.positions.length === 0) {
                return _positions;
            }

            this.positions.forEach((item) => {
                if (item.quantity > 0) {
                    _positions.push(item);
                }
            });

            _positions.push(this.discountPosition);

            return _positions;
        },
        inputDisabled() {
            return (!this.editMode || this.isLoading);
        },
        columns() {
            return [{
                property: 'name',
                label: 'b2b-offer.detail.discount.columnName',
            }, {
                property: 'quantity',
                label: 'b2b-offer.detail.discount.columnQuantity',
                inlineEdit: 'number',
                align: 'right'
            }, {
                property: 'amountNet',
                label: 'b2b-offer.detail.discount.columnPriceNet',
                align: 'right'
            }, {
                property: 'discountAmountNet',
                label: 'b2b-offer.detail.discount.columnDiscountPriceNet',
                align: 'right'
            }, {
                property: 'newOffer',
                label: 'b2b-offer.detail.discount.columnNewOffer',
                align: 'right'
            }];
        },
        discountPosition() {
            for (let i = 0; i < this.positions.length; i++) {
                const position = this.positions[i];

                if (position.id === this.DISCOUNT_ID) {
                    return position;
                }
            }
        },
        isAbsoluteDiscount() {
            return this.discountType === this.DISCOUNT_TYPES.ABSOLUTE;
        },
        isPercentageDiscount() {
            return this.discountType === this.DISCOUNT_TYPES.PERCENTAGE;
        },
        isEmptyDiscount() {
            return !this.isAbsoluteDiscount && !this.isPercentageDiscount;
        },
        contextWithInheritance() {
            return { ...Shopware.Context.api, inheritance: true };
        }
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            const { expiredAt } = this.offer;

            if (expiredAt) {
                this.expirationDate = expiredAt.date;
            }

            this.setInitialDiscountType();
        },
        async updateSummary() {
            const preview = await this.offerService.getOfferPreview(
                this.offer.id,
                this.getUpdatePayload(this.comment, this.expirationDate)
            );

            this.summary = {
                originalTotalAmountNet: this.offer.listAmountNet,
                requestTotalAmountNet: this.offer.discountAmountNet,
                newTotalAmountNet: preview.discountAmountNet,
                originalTaxes: this.offer.listAmount - this.offer.listAmountNet,
                requestTaxes: this.offer.discountAmount - this.offer.discountAmountNet,
                newTaxes: preview.discountAmount - preview.discountAmountNet,
                originalTotalWithTax: this.offer.listAmount,
                requestTotalWithTax: this.offer.discountAmount,
                newTotalWithTax: preview.discountAmount,
                originalTotalDiscount: 0,
                requestTotalDiscount: this.offer.listAmount - this.offer.discountAmount,
                newTotalDiscount: this.offer.listAmount - preview.discountAmount
            };
        },
        getDiscountInfo(item, type = '') {
            const { discountType } = this;

            const absolute = type === 'new' ? item.newDiscountAbsolute : item.discountAbsolute;
            const percentage = type === 'new' ? item.newDiscountPercentage : item.discountPercentage;

            const discount = discountType === this.DISCOUNT_TYPES.ABSOLUTE ? absolute : percentage;

            return discountType === this.DISCOUNT_TYPES.ABSOLUTE ? this.formatCurrency(discount) : `${discount} %`;
        },
        setDiscountType(type) {
            this.discountType = type;

            this.discountPosition.newDiscountPercentage = 0;
            this.discountPosition.newDiscountAbsolute = 0;
        },
        setInitialDiscountType() {
            const { percentageDiscount } = this.offer;

            if (percentageDiscount) {
                this.discountType = this.DISCOUNT_TYPES.PERCENTAGE;
            } else {
                this.discountType = this.DISCOUNT_TYPES.ABSOLUTE;
            }
        },
        removeItem(item) {
            item.quantity = 0;
        },
        hasItem(productNumber) {
            let _hasItem = false;

            this.positions.forEach(({ referenceNumber, quantity }) => {
                if (referenceNumber === productNumber && quantity > 0) {
                    _hasItem = true;
                }
            });

            return _hasItem;
        },
        async handleProductSearch(id, item) {
            if (this.hasItem(item.productNumber)) {
                this.createNotificationError({
                    variant: 'info',
                    title: this.$tc('b2b-offer.notifications.titleProductSearchError'),
                    message: this.$tc('b2b-offer.notifications.messageProductSearchError')
                });
                return;
            }

            const amount = item.price.length ? item.price[0].gross : 0;
            const amountNet = item.price.length ? item.price[0].net : 0;

            const newItem = {
                id,
                quantity: 1,
                referenceNumber: item.productNumber,
                discountAmount: amount,
                discountAmountNet: amountNet,
                amount: amount,
                amountNet: amountNet,
                name: item.name,
                created: true
            }

            this.positions.push(newItem);

            this.$emit('item-added');
        },
        handleAccept() {
            this.$emit('accept-offer');
        },
        handleDecline() {
            this.$emit('decline-offer');
        },
        handleEditMode() {
            this.$emit('set-edit-mode', true);
        },
        handleCancel() {
            this.$emit('set-edit-mode', false);
        },
        handleSendOffer() {
            this.$emit('update-offer', this.comment, this.expirationDate);
        },
        getDiscountSnippet() {
            if (this.discountType === this.DISCOUNT_TYPES.ABSOLUTE) {
                return this.$tc('b2b-offer.detail.discount.labelDiscountTypeAbsolute');
            } else {
                return this.$tc('b2b-offer.detail.discount.labelDiscountTypePercentage');
            }
        }
    }
});
