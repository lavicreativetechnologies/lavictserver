import template from './swag-b2b-sales-representative-client-selection.html.twig';
import './swag-b2b-sales-representative-client-selection.scss'
import SalesRepresentativeApiService from "../../../../service/sales-representative.api.service";

const { Component, Mixin } = Shopware;

Component.register('swag-b2b-sales-representative-client-selection', {
    template,

    inject: {
        salesRepService: SalesRepresentativeApiService.name
    },

    mixins: [
        Mixin.getByName('listing'),
        Mixin.getByName('notification'),
    ],

    props: {
        customerEditMode: {
            type: Boolean,
            required: true
        },
    },

    data() {
        return {
            isLoading: true,
            activeCustomer: this.customer,
            possibleClients: [],
            sortBy: 'assigned',
            sortDirection: 'DESC',
            total: 0,
            page: 1,
            limit: 25,
            disableRouteParams: true
        }
    },

    computed: {
        salesRepId() {
          return this.$route.params.id;
        },

        clientColumns() {
            return [
                {
                    property: 'assigned',
                    label: this.$tc('b2b-salesrep.customer.settings.salesRepresentativeClientSelection.assigned'),
                    align: 'center'
                }, {
                    property: 'email',
                    label: this.$tc('b2b-salesrep.customer.settings.salesRepresentativeClientSelection.email')
                }, {
                    property: 'firstName',
                    label: this.$tc('b2b-salesrep.customer.settings.salesRepresentativeClientSelection.firstName')
                }, {
                    property: 'lastName',
                    label: this.$tc('b2b-salesrep.customer.settings.salesRepresentativeClientSelection.lastName')
                }, {
                    property: 'type',
                    label: this.$tc('b2b-salesrep.customer.settings.salesRepresentativeClientSelection.type')
                }, {
                    property: 'shopOwnerEmail',
                    label: this.$tc('b2b-salesrep.customer.settings.salesRepresentativeClientSelection.owner')
                }
            ];
        }
    },

    methods: {
        onAssignedChange(assigned, clientAuthId) {
            const { salesRepId } = this;
            this.salesRepService.assignClient(salesRepId, { clientAuthId, assigned });
        },

        async getList() {
            this.isLoading = true;

            this.salesRepService.getClientList(
                this.salesRepId,
                {
                    page: this.page,
                    limit: this.limit,
                    term: this.term,
                    sort: [{
                        field: this.sortBy,
                        order: this.sortDirection,
                        naturalSorting: true
                    }]
                }
            ).then((response) => {
                this.possibleClients = response.data;
                this.total = response.meta.total;
            }).catch(() => {
                this.createNotificationError({
                    title: this.$tc('b2b.global.errors.unexpectedErrorTitle'),
                    message: this.$tc('b2b.global.errors.unexpectedErrorMessage')
                });
            }).finally(() => {
                this.isLoading = false;
            });
        }
    }
});