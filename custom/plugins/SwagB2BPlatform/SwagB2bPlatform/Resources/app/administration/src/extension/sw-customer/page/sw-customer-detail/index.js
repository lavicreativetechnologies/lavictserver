import template from './sw-customer-detail.html.twig';

Shopware.Component.override('sw-customer-detail', {
    template,

    inject: [
        'repositoryFactory'
    ],

    computed: {

        dataRepository() {
            return this.repositoryFactory.create('b2b_customer_data');
        },

        isSalesRepresentative() {
            if (!this.customer) {
                return false;
            }

            const { b2bCustomerData } = this.customer.extensions;

            if (b2bCustomerData) {
                return b2bCustomerData.isSalesRepresentative;
            }

            return false;
        },

        defaultCriteria() {
            const criteria = this.$super('defaultCriteria');
            criteria.addAssociation('b2bCustomerData');
            return criteria;
        },

        salesRepClients() {
            return {
                name: 'sw.customer.detail.salesRepresentativeClientSelection',
                params: { id: this.customerId },
                query: { edit: this.editMode }
            };
        },
    }
});