const { Component } = Shopware;
import template from './item-added.html.twig';
import './item-added.scss';

const NAME = 'b2b-offer-history-item-added';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\AuditLog\\Framework\\AuditLogValueLineItemAddEntity',
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});