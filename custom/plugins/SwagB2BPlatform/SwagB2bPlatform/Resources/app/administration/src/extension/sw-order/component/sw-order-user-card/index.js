import template from './sw-order-user-card.html.twig';

Shopware.Component.override('sw-order-user-card', {
    template,

    computed: {
        getOrderReferenceNumber() {
            return this.currentOrder.customFields.b2bOrderReferenceHolder || '-';
        },
        getRequestedDeliveryDate() {
            return this.currentOrder.customFields.b2bDeliveryDateHolder || '-';
        },
    },
});
