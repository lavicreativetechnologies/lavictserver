import Comment from './comment';
import itemAdded from './item-added';
import itemComment from './item-comment';
import itemPriceChange from './item-price-change';
import ItemQuantityChange from './item-quantity-change';
import ItemRemove from './item-remove';
import OfferDateAdded from './offer-date-added'
import OfferDiscount from './offer-discount';
import StatusChange from './status-change';

Shopware.Service('logItemService').registerLogItem(Comment);
Shopware.Service('logItemService').registerLogItem(itemAdded);
Shopware.Service('logItemService').registerLogItem(itemComment);
Shopware.Service('logItemService').registerLogItem(itemPriceChange);
Shopware.Service('logItemService').registerLogItem(ItemQuantityChange);
Shopware.Service('logItemService').registerLogItem(ItemRemove);
Shopware.Service('logItemService').registerLogItem(OfferDateAdded);
Shopware.Service('logItemService').registerLogItem(OfferDiscount);
Shopware.Service('logItemService').registerLogItem(StatusChange);
