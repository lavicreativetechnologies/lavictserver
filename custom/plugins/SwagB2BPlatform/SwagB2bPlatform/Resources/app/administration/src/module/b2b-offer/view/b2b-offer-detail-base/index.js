import template from './b2b-offer-detail-base.html.twig';
import './b2b-offer-detail-base.scss';

export const COMPONENT_NAME = 'b2b-offer-detail-base';
const { Component, Mixin } = Shopware;

Component.register(COMPONENT_NAME, {
    template,
    mixins: [
        Mixin.getByName('b2b-date'),
        Mixin.getByName('b2b-currency'),
        Mixin.getByName('b2b-offer')
    ],
    props: {
        offer: {
            required: true,
            type: Object
        },
        editMode: {
            required: false,
            type: Boolean
        },
        showEditButton: {
            required: false,
            type: Boolean
        },
        isLoading: {
            required: false,
            type: Boolean
        }
    },
    computed: {
        debtorLink() {
            return {
                name: 'sw.customer.detail',
                params: {
                    id: this.offer.debtor.id
                }
            };
        },
    },
    methods: {
        handleAccept() {
            this.$emit('accept-offer');
        },
        handleDecline() {
            this.$emit('decline-offer');
        },
        handleEditMode() {
            this.$emit('set-edit-mode', true);
        },
        handleCancel() {
            this.$emit('set-edit-mode', false);
        },
        getDiscountInfo() {
            const { discountValueNet, percentageDiscount } = this.offer;

            return percentageDiscount ? `${percentageDiscount} %` : this.formatCurrency(discountValueNet);
        }
    }
});
