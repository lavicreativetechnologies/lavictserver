const { Component } = Shopware;
import template from './comment.html.twig';
import './comment.scss';

const NAME = 'b2b-offer-history-comment';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\AuditLog\\Framework\\AuditLogValueOrderCommentEntity',
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});