/* istanbul ignore file */
import template from './b2b-offer-discount-grid.html.twig';
import './b2b-offer-discount-grid.scss';

const COMPONENT_NAME = 'b2b-offer-discount-grid';

Shopware.Component.extend(COMPONENT_NAME, 'sw-data-grid', {
    template
});