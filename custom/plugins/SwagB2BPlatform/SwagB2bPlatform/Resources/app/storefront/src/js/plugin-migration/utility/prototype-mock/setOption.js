export default function setOption(key, value) {
  const me = this;

  me.opts[key] = value;

  return me;
}
