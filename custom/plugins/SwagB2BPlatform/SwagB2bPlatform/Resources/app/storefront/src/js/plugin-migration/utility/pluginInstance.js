import mockPrototype from './mockPrototype';

export default class PluginInstance {
  constructor(pluginName, pluginObj) {
    this.name = pluginName;
    this.plugin = pluginObj;
    this.superclass = pluginObj;
    this.defaults = pluginObj.defaults || {};

    this.__mockPrototype();
    this.__init.bind(this);
  }

  __init(options = {}) {
    if (!this.init) {
      throw new Error('An init method is required!');
    }

    this.__refresh();
    this.__setOptions(options);

    if (this.$el.length === 0 && (this.selector && this.selector.length > 0)) {
      return;
    }

    this.init(options);
  }

  __refresh() {
    this.__mockPrototype();
  }

  __mockPrototype() {
    const { name, plugin } = this;
    const _mock = mockPrototype(name, plugin);

    Object.keys(_mock).forEach((attrName) => {
      this[attrName] = _mock[attrName];
    });
  }

  __setOptions(options) {
    this.opts = { ...this.defaults, ...options };
  }
}
