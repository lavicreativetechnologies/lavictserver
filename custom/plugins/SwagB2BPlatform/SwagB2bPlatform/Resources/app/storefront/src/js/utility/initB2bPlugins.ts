/* istanbul ignore file */

export default function(): Promise<void> {
    return new Promise((resolve): void => {
        const { initializePlugins, register } = window.PluginManager;
        const plugins = require('../plugins');
        const pluginNames = Object.keys(plugins);

        pluginNames.forEach((pluginName) => {
            const registryPluginName = `B2B_${pluginName}`;

            register(registryPluginName, plugins[pluginName]);
        });

        // todo: Use this in ajax-panel afterLoad event to make all ajaxPanel eventListeners obsolete and ..
        // todo: to allow registering each plugin once per target node
        initializePlugins();

        resolve();
    });
}
