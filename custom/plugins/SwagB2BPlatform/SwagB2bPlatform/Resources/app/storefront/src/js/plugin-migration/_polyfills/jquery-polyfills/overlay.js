const baseClass = 'modal--background';
const activeClass = 'modal--background-visible';

let _active = false;
let _closeOnClick = true;
let _onClose = null;

function close() {
  const overlay = document.querySelector(`.${baseClass}`);

  if (overlay) {
    overlay.classList.remove(activeClass);
  }

  if (_active) {
    _active = false;

    if (_onClose) {
      _onClose();
    }
  }
}

function createOverlay() {
  const overlay = document.createElement('div');
  document.body.appendChild(overlay);

  overlay.addEventListener('click', () => {
    if (_closeOnClick) {
      close();
    }
  });

  overlay.classList.add(baseClass);

  return overlay;
}

function open({ closeOnClick = true, onClose } = {}) {
  let overlay = document.querySelector(`.${baseClass}`);

  if (!overlay) {
    overlay = createOverlay();
  }

  setTimeout(() => {
    if (!_active) {
      _closeOnClick = closeOnClick;
      _onClose = onClose;

      _active = true;
      overlay.classList.add(activeClass);
    }
  });
}

export default {
  open,
  close,
};
