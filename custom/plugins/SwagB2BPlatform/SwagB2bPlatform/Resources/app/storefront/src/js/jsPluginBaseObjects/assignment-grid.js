/**
 * Handles the assignment grid view extensions
 *
 * * Submit the form through ajax
 * * Disable Grant buttons if access is disabled
 *
 * global: document
 */
export default {
  defaults: {
    gridFormSelector: 'form.b2b--assignment-form',

    allowInputSelector: '.assign--allow',

    denyInputSelector: '.assign--grantable',

    rowParentSelector: '.panel--tr',

    actionSearchSelector: '.action--search',

    errorClass: 'grid--errors',
  },

  init() {
    const me = this;
    const $searchInput = me.$el.find(me.defaults.actionSearchSelector).find('input[type=text]');

    this.applyDataAttributes();

    me.setSearchInputFocus($searchInput);

    me._on(document, 'tree_toggle_menu', $.proxy(me.renderGrid, me));

    me.renderGrid(me);
  },

  setSearchInputFocus($searchInput) {
    if (!$searchInput.length) {
      return;
    }

    $searchInput.focus();

    if (!$searchInput.length) {
      return;
    }

    const searchTerm = $searchInput.val();
    if (!searchTerm.length) {
      return;
    }

    $searchInput[0].setSelectionRange(searchTerm.length, searchTerm.length);
  },

  parseJSON(input) {
    try {
      return JSON.parse(input);
    } catch (e) {
      return null;
    }
  },

  renderGrid() {
    const me = this;
    let inAction = false;
    let inRevert = false;
    const createPreventedChangeEvent = function () {
      const $event = $.Event('change');
      $event.preventDefault();

      return $event;
    };

    const $forms = me.$el.find(me.defaults.gridFormSelector);

    $forms.filter('.b2b--assignment-row-form').each(function () {
      const $form = $(this);
      const $allowCheckbox = $form.find(me.defaults.allowInputSelector);
      const $grantableCheckbox = $form.find(me.defaults.denyInputSelector);
      const $allAllowCheckboxes = $form.closest(me.defaults.rowParentSelector).find(`${me.defaults.allowInputSelector}:gt(0):not(:disabled)`);
      const $allGrantableCheckboxes = $form.closest(me.defaults.rowParentSelector).find(`${me.defaults.denyInputSelector}:gt(0):not(:disabled)`);
      const allAllowChecked = function () {
        return ($allAllowCheckboxes.length === $allAllowCheckboxes.filter(':checked').length);
      };
      const allGrantableChecked = function () {
        return ($allGrantableCheckboxes.length === $allGrantableCheckboxes.filter(':checked').length);
      };

      $allowCheckbox.prop('checked', allAllowChecked());

      me._on($allAllowCheckboxes, 'change', () => {
        if (inAction) {
          return;
        }
        inAction = true;
        $allowCheckbox
          .prop('checked', allAllowChecked())
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });

      $grantableCheckbox.prop('checked', allGrantableChecked());

      me._on($allGrantableCheckboxes, 'change', () => {
        if (inAction) {
          return;
        }
        inAction = true;
        $grantableCheckbox
          .prop('checked', allGrantableChecked())
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });

      me._on($allowCheckbox, 'change', function (event) {
        if (inAction) {
          return;
        }

        inAction = true;
        $allAllowCheckboxes
          .prop('checked', $(this).is(':checked'))
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });

      me._on($grantableCheckbox, 'change', function (event) {
        if (inAction) {
          return;
        }

        inAction = true;
        $allGrantableCheckboxes
          .prop('checked', $(this).is(':checked'))
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });
    });

    $forms.each(function () {
      const $form = $(this);
      const allowCheckbox = $form.find(me.defaults.allowInputSelector);
      const grantableCheckbox = $form.find(me.defaults.denyInputSelector);

      if (!allowCheckbox.is(':checked')) {
        grantableCheckbox.prop('disabled', true);
      }

      me._on(allowCheckbox, 'change', () => {
        if (inRevert) {
          return;
        }

        grantableCheckbox.data('previous-state', grantableCheckbox.prop('checked'));

        if (allowCheckbox.is(':checked')) {
          grantableCheckbox.prop('disabled', false);
        } else {
          grantableCheckbox.attr('checked', false);
          grantableCheckbox.prop('disabled', true);
        }
      });
    });

    this._off($forms, 'submit');
    this._on($forms, 'submit', function (event) {
      event.preventDefault();

      const $form = $(this);
      const $checkbox = $(document.activeElement);
      const $targetId = $form.data('target');
      const $errorTarget = $(`[data-id="${$targetId}"]`);
      $checkbox.closest('span.checkbox').addClass('is--loading');
      $errorTarget.html('');

      $.ajax({
        url: $form.attr('action'),
        method: $form.attr('method'),
        data: $form.serialize(),
        success(response) {
          if ($errorTarget && response.errors) {
            $.each(response.errors, (index, value) => {
              $errorTarget.append($('<div>', {
                class: me.defaults.errorClass,
                html: value,
              }));
            });

            const $allowCheckbox = $form.find(me.defaults.allowInputSelector);
            const $grantableCheckbox = $form.find(me.defaults.denyInputSelector);
            const $previousState = $grantableCheckbox.data('previous-state');

            inAction = true;
            inRevert = true;
            $grantableCheckbox
              .prop('checked', $previousState)
              .prop('disabled', false)
              .trigger(createPreventedChangeEvent());
            $allowCheckbox
              .prop('checked', true)
              .trigger(createPreventedChangeEvent());
            inAction = false;
            inRevert = false;

            return;
          }

          const jsonResponse = me.parseJSON(response);

          if (jsonResponse && jsonResponse.routes) {
            $.each(jsonResponse.routes, (index, value) => {
              const $input = $(`#${value}`);

              inAction = true;

              $input
                .prop('checked', true)
                .trigger(createPreventedChangeEvent());

              inAction = false;
            });
          }
        },
        complete() {
          $checkbox.closest('span.checkbox').removeClass('is--loading');
          $form.find(me.defaults.denyInputSelector).removeAttr('previous-state');
        },
      });
    });
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
