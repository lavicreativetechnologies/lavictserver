const durationStrings = {
  default: 400,
  fast: 200,
  slow: 600,
};

const _fade = async (target, direction, duration) => {
  let curOpacity = parseFloat(target.style.opacity || (duration === 'in' ? 0 : 1));
  let proceed = false;

  curOpacity += (direction === 'in') ? 0.01 : -0.01;

  if (direction === 'in' && curOpacity < 1) {
    proceed = true;
  } else if (direction === 'out' && curOpacity > 0) {
    proceed = true;
  }

  if (proceed) {
    target.style.opacity = curOpacity; // eslint-disable-line

    await new Promise((resolve) => {
      setTimeout(resolve, duration);
    });

    requestAnimationFrame(() => {
      _fade.bind(this)(target, direction, duration);
    });
  }

  return this;
};

const getDuration = (duration) => {
  let _duration = 400;

  if (duration) {
    if (typeof duration === 'number') {
      _duration = duration;
    } else if (typeof duration === 'string' && durationStrings[duration]) {
      _duration = durationStrings[duration];
    }
  }

  return (_duration / 100);
};

function fade(duration, direction) {
  const target = $(this);
  const _duration = getDuration(duration);

  const _target = target.length > 0 ? target[0] : false;
  if (_target) {
    _fade.bind(this)(_target, direction, _duration);
  }
}

function fadeIn(duration = 400) {
  fade.bind(this)(duration, 'in');
}

function fadeOut(duration = 400) {
  fade.bind(this)(duration, 'out');
}

export {
  fadeIn,
  fadeOut,
};
