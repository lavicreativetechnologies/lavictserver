import getPluginWrapper from '../getPluginWrapper';

export default function () {
  const pluginWrapper = getPluginWrapper();
  const { _name } = this;

  if (!pluginWrapper.initialized[_name] && this.init) {
    this.init();
  } else if (!!pluginWrapper.initialized[_name] && this.update) {
    this.update();
  }

  return this;
}
