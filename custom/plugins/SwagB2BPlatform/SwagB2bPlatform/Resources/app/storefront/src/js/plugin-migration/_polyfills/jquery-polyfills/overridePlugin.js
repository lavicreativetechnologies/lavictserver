import { getTargetPluginName, getPluginWrapper } from '../../utility';

const applyOverrides = (name, overrides) => {
  const pluginWrapper = getPluginWrapper();
  const { plugin } = pluginWrapper.pluginObjects[name];

  pluginWrapper.pluginObjects[name].plugin = { ...plugin, ...overrides };
  pluginWrapper.pluginObjects[name].__init();
};

export default function (pluginName, overrideObject) {
  const _pluginName = getTargetPluginName(pluginName);

  if (_pluginName) {
    applyOverrides(_pluginName, overrideObject);
  }

  return this;
}
