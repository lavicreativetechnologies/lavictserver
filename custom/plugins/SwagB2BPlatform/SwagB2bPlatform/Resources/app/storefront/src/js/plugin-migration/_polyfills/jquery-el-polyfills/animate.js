/**
 * Polyfill for the jQuery.animate method.
 * The cssObj and duration parameters are used
 * to determine and run appropriate css animations via classes.
 *
 * @param cssObj
 * @param duration
 * @param animation
 * @param callback
 */

const supportedAnimations = {
  example: {
    in: 'fadeExample--in',
    out: 'fadeExample--out',
  },
  opacity: {
    in: 'fadeOpacity--in',
    out: 'fadeOpacity--out',
  },
};

const durationStrings = {
  default: 400,
  fast: 200,
  slow: 600,
};

const removeAnimationClasses = (list, target) => {
  list.forEach((animation) => {
    target.classList.remove(animation);
  });
};

const getAnimationDirection = (property, startValue, endValue) => {
  switch (property) {
    case 'opacity':
      return startValue < endValue ? 'in' : 'out';
    default:
      return '';
  }
};

const isSupportedProperty = (propertyName) => !!supportedAnimations[propertyName];

const getAnimationClassList = (cssObj, target) => {
  const classes = [];

  Object.keys(cssObj).forEach((property) => {
    if (isSupportedProperty(property)) {
      const _startValue = target.style[property] || 0;
      const _endValue = cssObj[property];
      const direction = getAnimationDirection(property, _startValue, _endValue);

      const animationClass = supportedAnimations[property][direction];
      if (animationClass) {
        classes.push(animationClass);
      }
    }
  });

  return classes;
};

const getDuration = (duration) => {
  let _duration = 400;

  if (duration) {
    if (typeof duration === 'number') {
      _duration = duration;
    } else if (typeof duration === 'string' && durationStrings[duration]) {
      _duration = durationStrings[duration];
    }
  }

  return `${_duration}ms`;
};

const applyAnimationClasses = (cssObj, target, duration) => {
  const _duration = getDuration(duration);
  const animationClassList = getAnimationClassList(cssObj, target);

  target.style.animationDuration = _duration; // eslint-disable-line
  target.style.animationDirection = 'forwards'; // eslint-disable-line

  animationClassList.forEach((animation) => {
    target.classList.add(animation);
  });

  return animationClassList;
};

export default async function (cssObj, duration, animationFn, callback) {
  const target = this.length > 0 ? this[0] : false;
  const oldAnimationDuration = target.style.animationDuration || 0;

  if (target) {
    const classList = applyAnimationClasses(cssObj, target, duration);

    await new Promise((resolve) => {
      setTimeout(resolve, duration);
    });

    target.style.animationDuration = oldAnimationDuration;
    removeAnimationClasses(classList, target);
  }

  if (callback) {
    callback.bind(this)();
  }

  return this;
}
