import { b2bTree } from '../../jsPluginBaseObjects';

export default ({
  ...b2bTree,
  name: 'b2bTree',
  initOnLoad: false,
});
