import { b2bPreloadAnchor } from '../../jsPluginBaseObjects';

export default ({
  ...b2bPreloadAnchor,
  name: 'b2bPreloadAnchor',
  initOnLoad: true,
  selector: '*[data-preloader-anchor="true"]',
});
