export default {
  defaults: {
    passwordActivationSelector: '.b2b--password-activation',
  },

  init() {
    const me = this;
    me._on(me.$el, 'change', me.defaults.passwordActivationSelector, $.proxy(me.onCheckboxChange, me));
  },

  onCheckboxChange(event) {
    const checkbox = event.currentTarget;
    const inputPassword = $('.b2b--input-password');
    const formPassword = $('.b2b--password');

    if ($(checkbox).is(':checked')) {
      formPassword.addClass('is--hidden');
    } else {
      inputPassword.val('');
      formPassword.removeClass('is--hidden');
    }
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
