import getPluginWrapper from './getPluginWrapper';
import getTargetPluginName from './getTargetPluginName';
import PluginInstance from './pluginInstance';

let pluginWrapper = null;

function _runPlugin(name, options) {
  pluginWrapper.pluginObjects[name].__setOptions(options);
  pluginWrapper.pluginObjects[name].__init(options);
}


export default function (name, plugin, options = {}) {
  if (!name) {
    throw new Error('Parameter "name" is required!');
  }

  pluginWrapper = getPluginWrapper();
  const { pluginObjects, initialized } = pluginWrapper;
  const targetName = getTargetPluginName(name);

  if (!targetName) {
    return false;
  }

  if (!pluginObjects[targetName] && !!plugin) {
    pluginObjects[targetName] = plugin;
  }

  if (!pluginObjects[targetName]) {
    return false;
  }

  const pluginObj = pluginObjects[targetName];

  if (pluginObj.constructor.name !== PluginInstance.name) {
    throw new Error('The object is not a valid PluginInstance!');
  }


  const mergedOptions = { ...pluginObj.defaults, ...options };

  if (initialized[targetName]) {
    pluginObj._destroy();
    pluginObj._overrideEl(pluginObj.selector);
  }

  _runPlugin(targetName, mergedOptions);

  pluginWrapper.initialized[targetName] = true;

  return true;
}
