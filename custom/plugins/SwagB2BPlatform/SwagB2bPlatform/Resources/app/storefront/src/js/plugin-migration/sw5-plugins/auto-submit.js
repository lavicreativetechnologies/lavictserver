import { b2bAutoSubmit } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAutoSubmit,
  name: 'b2bAutoSubmit',
  initOnLoad: true,
  selector: 'body',
});
