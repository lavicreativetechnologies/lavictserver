import { b2bAjaxPanelDefaultAddress } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelDefaultAddress,
  name: 'b2bAjaxPanelDefaultAddress',
  initOnLoad: false,
});
