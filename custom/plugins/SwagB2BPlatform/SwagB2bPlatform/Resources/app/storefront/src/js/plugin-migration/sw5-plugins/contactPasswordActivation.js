import { b2bContactPasswordActivation } from '../../jsPluginBaseObjects';

export default ({
  ...b2bContactPasswordActivation,
  name: 'b2bContactPasswordActivation',
  initOnLoad: false,
});
