import { b2bAjaxPanelLoading } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelLoading,
  name: 'b2bAjaxPanelLoading',
  initOnLoad: true,
  selector: 'body',
});
