/**
 * Disable anchor elements with a preloading indicator.
 *
 * Usage:
 * <a data-preloader-anchor="true">Anchor</>
 */
export default {
  defaults: {
    loaderCls: 'js--loading',

    disabledCls: 'is--disabled',
  },

  init() {
    const me = this;

    me.applyDataAttributes();

    me._on(me.$el, 'click', $.proxy(me.onAnchorClick, me));
  },

  onAnchorClick(event) {
    const me = this;

    if (me.$el.hasClass(me.opts.disabledCls)) {
      event.preventDefault();

      return;
    }

    me.$el.html(`${me.$el.text()}<div class="${me.opts.loaderCls}"></div>`).addClass(me.opts.disabledCls);
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
