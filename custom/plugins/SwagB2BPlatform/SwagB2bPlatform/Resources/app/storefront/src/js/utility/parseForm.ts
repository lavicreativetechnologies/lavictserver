function parseBody(form: HTMLFormElement): FormData {
    return new FormData(form);
}

function parseQueryString(form: HTMLFormElement): string {
    // @ts-ignore | by definition, URLSearchParams doesn't accept FormData as a parameter, while still being able to handle it
    return `?${new URLSearchParams(parseBody(form)).toString()}`;
}

export default function parseForm(form: HTMLFormElement, queryString: Boolean = false): FormData | string {
    return queryString ? parseQueryString(form) : parseBody(form);
}
