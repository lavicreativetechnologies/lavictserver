/**
 * Polyfill by
 * https://developer.mozilla.org/en-US/docs/Web/API/Element/toggleAttribute
 */

/* istanbul ignore else */
if (!Element.prototype.toggleAttribute) {
    Element.prototype.toggleAttribute = function(name: string): boolean {
        if (this.hasAttribute(name)) {
            this.removeAttribute(name);

            return false;
        }

        this.setAttribute(name, '');

        return true;
    };
}

export {};
