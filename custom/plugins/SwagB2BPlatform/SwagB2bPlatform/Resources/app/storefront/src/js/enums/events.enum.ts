enum B2BEvents {
    PANEL_BEFORE_LOAD = 'b2b--ajax-panel_loading',
    PANEL_AFTER_LOAD = 'b2b--ajax-panel_loaded',
    PANEL_REFRESH = 'b2b--ajax-panel_refresh',
    PANEL_RELOAD = 'b2b--ajax-panel_reload',
    UPLOAD_SUCCESS = 'b2b--upload_success',
    UPLOAD_FAILURE = 'b2b--upload_failure'
}

export default B2BEvents;
