/**
 * This plugin shows the off-canvas cart and refresh the Cart Widget in the header
 */
export default {
  defaults: {
    orderListRemoteAddButtonSelector: '.orderlist--add',

    orderListGridAddToCartSelector: '.order-list--add-to-cart',

    orderListDetailQuantitySelector: '#sQuantity',

    alertHideDelay: 2000,

    snippets: {
      createPlaceholder: null,
      createError: null,
    },

    inputContainerCls: 'b2b-orderlist-input-container',

    notificationContainerCls: 'b2b-orderlist-notification-container',

    orderlistDropdownSelector: '.b2b--orderlist-dropdown',

    orderListValidationSelector: '.b2b--order-list-validation',

    keyMap: {
      esc: 27,
    },

    inProgress: false,
  },

  init() {
    const me = this;
    const $orderlistDropdown = me.$el.find(me.opts.orderlistDropdownSelector);

    me.applyDataAttributes();

    me._on(me.$el.find(me.defaults.orderListValidationSelector), 'click', $.proxy(me.validateOrderList, me));

    if (!$orderlistDropdown.length) {
      return;
    }

    me.opts.snippets.createPlaceholder = $orderlistDropdown.data('new-placeholder');
    me.opts.snippets.createError = $orderlistDropdown.data('new-error');

    me._on(me.opts.orderlistDropdownSelector, 'change', $.proxy(me.changeOrderList, me));

    me._on(me.opts.orderListDetailQuantitySelector, 'change', $.proxy(me.handleDetailQuantity));

    me.$el.on(me.getEventName('click'), '.orderlist-create-abort', $.proxy(me.onOrderListCreateAbort, me));

    me.$el.on(me.getEventName('click'), '.orderlist-create-save', $.proxy(me.onOrderListCreateSubmit, me));

    me.$el.on(me.getEventName('keyup'), '.form--orderlist-add input', $.proxy(me.onOrderListCreateKeyUp, me));

    me.$el.on(me.getEventName('focus'), '.form--orderlist-add input', $.proxy(me.onOrderListCreateFocusIn, me));

    me.$el.on(me.getEventName('focusout'), '.form--orderlist-add input', $.proxy(me.onOrderListCreateFocusOut, me));

    me.removeAlertsDelayed();
  },

  validateOrderList() {
    const me = this;
    const $target = $(event.currentTarget);
    const $form = $target.closest('form');

    $.ajax({
      url: $target.data('check-url'),
      type: 'post',
      data: $form.serialize(),
      success(response) {
        if (response) {
          $.b2bConfirmModal.open(response, {
            confirm() {
              $.ajax({
                url: $target.data('action-url'),
                type: 'post',
                data: $form.serialize(),
                success() {
                  me.handleOrderListCreateOrder($form);

                  $.b2bConfirmModal.close();
                },
              });
            },
            cancel() {
              $.b2bConfirmModal.close();
            },
          });
        } else {
          me.handleOrderListCreateOrder($form);
        }
      },
      error() {
        $.ajax({
          url: $target.data('action-url'),
          type: 'post',
          data: $form.serialize(),
          success() {
            $.b2bConfirmModal.close();
          },
        });
      },
    });
  },

  removeAlertsDelayed() {
    const me = this;
    const $alerts = me.$el.find('.alert');
    const $select = me.$el.find(me.opts.orderlistDropdownSelector);
    const errorExists = me.$el.find('.is--error').length;

    if (errorExists) {
      $select.val($select.find('option:first').val());

      return;
    }

    if ($alerts.length) {
      $select.attr('disabled', 'disabled');

      setTimeout(() => {
        $select.val($select.find('option:first').val());
        $alerts.slideUp('fast');
        $select.removeAttr('disabled');
      }, me.opts.alertHideDelay);
    }
  },

  changeOrderList(event) {
    const me = this;
    const $select = $(event.currentTarget);

    if ($select.val() == '_new_') {
      me.enableOrderListCreate($select);
    } else {
      $select.closest('form').submit();
    }
  },

  onOrderListCreateAbort(event) {
    const me = this;
    const $select = me.$el.find(me.opts.orderlistDropdownSelector);

    event.preventDefault();

    me.disableOrderListCreate($select);
  },

  onOrderListCreateSubmit(event) {
    const me = this;
    const $select = me.$el.find(me.opts.orderlistDropdownSelector);
    const $submitButton = $(event.currentTarget);
    const $form = $submitButton.closest('.form--orderlist-add');
    const $parentForm = $select.closest('form');
    let orderList = null;

    event.preventDefault();

    if (me.opts.inProgress) {
      return;
    }

    me.opts.inProgress = true;

    $.ajax({
      url: $form.attr('action'),
      method: $form.attr('method'),
      data: $form.serialize(),
      success(jsonResponse) {
        orderList = JSON.parse(jsonResponse);

        if (!orderList.orderListId) {
          me.opts.inProgress = false;
          me.showOrderListCreateError();

          return;
        }

        $select.remove();
        $parentForm.append($('<input>', {
          type: 'hidden',
          name: 'orderlist',
          value: orderList.orderListId,
        }));

        $parentForm.submit();
      },
      error() {
        me.opts.inProgress = false;
        me.showOrderListCreateError();
      },
    });
  },

  onOrderListCreateKeyUp(event) {
    const me = this;
    const $select = me.$el.find(me.opts.orderlistDropdownSelector);

    me.hideOrderListCreateError();

    if (event.keyCode !== me.defaults.keyMap.esc) {
      return;
    }

    me.disableOrderListCreate($select);
  },

  onOrderListCreateFocusIn(event) {
    const me = this;
    const $abortButton = me.$el.find('.orderlist-create-abort');

    $abortButton.addClass('is--active');
  },

  onOrderListCreateFocusOut(event) {
    const me = this;
    const $abortButton = me.$el.find('.orderlist-create-abort');

    $abortButton.removeClass('is--active');
  },

  enableOrderListCreate($select) {
    const me = this;

    $select.closest('.select-field').hide();
    $select.closest('.select-field').after($('<div>', {
      class: me.opts.inputContainerCls,
      html: $('<form>', {
        action: $select.attr('data-action-create'),
        method: 'post',
        class: 'form--inline form--orderlist-add',
        html: [
          $('<input>', {
            type: 'text',
            name: 'name',
            maxlength: 50,
            required: true,
            placeholder: me.opts.snippets.createPlaceholder,
          }),
          $('<button>', {
            class: 'btn is--default orderlist-create-abort',
            type: 'button',
            html: $('<i>', {
              class: 'icon--cross',
            }),
          }),
          $('<button>', {
            class: 'btn is--primary orderlist-create-save',
            type: 'submit',
            html: $('<i>', {
              class: 'icon--arrow-right',
            }),
          }),
        ],
      }),
    }));

    $('.form--orderlist-add').find('input').focus();

    CSRF.updateForms();
  },

  disableOrderListCreate($select) {
    const me = this;

    $select.val($select.find('option:first').val());
    $select.closest('.select-field').show();
    $select.closest('.group--actions').find(`.${me.opts.inputContainerCls}`).remove();
  },

  showOrderListCreateError() {
    const me = this;

    me.$el.find('.form--orderlist-add input').addClass('is--error');

    me.$el.find('.b2b-orderlist-input-container').append($('<div>', {
      class: 'orderlist-create-error',
      html: $('<i>', {
        class: 'icon--warning',
        title: me.opts.snippets.createError,
      }),
    }));
  },

  hideOrderListCreateError() {
    const me = this;
    const $createInput = me.$el.find('.form--orderlist-add input');

    if (!$createInput.hasClass('is--error')) {
      return;
    }

    $createInput.removeClass('is-error');
    me.$el.find('.orderlist-create-error').remove();
  },

  handleDetailQuantity() {
    const $select = $(this);
    const $remoteBox = $('[data-id="order-list-remote-box"]');

    if ($remoteBox.length) {
      $remoteBox.find('[name="products[0][quantity]"]').val($select.val());
    }
  },

  handleOrderListCreateOrder($form) {
    event.preventDefault();

    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize(),
      success() {
        const $collapseCart = $('*[data-collapse-cart="true"]');

        if (!$collapseCart) {
          return;
        }

        $collapseCart.data('plugin_swCollapseCart').onMouseEnter(event);
        $.publish('plugin/swAddArticle/onAddArticle');
      },
    });
  },

  destroy() {
    const me = this;

    me.$el.off(me.getEventName('submit'), me.$el.find(me.opts.orderListGridAddToCartSelector));
    me.$el.off(me.getEventName('change'), me.opts.orderlistDropdownSelector);
    me.$el.off(me.getEventName('change'), me.opts.orderListDetailQuantitySelector);
    me.$el.off(me.getEventName('click'), '.orderlist-create-abort');
    me.$el.off(me.getEventName('click'), '.orderlist-create-save');
    me.$el.off(me.getEventName('keyup'), '.form--orderlist-add input');
    me.$el.off(me.getEventName('focus'), '.form--orderlist-add input');
    me.$el.off(me.getEventName('focusout'), '.form--orderlist-add input');

    me._destroy();
  },
};
