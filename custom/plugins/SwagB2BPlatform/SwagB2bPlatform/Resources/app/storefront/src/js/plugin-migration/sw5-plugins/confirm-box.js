import { b2bConfirmBox } from '../../jsPluginBaseObjects';

function hasOpenModal () {
  return !!document.querySelector('.js--modal-animated');
}

const overrides = {
  init (content, options) {
    this.open(content, options);
  },

  open (content, handler) {
    const me = this;
    const opts = me.defaults;

    if (hasOpenModal()) {
      const divModalBackground = document.querySelector('.modal--background-visible');
      const divSecondBackgroundModal = document.querySelector('.js--modal.sizing--fixed');
      const divModalBackgroundClone = divModalBackground.cloneNode(false);

      document.querySelector('.b2b--modal').appendChild(divModalBackgroundClone);
      divModalBackgroundClone.classList.add('modal--background', 'modal--background-visible', 'b2b-confirm-modal--background');
      divSecondBackgroundModal.addEventListener('click', () => {
        me.close();
      });
    }

    me._$handler = handler;

    let $modalBox = me._$modalBox;
    if (!$modalBox) {
      me._$previousOverlay = $.overlay.overlay;

      $.overlay.open($.extend({}, {
        closeOnClick: opts.closeOnOverlay,
        onClose: $.proxy(me.onOverlayClose, me),
        overlayCls: 'js--overlay b2bConfirmOverlay',
      }));

      me.initModalBox();
      me.registerEvents();

      $modalBox = me._$modalBox;
    }

    $modalBox.toggleClass('sizing--auto sizing--fixed', false)
      .toggleClass('sizing--content no--header', true)
      .css({
        width: 600,
        height: 'auto',
        display: 'block',
      });

    me._$content.html(content);

    $modalBox.find(me.defaults.confirmModalSelector).click($.proxy(me.onButtonConfirm, me));
    $modalBox.find(me.defaults.cancelModalSelector).click($.proxy(me.onButtonCancel, me));

    me.center();
    window.setTimeout(me.center.bind(me), 25);

    me.setTransition({ opacity: 1 }, 500, 'linear');

    $('html').addClass('no--scroll');

    return me;
  },

  close () {
    const me = this;
    const $modalBox = me._$modalBox;
    const isSecondModal = hasOpenModal();

    const modalBackground = document.querySelector('.b2b-confirm-modal--background');
    if (modalBackground) {
      modalBackground.remove();
    }

    if (!isSecondModal) {
      $.overlay.close();
      document.body.parentElement.classList.remove('no--scroll');
    }

    if ($modalBox !== null) {
      me.setTransition({
        opacity: 0,
      }, 500, 'linear', () => {
        $modalBox.remove();
        me._$content = null;
        me._$modalBox = null;
        me._$handler = null;
      });
    }

    $.overlay.overlay = me._$previousOverlay;
    me._$previousOverlay = null;
  },
};

export default ({
  ...b2bConfirmBox,
  ...overrides,
  name: 'b2bConfirmBox',
  alias: 'b2bConfirmModal',
  initOnLoad: false,
});
