import { b2bCollapsePanel } from '../../jsPluginBaseObjects';

export default ({
  ...b2bCollapsePanel,
  name: 'b2bCollapsePanel',
  alias: 'swCollapsePanel',
  initOnLoad: false,
});
