/* istanbul ignore file */
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import 'whatwg-fetch';
import { initB2BPlugins, waitForStorefront } from './utility';
import PluginWrapper from './plugin-migration/plugin-wrapper';

(async function initializeB2b(): Promise<void> {
    await waitForStorefront();
    await initB2BPlugins();
    await new PluginWrapper();
}());
