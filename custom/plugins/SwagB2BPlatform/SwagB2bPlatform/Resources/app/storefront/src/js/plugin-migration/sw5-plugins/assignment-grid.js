import { b2bAssignmentGrid } from '../../jsPluginBaseObjects';

const overrides = {
  renderGrid () {
    const me = this;
    let inAction = false;
    let inRevert = false;
    const createPreventedChangeEvent = function () { // eslint-disable-line
      const $event = $.Event('change');
      $event.preventDefault();

      return $event;
    };

    const $forms = me.$el.find(me.defaults.gridFormSelector);

    $forms.filter('.b2b--assignment-row-form').each(function () { // eslint-disable-line
      const $form = $(this);
      const $allowCheckbox = $form.find(me.defaults.allowInputSelector);
      const $grantableCheckbox = $form.find(me.defaults.denyInputSelector);
      const $allAllowCheckboxes = $form.closest(me.defaults.rowParentSelector).find(`${me.defaults.allowInputSelector}:gt(0):not(:disabled)`);
      const $allGrantableCheckboxes = $form.closest(me.defaults.rowParentSelector).find(`${me.defaults.denyInputSelector}:gt(0):not(:disabled)`);
      const allAllowChecked = function () { // eslint-disable-line
        return ($allAllowCheckboxes.length === $allAllowCheckboxes.filter(':checked').length);
      };
      const allGrantableChecked = function () { // eslint-disable-line
        return ($allGrantableCheckboxes.length === $allGrantableCheckboxes.filter(':checked').length);
      };

      $allowCheckbox.prop('checked', allAllowChecked());

      me._on($allAllowCheckboxes, 'change', () => {
        if (inAction) {
          return;
        }
        inAction = true;
        $allowCheckbox
          .prop('checked', allAllowChecked())
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });

      $grantableCheckbox.prop('checked', allGrantableChecked());

      me._on($allGrantableCheckboxes, 'change', () => {
        if (inAction) {
          return;
        }
        inAction = true;
        $grantableCheckbox
          .prop('checked', allGrantableChecked())
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });

      me._on($allowCheckbox, 'change', function () { // eslint-disable-line
        if (inAction) {
          return;
        }

        inAction = true;
        $allAllowCheckboxes
          .prop('checked', $(this).is(':checked'))
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });

      me._on($grantableCheckbox, 'change', function () { // eslint-disable-line
        if (inAction) {
          return;
        }

        inAction = true;
        $allGrantableCheckboxes
          .prop('checked', $(this).is(':checked'))
          .trigger(createPreventedChangeEvent());
        inAction = false;
      });
    });

    $forms.each(function () { // eslint-disable-line
      const $form = $(this);
      const allowCheckbox = $form.find(me.defaults.allowInputSelector);
      const grantableCheckbox = $form.find(me.defaults.denyInputSelector);

      if (!allowCheckbox.is(':checked')) {
        grantableCheckbox.prop('disabled', true);
      }

      me._on(allowCheckbox, 'change', () => {
        if (inRevert) {
          return;
        }

        grantableCheckbox.data('previous-state', grantableCheckbox.prop('checked'));

        if (allowCheckbox.is(':checked')) {
          grantableCheckbox.prop('disabled', false);
        } else {
          grantableCheckbox.attr('checked', false);
          grantableCheckbox.prop('disabled', true);
        }
      });
    });

    this._off($forms, 'submit');
    this._on($forms, 'submit', function (event) { // eslint-disable-line
      event.preventDefault();

      const $form = $(this);
      const $checkbox = $(document.activeElement);
      const $targetId = $form.data('target');
      const $errorTarget = $(`[data-id="${$targetId}"]`);
      $checkbox.closest('span.checkbox').addClass('is--loading');
      $errorTarget.html('');

      $.ajax({
        url: $form.attr('action'),
        method: $form.attr('method'),
        data: $form.serialize(),
        contentType: 'application/x-www-form-urlencoded',
        success (response) {
          const jsonResponse = me.parseJSON(response);
          if (!jsonResponse) {
            return;
          }

          if ($errorTarget && jsonResponse.errors) {
            $.each(jsonResponse.errors, (index, value) => {
              $errorTarget.append($('<div>', {
                class: me.defaults.errorClass,
                html: value,
              }));
            });

            const $allowCheckbox = $form.find(me.defaults.allowInputSelector);
            const $grantableCheckbox = $form.find(me.defaults.denyInputSelector);
            const $previousState = $grantableCheckbox.data('previous-state');

            inAction = true;
            inRevert = true;
            $grantableCheckbox
              .prop('checked', $previousState)
              .prop('disabled', false)
              .trigger(createPreventedChangeEvent());
            $allowCheckbox
              .prop('checked', true)
              .trigger(createPreventedChangeEvent());
            inAction = false;
            inRevert = false;

            return;
          }

          if (jsonResponse.routes) {
            $.each(jsonResponse.routes, (index, value) => {
              const $input = $(`#${value}`);

              inAction = true;

              $input
                .prop('checked', true)
                .trigger(createPreventedChangeEvent());

              inAction = false;
            });
          }
        },
        complete () {
          $checkbox.closest('span.checkbox').removeClass('is--loading');
          $form.find(me.defaults.denyInputSelector).removeAttr('previous-state');
        },
      });
    });
  },
};

export default ({
  ...b2bAssignmentGrid,
  ...overrides,
  name: 'b2bAssignmentGrid',
  initOnLoad: false,
});
