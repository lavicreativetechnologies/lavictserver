const emptyFn = function () {};

export default {
  /**
     * The complete template wrapped in jQuery.
     *
     * @private
     * @property _$modalBox
     * @type {jQuery}
     */
  _$modalBox: null,

  /**
     * Container for the title wrapped in jQuery.
     *
     * @private
     * @property _$header
     * @type {jQuery}
     */
  _$header: null,

  /**
     * The title element wrapped in jQuery.
     *
     * @private
     * @property _$title
     * @type {jQuery}
     */
  _$title: null,

  /**
     * The content element wrapped in jQuery.
     *
     * @private
     * @property _$content
     * @type {jQuery}
     */
  _$content: null,

  /**
     * The close button wrapped in jQuery.
     *
     * @private
     * @property _$closeButton
     * @type {jQuery}
     */
  _$closeButton: null,

  /**
     * Default options of a opening session.
     *
     * @public
     * @property defaults
     * @type {jQuery}
     */
  defaults: {
    /**
         * The mode in which the lightbox should be showing.
         *
         * 'local':
         *
         * The given content is either text or HTML.
         *
         * 'ajax':
         *
         * The given content is the URL from what it should load the HTML.
         *
         * 'iframe':
         *
         * The given content is the source URL of the iframe.
         *
         * @type {String}
         */
    mode: 'local',

    /**
         * Sizing mode of the modal box.
         *
         * 'auto':
         *
         * Will set the given width as max-width so the container can shrink.
         * Fullscreen mode on small mobile devices.
         *
         * 'fixed':
         *
         * Will use the width and height as static sizes and will not change to fullscreen mode.
         *
         * 'content':
         *
         * Will use the height of its content instead of a given height.
         * The 'height' option will be ignored when set.
         *
         * 'full':
         *
         * Will set the modalbox to fullscreen.
         *
         * @type {String}
         */
    sizing: 'auto',

    /**
         * The width of the modal box window.
         *
         * @type {Number}
         */
    width: 600,

    /**
         * The height of the modal box window.
         *
         * @type {Number}
         */
    height: 600,

    /**
         * The max height if sizing is set to `content`
         *
         * @type {Number}
         */
    maxHeight: 0,

    /**
         * Whether or not the overlay should be shown.
         *
         * @type {Boolean}
         */
    overlay: true,

    /**
         * Whether or not the modal box should be closed when the user clicks on the overlay.
         *
         * @type {Boolean}
         */
    closeOnOverlay: true,

    /**
         * Whether or not the closing button should be shown.
         *
         * @type {Boolean}
         */
    showCloseButton: true,

    /**
         * Speed for every CSS transition animation
         *
         * @type {Number}
         */
    animationSpeed: 500,

    /**
         * The window title of the modal box.
         * If empty, the header will be hidden.
         *
         * @type {String}
         */
    title: '',

    /**
         * Will be overridden by the current URL when the mode is 'ajax' or 'iframe'.
         * Can be accessed by the options object.
         *
         * @type {String}
         */
    src: '',

    /**
         * Array of key codes the modal box can be closed.
         *
         * @type {Array}
         */
    closeKeys: [27],

    /**
         * Whether or not it is possible to close the modal box by the keyboard.
         *
         * @type {Boolean}
         */
    keyboardClosing: true,

    /**
         * Function which will be called when the modal box is closing.
         *
         * @type {Function}
         */
    onClose: emptyFn,

    /**
         * Whether or not the picturefill function will be called when setting content.
         *
         * @type {Boolean}
         */
    updateImages: false,

    /**
         * Class that will be added to the modalbox.
         *
         * @type {String}
         */
    additionalClass: '',
  },

  /**
     * The current merged options of the last .open() call.
     *
     * @public
     * @property options
     * @type {Object}
     */
  options: {},

  /**
     * Opens the modal box.
     * Sets the given content and applies the given options to the current session.
     * If given, the overlay options will be passed in its .open() call.
     *
     * @public
     * @method open
     * @param {String|jQuery|HTMLElement} content
     * @param {Object} options
     */
  open(content, options) {
    const me = this;
    let $modalBox = me._$modalBox;
    let opts;

    me.options = opts = $.extend({}, me.defaults, options);

    if (opts.overlay) {
      $.overlay.open($.extend({}, {
        closeOnClick: opts.closeOnOverlay,
        onClose: $.proxy(me.onOverlayClose, me),
      }));
    }

    if (!$modalBox) {
      me.initModalBox();
      me.registerEvents();

      $modalBox = me._$modalBox;
    }

    me._$closeButton.toggle(opts.showCloseButton);

    $modalBox.toggleClass('sizing--auto', opts.sizing === 'auto');
    $modalBox.toggleClass('sizing--fixed', opts.sizing === 'fixed');
    $modalBox.toggleClass('sizing--content', opts.sizing === 'content');
    $modalBox.toggleClass('no--header', opts.title.length === 0);

    $modalBox.addClass(opts.additionalClass);

    if (opts.sizing === 'content') {
      opts.height = 'auto';
    } else {
      $modalBox.css('top', 0);
    }

    me.setTitle(opts.title);
    me.setWidth(opts.width);
    me.setHeight(opts.height);
    me.setMaxHeight(opts.maxHeight);

    // set display to block instead of .show() for browser compatibility
    $modalBox.css('display', 'block');

    switch (opts.mode) {
      case 'ajax':
        $.ajax(content, {
          data: {
            isXHR: 1,
          },
          success(result) {
            me.setContent(result);
            $.publish('plugin/swModal/onOpenAjax', me);
          },
        });
        me.options.src = content;
        break;
      case 'iframe':
        me.setContent(`<iframe class="content--iframe" src="${content}" width="100%" height="100%"></iframe>`);
        me.options.src = content;
        break;
      default:
        me.setContent(content);
        break;
    }

    me.setTransition({
      opacity: 1,
    }, me.options.animationSpeed, 'linear');

    $('html').addClass('no--scroll');

    $.publish('plugin/swModal/onOpen', [me]);

    return me;
  },

  /**
     * Closes the modal box and the overlay if its enabled.
     * if the fading is completed, the content will be removed.
     *
     * @public
     * @method close
     */
  close() {
    const me = this;
    const opts = me.options;
    const $modalBox = me._$modalBox;

    if (opts.overlay) {
      $.overlay.close();
    }

    $('html').removeClass('no--scroll');

    if ($modalBox !== null) {
      me.setTransition({
        opacity: 0,
      }, opts.animationSpeed, 'linear', () => {
        $modalBox.removeClass(opts.additionalClass);

        // set display to none instead of .hide() for browser compatibility
        $modalBox.css('display', 'none');

        opts.onClose.call(me);

        me._$content.empty();
      });
    }

    $.publish('plugin/swModal/onClose', [me]);

    return me;
  },

  /**
     * Sets the transition of the modal box.
     *
     * @public
     * @method setTransition
     * @param {Object} css
     * @param {Number} duration
     * @param {String} animation
     * @param {Function} callback
     */
  setTransition(css, duration, animation, callback) {
    const me = this;
    const $modalBox = me._$modalBox;
    const opts = $.extend({
      animation: 'ease',
      duration: me.options.animationSpeed,
    }, {
      animation,
      duration,
    });

    if (!$.support.transition) {
      $modalBox.stop(true).animate(css, opts.duration, opts.animation, callback);

      return;
    }

    $modalBox.stop(true).transition(css, opts.duration, opts.animation, callback);

    $.publish('plugin/swModal/onSetTransition', [me, css, opts]);
  },

  /**
     * Sets the title of the modal box.
     *
     * @public
     * @method setTitle
     * @param {String} title
     */
  setTitle(title) {
    const me = this;

    me._$title.html(title);

    $.publish('plugin/swModal/onSetTitle', [me, title]);
  },

  /**
     * Sets the content of the modal box.
     *
     * @public
     * @method setContent
     * @param {String|jQuery|HTMLElement} content
     */
  setContent(content) {
    const me = this;
    const opts = me.options;

    me._$content.html(content);

    if (opts.sizing === 'content') {
      // initial centering
      me.center();

      // centering again to fix some styling/positioning issues
      window.setTimeout(me.center.bind(me), 25);
    }

    if (opts.updateImages) {
      picturefill();
    }

    $.publish('plugin/swModal/onSetContent', [me]);
  },

  /**
     * Sets the width of the modal box.
     * If a string was passed containing a only number, it will be parsed as a pixel value.
     *
     * @public
     * @method setWidth
     * @param {Number|String} width
     */
  setWidth(width) {
    const me = this;

    me._$modalBox.css('width', (typeof width === 'string' && !(/^\d+$/.test(width))) ? width : parseInt(width, 10));

    $.publish('plugin/swModal/onSetWidth', [me]);
  },

  /**
     * Sets the height of the modal box.
     * If a string was passed containing a only number, it will be parsed as a pixel value.
     *
     * @public
     * @method setHeight
     * @param {Number|String} height
     */
  setHeight(height) {
    const me = this;
    const hasTitle = me._$title.text().length > 0;
    let headerHeight;

    height = (typeof height === 'string' && !(/^\d+$/.test(height))) ? height : window.parseInt(height, 10);

    if (hasTitle) {
      headerHeight = window.parseInt(me._$header.css('height'), 10);
      me._$content.css('height', (height - headerHeight));
    } else {
      me._$content.css('height', '100%');
    }

    me._$modalBox.css('height', height);
    $.publish('plugin/swModal/onSetHeight', [me]);
  },

  /**
     * Sets the max height of the modal box if the provided value is not empty or greater than 0.
     * If a string was passed containing a only number, it will be parsed as a pixel value.
     *
     * @public
     * @method setMaxHeight
     * @param {Number|String} height
     */
  setMaxHeight(height) {
    const me = this;

    if (!height) {
      return;
    }

    height = (typeof height === 'string' && !(/^\d+$/.test(height))) ? height : window.parseInt(height, 10);

    me._$modalBox.css('max-height', height);
    $.publish('plugin/swModal/onSetMaxHeight', [me]);
  },

  /**
     * Creates the modal box and all its elements.
     * Appends it to the body.
     *
     * @public
     * @method initModalBox
     */
  initModalBox() {
    const me = this;

    me._$modalBox = $('<div>', {
      class: 'js--modal',
    });

    me._$header = $('<div>', {
      class: 'header',
    }).appendTo(me._$modalBox);

    me._$title = $('<div>', {
      class: 'title',
    }).appendTo(me._$header);

    me._$content = $('<div>', {
      class: 'content',
    }).appendTo(me._$modalBox);

    me._$closeButton = $('<div>', {
      class: 'btn icon--cross is--small btn--grey modal--close',
      text: '✖',
    }).appendTo(me._$modalBox);

    $('body').append(me._$modalBox);

    $.publish('plugin/swModal/onInit', [me]);
  },

  /**
     * Registers all needed event listeners.
     *
     * @public
     * @method registerEvents
     */
  registerEvents() {
    const me = this;
    const $window = $(window);

    me._$closeButton.on('click', $.proxy(me.close, me));

    $window.on('modal', $.proxy(me.onKeyDown, me));
    StateManager.on('resize', me.onWindowResize, me);

    StateManager.registerListener({
      state: 'xs',
      enter() {
        me._$modalBox.addClass('is--fullscreen');
      },
      exit() {
        me._$modalBox.removeClass('is--fullscreen');
      },
    });

    $.publish('plugin/swModal/onRegisterEvents', [me]);
  },

  /**
     * Called when a key was pressed.
     * Closes the modal box when the keyCode is mapped to a close key.
     *
     * @public
     * @method onKeyDown
     */
  onKeyDown(event) {
    const me = this;
    const keyCode = event.which;
    const keys = me.options.closeKeys;
    const len = keys.length;
    let i = 0;

    if (!me.options.keyboardClosing) {
      return;
    }

    for (; i < len; i++) {
      if (keys[i] === keyCode) {
        me.close();
      }
    }

    $.publish('plugin/swModal/onKeyDown', [me, event, keyCode]);
  },

  /**
     * Called when the window was resized.
     * Centers the modal box when the sizing is set to 'content'.
     *
     * @public
     * @method onWindowResize
     */
  onWindowResize(event) {
    const me = this;

    if (me.options.sizing === 'content') {
      me.center();
    }

    $.publish('plugin/swModal/onWindowResize', [me, event]);
  },

  /**
     * Sets the top position of the modal box to center it to the screen
     *
     * @public
     * @method centerModalBox
     */
  center() {
    const me = this;
    const $modalBox = me._$modalBox;
    const windowHeight = window.innerHeight || $(window).height();

    $modalBox.css('top', (windowHeight - $modalBox.height()) / 2);

    $.publish('plugin/swModal/onCenter', [me]);
  },

  /**
     * Called when the overlay was clicked.
     * Closes the modalbox when the 'closeOnOverlay' option is active.
     *
     * @public
     * @method onOverlayClose
     */
  onOverlayClose() {
    const me = this;

    if (!me.options.closeOnOverlay) {
      return;
    }

    me.close();

    $.publish('plugin/swModal/onOverlayClick', [me]);
  },

  /**
     * Removes the current modalbox element from the DOM and destroys its items.
     * Also clears the options.
     *
     * @public
     * @method destroy
     */
  destroy() {
    const me = this;
    let p;

    me._$modalBox.remove();

    me._$modalBox = null;
    me._$header = null;
    me._$title = null;
    me._$content = null;
    me._$closeButton = null;

    for (p in me.options) {
      if (!me.options.hasOwnProperty(p)) {
        continue;
      }
      delete me.options[p];
    }

    StateManager.off('resize', me.onWindowResize, [me]);
  },
};
