enum B2BRegex {
    URL_PARAMETERS = '[?&]+([^=&]+)=([^&]*)',
    URL_PARAMETER = '(&|\\?)%identifier%=[^&]*'
}

export default B2BRegex;
