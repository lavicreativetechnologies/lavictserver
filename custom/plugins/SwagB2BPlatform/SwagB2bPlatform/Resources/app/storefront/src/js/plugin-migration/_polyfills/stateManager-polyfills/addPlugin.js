import { runPlugin, getPluginWrapper } from '../../utility';

function getPluginRef(newPluginName) {
  const { aliasMap, pluginObjects } = getPluginWrapper();
  const alias = aliasMap[newPluginName];

  return pluginObjects[alias] || pluginObjects[newPluginName];
}

function addPlugin(_selector, _pluginName, _options) {
  const plugin = getPluginRef(_pluginName);

  if (!plugin) {
    return false;
  }

  return runPlugin(plugin._name, plugin, _options);
}

export {
  addPlugin as default,
  getPluginRef,
};
