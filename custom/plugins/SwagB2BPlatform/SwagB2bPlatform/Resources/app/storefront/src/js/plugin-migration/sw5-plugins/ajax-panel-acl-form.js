import { b2bAjaxPanelAclForm } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelAclForm,
  name: 'b2bAjaxPanelAclForm',
  initOnLoad: true,
  selector: 'body',
});
