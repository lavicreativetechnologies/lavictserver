import { b2bDuplicateElements } from '../../jsPluginBaseObjects';

const overrides = {

  onButtonClick () {
    const me = this;

    const $container = me.defaults.containerSelector;
    const $item = $(`${$container} div${me.defaults.itemSelector}:last-child`);
    const $clonedItem = $item.clone();

    $clonedItem.find('input').val('');
    $clonedItem.find('button').remove();

    $clonedItem.append('<button type="button" class="btn" onclick="this.parentNode.remove()">✕</button>');

    $($container).append($clonedItem);
  },
};

export default ({
  ...b2bDuplicateElements,
  ...overrides,
  name: 'b2bDuplicateElements',
  initOnLoad: false,
});
