/* global StateManager, CSRF */

import { b2bAjaxPanel } from '../../jsPluginBaseObjects';

const overrides = {

  registerGlobalListeners () {
    const me = this;

    me._on(document, 'click', `${me.defaults.panelSelector} a, ${me.defaults.panelLinkSelector}`, function (event) { // eslint-disable-line
      if (event.isDefaultPrevented()) {
        return;
      }

      const $eventTarget = $(event.target);
      if (!$eventTarget.is(this) && $eventTarget.closest('form').length) {
        return;
      }

      const $anchor = $(this);
      if ($anchor.is(me.defaults.ignoreSelector)) {
        return;
      }

      me.breakEventExecution(event);

      let url;
      const $panel = $anchor.closest(me.defaults.panelSelector);
      const targetPanel = $anchor.data('target');
      const $targetPanel = $(`${me.defaults.panelSelector}[data-id="${targetPanel}"]`);

      if ($anchor[0].hasAttribute(me.defaults.panelLinkDataKey)) {
        url = $anchor.attr(me.defaults.panelLinkDataKey);
      } else {
        url = $anchor.data(me.defaults.panelLinkDataKey);
      }

      if ($targetPanel.length) {
        me.load(url, $targetPanel, $anchor);
      } else {
        me.load(url, $panel, $anchor);
      }
    });

    me._on(document, 'submit', `${me.defaults.panelSelector} form, form${me.defaults.panelLinkSelector}`, function (event) { // eslint-disable-line
      if (event.isDefaultPrevented()) {
        return;
      }

      const $form = $(this);

      if ($form.is(me.defaults.ignoreSelector)) {
        return;
      }

      me.breakEventExecution(event);

      const $panel = $form.closest(me.defaults.panelSelector);
      const url = $form.attr('action');
      const targetPanel = $form.data('target');
      const $targetPanel = $(`${me.defaults.panelSelector}[data-id="${targetPanel}"]`);

      if ($targetPanel.length) {
        me.load(url, $targetPanel, $form);
      } else {
        me.load(url, $panel, $form);
      }
    });

    me._on(document, me.defaults.panelAfterLoadEvent, me.defaults.panelSelector, function (event) { // eslint-disable-line
      if (event.isDefaultPrevented()) {
        return;
      }

      const $panel = $(this);

      me.breakEventExecution(event);

      $panel.find(me.defaults.panelSelector).each(function () { // eslint-disable-line
        const panel = this;

        me.register(panel);
      });
    });

    me._on(document, me.defaults.performAjaxCallEvent, (event, url, $target, source, contentType = '') => {
      me.load(url, $target, source, contentType);
    });

    me._on(document, me.defaults.panelRefreshEvent, me.defaults.panelSelector, function (event, reloadUrl) { // eslint-disable-line
      if (event.isDefaultPrevented()) {
        return;
      }

      const $panel = $(this);
      let ajaxData = {};

      if (reloadUrl) {
        ajaxData = {
          type: 'GET',
          url: reloadUrl,
        };
      } else {
        ajaxData = $panel.data(me.defaults.panelHistoryDataKey);
      }

      if (!ajaxData) {
        return;
      }

      me.breakEventExecution(event);
      me.doAjaxCall(ajaxData, $panel, $panel);
    });

    me._on(document, me.defaults.panelRegisterEvent, me.defaults.panelSelector, function (event) { // eslint-disable-line
      if (event.isDefaultPrevented()) {
        return;
      }

      const $panel = $(this);

      me.register($panel);
    });

    me._on(document, 'click', '*[data-form-id]', function (event) { // eslint-disable-line
      const me = this; // eslint-disable-line
      const formId = $(me).data('form-id');
      const $form = $(`#${formId}`);

      if (!$form.length) {
        return;
      }

      $form.submit();
    });
  },

  registerShopwarePlugins () {
    StateManager.addPlugin('.datepicker', 'swDatePicker');
  },

  refreshShopwarePlugins ($target) {
    $.modal.onWindowResize();

    if ($target.find('.datepicker').length) {
      StateManager.updatePlugin('.datepicker', 'swDatePicker');
    }

    CSRF.updateForms();
  },

  load (url, target, source, contentType = '') {
    const me = this;
    const $target = $(target);
    let serializedData = {};
    let method = 'GET';

    if (source && source.length) {
      if (source.is('form')) {
        if (source.attr('method')) {
          method = source.attr('method').toUpperCase() === 'POST' ? 'POST' : 'GET';
        }
        serializedData = source.serializeArray();
      } else if (source.data(me.defaults.panelPayloadDataKey)) {
        serializedData = source.data(me.defaults.panelPayloadDataKey);
      }
    }

    const ajaxData = {
      type: method,
      url,
      data: serializedData,
    };

    me.doAjaxCall(ajaxData, $target, source, contentType);
  },

  doAjaxCall (ajaxData, $target, $source, contentType) {
    const me = this;

    $target.append($('<div>', {
      class: me.defaults.panelLoadingClass,
      html: $('<span></span><span></span><span></span><span></span>'),
    }));

    if (me.notify($target, me.defaults.panelBeforeLoadEvent,
      { panel: $target, source: $source, ajaxData })) {
      return;
    }

    if (ajaxData.type === 'GET' && !$source[0].classList.contains('ajax-panel--ignore-history')) {
      $target.data(me.defaults.panelHistoryDataKey, ajaxData);
    }

    if (contentType && contentType !== '') {
      ajaxData.contentType = contentType; // eslint-disable-line
    }

    $.ajax($.extend({}, ajaxData, {
      success (response, status, xhr) {
        if (xhr.getResponseHeader('B2b-no-login')) {
          window.location.reload();

          return;
        }

        if ($source.data('close-success') && !$(response).find('.modal--errors').length) {
          $.modal.close();
        } else {
          $target.html(response);
        }

        $target.trigger(me.defaults.panelAfterLoadEvent, {
          panel: $target,
          source: $source,
          ajaxData,
        });
      },
      error () {
        me.error($target);
      },
      always () {
        $target.removeClass(me.defaults.panelLoadingClass);
      },
    }));
  },
};

export default ({
  ...b2bAjaxPanel,
  ...overrides,
  name: 'b2bAjaxPanel',
  initOnLoad: true,
  selector: 'body',
});
