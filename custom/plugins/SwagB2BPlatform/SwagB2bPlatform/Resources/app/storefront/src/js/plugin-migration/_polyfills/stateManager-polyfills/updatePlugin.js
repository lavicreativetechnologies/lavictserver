import { getPluginWrapper } from '../../utility';

function checkForAlias(name) {
  return getPluginWrapper().aliasMap[name] || name;
}

function getEl(selector) {
  return $(selector);
}

export default function (selector, pluginName) {
  const { pluginObjects } = getPluginWrapper();

  if (selector && pluginName) {
    const name = checkForAlias(pluginName);
    const el = getEl(selector);

    if (name && pluginObjects[name] && pluginObjects[name].init) {
      const plugin = pluginObjects[name];

      plugin.$el = el;
      plugin.opts = plugin.defaults;
      plugin.init();
    }
  }
}
