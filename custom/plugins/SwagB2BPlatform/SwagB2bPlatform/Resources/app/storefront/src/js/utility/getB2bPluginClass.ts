import PluginBaseClass from '../compatibility/PluginBase.class.js';
import { B2bPluginClassInterface } from '../interfaces/index';

declare const $;

// todo: Use the PluginBaseClass.EventEmitter for custom events, after refactoring is completed -> Remove this class

function getB2bPluginClass() {
    const EXTEND_CLASS = window.PluginBaseClass || PluginBaseClass;

    abstract class B2bPlugin extends EXTEND_CLASS implements B2bPluginClassInterface {
        public $emitter: {
            publish: (eventName: string, detail?: any) => void,
            subscribe: (eventName: string, callback: () => void, opts?: any) => void,
            unsubscribe: (eventName: string) => void
        };

        public options: any;

        public el: HTMLElement;

        public constructor(el: HTMLElement, options?: object, pluginName?: string) {
            super(el, options, pluginName);
        }

        protected _subscribe(selector: string, eventType: string, callback: (event: any, eventData?: AjaxPanelEventData) => void): void {
            $(selector).on(eventType, callback);
        }

        protected _unsubscribe(selector: string, eventType: string): void {
            $(selector).off(eventType);
        }

        protected _publish(selector: string, eventType: string, payload: object = {}): void {
            $(selector).trigger(eventType, payload);
        }
    }

    return B2bPlugin;
}

export default getB2bPluginClass;
