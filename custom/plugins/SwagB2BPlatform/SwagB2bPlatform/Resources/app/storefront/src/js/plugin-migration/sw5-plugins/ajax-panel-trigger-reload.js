import { b2bAjaxPanelTriggerReload } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelTriggerReload,
  name: 'b2bAjaxPanelTriggerReload',
  initOnLoad: true,
  selector: 'body',
});
