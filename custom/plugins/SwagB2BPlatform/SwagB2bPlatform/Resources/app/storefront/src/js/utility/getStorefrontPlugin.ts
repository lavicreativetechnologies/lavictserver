/* istanbul ignore file */

import PluginBaseClassInterface from '../interfaces/PluginBaseClass.interface';

function getStorefrontPlugin(pluginName: string): PluginBaseClassInterface[];
function getStorefrontPlugin(pluginName: string, firstOnly: boolean = true): PluginBaseClassInterface | PluginBaseClassInterface[] {
    const pluginMap = window.PluginManager.getPlugin(pluginName);
    const instances = pluginMap.get('instances');

    if (!instances.length) {
        return null;
    }

    return firstOnly ? instances[0] : instances;
}

export default getStorefrontPlugin;
