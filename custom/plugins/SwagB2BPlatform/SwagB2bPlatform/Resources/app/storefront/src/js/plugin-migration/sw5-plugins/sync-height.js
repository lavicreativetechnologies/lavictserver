import { b2bSyncHeight } from '../../jsPluginBaseObjects';

export default ({
  ...b2bSyncHeight,
  name: 'b2bSyncHeight',
  initOnLoad: true,
  selector: 'body',
});
