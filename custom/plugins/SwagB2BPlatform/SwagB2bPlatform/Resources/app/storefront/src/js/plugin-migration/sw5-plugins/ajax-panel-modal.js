import { b2bAjaxPanelModal } from '../../jsPluginBaseObjects';

const toggleSelector = '.b2b--modal-navigation--toggle';
const _init = b2bAjaxPanelModal.init;

function handleSmallModal (isSmallModal) {
  const modalContainer = document.querySelector('.js--modal');

  if (modalContainer) {
    if (isSmallModal) {
      modalContainer.classList.add('is--small');
    } else {
      modalContainer.classList.remove('is--small');
    }
  }
}

function isModalOrModalLink (target) {
  let currentTarget = target;
  let _isModal = false;

  while (currentTarget) {
    if (currentTarget.classList.contains('js--modal') || currentTarget.classList.contains('link-modal--small')) {
      _isModal = true;
      currentTarget = false;
    }
    currentTarget = currentTarget.parentElement;
  }

  return _isModal;
}

const overrides = {
  defaults: {
    ...b2bAjaxPanelModal.defaults,
    normalWidth: 1200,
  },
  init () {
    const me = this;
    _init.bind(me)();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const { source } = eventData;

        if (source && source[0]) {
          const _isModalOrModalLink = isModalOrModalLink(source[0]);

          if (_isModalOrModalLink) {
            const isSmallModal = source[0].classList.contains('link-modal--small') || source[0].classList.contains('modal--small');

            handleSmallModal(isSmallModal);
          }
        }
      },
    );

    me._on(
      document,
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        const { panel } = eventData;
        this.toggleNavigation(panel);
        this.shouldCloseOnSuccess(panel);
      },
    );
  },

  shouldCloseOnSuccess (panel) {
    const shouldClose = panel.find('[data-modal-close]').length;

    if (!shouldClose) {
      return;
    }

    $.modal.close();
  },

  toggleNavigation (panel) {
    if (!panel.hasClass('tab--container')) {
      const hideToggle = !!panel[0].querySelector('.no--navigation');
      const toggle = $(toggleSelector);
      toggle.parent().toggleClass('navigation--hidden', hideToggle);
    }
  },
};

export default ({
  ...b2bAjaxPanelModal,
  ...overrides,
  name: 'b2bAjaxPanelModal',
  initOnLoad: true,
  selector: 'body',
});
