export default ({
  name: 'b2bAccordion',
  selector: 'body',
  initOnLoad: true,
  defaults: {
    selectorAccordion: '.panel--accordion',
    classActive: 'panel--accordion-open',
    classMobile: 'panel--accordion-mobile-closed',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
  },
  init () {
    this._on(
      document,
      this.defaults.panelAfterLoadEvent,
      this.applyEventListener.bind(this),
    );

    this.applyEventListener();
  },
  applyEventListener (event) {
    const accordions = (event ? event.target : document)
      .querySelectorAll(this.defaults.selectorAccordion);

    if (accordions.length === 0) {
      return;
    }

    accordions.forEach((accordion) => {
      this.initAccordion(accordion);
    });
  },
  initAccordion (target) {
    const head = target.querySelector('.panel--title');

    head.addEventListener('click', this.handleClick.bind(this, target));
  },
  handleClick (target) {
    const hasMobileClass = target.classList.contains(this.defaults.classMobile);
    const isOpen = target.classList.contains(this.defaults.classActive);
    const isMobile = window.outerWidth < 1025;

    if (hasMobileClass && isMobile) {
      target.classList.remove(this.defaults.classMobile);
    } else {
      target.classList[isOpen ? 'remove' : 'add'](this.defaults.classActive);
    }

    if (!isOpen || (hasMobileClass && isMobile)) {
      const body = target.querySelector('.panel--body');
      this.scrollIntoView(body);
    }
  },
  scrollIntoView (target) {
    target.scrollIntoView({
      block: 'start',
      behavior: 'smooth',
    });
  },
});
