function off(event, callback) {
  if (event && callback) {
    $(window).off(event, callback);
  }
}

function on(event, callback, context = {}) {
  if (event && callback) {
    $(window).on(event, callback.bind(context));
  }
}

export {
  on,
  off,
};
