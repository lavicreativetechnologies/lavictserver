import { b2bAjaxPanelOrderNumber } from '../../jsPluginBaseObjects';

const overrides = {
  onSaveOrderNumber (event) {
    event.preventDefault();
    event.stopPropagation();

    const me = this;
    const $target = $(event.target);
    const $row = $target.closest('tr');
    const $saveUrl = $row.attr('data-save-url');
    const $id = $row.attr('data-id');
    const $orderNumberInput = $row.find(me.defaults.inputSelector);
    const $customOrderNumber = $row.find(me.defaults.inputCustomOrderNumberSelector);

    $.publish('b2b/ordernumber/onSaveOrderNumber', [me, $target, $row, $saveUrl, $id, $orderNumberInput, $customOrderNumber]);

    if (!$saveUrl) {
      return;
    }

    $.ajax({
      url: $saveUrl,
      type: 'POST',
      data: this.getPayload($row),
      success (data) {
        $('.b2b--ajax-panel[data-id="order-number-grid"]').trigger('b2b--ajax-panel_refresh');

        $.publish('b2b/ordernumber/onSaveOrderNumber/success', [me, data, $target, $row, $saveUrl, $id, $orderNumberInput, $customOrderNumber]);
      },
    });
  },

  getPayload ($row) {
    const me = this;

    const $id = $row.attr('data-id');
    const $orderNumberInput = $row.find(me.defaults.inputSelector);
    const $customOrderNumber = $row.find(me.defaults.inputCustomOrderNumberSelector);

    const $filterField = $('input[name="filters[all][field-name]"]').val();
    const $filterType = $('input[name="filters[all][type]"]').val();
    const $filterValue = $('input[name="filters[all][value]"]').val();
    const $sortBy = $('select[name="sort-by"] option[selected="selected"]').val();
    const $page = $('select[name="page"] option[selected="selected"]').val();
    const $csrf = $row.find('input[name="custom-csrf-input"]').val();

    return {
      id: $id,
      orderNumber: $($orderNumberInput).val(),
      customOrderNumber: $($customOrderNumber).val(),
      'filters[all][field-name]': $filterField,
      'filters[all][type]': $filterType,
      'filters[all][value]': $filterValue,
      'sort-by': $sortBy,
      page: $page,
      _csrf_token: $csrf,
    };
  },
};

export default ({
  ...b2bAjaxPanelOrderNumber,
  ...overrides,
  name: 'b2bAjaxPanelOrderNumber',
  initOnLoad: false,
});
