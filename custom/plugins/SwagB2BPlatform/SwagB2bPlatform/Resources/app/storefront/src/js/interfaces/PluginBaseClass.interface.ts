export default interface PluginBaseClassInterface {
    $emitter: {
        publish: (eventName: string, detail?: any) => void;
        subscribe: (eventName: string, callback: () => void, opts?: object) => void;
        unsubscribe: (eventName: string) => void;
    };
    options: any;
    el: HTMLElement;
    [x: string]: any;
}
