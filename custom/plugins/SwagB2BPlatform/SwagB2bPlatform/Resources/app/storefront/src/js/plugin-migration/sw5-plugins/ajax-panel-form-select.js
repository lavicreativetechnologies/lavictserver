import { b2bAjaxPanelFormSelect } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelFormSelect,
  name: 'b2bAjaxPanelFormSelect',
  initOnLoad: true,
  selector: 'body',
});
