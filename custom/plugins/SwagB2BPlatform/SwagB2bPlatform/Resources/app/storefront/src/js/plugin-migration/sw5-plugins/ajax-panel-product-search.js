import { b2bAjaxPanelProductSearch } from '../../jsPluginBaseObjects';

const overrides = {
};

export default ({
  ...b2bAjaxPanelProductSearch,
  ...overrides,
  name: 'b2bAjaxPanelProductSearch',
  initOnLoad: false,
});
