import { b2bSelectboxReplacement } from '../../jsPluginBaseObjects';

export default ({
  ...b2bSelectboxReplacement,
  name: 'b2bSelectboxReplacement',
  alias: 'swSelectboxReplacement',
  initOnLoad: false,
});
