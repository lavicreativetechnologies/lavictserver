export default async function (response, { always, error, success }) {
  const isValidCallback = (method) => (!!method && typeof method === 'function');
  const { status } = response;

  if (isValidCallback(always)) {
    always();
  }

  if (status >= 400 && isValidCallback(error)) {
    error();
  } else if (status < 400 && isValidCallback(success)) {
    const headersGetter = {
      getResponseHeader: (key) => response.headers.get(key),
      getAllResponseHeaders: () => response.headers.entries(),
    };

    const contentType = response.headers.get('Content-Type');
    let result;

    if (contentType === 'application/json') {
      result = await response.json();
    } else {
      result = await response.text();
    }

    success(result, status, headersGetter);
  }
}
