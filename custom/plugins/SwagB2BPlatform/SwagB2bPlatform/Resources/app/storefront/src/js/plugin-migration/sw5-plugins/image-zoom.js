import { b2bImageZoom } from '../../jsPluginBaseObjects';

export default ({
  ...b2bImageZoom,
  name: 'b2bImageZoom',
  initOnLoad: true,
  selector: '.js--img-zoom--container',
});
