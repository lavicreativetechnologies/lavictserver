import { b2bTreeSelect } from '../../jsPluginBaseObjects';

const overrides = {

  init () {
    this.applyDataAttributes();

    const me = this;

    me.$tree = me.$el.find(me.defaults.treeContainerSelector);

    if (!me.$tree.length) {
      return;
    }

    me.$tree.find(me.defaults.ajaxPanelSelector).on(
      me.defaults.ajaxPanelRefreshEvent, $.proxy(me.updateSelectedNodesOnRefresh, me),
    );
    me._on(me.$tree, me.defaults.ajaxPanelLoadedEvent, $.proxy(me.doLayout, me));
    me._on(me.$tree, 'click', 'a', $.proxy(me.toggleNodeOpenState, me));
    me._on(me.$tree, 'click', 'li', $.proxy(me.toggleSelection, me));
    me._on(me.$tree, 'click', me.defaults.deleteItemSelector, $.proxy(me.onDeleteClick, me));
    me._on(document, me.defaults.tabChangeEvent, $.proxy(me.onRemoveError, me));

    me.initDragAndDrop();
    me.doLayout();
  },

  initDragAndDrop () {
    const me = this;

    me.moveUrl = me.$tree.data('moveUrl');
    me.csrfToken = me.$tree.data('csrfToken');

    me._on(me.$tree, 'dragstart', 'li', $.proxy(me.startDrag, me));
    me._on(me.$tree, 'dragenter', '.drop-area span, .tree-handle', $.proxy(me.handleDragEnter, me));
    me._on(me.$tree, 'dragenter', '.b2b-tree-node-inner', $.proxy(me.toggleDragover, me));
    me._on(me.$tree, 'dragend, dragexit', $.proxy(me.doLayout, me));
    me._on(me.$tree, 'dragenter', 'li', me.stopEvent);
    me._on(me.$tree, 'dragover', 'li', me.stopEvent);
    me._on(me.$tree, 'drop', 'li', $.proxy(me.dropListItem, me));
    me._on(me.$tree, 'mouseleave', $.proxy(me.dragOutside, me));
    me._on(me.$tree, 'touchend', 'li.tree-select-item', $.proxy(me.endTouch, me));
    me._on(me.$tree, 'touchmove', 'li.tree-select-item', $.proxy(me.drag, me));
  },

  updateSelectedNodesOnRefresh (event) {
    const me = this;
    const nodeIds = [];
    const $el = $(event.currentTarget);
    const requestData = $el.data('lastPanelRequest');

    me.$tree.find('.is--opened').each(function () { // eslint-disable-line
      nodeIds.push($(this).data('id'));
    });

    requestData.data = {};
    nodeIds.forEach((nodeId) => {
      requestData.data = { openNodes: nodeId };
    });
    $el.data('lastPanelRequest', requestData);
  },

  removeItemAjax ($form) {
    const me = this;
    const $roleElement = $form.find('.roleId');
    const $createParentInput = $('.role-block input.b2b--tree-selection-aware');
    const $defaultRoleElement = me.$tree.find('#tree-node-id-0');

    $form.find('input.hidden-open-node-field').remove();
    me.$tree.find('.is--opened').each(function () { // eslint-disable-line
      $form.append(`<input type=hidden class="hidden-open-node-field" name="openNodes[]" value="${$(this).data('id')}"/>`);
    });

    const csrf = $form.data('csrfToken');

    if (csrf) {
      const csrfInput = $form.find('[name="_csrf_token"]');
      if (csrfInput) {
        csrfInput.val(csrf);
      }
    }

    me.$el.trigger(
      'b2b--do-ajax-call', [
        $form.attr('action'),
        me.$tree.find(me.defaults.ajaxPanelSelector),
        $form,
      ],
    );

    if (!$roleElement.length) {
      return;
    }

    if ($createParentInput.val() === $roleElement.val()) {
      $createParentInput.val($defaultRoleElement.data('id'));
    }

    me.resetTabContent($roleElement);
  },

  onRemoveError () {
    const me = this;
    const $errorBox = me.$tree.find(me.defaults.errorBoxSelector);

    if (!$errorBox) {
      return;
    }

    $errorBox.slideUp();
  },

  toggleNodeOpenState (event) {
    const me = this;
    me.stopEvent(event);
    const $anchor = $(event.currentTarget);

    const $listItem = $anchor.closest('li');
    const $list = $listItem.find('ul:eq(0)');

    if (!$listItem.hasClass(me.defaults.nodeClassHasChildren)
      || $listItem.hasClass(me.defaults.nodeLoadingClass)) {
      return;
    }

    if ($listItem.hasClass(me.defaults.nodeOpenClass)) {
      $listItem.addClass(me.defaults.nodeClosedClass)
        .removeClass(me.defaults.nodeOpenClass);

      $list.empty();

      return;
    }

    $listItem.removeClass(me.defaults.nodeClosedClass)
      .addClass(me.defaults.nodeLoadingClass);

    $.ajax({
      url: $anchor.attr('href'),
      success (response) {
        $listItem
          .removeClass(me.defaults.nodeLoadingClass)
          .addClass(me.defaults.nodeOpenClass);

        $list.append(response);

        const $storageItem = $anchor.closest(me.defaults.ajaxPanelSelector);
        const $selectedId = $storageItem.data('tree-selected-id');
        const targetLi = document.querySelector(`[data-id="${$selectedId}"]`);
        $(targetLi).addClass('selected');

        me.doLayout($list[0]);
      },
    });
  },

  removeClasses () {
    this.$tree
      .find('.dragover')
      .removeClass('dragover');

    this.$tree
      .find('.hover')
      .removeClass('hover');

    this.$tree
      .find('.dragged')
      .removeClass('dragged');
  },

  doLayout (listItem = null) {
    const me = this;

    this.$tree.find('.b2b-tree-node-inner').each(function () { // eslint-disable-line
      const $this = $(this);
      const multiplier = $this.parents('ul.is--b2b-tree-select').length - 1;

      $this.css('marginLeft', multiplier * me.defaults.nodeMargin);
    });

    let itemCount = 0;
    this.$tree.find('li').each(function () { // eslint-disable-line
      $(this).attr('id', `tree-node-id-${itemCount++}`);
    });

    this.$tree
      .find('.dragover')
      .removeClass('dragover');

    this.$tree
      .find('.hover')
      .removeClass('hover');

    const $el = this.$tree.find('.dragged');

    if ($el.length !== 0) {
      if (!listItem) {
        $el.removeClass('dragged');

        return;
      }

      if (listItem && me.offset) {
        const listHeight = listItem.clientHeight;
        const top = $el.css('top').replace('px', '');

        if (top < 0) {
          me.offset.top += listHeight;
        }

        $el.css({
          top: top < 0 ? top - (listHeight - 1) : top + (listHeight - 1),
        });
      }
    }
  },

  drop ($el, $originalItem, left, top) {
    const me = this;

    if ($originalItem.find($el).length) {
      me.doLayout();

      return;
    }

    if ($originalItem.is($el)) {
      me.doLayout();

      return;
    }

    if (!$originalItem.data('id')) {
      this.removeClasses();

      return;
    }

    const $parent = $originalItem.parents('li').first();
    if ($parent.hasClass('has--children')) {
      if (!$originalItem.siblings('li:visible').length) {
        $parent.removeClass('has--children');
      }
    }

    const $spacerElement = $(document.elementFromPoint(left, top));

    if (!$spacerElement.closest('.drop-area').length) {
      me.doLayout();

      return;
    }

    let type;

    if ($spacerElement.hasClass('drop-before')) {
      type = 'prev-sibling';
      $originalItem.remove().insertBefore($el);
    } else if ($spacerElement.hasClass('drop-as-child')) {
      type = 'last-child';
      $el.addClass('has--children');
      $originalItem.remove().appendTo($el.find('ul:eq(0)'));
    } else if ($spacerElement.hasClass('drop-after')) {
      type = 'next-sibling';
      $originalItem.remove().insertAfter($el);
    }

    if (type === 'last-child' && $el.hasClass('has--children')
      && !$el.hasClass('is--opened')) {
      $originalItem.hide();
    }

    $.post(me.moveUrl, {
      type,
      _csrf_token: me.csrfToken,
      roleId: $originalItem.data('id'),
      relatedRoleId: $($el).data('id'),
      success () {
        if (type === 'last-child' && $el.hasClass('has--children')
          && !$el.hasClass('is--opened')) {
          $el.find('a:eq(0)').click();
        }
      },
    });

    me.doLayout();
  },

  dropListItem (event) {
    const me = this;
    const $el = $(event.currentTarget);

    event.stopPropagation();
    event.preventDefault();

    const $originalItem = $(`#${event.originalEvent.dataTransfer.getData('text/plain')}`);

    me.drop($el, $originalItem, event.clientX, event.clientY);
  },

  dropListItemTouch (event) {
    const me = this;
    me.stopEvent(event);

    const $originalItem = $(event.currentTarget);
    const orig = event.originalEvent;

    const top = orig.changedTouches[0].clientY;
    const left = orig.changedTouches[0].clientX;

    const $el = me.getTargetListItem(top, left, $originalItem[0].id);

    me.drop($el, $originalItem, left, top);
  },

  getTargetListItem (top, left, id) {
    const me = this;
    let target = null;
    me.$tree.find('li.tree-select-item').each(function () { // eslint-disable-line
      const bounding = this.getBoundingClientRect();
      const targetId = this.id;
      // check if touchpoint is over element
      if ((bounding.top < top && top < bounding.bottom
        && bounding.left < left && bounding.right > left)
        && targetId !== id) {
        target = $(this);
      }
    });

    return target;
  },

  stopEvent (event) {
    event.stopPropagation();
    event.preventDefault();
  },

  startDrag (event) {
    event.stopPropagation();

    const $el = $(event.currentTarget);

    $el.addClass('dragged');

    event.originalEvent.dataTransfer.setData('text/plain', $el.attr('id'));
    event.originalEvent.dataTransfer.dropEffect = 'move';  // eslint-disable-line
    event.originalEvent.dataTransfer.effectAllowed = 'move';  // eslint-disable-line
  },

  endTouch (event) {
    if (document.querySelector('.dragged')) {
      this.dropListItemTouch(event);
    }
  },

  drag (event) {
    const me = this;
    me.stopEvent(event);
    const $el = $(event.currentTarget);

    const orig = event.originalEvent;
    const screenPosition = {
      top: orig.changedTouches[0].screenY,
      left: orig.changedTouches[0].screenX,
    };

    if (!$el.hasClass('dragged')) {
      me.offset = screenPosition;
      $el.addClass('dragged');
    }

    $el.css({
      top: screenPosition.top - me.offset.top,
      left: screenPosition.left - me.offset.left,
    });

    const top = orig.changedTouches[0].clientY;
    const left = orig.changedTouches[0].clientX;

    me.handleDragEnterTouch(top, left, $el[0].id);
  },

  dragEnter (element) {
    const me = this;
    const now = (new Date()).getTime();
    const timing = me.defaults.hoverTimeToWait;

    me.styleHover(element);

    if (!element.is('.tree-handle')) {
      return;
    }

    let lastTimeoutTime = element.data('lastTimeoutTime');

    if (!lastTimeoutTime) {
      lastTimeoutTime = 0;
    }

    if ((lastTimeoutTime + timing) > now) {
      return;
    }

    element.data('lastTimeoutTime', now);

    setTimeout(() => {
      const $link = element.closest('a');
      const $treeHandle = element.closest('li');

      if (!$link.length) {
        return;
      }

      if ($treeHandle.hasClass(me.defaults.nodeOpenClass)) {
        return;
      }

      if (!element.is('.hover')) {
        return;
      }

      $link.click();
    }, timing);
  },

  handleDragEnter (event) {
    const me = this;
    const $el = $(event.currentTarget);

    me.dragEnter($el);
  },

  handleDragEnterTouch (top, left, id) {
    const me = this;
    me.$tree.find('.b2b-tree-node-inner, .tree-handle, .drop-area span').each(function () { // eslint-disable-line
      const bounding = this.getBoundingClientRect();
      const $li = $(this).closest('li');
      const parentId = $li[0].id;
      // is touchpoint on li element
      if ((bounding.top < top && top < bounding.bottom
        && bounding.left < left && bounding.right > left)
        && parentId !== id) {
        if ($(this).hasClass('b2b-tree-node-inner')) {
          me.dragOver($(this));
        } else if ($(this).hasClass('tree-handle')) {
          me.dragEnter($(this));
        } else {
          me.styleHover($(this));
        }
      }
    });
  },

  toggleDragover (event) {
    const me = this;
    const $el = $(event.currentTarget);
    me.dragOver($el);
  },

  dragOver (element) {
    const me = this;
    me.$tree
      .find('.dragover')
      .removeClass('dragover');

    if (element.closest('.dragged').length) {
      return;
    }

    element.addClass('dragover');
  },

  styleHover (element) {
    const me = this;

    me.$tree
      .find('.hover')
      .removeClass('hover');

    if (element.hasClass('drop-as-child')) {
      const $li = element.closest('li');
      $li.find('.b2b-tree-node-inner:first').addClass('hover');
    } else {
      element.addClass('hover');
    }
  },
};

export default ({
  ...b2bTreeSelect,
  ...overrides,
  name: 'b2bTreeSelect',
  initOnLoad: false,
});
