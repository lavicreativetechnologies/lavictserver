/* eslint-disable */

import getPluginWrapper from '../getPluginWrapper';

export default async function _on() {
  const me = this;
  const $el = $(arguments[0]);
  const event = me.getEventName(arguments[1]);
  const args = Array.prototype.slice.call(arguments, 2);

  me._events.push({ el: $el, event });
  args.unshift(event);

  await $el.off(event);

  $el.on.apply($el, args);

  $.publish(`plugin/${me._name}/onRegisterEvent`, [$el, event]);


  getPluginWrapper().actions.pushEvent({
    event,
    target: $el,
    args,
  });

  return me;
}
/* eslint-enable */
