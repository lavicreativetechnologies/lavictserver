function isValidHeight(height) {
  let _height = height.toString();
  if (_height.indexOf('px') > -1) {
    _height = parseInt(_height.substr(0, _height.indexOf('px')), 10);

    return _height;
  }

  return false;
}

/* eslint-disable no-param-reassign */
function _slideDown(target, speed) {
  /* eslint-disable-next-line */
  return new Promise(async resolve => {
    const { display, height, transition } = target.style;
    const validHeight = isValidHeight(height);

    if (display === 'none' && validHeight) {
      target.style.height = 0;
      target.style.transition = `height ${speed}ms ease`;
      target.style.display = '';

      await new Promise((_resolve) => {
        setTimeout(_resolve);
      });

      if (window.requestAnimationFrame) {
        requestAnimationFrame(() => {
          target.style.height = `${validHeight}px`;
        });
      } else {
        target.style.height = `${validHeight}px`;
      }

      await new Promise((_resolve) => {
        setTimeout(_resolve, speed);
      });

      target.style.transition = transition;
    }

    resolve();
  });
}
/* eslint-enable no-param-reassign */

export default async function (speed = 400, callback) {
  const target = this.length > 0 ? this[0] : false;

  if (target) {
    await _slideDown(target, speed);
  }

  if (callback) {
    callback();
  }

  return this;
}
