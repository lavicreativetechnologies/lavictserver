import { getB2bPluginClass } from '../utility';
import { B2BEvents, B2BKeycodes } from '../enums';

export default class SubmitOnEnterPlugin extends getB2bPluginClass() {
    public static options = {
        selector_form: '[data-submit-enter]',
        selector_submit: 'button[type=submit]',
        event_form_submit: 'keypress',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe('body', B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelAfterLoadEvent(event): void {
        const panel = event.target;
        const form = panel.querySelector(this.options.selector_form);

        if (!form) {
            return;
        }

        form.addEventListener(this.options.event_form_submit, (submitEvent: KeyboardEvent) => {
            this.handleKeypressEvent.apply(this, [submitEvent, panel]);
        });
    }

    protected handleKeypressEvent(event: KeyboardEvent, panel: HTMLElement): void {
        if (event.keyCode !== B2BKeycodes.ENTER) {
            return;
        }

        event.preventDefault();
        const button: HTMLElement = panel.querySelector(this.options.selector_submit);

        if (!button) {
            return;
        }

        button.click();
    }
}
