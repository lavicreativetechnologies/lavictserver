import getPluginWrapper from './getPluginWrapper';

export default function (name) {
  const { pluginObjects, aliasMap } = getPluginWrapper();
  let targetName = false;

  if (aliasMap[name]) {
    targetName = aliasMap[name];
  } else {
    Object.keys(pluginObjects).forEach((pluginName) => {
      const obj = pluginObjects[pluginName];

      if (obj.name === name || obj._name === name || obj.alias === name) {
        targetName = pluginName;
      }
    });
  }

  return targetName;
}
