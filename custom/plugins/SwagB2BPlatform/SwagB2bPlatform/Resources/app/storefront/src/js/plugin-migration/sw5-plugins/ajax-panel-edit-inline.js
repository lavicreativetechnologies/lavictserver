import { b2bAjaxPanelEditInline } from '../../jsPluginBaseObjects';

const overrides = {
  init () {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'click',
      '*[data-mode="edit"]:not(.is--b2b-acl-forbidden)',
      (event) => {
        $('*[data-b2b-form-input-holder="true"]').b2bFormInputHolder();

        const $button = $(event.currentTarget);
        const $row = $button.closest(me.defaults.rowSelector);
        const $quantitySelect = $row.find('[data-display="edit-mode"]');
        const $quantityView = $row.find('[data-display="view-mode"]');
        const $commentInput = $row.next('tr');
        const $actualClickElement = $(event.target);

        if ($actualClickElement.hasClass('no--edit')) {
          return;
        }

        $button.attr('disabled', 'disabled');

        $quantitySelect.removeClass('is--hidden');
        $quantityView.addClass('is--hidden');
        $commentInput.removeClass('is--hidden');

        const $spacer = $commentInput.next('[data-display="spacer-mode"]');
        $spacer.remove();

        const quantityInput = $quantitySelect.find('input')[0];
        const submitBtn = $commentInput.find('button[type=submit]')[0];
        quantityInput.focus();

        quantityInput.addEventListener('keypress', (_event) => {
          me.handleEnter(_event, submitBtn);
        });
      },
    );
  },
  handleEnter (event, submitTarget) {
    if (event.keyCode === 13) {
      event.preventDefault();
      submitTarget.click();
    }
  },
};

export default ({
  ...b2bAjaxPanelEditInline,
  ...overrides,
  name: 'b2bAjaxPanelEditInline',
  initOnLoad: true,
  selector: 'body',
});
