import { b2bAjaxPanelUpload, b2bAjaxPanel } from '../../jsPluginBaseObjects';

const overrides = {
  registerGlobalListeners () {
    const me = this;
    const $form = me.$el.find('.b2b--upload-form');

    if (!$form || !me.isAdvancedUpload) {
      return;
    }

    me.$form = $form;
    me.opts.fetchUrl = $form.data('url');
    me.opts.csrfToken = $form.data('csrfToken');


    if (!$form.data('simple-upload')) {
      $form.addClass(me.defaults.advancedUploadCls);

      me._on(me.$el, 'drag dragstart dragend dragover dragenter dragleave drop', $.proxy(me.onDragStopStopEvent, me));
      me._on(me.$el, 'dragover dragenter', $.proxy(me.onDragOverOrEnterMarkFormAsDragOver, me));
      me._on(me.$el, 'dragleave dragend drop', $.proxy(me.onDragLeaveDropSubmitFile, me));
    }

    me._on(me.$el.find(me.defaults.nativeFileInputSelector), 'change', $.proxy(me.onFileInputChangeSubmit, me));
    me._on(me.$el, 'drop', $.proxy(me.onDropSubmitFile, me));
    me._on($form, 'submit', $.proxy(me.confirmSubmitFile, me));
  },

  submitFile (droppedFile) {
    const me = this;
    const formData = new FormData();
    const panelId = me.$form.data('target-panel-id');
    const $ajaxTargetPanel = $(`.b2b--ajax-panel[data-id="${panelId}"]`);

    formData.append('uploadedFile', droppedFile);

    $(`${me.defaults.additionalFormInputsContainer} input`).each(function handleFile () {
      const $input = $(this);
      let value = null;
      if ($input.attr('type') === 'checkbox') {
        value = $input.is(':checked');
      } else {
        value = $input.val();
      }
      formData.append($input.attr('name'), value);
    });

    if (me.opts.csrfToken) {
      formData.append('_csrf_token', me.opts.csrfToken);
    }

    if ($ajaxTargetPanel) {
      $ajaxTargetPanel.trigger(b2bAjaxPanel.defaults.panelBeforeLoadEvent, {
        panel: $ajaxTargetPanel,
      });
    }

    $.ajax({
      url: me.opts.fetchUrl,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      type: 'POST',
      success (data) {
        if (!$ajaxTargetPanel) {
          return;
        }

        const $body = $ajaxTargetPanel.find('.panel--body');

        if ($body.length) {
          $body.html(data);
        } else {
          $ajaxTargetPanel.html(data);
        }

        $ajaxTargetPanel.trigger(b2bAjaxPanel.defaults.panelAfterLoadEvent, {
          panel: $ajaxTargetPanel,
        });

        $.publish('b2b/upload/success', data);
      },
    });
  },

  confirmSubmitFile (event, file) {
    const me = this;
    const $target = $(this.getDropFileTarget(event));
    const $form = $target.closest('form');

    if (!($target.data('confirm') ?? false)) {
      me.submitFile(file);

      return;
    }

    const productCount = $('.table--ordernumber tbody').find('tr').length - 1;

    if (!productCount) {
      me.submitFile(file);

      return;
    }

    me.defaults.activeForm = $form;

    $.ajax({
      url: $target.data('confirm-url'),
      type: 'post',
      data: $form.serialize(),
      success (response) {
        me.showConfirmModal(file, response);
      },
      error () {

      },
    });
  },

  getDropFileTarget (event) {
    return event.currentTarget.classList.contains('b2b--ajax-panel')
      ? event.currentTarget.querySelector('input[type=file]') : event.currentTarget;
  },
};

export default ({
  ...b2bAjaxPanelUpload,
  ...overrides,
  name: 'b2bAjaxPanelUpload',
  initOnLoad: false,
  selector: '',
});
