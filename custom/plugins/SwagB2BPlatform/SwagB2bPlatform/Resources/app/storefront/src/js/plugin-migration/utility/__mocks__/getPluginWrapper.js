export default jest.fn(() => {
    return {
        pluginObjects: {
            ajaxPanel: {
                init: jest.fn()
            }
        }
    }
});