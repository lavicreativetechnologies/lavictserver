/* istanbul ignore file */
/**
 *
 * JS-Plugin objects (containing defaults and methods) should be provided in this directory to secure a
 * Single Point of Truth for plugins migrated from shopware 5 to shopware 6.
 *
 * The goal of this updated plugin structure is to minimize the usage of JQuery
 * and to move towards the shopware 6 plugin class structure.
 *
 * @HowTo: After you registered the plugin object inside this file ("index.js"),
 * import the object as shown in the following examples to use it with the appropriate syntax.
 *
 * > Usage for SW5 plugins ( EcmaScript 5 )
 * -> components/SwagB2bPlatform/Resources/frontend/_public/src/js
 *
 * >>> var b2bAjaxPanel = require('../path/to/../jsPluginBaseObjects');
 * >>> $.plugin('ajaxPanel', b2bAjaxPanel);
 *
 *
 * > Usage in SW6 plugins ( EcmaScript 6+ )
 * -> components/SwagB2bPlatform/Resources/storefront/js/plugin-migration/sw5-plugins
 *
 * >>> import { b2bAjaxPanel } from '../path/to/../jsPluginBaseObjects';
 * >>> export default Object.assign({}, b2bAjaxPanel, {
           name: 'b2bAjaxPanel',
           initOnLoad: true,
           selector: 'body'
       });
 *
 */

import b2bAjaxPanel from './ajax-panel';
import b2bAjaxPanelAclForm from './ajax-panel-acl-form';
import b2bAjaxPanelAclGrid from './ajax-panel-acl-grid';
import b2bAjaxPanelChart from './ajax-panel-chart';
import b2bAjaxPanelDefaultAddress from './ajax-panel-default-address';
import b2bAjaxPanelEditInline from './ajax-panel-edit-inline';
import b2bAjaxPanelFastOrderTable from './ajax-panel-fastorder-table';
import b2bAjaxPanelFormDisable from './ajax-panel-form-disable';
import b2bAjaxPanelFormSelect from './ajax-panel-form-select';
import b2bAjaxPanelLoading from './ajax-panel-loading';
import b2bAjaxPanelModal from './ajax-panel-modal';
import b2bAjaxPanelOrderNumber from './ajax-panel-order-number';
import b2bAjaxPanelPluginLoader from './ajax-panel-plugin-loader';
import b2bAjaxPanelProductSearch from './ajax-panel-product-search';
import b2bAjaxPanelTab from './ajax-panel-tab';
import b2bAjaxPanelTriggerReload from './ajax-panel-trigger-reload';
import b2bAjaxPanelUpload from './ajax-panel-upload';
import b2bAssignmentGrid from './assignment-grid';
import b2bAutoEnableForm from './auto-enable-form';
import b2bAutoSubmit from './auto-submit';
import b2bCollapsePanel from './collapsePanel';
import b2bConfirmBox from './confirm-box';
import b2bContactPasswordActivation from './contactPasswordActivation';
import b2bDatepicker from './datepicker';
import b2bDuplicateElements from './duplicate-elements';
import b2bFormInputHolder from './form-input-holder';
import b2bGridComponent from './grid-component';
import b2bImageZoom from './image-zoom';
import b2bModal from './modal';
import b2bOrderList from './order-list';
import b2bPreloadAnchor from './preloader-anchor';
import b2bQuantityInput from './quantity-input';
import b2bSelectboxReplacement from './selectboxReplacement';
import b2bSyncHeight from './sync-height';
import b2bTab from './tab';
import b2bTree from './tree';
import b2bTreeSelect from './tree-select';

export {
  b2bAjaxPanel,
  b2bAjaxPanelAclForm,
  b2bAjaxPanelAclGrid,
  b2bAjaxPanelChart,
  b2bAjaxPanelDefaultAddress,
  b2bAjaxPanelEditInline,
  b2bAjaxPanelFastOrderTable,
  b2bAjaxPanelFormDisable,
  b2bAjaxPanelFormSelect,
  b2bAjaxPanelLoading,
  b2bAjaxPanelModal,
  b2bAjaxPanelOrderNumber,
  b2bAjaxPanelPluginLoader,
  b2bAjaxPanelProductSearch,
  b2bAjaxPanelTab,
  b2bAjaxPanelTriggerReload,
  b2bAjaxPanelUpload,
  b2bAssignmentGrid,
  b2bAutoEnableForm,
  b2bAutoSubmit,
  b2bCollapsePanel,
  b2bConfirmBox,
  b2bContactPasswordActivation,
  b2bDatepicker,
  b2bDuplicateElements,
  b2bFormInputHolder,
  b2bGridComponent,
  b2bImageZoom,
  b2bModal,
  b2bOrderList,
  b2bPreloadAnchor,
  b2bQuantityInput,
  b2bSelectboxReplacement,
  b2bSyncHeight,
  b2bTab,
  b2bTree,
  b2bTreeSelect,
};
