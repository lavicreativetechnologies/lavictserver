import { getB2bPluginClass } from '../utility';
import { B2BEvents } from '../enums';

export default class AutofocusPlugin extends getB2bPluginClass() {
    public static options = {
        selector_autofocus: '[autofocus]',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe('body', B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelAfterLoadEvent(event): void {
        const panel = event.target;
        const autofocusInput = panel.querySelector(this.options.selector_autofocus);

        if (!autofocusInput) {
            return;
        }

        autofocusInput.focus();
    }
}
