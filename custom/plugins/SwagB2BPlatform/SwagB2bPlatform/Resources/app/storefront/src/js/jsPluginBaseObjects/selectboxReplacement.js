export default {
  defaults: {

    /** @property {String} Basic class name for the wrapper element. */
    baseCls: 'js--fancy-select select-field',

    /** @property {String} The selector for the polyfill check. */
    polyfillSelector: '.js--fancy-select, .select-field',

    /** @property {boolean} Copy all CSS classes to the wrapper element. */
    compatibility: true,
  },
  init() {
    const me = this;

    me.applyDataAttributes(true);

    me.createTemplate();

    return me;
  },
  createTemplate() {
    const me = this;
    const $parent = me.$el.parent(me.opts.polyfillSelector);
    let $wrapEl;

    if ($parent.length > 0) {
      return false;
    }

    $wrapEl = $('<div>', {
      class: me.opts.baseCls,
    });

    if (me.opts.compatibility) {
      $wrapEl.addClass(me.$el.attr('class'));
    }

    me.$wrapEl = me.$el.wrap($wrapEl);

    $.publish('plugin/swSelectboxReplacement/onCreateTemplate', [me, me.$wrapEl]);

    return me.$wrapEl;
  },
};
