import { getStorefrontPlugin } from '../../utility/index.ts';
import { b2bAjaxPanelFastOrderTable } from '../../jsPluginBaseObjects';

const CART_FORM_CLASS = 'cart-form';

const overrides = {
  defaults: {
    errors: {
      productUrl: 'product-url is not defined. Automatic fetching of product name is disabled.',
    },
    moduleSelector: '.module-fastorder',
    tableSelector: '.table--fastorder',
    fastOrderInputContainerSelector: '.fast-order-inputs',
    inputQuantitySelector: '.b2b-table-quantity',
    submitFormSelector: '.cart-form, .order-list-form',
    orderListDropdownSelector: '.b2b--orderlist-dropdown',
    orderListForm: '.order-list-form',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
    selectorMessageContainer: '.b2b--message-container',
    clearMessageTimeoutMs: 3000,
    classFadeOpacityOut: 'fadeOpacity--out',
    animationDuration: 1000,
  },

  registerGlobalListeners () {
    const me = this;

    $.publish('b2b/fastorder/registerGlobalListeners', [me]);

    const module = me.defaults.moduleSelector;

    me._on(module, 'keyup change', 'input', $.proxy(me.onTableInputChange, me));

    me._on(module, 'change', '.input-ordernumber', $.proxy(me.onTableInputOrderNumberChange, me));

    me._on(module, 'change', '.input-quantity', $.proxy(me.onAfterTableInputQuantityChange, me));

    me._on(module, 'submit', me.defaults.submitFormSelector, $.proxy(me.onFormSubmit, me));

    me._on(document, me.defaults.panelAfterLoadEvent, $.proxy(me.onAfterLoadEvent, me));

    $.subscribe(
      me.getEventName('b2b/fastorder/onTableInputOrderNumberChange/ajax/success'),
      $.proxy(me.onTableInputOrderNumberChangeSuccess, me),
    );

    $(window).bind('beforeunload', $.proxy(me.beforeUnload, me));

    $.subscribe('b2b/upload/success', this.onUploadSuccess.bind(this));
  },

  onAfterLoadEvent (event, eventData) {
    if (!eventData.source) {
      return;
    }
    const me = this;

    this.setSubmitState();

    $.publish('b2b/fastorder/onAfterLoadEvent', [me]);

    const isAddToCartSource = eventData.source[0].classList.contains(CART_FORM_CLASS);

    if (eventData.ajaxData.data.length > 1) {
      this.resetOrderListSelect();

      this.resetMessage();

      if (isAddToCartSource) {
        this.openCartOnSuccess(eventData.panel);
      }
    }

    $(me.defaults.inputQuantitySelector).each(() => {
      $(this).val('');
    });
  },

  resetOrderListSelect () {
    const select = this.$el.find(this.defaults.orderListDropdownSelector);
    select.prop('selectedIndex', 0).val();
  },

  openCartOnSuccess (containerPanel) {
    const $panel = $(containerPanel);
    const $messageContainer = $panel.find('.b2b--message-container');

    if (!$messageContainer.find('.b2b--message-success').length) {
      return;
    }

    const offCanvasCart = getStorefrontPlugin('OffCanvasCart');

    if (!offCanvasCart) {
      return;
    }

    offCanvasCart.openOffCanvas(window.router['frontend.cart.offcanvas']);
  },

  onTableInputChange (event) {
    const me = this;
    let $productTable = $(event.target).closest(me.defaults.tableSelector);

    if (!$productTable.length) {
      $productTable = $(this.defaults.tableSelector);
    }

    $.publish('b2b/fastorder/onTableInputChange', [me, $productTable]);

    this.setSubmitState($productTable);

    this.checkProductsForQuantity();
    this.appendNewRow($productTable);
  },

  appendNewRow ($productTable) {
    const $lastRow = $productTable.find('tr:last');

    if (!$lastRow.find('input:first').val().length && !$lastRow.find('input:last').val().length) {
      return;
    }

    const $newRow = $lastRow.clone();
    const rowIndex = $lastRow.data('index');
    const newIndex = rowIndex + 1;

    $newRow.find('b2b--search-results').remove();

    $newRow.find('input:first').attr('name', `products[${newIndex}][referenceNumber]`);
    $newRow.find('input:last').attr('name', `products[${newIndex}][quantity]`);

    const $newRowContent = $newRow.html();

    $productTable.append(`<tr data-index="${newIndex}">${$newRowContent}</tr>`);
  },

  setSubmitState () {
    const $table = $(this.defaults.tableSelector);
    const rows = $table[0].querySelectorAll('tbody tr');

    let hasValidRows = false;

    rows.forEach((row) => {
      const inputReference = row.querySelector('.input-ordernumber');
      const inputQuantity = row.querySelector('.input-quantity');
      if ((!inputReference.value || !inputQuantity.value || inputReference.value === '' || parseInt(inputQuantity.value, 10) <= 0)) {
        return;
      }

      hasValidRows = true;
    });

    const btn = this.$el.find('.cart--link');
    btn.prop('disabled', !hasValidRows);

    if (!document.querySelector('.product-detail-form-container')) {
      const select = this.$el.find('.b2b--orderlist-dropdown');
      select.prop('disabled', !hasValidRows);
    }
  },

  onTableInputOrderNumberChangeSuccess (event, me, data, $input, $row) {
    if (!data.includes('headline-product')) {
      return;
    }

    const $quantity = $row.find('input:last');

    if ($quantity.val() < 1) {
      $quantity.focus();
      const $productTable = $(me.defaults.tableSelector);
      this.setSubmitState($productTable);
    }
  },

  onAfterTableInputQuantityChange (event) {
    const $input = $(event.target);

    if (this.isLastRow($input)) {
      return;
    }

    this._on('body', 'click', () => {
      if (!$input || $input.val() > 0) {
        return;
      }

      $input.parents('tr')[0].remove();
    });
  },

  isLastRow ($el) {
    return $el.parents('tr')[0] === $(this.defaults.tableSelector).find('tr:last')[0];
  },

  onUploadSuccess () {
    this.updateRowIndex();
    this.onTableInputChange({});
  },

  updateRowIndex () {
    const $table = $(this.defaults.tableSelector);

    const rows = $table[0].querySelectorAll('tbody tr');
    rows.forEach((row, index) => {
      const inputReference = row.querySelector('.input-ordernumber');
      const inputQuantity = row.querySelector('.input-quantity');
      const _row = row;

      _row.dataset.index = index;
      inputReference.name = `products[${index}][referenceNumber]`;
      inputQuantity.name = `products[${index}][quantity]`;
    });
  },

  resetMessage () {
    const {
      animationDuration, selectorMessageContainer, clearMessageTimeoutMs, classFadeOpacityOut,
    } = this.defaults;
    clearTimeout(this.clearMessageTimeout);
    const target = document.querySelector(selectorMessageContainer);
    const animationDelay = clearMessageTimeoutMs - animationDuration;

    setTimeout(() => {
      target.style.animationDuration = `${animationDuration}ms`;
      target.classList.add(classFadeOpacityOut);
    }, animationDelay);

    this.clearMessageTimeout = setTimeout(() => {
      if (target && target.parentElement) {
        target.parentElement.removeChild(target);
      }
    }, clearMessageTimeoutMs);
  },
};

export default ({
  ...b2bAjaxPanelFastOrderTable,
  ...overrides,
  name: 'b2bAjaxPanelFastOrderTable',
  initOnLoad: true,
  selector: '.module-fastorder',
});
