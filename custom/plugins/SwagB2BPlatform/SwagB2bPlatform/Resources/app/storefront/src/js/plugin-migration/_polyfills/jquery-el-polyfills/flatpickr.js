import flatpickr from 'flatpickr';

const inputClass = 'form-control';

export default function (config) {
  const $el = this;

  const _config = {
    ...config,
    defaultDate: $el.data('defaultdate'),
    onReady(initialDate, initialDateFormatted, fp) {
      const { mobileInput } = fp;
      if (mobileInput) {
        mobileInput.classList.add(inputClass);
      }
    },
  };

  return flatpickr($el, _config);
}
