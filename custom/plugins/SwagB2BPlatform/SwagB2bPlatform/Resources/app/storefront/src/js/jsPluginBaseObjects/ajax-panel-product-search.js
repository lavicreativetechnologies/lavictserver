/**
 * This plugin allows to use an ajax based product search on any input element inside an ajax panel.
 * To use this plugin you have to pass the plugin name in the data-plugins attribute on the parent ajax panel.
 *
 * Ajax Panel:
 * <div class="b2b--ajax-panel" data-id="example-plugin" data-url="{url}" data-plugins="b2bAjaxProductSearch"></div>
 *
 * Input Inside the Ajax Panel:
 *
 * <div class="b2b--search-container">
 *      <input type="text" name="ordernumber" data-product-search="{url controller=b2bproductsearch action=searchProduct}">
 * </div>
 *
 */
export default {
  defaults: {

    resultsActive: false,

    isLoading: false,

    searchContainer: '.b2b--search-container',

    resultsSelector: '.b2b--search-results',

    inputSelector: 'input.input-ordernumber',

    quantitySelector: '.b2b--search-quantity',

    loadingContainerCls: 'container--element-loader',

    loadingContainerIcon: 'icon--loading-indicator',

    keyMap: {
      down: 40,
      up: 38,
      enter: 13,
      tab: 9,
      left: 37,
      right: 39,
    },

    searchDelay: 300,

    delayTimer: 0,
  },

  init() {
    const me = this;
    const $inputSearch = $(me.defaults.inputSelector);
    this.applyDataAttributes();

    if (!$inputSearch.length) {
      return;
    }

    $(me.defaults.inputSelector).attr('autocomplete', 'off');

    me._on(me.$el, 'keydown', '*[data-product-search]', $.proxy(me.onKeyDown, me));

    me._on(me.$el, 'keyup', '*[data-product-search]', $.proxy(me.onKeyUp, me));

    me._on(me.$el, 'focus', '*[data-product-search]', $.proxy(me.onFocus, me));

    me._on(me.$el, 'click', `${me.defaults.resultsSelector} li`, $.proxy(me.onClickSelectElement, me));

    me._on(me.$el, 'click', 'button[type="submit"]', $.proxy(me.onSubmitForm, me));

    me._on('body', 'click', $.proxy(me.onBodyClick, me));
  },

  loadingEnable(input) {
    const me = this;
    const $input = $(input);
    const $searchContainer = $input.closest(me.defaults.searchContainer);
    const $loadingContainer = $searchContainer.find(`.${me.defaults.loadingContainerCls}`);

    if ($input.data('loading')) {
      return;
    }

    $loadingContainer.remove();
    $input.after(`<div class="${me.defaults.loadingContainerCls}"><i class="${me.defaults.loadingContainerIcon}"></i></div>`);
    $searchContainer.find(`.${me.defaults.loadingContainerCls}`).fadeIn('fast');

    $input.data('loading', true);
    me.defaults.isLoading = true;
  },

  loadingDisable(input) {
    const me = this;
    const $input = $(input);
    const $searchContainer = $input.closest(me.defaults.searchContainer);

    if (!$input.data('loading')) {
      return;
    }

    $searchContainer.find(`.${me.defaults.loadingContainerCls}`).fadeOut('fast', function () {
      $(this).remove();

      $input.data('loading', false);
      me.defaults.isLoading = false;
    });
  },

  onSubmitForm(event) {
    const me = this;

    if (!me.defaults.resultsActive) {
      return;
    }

    event.preventDefault();
    event.stopPropagation();
  },

  onBodyClick(event) {
    const me = this;

    if (!me.defaults.resultsActive) {
      return;
    }

    me.hideResults();
  },

  onKeyDown(event) {
    const me = this;
    const { keyCode } = event;

    if (keyCode === me.defaults.keyMap.tab) {
      me.hideResults();
    }

    if (keyCode === me.defaults.keyMap.enter) {
      me.onSelectElement(event);
    }
  },

  onKeyUp(event) {
    const me = this;
    const input = $(event.currentTarget);
    const term = input.val();
    const { keyCode } = event;

    event.preventDefault();

    if (!term) {
      me.hideResults();
    }

    if (keyCode === me.defaults.keyMap.left || keyCode === me.defaults.keyMap.right) {
      return;
    }

    if (keyCode === me.defaults.keyMap.down && me.defaults.resultsActive) {
      me.onNextElement(event);

      return;
    }

    if (keyCode === me.defaults.keyMap.up && me.defaults.resultsActive) {
      me.onPreviousElement(event);

      return;
    }

    me.searchRequest(term, input);
  },

  onNextElement(event) {
    const me = this;
    const $currentTarget = $(event.currentTarget);
    const $searchContainer = $currentTarget.closest(me.defaults.searchContainer);
    const $resultsContainer = $searchContainer.find(me.defaults.resultsSelector);
    let $currentElement = $searchContainer.find('.is--active');

    if (!$currentElement.length) {
      $resultsContainer.find('li').first().addClass('is--active');

      return;
    }

    $currentElement = $resultsContainer.find('.is--active');
    $currentElement.removeClass('is--active');
    const $nextItem = $currentElement.next('li');
    if(!$nextItem.length) {
      return;
    }

    $nextItem.addClass('is--active');
    $resultsContainer.scrollTop($nextItem[0].offsetTop);
  },

  onPreviousElement(event) {
    const me = this;
    const $currentTarget = $(event.currentTarget);
    const $searchContainer = $currentTarget.closest(me.defaults.searchContainer);
    const $resultsContainer = $searchContainer.find(me.defaults.resultsSelector);
    let $currentElement = $searchContainer.find('.is--active');

    if (!$currentElement.length) {
      $resultsContainer.find('li').last().addClass('is--active');

      return;
    }

    $currentElement = $resultsContainer.find('.is--active');
    $currentElement.removeClass('is--active');
    const $prevItem = $currentElement.prev('li');

    if(!$prevItem.length) {
        return;
    }

    $prevItem.addClass('is--active');
    $resultsContainer.scrollTop($prevItem[0].offsetTop);
  },

  onSelectElement(event) {
    const me = this;
    const $currentTarget = $(event.currentTarget);
    const $searchContainer = $currentTarget.closest(me.defaults.searchContainer);
    const $currentElement = $searchContainer.find('.is--active');

    if (!$currentElement.length || !me.defaults.resultsActive) {
      return;
    }

    event.preventDefault();

    const orderNumber = $currentElement.find('span').html();
    $currentTarget.val(orderNumber);
    $currentTarget.trigger('change');

    me.setQuantityInput($searchContainer, $currentElement);

    me.hideResults();
  },

  onClickSelectElement(event) {
    const me = this;
    const $currentTarget = $(event.currentTarget);
    const $searchContainer = $currentTarget.closest(me.defaults.searchContainer);
    const $input = $searchContainer.find('input');

    if (!$currentTarget.length) {
      return;
    }

    const orderNumber = $currentTarget.find('span').html();
    $input.val(orderNumber);
    $input.trigger('change');

    me.setQuantityInput($searchContainer, $currentTarget);

    me.hideResults();
  },

  onFocus(event) {
    const me = this;
    const $searchInput = $(event.currentTarget);
    const term = $searchInput.val();

    me.searchRequest(term, $searchInput);
  },

  setQuantityInput($searchContainer, $currentElement) {
    const me = this;
    let $quantityInput = $searchContainer.closest('tr').find(me.defaults.quantitySelector);

    if (!$quantityInput.length) {
      $quantityInput = $searchContainer.closest('form').find(me.defaults.quantitySelector);
    }

    if ($quantityInput.length) {
      $quantityInput.attr({
        max: $currentElement.data('max'),
        min: $currentElement.data('min'),
        step: $currentElement.data('step'),
      });

      if (!$currentElement.data('max')) {
        $quantityInput.removeAttr('max');
      }
    }
  },

  removeQuantityInput(input) {
    const me = this;
    const $searchContainer = input.closest(me.defaults.searchContainer);
    const $quantityInput = $searchContainer.closest('tr').find(me.defaults.quantitySelector);

    if ($quantityInput.length) {
      $quantityInput.attr('min', 1);
      $quantityInput.removeAttr('max');
      $quantityInput.removeAttr('step');
    }
  },

  searchRequest(term, input) {
    const me = this;
    const searchUrl = input.data('product-search');

    me.removeQuantityInput(input);

    if (term.length) {
      me.loadingEnable(input);

      clearTimeout(me.defaults.delayTimer);
      me.defaults.delayTimer = setTimeout(() => {
        $.ajax({
          type: 'get',
          url: searchUrl,
          data: {
            term,
          },
          success(resultTemplate) {
            me.showResults(input, resultTemplate);
            me.loadingDisable(input);
          },
          error() {
            me.loadingDisable(input);
          },
        });
      }, me.defaults.searchDelay);
    } else {
      me.loadingDisable(input);
      me.hideResults(input);
    }
  },

  showResults(input, resultTemplate) {
    const me = this;

    if (me.defaults.resultsActive) {
      me.updateResults(input, resultTemplate);

      return;
    }

    me.defaults.resultsActive = true;

    input.after(`<div class="b2b--search-results">${resultTemplate}</div>`);
  },

  updateResults(input, resultTemplate) {
    const $searchContainer = input.closest('.b2b--search-container');
    const $searchResults = $searchContainer.find('.b2b--search-results');

    $searchResults.html(resultTemplate);
  },

  hideResults() {
    const me = this;

    me.defaults.resultsActive = false;
    $('.b2b--search-results').remove();
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
