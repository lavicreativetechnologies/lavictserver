import { b2bAutoEnableForm } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAutoEnableForm,
  name: 'b2bAutoEnableForm',
  initOnLoad: true,
  selector: 'body',
});
