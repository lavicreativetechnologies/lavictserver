import { b2bAjaxPanelAclGrid } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelAclGrid,
  name: 'b2bAjaxPanelAclGrid',
  initOnLoad: true,
  selector: 'body',
});
