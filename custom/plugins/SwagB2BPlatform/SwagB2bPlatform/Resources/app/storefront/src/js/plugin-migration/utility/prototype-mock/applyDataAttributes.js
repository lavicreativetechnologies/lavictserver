const numberRegex = /^-?\d*\.?\d*$/;
const objectRegex = /^[[{]/;

function deserializeValue(value) {
  /* eslint-disable no-nested-ternary */
  return !value ? value : value === 'true' || (
    value === 'false' ? false
      : value === 'null' ? null
        : numberRegex.test(value) ? +value
          : objectRegex.test(value) ? JSON.parse(value)
            : value
  );
  /* eslint-enable no-nested-ternary */
}


function applyDataAttributes(shouldDeserialize = false, ignoreList = []) {
  const me = this;

  Object.keys(me.opts).forEach((key) => {
    const attr = me.$el.attr(`data-${key}`);

    if (ignoreList.indexOf(key) === -1 && typeof attr !== 'undefined') {
      me.opts[key] = shouldDeserialize ? deserializeValue(attr) : attr;
    }
  });

  return me.opts;
}

export {
  applyDataAttributes,
  deserializeValue,
};
