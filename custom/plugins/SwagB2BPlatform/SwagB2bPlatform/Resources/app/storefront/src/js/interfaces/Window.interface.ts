import PluginBaseClassInterface from './PluginBaseClass.interface';

// todo: Add PluginBaseClass to this interface as soon as its available globally

declare global {
    interface Window {
        PluginManager: {
            register: (pluginName: string, pluginClass: any, selector?: string | NodeList | HTMLElement) => void;
            getPlugin: (pluginName: string) => PluginBaseClassInterface;
            initializePlugin: (pluginName: string, selector: string | NodeList | HTMLElement, options: object) => void;
            initializePlugins: () => void;
        },
        PluginBaseClass: any,
        Chart: any
    }
}

export {};
