import { b2bTab } from '../../jsPluginBaseObjects';

export default ({
  ...b2bTab,
  name: 'b2bTab',
  initOnLoad: true,
  selector: 'body',
});
