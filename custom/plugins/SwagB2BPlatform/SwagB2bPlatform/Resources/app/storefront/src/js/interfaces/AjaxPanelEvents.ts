declare global {
    interface AjaxPanelEventData {
        panel: HTMLElement;
        source: HTMLElement;
        ajaxData: any;
    }
}

export {};
