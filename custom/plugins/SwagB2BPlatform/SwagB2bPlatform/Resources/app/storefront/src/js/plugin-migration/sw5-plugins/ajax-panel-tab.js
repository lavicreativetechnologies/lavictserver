import { b2bAjaxPanelTab } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelTab,
  name: 'b2bAjaxPanelTab',
  initOnLoad: true,
  selector: 'body',
});
