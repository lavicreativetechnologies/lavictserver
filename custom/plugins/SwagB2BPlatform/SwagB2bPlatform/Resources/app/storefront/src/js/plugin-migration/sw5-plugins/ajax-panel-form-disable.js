import { b2bAjaxPanelFormDisable } from '../../jsPluginBaseObjects';

export default ({
  ...b2bAjaxPanelFormDisable,
  name: 'b2bAjaxPanelFormDisable',
  initOnLoad: true,
  selector: 'body',
});
