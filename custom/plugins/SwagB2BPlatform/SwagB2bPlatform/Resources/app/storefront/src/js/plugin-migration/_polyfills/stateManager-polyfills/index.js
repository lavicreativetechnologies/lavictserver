import addPlugin from './addPlugin';
import { on, off } from './eventHandler';
import updatePlugin from './updatePlugin';

/**
 * StateManager.registerListener() polyfill
 * @deprecated
 */
const registerListener = () => {};

export {
  addPlugin,
  off,
  on,
  registerListener,
  updatePlugin,
};
