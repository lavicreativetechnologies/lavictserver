/**
 * starts and destroys plugins on 'b2b--ajax-panel_loaded'. Just add a data attribute to the panel
 *
 * <div [...] data-plugins="b2bGridComponent,b2bAssignmentGrid">
 *
 * translates to:
 *
 * $('the_panel').b2bGridComponent()
 * $('the_panel').b2bAssignmentGrid()
 *
 */
export default {
  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        me.eachPluginName(eventData, (pluginName, $panel) => {
          me.destroyPlugin($panel, pluginName);
        });
      },
    );

    me._on(
      document,
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        me.eachPluginName(eventData, (pluginName, $panel) => {
          $panel[pluginName]();
        });
      },
    );
  },

  eachPluginName(eventData, callback) {
    const $panel = $(eventData.panel);
    let pluginNames = $panel.data('plugins');

    if (!pluginNames) {
      return;
    }

    pluginNames = pluginNames.split(',');

    for (let i = 0; i < pluginNames.length; i++) {
      callback(pluginNames[i], $panel);
    }
  },

  destroyPlugin(selector, pluginName) {
    const $el = (typeof selector === 'string') ? $(selector) : selector;
    const name = `plugin_${pluginName}`;
    const len = $el.length;
    let i = 0;
    let $currentEl;
    let plugin;

    if (!len) {
      return;
    }

    for (; i < len; i++) {
      $currentEl = $($el[i]);

      plugin = $currentEl.data(name);

      if (plugin) {
        plugin.destroy();
        $currentEl.removeData(name);
      }
    }
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
