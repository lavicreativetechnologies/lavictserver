module.exports = {
  preset: 'ts-jest',
  transform: {
    '^.+\\.(ts|js)$': 'ts-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  reporters: [
    'default', [
      'jest-junit',
      {
        'suiteName': 'platform storefront jest',
        'outputDirectory': '../../../../../build/artifacts/',
        'outputName': 'platform-storefront-jest.xml',
        'uniqueOutputName': 'false'
      }
    ]
  ],
  testTimeout: 10000,
  setupFilesAfterEnv: ['./tests/setupTests.js'],
  moduleNameMapper: {
    '^PluginMigration(.*)$': '<rootDir>/src/js/plugin-migration',
  },
  errorOnDeprecated: true,
  displayName: {
    name: 'B2B Platform Storefront',
    color: 'lime'
  },
  collectCoverage: true,
  collectCoverageFrom: ['src/js/**'],
  coverageDirectory: '../../../../../build/artifacts/platform-storefront-jest',
  coveragePathIgnorePatterns: [
    '<rootDir>/src/js/compatibility',
    '<rootDir>/src/js/enums',
    '<rootDir>/src/js/interfaces'
  ],
  timers: 'fake'
};
