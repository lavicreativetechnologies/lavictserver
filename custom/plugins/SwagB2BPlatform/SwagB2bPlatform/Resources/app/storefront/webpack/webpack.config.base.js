const pathHelper = require('./helper/pathHelper');
const path = require('path');

const { getOutputPath, getEslintrcPath } = pathHelper;
module.exports = {
    entry: './src/js/index.ts',
    output: {
        path: getOutputPath(),
        filename: 'SwagB2bPlatform.js'
    },
    resolve: {
        extensions: ['.js', '.ts'],
        alias: {
          PluginMigration: path.resolve(__dirname, '../src/js/plugin-migration')
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.ts$/,
                loader: 'eslint-loader',
                enforce: 'pre',
                options: {
                  configFile: getEslintrcPath('typescript'),
                  fix: true
                }
            },
            {
                test: /\.js$/,
                loader: 'eslint-loader',
                enforce: 'pre',
                options: {
                  configFile: getEslintrcPath('javascript'),
                  fix: true
                }
            },
            {
                test: /\.(svg|png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    }
                ]
            }
        ]
    }
};
