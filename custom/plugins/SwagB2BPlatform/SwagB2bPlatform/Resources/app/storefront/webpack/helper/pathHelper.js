const path = require('path');

const getOutputPath = () => {
    return path.join(__dirname, '../../dist/storefront/js');
};

const getEslintrcPath = (type) => {
    return path.join(__dirname, `../../.eslintrc-${type}.js`);
};

module.exports = {
    getOutputPath,
    getEslintrcPath
}
