module.exports = {
    root: true,
    parser: 'babel-eslint',
    extends: [
        'airbnb-base'
    ],
    env: {
        browser: true,
        jquery: true
    },
    parserOptions: {
        ecmaVersion: 2019,
        sourceType: 'module'
    },
    globals: {
        B2bPluginWrapper: 'readonly'
    },
    rules: {
        'curly': [2, 'all'],
        'brace-style': [1, '1tbs' , {
            'allowSingleLine': false
        }],
        'no-underscore-dangle': 0,
        'class-methods-use-this': 0,
        'import/prefer-default-export': 0,
        'no-plusplus': 0,
        'newline-before-return': 1,
        'import/no-unresolved': 0,
        'no-param-reassign': 0,
        'max-len': 0,
        'import/extensions': 0,
        'quotes': ['warn', 'single'],
        'indent': ['warn', 2, {
            'SwitchCase': 1,
        }],
        'space-before-function-paren': ['error', 'always']
    }
};
