<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Offer\Admin\DependencyInjection\OfferAdminConfiguration;
use Shopware\B2B\SalesRepresentative\Admin\DependencyInjection\SalesRepresentativeAdminConfiguration;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SwagB2bPlatformAdministrationConfiguration extends DependencyInjectionConfiguration
{
    /**
     * @return string[]
     */
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/../config/services.xml',
        ];
    }

    /**
     * @return CompilerPassInterface[]
     */
    public function getCompilerPasses(): array
    {
        return [];
    }

    /**
     * @return DependencyInjectionConfiguration[]
     */
    public function getDependingConfigurations(): array
    {
        return [
            new SalesRepresentativeAdminConfiguration(),
            new OfferAdminConfiguration(),
        ];
    }
}
