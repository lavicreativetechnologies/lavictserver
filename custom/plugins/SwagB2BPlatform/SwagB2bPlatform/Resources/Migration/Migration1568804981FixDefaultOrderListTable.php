<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1568804981FixDefaultOrderListTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1568804981;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            DELETE FROM `b2b_default_order_list`
            WHERE `entity_id` IS NULL OR `order_list_id` IS NULL;
        ');
        $connection->exec('
            DELETE FROM `b2b_default_order_list`
            WHERE `order_list_id` NOT IN (
                SELECT `id` FROM `b2b_order_list`
            );
        ');
        $connection->exec('
            ALTER TABLE `b2b_default_order_list` 
            CHANGE COLUMN `entity_id` `identity_id` VARCHAR(255) NOT NULL;
        ');
        $connection->exec('
            ALTER TABLE `b2b_default_order_list` 
            MODIFY `order_list_id` INTEGER NOT NULL;
        ');
        $connection->exec('
            ALTER TABLE `b2b_default_order_list` 
            ADD PRIMARY KEY (`order_list_id`, `identity_id`);
        ');
        $connection->exec('
            ALTER TABLE `b2b_default_order_list` 
            ADD CONSTRAINT `FK_b2b_default_order_list_b2b_order_list` FOREIGN KEY (`order_list_id`) REFERENCES `b2b_order_list`(`id`)
                ON UPDATE CASCADE ON DELETE CASCADE;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
