<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1517833946OrderNumber implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1517833946;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec(
            '
            CREATE TABLE b2b_order_number
            (
              id INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
              custom_ordernumber VARCHAR(255) NOT NULL,
              product_id BINARY(16) NOT NULL,
              context_owner_id INT(11) NOT NULL,
              CONSTRAINT FK_b2b_product_number_product_id FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE,    
              CONSTRAINT FK_b2b_order_number_context_owner_id FOREIGN KEY (context_owner_id) REFERENCES b2b_store_front_auth (id) ON DELETE CASCADE
            );'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
