<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Budget\BridgePlatform\BudgetNotificationMailEvent;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagB2bPlatform\SetUp;
use Symfony\Component\DependencyInjection\Container;
use function array_merge;
use function file_get_contents;

class Migration1493892870BudgetMail implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1493892870;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->query('
            CREATE TABLE `b2b_budget_notify` (
                `budget_id` INT(11) NOT NULL,
                `refresh_group` INT(11) NOT NULL,
                `time` DATETIME NOT NULL,

                PRIMARY KEY (`budget_id`, `refresh_group`),
                INDEX `FK_b2b_budget_notify_b2b_budget` (`budget_id`),

                CONSTRAINT `FK_b2b_budget_notify_b2b_budget` FOREIGN KEY (`budget_id`)
                  REFERENCES `b2b_budget` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        $templateId = IdValue::create(Uuid::randomHex());
        $templateTypeId = IdValue::create(Uuid::randomHex());

        $this->addMailTemplate($container, $templateTypeId, $templateId);
        $this->addEventAction($container, $templateTypeId, $templateId);
    }

    public function addMailTemplate(Container $container, IdValue $templateTypeId, IdValue $templateId): void
    {
        $fixturePath = __DIR__ . '/FileFixtures/Budget/';

        $translations = [
            'de-DE' => [
                'senderName' => '{{ salesChannel.name }}',
                'subject' => 'Budget ({{ budget.name }}) hat die angegebene Prozentzahl erreicht',
                'contentPlain' => file_get_contents($fixturePath . 'plain_de.twig'),
                'contentHtml' => file_get_contents($fixturePath . 'html_de.twig'),
            ],
            SetUp::DEFAULT_LANGUAGE_CODE => [
                'senderName' => '{{ salesChannel.name }}',
                'subject' => 'Budget ({{ budget.name }}) notify percentage reached',
                'contentPlain' => file_get_contents($fixturePath . 'plain_en.twig'),
                'contentHtml' => file_get_contents($fixturePath . 'html_en.twig'),
            ],
        ];

        $nameTranslation = [
            'de-DE' => [
                'name' => 'B2B - Budget Benachrichtigung',
            ],
            SetUp::DEFAULT_LANGUAGE_CODE => [
                'name' => 'B2B - Budget notification',
            ],
        ];

        $mailTemplateRepository = $container->get('mail_template.repository');
        $context = Context::createDefaultContext();

        $mailTemplateData = array_merge([
            'id' => $templateId->getValue(),
            'systemDefault' => false,
            'mailTemplateType' => array_merge([
                'id' => $templateTypeId->getValue(),
                'technicalName' => 'b2bBudgetNotify',
                'translations' => $nameTranslation,
            ], $nameTranslation[SetUp::DEFAULT_LANGUAGE_CODE]),
            'translations' => $translations,
        ], $translations[SetUp::DEFAULT_LANGUAGE_CODE]);

        $mailTemplateRepository->create([$mailTemplateData], $context);
    }

    public function addEventAction(Container $container, IdValue $templateTypeId, IdValue $templateId): void
    {
        $repository = $container->get('event_action.repository');
        $context = Context::createDefaultContext();

        $repository->create([
            [
                'eventName' => BudgetNotificationMailEvent::EVENT_NAME,
                'actionName' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
                'config' => [
                    'mail_template_type_id' => $templateTypeId->getValue(),
                    'mail_template_id' => $templateId->getValue(),
                ],
            ],
        ], $context);
    }
}
