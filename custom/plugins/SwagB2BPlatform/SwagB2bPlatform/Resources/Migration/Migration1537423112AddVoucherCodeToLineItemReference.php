<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1537423112AddVoucherCodeToLineItemReference implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1537423112;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_line_item_reference
            ADD voucher_code VARCHAR (100)
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
