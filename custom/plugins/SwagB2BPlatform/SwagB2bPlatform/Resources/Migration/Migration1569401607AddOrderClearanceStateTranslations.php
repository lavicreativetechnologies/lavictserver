<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use DateTime;
use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;
use function array_key_exists;

class Migration1569401607AddOrderClearanceStateTranslations implements MigrationStepInterface
{
    private const TABLE_NAME = 'state_machine_state_translation';

    private const ORDER_CLEARANCE_OPEN_TRANSLATION = [
        'English' => 'Order clearance open',
        'Deutsch' => 'Freigabe offen',
    ];

    private const ORDER_CLEARANCE_DENIED_TRANSLATION = [
        'English' => 'Order clearance denied',
        'Deutsch' => 'Freigabe abgelehnt',
    ];

    /**
     * @var Connection
     */
    private $connection;

    public function getCreationTimeStamp(): int
    {
        return 1569401607;
    }

    public function updateDatabase(Connection $connection): void
    {
        // nth
    }

    public function updateThroughServices(Container $container): void
    {
        $this->connection = $container->get(Connection::class);
        $languages = $this->fetchLanguageIdsAndNames();

        $orderClearanceOpenData = $this->fetchStateDataByTechnicalName('orderclearance_open');
        $orderClearanceDeniedData = $this->fetchStateDataByTechnicalName('orderclearance_denied');

        foreach ($languages as $name => $id) {
            if (!array_key_exists($name, self::ORDER_CLEARANCE_OPEN_TRANSLATION)) {
                continue;
            }

            $this->connection->insert(
                self::TABLE_NAME,
                [
                    'language_id' => $id,
                    'state_machine_state_id' => $orderClearanceOpenData['id'],
                    'name' => self::ORDER_CLEARANCE_OPEN_TRANSLATION[$name],
                    'created_at' => (new DateTime())->format('Y-m-d H:i:s'),
                ]
            );

            if (!array_key_exists($name, self::ORDER_CLEARANCE_DENIED_TRANSLATION)) {
                continue;
            }

            $this->connection->insert(
                self::TABLE_NAME,
                [
                    'language_id' => $id,
                    'state_machine_state_id' => $orderClearanceDeniedData['id'],
                    'name' => self::ORDER_CLEARANCE_DENIED_TRANSLATION[$name],
                    'created_at' => (new DateTime())->format('Y-m-d H:i:s'),
                ]
            );
        }
    }

    /**
     * @internal
     */
    protected function fetchStateDataByTechnicalName(string $name): array
    {
        return $this->connection->createQueryBuilder()
            ->select('*')
            ->from('state_machine_state')
            ->where('technical_name = :name')
            ->setParameter('name', $name)
            ->execute()
            ->fetch();
    }

    /**
     * @internal
     */
    protected function fetchLanguageIdsAndNames(): array
    {
        $languageData = $this->connection->fetchAll('SELECT `id`, `name` FROM `language`');

        $data = [];
        foreach ($languageData as $datum) {
            $data[$datum['name']] = $datum['id'];
        }

        return $data;
    }
}
