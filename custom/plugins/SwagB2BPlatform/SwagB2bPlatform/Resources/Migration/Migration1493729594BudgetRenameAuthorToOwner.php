<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1493729594BudgetRenameAuthorToOwner implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1493729594;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_budget
                DROP FOREIGN KEY FK_b2b_budget_b2b_store_front_auth_2;');
        $connection->exec('
            ALTER TABLE `b2b_budget`
                CHANGE COLUMN `author_id` `owner_id` INT(11) NULL DEFAULT NULL;
        ');
        $connection->exec('
            ALTER TABLE b2b_budget
                ADD FOREIGN KEY FK_b2b_budget_b2b_store_front_auth_owner (owner_id) REFERENCES  b2b_store_front_auth (id);
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
