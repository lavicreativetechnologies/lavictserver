<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1493479533InstallPrices implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1493479533;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->query('
            CREATE TABLE `b2b_prices` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `debtor_id` BINARY(16) NOT NULL,
              `price` double NOT NULL,
              `from` int(11) NOT NULL,
              `to` int(11) DEFAULT NULL,
              `product_id` BINARY(16) NOT NULL,
            
              PRIMARY KEY (`id`),
            
              UNIQUE INDEX `b2b_debtor_from_to_article_idx` (`debtor_id`, `from`, `to`, `product_id`),
              INDEX `b2b_prices_product_idx` (`product_id`),
              INDEX `b2b_prices_debtor_idx` (`debtor_id`),
              CONSTRAINT `FK_prices_debtor_id` FOREIGN KEY (`debtor_id`)
                REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `FK_prices_product_id` FOREIGN KEY (`product_id`)
                REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
