<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1597746496AddBudgetOwnerIdOnDeleteNull implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1597746496;
    }

    public function updateDatabase(Connection $connection): void
    {
        $constraintName = $connection->fetchColumn('
            SELECT CONSTRAINT_NAME
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE TABLE_NAME =  \'b2b_budget\'
            AND COLUMN_NAME =  \'owner_id\'
            AND TABLE_SCHEMA = \'' . $connection->getDatabase() . '\'
            LIMIT 1;
        ');

        $connection->exec('ALTER TABLE b2b_budget DROP FOREIGN KEY ' . $constraintName);
        $connection->exec('
            ALTER TABLE b2b_budget
            ADD CONSTRAINT FK_b2b_budget_b2b_store_front_auth_owner
                FOREIGN KEY (owner_id)
                REFERENCES  b2b_store_front_auth (id)
                ON DELETE SET NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
