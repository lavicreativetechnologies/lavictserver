<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1501061271AddShippingAmount implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1501061271;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_order_context`
              ADD COLUMN `shipping_amount` DOUBLE NOT NULL DEFAULT 0,
              ADD COLUMN `shipping_amount_net` DOUBLE NOT NULL DEFAULT 0;
        ');

        // TODO: Add net price?
        $connection->exec('
            UPDATE `b2b_order_context`
            INNER JOIN `order` 
            ON `order`.`order_number` = `b2b_order_context`.`ordernumber`
            INNER JOIN `order_delivery`
            ON `order`.`id` = `order_delivery`.`order_id`
            SET `b2b_order_context`.`shipping_amount` = JSON_EXTRACT(`order_delivery`.`shipping_costs`, \'$.totalPrice\'),
                `b2b_order_context`.`shipping_amount_net` = JSON_EXTRACT(`order_delivery`.`shipping_costs`, \'$.totalPrice\');
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
