<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1589296378AddCreatedAtAndUpdatedAtToOrderNumberTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1589296378;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_order_number
            ADD COLUMN `updated_at` DATETIME(3) NULL,
            ADD COLUMN `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
