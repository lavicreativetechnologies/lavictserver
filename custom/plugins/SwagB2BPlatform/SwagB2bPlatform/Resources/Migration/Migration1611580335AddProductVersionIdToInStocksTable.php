<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Core\Defaults;
use Symfony\Component\DependencyInjection\Container;

class Migration1611580335AddProductVersionIdToInStocksTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1611580335;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_in_stocks`
                ADD COLUMN `product_version_id` BINARY(16)
                    DEFAULT 0x' . Defaults::LIVE_VERSION . '
                    NOT NULL
                    AFTER `product_id`;
        ');

        $connection->exec('
            ALTER TABLE `b2b_in_stocks`
                ALTER COLUMN `product_version_id` DROP DEFAULT;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
