<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Acl\Framework\AclDdlService;
use Shopware\B2B\Address\Framework\DependencyInjection\AddressFrameworkConfiguration;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1493482191InstallAddressFlag implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1493482191;
    }

    public function updateDatabase(Connection $connection): void
    {
    }

    public function updateThroughServices(Container $container): void
    {
        foreach (AddressFrameworkConfiguration::createAclTables() as $table) {
            AclDdlService::create($container)->createTable($table);
        }
    }
}
