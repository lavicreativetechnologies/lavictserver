<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1561617283AddPasswordActivationTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1561617283;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE b2b_contact_password_activation (
                id INT(11) NOT NULL AUTO_INCREMENT,
                valid_until DATETIME NOT NULL,
                hash VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
                contact_id INT(11) NOT NULL,
                PRIMARY KEY (id),
                CONSTRAINT `b2b_contact_id_FK` FOREIGN KEY (`contact_id`) 
                 REFERENCES `b2b_debtor_contact` (`id`) ON DELETE CASCADE,
                UNIQUE KEY hash (hash),
                INDEX valid_until (valid_until)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
            ');
    }

    public function updateThroughServices(Container $container): void
    {
        //nth
    }
}
