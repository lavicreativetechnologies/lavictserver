<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1584358196AddCreatedAtAndUpdatedAtToInStockTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1584358196;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_in_stocks
            ADD COLUMN `updated_at` DATETIME(3) NULL,
            ADD COLUMN `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
