<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1519714409ChangeAuditLogAuthor implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1519714409;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
ALTER TABLE b2b_audit_log_author
  ADD `is_backend`  TINYINT(1) NOT NULL DEFAULT 0;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
