<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1588227074UnifyTableCollate implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1588227074;
    }

    public function updateDatabase(Connection $connection): void
    {
        $this->lockTableAndDisableFkChecks($connection);
        $this->alterTables($connection);
        $this->enableFkChecksAndUnlockTables($connection);
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }

    /**
     * @internal
     */
    protected function lockTableAndDisableFkChecks(Connection $connection): void
    {
        $connection->exec('LOCK TABLES  b2b_acl_contact_address WRITE,
                                        b2b_acl_contact_budget WRITE,
                                        b2b_acl_contact_contact WRITE,
                                        b2b_acl_contact_contingent_group WRITE,
                                        b2b_acl_contact_order_list WRITE,
                                        b2b_acl_contact_role WRITE,
                                        b2b_acl_contact_route_privilege WRITE,
                                        b2b_acl_role_address WRITE,
                                        b2b_acl_role_budget WRITE,
                                        b2b_acl_role_contact WRITE,
                                        b2b_acl_role_contingent_group WRITE,
                                        b2b_acl_role_order_list WRITE,
                                        b2b_acl_role_role WRITE,
                                        b2b_acl_role_route_privilege WRITE,
                                        b2b_acl_route WRITE,
                                        b2b_acl_route_privilege WRITE,
                                        b2b_audit_log WRITE,
                                        b2b_audit_log_author WRITE,
                                        b2b_audit_log_index WRITE,
                                        b2b_budget WRITE,
                                        b2b_budget_address WRITE,
                                        b2b_budget_notify WRITE,
                                        b2b_budget_transaction WRITE,
                                        b2b_contact_contingent_group WRITE,
                                        b2b_contact_password_activation WRITE,
                                        b2b_contingent_group WRITE,
                                        b2b_contingent_group_rule WRITE,
                                        b2b_contingent_group_rule_category WRITE,
                                        b2b_contingent_group_rule_product_number_allowance WRITE,
                                        b2b_contingent_group_rule_product_order_number WRITE,
                                        b2b_contingent_group_rule_product_price WRITE,
                                        b2b_contingent_group_rule_time_restriction WRITE,
                                        b2b_customer_address_data WRITE,
                                        b2b_customer_data WRITE,
                                        b2b_debtor_contact WRITE,
                                        b2b_default_order_list WRITE,
                                        b2b_in_stocks WRITE,
                                        b2b_line_item_list WRITE,
                                        b2b_line_item_reference WRITE,
                                        b2b_migration WRITE,
                                        b2b_offer WRITE,
                                        b2b_order_context WRITE,
                                        b2b_order_list WRITE,
                                        b2b_order_number WRITE,
                                        b2b_role WRITE,
                                        b2b_role_contact WRITE,
                                        b2b_role_contingent_group WRITE,
                                        b2b_sales_representative_clients WRITE,
                                        b2b_sales_representative_orders WRITE,
                                        b2b_store_front_auth WRITE
;');
        $connection->exec('SET FOREIGN_KEY_CHECKS = 0;');
    }

    /**
     * @internal
     */
    protected function alterTables(Connection $connection): void
    {
        $connection->exec('ALTER TABLE b2b_acl_contact_address CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_contact_budget CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_contact_contact CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_contact_contingent_group CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_contact_order_list CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_contact_role CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_contact_route_privilege CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_address CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_budget CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_contact CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_contingent_group CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_order_list CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_role CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_role_route_privilege CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_route CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_acl_route_privilege CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_audit_log CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_audit_log_author CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_audit_log_index CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_budget CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_budget_address CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_budget_notify CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_budget_transaction CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contact_contingent_group CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contact_password_activation CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group_rule CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group_rule_category CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group_rule_product_number_allowance CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group_rule_product_order_number CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group_rule_product_price CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_contingent_group_rule_time_restriction CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_customer_address_data CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_customer_data CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_debtor_contact CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_default_order_list CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_in_stocks CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_line_item_list CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_line_item_reference CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_migration CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_offer CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_order_context CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_order_list CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_order_number CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_role CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_role_contact CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_role_contingent_group CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_sales_representative_clients CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_sales_representative_orders CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
        $connection->exec('ALTER TABLE b2b_store_front_auth CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;');
    }

    /**
     * @internal
     */
    protected function enableFkChecksAndUnlockTables(Connection $connection): void
    {
        $connection->exec('SET FOREIGN_KEY_CHECKS = 1;');
        $connection->exec('UNLOCK TABLES;');
    }
}
