<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\B2B\OrderClearance\BridgePlatform\OrderClearanceAcceptedMailEvent;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagB2bPlatform\SetUp;
use Symfony\Component\DependencyInjection\Container;
use function array_merge;
use function file_get_contents;

class Migration1539090141AddAcceptedOrderClearanceToMail implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1539090141;
    }

    public function updateDatabase(Connection $connection): void
    {
        // nth
    }

    public function updateThroughServices(Container $container): void
    {
        $templateId = IdValue::create(Uuid::randomHex());
        $mailTemplateTypeId = IdValue::create(Uuid::randomHex());

        $this->addMailTemplate($container, $mailTemplateTypeId, $templateId);
        $this->addEventAction($container, $mailTemplateTypeId, $templateId);
    }

    public function addMailTemplate(Container $container, IdValue $mailTemplateTypeId, IdValue $templateId): void
    {
        $fixturePath = __DIR__ . '/FileFixtures/OrderClearance/';
        $translations = [
            'de-DE' => [
                'senderName' => '{{ salesChannel.name }}',
                'subject' => 'Ihre Bestellung bei {{ salesChannel.name }} wurde akzeptiert!',
                'contentPlain' => file_get_contents($fixturePath . 'plain_de.twig'),
                'contentHtml' => file_get_contents($fixturePath . 'html_de.twig'),
            ],
            SetUp::DEFAULT_LANGUAGE_CODE => [
                'senderName' => '{{ salesChannel.name }}',
                'subject' => 'Your order at {{ salesChannel.name }} has been accepted!',
                'contentPlain' => file_get_contents($fixturePath . 'plain_en.twig'),
                'contentHtml' => file_get_contents($fixturePath . 'html_en.twig'),
            ],
        ];

        $nameTranslations = [
            'de-DE' => [
                'name' => 'B2B - Bestellungsfreigabe akzeptiert',
            ],
            SetUp::DEFAULT_LANGUAGE_CODE => [
                'name' => 'B2B - Orderclearance accpeted',
            ],
        ];

        $mailTemplateRepository = $container->get('mail_template.repository');
        $context = Context::createDefaultContext();

        $mailTemplateData = array_merge([
            'id' => $templateId->getValue(),
            'systemDefault' => false,
            'mailTemplateType' => array_merge([
                'id' => $mailTemplateTypeId->getValue(),
                'technicalName' => 'b2bOrderClearanceAccepted',
                'translations' => $nameTranslations,
            ], $nameTranslations[SetUp::DEFAULT_LANGUAGE_CODE]),
            'translations' => $translations,
        ], $translations[SetUp::DEFAULT_LANGUAGE_CODE]);

        $mailTemplateRepository->create([$mailTemplateData], $context);
    }

    public function addEventAction(Container $container, IdValue $templateTypeId, IdValue $templateId): void
    {
        $repository = $container->get('event_action.repository');
        $context = Context::createDefaultContext();

        $repository->create([
            [
                'eventName' => OrderClearanceAcceptedMailEvent::EVENT_NAME,
                'actionName' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
                'config' => [
                    'mail_template_type_id' => $templateTypeId->getValue(),
                    'mail_template_id' => $templateId->getValue(),
                ],
            ],
        ], $context);
    }
}
