<?php declare(strict_types=1);

namespace SwagB2bPlatform\Controller;

use Shopware\B2B\Common\MvcExtension\Request;

class B2bConfirmController
{
    public function indexAction(Request $request): array
    {
        return [];
    }

    public function removeAction(Request $request): array
    {
        return [
            'confirmName' => $request->getParam('confirmName'),
        ];
    }

    public function errorAction(Request $request): array
    {
        return [
            'variables' => $request->getPost(),
        ];
    }

    public function overrideAction(Request $request): array
    {
        return [];
    }
}
