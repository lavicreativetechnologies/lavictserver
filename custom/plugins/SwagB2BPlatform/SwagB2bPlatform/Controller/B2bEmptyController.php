<?php declare(strict_types=1);

namespace SwagB2bPlatform\Controller;

use Shopware\B2B\Common\MvcExtension\Request;

class B2bEmptyController
{
    public function indexAction(Request $request): array
    {
        return [];
    }
}
