<?php declare(strict_types=1);

namespace SwagB2bPlatform\Controller;

use Shopware\B2B\Common\MvcExtension\Request as MvcRequest;
use Shopware\B2B\Common\RestApi\RestRoutingService;
use Shopware\B2B\Debtor\BridgePlatform\DebtorApiAuthenticator;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SwagB2bRestApiController extends AbstractController
{
    /**
     * @var RestRoutingService
     */
    private $routingService;

    /**
     * @var DebtorApiAuthenticator
     */
    private $apiAuthenticator;

    public function __construct(
        RestRoutingService $routingService,
        DebtorApiAuthenticator $apiAuthenticator
    ) {
        $this->routingService = $routingService;
        $this->apiAuthenticator = $apiAuthenticator;
    }

    /**
     * @RouteScope(scopes={"api"})
     * @Route("/api/b2b", name="api.b2b")
     * @Route("/api/b2b/{wildcard}", name="api.b2b.wildcard", requirements={"wildcard": ".*"})
     */
    public function restApi(MvcRequest $request, Request $original): JsonResponse
    {
        $this->apiAuthenticator
            ->authenticateFromPathInfo($original->getPathInfo());

        $data = $this->routingService
            ->getDispatchable($original->getMethod(), $original->getPathInfo())
            ->dispatch($request);

        return new JsonResponse($data);
    }
}
