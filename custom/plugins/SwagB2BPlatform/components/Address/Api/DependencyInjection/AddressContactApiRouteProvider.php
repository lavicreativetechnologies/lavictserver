<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class AddressContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}',
                'b2b_address.api_address_contact_controller',
                'getAllAllowed',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/grant',
                'b2b_address.api_address_contact_controller',
                'getAllGrant',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/address/{addressId}',
                'b2b_address.api_address_contact_controller',
                'getAllowed',
                ['contactEmail', 'addressId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/address/{addressId}',
                'b2b_address.api_address_contact_controller',
                'allow',
                ['contactEmail', 'addressId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/address/{addressId}/grant',
                'b2b_address.api_address_contact_controller',
                'allowGrant',
                ['contactEmail', 'addressId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/allow',
                'b2b_address.api_address_contact_controller',
                'multipleAllow',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/deny',
                'b2b_address.api_address_contact_controller',
                'multipleDeny',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/address_contact/{contactEmail}/address/{addressId}',
                'b2b_address.api_address_contact_controller',
                'deny',
                ['contactEmail', 'addressId'],
            ],
        ];
    }
}
