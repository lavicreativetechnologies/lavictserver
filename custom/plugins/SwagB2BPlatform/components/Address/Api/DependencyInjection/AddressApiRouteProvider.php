<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class AddressApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/address/type/{addressType}',
                'b2b_address.api_address_controller',
                'getList',
                ['addressType'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/address/{addressId}',
                'b2b_address.api_address_controller',
                'get',
                ['addressId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/address/type/{addressType}',
                'b2b_address.api_address_controller',
                'create',
                ['addressType'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/address/{addressId}/type/{addressType}',
                'b2b_address.api_address_controller',
                'update',
                ['addressId', 'addressType'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/address/{addressId}',
                'b2b_address.api_address_controller',
                'remove',
                ['addressId'],
            ],
        ];
    }
}
