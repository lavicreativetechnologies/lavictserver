<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Acl\Framework\AclTable;
use Shopware\B2B\Address\Bridge\AddressRepository;
use Shopware\B2B\Address\BridgePlatform\AddressRepository as PlatformAddressRepository;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Role\Framework\AclTableContactContextResolver;
use Shopware\B2B\Role\Framework\AclTableRoleContextResolver;

class AddressRoleAclTable extends AclTable
{
    public function __construct()
    {
        if (ShopwareVersion::isPlatform()) {
            $addressTable = PlatformAddressRepository::TABLE_NAME;
        } else {
            $addressTable = AddressRepository::TABLE_NAME;
        }

        parent::__construct(
            'role_address',
            'b2b_role',
            'id',
            $addressTable,
            'id'
        );
    }

    protected function getContextResolvers(): array
    {
        return [
            new AclTableRoleContextResolver(),
            new AclTableContactContextResolver(),
        ];
    }
}
