<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Common\IdValue;

interface ShippingAddressRepositoryInterface
{
    public function fetchShippingIdByUserId(IdValue $userId): IdValue;
}
