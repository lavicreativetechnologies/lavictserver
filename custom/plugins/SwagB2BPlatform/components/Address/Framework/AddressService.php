<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class AddressService
{
    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    public function __construct(
        AddressRepositoryInterface $addressRepository
    ) {
        $this->addressRepository = $addressRepository;
    }

    public function setAddressAsB2bType(IdValue $shippingAddressId, IdValue $billingAddressId, OwnershipContext $ownershipContext): void
    {
        $shippingAddress = $this->addressRepository->insecureFetchOneById($shippingAddressId);

        if (!$shippingAddress->type) {
            $this->addressRepository->updateAddress($shippingAddress, $ownershipContext, AddressEntity::TYPE_SHIPPING);
        }

        $billingAddress = $this->addressRepository->insecureFetchOneById($billingAddressId);

        if (!$billingAddress->type) {
            $this->addressRepository->updateAddress($billingAddress, $ownershipContext, AddressEntity::TYPE_BILLING);
        }
    }
}
