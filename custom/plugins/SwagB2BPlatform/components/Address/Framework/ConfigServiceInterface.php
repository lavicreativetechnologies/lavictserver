<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

interface ConfigServiceInterface
{
    public function getRequiredFields(): array;

    public function getRequiredFieldsByAddress(AddressEntity $address): array;
}
