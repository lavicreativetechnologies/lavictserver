<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface AddressRepositoryInterface extends GridRepository
{
    const TABLE_ALIAS = 'address';

    public function getCountryList(): array;

    public function fetchList(
        string $type,
        OwnershipContext $ownershipContext,
        AddressSearchStruct $searchStruct
    ): array;

    public function fetchTotalCount(
        string $type,
        OwnershipContext $context,
        AddressSearchStruct $addressSearchStruct
    ): int;

    public function fetchOneById(
        IdValue $id,
        Identity $identity,
        string $addressType = null
    ): AddressEntity;

    public function fetchOneByIdWithoutVisibility(
        IdValue $id,
        Identity $identity,
        string $addressType = null
    ): AddressEntity;

    public function addAddress(
        AddressEntity $addressEntity,
        string $type,
        OwnershipContext $ownershipContext
    ): AddressEntity;

    public function updateAddress(
        AddressEntity $addressEntity,
        OwnershipContext $ownershipContext,
        string $type
    ): AddressEntity;

    public function removeAddress(
        AddressEntity $addressEntity,
        OwnershipContext $ownershipContext
    ): AddressEntity;

    public function insecureFetchOneById(
        IdValue $id,
        string $addressType = null
    ): AddressEntity;

    public function overwriteShopUserIdWithShopOwnerId(
        IdValue $userId,
        OwnershipContext $ownershipContext
    );

    public function isAddressOfType(
        IdValue $addressId,
        string $type
    ): bool;
}
