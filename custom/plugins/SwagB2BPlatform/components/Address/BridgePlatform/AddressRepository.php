<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Address\Framework\AddressCompanyAssignmentFilter;
use Shopware\B2B\Address\Framework\AddressCompanyInheritanceFilter;
use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Address\Framework\AddressSearchStruct;
use Shopware\B2B\Address\Framework\CountryRepositoryInterface;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveUsedRecordException;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\MysqlRepository;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Company\Framework\CompanyFilterHelper;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Framework\Uuid\Uuid;
use function array_merge;
use function sprintf;

/**
 * ACL enabled Address access and CRUD
 */
class AddressRepository implements AddressRepositoryInterface
{
    const TABLE_NAME = 'customer_address';
    const TABLE_ATTRIBUTES_NAME = 'b2b_customer_address_data';
    const TABLE_ATTRIBUTES_ALIAS = 'attributes';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    /**
     * @var CompanyFilterHelper
     */
    private $companyFilterHelper;

    /**
     * @var AddressCompanyAssignmentFilter
     */
    private $addressCompanyAssignmentFilter;

    /**
     * @var AddressCompanyInheritanceFilter
     */
    private $addressCompanyInheritanceFilter;

    /**
     * @var AddressEntityFactory
     */
    private $addressEntityFactory;

    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        AclReadHelper $aclReadHelper,
        CompanyFilterHelper $companyFilterHelper,
        AddressCompanyAssignmentFilter $addressCompanyAssignmentFilter,
        AddressCompanyInheritanceFilter $addressCompanyInheritanceFilter,
        AddressEntityFactory $addressEntityFactory,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->aclReadHelper = $aclReadHelper;
        $this->companyFilterHelper = $companyFilterHelper;
        $this->addressCompanyAssignmentFilter = $addressCompanyAssignmentFilter;
        $this->addressCompanyInheritanceFilter = $addressCompanyInheritanceFilter;
        $this->addressEntityFactory = $addressEntityFactory;
        $this->countryRepository = $countryRepository;
    }

    public function getCountryList(): array
    {
        return $this->countryRepository->getCountryList();
    }

    public function fetchList(
        string $type,
        OwnershipContext $ownershipContext,
        AddressSearchStruct $searchStruct
    ): array {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->addSelect(
                '(SELECT COUNT(*) FROM customer 
                    WHERE (
                        default_billing_address_id = ' . self::TABLE_ALIAS . '.id 
                        OR default_shipping_address_id = ' . self::TABLE_ALIAS . '.id
                    )
                ) as is_used'
            )
            ->addSelect(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type as type')
            ->addSelect('salutation_key as salutation')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(
                self::TABLE_ALIAS,
                self::TABLE_ATTRIBUTES_NAME,
                self::TABLE_ATTRIBUTES_ALIAS,
                self::TABLE_ALIAS . '.id = ' . self::TABLE_ATTRIBUTES_ALIAS . '.address_id'
            )
            ->innerJoin(
                self::TABLE_ALIAS,
                'salutation',
                'salutation',
                self::TABLE_ALIAS . '.salutation_id = salutation.id'
            )
            ->where(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type = :type')
            ->andWhere(self::TABLE_ALIAS . '.customer_id = :owner')
            ->setParameter('owner', $ownershipContext->shopOwnerUserId->getStorageValue())
            ->setParameter('type', $type);

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $this->companyFilterHelper
            ->applyFilter(
                $this->aclReadHelper,
                $this->addressCompanyAssignmentFilter,
                $searchStruct,
                $query,
                $this->addressCompanyInheritanceFilter
            );

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $addressesData = $query->execute()
            ->fetchAll(PDO::FETCH_ASSOC);

        $addresses = [];
        foreach ($addressesData as $addressData) {
            $addresses[] = $this->addressEntityFactory->create($addressData);
        }

        return $addresses;
    }

    public function fetchTotalCount(
        string $type,
        OwnershipContext $context,
        AddressSearchStruct $addressSearchStruct
    ): int {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(
                self::TABLE_ALIAS,
                self::TABLE_ATTRIBUTES_NAME,
                self::TABLE_ATTRIBUTES_ALIAS,
                self::TABLE_ALIAS . '.id = ' . self::TABLE_ATTRIBUTES_ALIAS . '.address_id'
            )
            ->where(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type = :type')
            ->andWhere(self::TABLE_ALIAS . '.customer_id = :owner')
            ->setParameter('owner', $context->shopOwnerUserId->getStorageValue())
            ->setParameter('type', $type);

        $this->aclReadHelper->applyAclVisibility($context, $query);

        $this->companyFilterHelper
            ->applyFilter(
                $this->aclReadHelper,
                $this->addressCompanyAssignmentFilter,
                $addressSearchStruct,
                $query,
                $this->addressCompanyInheritanceFilter
            );

        $this->dbalHelper->applyFilters($addressSearchStruct, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    public function fetchOneById(IdValue $id, Identity $identity, string $addressType = null): AddressEntity
    {
        $queryBuilder = $this->getFetchOneByIdBaseQuery(
            $id,
            $identity->getOwnershipContext()->shopOwnerUserId,
            $addressType
        );

        $this->aclReadHelper->applyAclVisibility($identity->getOwnershipContext(), $queryBuilder);

        $addressData = $queryBuilder->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$addressData) {
            return $this->fetchOneByDefaultAddressId($id, $identity, $addressType);
        }

        return $this->addressEntityFactory->create($addressData);
    }

    /**
     * @internal
     */
    protected function getFetchOneByIdBaseQuery(
        IdValue $id,
        IdValue $ownerId,
        string $addressType = null
    ): QueryBuilder {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->addSelect(
                '(SELECT COUNT(*) FROM customer 
                 WHERE default_billing_address_id = ' . self::TABLE_ALIAS . '.id 
                 OR default_shipping_address_id = ' . self::TABLE_ALIAS . '.id) as is_used'
            )
            ->addSelect(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type as type')
            ->addSelect('salutation_key as salutation')
            ->leftJoin(
                self::TABLE_ALIAS,
                self::TABLE_ATTRIBUTES_NAME,
                self::TABLE_ATTRIBUTES_ALIAS,
                self::TABLE_ALIAS . '.id = ' . self::TABLE_ATTRIBUTES_ALIAS . '.address_id'
            )
            ->innerJoin(
                self::TABLE_ALIAS,
                'salutation',
                'salutation',
                self::TABLE_ALIAS . '.salutation_id = salutation.id'
            )
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->andWhere(self::TABLE_ALIAS . '.customer_id = :owner')
            ->setParameter('id', $id->getStorageValue())
            ->setParameter('owner', $ownerId->getStorageValue());

        if ($addressType) {
            $queryBuilder
                ->andWhere(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type = :addressType')
                ->setParameter('addressType', $addressType);
        }

        return $queryBuilder;
    }

    /**
     * @internal
     */
    protected function fetchOneByDefaultAddressId(IdValue $id, Identity $identity, string $addressType = null): AddressEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->addSelect('1 as is_used')
            ->addSelect(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type as type')
            ->addSelect('salutation_key as salutation')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->leftJoin(
                self::TABLE_ALIAS,
                self::TABLE_ATTRIBUTES_NAME,
                self::TABLE_ATTRIBUTES_ALIAS,
                self::TABLE_ALIAS . '.id = ' . self::TABLE_ATTRIBUTES_ALIAS . '.address_id'
            )
            ->innerJoin(
                self::TABLE_ALIAS,
                'salutation',
                'salutation',
                self::TABLE_ALIAS . '.salutation_id = salutation.id'
            )
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->andWhere(self::TABLE_ALIAS . '.customer_id = :owner')
            ->andWhere(self::TABLE_ALIAS . '.id in(:defaultIds)')
            ->setParameter('id', $id->getStorageValue())
            ->setParameter('owner', $identity->getOwnershipContext()->shopOwnerUserId->getStorageValue())
            ->setParameter(
                ':defaultIds',
                [
                    $identity->getMainBillingAddress()->id->getStorageValue(),
                    $identity->getMainShippingAddress()->id->getStorageValue(),
                ],
                Connection::PARAM_STR_ARRAY
            );

        if ($addressType) {
            $query
                ->andWhere(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type = :addressType')
                ->setParameter('addressType', $addressType);
        }

        $statement = $query->execute();

        $addressData = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$addressData) {
            throw new NotFoundException(sprintf('Unable to locate address with id "%s"', $id->getValue()));
        }

        return $this->addressEntityFactory->create($addressData);
    }

    public function fetchOneByIdWithoutVisibility(
        IdValue $id,
        Identity $identity,
        string $addressType = null
    ): AddressEntity {
        $queryBuilder = $this->getFetchOneByIdBaseQuery(
            $id,
            $identity->getOwnershipContext()->shopOwnerUserId,
            $addressType
        );

        $addressData = $queryBuilder->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$addressData) {
            return $this->fetchOneByDefaultAddressId($id, $identity, $addressType);
        }

        return $this->addressEntityFactory->create($addressData);
    }

    public function addAddress(
        AddressEntity $addressEntity,
        string $type,
        OwnershipContext $ownershipContext
    ): AddressEntity {
        if (!$addressEntity->isNew()) {
            throw new CanNotInsertExistingRecordException('The address provided already exists');
        }

        $addressId = Uuid::randomBytes();
        $this->connection->insert(
            self::TABLE_NAME,
            array_merge(
                $this->addressEntityFactory->toDatabaseArray($addressEntity),
                [
                    'id' => $addressId,
                    'customer_id' => $ownershipContext->shopOwnerUserId->getStorageValue(),
                    'created_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
                    'updated_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
                    'salutation_id' => $this->fetchSalutationIdByKey($addressEntity->salutation)->getStorageValue(),
                ]
            )
        );

        $addressEntity->id = IdValue::create($addressId);

        $this->connection->insert(
            self::TABLE_ATTRIBUTES_NAME,
            [
                'address_id' => $addressEntity->id->getStorageValue(),
                'b2b_type' => $type,
                'created_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
                'updated_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
            ]
        );

        $addressEntity->type = $type;

        return $addressEntity;
    }

    /**
     * @internal
     */
    protected function fetchSalutationIdByKey(string $salutationKey): IdValue
    {
        return IdValue::create($this->connection->createQueryBuilder()
            ->select('id')
            ->from('salutation')
            ->where('salutation_key = :key')
            ->setParameter('key', $salutationKey)
            ->execute()
            ->fetchColumn());
    }

    public function updateAddress(
        AddressEntity $addressEntity,
        OwnershipContext $ownershipContext,
        string $type
    ): AddressEntity {
        if ($addressEntity->isNew()) {
            throw new CanNotUpdateExistingRecordException('The address provided does not exist');
        }

        $data = [];
        if (isset($addressEntity->salutation)) {
            $data['salutation_id'] = $this->fetchSalutationIdByKey($addressEntity->salutation)->getStorageValue();
        }

        $this->connection->update(
            self::TABLE_NAME,
            array_merge($this->addressEntityFactory->toDatabaseArray($addressEntity), $data),
            [
                'id' => $addressEntity->id->getStorageValue(),
                'customer_id' => $ownershipContext->shopOwnerUserId->getStorageValue(),
            ]
        );

        $this->connection->update(
            self::TABLE_ATTRIBUTES_NAME,
            ['b2b_type' => $type],
            ['address_id' => $addressEntity->id->getStorageValue()]
        );

        $addressEntity->type = $type;

        return $addressEntity;
    }

    public function removeAddress(AddressEntity $addressEntity, OwnershipContext $ownershipContext): AddressEntity
    {
        if ($addressEntity->isNew()) {
            throw new CanNotRemoveExistingRecordException('The address provided does not exist');
        }

        if ($this->isAddressUsed($addressEntity, $ownershipContext)) {
            throw new CanNotRemoveUsedRecordException('The address provided is in use');
        }

        $this->connection->delete(
            self::TABLE_NAME,
            [
                'id' => $addressEntity->id->getStorageValue(),
                'customer_id' => $ownershipContext->shopOwnerUserId->getStorageValue(),
            ]
        );

        $addressEntity->id = IdValue::null();

        return $addressEntity;
    }

    /**
     * @internal
     */
    protected function isAddressUsed(AddressEntity $addressEntity, OwnershipContext $ownershipContext): bool
    {
        return (bool) $this->connection->createQueryBuilder()->select('COUNT(*)')
            ->from('customer')
            ->where('default_billing_address_id = :id')
            ->orWhere('default_shipping_address_id = :id')
            ->andWhere('id = :customer_id')
            ->setParameters([
                'id' => $addressEntity->id->getStorageValue(),
                'customer_id' => $ownershipContext->shopOwnerUserId->getStorageValue(),
            ])
            ->execute()
            ->fetchColumn();
    }

    public function insecureFetchOneById(IdValue $id, string $addressType = null): AddressEntity
    {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->addSelect(
                '(SELECT COUNT(*) FROM customer
                 WHERE default_billing_address_id = ' . self::TABLE_ALIAS . '.id 
                 OR default_shipping_address_id = ' . self::TABLE_ALIAS . '.id) as is_used'
            )
            ->addSelect(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type as type')
            ->addSelect('salutation_key as salutation')
            ->leftJoin(
                self::TABLE_ALIAS,
                self::TABLE_ATTRIBUTES_NAME,
                self::TABLE_ATTRIBUTES_ALIAS,
                self::TABLE_ALIAS . '.id = ' . self::TABLE_ATTRIBUTES_ALIAS . '.address_id'
            )
            ->innerJoin(
                self::TABLE_ALIAS,
                'salutation',
                'salutation',
                self::TABLE_ALIAS . '.salutation_id = salutation.id'
            )
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue());

        if ($addressType) {
            $queryBuilder
                ->andWhere(self::TABLE_ATTRIBUTES_ALIAS . '.b2b_type = :addressType')
                ->setParameter('addressType', $addressType);
        }

        $addressData = $queryBuilder->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$addressData) {
            throw new NotFoundException(sprintf('Unable to locate address with id "%s".', $id->getValue()));
        }

        return $this->addressEntityFactory->create($addressData);
    }

    public function overwriteShopUserIdWithShopOwnerId(IdValue $userId, OwnershipContext $ownershipContext): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            ['customer_id' => $ownershipContext->shopOwnerUserId->getStorageValue()],
            ['customer_id' => $userId->getStorageValue()]
        );
    }

    public function isAddressOfType(IdValue $addressId, string $type): bool
    {
        $addressIdType = (string) $this->connection->createQueryBuilder()
            ->select('b2b_type')
            ->from(self::TABLE_ATTRIBUTES_NAME)
            ->where('address_id = :id')
            ->setParameter('id', $addressId->getStorageValue())
            ->execute()
            ->fetchColumn();

        return $addressIdType !== $type;
    }

    /**
     * @return string query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [
            'company',
            'department',
            'title',
            'first_name',
            'last_name',
            'street',
            'zipcode',
            'city',
            'vat_id',
            'phone_number',
            'additional_address_line1',
            'additional_address_line2',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }
}
