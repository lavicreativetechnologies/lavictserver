<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Address\Framework\ConfigServiceInterface;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class ConfigService implements ConfigServiceInterface
{
    /**
     * @var SystemConfigService
     */
    private $config;

    public function __construct(SystemConfigService $config)
    {
        $this->config = $config;
    }

    public function getRequiredFields(): array
    {
        return [
            'phone' => $this->config->get('core.loginRegistration.phoneNumberFieldRequired'),
            'addressAdditional1' => $this->config->get('core.loginRegistration.additionalAddressField1Required'),
            'addressAdditional2' => $this->config->get('core.loginRegistration.additionalAddressField2Required'),
        ];
    }

    public function getRequiredFieldsByAddress(AddressEntity $address): array
    {
        $requiredFields = $this->getRequiredFields();
        $fields = [];

        if ($requiredFields['phone']) {
            $fields['phone'] = $address->phone;
        }
        if ($requiredFields['addressAdditional1']) {
            $fields['addressAdditional1'] = $address->additional_address_line1;
        }
        if ($requiredFields['addressAdditional2']) {
            $fields['addressAdditional2'] = $address->additional_address_line2;
        }

        return $fields;
    }
}
