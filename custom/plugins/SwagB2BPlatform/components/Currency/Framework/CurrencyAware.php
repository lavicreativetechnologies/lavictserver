<?php declare(strict_types=1);

namespace Shopware\B2B\Currency\Framework;

interface CurrencyAware
{
    const DEFAULT_FACTOR = 1.0;

    public function getCurrencyFactor(): float;

    public function setCurrencyFactor(float $factor);

    /**
     * @return string[]
     */
    public function getAmountPropertyNames(): array;
}
