<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use DateTime;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\MysqlRepository;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemCheckoutSource;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\Order\Framework\OrderCheckoutSource;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrderFinishSubscriber extends CartFinishSubscriber
{
    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var OrderContextService
     */
    private $orderContextService;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        AuthenticationService $authenticationService,
        LineItemListService $lineItemListService,
        OrderContextService $orderContextService,
        OrderContextRepository $orderContextRepository
    ) {
        parent::__construct($eventDispatcher);

        $this->authenticationService = $authenticationService;
        $this->lineItemListService = $lineItemListService;
        $this->orderContextService = $orderContextService;
        $this->orderContextRepository = $orderContextRepository;
    }

    protected function onCartFinish(Cart $cart, OrderEntity $orderEntity, SalesChannelContext $salesChannelContext): ?OrderContext
    {
        if (!$this->authenticationService->isB2b()) {
            return null;
        }

        if (!$this->getCartState()->getDestination() instanceof CartDestinationDefault) {
            return null;
        }

        $orderContext = $this->createOrderContext(
            $this->authenticationService->getIdentity(),
            $cart,
            $salesChannelContext
        );

        $orderContext->orderNumber = $orderEntity->getOrderNumber();
        $orderContext->clearedAt = (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT);

        $this->orderContextRepository->updateContext($orderContext);

        return $orderContext;
    }

    /**
     * @internal
     */
    protected function createOrderContext(Identity $identity, Cart $orderEntity, SalesChannelContext $salesChannelContext): OrderContext
    {
        $ownershipContext = $identity->getOwnershipContext();

        $list = $this->lineItemListService
            ->createListThroughCheckoutSource(
                $ownershipContext,
                new LineItemCheckoutSource($orderEntity)
            );

        return $this->orderContextService
            ->createContextThroughCheckoutSource(
                $ownershipContext,
                $list,
                $this->createOrderCheckoutSourceFromCart($orderEntity, $salesChannelContext)
            );
    }

    /**
     * @internal
     */
    protected function createOrderCheckoutSourceFromCart(Cart $cart, SalesChannelContext $salesChannelContext): OrderCheckoutSource
    {
        // ToDo set comment and device type
        return new OrderCheckoutSource(
            IdValue::create($salesChannelContext->getCustomer()->getActiveBillingAddress()->getId()),
            IdValue::create($salesChannelContext->getCustomer()->getActiveShippingAddress()->getId()),
            IdValue::create($salesChannelContext->getShippingMethod()->getId()),
            '',
            '',
            $cart->getShippingCosts()->getTotalPrice(),
            $cart->getShippingCosts()->getTotalPrice() - $cart->getShippingCosts()->getCalculatedTaxes()->getAmount(),
            IdValue::create($salesChannelContext->getPaymentMethod()->getId())
        );
    }
}
