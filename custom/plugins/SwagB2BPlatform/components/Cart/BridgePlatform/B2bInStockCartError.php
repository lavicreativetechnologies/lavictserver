<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

class B2bInStockCartError extends CartAccessError
{
    public const MESSAGE_KEY = 'cart-in-stock-block';

    private function __construct(string $cartToken)
    {
        parent::__construct(
            $cartToken,
            self::MESSAGE_KEY,
            self::LEVEL_ERROR,
            true
        );
    }

    public static function createCartBlockedForInStockMessage(string $cartToken): B2bInStockCartError
    {
        return new self($cartToken);
    }
}
