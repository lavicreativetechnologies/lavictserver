<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Offer\Framework\OfferEntity;

class OfferDestination extends CartDestinationDefault
{
    private const NAME = 'offer';

    /**
     * @var OfferEntity
     */
    private $offerEntity;

    /**
     * @var LineItemList
     */
    private $lineItemList;

    public function getName(): string
    {
        return self::NAME;
    }

    public function __construct(OfferEntity $offerEntity, LineItemList $lineItemList)
    {
        $this->offerEntity = $offerEntity;
        $this->lineItemList = $lineItemList;
    }

    public function getOfferEntity(): OfferEntity
    {
        return $this->offerEntity;
    }

    public function getLineItemList(): LineItemList
    {
        return $this->lineItemList;
    }
}
