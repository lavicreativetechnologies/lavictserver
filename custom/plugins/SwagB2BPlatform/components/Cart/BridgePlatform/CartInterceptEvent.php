<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\EventDispatcher\Event;

class CartInterceptEvent extends Event
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * @var SalesChannelContext
     */
    private $context;

    public function __construct(
        Cart $cart,
        SalesChannelContext $context
    ) {
        $this->cart = $cart;
        $this->context = $context;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function getContext(): SalesChannelContext
    {
        return $this->context;
    }
}
