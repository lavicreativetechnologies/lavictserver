<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartService;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemCheckoutProvider;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemCheckoutSource;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartValidatorInterface;
use Shopware\Core\Checkout\Cart\Error\ErrorCollection;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class CartValidator implements CartValidatorInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var LineItemCheckoutProvider
     */
    private $lineItemCheckoutProvider;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        AuthenticationService $authenticationService,
        LineItemCheckoutProvider $lineItemCheckoutProvider,
        CartService $cartService,
        ContextProvider $contextProvider
    ) {
        $this->authenticationService = $authenticationService;
        $this->lineItemCheckoutProvider = $lineItemCheckoutProvider;
        $this->cartService = $cartService;
        $this->contextProvider = $contextProvider;
    }

    public function validate(Cart $cart, ErrorCollection $errorCollection, SalesChannelContext $salesChannelContext): void
    {
        $this->clearB2BBlockerError($cart);

        $this->contextProvider
            ->setSalesChannelContext($salesChannelContext);

        if (!$this->authenticationService->isB2b()) {
            return;
        }

        $identity = $this->authenticationService->getIdentity();

        $cartState = CartState::extract($cart);

        $accessResult = $this->validateAccessibility($cart, $identity);

        $this->mapErrorsIntoCartState($accessResult, $cartState);
        $this->updateCart($errorCollection, $accessResult, $cartState);
    }

    /**
     * @internal
     */
    protected function validateAccessibility(Cart $cart, Identity $identity): CartAccessResult
    {
        $order = new OrderClearanceEntity();
        $order->list = $this->lineItemCheckoutProvider
            ->createList(new LineItemCheckoutSource($cart));

        return $this->cartService
            ->computeAccessibility(
                $identity,
                $order,
                CartService::ENVIRONMENT_NAME_ORDER
            );
    }

    /**
     * @internal
     */
    protected function mapErrorsIntoCartState(CartAccessResult $result, CartState $cartState): void
    {
        foreach ($result->information as $index => $info) {
            $cartState->getMessages()->add(CartAccessError::fromInformationMessage($info, $index));
        }

        foreach ($result->getErrors() as $index => $error) {
            $cartState->getMessages()->add(CartAccessError::fromErrorMessage($error, $index));
        }
    }

    /**
     * @internal
     */
    protected function updateCart(ErrorCollection $errorCollection, CartAccessResult $accessResult, CartState $cartState): void
    {
        if ($accessResult->hasErrors()) {
            $this->setCartToError($errorCollection, $cartState);

            return;
        }

        $this->setCartToPass($cartState);
    }

    /**
     * @internal
     */
    protected function clearB2BBlockerError(Cart $cart): void
    {
        $cart->getErrors()
            ->remove(CartAccessError::CART_BLOCKER_ID);
    }

    /**
     * @internal
     */
    protected function setCartToError(ErrorCollection $errorCollection, CartState $cartState): void
    {
        $cartBlockedError = CartAccessError::createCartBlockedForClearanceMessage();
        $errorCollection->add($cartBlockedError);

        $cartState->setDestination(new OrderClearanceInterceptDestination());
    }

    /**
     * @internal
     */
    protected function setCartToPass(CartState $cartState): void
    {
        if (!$cartState->getDestination() instanceof OrderClearanceInterceptDestination) {
            return;
        }

        $cartState->rollBackDestination();
    }
}
