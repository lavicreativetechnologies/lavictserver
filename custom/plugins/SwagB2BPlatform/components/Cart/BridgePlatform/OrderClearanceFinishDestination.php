<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;

/**
 * @deprecated tag:v4.4.0 Constructor will require an {@see OrderClearanceEntity} as first argument.
 */
class OrderClearanceFinishDestination implements CartDestination
{
    private const NAME = 'cleared-order';

    public function getName(): string
    {
        return self::NAME;
    }
}
