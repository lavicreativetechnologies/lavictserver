<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use RuntimeException;
use Shopware\B2B\Common\B2BException;

class CartFinishException extends RuntimeException implements B2BException
{
}
