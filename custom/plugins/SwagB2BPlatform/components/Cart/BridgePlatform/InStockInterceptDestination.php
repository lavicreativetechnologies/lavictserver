<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

class InStockInterceptDestination implements CartDestination
{
    public function getName(): string
    {
        return 'in-stock-block';
    }
}
