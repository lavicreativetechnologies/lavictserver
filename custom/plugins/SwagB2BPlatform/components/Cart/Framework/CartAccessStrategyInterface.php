<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Framework;

interface CartAccessStrategyInterface
{
    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult);

    public function addInformation(CartAccessResult $cartAccessResult);
}
