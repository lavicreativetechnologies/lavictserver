<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Framework;

use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * @deprecated tag:v4.2.0 repository is now a framework component use CartHistoryRepository instead
 */
interface CartHistoryRepositoryInterface
{
    public function fetchHistory(
        array $timeRestrictions,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext
    ): array;
}
