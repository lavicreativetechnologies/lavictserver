<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Framework;

class BlackListCartAccess implements CartAccessStrategyInterface
{
    /**
     * @var CartAccessStrategyInterface[]
     */
    private $strategies;

    public function __construct(CartAccessStrategyInterface ...$strategies)
    {
        $this->strategies = $strategies;
    }

    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult): void
    {
        foreach ($this->strategies as $strategy) {
            $strategy->checkAccess($context, $cartAccessResult);
        }
    }

    public function addInformation(CartAccessResult $cartAccessResult): void
    {
        foreach ($this->strategies as $strategy) {
            $strategy->addInformation($cartAccessResult);
        }
    }
}
