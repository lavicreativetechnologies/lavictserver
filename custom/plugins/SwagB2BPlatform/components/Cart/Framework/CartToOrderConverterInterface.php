<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Framework;

interface CartToOrderConverterInterface
{
    public function convertCartToOrder(): void;
}
