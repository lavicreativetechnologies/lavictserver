<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Bridge\DependencyInjection;

use Shopware\B2B\AclRoute\Framework\DependencyInjection\AclRouteFrameworkConfiguration;
use Shopware\B2B\Cart\BridgePlatform\DependencyInjection\CartBridgeConfiguration as PlatformCartBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Debtor\Framework\DependencyInjection\DebtorFrameworkConfiguration;
use Shopware\B2B\Order\Framework\DependencyInjection\OrderFrameworkConfiguration;
use Shopware\B2B\OrderClearance\Framework\DependencyInjection\OrderClearanceFrameworkConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CartBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformCartBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
            new CartModeCollector(),
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new StoreFrontAuthenticationFrameworkConfiguration(),
            new OrderFrameworkConfiguration(),
            new OrderClearanceFrameworkConfiguration(),
            new DebtorFrameworkConfiguration(),
            new AclRouteFrameworkConfiguration(),
        ];
    }
}
