            <br />
            {if $orderReference}
            <p>
                Order reference number: <strong>{$orderReference}</strong>
            </p>
            {/if}
            {if $requestedDeliveryDate}
            <p>
                Requested delivery date: <strong>{$requestedDeliveryDate}</strong>
            </p>
            {/if}
            <br />
