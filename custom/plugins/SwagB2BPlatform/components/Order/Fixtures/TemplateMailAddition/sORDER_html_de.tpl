            <br />
            {if $orderReference}
            <p>
                Bestellreferenznummer: <strong> {$orderReference} </strong>
            </p>
            {/if}
            {if $requestedDeliveryDate}
            <p>
                Wunschliefertermin: <strong> {$requestedDeliveryDate} </strong>
            </p>
            {/if}
            <br />
