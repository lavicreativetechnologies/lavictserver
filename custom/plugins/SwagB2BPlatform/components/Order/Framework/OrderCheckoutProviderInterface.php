<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

interface OrderCheckoutProviderInterface
{
    public function createOrder(OrderSource $source): OrderContext;

    public function updateOrder(OrderSource $source, OrderContext $order): OrderContext;
}
