<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use DateTime;
use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\MysqlRepository;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\TransferOwnerDataToContextOwnerInterface;

class OrderContextRepository implements TransferOwnerDataToContextOwnerInterface
{
    const TABLE_NAME = 'b2b_order_context';

    const TABLE_ALIAS = 'orderContext';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var array
     */
    private $auditLogReferences = [
        'orderContextId' => self::TABLE_NAME,
        'listId' => LineItemListRepository::TABLE_NAME,
        'itemId' => LineItemReferenceRepository::TABLE_NAME,
    ];

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function fetchOneOrderContextById(IdValue $id, OwnershipContext $ownershipContext): OrderContext
    {
        $rawOrderContextData = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->andWhere(self::TABLE_ALIAS . '.auth_id IN (SELECT DISTINCT id FROM b2b_store_front_auth WHERE '
                     . self::TABLE_ALIAS . '.auth_id = :authId OR context_owner_id = :authId)')
            ->setParameter('authId', $ownershipContext->contextOwnerId->getStorageValue())
            ->setParameter('id', $id->getStorageValue())
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $this->createOrderContextEntity($id, $rawOrderContextData);
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function fetchOneOrderContextByOrderNumber(string $orderNumber): OrderContext
    {
        $rawOrderContextData = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.ordernumber = :orderNumber')
            ->setParameter('orderNumber', $orderNumber)
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$rawOrderContextData) {
            throw new NotFoundException('Could not find order context with number "' . $orderNumber . '".');
        }

        return $this->createOrderContextEntity(IdValue::create($rawOrderContextData['id']), $rawOrderContextData);
    }

    public function addOrderContext(OrderContext $orderContext): void
    {
        $this->connection
            ->insert('b2b_order_context', $orderContext->toDatabaseArray());

        $orderContext->id = IdValue::create($this->connection->lastInsertId());
    }

    /**
     * @deprecated tag:v4.3.0 $orderContextId will become restricted to IdValue
     */
    public function setSalesRepresentativeById($orderContextId, IdValue $authId): void
    {
        $oderContextId = IdValue::create($orderContextId);

        $this->connection->update(
            'b2b_order_context',
            ['sales_representative_auth_id ' => $authId->getStorageValue()],
            ['id' => $orderContextId->getStorageValue()]
        );
    }

    /**
     * @deprecated tag:v4.2.0 use OrderClearanceRepository::acceptOrderContext instead
     */
    public function syncFinishOrder(OrderContext $orderContext): void
    {
        $this->updateContext($orderContext);

        $this->connection->update(
            'b2b_order_context',
            ['cleared_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT)],
            ['id' => $orderContext->id->getStorageValue()]
        );
    }

    public function fetchAuditLogReferencesByListId(IdValue $listId): array
    {
        $rawReferenceData = (array) $this->connection->createQueryBuilder()
            ->select([
                'list.id AS listId',
                'orderContext.id AS orderContextId',
            ])
            ->from('b2b_line_item_list', 'list')
            ->leftJoin('list', 'b2b_order_context', 'orderContext', 'list.id = orderContext.list_id')
            ->where('list.id = :listId')
            ->setParameter('listId', $listId->getStorageValue())
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $this->mapAuditLogReferences($rawReferenceData);
    }

    public function fetchAuditLogReferencesByLineItemId(IdValue $lineItemId): array
    {
        $rawReferenceData = (array) $this->connection->createQueryBuilder()
            ->select([
                'lineItem.id AS itemId',
                'list.id AS listId',
                'orderContext.id AS orderContextId',
            ])
            ->from('b2b_line_item_reference', 'lineItem')
            ->leftJoin('lineItem', 'b2b_line_item_list', 'list', 'lineItem.list_id = list.id')
            ->leftJoin('list', 'b2b_order_context', 'orderContext', 'list.id = orderContext.list_id')
            ->where('lineItem.id = :lineItemId')
            ->setParameter('lineItemId', $lineItemId->getStorageValue())
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $this->mapAuditLogReferences($rawReferenceData);
    }

    public function fetchAuditLogReferencesByContextId(IdValue $orderContextId): array
    {
        $rawReferenceData = (array) $this->connection->createQueryBuilder()
            ->select([
                'orderContext.id AS orderContextId',
                'list.id AS listId',
            ])
            ->from('b2b_order_context', 'orderContext')
            ->leftJoin('orderContext', 'b2b_line_item_list', 'list', 'list.id = orderContext.list_id')
            ->where('orderContext.id = :orderContextId')
            ->setParameter('orderContextId', $orderContextId->getStorageValue())
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $this->mapAuditLogReferences($rawReferenceData);
    }

    /**
     * @internal
     */
    protected function mapAuditLogReferences(array $rawData): array
    {
        $auditLogReferences = [];

        foreach ($this->auditLogReferences as $canonicalFieldName => $tableName) {
            if (!isset($rawData[$canonicalFieldName])) {
                continue;
            }

            $auditLogReferences[$tableName] = IdValue::create($rawData[$canonicalFieldName]);
        }

        return $auditLogReferences;
    }

    /**
     * @internal
     * @param array|false $rawOrderContextData
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    protected function createOrderContextEntity(IdValue $id, $rawOrderContextData): OrderContext
    {
        if (!$rawOrderContextData) {
            throw new NotFoundException('No context found with id "' . $id->getValue() . '"');
        }

        $orderContext = new OrderContext();
        $orderContext->fromDatabaseArray($rawOrderContextData);

        return $orderContext;
    }

    public function updateContext(OrderContext $orderContext): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            $orderContext->toDatabaseArray(),
            ['id' => $orderContext->id->getStorageValue()]
        );
    }

    public function transferOwnerDataToContextOwner(IdValue $authId, IdValue $contextOwnerId): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            ['auth_id' => $contextOwnerId->getStorageValue()],
            ['auth_id' => $authId->getStorageValue()]
        );
    }
}
