<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface ShopOrderRepositoryInterface
{
    public function setOrderIdentity(string $orderNumber, IdValue $authId);

    public function setOrderToShopOwnerUser(string $orderNumber, IdValue $debtorId);

    public function fetchOneOrderContextByShopOrderId(IdValue $shopOrderId): OrderContext;

    /**
     * @deprecated tag:v4.2.0 not necessary and actually quite horribly fragile, since shop updates are only synced back to the order context, not the other way around
     */
    public function updateStatusByName(IdValue $orderContextId, string $technicalStatusName, OwnershipContext $ownershipContext);

    /**
     * @throws ShopOrderStatusOutOfSyncException
     */
    public function testStatusInSync(IdValue $orderContextId, string $technicalStatusName);

    /**
     * @deprecated tag:v4.2.0 not necessary and actually quite horribly fragile, since shop updates are only synced back to the order context, not the other way around
     */
    public function updateStatus(IdValue $orderContextId, IdValue $statusId, OwnershipContext $ownershipContext);

    public function fetchOrderById(IdValue $orderId): array;

    public function setOrderCommentByOrderContextId(IdValue $orderContextId, string $comment);
}
