<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use JsonSerializable;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository;
use function get_object_vars;

class OrderContext implements JsonSerializable
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var IdValue
     */
    public $listId;

    /**
     * @var string
     */
    public $orderNumber;

    /**
     * @var IdValue
     */
    public $shippingAddressId;

    /**
     * @var IdValue
     */
    public $billingAddressId;

    /**
     * @var IdValue
     * ToDo refactor as it could be multiple values in sw6
     */
    public $paymentId;

    /**
     * shopware alias: dispatchID
     *
     * @var IdValue
     * ToDo refactor as it could be multiple values in sw6
     */
    public $shippingId;

    /**
     * @var float
     */
    public $shippingAmount;

    /**
     * @var float
     */
    public $shippingAmountNet;

    /**
     * @var string
     */
    public $comment = '';

    /**
     * @var string
     */
    public $deviceType = '';

    /**
     * @var string
     */
    public $status;

    /**
     * @var IdValue
     */
    public $authId;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var string
     */
    public $requestedDeliveryDate;

    /**
     * @var string
     */
    public $orderReference;

    /**
     * @var string
     */
    public $declinedAt;

    /**
     * @var string
     */
    public $clearedAt;

    /**
     * @var OrderOwnerEntity
     */
    public $owner;

    /**
     * @var IdValue
     */
    public $salesRepresentativeAuthId;

    /**
     * @var bool
     * @deprecated tag:v4.3.0 - no longer guaranteed that it works
     */
    public $isEditable = false;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->listId = IdValue::null();
        $this->shippingAddressId = IdValue::null();
        $this->billingAddressId = IdValue::null();
        $this->paymentId = IdValue::null();
        $this->shippingId = IdValue::null();
        $this->authId = IdValue::null();
        $this->salesRepresentativeAuthId = IdValue::null();
    }

    public function isEditable(): bool
    {
        return $this->status === OrderClearanceRepository::STATE_CLEARANCE_OPEN;
    }

    public function isOrdered(): bool
    {
        return isset($this->orderNumber) && $this->status !== OrderRepository::STATE_ORDER_CANCELLED;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'list_id' => $this->listId->getStorageValue(),

            'ordernumber' => $this->orderNumber,
            'cleared_at' => $this->clearedAt,

            'shipping_address_id' => $this->shippingAddressId->getStorageValue(),
            'billing_address_id' => $this->billingAddressId->getStorageValue(),

            'payment_id' => $this->paymentId->getStorageValue(),
            'shipping_id' => $this->shippingId->getStorageValue(),

            'shipping_amount' => $this->shippingAmount,
            'shipping_amount_net' => $this->shippingAmountNet,

            'order_reference' => $this->orderReference,
            'requested_delivery_date' => $this->requestedDeliveryDate,

            'comment' => $this->comment,
            'device_type' => $this->deviceType,
            'state' => $this->status,

            'auth_id' => $this->authId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $data): void
    {
        $this->id = IdValue::create($data['id']);

        $this->listId = IdValue::create($data['list_id']);

        $this->orderNumber = (string) $data['ordernumber'];

        $this->authId = IdValue::create($data['auth_id']);

        $this->createdAt = $data['created_at'];
        $this->clearedAt = $data['cleared_at'];
        $this->declinedAt = $data['declined_at'];

        $this->shippingAddressId = IdValue::create($data['shipping_address_id']);
        $this->billingAddressId = IdValue::create($data['billing_address_id']);

        $this->paymentId = IdValue::create($data['payment_id']);
        $this->shippingId = IdValue::create($data['shipping_id']);

        $this->shippingAmount = (float) $data['shipping_amount'];
        $this->shippingAmountNet = (float) $data['shipping_amount_net'];

        $this->comment = $data['comment'];
        $this->deviceType = $data['device_type'];

        $this->orderReference = (string) $data['order_reference'];
        $this->requestedDeliveryDate = (string) $data['requested_delivery_date'];
        $this->status = (string) $data['state'];

        if (isset($data['sales_representative_auth_id'])) {
            $this->salesRepresentativeAuthId = IdValue::create($data['sales_representative_auth_id']);
        }

        if (!$this->orderNumber) {
            $this->orderNumber = null;
        }

        $this->isEditable = $this->isEditable();
    }

    public function convertFromShopOrderArray(array $data): void
    {
        $this->listId = IdValue::null();

        $this->orderNumber = (string) $data['ordernumber'];

        $this->authId = IdValue::create($data['auth_id']);

        $this->createdAt = $data['ordertime'];
        $this->clearedAt = $data['ordertime'];

        $this->shippingAddressId = IdValue::create($data['shipping_address_id']);
        $this->billingAddressId = IdValue::create($data['billing_address_id']);

        $this->paymentId = IdValue::create($data['paymentID']);

        $this->shippingId = IdValue::create($data['shipping_id']);
        $this->shippingAmount = (float) $data['invoice_shipping'];
        $this->shippingAmountNet = (float) $data['invoice_shipping_net'];

        $this->status = $data['state'];
        $this->isEditable = $this->isEditable();
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
