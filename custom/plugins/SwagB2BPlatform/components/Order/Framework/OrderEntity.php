<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class OrderEntity extends OrderContext
{
    /**
     * @var LineItemList
     */
    public $list;

    /**
     * @deprecated tag:v4.3.0 No longer necessary, please use $this->clearedBy
     * @var OrderResponsibleEntity
     */
    public $responsible;

    /**
     * @var Identity|null
     */
    public $clearedBy;
}
