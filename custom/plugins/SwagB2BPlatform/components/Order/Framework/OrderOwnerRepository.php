<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Doctrine\DBAL\Connection;

/**
 * @deprecated v4.3.0: Service will be removed
 */
class OrderOwnerRepository
{
    const TABLE_NAME = 'b2b_sales_representative_orders';

    const TABLE_ALIAS = 'sales_representative_orders';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function fetchAuthIdFromOrderOnBehalfByOrderNumber(string $orderNumber): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.sales_representative_auth_id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where('order_number = :orderNumber')
            ->setParameter('orderNumber', $orderNumber);

        return (int) $query->execute()->fetchColumn();
    }
}
