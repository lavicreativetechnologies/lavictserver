<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\Entity;

class OrderOwnerEntity
{
    /**
     * @var Entity
     */
    public $entity;

    /**
     * @var array
     */
    public $behalfOf;
}
