<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AclAuthReadHelperLoader;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function date;
use function sprintf;
use function strtotime;

class OrderRepository implements OrderRepositoryInterface
{
    const TABLE_NAME = 'b2b_order_context';

    const TABLE_ALIAS = 'orderContext';

    const STATE_ORDER_OPEN = 'open';

    const STATE_ORDER_CANCELLED = 'cancelled';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var ShopOrderRepositoryInterface
     */
    private $shopOrderRepository;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var AclAuthReadHelperLoader
     */
    private $aclAuthReadHelper;

    /**
     * @var AuthenticationService
     */
    private $authService;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        ShopOrderRepositoryInterface $shopOrderRepository,
        LineItemListRepository $lineItemListRepository,
        AclAuthReadHelperLoader $aclAuthReadHelper,
        AuthenticationService $authService
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->shopOrderRepository = $shopOrderRepository;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->aclAuthReadHelper = $aclAuthReadHelper;
        $this->authService = $authService;
    }

    public function fetchLists(
        OwnershipContext $ownershipContext,
        OrderSearchStruct $orderSearchStruct,
        CurrencyContext $currencyContext
    ): array {
        $queryBuilder = $this->createBaseQueryBuilder($ownershipContext)
            ->addSelect(self::TABLE_ALIAS . '.*');

        if (!$orderSearchStruct->orderBy) {
            $orderSearchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $orderSearchStruct->orderDirection = 'DESC';
        }

        $this->aclAuthReadHelper->applyAclVisibility($ownershipContext, $queryBuilder);

        $this->dbalHelper->applySearchStruct($orderSearchStruct, $queryBuilder);

        if ($orderSearchStruct->filters) {
            $this->joinAdditionalSearchResource($queryBuilder);
        }

        $rawOrders = $queryBuilder
            ->execute()
            ->fetchAll();

        $orders = [];
        foreach ($rawOrders as $rawOrder) {
            $orders[] = $this->createOrderEntity($rawOrder, $currencyContext, $ownershipContext);
        }

        return $orders;
    }

    /**
     * @internal
     */
    protected function joinAdditionalSearchResource(QueryBuilder $queryBuilder): void
    {
        $queryBuilder
            ->innerJoin(
                self::TABLE_ALIAS,
                'b2b_debtor_contact',
                'debtorContact',
                self::TABLE_ALIAS . '.auth_id = debtorContact.auth_id'
            );
    }

    public function fetchTotalCount(OwnershipContext $ownershipContext, OrderSearchStruct $orderSearchStruct): int
    {
        $queryBuilder = $this->createBaseQueryBuilder($ownershipContext)
            ->select('COUNT(*)');

        if ($orderSearchStruct->filters) {
            $this->joinAdditionalSearchResource($queryBuilder);
        }

        $this->dbalHelper->applyFilters($orderSearchStruct, $queryBuilder);

        $this->aclAuthReadHelper->applyAclVisibility($ownershipContext, $queryBuilder);

        return (int) $queryBuilder
            ->execute()
            ->fetchColumn();
    }

    public function fetchOrderById(
        IdValue $orderContextId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OrderEntity {
        $queryBuilder = $this->createBaseQueryBuilder($ownershipContext)
            ->addSelect(self::TABLE_ALIAS . '.*')
            ->andWhere(self::TABLE_ALIAS . '.id=:id')
            ->setParameter('id', $orderContextId->getStorageValue());

        $this->aclAuthReadHelper->applyAclVisibility($ownershipContext, $queryBuilder);

        $rawOrder = $queryBuilder
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$rawOrder) {
            throw new NotFoundException(sprintf('orderContext with id %d not found', $orderContextId->getValue()));
        }

        $order = $this->createOrderEntity($rawOrder, $currencyContext, $ownershipContext);

        return $order;
    }

    public function fetchOrderContextByListId(IdValue $listId): OrderContext
    {
        $data = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('b2b_order_context', 'orderContext')
            ->where('orderContext.list_id = :listId')
            ->setParameter('listId', $listId->getStorageValue())
            ->execute()
            ->fetch();

        if (!$data) {
            throw new NotFoundException(sprintf('orderContext by listId %d not found', $listId->getValue()));
        }

        $orderContext = new OrderContext();
        $orderContext->fromDatabaseArray($data);

        return $orderContext;
    }

    public function setOrderCommentByOrderContextId(IdValue $orderContextId, string $comment): void
    {
        $this->shopOrderRepository->setOrderCommentByOrderContextId($orderContextId, $comment);
    }

    /**
     * query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'ordernumber',
            'order_reference',
            'requested_delivery_date',
            'created_at',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [
            '%2$s' => [
                'debtorContact' => [
                    'firstname',
                    'lastname',
                ],
            ],
        ];
    }

    /**
     * @internal
     */
    protected function createBaseQueryBuilder(OwnershipContext $ownershipContext): QueryBuilder
    {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_store_front_auth', 'auth', self::TABLE_ALIAS . '.auth_id = auth.id')
            ->where('auth.context_owner_id = :contextOwnerId')
            ->andWhere(self::TABLE_ALIAS . '.state != :stateOffer')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue())
            ->setParameter('stateOffer', 'offer');

        if (!$ownershipContext->contextOwnerId->equals($ownershipContext->authId)) {
            $queryBuilder->andWhere(self::TABLE_ALIAS . '.auth_id != :contextOwnerId');
        }

        return $queryBuilder;
    }

    public function fetchAuthIdFromOrderById(IdValue $orderId): IdValue
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.auth_id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :orderId')
            ->setParameter('orderId', $orderId->getStorageValue());

        $authId = $query->execute()->fetchColumn();

        if (!$authId) {
            throw new NotFoundException(sprintf('Order not found for %s', $orderId));
        }

        return IdValue::create($authId);
    }

    /**
     * @internal
     */
    protected function createOrderEntity(array $rawOrder, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): OrderEntity
    {
        $order = new OrderEntity();
        $order->fromDatabaseArray($rawOrder);
        $order->list = $this->lineItemListRepository->fetchOneListById(
            $order->listId,
            $currencyContext,
            $ownershipContext
        );

        if (!$order->authId instanceof NullIdValue) {
            try {
                $order->owner = $this->authService
                    ->getIdentityByAuthId(IdValue::create($rawOrder['auth_id']))->getEntity();
            } catch (NotFoundException $e) {
                $order->owner = null;
            }
        }

        if ($rawOrder['cleared_by']) {
            try {
                $order->clearedBy = $this->authService
                    ->getIdentityByAuthId(IdValue::create($rawOrder['cleared_by']));
            } catch (NotFoundException $e) {
                $order->clearedBy = null;
            }
        }

        if ($order->clearedBy && $rawOrder['cleared_at']) {
            $orderResponsible = new OrderResponsibleEntity();
            $orderResponsible->acceptedAt = date('d.m.Y H:i', strtotime($rawOrder['cleared_at']));
            $orderResponsible->entity = $order->clearedBy->getEntity();

            $order->responsible = $orderResponsible;
        }

        return $order;
    }
}
