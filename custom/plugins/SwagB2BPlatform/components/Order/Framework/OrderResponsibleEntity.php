<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\Entity;

/**
 * @deprecated tag:v4.2.0 now part of the OrderEntity
 */
class OrderResponsibleEntity
{
    /**
     * @var Entity
     */
    public $entity;

    /**
     * @var string
     */
    public $acceptedAt;
}
