<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;

/**
 * @deprecated tag:v4.2.0 table got removed, write through the order context instaed
 */
class AcceptedOrderClearanceRepository
{
    const TABLE_NAME = 'b2b_order_context';

    const TABLE_ALIAS = 'orderContext';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    public function storeAcceptedOrderClearance(string $orderNumber, IdValue $authId, IdValue $responsibleAuthId, string $dayTime): void
    {
        $this->connection
            ->update(
                self::TABLE_NAME,
                [
                    'cleared_by' => $responsibleAuthId->getStorageValue(),
                ],
                [
                    'ordernumber' => $orderNumber,
                    'auth_id' => $authId->getStorageValue(),
                ]
            );
    }

    public function fetchAcceptedOrderClearanceByOrderNumber(string $orderNumber): array
    {
        $order = $this->connection->createQueryBuilder()
            ->select([
                self::TABLE_ALIAS . '.ordernumber AS order_number',
                self::TABLE_ALIAS . '.auth_id AS auth_id',
                self::TABLE_ALIAS . '.cleared_by AS responsible_auth_id',
                self::TABLE_ALIAS . '.cleared_at AS accepted_at',
            ])
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where('ordernumber = :orderNumber')
            ->setParameter('orderNumber', $orderNumber)
            ->execute()
            ->fetch();

        if (!$order) {
            throw new NotFoundException('Order number not found in accepted order clearances');
        }

        return $order;
    }
}
