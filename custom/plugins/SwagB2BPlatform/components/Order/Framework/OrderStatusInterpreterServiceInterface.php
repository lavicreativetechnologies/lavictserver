<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

/**
 * @deprecated tag:v4.2.0 no longer used
 */
interface OrderStatusInterpreterServiceInterface
{
    /**
     * @deprecated tag:v4.2.0 no longer used
     */
    public function isOpen(OrderContext $orderContext): bool;

    /**
     * @deprecated tag:v4.2.0 no longer used
     */
    public function isClearanceOpen(OrderContext $orderContext): bool;
}
