<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogIndexEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogService;
use Shopware\B2B\AuditLog\Framework\AuditLogValueBasicEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemAddEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemRemoveEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueOrderCommentEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueOrderReferenceEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueRequestedDeliveryDateEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function get_class;

class OrderAuditLogService
{
    /**
     * @var AuditLogService
     */
    private $auditLogService;

    public function __construct(AuditLogService $auditLogService)
    {
        $this->auditLogService = $auditLogService;
    }

    public function createStatusChangeAuditLog(
        IdValue $referenceId,
        AuditLogValueBasicEntity $auditLogValue,
        Identity $identity
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        $auditLogIndex = new AuditLogIndexEntity();
        $auditLogIndex->referenceId = $referenceId;
        $auditLogIndex->referenceTable = OrderContextRepository::TABLE_NAME;

        return $this->auditLogService->createAuditLog($auditLog, $identity, [$auditLogIndex]);
    }

    public function addLogFromLineItemAuditLogService(
        AuditLogValueBasicEntity $auditLogValue,
        array $references,
        Identity $identity
    ): AuditLogEntity {
        return $this->auditLogService
            ->createAuditLog($this->createLogEntity($auditLogValue), $identity, $this->createIndices($references));
    }

    public function addBackendLogFromLineItemAuditLogService(
        AuditLogValueBasicEntity $auditLogValue,
        array $references
    ): AuditLogEntity {
        return $this->auditLogService
            ->createBackendAuditLog($this->createLogEntity($auditLogValue), $this->createIndices($references));
    }

    public function createOrderClearanceLineItemRemove(
        AuditLogValueLineItemRemoveEntity $auditLogValue,
        array $reference,
        Identity $identity
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        return $this->auditLogService
            ->createAuditLog($auditLog, $identity, $this->createIndices($reference));
    }

    public function createOrderClearanceLineItemAdd(
        AuditLogValueLineItemAddEntity $auditLogValue,
        array $reference,
        Identity $identity
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        return $this->auditLogService
            ->createAuditLog($auditLog, $identity, $this->createIndices($reference));
    }

    public function createOrderClearanceComment(
        AuditLogValueOrderCommentEntity $auditLogValue,
        array $reference,
        Identity $identity
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        return $this->auditLogService
            ->createAuditLog($auditLog, $identity, $this->createIndices($reference));
    }

    public function createOrderClearanceOrderReference(
        AuditLogValueOrderReferenceEntity $auditLogValue,
        array $reference,
        Identity $identity
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        return $this->auditLogService
            ->createAuditLog($auditLog, $identity, $this->createIndices($reference));
    }

    public function createOrderClearanceRequestedDeliveryDate(
        AuditLogValueRequestedDeliveryDateEntity $auditLogValue,
        array $reference,
        Identity $identity
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        return $this->auditLogService
            ->createAuditLog($auditLog, $identity, $this->createIndices($reference));
    }

    /**
     * @internal
     */
    protected function createLogEntity(AuditLogValueBasicEntity $auditLogValue): AuditLogEntity
    {
        $auditLog = new AuditLogEntity();
        $auditLog->logValue = $auditLogValue;
        $auditLog->logType = get_class($auditLogValue);

        return $auditLog;
    }

    /**
     * @internal
     */
    protected function createIndices(array $references): array
    {
        $indices = [];
        foreach ($references as $tableName => $index) {
            $indices[] = $this->createIndex($tableName, $index);
        }

        return $indices;
    }

    /**
     * @internal
     * @return AuditLogIndexEntity
     */
    protected function createIndex(string $tableName, IdValue $id)
    {
        $auditLogIndex = new AuditLogIndexEntity();
        $auditLogIndex->referenceId = $id;
        $auditLogIndex->referenceTable = $tableName;

        return $auditLogIndex;
    }
}
