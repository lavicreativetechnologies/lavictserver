<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use TypeError;

class OrderOwnerService
{
    /**
     * @var AuthenticationService
     */
    private $authService;

    public function __construct(
        AuthenticationService $authService
    ) {
        $this->authService = $authService;
    }

    /**
     * @deprecated v4.3.0: Will be removed, use createOrderContextOwner instead
     */
    public function createOrderOwner(OrderEntity $order): OrderOwnerEntity
    {
        return $this->createOrderContextOwner($order);
    }

    public function createOrderContextOwner(OrderContext $order): OrderOwnerEntity
    {
        $orderOwner = new OrderOwnerEntity();

        $orderOwner->entity = $this->authService
            ->getIdentityByAuthId($order->authId)->getEntity();

        try {
            $orderOwner->behalfOf = $this->authService->getIdentityByAuthId($order->salesRepresentativeAuthId)->getEntity();
        } catch (NotFoundException $e) {
            // nth
        } catch (TypeError $e) {
            // nth
        }

        return $orderOwner;
    }

    /**
     * @param OrderContext[] $orders
     */
    public function createOrderOwners(array $orders): array
    {
        foreach ($orders as $order) {
            $order->owner = $this->createOrderContextOwner($order);
        }

        return $orders;
    }
}
