<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * @deprecated tag:v4.2.0 the whole class is now in framework
 */
interface OrderRepositoryInterface extends GridRepository
{
    /**
     * @return OrderEntity[]
     */
    public function fetchLists(
        OwnershipContext $ownershipContext,
        OrderSearchStruct $orderSearchStruct,
        CurrencyContext $currencyContext
    ): array;

    public function fetchTotalCount(OwnershipContext $ownershipContext, OrderSearchStruct $orderSearchStruct): int;

    public function fetchOrderById(
        IdValue $orderContextId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OrderEntity;

    /**
     * @deprecated tab:v4.2.0 no longer used
     */
    public function fetchOrderContextByListId(IdValue $listId): OrderContext;

    public function fetchAuthIdFromOrderById(IdValue $orderId): IdValue;
}
