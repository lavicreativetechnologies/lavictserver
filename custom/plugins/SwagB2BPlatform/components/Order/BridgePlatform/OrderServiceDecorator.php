<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Checkout\Order\SalesChannel\OrderService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Validation\DataBag\DataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\StateMachine\Aggregation\StateMachineState\StateMachineStateEntity;
use Symfony\Component\HttpFoundation\ParameterBag;

class OrderServiceDecorator extends OrderService
{
    public const ORDER_REFERENCE_KEY = 'b2bOrderReferenceHolder';
    public const REQUESTED_DELIVERY_DATE_KEY = 'b2bDeliveryDateHolder';

    /**
     * @var OrderService
     */
    private $decorated;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var ShopOrderRepository
     */
    private $shopOrderRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var CartService
     */
    private $cartService;

    public function __construct(
        OrderService $decorated,
        OrderContextRepository $orderContextRepository,
        ShopOrderRepository $shopOrderRepository,
        AuthenticationService $authenticationService,
        CartService $cartService
    ) {
        $this->decorated = $decorated;
        $this->orderContextRepository = $orderContextRepository;
        $this->shopOrderRepository = $shopOrderRepository;
        $this->authenticationService = $authenticationService;
        $this->cartService = $cartService;
    }

    public function createOrder(DataBag $data, SalesChannelContext $context): string
    {
        if (!$this->authenticationService->isB2b() || !$this->hasAdditionalOrderData($data)) {
            return $this->decorated->createOrder($data, $context);
        }

        $orderReference = $data->get(self::ORDER_REFERENCE_KEY);
        $deliveryDate = $data->get(self::REQUESTED_DELIVERY_DATE_KEY);

        $cart = $this->cartService
            ->getCart($context->getToken(), $context);

        $cart->addExtension(AdditionalDataExtension::NAME,
            new AdditionalDataExtension($orderReference, $deliveryDate));

        $orderId = $this->decorated->createOrder($data, $context);

        $orderContext = $this->shopOrderRepository
            ->fetchOneOrderContextByShopOrderId(IdValue::create($orderId));

        $orderContext->orderReference = $orderReference;
        $orderContext->requestedDeliveryDate = $deliveryDate;

        $this->orderContextRepository->updateContext($orderContext);

        return $orderId;
    }

    public function orderStateTransition(
        string $orderId,
        string $transition,
        ParameterBag $data,
        Context $context,
        ?string $customerId = null
    ): StateMachineStateEntity {
        return $this->decorated->orderStateTransition(
            $orderId,
            $transition,
            $data,
            $context,
            $customerId);
    }

    public function orderTransactionStateTransition(
        string $orderTransactionId,
        string $transition,
        ParameterBag $data,
        Context $context
    ): StateMachineStateEntity {
        return $this->decorated->orderTransactionStateTransition(
            $orderTransactionId,
            $transition,
            $data,
            $context
        );
    }

    public function orderDeliveryStateTransition(
        string $orderDeliveryId,
        string $transition,
        ParameterBag $data,
        Context $context
    ): StateMachineStateEntity {
        return $this->decorated->orderDeliveryStateTransition(
            $orderDeliveryId,
            $transition,
            $data,
            $context
        );
    }

    /**
     * @internal
     */
    protected function hasAdditionalOrderData(DataBag $data): bool
    {
        return $data->has(self::ORDER_REFERENCE_KEY)
            || $data->has(self::REQUESTED_DELIVERY_DATE_KEY);
    }
}
