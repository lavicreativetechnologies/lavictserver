<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\Order\Framework\ShopOrderDetailsRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function json_decode;

class ShopOrderDetailsRepository implements ShopOrderDetailsRepositoryInterface
{
    const TABLE_ALIAS = 'orderLineItem';
    const TABLE_NAME = 'order_line_item';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    public function __construct(
        Connection $connection,
        LineItemListService $lineItemListService
    ) {
        $this->connection = $connection;
        $this->lineItemListService = $lineItemListService;
    }

    public function createListByOrderId(IdValue $orderId, OwnershipContext $ownershipContext): LineItemList
    {
        $productData = $this->fetchAllProductsByOrderId($orderId);

        return $this->createListByShopOrderDetailsArray($productData, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function fetchAllProductsByOrderId(IdValue $orderId): array
    {
        return $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.order_id = :orderId')
            ->setParameter('orderId', $orderId->getStorageValue())
            ->execute()
            ->fetchAll();
    }

    /**
     * @internal
     */
    protected function createListByShopOrderDetailsArray(array $shopOrderReferences, OwnershipContext $ownershipContext): LineItemList
    {
        $list = new LineItemList();

        foreach ($shopOrderReferences as $reference) {
            $lineItem = new LineItemReference();
            $this->setLineItemData($lineItem, $reference);

            $list->references[] = $lineItem;
        }

        $list = $this->lineItemListService->createListThroughListObject($list, $ownershipContext);

        $this->lineItemListService->updateListPrices($list, $ownershipContext);

        return $list;
    }

    /**
     * @internal
     */
    protected function setLineItemData(LineItemReference $reference, array $data): void
    {
        $payload = json_decode($data['payload'], true);

        $reference->name = $data['label'];
        $reference->referenceNumber = $payload['productNumber'] ?? null;
        $reference->quantity = (int) $data['quantity'];
        $reference->comment = '';
        $reference->amount = (float) $data['total_price'];
        $reference->amountNet = $this->getAmountNet(json_decode($data['price'], true));
        $reference->mode = LineItemType::create($data['type']);
    }

    private function getAmountNet(array $price): float
    {
        $net = (float) $price['totalPrice'];

        foreach ($price['calculatedTaxes'] as $tax) {
            $net -= (float) $tax['tax'];
        }

        return $net;
    }
}
