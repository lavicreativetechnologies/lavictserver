<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\Core\Framework\Struct\Struct;

class AdditionalDataExtension extends Struct
{
    public const NAME = 'additional-b2b-data';

    /**
     * @var string|null
     */
    private $orderReferenceNumber;

    /**
     * @var string|null
     */
    private $requestedDeliveryDate;

    public function __construct(
        string $orderReferenceNumber = null,
        string $requestedDeliveryDate = null
    ) {
        $this->orderReferenceNumber = $orderReferenceNumber;
        $this->requestedDeliveryDate = $requestedDeliveryDate;
    }

    public function getOrderReferenceNumber(): ?string
    {
        return $this->orderReferenceNumber;
    }

    public function getRequestedDeliveryDate(): ?string
    {
        return $this->requestedDeliveryDate;
    }
}
