<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface;

class OrderStatusInterpreterService implements OrderStatusInterpreterServiceInterface
{
    public function isOpen(OrderContext $orderContext): bool
    {
        return $orderContext->isOrdered();
    }

    public function isClearanceOpen(OrderContext $orderContext): bool
    {
        return $orderContext->status === OrderClearanceRepositoryInterface::STATE_CLEARANCE_OPEN;
    }
}
