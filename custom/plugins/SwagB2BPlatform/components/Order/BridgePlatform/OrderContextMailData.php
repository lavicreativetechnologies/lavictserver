<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class OrderContextMailData
{
    /**
     * @var OrderContext
     */
    private $orderContext;

    public function __construct(OrderContext $orderContext)
    {
        $this->orderContext = $orderContext;
    }

    public static function getOrderContextData(): EventDataType
    {
        $context = new ObjectType();
        $context->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('listId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('orderNumber', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('shippingAddressId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('billingAddressId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('paymentId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('shippingId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('shippingAmount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $context->add('shippingAmountNet', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $context->add('comment', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('deviceType', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('statusId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('status', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('authId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('createdAt', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('requestedDeliveryDate', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('orderReference', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('declinedAt', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('clearedAt', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $context->add('salesRepresentativeAuthId', new ScalarValueType(ScalarValueType::TYPE_STRING));

        /* @deprecated tag:v4.2.0 removed - this is not sensible information */
        $context->add('isEditable', new ScalarValueType(ScalarValueType::TYPE_BOOL));

        return $context;
    }

    public function getId(): string
    {
        return (string) $this->orderContext->id->getValue();
    }

    public function getListId(): string
    {
        return (string) $this->orderContext->listId->getValue();
    }

    public function getOrderNumber(): string
    {
        return $this->orderContext->orderNumber;
    }

    public function getShippingAddressId(): string
    {
        return (string) $this->orderContext->shippingAddressId->getValue();
    }

    public function getBillingAddressId(): string
    {
        return (string) $this->orderContext->billingAddressId->getValue();
    }

    public function getPaymentId(): string
    {
        return (string) $this->orderContext->paymentId->getValue();
    }

    public function getShippingId(): string
    {
        return (string) $this->orderContext->shippingId->getValue();
    }

    public function getShippingAmount(): float
    {
        return $this->orderContext->shippingAmount;
    }

    public function getShippingAmountNet(): float
    {
        return $this->orderContext->shippingAmountNet;
    }

    public function getComment(): string
    {
        return $this->orderContext->comment;
    }

    public function getDeviceType(): string
    {
        return $this->orderContext->deviceType;
    }

    public function getStatusId(): string
    {
        return (string) $this->orderContext->statusId->getValue();
    }

    public function getStatus(): string
    {
        return $this->orderContext->status;
    }

    public function getAuthId(): string
    {
        return (string) $this->orderContext->authId->getValue();
    }

    public function getCreatedAt(): string
    {
        return $this->orderContext->createdAt;
    }

    public function getRequestedDeliveryDate(): string
    {
        return $this->orderContext->requestedDeliveryDate;
    }

    public function getOrderReference(): string
    {
        return $this->orderContext->orderReference;
    }

    public function getDeclinedAt(): string
    {
        return $this->orderContext->declinedAt;
    }

    public function getClearedAt(): string
    {
        return $this->orderContext->clearedAt;
    }

    public function getSalesRepresentativeAuthId(): string
    {
        return (string) $this->orderContext->salesRepresentativeAuthId->getValue();
    }

    /**
     * @deprecated tag:v4.2.0 removed - this is not sensible information
     */
    public function isEditable(): bool
    {
        return $this->orderContext->isEditable;
    }
}
