<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Order\OrderEvents;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use function array_key_exists;

class AuthAssigner implements EventSubscriberInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var ShopOrderRepositoryInterface
     */
    private $shopOrderRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        ShopOrderRepositoryInterface $shopOrderRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->shopOrderRepository = $shopOrderRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderEvents::ORDER_WRITTEN_EVENT => 'setOrderIdentity',
        ];
    }

    public function setOrderIdentity(EntityWrittenEvent $event): void
    {
        if (!$this->authenticationService->isB2b()) {
            return;
        }

        $authId = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext()
            ->authId;

        $debtorId = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext()
            ->shopOwnerUserId;

        foreach ($event->getPayloads() as $payload) {
            if (array_key_exists('orderNumber', $payload)) {
                $this->shopOrderRepository
                    ->setOrderIdentity((string) $payload['orderNumber'], $authId);

                $this->shopOrderRepository
                    ->setOrderToShopOwnerUser((string) $payload['orderNumber'], $debtorId);
            }
        }
    }
}
