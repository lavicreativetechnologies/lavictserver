<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderContextShopWriterServiceInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\NotAuthenticatedException;
use Shopware\Core\Checkout\Cart\Delivery\Struct\ShippingLocation;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressCollection;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Payment\PaymentMethodEntity;
use Shopware\Core\Checkout\Shipping\ShippingMethodEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextService;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class OrderContextShopWriterService implements OrderContextShopWriterServiceInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var EntityRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $shippingMethodRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $paymentMethodRepository;

    /**
     * @var SalesChannelContextPersister
     */
    private $contextPersister;

    public function __construct(
        ContextProvider $contextProvider,
        EntityRepositoryInterface $addressRepository,
        EntityRepositoryInterface $shippingMethodRepository,
        EntityRepositoryInterface $paymentMethodRepository,
        SalesChannelContextPersister $contextPersister
    ) {
        $this->contextProvider = $contextProvider;
        $this->addressRepository = $addressRepository;
        $this->shippingMethodRepository = $shippingMethodRepository;
        $this->paymentMethodRepository = $paymentMethodRepository;
        $this->contextPersister = $contextPersister;
    }

    public function extendCart(OrderContext $orderContext): void
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        $newSalesChannelContext = $this->updateSalesChannelContext($orderContext, $salesChannelContext);

        $this->contextProvider->setSalesChannelContext($newSalesChannelContext);
    }

    /**
     * @internal
     */
    protected function updateSalesChannelContext(OrderContext $orderContext, SalesChannelContext $salesChannelContext): SalesChannelContext
    {
        $customer = $salesChannelContext->getCustomer();

        if ($customer === null) {
            throw new NotAuthenticatedException('Can not update the context without a valid login');
        }

        $addresses = $this->loadAddresses($orderContext, $salesChannelContext);
        $shippingMethod = $this->loadShippingMethod($orderContext, $salesChannelContext);
        $paymentMethod = $this->loadPaymentMethod($orderContext, $salesChannelContext);

        $shippingLocation = ShippingLocation::createFromAddress($addresses->get($orderContext->shippingAddressId->getValue()));

        $this->updateCustomer($orderContext, $customer, $addresses);
        $this->updateContextPersistence($salesChannelContext, $customer, $shippingMethod, $paymentMethod);

        return new SalesChannelContext(
            $salesChannelContext->getContext(),
            $salesChannelContext->getToken(),
            $salesChannelContext->getSalesChannel(),
            $salesChannelContext->getCurrency(),
            $salesChannelContext->getCurrentCustomerGroup(),
            $salesChannelContext->getFallbackCustomerGroup(),
            $salesChannelContext->getTaxRules(),
            $paymentMethod,
            $shippingMethod,
            $shippingLocation,
            $customer,
            $salesChannelContext->getRuleIds()
        );
    }

    /**
     * @internal
     */
    protected function loadShippingMethod(OrderContext $orderContext, SalesChannelContext $salesChannelContext): ShippingMethodEntity
    {
        try {
            $criteria = new Criteria([$orderContext->shippingId->getValue()]);
        } catch (InconsistentCriteriaIdsException $e) {
            return $salesChannelContext->getShippingMethod();
        }

        $context = $salesChannelContext->getContext();

        $result = $this->shippingMethodRepository
            ->search($criteria, $context);

        if ($result->count() === 0) {
            return $salesChannelContext->getShippingMethod();
        }

        return $result
            ->get($orderContext->shippingId->getValue());
    }

    /**
     * @internal
     */
    protected function loadPaymentMethod(OrderContext $orderContext, SalesChannelContext $salesChannelContext): PaymentMethodEntity
    {
        try {
            $criteria = new Criteria([$orderContext->paymentId->getValue()]);
        } catch (InconsistentCriteriaIdsException $e) {
            return $salesChannelContext->getPaymentMethod();
        }

        $context = $salesChannelContext->getContext();

        $result = $this->paymentMethodRepository
            ->search($criteria, $context);

        if ($result->count() === 0) {
            return $salesChannelContext->getPaymentMethod();
        }

        return $result
            ->get($orderContext->paymentId->getValue());
    }

    /**
     * @internal
     */
    protected function loadAddresses(OrderContext $orderContext, SalesChannelContext $salesChannelContext): CustomerAddressCollection
    {
        $criteria = new Criteria([
            $orderContext->shippingAddressId->getValue(),
            $orderContext->billingAddressId->getValue(),
        ]);
        $criteria->addAssociation('country');

        $result = $this->addressRepository->search(
            $criteria,
            $salesChannelContext->getContext()
        );

        return $result->getEntities();
    }

    /**
     * @internal
     */
    protected function updateCustomer(OrderContext $orderContext, CustomerEntity $customer, CustomerAddressCollection $addresses): void
    {
        $customer->setActiveBillingAddress($addresses->get($orderContext->billingAddressId->getValue()));
        $customer->setActiveShippingAddress($addresses->get($orderContext->shippingAddressId->getValue()));
    }

    /**
     * @internal
     */
    protected function updateContextPersistence(
        SalesChannelContext $salesChannelContext,
        CustomerEntity $customer,
        ShippingMethodEntity $shippingMethod,
        PaymentMethodEntity $paymentMethodEntity
    ): void {
        $this->contextPersister->save($salesChannelContext->getToken(), [
                SalesChannelContextService::BILLING_ADDRESS_ID => $customer->getActiveBillingAddress()->getId(),
                SalesChannelContextService::SHIPPING_ADDRESS_ID => $customer->getActiveShippingAddress()->getId(),
                SalesChannelContextService::SHIPPING_METHOD_ID => $shippingMethod->getId(),
                SalesChannelContextService::PAYMENT_METHOD_ID => $paymentMethodEntity->getId(),
                SalesChannelContextService::CUSTOMER_ID => $customer->getId(),
            ],
            $salesChannelContext->getSalesChannel()->getId(),
            $customer->getId()
        );
    }
}
