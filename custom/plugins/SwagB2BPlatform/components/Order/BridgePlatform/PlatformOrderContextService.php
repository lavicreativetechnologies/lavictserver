<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Order\Framework\AcceptedOrderClearanceRepository;
use Shopware\B2B\Order\Framework\OrderAuditLogService;
use Shopware\B2B\Order\Framework\OrderCheckoutProviderInterface;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderContextService;
use Shopware\B2B\Order\Framework\OrderContextShopWriterServiceInterface;
use Shopware\B2B\Order\Framework\OrderSource;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class PlatformOrderContextService extends OrderContextService
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        AuthenticationService $authenticationService,
        OrderContextRepository $orderContextRepository,
        OrderAuditLogService $orderClearanceAuditLogService,
        OrderCheckoutProviderInterface $checkoutProvider,
        OrderContextShopWriterServiceInterface $orderContextShopWriterService,
        AcceptedOrderClearanceRepository $acceptedOrderClearanceRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct(
            $authenticationService,
            $orderContextRepository,
            $orderClearanceAuditLogService,
            $checkoutProvider,
            $orderContextShopWriterService,
            $acceptedOrderClearanceRepository
        );

        $this->eventDispatcher = $eventDispatcher;
    }

    public function createContextThroughCheckoutSource(
        OwnershipContext $ownershipContext,
        LineItemList $list,
        OrderSource $orderSource,
        string $orderNumber = ''
    ): OrderContext {
        $orderContext = parent::createContextThroughCheckoutSource(
            $ownershipContext,
            $list,
            $orderSource,
            $orderNumber
        );

        $this->eventDispatcher->dispatch(new OrderContextCreatedEvent($orderContext));

        return $orderContext;
    }
}
