<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface;
use Shopware\B2B\Order\Framework\ShopOrderStatusOutOfSyncException;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Checkout\Order\OrderStates;
use function json_decode;
use function sprintf;

class ShopOrderRepository implements ShopOrderRepositoryInterface
{
    const TABLE_ALIAS = 'shopOrder';
    const TABLE_NAME = 'order';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function setOrderIdentity(string $orderNumber, IdValue $authId): void
    {
        $orderId = $this->fetchOrderIdByOrderNumber($orderNumber);

        $this->connection->executeUpdate('
            UPDATE `order`
            SET `custom_fields` = JSON_SET(`custom_fields`, "$.b2b_auth_id", :authId)
            WHERE `id` = :orderId
        ', ['authId' => $authId->getStorageValue(), 'orderId' => $orderId->getStorageValue()]);
    }

    public function setOrderToShopOwnerUser(string $orderNumber, IdValue $debtorId): void
    {
        $orderId = $this->fetchOrderIdByOrderNumber($orderNumber);

        $this->connection->executeUpdate(
            'UPDATE order_customer INNER JOIN customer c on c.id = :customerId
             SET order_customer.customer_id     = c.id,
                 order_customer.first_name      = c.first_name,
                 order_customer.last_name       = c.last_name,
                 order_customer.salutation_id   = c.salutation_id,
                 order_customer.title           = c.title,
                 order_customer.email           = c.email,
                 order_customer.title           = c.title,
                 order_customer.company         = c.company,
                 order_customer.customer_number = c.customer_number,
                 order_customer.updated_at      = NOW()
             WHERE order_customer.order_id = :orderId',
            [
                'customerId' => $debtorId->getStorageValue(),
                'orderId' => $orderId->getStorageValue(),
            ]
        );
    }

    public function setOrderCommentByOrderContextId(IdValue $orderContextId, string $comment): void
    {
        $this->connection->update(
            'b2b_order_context',
            ['comment' => $comment],
            ['id' => $orderContextId->getStorageValue()]
        );
    }

    public function fetchOneOrderContextByShopOrderId(IdValue $shopOrderId): OrderContext
    {
        $rawOrderContextData = $this->connection->createQueryBuilder()
            ->select('orderContext.*')
            ->from('b2b_order_context', 'orderContext')
            ->innerJoin('orderContext', '`' . self::TABLE_NAME . '`', self::TABLE_ALIAS, self::TABLE_ALIAS . '.order_number = orderContext.ordernumber')
            ->where(self::TABLE_ALIAS . '.id  = :shopOrderId')
            ->setParameter('shopOrderId', $shopOrderId->getStorageValue())
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$rawOrderContextData) {
            throw new NotFoundException('Could not find order context with id "' . $shopOrderId->getValue() . '".');
        }

        $context = new OrderContext();
        $context->fromDatabaseArray($rawOrderContextData);

        return $context;
    }

    /**
     * @internal
     * @throws NotFoundException
     */
    protected function fetchOrderIdByOrderNumber(string $orderNumber): IdValue
    {
        $orderId = $this->connection->createQueryBuilder()
            ->select('id')
            ->from('`order`')
            ->where('`order_number` = :orderNumber')
            ->setParameter('orderNumber', $orderNumber)
            ->execute()
            ->fetchColumn();

        if (!$orderId) {
            throw new NotFoundException(sprintf('order id by number id %s not found', $orderNumber));
        }

        return IdValue::create($orderId);
    }

    public function fetchOrderById(IdValue $orderId): array
    {
        //ToDo find a cleaner solution
        $result = $this->connection->executeQuery('
            SELECT
                `order`.order_number AS ordernumber,
                state.technical_name AS state,
                `order`.order_date_time AS ordertime,
                `order`.shipping_costs AS shipping_costs,
                order_customer.customer_id AS userID,
                order_transaction.id as paymentID
            FROM `order`
            LEFT JOIN order_customer ON order_customer.order_id = `order`.id AND order_customer.order_version_id = `order`.version_id
            LEFT JOIN order_transaction ON order_transaction.order_id = `order`.id AND order_transaction.order_version_id = `order`.version_id
            INNER JOIN state_machine_state state ON `order`.state_id = state.id
            WHERE `order`.id = :orderId
            LIMIT 1;
        ', ['orderId' => $orderId->getStorageValue()])->fetch();

        if (!$result) {
            throw new NotFoundException('Unable to fetch order with id "' . $orderId->getValue() . '"');
        }

        $shippingCosts = json_decode($result['shipping_costs'], true);
        $totalShipping = $netShipping = $shippingCosts['totalPrice'];

        foreach ($shippingCosts['calculatedTaxes'] as $tax) {
            $netShipping -= $tax['tax'];
        }

        $result['invoice_shipping'] = $totalShipping;
        $result['invoice_shipping_net'] = $netShipping;
        unset($result['shipping_costs']);

        return $result;
    }

    public function testStatusInSync(IdValue $orderContextId, string $technicalStatusName): void
    {
        $result = $this->connection->fetchAssoc('
            SELECT  LOWER(HEX(shopOrder.id)) AS id, LOWER(HEX(shopOrder.state_id)) AS state_id
            FROM `' . self::TABLE_NAME . '` as shopOrder
            INNER JOIN b2b_order_context b2bOrder ON b2bOrder.ordernumber = shopOrder.order_number
            WHERE b2bOrder.id = :id
        ', ['id' => $orderContextId->getStorageValue()]);

        if (!$result || !$result['id']) {
            throw new ShopOrderStatusOutOfSyncException(
                'No order found as referenced by order context "' . $orderContextId->getValue() . '" with "' . $technicalStatusName . '"'
            );
        }

        if ($result['state_id'] !== $this->fetchStatusIdByTechnicalName($technicalStatusName)) {
            throw new ShopOrderStatusOutOfSyncException(
                'Order has wrong state found as referenced by order context "' . $orderContextId->getValue() . '" with "' . $technicalStatusName . '"'
            );
        }
    }

    public function updateStatusByName(IdValue $orderContextId, string $technicalStatusName, OwnershipContext $ownershipContext): void
    {
        $orderNumber = $this->connection->fetchColumn(
            'SELECT ordernumber FROM b2b_order_context WHERE id=:id',
            ['id' => $orderContextId]
        );

        if (!$orderNumber) {
            return;
        }

        $statement = $this->connection->prepare('
            UPDATE `' . self::TABLE_NAME . '` as shopOrder
            INNER JOIN b2b_order_context b2bOrder ON b2bOrder.ordernumber = shopOrder.order_number
            SET shopOrder.state_id = :status
            WHERE b2bOrder.id = :id
            AND b2bOrder.auth_id IN (SELECT id FROM b2b_store_front_auth WHERE b2bOrder.auth_id = :ownerId OR context_owner_id = :ownerId);
        ');

        $params = [
            'status' => IdValue::create($this->fetchStatusIdByTechnicalName($technicalStatusName))->getStorageValue(),
            'id' => $orderContextId->getStorageValue(),
            'ownerId' => $ownershipContext->contextOwnerId->getStorageValue(),
        ];

        $statement->execute($params);

        if ($statement->rowCount() !== 1) {
            throw new CanNotUpdateExistingRecordException(
                'Could not update shop order state as referenced by order context "' . $orderContextId->getValue() . '" to "' . $technicalStatusName . '"'
            );
        }
    }

    /**
     * @internal
     */
    protected function fetchStatusIdByTechnicalName(string $name): string
    {
        return (string)
            $this->connection->createQueryBuilder()
                ->select('LOWER(HEX(state.id))')
                ->from('state_machine_state', 'state')
                ->innerJoin('state', 'state_machine', 'stateMachine', 'state.state_machine_id = stateMachine.id')
                ->where('state.technical_name = :name')
                ->andWhere('stateMachine.technical_name = :stateMachineName')
                ->setParameter('name', $name)
                ->setParameter('stateMachineName', OrderStates::STATE_MACHINE)
                ->execute()
                ->fetchColumn();
    }

    public function updateStatus(IdValue $orderContextId, IdValue $statusId, OwnershipContext $ownershipContext): void
    {
        $technicalName = (string) $this->connection->fetchColumn('
            SELECT state.technical_name
            FROM state_machine_state state
            WHERE state.id = :id
        ', ['id' => $statusId->getStorageValue()]);

        $this->updateStatusByName($orderContextId, $technicalName, $ownershipContext);
    }
}
