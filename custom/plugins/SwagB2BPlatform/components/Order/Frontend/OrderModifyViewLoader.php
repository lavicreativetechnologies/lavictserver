<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Frontend;

use Shopware\B2B\Order\Framework\OrderEntity;
use function array_merge;

class OrderModifyViewLoader implements OrderModifyViewInterface
{
    /**
     * @var OrderModifyViewInterface[]
     */
    private $orderViewModifiers = [];

    /**
     * @param OrderModifyViewInterface[] $orderViewModifiers
     */
    public function __construct(array $orderViewModifiers)
    {
        $this->orderViewModifiers = $orderViewModifiers;
    }

    public function modifyMasterDataView(OrderEntity $order): array
    {
        $additionalOrderData = [];
        foreach ($this->orderViewModifiers as $orderViewModifier) {
            $additionalOrderData = array_merge($additionalOrderData, $orderViewModifier->modifyMasterDataView($order));
        }

        return $additionalOrderData;
    }
}
