<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Frontend;

use Shopware\B2B\Order\Framework\OrderEntity;

interface OrderModifyViewInterface
{
    public function modifyMasterDataView(OrderEntity $order): array;
}
