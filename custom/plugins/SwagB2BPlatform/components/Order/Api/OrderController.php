<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Api;

use Shopware\B2B\AuditLog\Framework\AuditLogRepository;
use Shopware\B2B\AuditLog\Framework\AuditLogSearchStruct;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderConversionService;
use Shopware\B2B\Order\Framework\OrderRepositoryInterface;
use Shopware\B2B\Order\Framework\OrderSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OrderController
{
    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var GridHelper
     */
    private $orderGridHelper;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var AuditLogRepository
     */
    private $auditLogRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var OrderConversionService
     */
    private $orderConversionService;

    public function __construct(
        CurrencyService $currencyService,
        GridHelper $orderGridHelper,
        OrderRepositoryInterface $orderRepository,
        AuditLogRepository $auditLogRepository,
        OrderConversionService $orderConversionService,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->currencyService = $currencyService;
        $this->orderGridHelper = $orderGridHelper;
        $this->orderRepository = $orderRepository;
        $this->auditLogRepository = $auditLogRepository;
        $this->orderConversionService = $orderConversionService;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(Request $request): array
    {
        $search = new OrderSearchStruct();
        $ownershipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $this->orderGridHelper
            ->extractSearchDataInRestApi($request, $search);

        $orderLists = $this->orderRepository
            ->fetchLists($ownershipContext, $search, $currencyContext);

        $totalCount = $this->orderRepository
            ->fetchTotalCount($ownershipContext, $search);

        return ['success' => true, 'orders' => $orderLists, 'totalCount' => $totalCount];
    }

    public function getAction(string $orderContextId): array
    {
        $orderContextId = IdValue::create($orderContextId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $order = $this->orderRepository->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        return ['success' => true, 'orderContext' => $order];
    }

    public function getLogAction(string $orderContextId, Request $request): array
    {
        $orderContextId = IdValue::create($orderContextId);
        $currencyContext = $this->currencyService->createCurrencyContext();

        $searchStruct = new AuditLogSearchStruct();
        $this->orderGridHelper
            ->extractSearchDataInRestApi($request, $searchStruct);

        $logItems = $this->auditLogRepository
            ->fetchList(OrderContextRepository::TABLE_NAME, $orderContextId, $searchStruct, $currencyContext);

        $totalCount = $this->auditLogRepository
            ->fetchTotalCount(OrderContextRepository::TABLE_NAME, $orderContextId, $searchStruct);

        return [
            'success' => true,
            'logs' => $logItems,
            'totalCount' => $totalCount,
        ];
    }

    public function convertToB2bAction(string $shopOrderId): array
    {
        $shopOrderId = IdValue::create($shopOrderId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $orderContext = $this->orderConversionService
            ->convertOrderToB2bOrderContext($shopOrderId, $ownershipContext);

        return [
            'success' => true,
            'orderContext' => $orderContext,
        ];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
