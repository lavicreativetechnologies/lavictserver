<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class OrderApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/order',
                'b2b_order.api_order_controller',
                'getList',
                [],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order/{orderContextId}',
                'b2b_order.api_order_controller',
                'get',
                ['orderContextId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order/{orderContextId}/log',
                'b2b_order.api_order_controller',
                'getLog',
                ['orderContextId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/order/{shopOrderId}',
                'b2b_order.api_order_controller',
                'convertToB2b',
                ['shopOrderId'],
            ],
        ];
    }
}
