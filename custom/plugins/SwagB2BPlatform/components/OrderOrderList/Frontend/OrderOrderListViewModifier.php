<?php declare(strict_types=1);

namespace Shopware\B2B\OrderOrderList\Frontend;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Order\Framework\OrderEntity;
use Shopware\B2B\Order\Frontend\OrderModifyViewInterface;
use Shopware\B2B\OrderList\Framework\OrderListRelationRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class OrderOrderListViewModifier implements OrderModifyViewInterface
{
    /**
     * @var OrderListRelationRepositoryInterface
     */
    private $repository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        OrderListRelationRepositoryInterface $repository,
        AuthenticationService $authenticationService
    ) {
        $this->repository = $repository;
        $this->authenticationService = $authenticationService;
    }

    public function modifyMasterDataView(OrderEntity $order): array
    {
        $identity = $this->authenticationService->getIdentity();

        try {
            return ['orderInfo' => $this->repository
                ->fetchOrderListNameForListId($order->listId, $identity->getOwnershipContext()), ];
        } catch (NotFoundException $e) {
            //nth
        }

        return [];
    }
}
