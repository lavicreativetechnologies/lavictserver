<?php declare(strict_types=1);

namespace Shopware\B2B\OrderOrderList\Framework\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\LineItemList\Framework\DependencyInjection\LineItemListFrameworkConfiguration;
use Shopware\B2B\Order\Framework\DependencyInjection\OrderFrameworkConfiguration;
use Shopware\B2B\OrderList\Framework\DependencyInjection\OrderListFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderOrderListFrameworkConfiguration extends DependencyInjectionConfiguration
{
    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/framework-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new LineItemListFrameworkConfiguration(),
            new OrderListFrameworkConfiguration(),
            new OrderFrameworkConfiguration(),
        ];
    }
}
