<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\InStock\BridgePlatform\B2bInStockDefinition;
use Shopware\B2B\OrderNumber\BridgePlatform\B2bOrderNumberDefinition;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class SalesChannelProductExtension extends EntityExtension
{
    public const B2B_IN_STOCK_EXTENSION_NAME = 'b2bInStock';
    public const B2B_ORDER_NUMBER_EXTENSION_NAME = 'b2bOrderNumber';

    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(new OneToManyAssociationField(self::B2B_IN_STOCK_EXTENSION_NAME, B2bInStockDefinition::class, 'product_id'));
        $collection->add(new OneToManyAssociationField(self::B2B_ORDER_NUMBER_EXTENSION_NAME, B2bOrderNumberDefinition::class, 'product_id'));
    }

    public function getDefinitionClass(): string
    {
        return ProductDefinition::class;
    }
}
