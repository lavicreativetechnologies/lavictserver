<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Shop\Framework\OrderRelationServiceInterface;
use Shopware\Core\Checkout\Payment\PaymentMethodEntity;
use Shopware\Core\Checkout\Shipping\ShippingMethodEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

class OrderRelationService implements OrderRelationServiceInterface
{
    /**
     * @var EntityRepositoryInterface
     */
    private $shippingMethodRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $paymentMethodRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        EntityRepositoryInterface $shippingMethodRepository,
        EntityRepositoryInterface $paymentMethodRepository,
        ContextProvider $contextProvider
    ) {
        $this->shippingMethodRepository = $shippingMethodRepository;
        $this->paymentMethodRepository = $paymentMethodRepository;
        $this->contextProvider = $contextProvider;
    }

    public function getShippingNameForId(IdValue $shippingId): string
    {
        if ($shippingId instanceof NullIdValue) {
            throw new NotFoundException('Unable to find shipping method');
        }

        try {
            $criteria = new Criteria([$shippingId->getValue()]);
            /** @var ShippingMethodEntity|null $shippingMethod */
            $shippingMethod = $this->shippingMethodRepository->search(
                $criteria,
                $this->contextProvider->getContext()
            )->get($shippingId->getValue());

            if (!$shippingMethod) {
                throw new NotFoundException('Unable to find shipping method with id ' . $shippingId->getValue());
            }

            return $shippingMethod->getName();
        } catch (InconsistentCriteriaIdsException $exception) {
            throw new NotFoundException('Unable to find shipping method with id ' . $shippingId->getValue());
        }
    }

    public function getPaymentNameForId(IdValue $paymentId): string
    {
        if ($paymentId instanceof NullIdValue) {
            throw new NotFoundException('Unable to find payment method');
        }

        try {
            $criteria = new Criteria([$paymentId->getValue()]);
            /** @var PaymentMethodEntity|null $paymentMethod */
            $paymentMethod = $this->paymentMethodRepository->search(
                $criteria,
                $this->contextProvider->getContext()
            )->get($paymentId->getValue());

            if (!$paymentMethod) {
                throw new NotFoundException('Unable to find payment method with id ' . $paymentId->getValue());
            }

            return $paymentMethod->getName();
        } catch (InconsistentCriteriaIdsException $exception) {
            throw new NotFoundException('Unable to find payment method with id ' . $paymentId->getValue());
        }
    }
}
