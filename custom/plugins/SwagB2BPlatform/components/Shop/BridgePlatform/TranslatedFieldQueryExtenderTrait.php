<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;
use Shopware\Core\Framework\Context;
use function implode;
use function sprintf;
use function str_replace;
use function uniqid;

trait TranslatedFieldQueryExtenderTrait
{
    /**
     * @internal
     */
    protected function addTranslatedFieldSelect(
        QueryBuilder $query,
        string $columnName,
        string $columnAlias,
        string $translationTableName,
        string $joinTableName,
        string $joinTableAlias,
        string $joinField,
        Context $context,
        bool $hasVersion = false
    ): void {
        $columns = [];
        $mainTableName = str_replace('_translation', '', $translationTableName);
        $primaryIdColumn = $mainTableName . '_id';
        $versionIdColumn = $mainTableName . '_version_id';
        $translationQuery = $query->getConnection()->createQueryBuilder();
        $translationQuery->from($joinTableName, $joinTableAlias);

        foreach ($context->getLanguageIdChain() as $languageId) {
            $language = IdValue::create($languageId);
            $alias = uniqid($translationTableName, false);
            $languageParameter = 'languageId' . $alias;
            $joinCondition = sprintf(
                '%s.%s = %s.%s AND %s.language_id = :%s',
                $joinTableAlias,
                $joinField,
                $alias,
                $primaryIdColumn,
                $alias,
                $languageParameter
            );

            if ($hasVersion) {
                $joinCondition .= sprintf(' AND %s.%s = :versionId', $alias, $versionIdColumn);
                $query->setParameter('versionId', IdValue::create($context->getVersionId())->getStorageValue());
            }

            $translationQuery->leftJoin(
                $joinTableAlias,
                $translationTableName,
                $alias,
                $joinCondition
            );
            $query->setParameter($languageParameter, $language->getStorageValue());

            $columns[] = $alias . '.' . $columnName;
        }

        $translationQuery->addSelect(sprintf('DISTINCT COALESCE(%s) as %s', implode(',', $columns), $columnAlias));
        $translationQuery->addSelect($joinTableAlias . '.' . $joinField . ' as ' . $primaryIdColumn);

        $joinAlias = $columnAlias . '_translation';
        $query->addSelect($joinAlias . '.' . $columnAlias);

        $query->leftJoin($joinTableAlias, '(' . $translationQuery->getSQL() . ')', $joinAlias, sprintf(
            '%s.%s = %s.%s',
            $joinTableAlias,
            $joinField,
            $joinAlias,
            $primaryIdColumn
        ));
    }
}
