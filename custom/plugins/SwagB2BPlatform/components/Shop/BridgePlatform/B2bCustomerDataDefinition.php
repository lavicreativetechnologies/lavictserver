<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class B2bCustomerDataDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'b2b_customer_data';
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new FkField('customer_id', 'customer_id', CustomerDefinition::class))->addFlags(new Required(), new PrimaryKey()),
            new BoolField('is_debtor', 'isDebtor'),
            new BoolField('is_sales_representative', 'isSalesRepresentative'),

            new OneToOneAssociationField('customer', 'customer_id', 'id', CustomerDefinition::class, false),
        ]);
    }
}
