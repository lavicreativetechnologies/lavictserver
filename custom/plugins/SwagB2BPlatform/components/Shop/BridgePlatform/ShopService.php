<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Shop\Framework\ShopServiceInterface;

class ShopService implements ShopServiceInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(ContextProvider $contextProvider)
    {
        $this->contextProvider = $contextProvider;
    }

    public function getRootCategoryId(): IdValue
    {
        return IdValue::create($this->contextProvider->getSalesChannelContext()->getSalesChannel()->getNavigationCategoryId());
    }

    public function getCurrentCurrencyFactor(): float
    {
        return $this->contextProvider->getSalesChannelContext()->getCurrency()->getFactor();
    }

    public function getCurrentCurrencySymbol(): string
    {
        return $this->contextProvider->getSalesChannelContext()->getCurrency()->getSymbol();
    }
}
