<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Shop\Framework\RoundingInterface;
use Shopware\Core\Checkout\Cart\Price\PriceRounding;

class RoundingService implements RoundingInterface
{
    private const DECIMAL_PRECISION = 2;

    /**
     * @var PriceRounding
     */
    private $priceRounding;

    public function __construct(
        PriceRounding $priceRounding
    ) {
        $this->priceRounding = $priceRounding;
    }

    public function round(float $netPrice, float $tax = 0, int $quantity = null): float
    {
        if (!$quantity) {
            $quantity = 1;
        }

        return $this->priceRounding->round(
            ($netPrice / 100 * (100 + $tax)) * $quantity,
            self::DECIMAL_PRECISION
        );
    }
}
