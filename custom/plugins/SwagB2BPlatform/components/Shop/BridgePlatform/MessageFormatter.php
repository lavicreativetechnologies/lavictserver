<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Shop\Framework\MessageFormatterInterface;
use Shopware\B2B\Shop\Framework\SessionStorageInterface;

class MessageFormatter implements MessageFormatterInterface
{
    /**
     * @var SessionStorageInterface
     */
    private $sessionStorage;

    public function __construct(SessionStorageInterface $sessionStorage)
    {
        $this->sessionStorage = $sessionStorage;
    }

    public function formatSessionMessage(string $sessionMessageName): void
    {
        $message = $this->sessionStorage->get($sessionMessageName);

        if ($message) {
            $formattedMessage = [
                $message[0] => [[
                    'property' => null,
                    'snippetKey' => $message[1],
                    'messageTemplate' => null,
                    'parameters' => $message[2] ?? null,
                    /*
                     * @deprecated tag:v4.2.0 - will be removed, use 'parameters' instead
                     */
                    'parameter' => $message[2] ?? null,
                    'cause' => null,
                ]],
            ];

            $this->sessionStorage->set($sessionMessageName, $formattedMessage);
        }
    }
}
