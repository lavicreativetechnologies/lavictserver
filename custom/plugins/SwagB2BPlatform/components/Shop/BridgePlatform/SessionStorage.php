<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Shop\Framework\SessionStorageInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use function class_alias;

/**
 * @deprecated tag:v4.4.0 Will be renamed to Storage and will store data in the SalesChannelContext, instead of Session.
 */
class SessionStorage implements SessionStorageInterface
{
    /**
     * @var Session
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function set(string $key, $value): void
    {
        $this->session->set($key, $value);
    }

    public function get(string $key)
    {
        return $this->session->get($key);
    }

    public function remove(string $key): void
    {
        $this->session->remove($key);
    }
}

class_alias('\Shopware\B2B\Shop\BridgePlatform\SessionStorage', '\Shopware\B2B\Shop\BridgePlatform\Storage');
