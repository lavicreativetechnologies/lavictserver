<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CustomerExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(new OneToOneAssociationField('b2bCustomerData', 'id', 'customer_id', B2bCustomerDataDefinition::class, false));
    }

    public function getDefinitionClass(): string
    {
        return CustomerDefinition::class;
    }
}
