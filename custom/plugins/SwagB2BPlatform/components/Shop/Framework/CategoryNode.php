<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

use Shopware\B2B\Common\IdValue;

class CategoryNode
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     */
    public $hasChildren;

    public function __construct(IdValue $id, string $title, bool $hasChildren)
    {
        $this->id = $id;
        $this->name = $title;
        $this->hasChildren = $hasChildren;
    }
}
