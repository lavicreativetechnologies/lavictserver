<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

interface MessageFormatterInterface
{
    public function formatSessionMessage(string $sessionMessageName): void;
}
