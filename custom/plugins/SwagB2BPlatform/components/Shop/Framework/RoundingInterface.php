<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

interface RoundingInterface
{
    /**
     * @deprecated tag:v4.4.0 Default value for $quantity will be 1
     */
    public function round(float $netPrice, float $tax = 0, int $quantity = null): float;
}
