<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * @deprecated tag:v4.2 This interface will be removed. Use the TaxProvider from the bridge instead.
 */
interface TaxProviderInterface
{
    public function getDiscountTaxes(array $references, OwnershipContext $ownershipContext): float;

    public function getDiscountTax(LineItemReference $reference, OwnershipContext $ownershipContext): float;

    public function getProductTax(LineItemReference $reference): float;
}
