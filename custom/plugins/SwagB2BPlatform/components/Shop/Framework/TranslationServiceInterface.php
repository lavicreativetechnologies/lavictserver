<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

interface TranslationServiceInterface
{
    public function get(string $name, string $namespace, string $default): string;
}
