<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

use Shopware\B2B\Common\IdValue;

interface OrderRelationServiceInterface
{
    public function getShippingNameForId(IdValue $shippingId): string;

    public function getPaymentNameForId(IdValue $paymentId): string;
}
