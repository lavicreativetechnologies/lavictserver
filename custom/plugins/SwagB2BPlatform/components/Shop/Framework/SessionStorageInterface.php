<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

/**
 * @deprecated tag:v4.4.0 Will be renamed to StorageInterface.
 */
interface SessionStorageInterface
{
    /**
     * @param $value
     */
    public function set(string $key, $value);

    public function get(string $key);

    public function remove(string $key);
}
