<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Debtor\Framework\DependencyInjection\DebtorFrameworkConfiguration;
use Shopware\B2B\LineItemList\Framework\DependencyInjection\LineItemListFrameworkConfiguration;
use Shopware\B2B\Shop\BridgePlatform\DependencyInjection\ShopBridgeConfiguration as PlatformShopBridgeConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ShopBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformShopBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
            new QueryFactoryCompilerPass(),
            new ModelValidOperatorCompilerPass(),
            new RoundingServiceCompilerPass(),
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new LineItemListFrameworkConfiguration(),
            new DebtorFrameworkConfiguration(),
        ];
    }
}
