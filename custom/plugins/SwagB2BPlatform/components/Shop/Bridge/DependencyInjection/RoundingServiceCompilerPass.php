<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Bridge\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RoundingServiceCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has('shopware.cart.net_rounding')) {
            $container->setAlias('b2b_shop.rounding_service', 'b2b_shop.b2b_rounding_service_lower_sw56');

            return;
        }

        $definition = $container->findDefinition('b2b_shop.b2b_rounding_service');
        $definition->addArgument(new Reference('shopware.cart.net_rounding.after_tax'));
        $container->setAlias('b2b_shop.rounding_service', 'b2b_shop.b2b_rounding_service');
    }
}
