<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Debtor\Framework\DebtorApiAuthenticatorInterface;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\Debtor\Framework\DebtorService;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextService;
use function explode;
use function mb_strpos;

class DebtorApiAuthenticator implements DebtorApiAuthenticatorInterface
{
    const PATH_INFO_PREFIX = '/api/b2b/debtor/';

    /**
     * @var DebtorService
     */
    private $debtorService;

    /**
     * @var DebtorRepositoryInterface
     */
    private $debtorRepository;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var SalesChannelContextFactory
     */
    private $salesChannelContextFactory;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    public function __construct(
        DebtorService $debtorService,
        DebtorRepositoryInterface $debtorRepository,
        LoginContextService $loginContextService,
        SalesChannelContextFactory $salesChannelContextFactory,
        ContextProvider $salesChannelContextProvider
    ) {
        $this->debtorService = $debtorService;
        $this->debtorRepository = $debtorRepository;
        $this->loginContextService = $loginContextService;
        $this->salesChannelContextFactory = $salesChannelContextFactory;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
    }

    public function authenticateFromPathInfo(string $pathInfo): void
    {
        if (mb_strpos($pathInfo, self::PATH_INFO_PREFIX) !== 0) {
            return;
        }

        $parts = explode('/', $pathInfo);
        $debtorIdentifier = $parts[4];

        if (Uuid::isValid($debtorIdentifier)) {
            $debtorIdentifier = IdValue::create($debtorIdentifier);
            $this->setSalesChannelContextByAuthenticationIdentifier($debtorIdentifier);
            $this->debtorService->authenticateByAuthenticationIdentifier($debtorIdentifier);

            return;
        }

        $this->setSalesChannelContextByEmail($debtorIdentifier);
        $this->debtorService->authenticateByEmail($debtorIdentifier);
    }

    /**
     * @internal
     */
    protected function setSalesChannelContextByAuthenticationIdentifier(IdValue $debtorIdentifier): void
    {
        $this->setSalesChannelContext($this->debtorRepository->fetchOneById($debtorIdentifier));
    }

    /**
     * @internal
     */
    protected function setSalesChannelContextByEmail(string $email): void
    {
        $this->setSalesChannelContext($this->debtorRepository->fetchOneByEmail($email));
    }

    /**
     * @internal
     */
    protected function setSalesChannelContext(DebtorEntity $debtorEntity): void
    {
        $this->salesChannelContextProvider->setSalesChannelContext(
            $this->salesChannelContextFactory->create(
                Uuid::randomHex(),
                $debtorEntity->subshopID->getValue(),
                [
                    SalesChannelContextService::CUSTOMER_ID => $debtorEntity->id->getValue(),
                ]
            )
        );
    }
}
