<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;

class DebtorEntityFactory
{
    const PASSWORD_DEFAULT_ENCODER = 'bcrypt';

    public function create(CustomerEntity $customerEntity): DebtorEntity
    {
        $debtor = new DebtorEntity();
        $debtor->id = IdValue::create($customerEntity->getId());
        $debtor->subshopID = IdValue::create($customerEntity->getSalesChannelId());
        $debtor->password = $customerEntity->getPassword() ?? $customerEntity->getLegacyPassword();
        $debtor->encoder = $customerEntity->getLegacyEncoder() ?? self::PASSWORD_DEFAULT_ENCODER;
        $debtor->email = $customerEntity->getEmail();
        $debtor->active = $customerEntity->getActive();
        $debtor->paymentID = IdValue::create($customerEntity->getDefaultPaymentMethodId());
        $debtor->firstlogin = $customerEntity->getFirstLogin();
        $debtor->lastlogin = $customerEntity->getLastLogin();
        $debtor->newsletter = $customerEntity->getNewsletter();
        $debtor->customergroup = IdValue::create($customerEntity->getGroupId());

        /*
         * @deprecated tag:v4.4.0 please use $debtor->language instead
         */
        $debtor->languageId = IdValue::create($customerEntity->getLanguageId());
        $debtor->language = IdValue::create($customerEntity->getLanguageId());
        $debtor->default_billing_address_id = IdValue::create($customerEntity->getDefaultBillingAddressId());
        $debtor->default_shipping_address_id = IdValue::create($customerEntity->getDefaultShippingAddressId());
        $debtor->title = $customerEntity->getTitle();
        $debtor->salutation = $customerEntity->getSalutation()->getDisplayName();

        /*
         * @deprecated tag:v4.4.0 will be removed without replacement
         */
        $debtor->salutationId = IdValue::create($customerEntity->getSalutation()->getUniqueIdentifier());
        $debtor->firstName = $customerEntity->getFirstName();
        $debtor->lastName = $customerEntity->getLastName();
        $debtor->birthday = $customerEntity->getBirthday();
        $debtor->customernumber = $customerEntity->getCustomerNumber();

        return $debtor;
    }
}
