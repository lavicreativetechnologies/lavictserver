<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationEntity;
use function is_a;

class DebtorAuthenticationIdentityLoader implements AuthenticationIdentityLoaderInterface
{
    /**
     * @var DebtorRepositoryInterface
     */
    private $debtorRepository;

    public function __construct(
        DebtorRepositoryInterface $debtorRepository
    ) {
        $this->debtorRepository = $debtorRepository;
    }

    public function fetchIdentityByEmail(string $email, LoginContextService $contextService, bool $isApi = false): Identity
    {
        $entity = $this->debtorRepository->fetchOneByEmail($email);

        return $this->getIdentity($entity, $contextService, $isApi);
    }

    public function fetchIdentityByAuthentication(StoreFrontAuthenticationEntity $authentication, LoginContextService $contextService, bool $isApi = false): Identity
    {
        if (!is_a(DebtorRepositoryInterface::class, $authentication->providerKey, true)) {
            throw new NotFoundException('The given authentication, does not belong to this loader');
        }

        $entity = $this->debtorRepository
            ->fetchOneById($authentication->providerContext);

        return $this->getIdentity($entity, $contextService, $isApi);
    }

    /**
     * @internal
     */
    protected function getIdentity(DebtorEntity $entity, LoginContextService $contextService, bool $isApi): DebtorIdentity
    {
        $authId = $contextService->getAuthId(DebtorRepositoryInterface::class, $entity->id);

        return new DebtorIdentity(
            $authId,
            $entity->id,
            $this->debtorRepository->getTableName(),
            $entity,
            $contextService->getAvatar($authId),
            $isApi
        );
    }

    public function fetchIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity, LoginContextService $contextService, bool $isApi = false): Identity
    {
        if (!$credentialsEntity->email) {
            throw new NotFoundException('Unable to handle context');
        }

        $debtorEntity = $this->debtorRepository->fetchOneByCredentials($credentialsEntity);

        return $this->getIdentity($debtorEntity, $contextService, $isApi);
    }
}
