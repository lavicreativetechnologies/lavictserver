<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialQueryExtenderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use function sprintf;

class DebtorRepository implements DebtorRepositoryInterface
{
    const TABLE_NAME = 'customer';

    const TABLE_ALIAS = 'customer';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var CredentialQueryExtenderInterface
     */
    private $credentialQueryExtender;

    /**
     * @var EntityRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var DebtorEntityFactory
     */
    private $debtorEntityFactory;

    public function __construct(
        Connection $connection,
        CredentialQueryExtenderInterface $credentialQueryExtender,
        EntityRepositoryInterface $customerRepository,
        ContextProvider $contextProvider,
        DebtorEntityFactory $debtorEntityFactory
    ) {
        $this->connection = $connection;
        $this->credentialQueryExtender = $credentialQueryExtender;
        $this->customerRepository = $customerRepository;
        $this->contextProvider = $contextProvider;
        $this->debtorEntityFactory = $debtorEntityFactory;
    }

    public function fetchOneByEmail(string $email): DebtorEntity
    {
        /** @var CustomerEntity|null $debtor */
        $debtor = $this->customerRepository->search(
            (new Criteria())
                ->addFilter(new EqualsFilter('email', $email))
                ->addFilter(new EqualsFilter('customer.b2bCustomerData.isDebtor', true))
                ->addAssociation('salutation')
                ->addAssociation('salesChannel')
                ->setLimit(1),
            $this->contextProvider->getContext()
        )->first();

        if (!$debtor) {
            throw new NotFoundException(sprintf('Debtor not found for %s', $email));
        }

        return $this->debtorEntityFactory->create($debtor);
    }

    public function fetchOneByCredentials(AbstractCredentialsEntity $credentialsEntity): DebtorEntity
    {
        /** @var CustomerEntity|null $debtor */
        $debtor = $this->customerRepository->search(
            (new Criteria())
                ->addFilter(new EqualsFilter('email', $credentialsEntity->email))
                ->addFilter(new EqualsFilter('customer.b2bCustomerData.isDebtor', true))
                ->addAssociation('salutation')
                ->setLimit(1),
            $this->contextProvider->getContext()
        )->first();

        if (!$debtor) {
            throw new NotFoundException(sprintf('Debtor not found for %s', $credentialsEntity->email));
        }

        return $this->debtorEntityFactory->create($debtor);
    }

    public function fetchOneById(IdValue $id): DebtorEntity
    {
        /** @var CustomerEntity $debtor */
        $debtor = $this->customerRepository->search(
            (new Criteria([$id->getValue()]))
                ->addFilter(new EqualsFilter('customer.b2bCustomerData.isDebtor', true))
                ->addAssociation('salutation')
                ->addAssociation('salesChannel')
                ->setLimit(1),
            $this->contextProvider->getContext()
        )->get($id->getValue());

        if (!$debtor) {
            throw new NotFoundException(sprintf('Debtor not found for %s', $id->getValue()));
        }

        return $this->debtorEntityFactory->create($debtor);
    }

    /**
     * @return DebtorEntity[]
     */
    public function fetchAllDebtors(): array
    {
        $debtors = $this->customerRepository->search(
            (new Criteria())
                ->addFilter(new EqualsFilter('customer.b2bCustomerData.isDebtor', true))
                ->addAssociation('salutation'),
            $this->contextProvider->getContext()
        );

        if (!$debtors) {
            throw new NotFoundException('No debtor found!');
        }

        $entities = [];
        foreach ($debtors as $debtor) {
            $entities[] = $this->debtorEntityFactory->create($debtor);
        }

        return $entities;
    }

    /**
     * @throws NotFoundException
     */
    public function fetchIdentityById(IdValue $id, LoginContextService $contextService): Identity
    {
        $entity = $this->fetchOneById($id);

        $authId = $contextService->getAuthId(DebtorRepositoryInterface::class, $entity->id);

        return new DebtorIdentity(
            $authId,
            $entity->id,
            self::TABLE_NAME,
            $entity,
            $contextService->getAvatar($authId)
        );
    }

    public function filterActiveDebtor(QueryBuilder $queryBuilder, string $fromAlias, string $idField): void
    {
        $queryBuilder->innerJoin($fromAlias, 'b2b_customer_data', 'b2bcd', 'UNHEX(' . $idField . ') = b2bcd.customer_id')
            ->where('b2bcd.is_debtor = 1');
    }

    public function getTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function setUserAsDebtor(IdValue $userId): void
    {
        $this->connection->update(
            'b2b_customer_data',
            ['is_debtor' => 1],
            ['customer_id' => $userId->getStorageValue()]
        );
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
