<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\Framework;

use DateTimeInterface;
use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractNamedUser;

class DebtorEntity extends AbstractNamedUser implements Entity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $encoder;

    /**
     * @var string
     */
    public $email;

    /**
     * @var bool
     */
    public $active;

    /**
     * @var IdValue
     */
    public $paymentID;

    /**
     * @var DateTimeInterface
     */
    public $firstlogin;

    /**
     * @var DateTimeInterface
     */
    public $lastlogin;

    /**
     * @var int
     */
    public $newsletter;

    /**
     * @var string
     */
    public $customergroup;

    /**
     * @var IdValue
     */
    public $language;

    /**
     * @var IdValue
     */
    public $subshopID;

    /**
     * @var IdValue
     */
    public $default_billing_address_id;

    /**
     * @var IdValue
     */
    public $default_shipping_address_id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $salutation;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $birthday;

    /**
     * @var string
     */
    public $customernumber;

    public function __construct()
    {
        $this->language = IdValue::null();
    }

    public function fromDatabaseArray(array $debtorData): Entity
    {
        $this->id = IdValue::create($debtorData['id']);
        $this->password = $debtorData['password'];
        $this->encoder = $debtorData['encoder'];
        $this->email = $debtorData['email'];
        $this->active = (bool) $debtorData['active'];
        $this->paymentID = IdValue::create($debtorData['paymentID']);
        $this->firstlogin = $debtorData['firstlogin'];
        $this->lastlogin = $debtorData['lastlogin'];
        $this->newsletter = $debtorData['newsletter'];
        $this->customergroup = $debtorData['customergroup'];
        $this->language = IdValue::create($debtorData['language']);
        $this->default_billing_address_id = IdValue::create($debtorData['default_billing_address_id']);
        $this->default_shipping_address_id = IdValue::create($debtorData['default_shipping_address_id']);
        $this->title = (string) $debtorData['title'];
        $this->salutation = (string) $debtorData['salutation'];
        $this->firstName = (string) $debtorData['firstname'];
        $this->lastName = (string) $debtorData['lastname'];
        $this->birthday = $debtorData['birthday'];
        $this->customernumber = (string) $debtorData['customernumber'];

        if (isset($debtorData['sales_channel_id'])) {
            $this->subshopID = IdValue::create($debtorData['sales_channel_id']);
        } elseif (isset($debtorData['subshopID'])) {
            $this->subshopID = IdValue::create($debtorData['subshopID']);
        }

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
