<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\Framework;

interface DebtorApiAuthenticatorInterface
{
    public function authenticateFromPathInfo(string $pathInfo);
}
