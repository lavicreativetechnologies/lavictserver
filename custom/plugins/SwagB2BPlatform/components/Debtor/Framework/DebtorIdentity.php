<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserAddress;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserLoginCredentials;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserOrderCredentials;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserPostalSettings;

class DebtorIdentity implements Identity
{
    /**
     * @var IdValue
     */
    private $id;

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var DebtorEntity
     */
    private $debtorCrudEntity;

    /**
     * @var IdValue
     */
    private $authId;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var bool
     */
    private $isApi;

    public function __construct(
        IdValue $authId,
        IdValue $id,
        string $tableName,
        DebtorEntity $debtorCrudEntity,
        string $avatar = '',
        bool $isApi = false
    ) {
        $this->id = $id;
        $this->tableName = $tableName;
        $this->debtorCrudEntity = $debtorCrudEntity;
        $this->authId = $authId;
        $this->avatar = $avatar;
        $this->isApi = $isApi;
    }

    public function getAuthId(): IdValue
    {
        return $this->authId;
    }

    public function getContextAuthId(): IdValue
    {
        return $this->authId;
    }

    public function getId(): IdValue
    {
        return $this->id;
    }

    public function getTableName(): string
    {
        return $this->tableName;
    }

    public function getEntity(): Entity
    {
        return $this->debtorCrudEntity;
    }

    public function getOwnershipEntity(): Entity
    {
        return $this->debtorCrudEntity;
    }

    public function getLoginCredentials(): UserLoginCredentials
    {
        $debtorEntity = $this->getDebtorEntity();

        return new UserLoginCredentials(
            $debtorEntity->email,
            $debtorEntity->password,
            $debtorEntity->encoder,
            $debtorEntity->active
        );
    }

    public function getPostalSettings(): UserPostalSettings
    {
        $debtorEntity = $this->getDebtorEntity();

        return new UserPostalSettings(
            $debtorEntity->salutation,
            $debtorEntity->title,
            $debtorEntity->firstName,
            $debtorEntity->lastName,
            $debtorEntity->language,
            $debtorEntity->email
        );
    }

    public function getOrderCredentials(): UserOrderCredentials
    {
        $debtorEntity = $this->getDebtorEntity();

        return new UserOrderCredentials(
            $debtorEntity->customernumber,
            $debtorEntity->id,
            $debtorEntity->email
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getMainShippingAddress(): UserAddress
    {
        return new UserAddress($this->getDebtorEntity()->default_shipping_address_id);
    }

    public function getMainBillingAddress(): UserAddress
    {
        return new UserAddress($this->getDebtorEntity()->default_billing_address_id);
    }

    private function getDebtorEntity(): DebtorEntity
    {
        return $this->getEntity();
    }

    public function getOwnershipContext(): OwnershipContext
    {
        return new OwnershipContext(
            $this->authId,
            $this->authId,
            $this->getDebtorEntity()->email,
            $this->getDebtorEntity()->id,
            $this->getDebtorEntity()->id,
            __CLASS__
        );
    }

    public function isSuperAdmin(): bool
    {
        return true;
    }

    public function isApiUser(): bool
    {
        return $this->isApi ? true : false;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): void
    {
        $this->avatar = $avatar;
    }
}
