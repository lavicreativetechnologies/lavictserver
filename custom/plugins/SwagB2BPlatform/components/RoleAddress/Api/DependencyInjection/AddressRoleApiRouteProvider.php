<?php declare(strict_types=1);

namespace Shopware\B2B\RoleAddress\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class AddressRoleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/address_role/{roleId}',
                'b2b_address.api_address_role_controller',
                'getAllAllowed',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/grant',
                'b2b_address.api_address_role_controller',
                'getAllGrant',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/address/{addressId}',
                'b2b_address.api_address_role_controller',
                'getAllowed',
                ['roleId', 'addressId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/address/{addressId}',
                'b2b_address.api_address_role_controller',
                'allow',
                ['roleId', 'addressId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/address/{addressId}/grant',
                'b2b_address.api_address_role_controller',
                'allowGrant',
                ['roleId', 'addressId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/allow',
                'b2b_address.api_address_role_controller',
                'multipleAllow',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/deny',
                'b2b_address.api_address_role_controller',
                'multipleDeny',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/address_role/{roleId}/address/{addressId}',
                'b2b_address.api_address_role_controller',
                'deny',
                ['roleId', 'addressId'],
            ],
        ];
    }
}
