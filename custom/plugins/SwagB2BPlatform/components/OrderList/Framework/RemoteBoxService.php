<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use InvalidArgumentException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function count;
use function str_replace;
use function ucfirst;

class RemoteBoxService
{
    /**
     * @var ValidationException[]
     */
    private $errors = [];

    /**
     * @var OrderListService
     */
    private $orderListService;

    public function __construct(
        OrderListService $orderListService
    ) {
        $this->orderListService = $orderListService;
    }

    public function addError(ValidationException $exception): void
    {
        $this->errors[] = $exception;
    }

    /**
     * @deprecated tag:v4.4.0 - $ownershipContext is no longer used
     */
    public function createLineItemListFromProductsRequest(array $products, OwnershipContext $ownershipContext): LineItemList
    {
        $lineItemList = new LineItemList();

        $references = [];
        foreach ($products as $product) {
            try {
                $reference = $this->orderListService
                    ->createReferenceFromProductRequest($product);

                $references[] = $reference;
            } catch (ValidationException $e) {
                $this->errors[] = $e;
            }
        }

        $lineItemList->references = $references;

        if (!count($lineItemList->references)) {
            throw new InvalidArgumentException('no valid products found');
        }

        $lineItemList->recalculateListAmounts();

        return $lineItemList;
    }

    /**
     * @return array $errors
     */
    public function popValidationResponse(): array
    {
        $errors = [];
        foreach ($this->errors as $error) {
            foreach ($error->getViolations() as $violation) {
                $errors[] = [
                    'property' => ucfirst($violation->getPropertyPath()),
                    'snippetKey' => str_replace(['}', '{', '%', '.', ' '], '', $violation->getMessageTemplate()),
                    'messageTemplate' => $violation->getMessageTemplate(),
                    'parameters' => $violation->getParameters(),
                ];
            }
        }

        $this->errors = [];

        return $errors;
    }
}
