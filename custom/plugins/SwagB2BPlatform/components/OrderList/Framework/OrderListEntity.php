<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use function get_object_vars;
use function property_exists;

class OrderListEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var IdValue
     */
    public $budgetId;

    /**
     * @var IdValue
     */
    public $listId;

    /**
     * @var LineItemList
     */
    public $lineItemList;

    /**
     * @var IdValue
     */
    public $contextOwnerId;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->budgetId = IdValue::null();
        $this->listId = IdValue::null();
        $this->contextOwnerId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'name' => $this->name,
            'budget_id' => $this->budgetId->getStorageValue(),
            'list_id' => $this->listId->getStorageValue(),
            'context_owner_id' => $this->contextOwnerId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $orderListData): CrudEntity
    {
        $this->id = IdValue::create($orderListData['id']);
        $this->name = $orderListData['name'];
        $this->budgetId = IdValue::create($orderListData['budget_id']);
        $this->listId = IdValue::create($orderListData['list_id']);
        $this->contextOwnerId = IdValue::create($orderListData['context_owner_id']);

        $this->castTypes();

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->castTypes();
    }

    public function toArray(): array
    {
        $object = get_object_vars($this);

        unset($object['lineItemList']);

        return $object;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @internal
     */
    protected function castTypes(): void
    {
        $this->id = IdValue::create($this->id);
        $this->listId = IdValue::create($this->listId);
        $this->contextOwnerId = IdValue::create($this->contextOwnerId);
        $this->budgetId = IdValue::create($this->budgetId);
    }
}
