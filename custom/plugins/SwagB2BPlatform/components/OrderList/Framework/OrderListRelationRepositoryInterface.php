<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface OrderListRelationRepositoryInterface
{
    public function fetchOrderListNameForListId(IdValue $listId, OwnershipContext $ownershipContext): string;

    public function fetchOrderListNameForPositionNumber(IdValue $listId, string $productNumber, OwnershipContext $ownershipContext): string;

    public function addOrderListToCartAttribute(LineItemList $list, string $orderListName);
}
