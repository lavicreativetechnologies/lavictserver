<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderListValidationService
{
    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
    }

    public function createInsertValidation(OrderListEntity $orderList): Validator
    {
        return $this->createCrudValidation($orderList)
            ->validateThat('id', $orderList->id)
            ->isNullIdValue()

            ->getValidator($this->validator);
    }

    public function createUpdateValidation(OrderListEntity $orderList): Validator
    {
        return $this->createCrudValidation($orderList)
            ->validateThat('id', $orderList->id)
            ->isIdValue()

            ->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function createCrudValidation(OrderListEntity $orderList): ValidationBuilder
    {
        return $this->validationBuilder

            ->validateThat('name', $orderList->name)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_70)

            ->validateThat('listId', $orderList->listId)
                ->isIdValue()

            ->validateThat('budgetId', $orderList->budgetId)
                ->isIdValueOrNullIdValue()

            ->validateThat('contextOwnerId', $orderList->contextOwnerId)
                ->isIdValue();
    }
}
