<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Frontend;

use Shopware\B2B\Budget\Framework\BudgetService;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\EmptyForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\OrderList\Framework\OrderListCrudService;
use Shopware\B2B\OrderList\Framework\OrderListRepository;
use Shopware\B2B\OrderList\Framework\OrderListSearchStruct;
use Shopware\B2B\OrderList\Framework\OrderListService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use function array_merge;
use function count;

class OrderListController
{
    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderListCrudService
     */
    private $orderListCrudService;

    /**
     * @var BudgetService
     */
    private $budgetService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OrderListService
     */
    private $orderListService;

    public function __construct(
        AuthenticationService $authenticationService,
        OrderListRepository $orderListRepository,
        OrderListCrudService $orderListCrudService,
        GridHelper $orderListGridHelper,
        BudgetService $budgetService,
        CurrencyService $currencyService,
        OrderListService $orderListService
    ) {
        $this->authenticationService = $authenticationService;
        $this->orderListRepository = $orderListRepository;
        $this->orderListCrudService = $orderListCrudService;
        $this->gridHelper = $orderListGridHelper;
        $this->budgetService = $budgetService;
        $this->currencyService = $currencyService;
        $this->orderListService = $orderListService;
    }

    public function indexAction(): void
    {
        //nth
    }

    public function gridAction(Request $request): array
    {
        $ownershipContext = $this->getOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $searchStruct = new OrderListSearchStruct();

        $this->gridHelper->extractSearchDataInStoreFront($request, $searchStruct);

        $orderLists = $this->orderListRepository
            ->fetchList($searchStruct, $ownershipContext, $currencyContext);

        $totalCount = $this->orderListRepository
            ->fetchTotalCount($searchStruct, $ownershipContext);

        $maxPage = $this->gridHelper->getMaxPage($totalCount);

        $currentPage = $this->gridHelper->getCurrentPage($request);

        $gridState = $this->gridHelper->getGridState($request, $searchStruct, $orderLists, $maxPage, $currentPage);

        return ['gridState' => $gridState];
    }

    public function newAction(): array
    {
        $validationResponse = $this->gridHelper->getValidationResponse('orderList');

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $ownershipContext = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        return array_merge([
            'isNew' => true,
            'budgets' => $this->budgetService
                ->getUserSelectableBudgetsWithStatus($ownershipContext, PHP_INT_MAX, $currencyContext),
        ], $validationResponse);
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function createAction(Request $request): array
    {
        $request->checkPost();

        $post = $request->getPost();

        $serviceRequest = $this->orderListCrudService
            ->createNewRecordRequest($post);

        $identity = $this->authenticationService
            ->getIdentity();

        try {
            $orderList = $this->orderListCrudService
                ->create($serviceRequest, $identity->getOwnershipContext());
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
            throw new B2bControllerForwardException('new');
        }

        return [
            'id' => $orderList->id->getValue(),
            'name' => $orderList->name,
            'budgetId' => $orderList->budgetId->getValue(),
        ];
    }

    public function createAjaxAction(Request $request): array
    {
        $request->checkPost();

        $post = $request->getPost();

        $serviceRequest = $this->orderListCrudService
            ->createNewRecordRequest($post);

        $identity = $this->authenticationService
            ->getIdentity();

        try {
            $orderList = $this->orderListCrudService
                ->create($serviceRequest, $identity->getOwnershipContext());
        } catch (ValidationException $e) {
            return [];
        }

        return [
            'orderListId' => $orderList->id->getValue(),
            'name' => $orderList->name,
        ];
    }

    public function detailAction(Request $request): array
    {
        $orderListId = $request->requireIdValue('orderlist');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getOwnershipContext();

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        return ['orderList' => $orderList];
    }

    public function editAction(Request $request): array
    {
        $orderListId = $request->requireIdValue('orderlist');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getOwnershipContext();

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $validationResponse = $this->gridHelper->getValidationResponse('orderList');

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        return array_merge([
            'orderList' => $orderList,
            'budgets' => $this->budgetService
                ->getUserSelectableBudgetsWithStatus($ownershipContext, PHP_INT_MAX, $currencyContext),
        ], $validationResponse);
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function updateAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getOwnershipContext();

        $orderListId = $request->requireIdValue('orderlist');

        $post = $request->getPost();
        $post['id'] = $orderListId;

        $serviceRequest = $this->orderListCrudService
            ->createExistingRecordRequest($post);

        try {
            $orderList = $this->orderListCrudService->update($serviceRequest, $currencyContext, $ownershipContext);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
            throw new B2bControllerForwardException('edit', null, ['orderlist' => $orderListId]);
        }

        throw new B2bControllerForwardException('edit', null, ['orderlist' => $orderList->id]);
    }

    public function produceCartAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $orderListId = $request->requireIdValue('orderListId');

        $this->orderListCrudService
            ->produceCart($orderListId, $currencyContext, $this->getOwnershipContext());

        throw new EmptyForwardException();
    }

    public function exportCsvAction(Request $request): array
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $orderListId = $request->requireIdValue('id');

        $exportData = $this->orderListService->getCsvExportData($orderListId, $currencyContext, $this->getOwnershipContext());

        $response = new Response();

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $exportData['name'] . '.csv'
        );

        $response->headers->set('Content-Disposition', $disposition);

        $response->sendHeaders();

        return ['csvData' => $exportData['csv']];
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function duplicateAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $id = $request->requireIdValue('id');

        $identity = $this->authenticationService
            ->getIdentity();

        $this->orderListCrudService
            ->duplicate($id, $identity->getOwnershipContext(), $currencyContext);

        throw new B2bControllerForwardException('grid');
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function removeAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getOwnershipContext();

        $serviceRequest = $this->orderListCrudService
            ->createExistingRecordRequest($request->getPost());

        try {
            $this->orderListCrudService
                ->remove($serviceRequest, $currencyContext, $ownershipContext);
        } catch (NotFoundException $e) {
            // nth
        }

        throw new B2bControllerForwardException('grid');
    }

    public function validateBeforeCartAction(Request $request): array
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getOwnershipContext();

        $id = $request->requireIdValue('orderListId');
        $orderList = $this->orderListRepository->fetchOneById($id, $currencyContext, $ownershipContext);

        $unavailableReferences = $this->orderListService->getUnavailableReferences($orderList);

        if (count($unavailableReferences)) {
            return [
                'orderListId' => $id->getValue(),
                'unavailableReferences' => $unavailableReferences,
            ];
        }

        $this->orderListCrudService
            ->produceCart($id, $currencyContext, $ownershipContext);

        throw new EmptyForwardException();
    }

    /**
     * @internal
     */
    protected function getOwnershipContext(): OwnershipContext
    {
        return $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();
    }
}
