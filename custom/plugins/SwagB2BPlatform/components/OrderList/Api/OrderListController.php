<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Api;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\Order\Framework\OrderLineItemReferenceCrudService;
use Shopware\B2B\OrderList\Framework\OrderListCrudService;
use Shopware\B2B\OrderList\Framework\OrderListRepository;
use Shopware\B2B\OrderList\Framework\OrderListSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function in_array;

class OrderListController
{
    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    /**
     * @var OrderListCrudService
     */
    private $orderListCrudService;

    /**
     * @var OrderLineItemReferenceCrudService
     */
    private $lineItemReferenceCrudService;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(
        OrderListRepository $orderListRepository,
        GridHelper $requestHelper,
        OrderListCrudService $orderListCrudService,
        OrderLineItemReferenceCrudService $lineItemReferenceCrudService,
        LineItemListRepository $lineItemListRepository,
        AuthStorageAdapterInterface $authStorageAdapter,
        CurrencyService $currencyService
    ) {
        $this->requestHelper = $requestHelper;
        $this->orderListRepository = $orderListRepository;
        $this->orderListCrudService = $orderListCrudService;
        $this->lineItemReferenceCrudService = $lineItemReferenceCrudService;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->currencyService = $currencyService;
    }

    public function getListAction(Request $request): array
    {
        $search = new OrderListSearchStruct();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $ownerShipContext = $this->getDebtorOwnershipContext();

        $rules = $this->orderListRepository->fetchList($search, $ownerShipContext, $currencyContext);

        $totalCount = $this->orderListRepository
            ->fetchTotalCount($search, $ownerShipContext);

        return ['success' => true, 'orderLists' => $rules, 'totalCount' => $totalCount];
    }

    public function getAction(string $orderListId): array
    {
        $orderListId = IdValue::create($orderListId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        return ['success' => true, 'orderList' => $orderList];
    }

    public function createAction(Request $request): array
    {
        $ownerShipContext = $this->getDebtorOwnershipContext();

        $data = $request->getPost();

        $newRecord = $this->orderListCrudService
            ->createNewRecordRequest($data);

        $orderList = $this->orderListCrudService
            ->create($newRecord, $ownerShipContext);

        return ['success' => true, 'orderList' => $orderList];
    }

    public function updateAction(string $orderListId, Request $request): array
    {
        $orderListId = IdValue::create($orderListId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $data = $request->getPost();
        $data['id'] = $orderListId;

        $existingRecord = $this->orderListCrudService
            ->createExistingRecordRequest($data);

        $orderList = $this->orderListCrudService
            ->update($existingRecord, $currencyContext, $ownershipContext);

        return ['success' => true, 'orderList' => $orderList];
    }

    public function removeAction(string $orderListId): array
    {
        $orderListId = IdValue::create($orderListId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $existingRecord = $this->orderListCrudService
            ->createExistingRecordRequest([
                'id' => $orderListId,
            ]);

        $ownershipContext = $this->getDebtorOwnershipContext();

        $orderList = $this->orderListCrudService
            ->remove($existingRecord, $currencyContext, $ownershipContext);

        return ['success' => true, 'orderList' => $orderList];
    }

    public function duplicateAction(string $orderListId): array
    {
        $orderListId = IdValue::create($orderListId);

        $ownerShipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $orderList = $this->orderListCrudService
            ->duplicate($orderListId, $ownerShipContext, $currencyContext);

        return ['success' => true, 'orderList' => $orderList];
    }

    public function addItemsAction(string $orderListId, Request $request): array
    {
        $orderListId = IdValue::create($orderListId);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->getDebtorIdentity();

        $orderList = $this->orderListRepository->fetchOneById($orderListId, $currencyContext, $identity->getOwnershipContext());
        $items = $request->getPost();

        $references = [];
        foreach ($items as $item) {
            $crudService = $this->lineItemReferenceCrudService->createCreateCrudRequest($item);
            $references[] = $this->lineItemReferenceCrudService->addLineItem($orderList->listId, $crudService, $currencyContext, $identity->getOwnershipContext());
        }

        $lineItemList = $this->lineItemListRepository->fetchOneListById($orderList->listId, $currencyContext, $identity->getOwnershipContext());

        return ['success' => true, 'orderList' => $orderList, 'lineItemList' => $lineItemList, 'items' => $references];
    }

    public function removeItemsAction(string $orderListId, Request $request): array
    {
        $orderListId = IdValue::create($orderListId);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->getDebtorIdentity();

        $orderList = $this->orderListRepository->fetchOneById($orderListId, $currencyContext, $identity->getOwnershipContext());
        $itemIds = IdValue::createMultiple((array) $request->getPost());

        $items = $this->lineItemListRepository
            ->fetchOneListById($orderList->listId, $currencyContext, $identity->getOwnershipContext())
            ->references;

        $references = [];
        foreach ($items as $item) {
            if (in_array($item->id, $itemIds, true)) {
                $item->id = IdValue::null();
                $references[] = $item;
            }
        }

        foreach ($itemIds as $itemId) {
            $this->lineItemReferenceCrudService->deleteLineItem($orderList->listId, $itemId, $currencyContext, $identity->getOwnershipContext());
        }

        $lineItemList = $this->lineItemListRepository->fetchOneListById($orderList->listId, $currencyContext, $identity->getOwnershipContext());

        return ['success' => true, 'orderList' => $orderList, 'lineItemList' => $lineItemList, 'items' => $references];
    }

    public function updateItemsAction(string $orderListId, Request $request): array
    {
        $orderListId = IdValue::create($orderListId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->getDebtorIdentity();

        $orderList = $this->orderListRepository->fetchOneById($orderListId, $currencyContext, $identity->getOwnershipContext());
        $items = $request->getPost();

        $references = [];
        foreach ($items as $item) {
            $crudService = $this->lineItemReferenceCrudService->createUpdateCrudRequest($item);
            $references[] = $this->lineItemReferenceCrudService
                ->updateLineItem($orderList->listId, $crudService, $currencyContext, $identity->getOwnershipContext());
        }

        $lineItemList = $this->lineItemListRepository->fetchOneListById($orderList->listId, $currencyContext, $identity->getOwnershipContext());

        return ['success' => true, 'orderList' => $orderList, 'lineItemList' => $lineItemList, 'items' => $references];
    }

    public function getItemsAction(string $orderListId): array
    {
        $orderListId = IdValue::create($orderListId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $orderList = $this->orderListRepository->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $lineItemList = $this->lineItemListRepository->fetchOneListById($orderList->listId, $currencyContext, $ownershipContext);

        return ['success' => true, 'orderList' => $orderList, 'lineItemList' => $lineItemList, 'items' => $lineItemList->references];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->getDebtorIdentity()->getOwnershipContext();
    }

    /**
     * @internal
     */
    protected function getDebtorIdentity(): Identity
    {
        return $this->authStorageAdapter->getIdentity();
    }
}
