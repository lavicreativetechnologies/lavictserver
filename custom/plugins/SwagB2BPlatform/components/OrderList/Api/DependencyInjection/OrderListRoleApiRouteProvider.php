<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class OrderListRoleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}',
                'b2b_order_list.api_order_list_role_controller',
                'getAllAllowed',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/grant',
                'b2b_order_list.api_order_list_role_controller',
                'getAllGrant',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_role_controller',
                'getAllowed',
                ['roleId', 'orderListId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_role_controller',
                'allow',
                ['roleId', 'orderListId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/order_list/{orderListId}/grant',
                'b2b_order_list.api_order_list_role_controller',
                'allowGrant',
                ['roleId', 'orderListId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/allow',
                'b2b_order_list.api_order_list_role_controller',
                'multipleAllow',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/deny',
                'b2b_order_list.api_order_list_role_controller',
                'multipleDeny',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/order_list_role/{roleId}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_role_controller',
                'deny',
                ['roleId', 'orderListId'],
            ],
        ];
    }
}
