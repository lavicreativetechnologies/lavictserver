<?php declare(strict_types=1);

namespace Shopware\B2B\RoleBudget\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class BudgetRoleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}',
                'b2b_budget.api_budget_role_controller',
                'getAllAllowed',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/grant',
                'b2b_budget.api_budget_role_controller',
                'getAllGrant',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/budget/{budgetId}',
                'b2b_budget.api_budget_role_controller',
                'getAllowed',
                ['roleId', 'budgetId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/budget/{budgetId}',
                'b2b_budget.api_budget_role_controller',
                'allow',
                ['roleId', 'budgetId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/budget/{budgetId}/grant',
                'b2b_budget.api_budget_role_controller',
                'allowGrant',
                ['roleId', 'budgetId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/allow',
                'b2b_budget.api_budget_role_controller',
                'multipleAllow',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/deny',
                'b2b_budget.api_budget_role_controller',
                'multipleDeny',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/budget_role/{roleId}/budget/{budgetId}',
                'b2b_budget.api_budget_role_controller',
                'deny',
                ['roleId', 'budgetId'],
            ],
        ];
    }
}
