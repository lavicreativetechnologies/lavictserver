<?php declare(strict_types=1);

namespace Shopware\B2B\ProductName\Framework;

use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use function array_map;

class ProductNameService
{
    /**
     * @var ProductServiceInterface
     */
    private $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param ProductNameAware[] $entities
     */
    public function translateProductNames(array $entities): void
    {
        $productName = $this->productService->fetchProductNamesByOrderNumbers(array_map(function ($entity) {
            return $entity->getProductOrderNumber();
        }, $entities));

        foreach ($entities as $entity) {
            $entity->setProductName($productName[$entity->getProductOrderNumber()] ?? null);
        }
    }
}
