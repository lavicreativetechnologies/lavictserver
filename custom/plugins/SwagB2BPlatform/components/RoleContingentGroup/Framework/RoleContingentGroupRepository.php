<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContingentGroup\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Role\Framework\RoleAclGrantContext;
use Shopware\B2B\Role\Framework\RoleAssignmentEntity;
use Shopware\B2B\Role\Framework\RoleRemoveDependencyValidatorInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * DB-Representation of role:contact assignment
 */
class RoleContingentGroupRepository implements RoleRemoveDependencyValidatorInterface
{
    const TABLE_NAME = 'b2b_role_contingent_group';
    const TABLE_ALIAS = 'roles_contingent_groups';
    const TABLE_NAME_ROLE = 'b2b_role';
    const TABLE_ALIAS_ROLE = 'role';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    public function __construct(Connection $connection, AclReadHelper $aclReadHelper)
    {
        $this->connection = $connection;
        $this->aclReadHelper = $aclReadHelper;
    }

    public function removeRoleContingentGroupAssignment(IdValue $roleId, IdValue $contingentGroupId): void
    {
        $this->connection->delete(
            self::TABLE_NAME,
            [
                'role_id' => $roleId->getStorageValue(),
                'contingent_group_id' => $contingentGroupId->getStorageValue(),
            ]
        );
    }

    /**
     * @return RoleAssignmentEntity[]
     */
    public function fetchAllRolesAndCheckForContingentGroupAssignment(
        IdValue $roleId,
        OwnershipContext $ownershipContext
    ): array {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS_ROLE . '.*')
            ->addSelect(self::TABLE_ALIAS . '.id as assignmentId')
            ->addSelect('(' . self::TABLE_ALIAS_ROLE . '.left + 1 != ' . self::TABLE_ALIAS_ROLE . '.right) as hasChildren')
            ->from(self::TABLE_NAME_ROLE, self::TABLE_ALIAS_ROLE)
            ->leftJoin(self::TABLE_ALIAS_ROLE, self::TABLE_NAME, self::TABLE_ALIAS, self::TABLE_ALIAS_ROLE . '.id = ' . self::TABLE_ALIAS . '.role_id')
            ->where(self::TABLE_ALIAS_ROLE . '.id = :roleId')
            ->andWhere(self::TABLE_ALIAS_ROLE . '.context_owner_id = :contextOwnerId')
            ->setParameter('roleId', $roleId->getStorageValue())
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue())
            ->execute();

        $rolesData = $query->fetchAll(PDO::FETCH_ASSOC);

        $roles = [];
        foreach ($rolesData as $roleData) {
            $roles[] = (new RoleAssignmentEntity())->fromDatabaseArray($roleData);
        }

        return $roles;
    }

    public function assignRoleContingentGroup(IdValue $roleId, IdValue $contingentGroupId): void
    {
        $data = [
            'role_id' => $roleId->getStorageValue(),
            'contingent_group_id' => $contingentGroupId->getStorageValue(),
        ];

        $this->connection->insert(
            self::TABLE_NAME,
            $data
        );
    }

    public function extendRoleRemoveDependencyQuery(QueryBuilder $queryBuilder, RoleAclGrantContext $grantContext, string $mainTableAlias, string $alias): void
    {
        $queryBuilder->leftJoin($mainTableAlias, self::TABLE_NAME, $alias, "{$alias}.role_id = {$mainTableAlias}.id");
        $queryBuilder->orWhere("{$alias}.id is not null");
    }
}
