<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

use Exception;
use Shopware\B2B\Common\Validator\ValidationException;
use Symfony\Component\Validator\ConstraintViolationList;
use function count;

class OrderNumberFileValidationException extends ValidationException
{
    public function __construct(string $message = '', int $code = 0, Exception $previous = null)
    {
        $entity = new OrderNumberFileEntity();
        $violations = new ConstraintViolationList([]);
        parent::__construct($entity, $violations, $message, $code, $previous);
    }

    public function addViolations(ValidationException $exception): void
    {
        $this->getViolations()->addAll($exception->getViolations());
    }

    public function count(): int
    {
        return count($this->getViolations());
    }
}
