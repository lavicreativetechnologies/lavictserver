<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Version\ShopwareVersion;
use function array_key_exists;
use function get_object_vars;
use function property_exists;

class OrderNumberEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $customOrderNumber;

    /**
     * @var IdValue
     */
    public $productId;

    /**
     * Read only
     *
     * @var string
     */
    public $orderNumber;

    /**
     * Read only
     *
     * @var string
     */
    public $name;

    /**
     * Read only
     *
     * @var IdValue
     */
    public $contextOwnerId;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->contextOwnerId = IdValue::null();
        $this->productId = IdValue::null();
    }

    /**
     * @deprecated tag:v4.3.0 - Support for property "productDetailsId" will be removed
     */
    public function __get(string $property)
    {
        if ($property === 'productDetailsId') {
            return $this->productId;
        }

        return null;
    }

    /**
     * @deprecated tag:v4.3.0 - Support for property "productDetailsId" will be removed
     */
    public function __set(string $property, $value): void
    {
        if ($property === 'productDetailsId') {
            $this->productId = $value;
        }
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        $data = [
            'id' => $this->id->getStorageValue(),
            'custom_ordernumber' => $this->customOrderNumber,
            'product_id' => $this->productId->getStorageValue(),
            'context_owner_id' => $this->contextOwnerId->getStorageValue(),
        ];

        if (ShopwareVersion::isPlatform()) {
            $data['product_version_id'] = IdValue::create(\Shopware\Core\Defaults::LIVE_VERSION)->getStorageValue();
        }

        return $data;
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->customOrderNumber = $data['custom_ordernumber'];
        $this->orderNumber = $data['ordernumber'];
        $this->productId = IdValue::create($data['product_id']);
        $this->contextOwnerId = IdValue::create($data['context_owner_id']);

        return $this;
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        $vars['productDetailsId'] = $this->productId->getValue();

        return $vars;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @return OrderNumberEntity
     */
    public function setData(array $data)
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        if (array_key_exists('productDetailsId', $data)) {
            $this->productId = $data['productDetailsId'];
        }

        $this->id = IdValue::create($this->id);
        $this->contextOwnerId = IdValue::create($this->contextOwnerId);
        $this->productId = IdValue::create($this->productId);

        return $this;
    }
}
