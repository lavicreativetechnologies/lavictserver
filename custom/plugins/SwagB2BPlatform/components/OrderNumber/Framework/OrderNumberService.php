<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

use Shopware\B2B\Common\File\CsvReader;
use Shopware\B2B\Common\File\CsvWriter;
use Shopware\B2B\Common\File\XlsReader;
use Shopware\B2B\Common\File\XlsWriter;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Symfony\Component\HttpFoundation\File\File;
use function array_key_exists;
use function array_map;
use function file_get_contents;
use function in_array;
use function mb_strtolower;
use function sys_get_temp_dir;
use function tempnam;
use function trim;
use function unlink;

class OrderNumberService
{
    /**
     * @var CsvReader
     */
    private $csvReader;

    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    /**
     * @var XlsReader
     */
    private $xlsReader;

    /**
     * @var CsvWriter
     */
    private $csvWriter;

    /**
     * @var XlsWriter
     */
    private $xlsWriter;

    /**
     * @var OrderNumberTranslationServiceInterface
     */
    private $translationService;

    /**
     * @var OrderNumberCrudService
     */
    private $orderNumberCrudService;

    public function __construct(
        CsvReader $csvReader,
        XlsReader $xlsReader,
        OrderNumberRepositoryInterface $orderNumberRepository,
        ProductServiceInterface $productService,
        CsvWriter $csvWriter,
        XlsWriter $xlsWriter,
        OrderNumberTranslationServiceInterface $translationService,
        OrderNumberCrudService $orderNumberCrudService
    ) {
        $this->csvReader = $csvReader;
        $this->orderNumberRepository = $orderNumberRepository;
        $this->productService = $productService;
        $this->xlsReader = $xlsReader;
        $this->csvWriter = $csvWriter;
        $this->xlsWriter = $xlsWriter;
        $this->translationService = $translationService;
        $this->orderNumberCrudService = $orderNumberCrudService;
    }

    public function processOrderNumberFile(
        File $uploadedFile,
        OrderNumberContext $orderNumberContext,
        OwnershipContext $ownershipContext
    ): void {
        $orderNumberFileEntities = $this->extractOrderNumbersFromFile($uploadedFile, $orderNumberContext);

        $this->orderNumberCrudService->replace($orderNumberFileEntities, $ownershipContext);
    }

    /**
     * @return OrderNumberFileEntity[]
     * @internal
     */
    protected function extractOrderNumbersFromFile(File $fileObject, OrderNumberContext $orderNumberContext): array
    {
        $filePath = $fileObject->getPath() . '/' . $fileObject->getFilename();
        $fileExtension = mb_strtolower($fileObject->getExtension());

        if ($fileExtension === 'csv') {
            $orderNumberData = $this->csvReader
                ->read($filePath, $orderNumberContext);
        } elseif (in_array($fileExtension, ['xls', 'xlsx'], true)) {
            $orderNumberData = $this->xlsReader
                ->read($filePath, $orderNumberContext);
        } else {
            throw new UnsupportedFileException($fileExtension);
        }

        return $this->mapDataToOrderNumberEntity($orderNumberData, $orderNumberContext);
    }

    /**
     * @internal
     * @return OrderNumberFileEntity[]
     */
    protected function mapDataToOrderNumberEntity(array $data, OrderNumberContext $orderNumberContext): array
    {
        $orderNumberEntities = [];
        $rowOffset = 1 + (int) $orderNumberContext->headline;

        foreach ($data as $row => $value) {
            $orderNumberEntity = new OrderNumberFileEntity();

            $orderNumberEntity->orderNumber = '';
            if (array_key_exists($orderNumberContext->orderNumberColumn, $value)) {
                $orderNumberEntity->orderNumber = trim((string) $value[$orderNumberContext->orderNumberColumn]);
            }
            $orderNumberEntity->customOrderNumber = '';
            if (array_key_exists($orderNumberContext->customOrderNumberColumn, $value)) {
                $orderNumberEntity->customOrderNumber = trim((string) $value[$orderNumberContext->customOrderNumberColumn]);
            }

            $orderNumberEntity->row = $row + $rowOffset;
            $orderNumberEntities[] = $orderNumberEntity;
        }

        return $orderNumberEntities;
    }

    public function getCsvExportData(OwnershipContext $ownerShip): string
    {
        $exportData = $this->fetchExportData($ownerShip);

        $name = tempnam(sys_get_temp_dir(), 'csv');

        $this->csvWriter->write($exportData, $name);
        $csv = file_get_contents($name);

        unlink($name);

        return $csv;
    }

    public function getXlsExportData(OwnershipContext $ownerShip): string
    {
        $exportData = $this->fetchExportData($ownerShip);

        $name = tempnam(sys_get_temp_dir(), 'xls');

        $this->xlsWriter->write($exportData, $name);
        $xls = file_get_contents($name);

        unlink($name);

        return $xls;
    }

    /**
     * @internal
     */
    protected function fetchExportData(OwnershipContext $context): array
    {
        $products = $this->orderNumberRepository->fetchAllProductsForExport($context);
        $products = $this->fetchOrderNumberProductNames($products);

        return $this->parseExportData(...$products);
    }

    /**
     * @internal
     * @param OrderNumberEntity[] $orderNumberEntities
     */
    protected function parseExportData(OrderNumberEntity ...$orderNumberEntities): array
    {
        $product = $this->translationService->getProductTranslation();
        $productOrderNumber = $this->translationService->getProductOrderNumberTranslation();
        $customProductOrderNumber = $this->translationService->getCustomProductOrderNumberTranslation();

        $return[] = [$productOrderNumber, $customProductOrderNumber, $product];

        foreach ($orderNumberEntities as $orderNumberEntity) {
            $orderNumberArray = [];
            $orderNumberArray[$productOrderNumber] = $orderNumberEntity->orderNumber;
            $orderNumberArray[$customProductOrderNumber] = $orderNumberEntity->customOrderNumber;
            $orderNumberArray[$product] = $orderNumberEntity->name;

            $return[] = $orderNumberArray;
        }

        return $return;
    }

    /**
     * @param OrderNumberEntity[] $orderNumberEntities
     * @return OrderNumberEntity[]
     */
    public function fetchOrderNumberProductNames(array $orderNumberEntities): array
    {
        $orderNumbers = array_map(
            function (OrderNumberEntity $orderNumberEntity) {
                return $orderNumberEntity->orderNumber;
            },
            $orderNumberEntities
        );

        $productNames = $this->productService->fetchProductNamesByOrderNumbers($orderNumbers);

        foreach ($orderNumberEntities as $orderNumberEntity) {
            if (!isset($productNames[$orderNumberEntity->orderNumber])) {
                continue;
            }

            $productName = $productNames[$orderNumberEntity->orderNumber];
            $orderNumberEntity->name = $productName;
        }

        return $orderNumberEntities;
    }

    public function getProductNamesForApi(array $orderNumberEntities): array
    {
        $orderNumbers = array_map(
            function (OrderNumberEntity $orderNumberEntity) {
                return $orderNumberEntity->orderNumber;
            },
            $orderNumberEntities
        );
        $productNames = $this->productService->fetchProductNamesByOrderNumbers($orderNumbers);

        foreach ($orderNumberEntities as $orderNumberEntity) {
            if (isset($productNames[$orderNumberEntity->orderNumber])) {
                $orderNumberEntity->name = $productNames[$orderNumberEntity->orderNumber];
            }
        }

        return $orderNumberEntities;
    }

    public function getProductNameForApi(OrderNumberEntity $orderNumberEntity)
    {
        return $this->getProductNamesForApi([$orderNumberEntity])[0];
    }
}
