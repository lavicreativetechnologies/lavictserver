<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OrderNumberCrudService extends AbstractCrudService
{
    /**
     * @var OrderNumberValidationService
     */
    private $validationService;

    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    public function __construct(
        OrderNumberValidationService $validationService,
        OrderNumberRepositoryInterface $orderNumberRepository,
        DbalHelper $dbalHelper
    ) {
        $this->validationService = $validationService;
        $this->orderNumberRepository = $orderNumberRepository;
        $this->dbalHelper = $dbalHelper;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'customOrderNumber',
                'orderNumber',
                'productDetailsId',
                'name',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'customOrderNumber',
                'orderNumber',
                'productDetailsId',
                'name',
            ]
        );
    }

    public function create(CrudServiceRequest $request, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $data = $request->getFilteredData();
        $data['contextOwnerId'] = $ownershipContext->contextOwnerId;

        $orderNumber = new OrderNumberEntity();

        $orderNumber->setData($data);

        try {
            $orderNumber->productDetailsId = $this->orderNumberRepository->fetchDetailsId($orderNumber);
        } catch (NotFoundException $e) {
            // nth
        }

        $validation = $this->validationService
            ->createInsertValidation($orderNumber);

        $this->testValidation($orderNumber, $validation);

        $orderNumber = $this->orderNumberRepository->createOrderNumber($orderNumber);

        return $orderNumber;
    }

    public function update(CrudServiceRequest $request, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $data = $request->getFilteredData();
        $data['contextOwnerId'] = $ownershipContext->contextOwnerId;

        $orderNumber = new OrderNumberEntity();
        $orderNumber->setData($data);

        try {
            $orderNumber->productDetailsId = $this->orderNumberRepository->fetchDetailsId($orderNumber);
        } catch (NotFoundException $e) {
            // nth
        }

        if ($orderNumber->id instanceof NullIdValue) {
            $orderNumber->id = $this->orderNumberRepository->fetchIdByProductDetailsId($orderNumber->productDetailsId);
        }

        $validation = $this->validationService
            ->createUpdateValidation($orderNumber);

        $this->testValidation($orderNumber, $validation);

        return $this->orderNumberRepository
            ->updateOrderNumber($orderNumber);
    }

    public function remove(IdValue $id, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $orderNumber = $this->orderNumberRepository->fetchOneById($id, $ownershipContext);

        $this->orderNumberRepository
            ->removeOrderNumber($orderNumber);

        return $orderNumber;
    }

    /**
     * @param OrderNumberFileEntity[] $orderNumbers
     * @throws ValidationException
     */
    public function replace(array $orderNumbers, OwnershipContext $ownershipContext): void
    {
        $violations = new OrderNumberFileValidationException();
        $fileOrderNumberEntities = [];

        foreach ($orderNumbers as $orderNumberEntity) {
            try {
                $fileOrderNumberEntities[] = $this->createCsvImport($orderNumberEntity, $ownershipContext, $orderNumbers);
            } catch (ValidationException $exception) {
                $violations->addViolations($exception);
            }
        }

        if ($violations->count()) {
            throw $violations;
        }

        $this->orderNumberRepository->clearOrderNumbers($ownershipContext);
        $this->createMultipleOrderNumbers($fileOrderNumberEntities);
    }

    /**
     * @internal
     * @param OrderNumberFileEntity[] $orderNumberEntities
     */
    protected function createMultipleOrderNumbers(array $orderNumberEntities): void
    {
        $this->dbalHelper->transact(
            function () use ($orderNumberEntities): void {
                foreach ($orderNumberEntities as $orderNumberEntity) {
                    $this->orderNumberRepository->createOrderNumber($orderNumberEntity);
                }
            }
        );
    }

    /**
     * @internal
     * @param OrderNumberFileEntity[] $orderNumberEntities
     */
    protected function createCsvImport(
        OrderNumberFileEntity $orderNumberEntity,
        OwnershipContext $ownershipContext,
        array $orderNumberEntities
    ): OrderNumberFileEntity {
        $orderNumberEntity->contextOwnerId = $ownershipContext->contextOwnerId;

        try {
            $orderNumberEntity->productDetailsId = $this->orderNumberRepository->fetchDetailsId($orderNumberEntity);
        } catch (NotFoundException $e) {
            // nth
        }

        $validator = $this->validationService->createCsvImportValidation($orderNumberEntity, $orderNumberEntities);

        $this->testValidation($orderNumberEntity, $validator);

        return $orderNumberEntity;
    }
}
