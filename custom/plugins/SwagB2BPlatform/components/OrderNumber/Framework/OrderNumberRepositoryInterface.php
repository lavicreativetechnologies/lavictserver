<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\SearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface OrderNumberRepositoryInterface extends GridRepository
{
    public const TABLE_ALIAS = 'number';
    public const TABLE_NAME = 'b2b_order_number';
    public const ARTICLE_DETAIL_TABLE_NAME = 's_articles_details';
    public const ARTICLE_DETAIL_TABLE_ALIAS = 'details';
    public const PRODUCT_TABLE_NAME = 'product';
    public const PRODUCT_TABLE_ALIAS = 'product';

    public function fetchOneById(IdValue $id, OwnershipContext $ownershipContext): OrderNumberEntity;

    public function fetchList(SearchStruct $searchStruct, OwnershipContext $ownershipContext): array;

    public function fetchOrderNumberEntityByOrderNumber(string $orderNumber, SearchStruct $searchStruct, OwnershipContext $ownershipContext): OrderNumberEntity;

    public function fetchTotalCount(SearchStruct $searchStruct, OwnershipContext $ownershipContext): int;

    public function isCustomOrderNumberAvailable(OrderNumberEntity $orderNumberEntity): bool;

    public function updateOrderNumber(OrderNumberEntity $orderNumberEntity): OrderNumberEntity;

    public function createOrderNumber(OrderNumberEntity $orderNumberEntity): OrderNumberEntity;

    public function removeOrderNumber(OrderNumberEntity $orderNumberEntity): OrderNumberEntity;

    public function removeOrderNumberByCustomOrderNumber(string $orderNumber): void;

    public function removeMultipleOrderNumbersByCustomOrderNumber(array $customOrderNumbers): void;

    public function fetchAllProductsForExport(OwnershipContext $context): array;

    public function isOrderNumberUnique(OrderNumberEntity $orderNumberEntity): bool;

    public function clearOrderNumbers(OwnershipContext $ownershipContext);

    public function fetchDetailsId(OrderNumberEntity $orderNumberEntity): IdValue;

    public function fetchCustomOrderNumbers(array $orderNumbers, OwnershipContext $ownershipContext): array;

    public function fetchCustomOrderNumber(string $orderNumber, OwnershipContext $ownershipContext): string;

    public function fetchOriginalOrderNumbers(array $numbers, OwnershipContext $ownerShipContext): array;

    public function fetchOriginalOrderNumber(string $referenceNumber, OwnershipContext $ownershipContext): string;

    public function fetchNumbers(array $orderNumbers, OwnershipContext $ownershipContext): array;

    /**
     * @deprecated tag:v4.4.0 - $ownershipContext will be required
     */
    public function fetchIdByProductDetailsId(IdValue $id, OwnershipContext $ownershipContext = null): IdValue;

    /**
     * @deprecated tag:v4.4.0 - $ownershipContext will be required
     */
    public function fetchIdByCustomOrderNumber(string $customOrderNumber, OwnershipContext $ownershipContext = null): IdValue;
}
