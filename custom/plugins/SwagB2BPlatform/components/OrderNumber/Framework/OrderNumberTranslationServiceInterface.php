<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

interface OrderNumberTranslationServiceInterface
{
    public function getProductTranslation(): string;

    public function getProductOrderNumberTranslation(): string;

    public function getCustomProductOrderNumberTranslation(): string;
}
