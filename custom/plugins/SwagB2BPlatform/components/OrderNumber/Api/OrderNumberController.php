<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Api;

use Shopware\B2B\Common\Repository\SearchStruct;
use Shopware\B2B\OrderNumber\Framework\OrderNumberCrudService;
use Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface;
use Shopware\B2B\OrderNumber\Framework\OrderNumberService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;

class OrderNumberController
{
    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var AuthenticationIdentityLoaderInterface
     */
    private $contextIdentityLoader;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var OrderNumberCrudService
     */
    private $orderNumberCrudService;

    /**
     * @var OrderNumberService
     */
    private $orderNumberService;

    public function __construct(
        OrderNumberRepositoryInterface $orderNumberRepository,
        AuthenticationIdentityLoaderInterface $contextIdentityLoader,
        LoginContextService $loginContextService,
        OrderNumberCrudService $orderNumberCrudService,
        OrderNumberService $orderNumberService
    ) {
        $this->orderNumberRepository = $orderNumberRepository;
        $this->contextIdentityLoader = $contextIdentityLoader;
        $this->loginContextService = $loginContextService;
        $this->orderNumberCrudService = $orderNumberCrudService;
        $this->orderNumberService = $orderNumberService;
    }

    public function getListAction(string $debtorMail): array
    {
        $searchStruct = new SearchStruct();

        $ownershipContext = $this->contextIdentityLoader
            ->fetchIdentityByEmail($debtorMail, $this->loginContextService)
            ->getOwnershipContext();

        $list = $this->orderNumberRepository
            ->fetchList($searchStruct, $ownershipContext);

        $result = $this->orderNumberService->getProductNamesForApi($list);

        return ['success' => true, 'list' => $result];
    }

    public function createAction(string $debtorMail, string $orderNumber, string $customOrderNumber): array
    {
        $data = [
            'customOrderNumber' => $customOrderNumber,
            'orderNumber' => $orderNumber,
        ];

        $serviceRequest = $this->orderNumberCrudService
            ->createNewRecordRequest($data);

        $ownershipContext = $this->contextIdentityLoader
            ->fetchIdentityByEmail($debtorMail, $this->loginContextService)
            ->getOwnershipContext();

        $result = $this->orderNumberCrudService->create($serviceRequest, $ownershipContext);

        return ['success' => true, 'orderNumber' => $result];
    }

    public function updateAction(string $debtorMail, string $orderNumber, string $customOrderNumber): array
    {
        $data = [
            'customOrderNumber' => $customOrderNumber,
            'orderNumber' => $orderNumber,
        ];

        $serviceRequest = $this->orderNumberCrudService
            ->createExistingRecordRequest($data);

        $ownershipContext = $this->contextIdentityLoader
            ->fetchIdentityByEmail($debtorMail, $this->loginContextService)
            ->getOwnershipContext();

        $result = $this->orderNumberCrudService->update($serviceRequest, $ownershipContext);

        return ['success' => true, 'orderNumber' => $result];
    }

    public function removeAction(string $debtorMail, string $customOrderNumber): array
    {
        $ownershipContext = $this->contextIdentityLoader
            ->fetchIdentityByEmail($debtorMail, $this->loginContextService)
            ->getOwnershipContext();

        $id = $this->orderNumberRepository->fetchIdByCustomOrderNumber($customOrderNumber, $ownershipContext);

        $result = $this->orderNumberCrudService->remove($id, $ownershipContext);

        return ['success' => true, 'orderNumber' => $result];
    }

    public function getAction(string $debtorMail, string $orderNumber): array
    {
        $searchStruct = new SearchStruct();

        $ownershipContext = $this->contextIdentityLoader
            ->fetchIdentityByEmail($debtorMail, $this->loginContextService)
            ->getOwnershipContext();

        $orderNumberEntity = $this->orderNumberRepository
            ->fetchOrderNumberEntityByOrderNumber($orderNumber, $searchStruct, $ownershipContext);

        $result = $this->orderNumberService->getProductNameForApi($orderNumberEntity);

        return ['success' => true, 'orderNumber' => $result];
    }
}
