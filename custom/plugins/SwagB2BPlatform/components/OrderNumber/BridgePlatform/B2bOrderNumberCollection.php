<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void                      add(B2bOrderNumberEntity $entity)
 * @method void                      set(string $key, B2bOrderNumberEntity $entity)
 * @method B2bOrderNumberEntity[]    getIterator()
 * @method B2bOrderNumberEntity[]    getElements()
 * @method B2bOrderNumberEntity|null get(string $key)
 * @method B2bOrderNumberEntity|null first()
 * @method B2bOrderNumberEntity|null last()
 */
class B2bOrderNumberCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return B2bOrderNumberEntity::class;
    }
}
