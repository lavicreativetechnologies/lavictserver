<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;

class B2bOrderNumberEntity extends Entity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $customOrderNumber;

    /**
     * @var string
     */
    protected $productId;

    /**
     * @var int
     */
    protected $contextOwnerId;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCustomOrderNumber(): string
    {
        return $this->customOrderNumber;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getContextOwnerId(): int
    {
        return $this->contextOwnerId;
    }
}
