<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderNumberCheckoutSubscriber implements EventSubscriberInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CheckoutOrderPlacedEvent::class => 'updateProductNumbersWithCustomOrderNumbersForOrderConfirmMail',
        ];
    }

    public function updateProductNumbersWithCustomOrderNumbersForOrderConfirmMail(CheckoutOrderPlacedEvent $event): void
    {
        if (!$this->authenticationService->isB2b()) {
            return;
        }

        foreach ($event->getOrder()->getLineItems() as $lineItem) {
            $payload = $lineItem->getPayload();

            if (!isset($payload[OrderNumberCartProcessor::B2B_ORDER_NUMBER_PAYLOAD_KEY])) {
                continue;
            }

            $payload['productNumber'] = $payload[OrderNumberCartProcessor::B2B_ORDER_NUMBER_PAYLOAD_KEY];

            $lineItem->setPayload($payload);
        }
    }
}
