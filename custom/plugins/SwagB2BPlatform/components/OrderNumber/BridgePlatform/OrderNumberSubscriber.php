<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use function array_key_exists;

class OrderNumberSubscriber implements EventSubscriberInterface
{
    private const PRODUCT_NUMBER_KEY = 'productNumber';

    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    public function __construct(
        OrderNumberRepositoryInterface $orderNumberRepository
    ) {
        $this->orderNumberRepository = $orderNumberRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductEvents::PRODUCT_WRITTEN_EVENT => 'deleteMatchingCustomOrderNumbers',
        ];
    }

    public function deleteMatchingCustomOrderNumbers(EntityWrittenEvent $event): void
    {
        $customOrderNumbers = [];

        foreach ($event->getPayloads() as $payload) {
            if (array_key_exists(self::PRODUCT_NUMBER_KEY, $payload)) {
                $customOrderNumbers[] = $payload[self::PRODUCT_NUMBER_KEY];
            }
        }

        $this->orderNumberRepository
            ->removeMultipleOrderNumbersByCustomOrderNumber($customOrderNumbers);
    }
}
