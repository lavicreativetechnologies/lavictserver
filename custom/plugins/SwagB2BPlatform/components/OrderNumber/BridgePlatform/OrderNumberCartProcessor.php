<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class OrderNumberCartProcessor implements CartProcessorInterface
{
    private const PRODUCT_PREFIX = 'product-';
    public const B2B_ORDER_NUMBER_PAYLOAD_KEY = 'b2bOrderNumber';

    public function process(
        CartDataCollection $data,
        Cart $original,
        Cart $toCalculate,
        SalesChannelContext $context,
        CartBehavior $behavior
    ): void {
        foreach ($original->getLineItems() as $lineItem) {
            $product = $data->get(self::PRODUCT_PREFIX . $lineItem->getReferencedId());

            if (!$product) {
                continue;
            }

            $orderNumberCollection = $product->getExtension(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME);

            if (!$orderNumberCollection || !$orderNumberCollection->first()) {
                continue;
            }

            $lineItem->setPayloadValue(self::B2B_ORDER_NUMBER_PAYLOAD_KEY, $orderNumberCollection->first()->getCustomOrderNumber());
        }
    }
}
