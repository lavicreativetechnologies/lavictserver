<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Repository\SearchStruct;
use Shopware\B2B\OrderNumber\Framework\OrderNumberEntity;
use Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_shift;
use function sprintf;

class OrderNumberRepository implements OrderNumberRepositoryInterface
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
    }

    public function fetchNumbers(array $orderNumbers, OwnershipContext $ownershipContext): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(['product.product_number', 'coalesce(' . self::TABLE_ALIAS . '.custom_ordernumber, product.product_number) as custom_ordernumber'])
            ->from('product', 'product')
            ->leftJoin('product', self::TABLE_NAME, self::TABLE_ALIAS, self::TABLE_ALIAS . '.product_id = product.id and context_owner_id = :contextOwner')
            ->where('custom_ordernumber IN (:orderNumbers) or product.product_number in (:orderNumbers)')
            ->setParameter('orderNumbers', $orderNumbers, Connection::PARAM_STR_ARRAY)
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue());

        return $query->execute()->fetchAll(PDO::FETCH_KEY_PAIR);
    }

    /**
     * @return string query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'custom_ordernumber',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [
            '%2$s' => [
                'product' => 'product_number',
            ],
        ];
    }

    public function fetchOneById(IdValue $id, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $query = $this->connection->createQueryBuilder();
        $query->select([self::TABLE_ALIAS . '.id', 'custom_ordernumber', 'product.product_number as ordernumber', 'product_id', 'context_owner_id'])
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'product', 'product', 'product_id = product.id')
            ->where(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
            ->andWhere(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue())
            ->setParameter('id', $id->getStorageValue());

        $statement = $query->execute();
        $orderNumberData = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$orderNumberData) {
            throw new NotFoundException(sprintf('The order with the id %s does not exist', $id->getValue()));
        }

        return (new OrderNumberEntity())->fromDatabaseArray($orderNumberData);
    }

    /**
     * @return OrderNumberEntity[]
     */
    public function fetchList(SearchStruct $searchStruct, OwnershipContext $ownershipContext): array
    {
        $query = $this->connection->createQueryBuilder();
        $query->select([self::TABLE_ALIAS . '.id', 'custom_ordernumber', 'product.product_number as ordernumber', 'product_id', 'context_owner_id'])
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'product', 'product', 'product_id = product.id')
            ->where(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
            ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'ASC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $statement = $query->execute();
        $orderNumberData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $orderNumbers = [];
        foreach ($orderNumberData as $orderData) {
            $orderNumbers[] = (new OrderNumberEntity())->fromDatabaseArray($orderData);
        }

        return $orderNumbers;
    }

    public function fetchOrderNumberEntityByOrderNumber(string $orderNumber, SearchStruct $searchStruct, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $query = $this->connection->createQueryBuilder();
        $query->select([self::TABLE_ALIAS . '.id', 'custom_ordernumber', 'product.product_number as ordernumber', 'product_id', 'context_owner_id'])
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'product', 'product', 'product_id = product.id')
            ->where(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
            ->andWhere('product.product_number = :orderNumber')
            ->setParameters([
                'ownerId' => $ownershipContext->contextOwnerId->getStorageValue(),
                'orderNumber' => $orderNumber,
            ]);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'ASC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $statement = $query->execute();
        $orderNumberData = $statement->fetch(PDO::FETCH_ASSOC);

        $entity = new OrderNumberEntity();
        $entity->fromDatabaseArray($orderNumberData);

        return $entity;
    }

    public function fetchTotalCount(SearchStruct $searchStruct, OwnershipContext $ownershipContext): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'product', 'product', 'product_id = product.id')
            ->where(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
            ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());

        $this->dbalHelper->applyFilters($searchStruct, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    public function isCustomOrderNumberAvailable(OrderNumberEntity $orderNumberEntity): bool
    {
        $subSelectOrderNumber = $this->connection->createQueryBuilder()
            ->select('custom_ordernumber as customOrderNumber')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where('id != :orderNumberId')
            ->andWhere('context_owner_id = :ownerId');

        $subSelectProduct = $this->connection->createQueryBuilder()
            ->select('product_number as customOrderNumber')
            ->from(self::PRODUCT_TABLE_NAME, self::PRODUCT_TABLE_ALIAS);

        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from('(' . $subSelectOrderNumber->getSQL() . ' UNION ' . $subSelectProduct->getSQL() . ')', 'order_numbers')
            ->where('customOrderNumber = :customOrderNumber')
            ->setParameter('customOrderNumber', $orderNumberEntity->customOrderNumber)
            ->setParameter('orderNumberId', $orderNumberEntity->id->getStorageValue() ?: 0)
            ->setParameter('ownerId', $orderNumberEntity->contextOwnerId->getStorageValue());

        $isAvailable = (bool) $query->execute()->fetchColumn(0);

        return !$isAvailable;
    }

    public function updateOrderNumber(OrderNumberEntity $orderNumberEntity): OrderNumberEntity
    {
        if ($orderNumberEntity->isNew()) {
            throw new CanNotUpdateExistingRecordException('The order number provided does not exist');
        }

        if (!$orderNumberEntity->productId || $orderNumberEntity->productId instanceof NullIdValue) {
            $orderNumberEntity->productId = $this->fetchDetailsId($orderNumberEntity);
        }

        $this->connection->update(
            self::TABLE_NAME,
            $orderNumberEntity->toDatabaseArray(),
            [
                'id' => $orderNumberEntity->id->getStorageValue(),
                'context_owner_id' => $orderNumberEntity->contextOwnerId->getStorageValue(),
            ]
        );

        return $orderNumberEntity;
    }

    public function createOrderNumber(OrderNumberEntity $orderNumberEntity): OrderNumberEntity
    {
        if (!$orderNumberEntity->isNew()) {
            throw new CanNotInsertExistingRecordException('The custom order number provided already exist.');
        }

        if (!$orderNumberEntity->productId || $orderNumberEntity->productId instanceof NullIdValue) {
            $orderNumberEntity->productId = $this->fetchDetailsId($orderNumberEntity);
        }

        $this->connection->insert(
            self::TABLE_NAME,
            $orderNumberEntity->toDatabaseArray()
        );

        $orderNumberEntity->id = IdValue::create($this->connection->lastInsertId());

        return $orderNumberEntity;
    }

    /**
     * @throws NotFoundException
     */
    public function fetchDetailsId(OrderNumberEntity $orderNumberEntity): IdValue
    {
        $query = $this->connection->createQueryBuilder()
            ->select('id')
            ->from('product', 'product')
            ->where('product_number = :orderNumber')
            ->setParameter('orderNumber', $orderNumberEntity->orderNumber);

        $result = $query->execute()->fetchColumn(0);

        if (!$result) {
            throw new NotFoundException('The order number provided does not exist.');
        }

        return IdValue::create($result);
    }

    public function removeOrderNumber(OrderNumberEntity $orderNumberEntity): OrderNumberEntity
    {
        if ($orderNumberEntity->isNew()) {
            throw new NotFoundException('The order number provided does not exist');
        }

        $this->connection->delete(
            self::TABLE_NAME,
            [
                'id' => $orderNumberEntity->id->getStorageValue(),
                'context_owner_id' => $orderNumberEntity->contextOwnerId->getStorageValue(),
            ]
        );

        return $orderNumberEntity;
    }

    public function removeOrderNumberByCustomOrderNumber(string $ordernumber): void
    {
        $this->connection->delete(self::TABLE_NAME, ['custom_ordernumber' => $ordernumber]);
    }

    public function removeMultipleOrderNumbersByCustomOrderNumber(array $customOrderNumbers): void
    {
        $this->dbalHelper->transact(
            function () use ($customOrderNumbers): void {
                foreach ($customOrderNumbers as $customOrderNumber) {
                    $this->removeOrderNumberByCustomOrderNumber($customOrderNumber);
                }
            }
        );
    }

    /**
     * @return OrderNumberEntity[]
     */
    public function fetchAllProductsForExport(OwnershipContext $context): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select([
                'product.product_number as ordernumber',
                self::TABLE_ALIAS . '.custom_ordernumber',
                'product_id',
                self::TABLE_ALIAS . '.id',
                self::TABLE_ALIAS . '.context_owner_id',
            ])
            ->from('product', 'product')
            ->innerJoin('product', self::TABLE_NAME, self::TABLE_ALIAS, self::TABLE_ALIAS . '.product_id = product.id and context_owner_id = :ownerId')
            ->orderBy('product.product_number', 'ASC')
            ->setParameter('ownerId', $context->contextOwnerId->getStorageValue());

        $statement = $query->execute();

        $orderNumberData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $orderNumbers = [];
        foreach ($orderNumberData as $orderData) {
            $orderNumbers[] = (new OrderNumberEntity())->fromDatabaseArray($orderData);
        }

        return $orderNumbers;
    }

    public function clearOrderNumbers(OwnershipContext $ownershipContext): void
    {
        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue())
            ->execute();
    }

    public function isOrderNumberUnique(OrderNumberEntity $orderNumberEntity): bool
    {
        $query = $this->connection->createQueryBuilder()
            ->select('count(*)')
            ->from('product', 'product')
            ->innerJoin('product', self::TABLE_NAME, self::TABLE_ALIAS, self::TABLE_ALIAS . '.product_id = product.id')
            ->where(self::TABLE_ALIAS . '.id != :orderNumberId')
            ->andWhere('context_owner_id = :ownerId')
            ->andWhere('product.product_number = :orderNumber')
            ->setParameter('orderNumber', $orderNumberEntity->orderNumber)
            ->setParameter('orderNumberId', $orderNumberEntity->id instanceof NullIdValue ? 0 : $orderNumberEntity->id->getStorageValue())
            ->setParameter('ownerId', $orderNumberEntity->contextOwnerId->getStorageValue());

        $exists = (bool) $query->execute()->fetchColumn(0);

        return !$exists;
    }

    public function fetchCustomOrderNumbers(array $orderNumbers, OwnershipContext $context): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(['product_number', 'coalesce(custom_ordernumber, product_number) as custom_ordernumber'])
            ->from('product', 'product')
            ->leftJoin('product', self::TABLE_NAME, self::TABLE_ALIAS, self::TABLE_ALIAS . '.product_id = product.id and context_owner_id = :contextOwner')
            ->where('custom_ordernumber IN (:orderNumbers) OR product_number IN (:orderNumbers)')
            ->setParameter('orderNumbers', $orderNumbers, Connection::PARAM_STR_ARRAY)
            ->setParameter('contextOwner', $context->contextOwnerId->getStorageValue());

        $customOrderNumbers = $query->execute()->fetchAll(FetchMode::NUMERIC);
        $customOrderNumbers = $this->buildKeyValue($customOrderNumbers);

        return $customOrderNumbers;
    }

    /**
     * @internal
     */
    protected function buildKeyValue(array $array): array
    {
        $data = [];

        foreach ($array as $item) {
            $data[$item[0]] = $item[1];
        }

        return $data;
    }

    public function fetchCustomOrderNumber(string $orderNumber, OwnershipContext $ownershipContext): string
    {
        $orderNumbers = $this->fetchCustomOrderNumbers([$orderNumber], $ownershipContext);
        $customOrderNumber = array_shift($orderNumbers);

        return $customOrderNumber ?: $orderNumber;
    }

    public function fetchOriginalOrderNumbers(array $numbers, OwnershipContext $context): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(['coalesce(custom_ordernumber, product_number) as custom_ordernumber', 'product_number'])
            ->from('product', 'product')
            ->leftJoin('product', self::TABLE_NAME, self::TABLE_ALIAS, self::TABLE_ALIAS . '.product_id = product.id and context_owner_id = :contextOwner')
            ->where('product_number IN (:orderNumbers) OR custom_ordernumber IN (:orderNumbers)')
            ->setParameter('orderNumbers', $numbers, Connection::PARAM_STR_ARRAY)
            ->setParameter('contextOwner', $context->contextOwnerId->getStorageValue());

        $originalOrderNumbers = $query->execute()->fetchAll(FetchMode::NUMERIC);
        $originalOrderNumbers = $this->buildKeyValue($originalOrderNumbers);

        foreach ($numbers as &$number) {
            if (isset($originalOrderNumbers[$number])) {
                $number = $originalOrderNumbers[$number];
            }
        }

        return $numbers;
    }

    public function fetchOriginalOrderNumber(string $orderNumber, OwnershipContext $ownershipContext): string
    {
        $orderNumbers = $this->fetchOriginalOrderNumbers([$orderNumber], $ownershipContext);
        $originalOrderNumber = array_shift($orderNumbers);

        return $originalOrderNumber ?: $orderNumber;
    }

    /**
     * @deprecated tag:v4.4.0 - $ownershipContext will be required
     */
    public function fetchIdByProductDetailsId(IdValue $id, OwnershipContext $ownershipContext = null): IdValue
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where('product_id = :id')
            ->setParameter('id', $id->getStorageValue());

        if ($ownershipContext !== null) {
            $query->andWhere(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
                ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());
        }

        return IdValue::create(
                $query->execute()->fetchColumn()
        );
    }

    /**
     * @deprecated tag:v4.4.0 - $ownershipContext will be required
     */
    public function fetchIdByCustomOrderNumber(string $customOrderNumber, OwnershipContext $ownershipContext = null): IdValue
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where('custom_ordernumber = :customOrderNumber')
            ->setParameter('customOrderNumber', $customOrderNumber);

        if ($ownershipContext !== null) {
            $query->andWhere(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
                ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());
        }

        return IdValue::create(
           $query->execute()->fetchColumn()
        );
    }
}
