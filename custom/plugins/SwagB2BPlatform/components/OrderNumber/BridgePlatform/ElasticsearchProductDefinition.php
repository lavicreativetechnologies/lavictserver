<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use ONGR\ElasticsearchDSL\Query\Compound\BoolQuery;
use ONGR\ElasticsearchDSL\Query\FullText\MatchQuery;
use ONGR\ElasticsearchDSL\Query\Joining\NestedQuery;
use ONGR\ElasticsearchDSL\Query\TermLevel\TermQuery;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Elasticsearch\Framework\AbstractElasticsearchDefinition;
use Shopware\Elasticsearch\Framework\FullText;
use Shopware\Elasticsearch\Framework\Indexing\EntityMapper;

class ElasticsearchProductDefinition extends AbstractElasticsearchDefinition
{
    private const EXTENSION_NAME = SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME;
    public const CUSTOM_ORDER_NUMBER_FIELD = 'customOrderNumber';
    public const CONTEXT_OWNER_ID_FIELD = 'contextOwnerId';
    public const CUSTOM_ORDER_NUMBER_SEARCH_FIELD = 'search';
    public const CUSTOM_ORDER_NUMBER_FIELD_PATH = self::EXTENSION_NAME . '.' . self::CUSTOM_ORDER_NUMBER_FIELD;
    public const CUSTOM_ORDER_NUMBER_SEARCH_FIELD_PATH = self::EXTENSION_NAME . '.' . self::CUSTOM_ORDER_NUMBER_FIELD . '.' . self::CUSTOM_ORDER_NUMBER_SEARCH_FIELD;
    public const CONTEXT_OWNER_ID_FIELD_PATH = self::EXTENSION_NAME . '.' . self::CONTEXT_OWNER_ID_FIELD;

    /**
     * @var AbstractElasticsearchDefinition
     */
    private $decorated;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        AbstractElasticsearchDefinition $decorated,
        EntityMapper $mapper,
        AuthenticationService $authenticationService
    ) {
        parent::__construct($mapper);
        $this->decorated = $decorated;
        $this->authenticationService = $authenticationService;
    }

    public function getEntityDefinition(): EntityDefinition
    {
        return $this->decorated->getEntityDefinition();
    }

    public function extendCriteria(Criteria $criteria): void
    {
        $this->decorated->extendCriteria($criteria);
        $criteria->addAssociation(self::EXTENSION_NAME);
    }

    public function getMapping(Context $context): array
    {
        $mapping = $this->decorated->getMapping($context);
        $definition = $this->decorated->getEntityDefinition();

        $mapping['properties'][self::EXTENSION_NAME] = $this->mapper->mapField(
            $definition,
            $definition->getField(self::EXTENSION_NAME),
            $context
        );

        $mapping['properties'][self::EXTENSION_NAME]['properties'][self::CUSTOM_ORDER_NUMBER_FIELD]['fields'] = [
            self::CUSTOM_ORDER_NUMBER_SEARCH_FIELD => [
                'type' => 'text',
                'analyzer' => 'sw_ngram_analyzer',
                'search_analyzer' => 'sw_ngram_analyzer',
            ],
        ];

        return $mapping;
    }

    public function buildTermQuery(Context $context, Criteria $criteria): BoolQuery
    {
        $termQuery = $this->decorated->buildTermQuery($context, $criteria);

        if (!$this->authenticationService->isB2b()) {
            return $termQuery;
        }

        $bool = new BoolQuery();

        $filter = new NestedQuery(
            self::EXTENSION_NAME,
            $bool
        );
        $bool->add(
            new MatchQuery(
                self::CUSTOM_ORDER_NUMBER_SEARCH_FIELD_PATH,
                $criteria->getTerm(),
                ['boost' => 10]
            ),
            BoolQuery::MUST
        );
        $bool->add(
            new TermQuery(
                self::CONTEXT_OWNER_ID_FIELD_PATH,
                $this->authenticationService->getIdentity()->getContextAuthId()->getValue()
            ),
            BoolQuery::MUST
        );

        $termQuery->add($filter, BoolQuery::SHOULD);

        return $termQuery;
    }

    public function extendEntities(EntityCollection $collection): EntityCollection
    {
        return $this->decorated->extendEntities($collection);
    }

    public function hasNewIndexerPattern(): bool
    {
        return $this->decorated->hasNewIndexerPattern();
    }

    public function buildFullText(Entity $entity): FullText
    {
        return $this->decorated->buildFullText($entity);
    }
}
