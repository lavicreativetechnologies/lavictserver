<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Frontend\DependencyInjection;

use Shopware\B2B\Common\Controller\DependencyInjection\ControllerConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\OrderNumber\Framework\DependencyInjection\OrderNumberFrameworkConfiguration;
use Shopware\B2B\Shop\Framework\DependencyInjection\ShopFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderNumberFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new ControllerConfiguration(),
            new OrderNumberFrameworkConfiguration(),
            new ShopFrameworkConfiguration(),
        ];
    }
}
