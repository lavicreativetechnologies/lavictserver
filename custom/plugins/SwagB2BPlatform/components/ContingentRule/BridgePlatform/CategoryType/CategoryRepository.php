<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\BridgePlatform\CategoryType;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\TranslatedFieldQueryExtenderTrait;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use function str_replace;

class CategoryRepository implements ContingentRuleTypeRepositoryInterface
{
    use TranslatedFieldQueryExtenderTrait;

    const TABLE_NAME = 'b2b_contingent_group_rule_category';

    const TABLE_ALIAS = 'categoryType';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var SalesChannelContext
     */
    private $salesChannelContext;

    public function __construct(Connection $connection, SalesChannelContext $salesChannelContext)
    {
        $this->connection = $connection;
        $this->salesChannelContext = $salesChannelContext;
    }

    public function createSubQuery(): string
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'category', 'category', self::TABLE_ALIAS . '.category_id = category.id');

        $this->addTranslatedFieldSelect(
            $query,
            'name',
            'category_name',
            'category_translation',
            'category',
            'category',
            'id',
            $this->salesChannelContext->getContext(),
            true
        );

        return $this->buildSql($query);
    }

    /**
     * @internal
     */
    protected function buildSql(QueryBuilder $query): string
    {
        $sql = $query->getSQL();

        foreach ($query->getParameters() as $parameter => $parameterValue) {
            $sql = str_replace(':' . $parameter, $this->connection->quote($parameterValue), $sql);
        }

        return $sql;
    }

    public function addSelect(QueryBuilder $query, string $prefix): void
    {
        $query->addSelect($prefix . '.contingent_rule_id as ' . $prefix . '_contingent_rule_id')
            ->addSelect($prefix . '.category_id as ' . $prefix . '_category_id')
            ->addSelect($prefix . '.category_name as ' . $prefix . '_category_name');
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
