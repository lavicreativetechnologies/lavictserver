<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\BridgePlatform\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ContingentRuleBridgeConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
            new ContingentRuleRestrictionTypesExtender(),
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [];
    }
}
