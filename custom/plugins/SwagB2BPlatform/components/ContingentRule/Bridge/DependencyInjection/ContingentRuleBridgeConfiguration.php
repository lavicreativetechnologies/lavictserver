<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\ContingentRule\BridgePlatform\DependencyInjection\ContingentRuleBridgeConfiguration as PlatformContingentRuleBridgeConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ContingentRuleBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformContingentRuleBridgeConfiguration();
        }

        return new self();
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
            new ContingentRuleRestrictionTypesExtender(),
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [];
    }
}
