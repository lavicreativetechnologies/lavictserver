<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Bridge\DependencyInjection;

use Shopware\B2B\ContingentRule\Bridge\CategoryType\CategoryType;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ContingentRuleRestrictionTypesExtender implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $restrictTypes = $container->getParameter('b2b_contingent_rule.restrict_types');
        $restrictTypes[] = CategoryType::NAME;
        $container->setParameter('b2b_contingent_rule.restrict_types', $restrictTypes);
    }
}
