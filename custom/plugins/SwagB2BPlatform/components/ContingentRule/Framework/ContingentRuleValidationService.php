<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

use InvalidArgumentException;
use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContingentRuleValidationService
{
    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ContingentRuleTypeFactory
     */
    private $typeFactory;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator,
        ContingentRuleTypeFactory $typeFactory
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
        $this->typeFactory = $typeFactory;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function createInsertValidation(ContingentRuleEntity $contingentRule): Validator
    {
        return $this->createCrudValidation($contingentRule)
            ->validateThat('id', $contingentRule->id)
            ->isNullIdValue()
            ->validateThat('contingentGroupId', $contingentRule->contingentGroupId)
            ->isIdValue()

            ->getValidator($this->validator);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function createUpdateValidation(ContingentRuleEntity $contingentRule): Validator
    {
        return $this->createCrudValidation($contingentRule)
            ->validateThat('id', $contingentRule->id)
            ->isIdValue()

            ->getValidator($this->validator);
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function createCrudValidation(ContingentRuleEntity $contingentRule): ValidationBuilder
    {
        $baseValidations = $this->validationBuilder

            ->validateThat('type', $contingentRule->type)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255);

        return $this->typeFactory
            ->findTypeByName($contingentRule->type)
            ->createValidationExtender($contingentRule)
            ->extendValidator($baseValidations);
    }
}
