<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductNumberAllowanceType;

use Shopware\B2B\Cart\Framework\CartAccessContext;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use function array_filter;
use function count;
use function implode;
use function in_array;
use function spl_object_hash;

class ProductNumberAllowanceAccessStrategy implements CartAccessStrategyInterface
{
    /**
     * @var string[]
     */
    private $productNumbers;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    /**
     * @param string[] $productNumbers
     */
    public function __construct(array $productNumbers, ProductServiceInterface $productService)
    {
        $this->productNumbers = $productNumbers;
        $this->productService = $productService;
    }

    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult): void
    {
        $references = $context->orderClearanceEntity
            ->list
            ->references;

        $errors = array_filter($references, function (LineItemReference $lineItem) {
            if (!$this->productService->isNormalProduct($lineItem)) {
                return false;
            }

            return !in_array($lineItem->referenceNumber, $this->productNumbers, true);
        });

        if (!count($errors)) {
            return;
        }

        $cartAccessResult->addError(
            self::class,
            'ProductNumberAllowanceError',
            [
                'allowedValue' => implode(', ', $this->productNumbers),
                'identifier' => spl_object_hash($this),
            ]
        );
    }

    public function addInformation(CartAccessResult $cartAccessResult): void
    {
        $cartAccessResult->addInformation(
            self::class,
            'ProductNumberAllowanceError',
            [
                'allowedValue' => implode(', ', $this->productNumbers),
                'identifier' => spl_object_hash($this),
            ]
        );
    }
}
