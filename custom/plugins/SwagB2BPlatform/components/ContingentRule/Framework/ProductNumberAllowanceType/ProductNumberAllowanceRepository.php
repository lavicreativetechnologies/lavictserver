<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductNumberAllowanceType;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeRepositoryInterface;

class ProductNumberAllowanceRepository implements ContingentRuleTypeRepositoryInterface
{
    const TABLE_NAME = 'b2b_contingent_group_rule_product_number_allowance';

    const TABLE_ALIAS = 'ProductNumberAllowanceType';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function createSubQuery(): string
    {
        return $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->getSQL();
    }

    public function addSelect(QueryBuilder $query, string $prefix): void
    {
        $query->addSelect($prefix . '.contingent_rule_id as ' . $prefix . '_contingent_rule_id')
            ->addSelect($prefix . '.product_numbers as ' . $prefix . '_product_numbers');
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
