<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductNumberAllowanceType;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeRepositoryInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeValidationExtender;
use Shopware\B2B\ContingentRule\Framework\UnsupportedContingentRuleEntityTypeException;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class ProductNumberAllowanceType implements ContingentRuleTypeInterface
{
    const NAME = 'ProductNumberAllowance';

    /**
     * @var ProductNumberAllowanceRepository
     */
    private $repository;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    public function __construct(ProductNumberAllowanceRepository $repository, ProductServiceInterface $productService)
    {
        $this->repository = $repository;
        $this->productService = $productService;
    }

    public function getTypeName(): string
    {
        return self::NAME;
    }

    public function createEntity(): ContingentRuleEntity
    {
        return new ProductNumberAllowanceRuleEntity($this->getTypeName());
    }

    public function createValidationExtender(ContingentRuleEntity $entity): ContingentRuleTypeValidationExtender
    {
        return new ProductNumberAllowanceRuleValidationExtender($entity);
    }

    public function createCartAccessStrategy(
        OwnershipContext $ownershipContext,
        ContingentRuleEntity $entity
    ): CartAccessStrategyInterface {
        if (!$entity instanceof ProductNumberAllowanceRuleEntity) {
            throw new UnsupportedContingentRuleEntityTypeException($entity);
        }

        return new ProductNumberAllowanceAccessStrategy($entity->productNumbers, $this->productService);
    }

    public function getRepository(Connection $connection): ContingentRuleTypeRepositoryInterface
    {
        return new ProductNumberAllowanceRepository($connection);
    }

    public function getRequestKeys(): array
    {
        return [
            'productNumbers',
        ];
    }
}
