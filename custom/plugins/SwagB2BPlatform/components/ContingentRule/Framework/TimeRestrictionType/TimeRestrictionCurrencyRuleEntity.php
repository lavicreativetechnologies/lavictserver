<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\TimeRestrictionType;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Currency\Framework\CurrencyAware;
use function array_merge;

class TimeRestrictionCurrencyRuleEntity extends TimeRestrictionRuleEntity implements CurrencyAware
{
    /**
     * @var float
     */
    public $currencyFactor = self::DEFAULT_FACTOR;

    public function toDatabaseArray(): array
    {
        return array_merge(
            parent::toDatabaseArray(),
            [
                'currency_factor' => $this->currencyFactor,
        ]
        );
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->currencyFactor = (float) $data['currency_factor'];

        return parent::fromDatabaseArray($data);
    }

    public function fromDatabaseArrayPrefixed(array $data): CrudEntity
    {
        $this->currencyFactor = (float) $data[$data['type'] . '_currency_factor'];

        return parent::fromDatabaseArrayPrefixed($data);
    }

    public function getCurrencyFactor(): float
    {
        return $this->currencyFactor;
    }

    public function setCurrencyFactor(float $factor): void
    {
        $this->currencyFactor = $factor;
    }

    public function getAmountPropertyNames(): array
    {
        return [
            'value',
        ];
    }
}
