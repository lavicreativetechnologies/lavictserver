<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\TimeRestrictionType;

use Shopware\B2B\Cart\Framework\CartAccessContext;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\Cart\Framework\CartHistory;
use function count;
use function spl_object_hash;

class OrderItemQuantityAccessStrategy implements CartAccessStrategyInterface
{
    /**
     * @var CartHistory
     */
    private $cartHistory;

    /**
     * @var int
     */
    private $value;

    public function __construct(CartHistory $cartHistory, int $value)
    {
        $this->cartHistory = $cartHistory;
        $this->value = $value;
    }

    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult): void
    {
        $history = $this->cartHistory->orderItemQuantity + count($context->orderClearanceEntity->list->references);

        if ($history <= $this->value) {
            return;
        }

        $cartAccessResult->addError(
            __CLASS__,
            'OrderItemQuantityError',
            [
                'allowedValue' => $this->value,
                'appliedValue' => $history,
                'timeRestriction' => $this->cartHistory->timeRestriction,
                'cartHistory' => $this->cartHistory,
                'identifier' => spl_object_hash($this),
            ]
        );
    }

    public function addInformation(CartAccessResult $cartAccessResult): void
    {
        $cartAccessResult->addInformation(
            __CLASS__,
            'OrderItemQuantityError',
            [
                'allowedValue' => $this->value,
                'timeRestriction' => $this->cartHistory->timeRestriction,
                'cartHistory' => $this->cartHistory,
                'identifier' => spl_object_hash($this),
            ]
        );
    }
}
