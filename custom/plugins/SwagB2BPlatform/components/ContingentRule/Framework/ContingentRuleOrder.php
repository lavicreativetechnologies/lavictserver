<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

use function property_exists;

class ContingentRuleOrder
{
    /**
     * @var int
     */
    public $orderAmount;

    /**
     * @var int
     */
    public $orderQuantity;

    /**
     * @var int
     */
    public $orderItemQuantity;

    public function fromDatabaseArray(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }
    }
}
