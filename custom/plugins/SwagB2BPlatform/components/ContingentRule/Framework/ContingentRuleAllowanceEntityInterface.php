<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

interface ContingentRuleAllowanceEntityInterface
{
    public function mergeRule(self $rule): self;
}
