<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

use Shopware\B2B\Common\DummyEntity;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\ContingentRule\Framework\ProductOrderNumberType\ProductOrderNumberRuleEntity;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use function array_filter;
use function array_map;
use function array_merge;
use function in_array;

class ContingentValidationService
{
    const PRODUCT_NUMBER_ERROR = [
        'type' => 'ProductNumber',
        'message' => 'TheProductNumberIsInvalid',
    ];

    const TYPE_ERROR = [
        'type' => 'Type',
        'message' => 'NoValidTypeSelected',
    ];

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    public function __construct(
        ProductServiceInterface $productService
    ) {
        $this->productService = $productService;
    }

    public function createTypeValidationException(): ValidationException
    {
        $violation = $this->createViolation(self::TYPE_ERROR);

        return $this->createValidationException([$violation]);
    }

    public function createProductNumberValidationException(CrudServiceRequest $request): ValidationException
    {
        $productNumbers = $request->getFilteredData()['productNumbers'] ?? [];
        $productNumber = $request->getFilteredData()['productOrderNumber'] ?? null;

        $productNumbers = array_filter(array_merge([$productNumber], $productNumbers));

        $validProductNumbers = $this->productService
            ->fetchProductOrderNumbersByReferenceNumbers($productNumbers);

        $violations = [];
        foreach ($productNumbers as $productNumber) {
            if (!in_array($productNumber, $validProductNumbers, true)) {
                $violations[] = $this->createViolation(
                    self::PRODUCT_NUMBER_ERROR,
                    [
                        'productNumber' => $productNumber,
                    ]
                );
            }
        }

        return $this->createValidationException($violations);
    }

    /**
     * @internal
     */
    protected function createViolation(array $errorMessage, array $parameters = []): ConstraintViolation
    {
        return new ConstraintViolation(
            $errorMessage['message'],
            $errorMessage['message'],
            $parameters,
            '',
            $errorMessage['type'],
            ''
        );
    }

    /**
     * @internal
     */
    protected function createValidationException(array $violations): ValidationException
    {
        $violationList = new ConstraintViolationList($violations);

        return new ValidationException(
            new DummyEntity(),
            $violationList,
            'Validation violations detected, can not proceed:',
            400
        );
    }

    public function validateContingentRules(array $contingentRules): array
    {
        foreach ($contingentRules as $key => $rule) {
            if ($rule instanceof ProductOrderNumberRuleEntity) {
                try {
                    $this->productService->fetchOrderNumberByReferenceNumber($rule->productOrderNumber);

                    $contingentRules[$key]->valid = true;
                } catch (NotFoundException $e) {
                    $contingentRules[$key]->valid = false;
                }
            }
        }

        return $contingentRules;
    }

    public function getInvalidContingentRuleOrderNumberIds(array $contingentRules): array
    {
        $invalidRules = array_filter($contingentRules, function ($rule) {
            return $rule instanceof ProductOrderNumberRuleEntity && !$rule->valid;
        });

        return array_map(
            function (ProductOrderNumberRuleEntity $rule) {
                return $rule->id;
            },
            $invalidRules
        );
    }
}
