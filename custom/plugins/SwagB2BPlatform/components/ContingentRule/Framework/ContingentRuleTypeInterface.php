<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface ContingentRuleTypeInterface
{
    public function getTypeName(): string;

    public function createEntity(): ContingentRuleEntity;

    public function createValidationExtender(ContingentRuleEntity $entity): ContingentRuleTypeValidationExtender;

    public function createCartAccessStrategy(OwnershipContext $ownershipContext, ContingentRuleEntity $entity): CartAccessStrategyInterface;

    public function getRepository(Connection $connection): ContingentRuleTypeRepositoryInterface;

    /**
     * @return string[]
     */
    public function getRequestKeys(): array;
}
