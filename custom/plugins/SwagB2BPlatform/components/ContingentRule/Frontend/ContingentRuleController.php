<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Frontend;

use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleCrudService;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleRepository;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleSearchStruct;
use Shopware\B2B\ContingentRule\Framework\ContingentValidationService;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function array_merge;
use function in_array;

class ContingentRuleController
{
    /**
     * @var ContingentRuleRepository
     */
    private $contingentRuleRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var ContingentRuleCrudService
     */
    private $contingentRuleCrudService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var array
     */
    private $supportedTypes;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var ContingentValidationService
     */
    private $contingentValidation;

    public function __construct(
        ContingentRuleRepository $contingentRuleRepository,
        GridHelper $gridHelper,
        ContingentRuleCrudService $contingentRuleCrudService,
        AuthenticationService $authenticationService,
        CurrencyService $currencyService,
        array $supportedTypes,
        ContingentValidationService $contingentValidation
    ) {
        $this->contingentRuleRepository = $contingentRuleRepository;
        $this->gridHelper = $gridHelper;
        $this->contingentRuleCrudService = $contingentRuleCrudService;
        $this->authenticationService = $authenticationService;
        $this->currencyService = $currencyService;
        $this->supportedTypes = $supportedTypes;
        $this->contingentValidation = $contingentValidation;
    }

    public function gridAction(Request $request): array
    {
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();
        $contingentGroupId = $request->requireIdValue('id');

        $searchStruct = new ContingentRuleSearchStruct();
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $this->gridHelper->extractSearchDataInStoreFront($request, $searchStruct);

        $contingentRules = $this->contingentRuleRepository
            ->fetchList($this->supportedTypes, $contingentGroupId, $searchStruct, $currencyContext, $ownershipContext);

        $contingentRules = $this->contingentValidation->validateContingentRules($contingentRules);

        $this->contingentRuleCrudService->removeMultiple($this->contingentValidation
            ->getInvalidContingentRuleOrderNumberIds($contingentRules), $ownershipContext, $currencyContext);

        $count = $this->contingentRuleRepository
            ->fetchTotalCount($this->supportedTypes, $contingentGroupId, $searchStruct, $ownershipContext);

        $maxPage = $this->gridHelper->getMaxPage($count);
        $currentPage = $this->gridHelper->getCurrentPage($request);

        return [
            'gridState' => $this->gridHelper->getGridState($request, $searchStruct, $contingentRules, $maxPage, $currentPage),
            'additionalFormValues' => ['id' => $contingentGroupId],
            'invalidProductNumber' => $request->getParam('invalid'),
        ];
    }

    public function detailAction(Request $request): array
    {
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $id = $request->requireIdValue('id');
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $rule = $this->contingentRuleRepository->fetchOneById($id, $currencyContext, $ownershipContext);

        $validationResponse = $this->gridHelper->getValidationResponse('rule');

        return array_merge(
            $validationResponse,
            [
                'registeredRules' => $this->supportedTypes,
                'rule' => $rule,
                'contingentGroupId' => $rule->contingentGroupId,
                'preselectedRuleType' => $request->getParam('preselectedRuleType'),
            ]
        );
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function updateAction(Request $request): void
    {
        $request->checkPost('grid', ['id' => $request->requireParam('contingentGroupId')]);

        $post = $request->getPost();

        try {
            $this->validatePost($post);
            $serviceRequest = $this->contingentRuleCrudService
                ->createExistingRecordRequest($post);

            $this->validateCrudServiceRequest($serviceRequest);
        } catch (ValidationException $e) {
            throw new B2bControllerForwardException(
                'detail',
                null,
                [
                    'id' => $request->requireIdValue('id'),
                    'contingentGroupId' => $request->requireIdValue('contingentGroupId'),
                    'preselectedRuleType' => $post['type'] ?? null,
                ]
            );
        }

        $ownershipContext = $this->authenticationService->getIdentity()
            ->getOwnershipContext();
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        try {
            $this->contingentRuleCrudService
                ->update($serviceRequest, $ownershipContext, $currencyContext);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);

            throw new B2bControllerForwardException('detail', null, ['contingentGroupId' => $request->requireParam('contingentGroupId')]);
        }

        throw new B2bControllerForwardException('grid', null, ['id' => $post['contingentGroupId']]);
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function removeAction(Request $request): void
    {
        $request->checkPost('grid', ['id' => $request->requireParam('contingentGroupId')]);

        $ownershipContext = $this->authenticationService->getIdentity()
            ->getOwnershipContext();
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $this->contingentRuleCrudService
            ->remove($request->requireIdValue('id'), $ownershipContext, $currencyContext);

        throw new B2bControllerForwardException('grid', null, ['id' => $request->requireParam('contingentGroupId')]);
    }

    public function newAction(Request $request): array
    {
        $contingentGroupId = $request->requireIdValue('contingentGroupId');

        $validationResponse = $this->gridHelper->getValidationResponse('rule');

        return array_merge(
            [
                'registeredRules' => $this->supportedTypes,
                'contingentGroupId' => $contingentGroupId,
                'preselectedRuleType' => $request->getParam('preselectedRuleType'),
            ],
            $validationResponse
        );
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function createAction(Request $request): void
    {
        $request->checkPost('grid', ['id' => $request->requireParam('contingentGroupId')]);
        $post = $request->getPost();

        try {
            $this->validatePost($post);
            $serviceRequest = $this->contingentRuleCrudService
                ->createNewRecordRequest($post);

            $this->validateCrudServiceRequest($serviceRequest);
        } catch (ValidationException $e) {
            throw new B2bControllerForwardException(
                'new',
                null,
                [
                    'contingentGroupId' => $request->requireParam('contingentGroupId'),
                    'preselectedRuleType' => $post['type'] ?? null,
                ]
            );
        }

        $ownershipContext = $this->authenticationService->getIdentity()
            ->getOwnershipContext();

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        try {
            $rule = $this->contingentRuleCrudService
                ->create($serviceRequest, $ownershipContext, $currencyContext);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);

            throw new B2bControllerForwardException('new', null, ['contingentGroupId' => $request->getParam('contingentGroupId')]);
        }

        throw new B2bControllerForwardException('grid', null, ['id' => $rule->contingentGroupId]);
    }

    /**
     * @internal
     * @throws ValidationException
     */
    protected function validatePost(array $post): void
    {
        $type = $post['type'] ?? null;

        if (in_array($type, $this->supportedTypes, true)) {
            return;
        }

        $validationError = $this->contingentValidation->createTypeValidationException();
        $this->gridHelper->pushValidationException($validationError);

        throw $validationError;
    }

    /**
     * @internal
     * @throws ValidationException
     */
    protected function validateCrudServiceRequest(CrudServiceRequest $request): void
    {
        $validationError = $this->contingentValidation
            ->createProductNumberValidationException($request);

        if (!$validationError->getViolations()->count()) {
            return;
        }

        $this->gridHelper->pushValidationException($validationError);

        throw $validationError;
    }
}
