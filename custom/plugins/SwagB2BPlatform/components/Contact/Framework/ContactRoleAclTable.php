<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Acl\Framework\AclTable;
use Shopware\B2B\Role\Framework\AclTableContactContextResolver;
use Shopware\B2B\Role\Framework\AclTableRoleContextResolver;

class ContactRoleAclTable extends AclTable
{
    public function __construct()
    {
        parent::__construct(
            'role_contact',
            'b2b_role',
            'id',
            'b2b_debtor_contact',
            'id'
        );
    }

    protected function getContextResolvers(): array
    {
        return [
            new AclTableRoleContextResolver(),
            new AclTableContactContextResolver(),
        ];
    }
}
