<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use DateTime;
use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use function get_object_vars;
use function property_exists;

class ContactPasswordActivationEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var DateTime
     */
    public $validUntil;

    /**
     * @var string
     */
    public $hash;

    /**
     * @var IdValue
     */
    public $contactId;

    /**
     * @var ContactEntity|null
     */
    public $contact;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->contactId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function isValid(): bool
    {
        return $this->validUntil > new DateTime();
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'valid_until' => $this->validUntil->format('Y-m-d H:i:s'),
            'hash' => $this->hash,
            'contact_id' => $this->contactId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->validUntil = DateTime::createFromFormat('Y-m-d H:i:s', $data['valid_until']);
        $this->hash = $data['hash'];
        $this->contactId = IdValue::create($data['contact_id']);

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }
    }

    public function toArray(): array
    {
        $arrayData = get_object_vars($this);

        foreach ($arrayData as $key => $value) {
            if ($value instanceof IdValue) {
                $arrayData[$key] = $value->getValue();
            }
        }

        return $arrayData;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
