<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Acl\Framework\AclAccessWriterInterface;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Common\Service\MailException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\TransferOwnerDataToContextOwnerService;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserRepositoryInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use function array_key_exists;

class ContactCrudService extends AbstractCrudService
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var ContactValidationService
     */
    private $validationService;

    /**
     * @var AclRepository
     */
    private $aclAddressRepository;

    /**
     * @var ContactPasswordProviderInterface
     */
    private $passwordProvider;

    /**
     * @var ContactPasswordActivationServiceInterface
     */
    private $contactPasswordActivationService;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var AclAccessWriterInterface[]
     */
    private $aclAccessWriters;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    /**
     * @var StoreFrontAuthenticationRepository
     */
    private $authRepository;

    /**
     * @var TransferOwnerDataToContextOwnerService
     */
    private $transferOwnerDataToContextOwnerService;

    /**
     * @param AclAccessWriterInterface[] $aclAccessWriters
     */
    public function __construct(
        ContactRepository $contactRepository,
        ContactValidationService $validationService,
        AclRepository $aclAddressRepository,
        ContactPasswordProviderInterface $passwordProvider,
        ContactPasswordActivationServiceInterface $contactPasswordActivationService,
        LoginContextService $loginContextService,
        array $aclAccessWriters,
        UserRepositoryInterface $userRepository,
        AddressRepositoryInterface $addressRepository,
        CredentialsBuilderInterface $credentialsBuilder,
        StoreFrontAuthenticationRepository $authRepository,
        TransferOwnerDataToContextOwnerService $transferOwnerDataToContextOwnerService
    ) {
        $this->contactRepository = $contactRepository;
        $this->validationService = $validationService;
        $this->aclAddressRepository = $aclAddressRepository;
        $this->passwordProvider = $passwordProvider;
        $this->contactPasswordActivationService = $contactPasswordActivationService;
        $this->loginContextService = $loginContextService;
        $this->aclAccessWriters = $aclAccessWriters;
        $this->userRepository = $userRepository;
        $this->addressRepository = $addressRepository;
        $this->credentialsBuilder = $credentialsBuilder;
        $this->authRepository = $authRepository;
        $this->transferOwnerDataToContextOwnerService = $transferOwnerDataToContextOwnerService;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'passwordNew',
                'passwordRepeat',
                'passwordActivation',
                'password',
                'encoder',
                'email',
                'active',
                'language',
                'title',
                'salutation',
                'firstName',
                'lastName',
                'contextOwnerId',
                'department',
                'defaultBillingAddressId',
                'defaultShippingAddressId',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'passwordNew',
                'passwordRepeat',
                'passwordActivation',
                'password',
                'encoder',
                'email',
                'active',
                'language',
                'title',
                'salutation',
                'firstName',
                'lastName',
                'contextOwnerId',
                'department',
                'defaultBillingAddressId',
                'defaultShippingAddressId',
            ]
        );
    }

    /**
     * @throws ValidationException
     */
    public function create(
        CrudServiceRequest $request,
        Identity $identity,
        AclGrantContext $grantContext
    ): ContactEntity {
        $data = $request->getFilteredData();

        $contact = new ContactEntity();

        $contact->setData($data);

        $this->checkPassword($contact, $request, true);
        $this->passwordProvider->setEncoder($contact);
        $contact->password = $request->requireParam('passwordNew');

        $validation = $this->validationService
            ->createInsertValidation($contact, $identity->getOwnershipContext());

        $this->testValidation($contact, $validation);

        $this->passwordProvider->setPassword($contact, $request->requireParam('passwordNew'));

        $credentials = $this->credentialsBuilder->createCredentialsByEmail($contact->email);

        $this->contactRepository
            ->addContact($contact, $identity->getOwnershipContext());

        $contact = $this->contactRepository
            ->fetchOneByCredentials($credentials);

        if ($request->hasValueForParam('passwordActivation')) {
            try {
                $this->contactPasswordActivationService->sendPasswordActivationEmail($contact);
            } catch (MailException $e) {
                $this->testValidation($contact, $this->validationService->createMailNotSent($contact));
            }
        }

        $contact->authId = $this->loginContextService
            ->getAuthId(ContactRepository::class, $contact->id, $contact->contextOwnerId);

        $this->contactRepository
            ->setAuthId($contact->id, $contact->authId, $identity->getOwnershipContext());

        $this->aclAddressRepository->allowAll(
            $contact,
            [
                $identity->getMainShippingAddress()->id,
                $identity->getMainBillingAddress()->id,
            ]
        );

        foreach ($this->aclAccessWriters as $aclAccessWriter) {
            $aclAccessWriter->addNewSubject(
                $identity->getOwnershipContext(),
                $grantContext,
                $contact->id,
                true
            );
        }

        return $contact;
    }

    public function createByApi(
        CrudServiceRequest $request,
        OwnershipContext $ownershipContext
    ): ContactEntity {
        $data = $request->getFilteredData();

        $contact = new ContactEntity();
        $contact->setData($data);

        try {
            $contact = $this->contactRepository
                ->fetchOneByEmail($contact->email, $ownershipContext);
        } catch (NotFoundException $e) {
            $this->contactRepository
                ->addContact($contact, $ownershipContext);

            $contact = $this->contactRepository
                ->fetchOneByEmail($contact->email, $ownershipContext);

            $contact->authId = $this->loginContextService
                ->getAuthId(ContactRepository::class, $contact->id, $contact->contextOwnerId);

            $this->contactRepository
                ->setAuthId($contact->id, $contact->authId, $ownershipContext);
        }

        $contact->authId = $this->loginContextService
            ->getAuthId(ContactRepository::class, $contact->id, $contact->contextOwnerId);

        return $contact;
    }

    /**
     * @throws ValidationException
     */
    public function update(CrudServiceRequest $request, OwnershipContext $ownershipContext): ContactEntity
    {
        $data = $request->getFilteredData();
        $contact = new ContactEntity();
        $contact->setData($data);

        if (array_key_exists('passwordNew', $data) && $data['passwordNew']) {
            $contact->password = $request->requireParam('passwordNew');
            $passwordCheckRequired = true;
        }

        $this->checkPassword($contact, $request, $passwordCheckRequired ?? false);

        $validation = $this->validationService
            ->createUpdateValidation($contact, $ownershipContext);

        $this->testValidation($contact, $validation);

        if ($request->hasValueForParam('passwordActivation')) {
            try {
                $this->contactPasswordActivationService->sendPasswordActivationEmail($contact);
            } catch (MailException $e) {
                $this->testValidation($contact, $this->validationService->createMailNotSent($contact));
            }
        }

        if ($request->hasValueForParam('passwordNew')) {
            $this->passwordProvider->setPassword($contact, $request->requireParam('passwordNew'));
        }

        $originalContact = $this->contactRepository->fetchOneById($contact->id, $ownershipContext);

        $this->contactRepository
            ->updateContact($contact, $ownershipContext);

        if ($originalContact->email !== $contact->email) {
            $credentials = $this->credentialsBuilder->createCredentialsByEmail($originalContact->email);
            $this->userRepository->updateEmail($credentials, $contact->email);
        }

        if ($request->hasValueForParam('passwordNew')) {
            $this->passwordProvider->resetFailedLoginsAndLockedUntil($contact);
        }

        return $contact;
    }

    public function remove(IdValue $id, OwnershipContext $ownershipContext): ContactEntity
    {
        $contact = $this->contactRepository->fetchOneById($id, $ownershipContext);

        $validation = $this->validationService
            ->createDeleteValidation($contact, $ownershipContext);

        $this->testValidation($contact, $validation);

        $this->contactRepository
            ->removeContact($contact, $ownershipContext);

        $this->userRepository
            ->removeUsersByEmail([$contact->email]);

        $this->transferOwnerDataToContextOwnerService
            ->transferOwnerDataToContextOwner($contact->authId, $ownershipContext->contextOwnerId);

        $this->authRepository->removeUser($contact->authId);

        return $contact;
    }

    /**
     * @throws ValidationException
     */
    public function activateContact(CrudServiceRequest $request, OwnershipContext $ownershipContext): ContactEntity
    {
        $data = $request->getFilteredData();
        $contact = new ContactEntity();
        $contact->setData($data);

        $this->checkPassword($contact, $request, true);

        $validation = $this->validationService
            ->createUpdateValidation($contact, $ownershipContext);

        $this->testValidation($contact, $validation);

        try {
            $this->contactPasswordActivationService->sendPasswordActivationEmail($contact);
        } catch (MailException $e) {
            $this->testValidation($contact, $this->validationService->createMailNotSent($contact));
        }

        $this->passwordProvider->setPassword($contact, $request->requireParam('passwordNew'));

        $originalContact = $this->contactRepository->fetchOneById($contact->id, $ownershipContext);

        $this->contactRepository
            ->updateContact($contact, $ownershipContext);

        $this->passwordProvider->resetFailedLoginsAndLockedUntil($contact);

        return $contact;
    }

    /**
     * @internal
     */
    protected function checkPassword(ContactEntity $contact, CrudServiceRequest $request, bool $required): void
    {
        if (!$this->hasAnyPasswordSet($request)) {
            if ($required) {
                $this->throwPasswordRequiredException($contact);
            }

            return;
        }

        if ($this->hasMatchingPasswords($request)) {
            $this->throwPasswordNotMatchingException($contact);
        }
    }

    /**
     * @internal
     */
    protected function throwPasswordNotMatchingException(ContactEntity $contact): void
    {
        $violation = new ConstraintViolation(
            'The password is not equal to the repeated password.',
            'The password is not equal to the repeated password.',
            [],
            '',
            'Confirm',
            '',
            null
        );

        $violationList = new ConstraintViolationList([$violation]);

        throw new ValidationException(
            $contact,
            $violationList,
            'Validation violations detected, can not proceed:',
            400
        );
    }

    /**
     * @internal
     */
    protected function throwPasswordRequiredException(ContactEntity $contact): void
    {
        $violation = new ConstraintViolation(
            'A password is required.',
            'A password is required.',
            [],
            '',
            'Password',
            '',
            null
        );

        $violationList = new ConstraintViolationList([$violation]);

        throw new ValidationException(
            $contact,
            $violationList,
            'Validation violations detected, can not proceed:',
            400
        );
    }

    /**
     * @internal
     */
    protected function hasAnyPasswordSet(CrudServiceRequest $request): bool
    {
        return $request->hasValueForParam('passwordNew') && $request->hasValueForParam('passwordRepeat');
    }

    /**
     * @internal
     */
    protected function hasMatchingPasswords(CrudServiceRequest $request): bool
    {
        return !$request->hasValueForParam('passwordNew')
            || !$request->hasValueForParam('passwordRepeat')
            || $request->requireParam('passwordNew') !== $request->requireParam('passwordRepeat')
            || !$request->requireParam('passwordNew');
    }

    public function validateShopUserDataForRequest(array $data): array
    {
        return [
            'password' => $data['password'],
            'encoder' => $data['encoder'],
            'email' => $data['email'],
            'active' => (bool) $data['active'],
            'language' => IdValue::create($data['language']),
            'title' => $data['title'],
            'salutation' => $data['salutation'],
            'firstName' => $data['firstname'],
            'lastName' => $data['lastname'],
            'contextOwnerId' => IdValue::create($data['contextOwnerId']),
            'defaultBillingAddressId' => IdValue::create($data['default_billing_address_id']),
            'defaultShippingAddressId' => IdValue::create($data['default_shipping_address_id']),
        ];
    }
}
