<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\StoreFrontAuthentication\Framework\AclAuthReadHelperInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;

class ContactAclAuthReadHelper implements AclAuthReadHelperInterface
{
    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    public function __construct(AclReadHelper $aclReadHelper)
    {
        $this->aclReadHelper = $aclReadHelper;
    }

    public function applyAclVisibility(OwnershipContext $ownershipContext, QueryBuilder $queryBuilder, string $alias = 'aclQuery'): void
    {
        if ($ownershipContext->identityClassName !== ContactIdentity::class) {
            return;
        }

        $queryBuilder
            ->leftJoin(
                StoreFrontAuthenticationRepository::TABLE_ALIAS,
                ContactRepository::TABLE_NAME,
                ContactRepository::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id = '
                . ContactRepository::TABLE_ALIAS . '.auth_id'
            );

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $queryBuilder, $alias);
    }
}
