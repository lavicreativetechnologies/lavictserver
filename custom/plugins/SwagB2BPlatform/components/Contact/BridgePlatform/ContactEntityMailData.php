<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\BridgePlatform;

use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Debtor\BridgePlatform\DebtorEntityMailData;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class ContactEntityMailData
{
    /**
     * @var ContactEntity
     */
    private $contactEntity;

    public function __construct(?ContactEntity $contactEntity)
    {
        $this->contactEntity = $contactEntity;
    }

    public static function getContactMailDataType(): EventDataType
    {
        $contact = new ObjectType();
        $contact->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('encoder', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('email', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('active', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $contact->add('language', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('title', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('salutation', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('firstName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('lastName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('department', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('contextOwnerId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('debtor', DebtorEntityMailData::getDebtorEntityDataType());
        $contact->add('authId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('defaultBillingAddressId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('defaultShippingAddressId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $contact->add('avatar', new ScalarValueType(ScalarValueType::TYPE_STRING));

        return $contact;
    }

    public function getId(): string
    {
        return $this->contactEntity ? (string) $this->contactEntity->id->getValue() : '';
    }

    public function getPassword(): string
    {
        return $this->contactEntity ? $this->contactEntity->password : '';
    }

    public function getEncoder(): string
    {
        return $this->contactEntity ? $this->contactEntity->encoder : '';
    }

    public function getEmail(): string
    {
        return $this->contactEntity ? $this->contactEntity->email : '';
    }

    public function isActive(): bool
    {
        return $this->contactEntity ? $this->contactEntity->active : '';
    }

    public function getLanguage(): string
    {
        return $this->contactEntity ? (string) $this->contactEntity->language->getValue() : '';
    }

    public function getTitle(): string
    {
        return $this->contactEntity ? $this->contactEntity->title : '';
    }

    public function getSalutation(): string
    {
        return $this->contactEntity ? $this->contactEntity->salutation : '';
    }

    public function getFirstName(): string
    {
        return $this->contactEntity ? $this->contactEntity->firstName : '';
    }

    public function getLastName(): string
    {
        return $this->contactEntity ? $this->contactEntity->lastName : '';
    }

    public function getDepartment(): string
    {
        return $this->contactEntity ? $this->contactEntity->department : '';
    }

    public function getContextOwnerId(): string
    {
        return $this->contactEntity ? (string) $this->contactEntity->contextOwnerId->getValue() : '';
    }

    public function getDebtor(): DebtorEntityMailData
    {
        return new DebtorEntityMailData($this->contactEntity ? $this->contactEntity->debtor : null);
    }

    public function getAuthId(): string
    {
        return $this->contactEntity ? (string) $this->contactEntity->authId->getValue() : '';
    }

    public function getDefaultBillingAddressId(): string
    {
        return $this->contactEntity ? (string) $this->contactEntity->defaultBillingAddressId->getValue() : '';
    }

    public function getDefaultShippingAddressId(): string
    {
        return $this->contactEntity ? (string) $this->contactEntity->defaultShippingAddressId->getValue() : '';
    }

    public function getAvatar(): string
    {
        return $this->contactEntity ? $this->contactEntity->avatar : '';
    }
}
