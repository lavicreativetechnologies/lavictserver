<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\BridgePlatform;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactIdentity;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationEntity;
use function explode;
use function in_array;

class ContactAuthenticationIdentityLoader implements AuthenticationIdentityLoaderInterface
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var DebtorRepositoryInterface
     */
    private $debtorRepository;

    public function __construct(
        ContactRepository $contactRepository,
        DebtorRepositoryInterface $debtorRepository
    ) {
        $this->contactRepository = $contactRepository;
        $this->debtorRepository = $debtorRepository;
    }

    public function fetchIdentityByEmail(string $email, LoginContextService $contextService, bool $isApi = false): Identity
    {
        $entity = $this->contactRepository->insecureFetchOneByEmail($email);

        return $this->getIdentity($entity, $contextService);
    }

    public function fetchIdentityByAuthentication(StoreFrontAuthenticationEntity $authentication, LoginContextService $contextService, bool $isApi = false): Identity
    {
        if (!$this->isValidProviderKey($authentication->providerKey)) {
            throw new NotFoundException('The given authentication, does not belong to this loader');
        }

        $entity = $this->contactRepository
            ->insecureFetchOneById($authentication->providerContext);

        return $this->getIdentity($entity, $contextService);
    }

    /**
     * @internal
     */
    protected function getIdentity(ContactEntity $entity, LoginContextService $contextService): ContactIdentity
    {
        /** @var DebtorIdentity $debtorIdentity */
        $debtorIdentity = $this
            ->debtorRepository
            ->fetchIdentityById($entity->debtor->id, $contextService);

        $authId = $contextService->getAuthId(ContactRepository::class, $entity->id, $debtorIdentity->getAuthId());

        $this->contactRepository->setAuthId($entity->id, $authId, $debtorIdentity->getOwnershipContext());

        return new ContactIdentity(
            $authId,
            $entity->id,
            ContactRepository::TABLE_NAME,
            $entity,
            $debtorIdentity,
            $contextService->getAvatar($authId)
        );
    }

    public function fetchIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity, LoginContextService $contextService, bool $isApi = false): Identity
    {
        if (!$credentialsEntity->email) {
            throw new NotFoundException('Unable to handle context');
        }

        $contactEntity = $this->contactRepository->fetchOneByCredentials($credentialsEntity);

        return $this->getIdentity($contactEntity, $contextService);
    }

    /**
     * @internal
     */
    protected function isValidProviderKey(string $providerKey): bool
    {
        return in_array('Contact', explode('\\', $providerKey), true);
    }
}
