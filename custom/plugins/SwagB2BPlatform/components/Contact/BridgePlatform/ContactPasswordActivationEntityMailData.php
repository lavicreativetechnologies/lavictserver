<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\BridgePlatform;

use Shopware\B2B\Contact\Framework\ContactPasswordActivationEntity;
use Shopware\B2B\Shop\BridgePlatform\DateTimeDataType;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class ContactPasswordActivationEntityMailData
{
    /**
     * @var ContactPasswordActivationEntity
     */
    private $activationEntity;

    public function __construct(ContactPasswordActivationEntity $activationEntity)
    {
        $this->activationEntity = $activationEntity;
    }

    public static function getPasswordActivationDataType(): EventDataType
    {
        $password = new ObjectType();
        $password->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $password->add('validUntil', DateTimeDataType::getDateTimeData());
        $password->add('hash', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $password->add('contactId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $password->add('contact', ContactEntityMailData::getContactMailDataType());

        return $password;
    }

    public function getId(): string
    {
        return (string) $this->activationEntity->id;
    }

    public function getValidUtil(): DateTimeDataType
    {
        return new DateTimeDataType($this->activationEntity->validUntil);
    }

    public function getHash(): string
    {
        return $this->activationEntity->hash;
    }

    public function getContactId(): string
    {
        return (string) $this->activationEntity->contactId->getValue();
    }

    public function getContact(): ContactEntityMailData
    {
        return new ContactEntityMailData($this->activationEntity->contact);
    }
}
