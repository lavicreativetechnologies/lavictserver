<?php declare(strict_types=1);

namespace Shopware\B2B\Dashboard\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

interface EmotionRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function fetchEmotion(Identity $identity): EmotionEntity;

    /**
     * @throws NotFoundException
     */
    public function getDirectEmotionIdByAuthId(IdValue $authId): IdValue;

    public function updateEmotion(IdValue $authId, IdValue $emotionId);

    /**
     * @return EmotionEntity[]
     */
    public function getAllEmotions(): array;
}
