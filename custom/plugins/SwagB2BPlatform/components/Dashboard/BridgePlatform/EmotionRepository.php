<?php declare(strict_types=1);

namespace Shopware\B2B\Dashboard\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Dashboard\Framework\EmotionEntity;
use Shopware\B2B\Dashboard\Framework\EmotionRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class EmotionRepository implements EmotionRepositoryInterface
{
    public function fetchEmotion(Identity $identity): EmotionEntity
    {
        throw new NotFoundException('Shopware platform cms not supported yet');
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectEmotionIdByAuthId(IdValue $authId): IdValue
    {
        throw new NotFoundException('Shopware platform cms not supported yet');
    }

    public function updateEmotion(IdValue $authId, IdValue $emotionId): void
    {
        throw new NotFoundException('Shopware platform cms not supported yet');
    }

    /**
     * {@inheritdoc}
     */
    public function getAllEmotions(): array
    {
        throw new NotFoundException('Shopware platform cms not supported yet');
    }
}
