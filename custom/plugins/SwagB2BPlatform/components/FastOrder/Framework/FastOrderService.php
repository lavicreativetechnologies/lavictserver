<?php declare(strict_types=1);

namespace Shopware\B2B\FastOrder\Framework;

use Shopware\B2B\Common\File\CsvReader;
use Shopware\B2B\Common\File\XlsReader;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemShopWriterServiceInterface;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Symfony\Component\HttpFoundation\File\File;
use function array_diff;
use function array_key_exists;
use function array_keys;
use function count;
use function in_array;
use function trim;

class FastOrderService
{
    /**
     * @var CsvReader
     */
    private $csvReader;

    /**
     * @var XlsReader
     */
    private $xlsReader;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    /**
     * @var LineItemShopWriterServiceInterface
     */
    private $lineItemShopWriterService;

    public function __construct(
        CsvReader $csvReader,
        XlsReader $xlsReader,
        ProductServiceInterface $productService,
        LineItemShopWriterServiceInterface $lineItemShopWriterService
    ) {
        $this->csvReader = $csvReader;
        $this->xlsReader = $xlsReader;
        $this->productService = $productService;
        $this->lineItemShopWriterService = $lineItemShopWriterService;
    }

    public function processFastOrderFile(File $file, FastOrderContext $fastOrderContext, OwnershipContext $ownershipContext): array
    {
        $fastOrders = $this->createLineItemReferencesFromFileObject($file, $fastOrderContext, $ownershipContext);

        if (isset($fastOrders['error'])) {
            return $fastOrders;
        }

        $productOrderNumbers = array_keys($fastOrders);

        $matchingProductOrderNumbers = $this->productService
            ->fetchProductNamesByOrderNumbers($productOrderNumbers);

        $notMatchingProducts = array_diff(
            $productOrderNumbers,
            array_keys($matchingProductOrderNumbers)
        );

        $products = [];
        foreach ($productOrderNumbers as $productOrderNumber) {
            if (!array_key_exists($productOrderNumber, $matchingProductOrderNumbers)) {
                continue;
            }

            $lineItemReference = $fastOrders[$productOrderNumber];
            $lineItemReference->name = $matchingProductOrderNumbers[$productOrderNumber];

            $products[] = $lineItemReference;
        }

        return [
            'matchingProducts' => $products,
            'notMatchingProducts' => $notMatchingProducts,
        ];
    }

    /**
     * @internal
     * @return array|LineItemReference[]
     */
    protected function createLineItemReferencesFromFileObject(File $fileObject, FastOrderContext $fastOrderContext, OwnershipContext $ownershipContext): array
    {
        $filePath = $fileObject->getPath() . '/' . $fileObject->getFilename();
        $fileExtension = $fileObject->getExtension();

        if ($fileExtension === 'csv') {
            $fastOrders = $this->csvReader
                ->read($filePath, $fastOrderContext);
        } elseif (in_array($fileExtension, ['xls', 'xlsx'], true)) {
            $fastOrders = $this->xlsReader
                ->read($filePath, $fastOrderContext);
        } else {
            return ['error' => 'file'];
        }

        return $this->mapDataToLineItemReference($fastOrders, $fastOrderContext, $ownershipContext);
    }

    /**
     * @internal
     * @return array|LineItemReference[]
     */
    protected function mapDataToLineItemReference(array $data, FastOrderContext $fastOrderContext, OwnershipContext $ownershipContext)
    {
        $lineItemReferences = [];

        foreach ($data as $value) {
            $orderNumber = trim((string) $value[$fastOrderContext->orderNumberColumn]);
            $quantity = (int) ($value[$fastOrderContext->quantityColumn] ?? 0);

            if (!$orderNumber) {
                continue;
            }

            try {
                $orderNumber = $this->productService->fetchOrderNumberByReferenceNumber($orderNumber);
            } catch (NotFoundException $e) {
                //nth
            }

            if (isset($lineItemReferences[$orderNumber])) {
                $lineItemReferences[$orderNumber]->quantity += $quantity;
            } else {
                $reference = new LineItemReference();
                $reference->referenceNumber = $orderNumber;
                $reference->quantity = $quantity;

                $lineItemReferences[$orderNumber] = $reference;
            }
        }

        if (count($lineItemReferences) === 0) {
            return ['error' => 'products'];
        }

        return $lineItemReferences;
    }

    /**
     * @param bool $clearBasket
     */
    public function produceCart(LineItemList $lineItemList, $clearBasket = false): void
    {
        $this->lineItemShopWriterService
            ->triggerCart($lineItemList, $clearBasket);
    }
}
