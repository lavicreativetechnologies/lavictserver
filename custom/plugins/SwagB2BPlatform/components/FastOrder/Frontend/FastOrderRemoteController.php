<?php declare(strict_types=1);

namespace Shopware\B2B\FastOrder\Frontend;

use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\FastOrder\Framework\FastOrderService;
use Shopware\B2B\OrderList\Framework\OrderListRepository;
use Shopware\B2B\OrderList\Framework\OrderListService;
use Shopware\B2B\OrderList\Framework\RemoteBoxService;
use Shopware\B2B\OrderList\Frontend\RemoteBoxController;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function array_merge;

class FastOrderRemoteController extends RemoteBoxController
{
    /**
     * @var FastOrderService
     */
    private $fastOrderService;

    /**
     * @var RemoteBoxService
     */
    private $remoteBoxService;

    /**
     * @var OrderListService
     */
    private $orderListService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    public function __construct(
        FastOrderService $fastOrderService,
        OrderListService $orderListService,
        RemoteBoxService $remoteBoxService,
        CurrencyService $currencyService,
        AuthenticationService $authenticationService,
        OrderListRepository $orderListRepository,
        GridHelper $gridHelper
    ) {
        parent::__construct(
            $remoteBoxService,
            $authenticationService,
            $currencyService,
            $gridHelper,
            $orderListRepository
        );
        $this->fastOrderService = $fastOrderService;
        $this->remoteBoxService = $remoteBoxService;
        $this->orderListService = $orderListService;
        $this->currencyService = $currencyService;
        $this->orderListRepository = $orderListRepository;
    }

    public function remoteListFastOrderAction(Request $request): array
    {
        $orderLists = $this->getOrderListsFromRequest($request);

        $defaultOrderList = null;
        try {
            $defaultOrderList = $this->orderListRepository
                ->fetchDefaultOrderList($this->currencyService->createCurrencyContext(), $this->getOwnershipContext());
        } catch (NotFoundException $e) {
            // nth
        }

        return [
            'orderLists' => $orderLists,
            'orderListId' => (int) $request->getParam('orderListId'),
            'message' => $request->getParam('message'),
            'validationExceptions' => $request->getParam('validationExceptions'),
            'defaultOrderList' => $defaultOrderList,
        ];
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function addProductsToOrderListAction(Request $request): void
    {
        $responseData = [
            'orderListId' => $request->requireParam('orderlist'),
        ];
        $ownershipContext = $this->getOwnershipContext();

        $lineItemList = $this
            ->createLineItemListFromRequest($request, $responseData);

        try {
            $this->orderListService->addListThroughLineItemList(
                $request->requireIdValue('orderlist'),
                $lineItemList,
                $this->currencyService->createCurrencyContext(),
                $ownershipContext
            );
        } catch (NotFoundException $e) {
            throw new B2bControllerForwardException(
                $this->getListingActionName(),
                $this->getControllerName(),
                array_merge($responseData, ['message' => ['key' => 'NoOrderList', 'type' => 'error']])
            );
        } catch (ValidationException $e) {
            $this->remoteBoxService->addError($e);
            throw new B2bControllerForwardException(
                $this->getListingActionName(),
                $this->getControllerName(),
                array_merge($responseData, $this->getMessages(false))
            );
        }

        $responseData = array_merge($responseData, $this->getMessages());

        throw new B2bControllerForwardException(
            $this->getListingActionName(),
            $this->getControllerName(),
            $responseData
        );
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function addProductsToCartAction(Request $request): void
    {
        $responseData = [
            'orderListId' => $request->getParam('orderlist'),
        ];

        try {
            $lineItemList = $this->createLineItemListFromRequest($request, $responseData);

            $this->fastOrderService->produceCart($lineItemList);

            $responseData = array_merge($responseData, $this->getMessages());
        } catch (B2bControllerForwardException $forwardException) {
            $responseData = array_merge($forwardException->getParams(), $responseData);
        }

        $responseData['message']['key'] .= 'Cart';

        throw new B2bControllerForwardException(
            $this->getListingActionName(),
            $this->getControllerName(),
            $responseData
        );
    }

    protected function getControllerName(): string
    {
        return 'b2bfastorderremote';
    }

    protected function getListingActionName(): string
    {
        return 'remoteListFastOrder';
    }
}
