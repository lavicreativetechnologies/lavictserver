<?php declare(strict_types=1);

namespace Shopware\B2B\FastOrder\Frontend;

use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\FastOrder\Framework\FastOrderContext;
use Shopware\B2B\FastOrder\Framework\FastOrderService;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function trim;

class FastOrderController
{
    const COUNTING_FROM = 1;

    /**
     * @var FastOrderService
     */
    private $fastOrderService;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        FastOrderService $fastOrderService,
        ProductServiceInterface $productService,
        AuthenticationService $authenticationService
    ) {
        $this->fastOrderService = $fastOrderService;
        $this->productService = $productService;
        $this->authenticationService = $authenticationService;
    }

    public function indexAction(): void
    {
        // nth
    }

    public function uploadAction(): void
    {
        // nth
    }

    public function defaultListAction(): void
    {
        // nth
    }

    public function processUploadAction(Request $request): array
    {
        $fastOrderContext = $this->createFastOrderContextFromRequest($request);
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $fastOrderFile = $request->requireFileParam('uploadedFile');

        $products = $this->fastOrderService
            ->processFastOrderFile(
                $fastOrderFile,
                $fastOrderContext,
                $ownershipContext
            );

        return $products;
    }

    public function getProductNameAction(Request $request): array
    {
        $orderNumber = $request->getParam('orderNumber', '');

        try {
            $productName = $this->productService->fetchProductNameByOrderNumber(trim($orderNumber));
        } catch (NotFoundException $e) {
            $productName = false;
        }

        return ['productName' => $productName];
    }

    /**
     * @internal
     */
    protected function createFastOrderContextFromRequest(Request $request): FastOrderContext
    {
        $context = new FastOrderContext();
        if ($request->hasParam('orderNumberColumn')) {
            $context->orderNumberColumn = (int) $request
                ->getParam('orderNumberColumn');
        }

        if ($request->hasParam('quantityColumn')) {
            $context->quantityColumn = (int) $request
                ->getParam('quantityColumn');
        }

        if ($request->hasParam('csvDelimiter')) {
            $context->csvDelimiter = $request
                ->getParam('csvDelimiter');
        }

        if ($request->hasParam('csvEnclosure') && ($enclosure = $request->getParam('csvEnclosure')) !== '') {
            $context->csvEnclosure = $enclosure;
        }

        if ($request->hasParam('headline')) {
            $context->headline = $request
                    ->getParam('headline') === 'true';
        }

        $context->orderNumberColumn -= static::COUNTING_FROM;
        $context->quantityColumn -= static::COUNTING_FROM;

        return $context;
    }
}
