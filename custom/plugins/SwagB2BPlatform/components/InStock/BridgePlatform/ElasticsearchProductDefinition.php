<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use ONGR\ElasticsearchDSL\Query\Compound\BoolQuery;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Elasticsearch\Framework\AbstractElasticsearchDefinition;
use Shopware\Elasticsearch\Framework\FullText;
use Shopware\Elasticsearch\Framework\Indexing\EntityMapper;

class ElasticsearchProductDefinition extends AbstractElasticsearchDefinition
{
    private const EXTENSION_NAME = SalesChannelProductExtension::B2B_IN_STOCK_EXTENSION_NAME;

    /**
     * @var AbstractElasticsearchDefinition
     */
    private $decorated;

    public function __construct(
        AbstractElasticsearchDefinition $decorated,
        EntityMapper $mapper
    ) {
        parent::__construct($mapper);
        $this->decorated = $decorated;
    }

    public function getEntityDefinition(): EntityDefinition
    {
        return $this->decorated->getEntityDefinition();
    }

    public function extendCriteria(Criteria $criteria): void
    {
        $this->decorated->extendCriteria($criteria);
        $criteria->addAssociation(self::EXTENSION_NAME);
    }

    public function getMapping(Context $context): array
    {
        $mapping = $this->decorated->getMapping($context);
        $definition = $this->decorated->getEntityDefinition();

        $mapping['properties'][self::EXTENSION_NAME] = $this->mapper->mapField(
            $definition,
            $definition->getField(self::EXTENSION_NAME),
            $context
        );

        return $mapping;
    }

    public function buildTermQuery(Context $context, Criteria $criteria): BoolQuery
    {
        return $this->decorated->buildTermQuery($context, $criteria);
    }

    public function extendEntities(EntityCollection $collection): EntityCollection
    {
        return $this->decorated->extendEntities($collection);
    }

    public function hasNewIndexerPattern(): bool
    {
        return $this->decorated->hasNewIndexerPattern();
    }

    public function buildFullText(Entity $entity): FullText
    {
        return $this->decorated->buildFullText($entity);
    }
}
