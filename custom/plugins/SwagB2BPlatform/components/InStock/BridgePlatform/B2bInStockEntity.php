<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;

class B2bInStockEntity extends Entity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $authId;

    /**
     * @var int
     */
    protected $inStock;

    /**
     * @var string
     */
    protected $productId;

    public function getId(): int
    {
        return $this->id;
    }

    public function getAuthId(): int
    {
        return $this->authId;
    }

    public function getInStock(): int
    {
        return $this->inStock;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
