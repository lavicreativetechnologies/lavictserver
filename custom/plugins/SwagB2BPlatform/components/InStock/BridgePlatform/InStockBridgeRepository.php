<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\InStock\Framework\InStockEntity;
use Shopware\B2B\InStock\Framework\InStockRepository;
use Shopware\B2B\InStock\Framework\InStockSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class InStockBridgeRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
    }

    /**
     * @param string[] $productIds
     * @return InStockEntity[]
     */
    public function fetchInStocksByProductIds(
        array $productIds,
        Identity $identity,
        InStockSearchStruct $searchStruct
    ): array {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(InStockRepository::TABLE_NAME)
            ->where('auth_id = :authId')
            ->orWhere('auth_id = :contextAuthId')
            ->andWhere('product_id IN (:productIds)')
            ->setParameters(
                [
                    'authId' => $identity->getAuthId()->getStorageValue(),
                    'contextAuthId' => $identity->getContextAuthId()->getStorageValue(),
                ]
            )
            ->setParameter('productIds', $productIds, Connection::PARAM_STR_ARRAY);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = 'id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $data = $query->execute()->fetchAll();

        $inStocks = [];
        foreach ($data as $inStock) {
            /** @var InStockEntity $entity */
            $entity = (new InStockEntity())->fromDatabaseArray($inStock);
            $inStocks[$entity->productId->getValue()] = $entity;
        }

        return $inStocks;
    }
}
