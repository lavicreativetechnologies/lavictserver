<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\B2bInStockCartError;
use Shopware\B2B\Cart\BridgePlatform\CartAccessError;
use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\InStockInterceptDestination;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\InStock\Framework\InStockSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartValidatorInterface;
use Shopware\Core\Checkout\Cart\Error\ErrorCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use function array_keys;

class InStockCartValidator implements CartValidatorInterface
{
    private const PRODUCT_HAS_NO_STOCKS_KEY = 'ProductHasNotEnoughStocks';

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var InStockBridgeRepository
     */
    private $inStockBridgeRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        InStockBridgeRepository $inStockBridgeRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->inStockBridgeRepository = $inStockBridgeRepository;
    }

    public function validate(
        Cart $cart,
        ErrorCollection $errorCollection,
        SalesChannelContext $salesChannelContext
    ): void {
        if (!$this->authenticationService->isB2b() || !$cart->getLineItems()->count()) {
            return;
        }

        $lineItems = $cart->getLineItems();
        $inStocks = $this->inStockBridgeRepository->fetchInStocksByProductIds(
            IdValue::fromHexToBytesList(array_keys($lineItems->getReferenceIds())),
            $this->authenticationService->getIdentity(),
            new InStockSearchStruct()
        );

        $cartAccessResult = new CartAccessResult();
        $cartState = CartState::extract($cart);

        foreach ($inStocks as $productId => $inStock) {
            /** @var LineItem $lineItem */
            $lineItem = $lineItems->get($productId);
            if ($inStock->inStock - $lineItem->getQuantity() >= 0) {
                continue;
            }

            $cartAccessResult->addError(
                __CLASS__,
                self::PRODUCT_HAS_NO_STOCKS_KEY,
                ['name' => $lineItem->getLabel(), 'id' => $productId]
            );
        }

        if (!$cartAccessResult->hasErrors()) {
            return;
        }

        $this->mapErrorsIntoCartState($cartAccessResult, $cartState);
        $errorCollection->add(B2bInStockCartError::createCartBlockedForInStockMessage($cart->getToken()));

        $cartState->setDestination(new InStockInterceptDestination());
    }

    /**
     * @internal
     */
    protected function mapErrorsIntoCartState(CartAccessResult $result, CartState $cartState): void
    {
        foreach ($result->getErrors() as $index => $error) {
            $cartState->getMessages()->add(CartAccessError::fromErrorMessage($error, $index));
        }
    }
}
