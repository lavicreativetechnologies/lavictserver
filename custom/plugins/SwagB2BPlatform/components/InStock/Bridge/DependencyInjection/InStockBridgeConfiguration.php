<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Filter\DependencyInjection\FilterConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\InStock\BridgePlatform\DependencyInjection\InStockBridgeConfiguration as PlatformInStockBridgeConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Bridge\DependencyInjection\StoreFrontAuthenticationBridgeConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class InStockBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformInStockBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            StoreFrontAuthenticationBridgeConfiguration::create(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
            new FilterConfiguration(),
        ];
    }
}
