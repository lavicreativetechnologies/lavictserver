<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Framework;

use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InStockValidationService
{
    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
    }

    public function createInsertValidation(InStockEntity $entity): Validator
    {
        return $this->createCrudValidation($entity)
            ->validateThat('id', $entity->id)
                ->isNullIdValue()
            ->getValidator($this->validator);
    }

    public function createUpdateValidation(InStockEntity $entity): Validator
    {
        return $this->createCrudValidation($entity)
                ->validateThat('id', $entity->id)
                    ->isIdValue()
                ->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function createCrudValidation(InStockEntity $entity): ValidationBuilder
    {
        return $this->validationBuilder
            ->validateThat('productId', $entity->productId)
                ->isIdValue()
            ->validateThat('authId', $entity->authId)
                ->isIdValue()
            ->validateThat('inStock', $entity->inStock)
                ->isNotBlank()
                ->isInt();
    }
}
