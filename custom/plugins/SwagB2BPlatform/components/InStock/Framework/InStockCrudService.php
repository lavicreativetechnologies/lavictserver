<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;

class InStockCrudService extends AbstractCrudService
{
    /**
     * @var InStockRepository
     */
    private $inStockRepository;

    /**
     * @var InStockValidationService
     */
    private $validationService;

    public function __construct(
        InStockRepository $inStockRepository,
        InStockValidationService $validationService
    ) {
        $this->inStockRepository = $inStockRepository;
        $this->validationService = $validationService;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'authId',
                'productId',
                'inStock',
                'articlesDetailsId',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'authId',
                'productId',
                'inStock',
                'articlesDetailsId',
            ]
        );
    }

    public function create(CrudServiceRequest $request): InStockEntity
    {
        $data = $request->getFilteredData();

        $entity = new InStockEntity();
        $entity->setData($data);

        $validation = $this->validationService
            ->createInsertValidation($entity);

        $this->testValidation($entity, $validation);

        $this->inStockRepository->addInStock($entity);

        return $entity;
    }

    public function update(CrudServiceRequest $request): InStockEntity
    {
        $data = $request->getFilteredData();

        $entity = new InStockEntity();
        $entity->setData($data);

        $validation = $this->validationService
            ->createUpdateValidation($entity);

        $this->testValidation($entity, $validation);

        return $this->inStockRepository
            ->updateInStock($entity);
    }

    public function remove(IdValue $id): InStockEntity
    {
        $entity = $this->inStockRepository->fetchOneById($id);

        $this->inStockRepository->removeInStock($entity);

        return $entity;
    }
}
