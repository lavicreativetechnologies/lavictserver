<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Framework;

use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function array_replace;

class InStockHelper
{
    /**
     * @var InStockRepository
     */
    private $inStockRepository;

    public function __construct(InStockRepository $inStockRepository)
    {
        $this->inStockRepository = $inStockRepository;
    }

    /**
     * @return InStockEntity[]
     */
    public function getCascadedInStocksForAuthId(Identity $identity, InStockSearchStruct $searchStruct): array
    {
        $inStocks = $this->inStockRepository
            ->fetchInStocksByAuthId($identity->getAuthId(), $searchStruct);

        $debtorInStocks = $this->inStockRepository
            ->fetchInStocksByAuthId($identity->getContextAuthId(), $searchStruct);

        return array_replace($debtorInStocks, $inStocks);
    }
}
