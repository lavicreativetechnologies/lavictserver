<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Version\ShopwareVersion;
use function array_key_exists;
use function get_object_vars;
use function property_exists;

class InStockEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var IdValue
     */
    public $authId;

    /**
     * @var IdValue
     */
    public $productId;

    /**
     * @var int
     */
    public $inStock;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->authId = IdValue::null();
        $this->productId = IdValue::null();
    }

    /**
     * @deprecated tag:v4.3.0 - Support for property "articlesDetailsId" will be removed
     */
    public function __get(string $property): ?IdValue
    {
        if ($property === 'articlesDetailsId') {
            return $this->productId;
        }

        return null;
    }

    /**
     * @deprecated tag:v4.3.0 - Support for property "articlesDetailsId" will be removed
     */
    public function __set(string $property, $value): void
    {
        if ($property === 'articlesDetailsId') {
            $this->productId = IdValue::create($value);
        }
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        $data = [
            'id' => $this->id->getStorageValue(),
            'auth_id' => $this->authId->getStorageValue(),
            'product_id' => $this->productId->getStorageValue(),
            'in_stock' => $this->inStock,
        ];

        if (ShopwareVersion::isPlatform()) {
            $data['product_version_id'] = IdValue::create(\Shopware\Core\Defaults::LIVE_VERSION)->getStorageValue();
        }

        return $data;
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->inStock = (int) $data['in_stock'];
        $this->id = IdValue::create($data['id']);
        $this->authId = IdValue::create($data['auth_id']);
        $this->productId = IdValue::create($data['product_id']);

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        if (array_key_exists('articlesDetailsId', $data)) {
            $this->productId = $data['articlesDetailsId'];
        }

        $this->id = IdValue::create($this->id);
        $this->authId = IdValue::create($this->authId);
        $this->productId = IdValue::create($this->productId);
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        $vars['articlesDetailsId'] = $this->productId->getValue();

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
