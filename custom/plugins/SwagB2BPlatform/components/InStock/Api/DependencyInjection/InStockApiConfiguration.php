<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Api\DependencyInjection;

use Shopware\B2B\Common\Controller\DependencyInjection\ControllerConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\RestApi\DependencyInjection\RestApiConfiguration;
use Shopware\B2B\InStock\Framework\DependencyInjection\InStockFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class InStockApiConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/api-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new ControllerConfiguration(),
            new RestApiConfiguration(),
            new InStockFrameworkConfiguration(),
        ];
    }
}
