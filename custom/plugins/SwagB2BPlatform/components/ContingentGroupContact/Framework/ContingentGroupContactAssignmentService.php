<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroupContact\Framework;

use Shopware\B2B\Common\IdValue;

/**
 * Assigns contingents to contacts M:N
 */
class ContingentGroupContactAssignmentService
{
    /**
     * @var ContingentContactRepository
     */
    private $contingentContactRepository;

    public function __construct(ContingentContactRepository $contingentContactRepository)
    {
        $this->contingentContactRepository = $contingentContactRepository;
    }

    /**
     * @throws MismatchingDataException
     */
    public function assign(IdValue $contingentGroupId, IdValue $contactId): void
    {
        if ($this->contingentContactRepository->isContingentGroupContactDebtor($contingentGroupId, $contactId)) {
            throw new MismatchingDataException();
        }

        $this->contingentContactRepository->assignContingentContact($contingentGroupId, $contactId);
    }

    /**
     * @throws \Shopware\B2B\ContingentGroupContact\Framework\MismatchingDataException
     */
    public function removeAssignment(IdValue $contingentGroupId, IdValue $contactId): void
    {
        $this->contingentContactRepository
            ->removeContingentContactAssignment($contingentGroupId, $contactId);
    }
}
