<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroupContact\Framework;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;

/**
 * DB-Representation of contingent:contact assignment
 */
class ContingentContactRepository implements GridRepository
{
    const TABLE_NAME = 'b2b_contact_contingent_group';

    const TABLE_ALIAS = 'contact_contingent_groups';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'name',
            'description',
        ];
    }

    public function removeContingentContactAssignment(IdValue $contingentGroupId, IdValue $contactId): void
    {
        $this->connection->delete(
            self::TABLE_NAME,
            [
                'contingent_group_id' => $contingentGroupId->getStorageValue(),
                'contact_id' => $contactId->getStorageValue(),
            ]
        );
    }

    public function assignContingentContact(IdValue $contingentGroupId, IdValue $contactId): void
    {
        $data = [
            'contingent_group_id' => $contingentGroupId->getStorageValue(),
            'contact_id' => $contactId->getStorageValue(),
        ];

        $this->connection->insert(
            self::TABLE_NAME,
            $data
        );
    }

    public function getActiveContingentsByContactId(IdValue $contactId): array
    {
        $contingentGroups = $this->connection->fetchAll(
            'SELECT contingent_group_id FROM ' . self::TABLE_NAME . '
             WHERE contact_id = :contactId
            ',
            [
                ':contactId' => $contactId->getStorageValue(),
            ]
        );

        return (array) $contingentGroups;
    }

    public function isContingentGroupContactDebtor(IdValue $contingentGroupId, IdValue $contactId): bool
    {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where('contact_id = :contactId')
            ->andWhere('contingent_group_id = :groupId')

            ->setParameter('contactId', $contactId->getStorageValue())
            ->setParameter('groupId', $contingentGroupId->getStorageValue())
            ->execute();

        return (bool) $query->fetch(PDO::FETCH_COLUMN);
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }
}
