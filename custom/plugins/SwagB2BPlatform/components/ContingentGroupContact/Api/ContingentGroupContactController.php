<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroupContact\Api;

use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\ContingentGroup\Framework\ContingentGroupRepository;
use Shopware\B2B\ContingentGroupContact\Framework\ContingentGroupContactAssignmentService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use function count;

class ContingentGroupContactController
{
    /**
     * @var ContingentGroupContactAssignmentService
     */
    private $contingentGroupContactAssignmentService;

    /**
     * @var ContingentGroupRepository
     */
    private $contingentGroupRepository;

    /**
     * @var AuthenticationIdentityLoaderInterface
     */
    private $contactRepository;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    public function __construct(
        ContingentGroupContactAssignmentService $contingentGroupContactAssignmentService,
        ContingentGroupRepository $contingentGroupRepository,
        AuthenticationIdentityLoaderInterface $contactRepository,
        LoginContextService $loginContextService
    ) {
        $this->contingentGroupContactAssignmentService = $contingentGroupContactAssignmentService;
        $this->contingentGroupRepository = $contingentGroupRepository;
        $this->contactRepository = $contactRepository;
        $this->loginContextService = $loginContextService;
    }

    public function getListAction(string $email): array
    {
        $contactIdentity = $this->contactRepository
            ->fetchIdentityByEmail($email, $this->loginContextService)
            ->getOwnershipContext();

        $contingentGroupContacts = $this->contingentGroupRepository
            ->fetchContingentGroupIdsForContact($contactIdentity->identityId);

        $totalCount = count($contingentGroupContacts);

        return ['success' => true, 'contingentGroupContacts' => $contingentGroupContacts, 'totalCount' => $totalCount];
    }

    public function createAction(string $email, Request $request): array
    {
        $contactIdentity = $this->contactRepository
            ->fetchIdentityByEmail($email, $this->loginContextService)
            ->getOwnershipContext();

        $this->contingentGroupContactAssignmentService
            ->assign($request->getIdValue('contingentGroupId'), $contactIdentity->identityId);

        return ['success' => true];
    }

    public function removeAction(string $email, Request $request): array
    {
        $contactIdentity = $this->contactRepository
            ->fetchIdentityByEmail($email, $this->loginContextService)
            ->getOwnershipContext();

        $this->contingentGroupContactAssignmentService
            ->removeAssignment($request->getIdValue('contingentGroupId'), $contactIdentity->identityId);

        return ['success' => true];
    }
}
