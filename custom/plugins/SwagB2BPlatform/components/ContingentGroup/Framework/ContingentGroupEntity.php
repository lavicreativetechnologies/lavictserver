<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use function get_object_vars;
use function property_exists;

class ContingentGroupEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * foreign key to s_user.id
     *
     * @var IdValue
     */
    public $contextOwnerId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->contextOwnerId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'context_owner_id' => $this->contextOwnerId->getStorageValue(),
            'name' => $this->name,
            'description' => $this->description,
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->contextOwnerId = IdValue::create($data['context_owner_id']);
        $this->name = $data['name'];
        $this->description = $data['description'];

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->contextOwnerId = IdValue::create($this->contextOwnerId);
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
