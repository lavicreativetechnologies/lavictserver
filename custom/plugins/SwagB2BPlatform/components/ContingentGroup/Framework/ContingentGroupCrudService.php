<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Framework;

use Shopware\B2B\Acl\Framework\AclAccessWriterInterface;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class ContingentGroupCrudService extends AbstractCrudService
{
    /**
     * @var ContingentGroupRepository
     */
    private $contingentGroupRepository;

    /**
     * @var ContingentGroupValidationService
     */
    private $groupValidationService;

    /**
     * @var AclAccessWriterInterface
     */
    private $aclAccessWriter;

    public function __construct(
        ContingentGroupRepository $contingentGroupRepository,
        ContingentGroupValidationService $groupValidationService,
        AclAccessWriterInterface $aclAccessWriter
    ) {
        $this->contingentGroupRepository = $contingentGroupRepository;
        $this->groupValidationService = $groupValidationService;
        $this->aclAccessWriter = $aclAccessWriter;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'name',
                'description',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'name',
                'description',
            ]
        );
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException
     */
    public function create(
        CrudServiceRequest $request,
        OwnershipContext $ownershipContext,
        AclGrantContext $grantContext
    ): ContingentGroupEntity {
        $data = $request->getFilteredData();
        $data['contextOwnerId'] = $ownershipContext->contextOwnerId;

        $contingent = new ContingentGroupEntity();

        $contingent->setData($data);

        $validation = $this->groupValidationService
            ->createInsertValidation($contingent);

        $this->testValidation($contingent, $validation);

        $contingentGroup = $this->contingentGroupRepository
            ->addContingentGroup($contingent, $ownershipContext);

        $this->aclAccessWriter->addNewSubject(
            $ownershipContext,
            $grantContext,
            $contingentGroup->id,
            true
        );

        return $contingentGroup;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException
     */
    public function update(CrudServiceRequest $request, OwnershipContext $ownershipContext): ContingentGroupEntity
    {
        $data = $request->getFilteredData();
        $contingentGroup = new ContingentGroupEntity();
        $contingentGroup->setData($data);
        $contingentGroup->contextOwnerId = $ownershipContext->contextOwnerId;

        $validation = $this->groupValidationService
            ->createUpdateValidation($contingentGroup);

        $this->testValidation($contingentGroup, $validation);

        $this->aclAccessWriter->testUpdateAllowed($ownershipContext, $contingentGroup->id);

        $this->contingentGroupRepository
            ->updateContingentGroup($contingentGroup, $ownershipContext);

        return $contingentGroup;
    }

    public function remove(IdValue $id, OwnershipContext $ownershipContext): ContingentGroupEntity
    {
        $this->aclAccessWriter->testUpdateAllowed($ownershipContext, $id);

        $contingentGroup = $this->contingentGroupRepository->fetchOneById($id, $ownershipContext);

        $this->contingentGroupRepository
            ->removeContingentGroup($contingentGroup, $ownershipContext);

        return $contingentGroup;
    }
}
