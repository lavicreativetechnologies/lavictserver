<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;

class ContingentGroupAssignmentEntity extends ContingentGroupEntity
{
    /**
     * @var IdValue
     */
    public $assignmentId;

    public function __construct()
    {
        parent::__construct();
        $this->assignmentId = IdValue::null();
    }

    public function fromDatabaseArray(array $roleData): CrudEntity
    {
        parent::fromDatabaseArray($roleData);

        $this->assignmentId = IdValue::create($roleData['assignmentId']);

        return $this;
    }
}
