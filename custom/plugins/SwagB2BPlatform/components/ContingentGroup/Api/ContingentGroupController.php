<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Api;

use InvalidArgumentException;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Acl\Framework\AclGrantContextProviderChain;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\ContingentGroup\Framework\ContingentGroupCrudService;
use Shopware\B2B\ContingentGroup\Framework\ContingentGroupRepository;
use Shopware\B2B\ContingentGroup\Framework\ContingentGroupSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class ContingentGroupController
{
    /**
     * @var ContingentGroupRepository
     */
    private $contingentGroupRepository;

    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var ContingentGroupCrudService
     */
    private $contingentGroupCrudService;

    /**
     * @var AclGrantContextProviderChain
     */
    private $grantContextProviderChain;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        ContingentGroupRepository $contingentGroupRepository,
        GridHelper $requestHelper,
        ContingentGroupCrudService $contingentGroupCrudService,
        AclGrantContextProviderChain $grantContextProviderChain,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->contingentGroupRepository = $contingentGroupRepository;
        $this->requestHelper = $requestHelper;
        $this->contingentGroupCrudService = $contingentGroupCrudService;
        $this->grantContextProviderChain = $grantContextProviderChain;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(Request $request): array
    {
        $search = new ContingentGroupSearchStruct();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $context = $this->getDebtorOwnershipContext();

        $contingentGroups = $this->contingentGroupRepository
            ->fetchList($context, $search);

        $totalCount = $this->contingentGroupRepository
            ->fetchTotalCount($context, $search);

        return ['success' => true, 'contingentGroups' => $contingentGroups, 'totalCount' => $totalCount];
    }

    public function getAction(string $contingentGroupId): array
    {
        $contingentGroupId = IdValue::create($contingentGroupId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $contingentGroup = $this->contingentGroupRepository
            ->fetchOneById($contingentGroupId, $ownershipContext);

        return ['success' => true, 'contingentGroup' => $contingentGroup];
    }

    public function createAction(Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();

        $aclGrantContext = $this->extractGrantContext($request, $ownershipContext);

        $data = [
            'name' => $request->getParam('name'),
            'description' => $request->getParam('description'),
        ];

        $newRecord = $this->contingentGroupCrudService
            ->createNewRecordRequest($data);

        $contingentGroup = $this->contingentGroupCrudService
            ->create($newRecord, $ownershipContext, $aclGrantContext);

        return ['success' => true, 'contingentGroup' => $contingentGroup];
    }

    public function updateAction(string $contingentGroupId, Request $request): array
    {
        $contingentGroupId = IdValue::create($contingentGroupId);
        $context = $this->getDebtorOwnershipContext();

        $data = [
            'id' => $contingentGroupId,
            'name' => $request->getParam('name'),
            'description' => $request->getParam('description'),
        ];

        $existingRecord = $this->contingentGroupCrudService
            ->createExistingRecordRequest($data);

        $contingentGroup = $this->contingentGroupCrudService
            ->update($existingRecord, $context);

        return ['success' => true, 'contingentGroup' => $contingentGroup];
    }

    public function removeAction(string $contingentGroupId, Request $request): array
    {
        $contingentGroupId = IdValue::create($contingentGroupId);
        $context = $this->getDebtorOwnershipContext();

        $contingentGroup = $this->contingentGroupCrudService
            ->remove($contingentGroupId, $context);

        return ['success' => true, 'contingentGroup' => $contingentGroup];
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function extractGrantContext(Request $request, OwnershipContext $ownershipContext): AclGrantContext
    {
        $grantContextIdentifier = $request->requireParam('grantContextIdentifier');

        return $this->grantContextProviderChain->fetchOneByIdentifier($grantContextIdentifier, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
