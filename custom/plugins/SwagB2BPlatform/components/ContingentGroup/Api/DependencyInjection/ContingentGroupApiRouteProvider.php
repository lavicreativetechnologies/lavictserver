<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContingentGroupApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingentgroup',
                'b2b_contingent_group.api_contingent_group_controller',
                'getList',
                [],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingentgroup/{contingentGroupId}',
                'b2b_contingent_group.api_contingent_group_controller',
                'get',
                ['contingentGroupId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contingentgroup',
                'b2b_contingent_group.api_contingent_group_controller',
                'create',
                [],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contingentgroup/{contingentGroupId}',
                'b2b_contingent_group.api_contingent_group_controller',
                'update',
                ['contingentGroupId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contingentgroup/{contingentGroupId}',
                'b2b_contingent_group.api_contingent_group_controller',
                'remove',
                ['contingentGroupId'],
            ],
        ];
    }
}
