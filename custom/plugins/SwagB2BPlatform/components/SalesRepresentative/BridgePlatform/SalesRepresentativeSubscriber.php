<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeIdentity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Framework\Routing\KernelListenerPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use function end;
use function explode;
use function in_array;

class SalesRepresentativeSubscriber implements EventSubscriberInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var array
     */
    protected static $whitelist = [
        'AuthController' => ['logout'],
        'b2bsalesrepresentative' => ['__all__'],
        'CheckoutController' => ['info'],
        'ajax_search' => ['index'],
        'b2baccount' => ['__all__'],
        'error_controller' => ['__all__'],
        'CookieController' => ['__all__'],
    ];

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => ['redirectSalesRepresentative', KernelListenerPriorities::KERNEL_CONTROLLER_EVENT_SCOPE_VALIDATE],
        ];
    }

    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    public function redirectSalesRepresentative(ControllerEvent $event): void
    {
        $requestArguments = $this->extractRequestArgs($event);
        $requestedController = $requestArguments['requestedController'];
        $requestedAction = $requestArguments['requestedAction'];

        if (!$this->authenticationService->is(SalesRepresentativeIdentity::class)) {
            if ($requestedController !== 'b2bsalesrepresentative' || $requestedAction === 'salesRepresentativeLogin') {
                return;
            }

            throw new B2bControllerRedirectException('index', 'b2bdashboard');
        }

        if (isset(static::$whitelist[$requestedController])
            && (in_array($requestedAction, static::$whitelist[$requestedController], true)
                || in_array('__all__', static::$whitelist[$requestedController], true))
        ) {
            return;
        }

        throw new B2bControllerRedirectException('index', 'b2bsalesrepresentative');
    }

    /**
     * @internal
     */
    protected function extractRequestArgs(ControllerEvent $args): array
    {
        $attributes = $args->getRequest()->attributes;

        if ($attributes->has('_b2b')) {
            return [
                'requestedController' => $attributes->get('_b2b_controller_route_name'),
                'requestedAction' => $attributes->get('_b2b_controller_action'),
            ];
        }

        $controllerRoute = $attributes->get('_controller');
        $explodedRoute = explode('::', $controllerRoute);

        $explodedControllerPath = explode('\\', $explodedRoute[0]);

        return [
            'requestedController' => end($explodedControllerPath),
            'requestedAction' => $explodedRoute[1] ?? 'index',
        ];
    }
}
