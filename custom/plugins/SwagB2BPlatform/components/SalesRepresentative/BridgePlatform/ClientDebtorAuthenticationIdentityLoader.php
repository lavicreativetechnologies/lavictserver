<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Debtor\BridgePlatform\DebtorAuthenticationIdentityLoader;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\SalesRepresentative\Framework\ClientIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;

class ClientDebtorAuthenticationIdentityLoader extends DebtorAuthenticationIdentityLoader implements ClientIdentityLoaderInterface
{
    private const ALIAS = 'debtor';

    public function __construct(DebtorRepositoryInterface $debtorRepository)
    {
        parent::__construct($debtorRepository);
    }

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function addClientData(QueryBuilder $query): void
    {
        $query->leftJoin(
            StoreFrontAuthenticationRepository::TABLE_ALIAS,
            '(
                SELECT
                    customer.id,
                    customer.first_name as firstname,
                    customer.last_name as lastname,
                    customer.salutation_id as salutation,
                    customer.email,
                    customer.active,
                    address.`phone_number` as phone,
                    address.`company`,
                    "debtor" as `type`,
                    customer.email as `shopOwnerEmail`
                FROM customer
                INNER JOIN customer_address address
                    ON customer.default_billing_address_id = address.id
            )',
            self::ALIAS,
            StoreFrontAuthenticationRepository::TABLE_ALIAS . '.provider_context = hex(debtor.id) AND '
                . StoreFrontAuthenticationRepository::TABLE_ALIAS . '.provider_key = :debtorProviderKey'
        )
            ->setParameter('debtorProviderKey', DebtorRepositoryInterface::class);
    }
}
