<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use function sprintf;

class SalesRepresentativeRepository implements GridRepository, SalesRepresentativeRepositoryInterface
{
    const TABLE_NAME = 'customer';

    const TABLE_ALIAS = 'customer';

    /**
     * @var EntityRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SalesRepresentativeEntityFactory
     */
    private $salesRepresentativeEntityFactory;

    public function __construct(
        EntityRepositoryInterface $customerRepository,
        ContextProvider $contextProvider,
        SalesRepresentativeEntityFactory $salesRepresentativeEntityFactory
    ) {
        $this->contextProvider = $contextProvider;
        $this->customerRepository = $customerRepository;
        $this->salesRepresentativeEntityFactory = $salesRepresentativeEntityFactory;
    }

    public function fetchOneById(IdValue $id): SalesRepresentativeEntity
    {
        $salesRep = $this->customerRepository->search(
            (new Criteria([$id->getValue()]))
                ->addFilter(new EqualsFilter('customer.b2bCustomerData.isSalesRepresentative', true))
                ->addAssociation('salutation')
                ->setLimit(1),
            $this->contextProvider->getContext()
        )->first();

        if (!$salesRep) {
            throw new NotFoundException(sprintf('Sales representative not found for %s', $id->getValue()));
        }

        return $this->salesRepresentativeEntityFactory->create($salesRep);
    }

    public function fetchOneByEmail(string $email): SalesRepresentativeEntity
    {
        $salesRep = $this->customerRepository->search(
            (new Criteria())
            ->addFilter(new EqualsFilter('email', $email))
            ->addFilter(new EqualsFilter('customer.b2bCustomerData.isSalesRepresentative', true))
            ->addAssociation('salutation')
            ->setLimit(1),
            $this->contextProvider->getContext()
        )->first();

        if (!$salesRep) {
            throw new NotFoundException(sprintf('Sales representative not found for %s', $email));
        }

        return $this->salesRepresentativeEntityFactory->create($salesRep);
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [
            '%2$s' => [
                'contact' => [
                    'phone',
                    'email',
                    'firstname',
                    'lastname',
                    'shopOwnerEmail',
                    'type',
                ],
                'debtor' => [
                    'phone',
                    'email',
                    'firstname',
                    'lastname',
                    'shopOwnerEmail',
                    'type',
                ],
            ],
        ];
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
