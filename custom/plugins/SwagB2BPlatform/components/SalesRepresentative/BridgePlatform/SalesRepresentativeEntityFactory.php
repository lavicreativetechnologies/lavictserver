<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;

class SalesRepresentativeEntityFactory
{
    private const PASSWORD_DEFAULT_ENCODER = 'bcrypt';

    public function create(CustomerEntity $customerEntity): SalesRepresentativeEntity
    {
        $salesRep = new SalesRepresentativeEntity();
        $salesRep->id = IdValue::create($customerEntity->getId());
        $salesRep->password = $customerEntity->getPassword() ?? $customerEntity->getLegacyPassword();
        $salesRep->encoder = $customerEntity->getLegacyEncoder() ?? self::PASSWORD_DEFAULT_ENCODER;
        $salesRep->email = $customerEntity->getEmail();
        $salesRep->active = $customerEntity->getActive();
        $salesRep->paymentID = IdValue::create($customerEntity->getDefaultPaymentMethodId());
        $salesRep->firstlogin = $customerEntity->getFirstLogin();
        $salesRep->lastlogin = $customerEntity->getLastLogin();
        $salesRep->newsletter = $customerEntity->getNewsletter();
        $salesRep->customergroup = IdValue::create($customerEntity->getGroupId());
        /*
         * @deprecated tag:v4.4.0 please use $salesRep->language instead
         */
        $salesRep->languageId = IdValue::create($customerEntity->getLanguageId());
        $salesRep->language = IdValue::create($customerEntity->getLanguageId());
        $salesRep->default_billing_address_id = IdValue::create($customerEntity->getDefaultBillingAddressId());
        $salesRep->default_shipping_address_id = IdValue::create($customerEntity->getDefaultShippingAddressId());
        $salesRep->title = $customerEntity->getTitle();
        $salesRep->salutation = $customerEntity->getSalutation()->getDisplayName();

        /*
         * @deprecated tag:v4.4.0 will be removed without replacement
         */
        $salesRep->salutationId = IdValue::create($customerEntity->getSalutation()->getUniqueIdentifier());
        $salesRep->firstName = $customerEntity->getFirstName();
        $salesRep->lastName = $customerEntity->getLastName();
        $salesRep->birthday = $customerEntity->getBirthday();
        $salesRep->customernumber = $customerEntity->getCustomerNumber();

        return $salesRep;
    }
}
