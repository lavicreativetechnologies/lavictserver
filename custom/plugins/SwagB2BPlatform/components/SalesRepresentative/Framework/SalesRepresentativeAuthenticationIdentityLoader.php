<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationEntity;

class SalesRepresentativeAuthenticationIdentityLoader implements AuthenticationIdentityLoaderInterface
{
    /**
     * @var SalesRepresentativeRepositoryInterface
     */
    private $salesRepresentativeRepository;

    public function __construct(
        SalesRepresentativeRepositoryInterface $salesRepresentativeRepository
    ) {
        $this->salesRepresentativeRepository = $salesRepresentativeRepository;
    }

    public function fetchIdentityByEmail(string $email, LoginContextService $contextService, bool $isApi = false): Identity
    {
        $entity = $this->salesRepresentativeRepository->fetchOneByEmail($email);

        return $this->getIdentity($entity, $contextService);
    }

    public function fetchIdentityByAuthentication(StoreFrontAuthenticationEntity $authentication, LoginContextService $contextService, bool $isApi = false): Identity
    {
        if ($authentication->providerKey !== SalesRepresentativeRepositoryInterface::class) {
            throw new NotFoundException('The given authentication, does not belong to this loader');
        }

        $entity = $this->salesRepresentativeRepository
            ->fetchOneById($authentication->providerContext);

        return $this->getIdentity($entity, $contextService);
    }

    /**
     * @internal
     */
    protected function getIdentity(SalesRepresentativeEntity $entity, LoginContextService $contextService): SalesRepresentativeIdentity
    {
        $authId = $contextService->getAuthId(SalesRepresentativeRepositoryInterface::class, $entity->id);

        return new SalesRepresentativeIdentity(
            $authId,
            $entity->id,
            $this->salesRepresentativeRepository->getTableName(),
            $entity,
            $contextService->getAvatar($authId)
        );
    }

    public function fetchIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity, LoginContextService $contextService, bool $isApi = false): Identity
    {
        if (!$credentialsEntity->email) {
            throw new NotFoundException('Unable to handle context');
        }

        return $this->fetchIdentityByEmail($credentialsEntity->email, $contextService);
    }
}
