<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;

class SalesRepresentativeDebtorIdentity extends DebtorIdentity implements SalesRepresentativeIdentityInterface
{
    /**
     * @var IdValue
     */
    private $salesRepresentativeId;

    /**
     * @var IdValue
     */
    private $mediaId;

    public function __construct(
        IdValue $salesRepresentativeId,
        IdValue $authId,
        IdValue $id,
        string $tableName,
        DebtorEntity $entity,
        string $avatar = '',
        IdValue $mediaId = null,
        bool $isApi = false
    ) {
        parent::__construct($authId, $id, $tableName, $entity, $avatar, $isApi);
        $this->salesRepresentativeId = $salesRepresentativeId;
        $this->mediaId = $mediaId;
    }

    public function getSalesRepresentativeId(): IdValue
    {
        return $this->salesRepresentativeId;
    }

    /**
     * @return IdValue|null
     */
    public function getMediaId()
    {
        return $this->mediaId;
    }
}
