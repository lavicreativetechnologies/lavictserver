<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationEntity;
use function array_fill;
use function array_key_exists;
use function array_keys;
use function array_map;
use function count;
use function implode;
use function sprintf;

class ClientIdentityChainLoader implements ClientIdentityLoaderInterface
{
    const CLIENT_COLUMN_PREFIX = 'clientData_';

    private $fieldNames = [
        'id',
        'firstname',
        'lastname',
        'salutation',
        'phone',
        'email',
        'active',
        'company',
        'type',
        'shopOwnerEmail',
    ];

    /**
     * @var ClientIdentityLoaderInterface[]
     */
    private $clientRepositories;

    /**
     * @param ClientIdentityLoaderInterface[] $clientRepositories
     */
    public function __construct(array $clientRepositories)
    {
        $this->setRepositories($clientRepositories);
    }

    /**
     * @param ClientIdentityLoaderInterface[] $clientRepositories
     */
    public function setRepositories(array $clientRepositories): void
    {
        $this->clientRepositories = [];

        foreach ($clientRepositories as $repository) {
            $this->addRepository($repository);
        }
    }

    public function addRepository(ClientIdentityLoaderInterface $repository): void
    {
        $this->clientRepositories[] = $repository;
    }

    public function fetchIdentityByEmail(string $email, LoginContextService $contextService, bool $isApi = false): Identity
    {
        foreach ($this->clientRepositories as $repository) {
            try {
                return $repository->fetchIdentityByEmail($email, $contextService);
            } catch (NotFoundException $e) {
                continue;
            }
        }

        throw new NotFoundException(sprintf('No identity found with email %s', $email));
    }

    public function fetchIdentityByAuthentication(StoreFrontAuthenticationEntity $authentication, LoginContextService $contextService, bool $isApi = false): Identity
    {
        foreach ($this->clientRepositories as $repository) {
            try {
                return $repository->fetchIdentityByAuthentication($authentication, $contextService);
            } catch (NotFoundException $e) {
                continue;
            }
        }

        throw new NotFoundException(sprintf('No identity found from %s and id %s', $authentication->providerKey, $authentication->providerContext->getValue()));
    }

    public function fetchIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity, LoginContextService $contextService, bool $isApi = false): Identity
    {
        foreach ($this->clientRepositories as $repository) {
            try {
                return $repository->fetchIdentityByCredentials($credentialsEntity, $contextService);
            } catch (NotFoundException $e) {
                continue;
            }
        }

        throw new NotFoundException('No identity found with the credentials');
    }

    public function addClientData(QueryBuilder $query): void
    {
        foreach ($this->clientRepositories as $repository) {
            $repository->addClientData($query);
        }

        $query->addSelect(
            implode(',', $this->getAdditionalSelect())
        );
    }

    /**
     * @internal
     */
    protected function getAdditionalSelect(): array
    {
        $additionalSelect = [];
        $countIdentities = 0;
        foreach ($this->clientRepositories as $repository) {
            foreach ($this->fieldNames as $fieldName) {
                if (!array_key_exists($fieldName, $additionalSelect)) {
                    $additionalSelect[$fieldName] = '';
                }
                $additionalSelect[$fieldName] .= 'IFNULL(' . $repository->getAlias() . '.' . $fieldName . ',';
            }
            ++$countIdentities;
        }

        return $this->formatAdditionalSelect($additionalSelect, $countIdentities);
    }

    protected function formatAdditionalSelect(array $additionalSelect, int $countIdentities): array
    {
        return array_map(
            function (string $query, string $brackets, string $fieldName) {
                return $query . 'NULL' . $brackets . ' as ' . self::CLIENT_COLUMN_PREFIX . $fieldName;
            },
            $additionalSelect,
            array_fill(0, count($additionalSelect), implode('', array_fill(0, $countIdentities, ')'))),
            array_keys($additionalSelect)
        );
    }

    public function getAlias(): string
    {
        throw new Exception('Not implemented');
    }
}
