<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactIdentity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;

class SalesRepresentativeContactIdentity extends ContactIdentity implements SalesRepresentativeIdentityInterface
{
    /**
     * @var IdValue
     */
    private $salesRepresentativeId;

    /**
     * @var IdValue
     */
    private $mediaId;

    /**
     * @param int $mediaId
     */
    public function __construct(
        IdValue $salesRepresentativeId,
        IdValue $authId,
        IdValue $id,
        string $tableName,
        ContactEntity $entity,
        DebtorIdentity $identity,
        string $avatar = '',
        IdValue $mediaId = null
    ) {
        parent::__construct($authId, $id, $tableName, $entity, $identity, $avatar);
        $this->salesRepresentativeId = $salesRepresentativeId;
        $this->mediaId = $mediaId;
    }

    public function getSalesRepresentativeId(): IdValue
    {
        return $this->salesRepresentativeId;
    }

    /**
     * @return IdValue|null
     */
    public function getMediaId()
    {
        return $this->mediaId;
    }
}
