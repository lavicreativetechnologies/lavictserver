<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Api;

use InvalidArgumentException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\IntIdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientEntity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeRepositoryInterface;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeSearchStruct;
use function array_diff;
use function array_map;

class ApiSalesRepresentativeController
{
    /**
     * @var SalesRepresentativeClientRepository
     */
    private $clientRepository;

    /**
     * @var SalesRepresentativeRepositoryInterface
     */
    private $salesRepresentativeRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    public function __construct(
        SalesRepresentativeClientRepository $clientRepository,
        SalesRepresentativeRepositoryInterface $salesRepresentativeRepository,
        GridHelper $gridHelper
    ) {
        $this->clientRepository = $clientRepository;
        $this->salesRepresentativeRepository = $salesRepresentativeRepository;
        $this->gridHelper = $gridHelper;
    }

    public function getListAction(string $salesRepresentativeId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);
        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);
        $searchStruct = new SalesRepresentativeSearchStruct();
        $this->gridHelper
            ->extractSearchDataInRestApi($request, $searchStruct);

        $clients = $this->clientRepository->fetchClientList($searchStruct, $salesRepEntity->id);

        $totalCount = $this->clientRepository->fetchTotalCount($searchStruct, $salesRepEntity->id);

        return [
            'success' => true,
            'clients' => $clients,
            'limit' => $searchStruct->limit,
            'offset' => $searchStruct->offset,
            'totalCount' => $totalCount,
        ];
    }

    public function getAction(string $salesRepresentativeId, string $clientId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);
        $clientId = IdValue::create($clientId);

        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);

        $client = $this->clientRepository->fetchOneById($salesRepEntity, $clientId);

        return [
            'success' => true,
            'client' => $client,
        ];
    }

    public function deleteClientAction(string $salesRepresentativeId, string $clientId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);
        $clientId = IdValue::create($clientId);

        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);
        $client = $this->clientRepository->fetchOneById($salesRepEntity, $clientId);
        $this->clientRepository->deleteSalesRepresentativeClient($salesRepEntity, $client);

        return [
            'success' => true,
            'client' => $client,
        ];
    }

    public function deleteClientsAction(string $salesRepresentativeId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);

        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);
        $this->clientRepository->deleteClientsBySalesRepresentativeId($salesRepEntity->id);

        return [
            'success' => true,
        ];
    }

    public function assignClientsAction(string $salesRepresentativeId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);

        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);
        $clientAuthIds = $request->getParam('clientAuthIds');

        $searchStruct = new SalesRepresentativeSearchStruct();

        $alreadyAssignedClients = $this->clientRepository->fetchClientList($searchStruct, $salesRepEntity->id);
        $alreadyAssignedClients = array_map(
            function (SalesRepresentativeClientEntity $client) {
                return $client->authId;
            },
            $alreadyAssignedClients
        );
        $clientAuthIds = array_diff($clientAuthIds, $alreadyAssignedClients);
        $clientAuthIds = IdValue::createMultiple($clientAuthIds);

        $this->clientRepository->addClientsToSalesRepresentative($clientAuthIds, $salesRepEntity->id);

        return $this->getListAction((string) $salesRepresentativeId->getValue(), $request);
    }

    public function assignClientAction(string $salesRepresentativeId, string $clientId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);
        $clientId = IdValue::create($clientId);

        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);

        $this->clientRepository->addClientsToSalesRepresentative([$clientId], $salesRepEntity->id);

        return $this->getAction((string) $salesRepresentativeId->getValue(), (string) $clientId->getValue(), $request);
    }

    public function reAssignClientsAction(string $salesRepresentativeId, Request $request): array
    {
        $salesRepresentativeId = $this->createSalesRepresentativeIdValue($salesRepresentativeId);

        $salesRepEntity = $this->salesRepresentativeRepository->fetchOneById($salesRepresentativeId);
        $clientAuthIds = IdValue::createMultiple($request->getParam('clientAuthIds'));
        $this->clientRepository->deleteClientsBySalesRepresentativeId($salesRepEntity->id);
        $this->clientRepository->addClientsToSalesRepresentative($clientAuthIds, $salesRepEntity->id);

        return $this->getListAction((string) $salesRepresentativeId->getValue(), $request);
    }

    /**
     * @internal
     */
    protected function createSalesRepresentativeIdValue(string $salesRepresentativeId): IdValue
    {
        $salesRepresentativeId = IdValue::create($salesRepresentativeId);

        if (ShopwareVersion::isPlatform() && $salesRepresentativeId instanceof IntIdValue) {
            throw new InvalidArgumentException('Invalid id format. Please use uuid format.');
        }

        return $salesRepresentativeId;
    }
}
