<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Frontend;

use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeDebtorIdentity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeIdentity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeSearchStruct;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeService;
use Shopware\B2B\Shop\Framework\SessionStorageInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class SalesRepresentativeController
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var SalesRepresentativeService
     */
    private $salesRepresentativeService;

    /**
     * @var SalesRepresentativeClientRepository
     */
    private $clientRepository;

    /**
     * @var SessionStorageInterface
     */
    private $sessionStorage;

    public function __construct(
        AuthenticationService $authenticationService,
        GridHelper $gridHelper,
        SalesRepresentativeClientRepository $clientRepository,
        SalesRepresentativeService $salesRepresentativeService,
        SessionStorageInterface $sessionStorage
    ) {
        $this->authenticationService = $authenticationService;
        $this->gridHelper = $gridHelper;
        $this->salesRepresentativeService = $salesRepresentativeService;
        $this->clientRepository = $clientRepository;
        $this->sessionStorage = $sessionStorage;
    }

    public function indexAction(): array
    {
        return $this->gridHelper->getValidationResponse('client');
    }

    public function gridAction(Request $request): array
    {
        $searchStruct = new SalesRepresentativeSearchStruct();

        $this->gridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        $identity = $this->authenticationService->getIdentity();

        /** @var SalesRepresentativeEntity $entity */
        $entity = $identity->getEntity();

        $clients = $this->clientRepository->fetchClientList($searchStruct, $entity->id);

        $clients = $this->salesRepresentativeService->setEntitiesToClients($clients);

        $totalCount = $this->clientRepository->fetchTotalCount($searchStruct, $entity->id);

        $maxPage = $this->gridHelper
            ->getMaxPage($totalCount);

        $currentPage = (int) $request->getParam('page', 1);

        $salesRepresentativeGridState = $this->gridHelper
            ->getGridState($request, $searchStruct, $clients, $maxPage, $currentPage);

        return [
            'salesRepresentativeGrid' => $salesRepresentativeGridState,
        ];
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function clientLoginAction(Request $request): void
    {
        $id = $request->requireIdValue('id');

        /** @var SalesRepresentativeIdentity $identity */
        $identity = $this->authenticationService->getIdentity();

        /** @var SalesRepresentativeEntity $entity */
        $entity = $identity->getEntity();

        if (!$this->clientRepository->isSalesRepresentativeClient($entity, $id)) {
            throw new B2bControllerRedirectException('index', 'b2bsalesrepresentative');
        }

        try {
            $this->salesRepresentativeService->loginByAuthId($identity, $id);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);

            throw new B2bControllerForwardException('index');
        }

        $clientIdentity = $this->authenticationService->getIdentity();

        $this->salesRepresentativeService->setClientIdentity($clientIdentity, $identity);

        $this->sessionStorage->set('salesRepresentativeId', $identity->getAuthId()->getValue());

        throw new B2bControllerRedirectException('index', 'b2bdashboard');
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function salesRepresentativeLoginAction(): void
    {
        /** @var SalesRepresentativeDebtorIdentity $identity */
        $identity = $this->authenticationService->getIdentity();

        $this->salesRepresentativeService->loginByAuthId($identity, $identity->getSalesRepresentativeId());

        throw new B2bControllerRedirectException('index', 'b2bsalesrepresentative');
    }
}
