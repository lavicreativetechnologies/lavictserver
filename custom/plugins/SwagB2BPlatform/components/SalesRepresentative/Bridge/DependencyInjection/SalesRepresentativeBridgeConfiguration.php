<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Contact\Framework\DependencyInjection\ContactFrameworkConfiguration;
use Shopware\B2B\Debtor\Framework\DependencyInjection\DebtorFrameworkConfiguration;
use Shopware\B2B\OrderSalesRepresentative\Bridge\DependencyInjection\OrderSalesRepresentativeBridgeConfiguration;
use Shopware\B2B\SalesRepresentative\BridgePlatform\DependencyInjection\SalesRepresentativeBridgeConfiguration as PlatformSalesRepresentativeBridgeConfiguration;
use Shopware\B2B\SalesRepresentative\Framework\DependencyInjection\SalesRepresentativeFrameworkConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SalesRepresentativeBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformSalesRepresentativeBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new ContactFrameworkConfiguration(),
            new DebtorFrameworkConfiguration(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
            new SalesRepresentativeFrameworkConfiguration(),
            OrderSalesRepresentativeBridgeConfiguration::create(),
        ];
    }
}
