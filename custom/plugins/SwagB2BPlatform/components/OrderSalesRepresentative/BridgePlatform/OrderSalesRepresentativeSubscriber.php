<?php declare(strict_types=1);

namespace Shopware\B2B\OrderSalesRepresentative\BridgePlatform;

use Shopware\B2B\Order\BridgePlatform\OrderContextCreatedEvent;
use Shopware\B2B\OrderSalesRepresentative\Framework\OrderSalesRepresentativeService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderSalesRepresentativeSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrderSalesRepresentativeService
     */
    private $orderSalesRepresentativeService;

    public function __construct(
        OrderSalesRepresentativeService $orderSalesRepresentativeService
    ) {
        $this->orderSalesRepresentativeService = $orderSalesRepresentativeService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            OrderContextCreatedEvent::class => 'subscribeToOrderContextCreated',
        ];
    }

    public function subscribeToOrderContextCreated(OrderContextCreatedEvent $event): void
    {
        $this->orderSalesRepresentativeService->addSalesRepresentativeToOrder($event->getOrderContext());
    }
}
