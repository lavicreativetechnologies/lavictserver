<?php declare(strict_types=1);

namespace Shopware\B2B\OrderSalesRepresentative\Frontend;

use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Order\Framework\OrderEntity;
use Shopware\B2B\Order\Frontend\OrderModifyViewInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class OrderSalesRepresentativeViewModifier implements OrderModifyViewInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function modifyMasterDataView(OrderEntity $order): array
    {
        if ($order->salesRepresentativeAuthId instanceof NullIdValue) {
            return [];
        }

        return ['salesRepresentative' => $this->authenticationService
            ->getIdentityByAuthId($order->salesRepresentativeAuthId)
            ->getEntity(),
        ];
    }
}
