<?php declare(strict_types=1);

namespace Shopware\B2B\OrderSalesRepresentative\Bridge\DependencyInjection;

use Shopware\B2B\Cart\Bridge\DependencyInjection\CartBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Order\Framework\DependencyInjection\OrderFrameworkConfiguration;
use Shopware\B2B\OrderSalesRepresentative\BridgePlatform\DependencyInjection\OrderSalesRepresentativeBridgeConfiguration as PlatformOrderSalesRepresentativeBridgeConfiguration;
use Shopware\B2B\SalesRepresentative\Framework\DependencyInjection\SalesRepresentativeFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderSalesRepresentativeBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformOrderSalesRepresentativeBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            CartBridgeConfiguration::create(),
            new OrderFrameworkConfiguration(),
            new SalesRepresentativeFrameworkConfiguration(),
        ];
    }
}
