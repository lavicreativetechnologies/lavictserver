<?php declare(strict_types=1);

namespace Shopware\B2B\OrderSalesRepresentative\Framework\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\OrderSalesRepresentative\Bridge\DependencyInjection\OrderSalesRepresentativeBridgeConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderSalesRepresentativeFrameworkConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/framework-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getDependingConfigurations(): array
    {
        return [
            OrderSalesRepresentativeBridgeConfiguration::create(),
        ];
    }
}
