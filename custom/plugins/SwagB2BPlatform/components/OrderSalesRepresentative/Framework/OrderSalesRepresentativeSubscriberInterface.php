<?php declare(strict_types=1);

namespace Shopware\B2B\OrderSalesRepresentative\Framework;

use Shopware\B2B\Order\Framework\OrderContext;

/**
 * @deprecated tag:v4.3.0 use \Shopware\B2B\OrderSalesRepresentative\Framework\OrderSalesRepresentativeService instead
 */
interface OrderSalesRepresentativeSubscriberInterface
{
    /**
     * @deprecated tag:v4.3.0 use \Shopware\B2B\OrderSalesRepresentative\Framework\OrderSalesRepresentativeService::addSalesRepresentativeToOrder instead
     */
    public function addSalesRepresentativeToOrder(OrderContext $orderContext);
}
