<?php declare(strict_types=1);

namespace Shopware\B2B\Statistic\Framework;

use DateTime;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\Filter\DateRangeFilter;
use Shopware\B2B\Common\Filter\EqualsFilter;
use Shopware\B2B\Common\Filter\Filter;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * @deprecated tag:v4.2.0 removed in favor of framework implementation
 */
interface StatisticRepositoryInterface extends GridRepository
{
    /**
     * @return StatisticAggregate[]
     */
    public function fetchGroupedList(OwnershipContext $ownershipContext, StatisticSearchStruct $searchStruct): array;

    /**
     * @return Statistic[]
     */
    public function fetchList(OwnershipContext $ownershipContext, StatisticSearchStruct $searchStruct): array;

    public function fetchTotalCount(OwnershipContext $ownershipContext, StatisticSearchStruct $searchStruct): int;

    /**
     * @return ContactEntity[]
     */
    public function fetchStatisticContactList(OwnershipContext $context): array;

    /**
     * @return RoleEntity[]
     */
    public function fetchStatisticRoleList(OwnershipContext $context): array;

    public function fetchStatisticStatesList(OwnershipContext $context): array;

    public function createDateRangeFilter(DateTime $from, DateTime $to): DateRangeFilter;

    public function createEqualsAuthorFilter(IdValue $authId): EqualsFilter;

    /**
     * @deprecated tag:v4.2.0 no longer used, use createEqualsStateNameFilter instead
     */
    public function createEqualsStatesFilter(IdValue $stateId): Filter;

    public function createEqualsStateNameFilter(string $stateName): Filter;
}
