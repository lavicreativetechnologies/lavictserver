<?php declare(strict_types=1);

namespace Shopware\B2B\Statistic\Framework;

use DateTime;
use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\Filter\DateRangeFilter;
use Shopware\B2B\Common\Filter\EqualsFilter;
use Shopware\B2B\Common\Filter\Filter;
use Shopware\B2B\Common\Filter\FilterSubQueryWithEquals;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Offer\Framework\OfferContextRepository;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\Role\Framework\RoleRepository;
use Shopware\B2B\Shop\BridgePlatform\TranslatedFieldQueryExtenderTrait;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class StatisticRepository implements StatisticRepositoryInterface
{
    use TranslatedFieldQueryExtenderTrait;

    const TABLE_ALIAS = 'orderContext';

    const TABLE_NAME = 'b2b_order_context';

    /**
     * @var array
     */
    private $groupByFunctions = [
        'week' => 'WEEKOFYEAR',
        'month' => 'MONTH',
        'year' => 'YEAR',
    ];

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        AuthenticationService $authenticationService
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @return StatisticAggregate[]
     */
    public function fetchGroupedList(OwnershipContext $ownershipContext, StatisticSearchStruct $searchStruct): array
    {
        $groupByFunction = $this->groupByFunctions[$searchStruct->groupBy];

        $query = $this->connection->createQueryBuilder()
            ->select([
                'list.id as listId',
                $groupByFunction . '(orderContext.created_at) AS createdAtGrouping',
                'YEAR(orderContext.created_at) AS createdAtYear',
                'SUM(list.amount) AS orderAmount',
                'SUM(list.amount_net) AS orderAmountNet',
                'COUNT(orderContext.id) AS orders',
                'SUM(reference.itemCount) AS itemCount',
                'SUM(reference.itemQuantityCount) AS itemQuantityCount',
            ])
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_line_item_list', 'list', 'list.id = orderContext.list_id')
            ->innerJoin(
                'list',
                '(
                    SELECT
                        list_id,
                        COUNT(id) as itemCount,
                        SUM(quantity) as itemQuantityCount
                    FROM b2b_line_item_reference
                    GROUP BY list_id
                )',
                'reference',
                'list.id = reference.list_id'
            )
            ->where(self::TABLE_ALIAS . '.auth_id IN (SELECT id FROM b2b_store_front_auth WHERE b2b_store_front_auth.id = :authId OR context_owner_id = :authId)')
            ->andWhere('orderContext.state != :stateOffer')
            ->groupBy('createdAtYear')
            ->addGroupBy('createdAtGrouping')
            ->orderBy('orderContext.created_at')
            ->setParameter('stateOffer', OfferContextRepository::STATE_OFFER)
            ->setParameter('authId', $ownershipContext->authId->getStorageValue());

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $statement = $query->execute();

        $statistics = [];
        while (false !== ($row = $statement->fetch(PDO::FETCH_ASSOC))) {
            $statistics[] = (new StatisticAggregate())->fromDatabaseArray($row);
        }

        return $statistics;
    }

    /**
     * @return Statistic[]
     */
    public function fetchList(OwnershipContext $ownershipContext, StatisticSearchStruct $searchStruct): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select([
                'orderContext.created_at',
                'orderContext.cleared_at',
                'orderContext.ordernumber',
                'list.amount',
                'list.amount_net',
                'itemCount',
                'itemQuantityCount',
                'list.id as listId',
                'orderContext.id AS orderContextId',
                'orderContext.auth_id AS auth_id',
                'orderContext.state',
            ])
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_line_item_list', 'list', 'list.id = orderContext.list_id')
            ->innerJoin(
                'list',
                '(
                    SELECT
                        list_id,
                        COUNT(id) as itemCount,
                        SUM(quantity) as itemQuantityCount
                    FROM b2b_line_item_reference
                    GROUP BY list_id
                )',
                'reference',
                'list.id = reference.list_id'
            )
            ->where(self::TABLE_ALIAS . '.auth_id IN (SELECT id FROM b2b_store_front_auth WHERE b2b_store_front_auth.id = :authId OR context_owner_id = :authId)')
            ->andWhere('orderContext.state != :stateOffer')
            ->orderBy('orderContext.created_at', 'DESC')
            ->setParameter('authId', $ownershipContext->authId->getStorageValue())
            ->setParameter('stateOffer', OfferContextRepository::STATE_OFFER);

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $statement = $query->execute();

        $statistics = [];
        while (false !== ($row = $statement->fetch(PDO::FETCH_ASSOC))) {
            try {
                $identity = $this->authenticationService->getIdentityByAuthId(IdValue::create($row['auth_id']));
                $row['contact'] = $identity->getPostalSettings();
            } catch (NotFoundException $e) {
                //nth
            }
            $statistics[] = (new Statistic())->fromDatabaseArray($row);
        }

        return $statistics;
    }

    public function fetchTotalCount(OwnershipContext $ownershipContext, StatisticSearchStruct $searchStruct): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_line_item_list', 'list', 'list.id = orderContext.list_id')
            ->innerJoin(
                'list',
                '(
                    SELECT
                        list_id,
                        COUNT(id) as itemCount,
                        SUM(quantity) as itemQuantityCount
                    FROM b2b_line_item_reference
                    GROUP BY list_id
                )',
                'reference',
                'list.id = reference.list_id'
            )
            ->where(self::TABLE_ALIAS . '.auth_id IN (SELECT id FROM b2b_store_front_auth WHERE b2b_store_front_auth.id = :authId OR context_owner_id = :authId)')
            ->andWhere('orderContext.state != :stateOffer')
            ->orderBy('orderContext.created_at', 'DESC')
            ->setParameter('authId', $ownershipContext->authId->getStorageValue())
            ->setParameter('stateOffer', OfferContextRepository::STATE_OFFER);

        $this->dbalHelper->applyFilters($searchStruct, $query);

        return (int) $query
            ->execute()
            ->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * @return ContactEntity[]
     */
    public function fetchStatisticContactList(OwnershipContext $context): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(ContactRepository::TABLE_NAME, ContactRepository::TABLE_ALIAS)
            ->where(ContactRepository::TABLE_ALIAS . '.auth_id IN (SELECT id FROM b2b_store_front_auth WHERE b2b_store_front_auth.id = :authId OR context_owner_id = :authId)')
            ->andWhere('0 != (SELECT COUNT(*) FROM b2b_order_context WHERE auth_id = contact.auth_id)')
            ->orderBy(ContactRepository::TABLE_ALIAS . '.lastname', 'ASC')
            ->setParameter('authId', $context->authId->getStorageValue());

        $statement = $query->execute();
        $contactsData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $contacts = [];
        foreach ($contactsData as $contactData) {
            $contacts[] = (new ContactEntity())->fromDatabaseArray($contactData);
        }

        return $contacts;
    }

    /**
     * @return RoleEntity[]
     */
    public function fetchStatisticRoleList(OwnershipContext $context): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->from(RoleRepository::TABLE_ROLE_NAME, RoleRepository::TABLE_ROLE_ALIAS)
            ->where(RoleRepository::TABLE_ROLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->andWhere(RoleRepository::TABLE_ROLE_ALIAS . '.id IN (SELECT role_id FROM b2b_order_context orderContext INNER JOIN b2b_debtor_contact contact ON orderContext.auth_id = contact.auth_id INNER JOIN b2b_role_contact role ON role.debtor_contact_id = contact.id)')
            ->orderBy(RoleRepository::TABLE_ROLE_ALIAS . '.name', 'ASC')
            ->setParameter('contextOwnerId', $context->contextOwnerId->getStorageValue());

        $statement = $query->execute();
        $rawRoles = $statement->fetchAll(PDO::FETCH_ASSOC);

        $roles = [];
        foreach ($rawRoles as $rawRole) {
            $roles[] = (new RoleEntity())->fromDatabaseArray($rawRole);
        }

        return $roles;
    }

    public function fetchStatisticStatesList(OwnershipContext $context): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('DISTINCT state as state')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.auth_id IN (SELECT id FROM b2b_store_front_auth WHERE b2b_store_front_auth.id = :authId OR context_owner_id = :authId)')
            ->andWhere('orderContext.state != :stateOffer')
            ->setParameter('authId', $context->authId->getStorageValue())
            ->setParameter('stateOffer', OfferContextRepository::STATE_OFFER);

        $query->orderBy('state', 'ASC');

        $statement = $query->execute();
        $rawStates = $statement->fetchAll(PDO::FETCH_ASSOC);

        $states = [];
        foreach ($rawStates as $rawState) {
            $states[] = [
                'id' => $rawState['state'],
                'name' => $rawState['state'],
            ];
        }

        return $states;
    }

    public function createDateRangeFilter(DateTime $from, DateTime $to): DateRangeFilter
    {
        return new DateRangeFilter($this->getMainTableAlias(), 'created_at', $from, $to);
    }

    public function createEqualsAuthorFilter(IdValue $authId): EqualsFilter
    {
        return new EqualsFilter($this->getMainTableAlias(), 'auth_id', $authId->getStorageValue());
    }

    /**
     * @deprecated tag:v4.2.0 no longer used
     */
    public function createEqualsStatesFilter(IdValue $stateId): Filter
    {
        return new EqualsFilter($this->getMainTableAlias(), 'status_id', $stateId->getStorageValue());
    }

    public function createEqualsStateNameFilter(string $stateName): Filter
    {
        return new EqualsFilter($this->getMainTableAlias(), 'state', $stateName);
    }

    public function createEqualsRoleFilter(IdValue $roleId): FilterSubQueryWithEquals
    {
        return new FilterSubQueryWithEquals(
            self::TABLE_ALIAS . '.auth_id IN (
                SELECT DISTINCT %1$s_contact.auth_id
                FROM b2b_debtor_contact %1$s_contact
                INNER JOIN b2b_role_contact %1$s ON %1$s_contact.id = %1$s.debtor_contact_id
                WHERE %2$s
            )',
            'role_id',
            $roleId->getStorageValue()
        );
    }

    /**
     * @return string query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }
}
