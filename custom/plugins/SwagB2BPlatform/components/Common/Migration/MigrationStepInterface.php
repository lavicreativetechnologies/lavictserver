<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Migration;

use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\Container;

interface MigrationStepInterface
{
    public function getCreationTimeStamp(): int;

    public function updateDatabase(Connection $connection);

    public function updateThroughServices(Container $container);
}
