<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Filter;

use function array_merge;

class FilterResponse
{
    /**
     * @var string
     */
    public $queryPart;

    /**
     * @var array
     */
    public $params;

    public function __construct(string $queryPart = '', array $params = [])
    {
        $this->queryPart = $queryPart;
        $this->params = $params;
    }

    /**
     * @return FilterResponse
     */
    public function addQueryPart(string $queryPart): self
    {
        $this->queryPart .= ' ' . $queryPart;

        return $this;
    }

    /**
     * @return FilterResponse
     */
    public function addParams(array $params): self
    {
        $this->params = array_merge(
            $this->params,
            $params
        );

        return $this;
    }
}
