<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Filter;

interface Filter
{
    public function getFilterResponse(string $paramPrefix): FilterResponse;
}
