<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Filter;

use function array_reverse;
use function count;
use function explode;
use function implode;
use function is_numeric;

class LikeFilter implements Filter
{
    /**
     * @var string
     */
    private $tableAlias;

    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var string|int|float
     */
    private $value;

    /**
     * @param string|int|float $value
     */
    public function __construct(string $tableAlias, string $fieldName, $value)
    {
        $this->tableAlias = $tableAlias;
        $this->fieldName = $fieldName;
        $this->value = $value;

        $this->validateDateTime((string) $value);
    }

    /**
     * @internal
     */
    protected function validateDateTime(string $value): void
    {
        $explodedValue = explode('.', $value);
        if (count($explodedValue) === 1) {
            return;
        }

        foreach ($explodedValue as $item) {
            if (!is_numeric($item)) {
                return;
            }
        }

        $explodedValue = array_reverse($explodedValue);
        $this->value = implode('-', $explodedValue);
    }

    public function getFilterResponse(string $paramPrefix): FilterResponse
    {
        $response = new FilterResponse('(');

        foreach ([$this->value . '%', '%' . $this->value . '%', '%' . $this->value] as $index => $value) {
            $paramName = $paramPrefix . $index;

            if ($index > 0) {
                $response->addQueryPart('OR');
            }

            $response->addQueryPart($this->tableAlias . '.' . $this->fieldName . ' LIKE :' . $paramName);
            $response->addParams([$paramName => $value]);
        }
        $response->addQueryPart(')');

        return $response;
    }
}
