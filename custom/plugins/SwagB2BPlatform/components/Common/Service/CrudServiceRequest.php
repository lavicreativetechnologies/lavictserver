<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Service;

use DomainException;
use InvalidArgumentException;
use Shopware\B2B\Common\IdValue;
use function array_key_exists;
use function in_array;
use function sprintf;

class CrudServiceRequest
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $allowedKeys;

    public function __construct(array $data, array $allowedKeys)
    {
        $this->data = $data;
        $this->allowedKeys = $allowedKeys;
    }

    public function getFilteredData(): array
    {
        $filteredData = [];

        foreach ($this->allowedKeys as $key) {
            if (!array_key_exists($key, $this->data)) {
                continue;
            }
            $filteredData[$key] = $this->data[$key];
        }

        return $filteredData;
    }

    /**
     * @throws InvalidArgumentException
     * @throws DomainException
     */
    public function requireParam(string $key)
    {
        if (!in_array($key, $this->allowedKeys, true)) {
            throw new DomainException(sprintf('Trying to require an invalid key "%s"', $key));
        }

        if (!array_key_exists($key, $this->data)) {
            throw new InvalidArgumentException(sprintf('Trying to require a missing key "%s"', $key));
        }

        return $this->data[$key];
    }

    public function requireIdValue($key): IdValue
    {
        return IdValue::create($this->requireParam($key));
    }

    public function hasValueForParam(string $key): bool
    {
        try {
            $value = $this->requireParam($key);
        } catch (DomainException $e) {
            return false;
        } catch (InvalidArgumentException $e) {
            return false;
        }

        return (bool) $value;
    }
}
