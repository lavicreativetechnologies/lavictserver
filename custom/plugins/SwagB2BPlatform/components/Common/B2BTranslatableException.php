<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

interface B2BTranslatableException extends B2BException
{
    public function getTranslationMessage(): string;

    public function getTranslationParams(): array;
}
