<?php declare(strict_types=1);

namespace Shopware\B2B\Common\MvcExtension;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use function uniqid;

class FileFactory
{
    public static function fromArray(array $file): File
    {
        $fileObject = new File($file['tmp_name']);
        $fileName = uniqid('ShopwareB2b_', true) . '_' . $file['name'];

        $fileObject = $fileObject->move($fileObject->getPath(), $fileName);

        return $fileObject;
    }

    public static function fromUploadedFile(UploadedFile $file): File
    {
        $fileObject = new File($file->getPathname());
        $fileName = uniqid('ShopwareB2b_', true) . '_' . $file->getClientOriginalName();

        $fileObject = $fileObject->move($fileObject->getPath(), $fileName);

        return $fileObject;
    }
}
