<?php declare(strict_types=1);

namespace Shopware\B2B\Common\MvcExtension;

/**
 * @deprecated tag:v4.4.0 - Will be removed without replacement
 */
interface MvcEnvironmentInterface
{
    public function getPathinfo(): string;
}
