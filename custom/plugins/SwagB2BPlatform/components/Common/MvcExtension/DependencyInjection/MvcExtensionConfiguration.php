<?php declare(strict_types=1);

namespace Shopware\B2B\Common\MvcExtension\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class MvcExtensionConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [];
    }
}
