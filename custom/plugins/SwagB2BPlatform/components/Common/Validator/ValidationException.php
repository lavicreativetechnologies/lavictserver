<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Validator;

use Exception;
use InvalidArgumentException;
use Shopware\B2B\Common\B2BException;
use Shopware\B2B\Common\Entity;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use function implode;

class ValidationException extends InvalidArgumentException implements B2BException
{
    /**
     * @var ConstraintViolationListInterface
     */
    private $violations;

    /**
     * @var array
     */
    private $sortedViolations = [];

    /**
     * @var Entity
     */
    private $entity;

    /**
     * @param string $message
     * @param null $code
     */
    public function __construct(
        Entity $entity,
        ConstraintViolationListInterface $violations,
        $message,
        $code = null,
        Exception $previous = null
    ) {
        $readableViolationList = [];

        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $fieldName = $violation->getPropertyPath();

            if (!isset($this->sortedViolations[$fieldName])) {
                $this->sortedViolations[$fieldName] = [];
            }

            $this->sortedViolations[$fieldName][] = $violation;
            $readableViolationList[] = $violation->getPropertyPath() . ': ' . $violation->getMessage();
        }

        parent::__construct($message . "\n\t" . implode("\n\t<br>", $readableViolationList), $code, $previous);

        $this->violations = $violations;
        $this->entity = $entity;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolations()
    {
        return $this->violations;
    }

    public function getEntity(): Entity
    {
        return $this->entity;
    }
}
