<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsNotOnlyWhitespace extends Constraint
{
    public $message = 'This value should not contain only whitespace.';
}
