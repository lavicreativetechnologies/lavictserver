<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version;

use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use function class_exists;
use function method_exists;
use function property_exists;

class ShopwareVersion
{
    public static function isPlatform(): bool
    {
        return class_exists('Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition');
    }

    public static function isClassic(): bool
    {
        return !self::isPlatform();
    }

    public static function isShopware6_1_0(): bool
    {
        return self::isPlatform() && !property_exists(SalesChannelProductEntity::class, 'calculatedMaxPurchase');
    }

    public static function isShopware6_2_X(): bool
    {
        return self::isPlatform() && method_exists(CartService::class, 'deleteCart');
    }

    public static function isShopware6_3_x(): bool
    {
        return self::isPlatform() && class_exists('\\Shopware\\Core\\Checkout\\Cart\\CartCalculator');
    }

    public static function isShopware6_3_2(): bool
    {
        return self::isPlatform() && class_exists('\\Shopware\\Core\\Checkout\\Cart\\Event\\CartSavedEvent');
    }

    public static function isShopware6_3_5(): bool
    {
        return self::isPlatform() && class_exists('\\Shopware\\Core\\Framework\\Webhook\\Hookable\\HookableEventCollector');
    }
}
