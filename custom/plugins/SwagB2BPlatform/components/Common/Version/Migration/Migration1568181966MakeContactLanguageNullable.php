<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1568181966MakeContactLanguageNullable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1568181966;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_debtor_contact MODIFY language VARCHAR(10) NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
