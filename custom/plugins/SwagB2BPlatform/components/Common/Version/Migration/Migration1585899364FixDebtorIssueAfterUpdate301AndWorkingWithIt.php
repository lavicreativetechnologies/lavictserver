<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1585899364FixDebtorIssueAfterUpdate301AndWorkingWithIt implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1585899364;
    }

    public function updateDatabase(Connection $connection)
    {
        $query = $connection->createQueryBuilder();

        $debtors = $query->select('auth1.id as `newDebtorId`', 'auth2.id as `oldDebtorId`')
        ->from('b2b_store_front_auth', 'auth1')
        ->innerJoin(
            'auth1',
            'b2b_store_front_auth',
            'auth2',
            'auth1.provider_context = auth2.provider_context
            AND auth1.provider_key LIKE \'%DebtorRepositoryInterface\'
            AND auth2.provider_key LIKE \'%DebtorRepository\''
        )->execute()->fetchAll();

        if (!$debtors) {
            $connection->exec('
                UPDATE b2b_store_front_auth SET provider_key = \'Shopware\\\B2B\\\Debtor\\\Framework\\\DebtorRepositoryInterface\'
                WHERE provider_key =
                \'Shopware\\\B2B\\\Debtor\\\Bridge\\\DebtorRepository\'
            ');

            return;
        }

        $columnsToUpdate = [
            'b2b_role' => ['context_owner_id'],
            'b2b_contingent_group' => ['context_owner_id'],
            'b2b_order_list' => ['context_owner_id'],
            'b2b_in_stocks' => ['auth_id'],
            'b2b_budget' => ['context_owner_id', 'owner_id'],
            'b2b_sales_representative_clients' => ['client_id'],
            'b2b_budget_transaction' => ['auth_id'],
            'b2b_line_item_list' => ['context_owner_id'],
            'b2b_order_context' => ['auth_id', 'sales_representative_auth_id'],
            'b2b_debtor_contact' => ['auth_id', 'context_owner_id'],
            'b2b_offer' => ['auth_id'],
            'b2b_order_number' => ['context_owner_id'],
            'b2b_store_front_auth' => ['context_owner_id'],
        ];

        $query = $connection->createQueryBuilder();

        foreach ($debtors as $debtor) {
            foreach ($columnsToUpdate as $tableName => $columns) {
                switch ($tableName) {
                    case 'b2b_role':
                        $this->updateRoleTable($connection, $debtor);
                        break;
                    case 'b2b_sales_representative_clients':
                        $this->updateSalesRepresentativeClients($connection, $debtor);
                        break;
                    default:
                        foreach ($columns as $column) {
                            $query->update($tableName)
                                ->add('set', $column . ' = ' . $debtor['newDebtorId'])
                                ->where($column . ' = :oldDebtorId')
                                ->setParameter(':oldDebtorId', $debtor['oldDebtorId'])
                                ->execute();
                        }
                }
            }
        }

        $connection->exec(
            'DELETE FROM b2b_store_front_auth WHERE provider_key = \'Shopware\\\B2B\\\Debtor\\\Bridge\\\DebtorRepository\''
        );
    }

    public function updateThroughServices(Container $container)
    {
        // nth
    }

    /**
     * @internal
     * sales representative client duplicate entry check because of unique key constraint
     */
    protected function updateSalesRepresentativeClients(Connection $connection, array $debtor)
    {
        $query = $connection->createQueryBuilder();
        try {
            $query->update('b2b_sales_representative_clients')
                ->add('set', 'client_id = ' . $debtor['newDebtorId'])
                ->where('client_id = :oldDebtorId')
                ->setParameter(':oldDebtorId', $debtor['oldDebtorId'])
                ->execute();
        } catch (DBALException $e) {
            $query->delete('b2b_sales_representative_clients')
                ->where('client_id = :oldDebtorId')
                ->setParameter(':oldDebtorId', $debtor['oldDebtorId'])
                ->execute();
        }
    }

    /**
     * @internal
     */
    protected function updateRoleTable(Connection $connection, array $debtor)
    {
        $query = $connection->createQueryBuilder();
        $rightRootValue = $query
            ->select('`right`')
            ->from('b2b_role')
            ->where('`context_owner_id` = :contextOwnerId AND `level` = 0 AND `left` = 1')
            ->setParameter(':contextOwnerId', $debtor['newDebtorId'])
            ->execute()
            ->fetchColumn();

        if (!$rightRootValue) {
            // no root role for new debtor exist

            $query = $connection->createQueryBuilder();
            $query->update('b2b_role')
                ->set('context_owner_id', $debtor['newDebtorId'])
                ->where('context_owner_id = :oldDebtorId')
                ->setParameter(':oldDebtorId', $debtor['oldDebtorId'])
                ->execute();

            return;
        }

        $query = $connection->createQueryBuilder();
        $oldRoleCount = $query
            ->select('COUNT(id)')
            ->from('b2b_role')
            ->where('`context_owner_id` = :contextOwnerId')
            ->setParameter(':contextOwnerId', $debtor['oldDebtorId'])
            ->execute()
            ->fetchColumn();

        if (!$oldRoleCount) {
            // old debtor has no roles to migrate
            return;
        }

        // update level, left, right, context_owner_id for migration
        $connection->executeUpdate('UPDATE b2b_role SET
            `name` = CONCAT(`name`, \'__migrated\'),
            `context_owner_id` = :contextOwnerId,
            `left` = (`left` - 1 + :rightRootValue),
            `right` = (`right` - 1 + :rightRootValue),
            `level` = (`level` + 1)
            WHERE context_owner_id = :oldContextOwnerId',
            [
                ':contextOwnerId' => $debtor['newDebtorId'],
                ':rightRootValue' => $rightRootValue,
                ':oldContextOwnerId' => $debtor['oldDebtorId'],
            ]
        );

        // set new right value for new root
        $connection->executeUpdate('UPDATE b2b_role SET
            `right` = `right` + :oldRoleCountTimesTwo
            WHERE `context_owner_id` = :contextOwnerId AND `level` = 0 AND `left` = 1',
            [
                ':contextOwnerId' => $debtor['newDebtorId'],
                ':oldRoleCountTimesTwo' => ((int) $oldRoleCount * 2),
            ]
        );
    }
}
