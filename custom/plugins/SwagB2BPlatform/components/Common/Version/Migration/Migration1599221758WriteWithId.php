<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1599221758WriteWithId implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1599221758;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_audit_log
                ADD write_with_id INT(11) UNSIGNED DEFAULT NULL;
        ');

        $connection->exec('
            ALTER TABLE b2b_audit_log
                ADD CONSTRAINT FK_b2b_audit_log_write_with
                    FOREIGN KEY (write_with_id)
                    REFERENCES b2b_audit_log (id) ON DELETE SET NULL ON UPDATE CASCADE;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
