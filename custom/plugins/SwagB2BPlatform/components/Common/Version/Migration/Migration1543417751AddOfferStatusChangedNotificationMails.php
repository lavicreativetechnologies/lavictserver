<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Bundle\AttributeBundle\Service\TypeMapping;
use Shopware\Models\Mail\Mail;
use Shopware\Models\Shop\Locale;
use Shopware_Components_Translation;
use Symfony\Component\DependencyInjection\Container;
use function array_keys;
use function array_merge;
use function file_get_contents;

class Migration1543417751AddOfferStatusChangedNotificationMails implements MigrationStepInterface
{
    /**
     * @var Locale
     */
    protected $default_locale;

    const FIXTURE_PATH = __DIR__ . '/../../../Offer/Fixtures/b2bOfferStatusChangedNotificationMail/';

    public function getCreationTimeStamp(): int
    {
        return 1543417751;
    }

    public function updateDatabase(Connection $connection): void
    {
        // nth
    }

    public function updateThroughServices(Container $container): void
    {
        $this->default_locale = $container->get('models')
            ->getRepository('Shopware\Models\Shop\Shop')
            ->getDefault()->getLocale();

        $this->addMailTemplate($container);

        $this->addMailCheckbox($container);
    }

    protected function addMailCheckbox(Container $container): void
    {
        $attributeService = $container->get('shopware_attribute.crud_service');

        $translations = [
            'en_GB' => [
                'label' => 'Receive offer status notification',
                'supportText' => 'Activate or deactivate notification mail',
                'helpText' => 'The system sends out an email to every admin, who checked this box, when the status of an offer changed and an admin is required to proceed.',
            ],
            'de_DE' => [
                'label' => 'Erhalte Nachrichten bei Angebotsstatusänderungen',
                'supportText' => 'Aktiviere oder deaktiviere Benachrichtigungsmail',
                'helpText' => 'Das System schickt an jeden Administrator, der dieses Feld anwählt eine E-Mail, jedesmal, wenn sich der Status eines Angbots ändert und es einen Administrator zum fortfahren benötigt.',
            ],
        ];

        $attributeData = array_merge([
            'translatable' => false,
            'displayInBackend' => true,
        ], $translations[$this->default_locale->toString()]);
        unset($translations[$this->default_locale->toString()]);

        $attributeService->update(
            's_core_auth_attributes',
            'send_offer_status_changed_notification_mail',
            TypeMapping::TYPE_BOOLEAN,
            $attributeData,
            null,
            false,
            0
        );
    }

    protected function addMailTemplate(Container $container): void
    {
        $mailName = 'b2bOfferStatusChanged_general';

        $modelManager = $container->get('models');
        $mailRepo = $modelManager->getRepository('Shopware\Models\Mail\Mail');

        if ($mailRepo->findOneBy(['name' => $mailName])) {
            return;
        }

        $emailContent = [
            'name' => $mailName,
            'fromMail' => '{config name=mail}',
            'fromName' => '{config name=shopName}',
            'isHtml' => true,
            'mailType' => Mail::MAILTYPE_SYSTEM,
        ];

        $translations = [
            'en_GB' => [
                'subject' => 'The status of {if $context.isToEnquirer}your offer{else}the offer by {$offer.enquirer}{/if} at {config name=shopName} has changed',
                'content' => file_get_contents(self::FIXTURE_PATH . 'plain_en.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'html_en.tpl'),
            ],
            'de_DE' => [
                'subject' => 'Der Status {if $context.isToEnquirer}deines Angebotes{else}des Angebots von {$offer.enquirer}{/if} bei {config name=shopName} hat sich ge&auml;ndert.',
                'content' => file_get_contents(self::FIXTURE_PATH . 'plain_de.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'html_de.tpl'),
            ],
        ];

        $emailContent = array_merge($emailContent, $translations[$this->default_locale->toString()]);
        unset($translations[$this->default_locale->toString()]);

        $modelManager->persist((new Mail())->fromArray($emailContent));
        $modelManager->flush();

        $mail = $mailRepo->findOneBy(['name' => $mailName]);

        $shopRepo = $modelManager->getRepository('Shopware\Models\Shop\Shop');

        /** @var Shopware_Components_Translation $translation */
        $translation = $container->get('translation');

        foreach (array_keys($translations) as $foreignLanguage) {
            $foreignShop = $shopRepo->findBy(['locale' => $foreignLanguage]);

            foreach ($foreignShop as $shopId) {
                $translation->write($shopId, 'config_mails', $mail->getId(), $translations[$foreignLanguage]);
            }
        }
    }
}
