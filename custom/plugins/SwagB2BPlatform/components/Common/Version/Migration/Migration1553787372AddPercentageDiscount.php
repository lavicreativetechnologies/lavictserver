<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1553787372AddPercentageDiscount implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1553787372;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_offer
            ADD `percentage_discount` DOUBLE DEFAULT NULL
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
