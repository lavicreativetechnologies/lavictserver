<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Models\Mail\Mail;
use Shopware_Components_Translation;
use Symfony\Component\DependencyInjection\Container;
use function array_merge;
use function file_get_contents;

class Migration1493892870BudgetMail implements MigrationStepInterface
{
    const LANGUAGES = [
        'german' => 'de_DE',
        'english' => 'en_GB',
    ];

    public function getCreationTimeStamp(): int
    {
        return 1493892870;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->query('
            CREATE TABLE `b2b_budget_notify` (
                `budget_id` INT(11) NOT NULL,
                `refresh_group` INT(11) NOT NULL,
                `time` DATETIME NOT NULL,
            
                PRIMARY KEY (`budget_id`, `refresh_group`),
                INDEX `FK_b2b_budget_notify_b2b_budget` (`budget_id`),
            
                CONSTRAINT `FK_b2b_budget_notify_b2b_budget` FOREIGN KEY (`budget_id`) 
                  REFERENCES `b2b_budget` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        $modelManager = $container->get('models');

        $mailRepo = $modelManager->getRepository('Shopware\Models\Mail\Mail');

        if ($mailRepo->findOneBy(['name' => 'b2bBudgetNotify'])) {
            return;
        }

        $query = $container->get('dbal_connection')->createQueryBuilder();

        $fixturePath = __DIR__ . '/../../../Budget/Fixtures/';

        $mail = new Mail();

        $emailContent = [
            'name' => 'b2bBudgetNotify',
            'isHtml' => false,
            'subject' => 'Budget ({$budget.name}) notify percentage reached',
            'fromMail' => '{config name=mail}',
            'fromName' => '{config name=shopName}',
            'content' => file_get_contents($fixturePath . 'plain_en.tpl'),
            'mailType' => $mail::MAILTYPE_SYSTEM,
        ];

        $translations = [
            'subject' => 'Budget ({$budget.name}) hat die angegebene Prozentzahl erreicht',
            'content' => file_get_contents($fixturePath . 'plain_de.tpl'),
        ];

        $foreignLanguage = self::LANGUAGES['german'];

        $defaultShopLanguage = $query->select('locales.locale')
            ->from('s_core_locales', 'locales')
            ->leftJoin('locales', 's_core_shops', 'shops', 'shops.locale_id = locales.id')
            ->where('shops.default = 1')
            ->execute()
            ->fetch(PDO::FETCH_COLUMN);

        if ($defaultShopLanguage === self::LANGUAGES['german']) {
            $germanMailContent = [
                'subject' => 'Budget ({$budget.name}) hat die angegebene Prozentzahl erreicht',
                'content' => file_get_contents($fixturePath . 'plain_de.tpl'),
            ];

            $translations = [
                'subject' => 'Budget ({$budget.name}) notify percentage reached',
                'content' => file_get_contents($fixturePath . 'plain_en.tpl'),
            ];

            $foreignLanguage = self::LANGUAGES['english'];

            $emailContent = array_merge($emailContent, $germanMailContent);
        }

        $mail->fromArray($emailContent);

        $modelManager->persist($mail);
        $modelManager->flush();

        $mail = $mailRepo->findOneBy(['name' => 'b2bBudgetNotify']);

        $foreignShopIds = $query->select('coreShops.id')
            ->from('s_core_shops', 'coreShops')
            ->leftJoin('coreShops', 's_core_locales', 'coreLocales', 'coreShops.locale_id = coreLocales.id')
            ->where('coreLocales.locale = :locale')
            ->setParameter('locale', $foreignLanguage)
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);

        /** @var Shopware_Components_Translation $translation */
        $translation = $container->get('translation');

        foreach ($foreignShopIds as $shopId) {
            $translation->write($shopId, 'config_mails', $mail->getId(), $translations);
        }
    }
}
