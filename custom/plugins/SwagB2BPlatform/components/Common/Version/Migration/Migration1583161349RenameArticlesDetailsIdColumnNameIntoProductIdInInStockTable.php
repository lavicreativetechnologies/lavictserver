<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1583161349RenameArticlesDetailsIdColumnNameIntoProductIdInInStockTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1583161349;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_in_stocks`
                CHANGE COLUMN `articles_details_id` `product_id` INT(11) unsigned NOT NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
