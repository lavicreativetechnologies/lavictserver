<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1562067356AddCustomOrderNumberIndex implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1562067356;
    }

    public function updateDatabase(Connection $connection): void
    {
        //Add index on custom ordernumber
        $connection->exec('
			CREATE INDEX fk_custom_ordernumber ON b2b_order_number (custom_ordernumber);
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
