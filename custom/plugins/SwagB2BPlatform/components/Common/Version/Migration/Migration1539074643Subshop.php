<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1539074643Subshop implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1539074643;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_debtor_contact DROP KEY email;

            ALTER TABLE b2b_debtor_contact
              ADD CONSTRAINT email_context_owner
            UNIQUE (email, context_owner_id);
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
