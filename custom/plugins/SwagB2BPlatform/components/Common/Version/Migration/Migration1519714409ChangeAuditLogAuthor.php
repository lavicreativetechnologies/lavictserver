<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1519714409ChangeAuditLogAuthor implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1519714409;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
ALTER TABLE b2b_audit_log_author
  ADD `is_backend`  TINYINT(1) NOT NULL DEFAULT 0;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
