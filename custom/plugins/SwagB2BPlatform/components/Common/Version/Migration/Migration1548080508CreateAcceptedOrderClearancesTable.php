<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1548080508CreateAcceptedOrderClearancesTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1548080508;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE `b2b_accepted_order_clearances` (
              `order_number` VARCHAR(255),
              `auth_id` INT(11),
              `responsible_auth_id` INT(11),
              `accepted_at` DATETIME,
              
              CONSTRAINT `b2b_accepted_order_clearances_auth_id_FK` FOREIGN KEY (`auth_id`) 
                 REFERENCES `b2b_store_front_auth` (`id`) ON UPDATE NO ACTION ON DELETE SET NULL,
                 
              CONSTRAINT `b2b_accepted_order_clearances_responsible_auth_id_FK` FOREIGN KEY (`responsible_auth_id`) 
                 REFERENCES `b2b_store_front_auth` (`id`) ON UPDATE NO ACTION ON DELETE SET NULL 
            )
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
