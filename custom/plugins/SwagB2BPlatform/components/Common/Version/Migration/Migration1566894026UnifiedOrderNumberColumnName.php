<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1566894026UnifiedOrderNumberColumnName implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1566894026;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_order_number CHANGE product_details_id product_id INT(11) UNSIGNED NOT NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
