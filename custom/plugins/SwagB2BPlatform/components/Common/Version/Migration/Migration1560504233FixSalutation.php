<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1560504233FixSalutation implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1560504233;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec(
            'UPDATE s_user_addresses SET salutation = REPLACE(salutation, \'mrs\', \'ms\');'
        );
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
