<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1588768963DropSalesRepresentativeOrders implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1588768963;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('DROP TABLE b2b_sales_representative_orders');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
