<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1536675714ProductNumberAllowanceRuleType implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1536675714;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec(
            'CREATE TABLE b2b_contingent_group_rule_product_number_allowance (
              contingent_rule_id INT(11) NOT NULL,
              product_numbers TEXT NOT NULL,
            
              PRIMARY KEY (`contingent_rule_id`),
            
              CONSTRAINT b2b_contingent_group_rule_product_number_allowance_rule_id_FK FOREIGN KEY (`contingent_rule_id`)
              REFERENCES `b2b_contingent_group_rule` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE
            )
              COLLATE=\'utf8_unicode_ci\''
        );
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
