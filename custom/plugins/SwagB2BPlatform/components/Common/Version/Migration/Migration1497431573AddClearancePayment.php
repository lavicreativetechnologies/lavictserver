<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware_Components_Translation;
use Symfony\Component\DependencyInjection\Container;
use function count;

class Migration1497431573AddClearancePayment implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1497431573;
    }

    public function updateDatabase(Connection $connection): void
    {
    }

    public function updateThroughServices(Container $container): void
    {
        /** @var \Shopware\Components\Plugin\PaymentInstaller $paymentInstaller */
        $paymentInstaller = $container->get('shopware.plugin_payment_installer');

        $options = [
            'name' => 'b2b_order_clearance_payment',
            'description' => 'Freigabe',
            'action' => '',
            'active' => 1,
            'position' => 0,
            'additionalDescription' => '',
        ];
        $payment = $paymentInstaller->createOrUpdate('SwagB2bPlugin', $options);

        $query = $container->get('dbal_connection')->createQueryBuilder();

        $englishShopIds = $query->select('shops.id')
            ->from('s_core_shops', 'shops')
            ->leftJoin('shops', 's_core_locales', 'locales', 'shops.locale_id = locales.id')
            ->where('locales.locale = :locale')
            ->setParameter('locale', 'en_GB')
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);

        if (count($englishShopIds) === 0) {
            return;
        }

        /** @var Shopware_Components_Translation $translation */
        $translation = $container->get('translation');

        foreach ($englishShopIds as $englishShopId) {
            $translation->write($englishShopId, 'config_payment', $payment->getId(), ['description' => 'Clearance'], true);
        }
    }
}
