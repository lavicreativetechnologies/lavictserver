<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1540548885ChangeDebtorRepositoryToInterface implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1540548885;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            UPDATE b2b_store_front_auth SET provider_key = \'Shopware\\\B2B\\\Debtor\\\Framework\\\DebtorRepositoryInterface\'
            WHERE provider_key =
            \'Shopware\\\B2B\\\Debtor\\\Framework\\\DebtorRepository\'
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        //nth
    }
}
