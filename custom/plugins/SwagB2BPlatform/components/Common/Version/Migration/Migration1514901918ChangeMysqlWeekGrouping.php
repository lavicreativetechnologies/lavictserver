<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1514901918ChangeMysqlWeekGrouping implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1514901918;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            UPDATE `b2b_contingent_group_rule_time_restriction`
            SET `time_restriction` = "YEARWEEK"
            WHERE `time_restriction` = "WEEKOFYEAR"
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
