<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

interface CrudEntity extends Entity, \JsonSerializable
{
    public function isNew(): bool;

    public function toDatabaseArray(): array;

    /**
     * @return CrudEntity
     */
    public function fromDatabaseArray(array $data): self;
}
