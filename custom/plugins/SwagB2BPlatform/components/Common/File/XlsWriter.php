<?php declare(strict_types=1);

namespace Shopware\B2B\Common\File;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use function range;

class XlsWriter
{
    public function write(array $data, string $fileName = 'php://output'): void
    {
        $spreadsheet = new Spreadsheet();
        $alphabet = range('A', 'Z');

        $rowCounter = 1;
        foreach ($data as $row) {
            $columnCounter = 1;
            foreach ($row as $column) {
                $index = $alphabet[$columnCounter - 1] . $rowCounter;
                $spreadsheet->setActiveSheetIndex(0)->setCellValue($index, $column);
                $spreadsheet->getActiveSheet()->getColumnDimension(Coordinate::stringFromColumnIndex($columnCounter))->setAutoSize(true);
                $columnCounter++;
            }
            $spreadsheet->getActiveSheet()->getRowDimension($rowCounter)->setRowHeight(16);
            $rowCounter++;
        }

        $spreadsheet->getActiveSheet()->setTitle('Export');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($fileName);
    }
}
