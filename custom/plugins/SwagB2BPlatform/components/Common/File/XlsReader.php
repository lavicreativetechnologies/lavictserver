<?php declare(strict_types=1);

namespace Shopware\B2B\Common\File;

use InvalidArgumentException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use function file_exists;

class XlsReader
{
    public function read(string $file, CsvContext $context): array
    {
        if (!file_exists($file)) {
            throw new InvalidArgumentException('the provided file does not exists');
        }

        /** @var \PhpOffice\PhpSpreadsheet\Reader\Xls $reader */
        $reader = IOFactory::createReaderForFile($file);
        $spreadsheet = $reader->load($file);

        $rowIterator = $spreadsheet->getActiveSheet()->getRowIterator(1);

        $data = [];
        $headline = $context->headline;

        foreach ($rowIterator as $row) {
            if ($headline === true) {
                $headline = false;
                continue;
            }

            $columnArray = [];
            $columnIterator = $row->getCellIterator();
            foreach ($columnIterator as $column) {
                $columnArray[] = $column->getValue();
            }

            $data[] = $columnArray;
        }

        return $data;
    }
}
