<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use function get_class;

class B2BContainerBuilder
{
    /**
     * @var DependencyInjectionConfiguration[]
     */
    private $registeredConfigurations = [];

    /**
     * @return B2BContainerBuilder
     */
    public static function create(): self
    {
        return new self();
    }

    public function addConfiguration(DependencyInjectionConfiguration $configuration): void
    {
        $this->registeredConfigurations[] = $configuration;
    }

    public function registerConfigurations(ContainerBuilder $containerBuilder): void
    {
        $loader = new XmlFileLoader(
            $containerBuilder,
            new FileLocator()
        );

        foreach ($this->registeredConfigurations as $configuration) {
            $this->registerConfiguration($containerBuilder, $configuration, $loader);
        }

        $this->registeredConfigurations = [];
    }

    /**
     * @internal
     */
    protected function registerConfiguration(
        ContainerBuilder $containerBuilder,
        DependencyInjectionConfiguration $configuration,
        XmlFileLoader $loader
    ): void {
        $paramId = 'b2b_' . get_class($configuration);

        if ($containerBuilder->hasParameter($paramId)) {
            return;
        }

        foreach ($configuration->getServiceFiles($containerBuilder) as $serviceFilePath) {
            $loader->load($serviceFilePath);
        }

        foreach ($configuration->getCompilerPasses() as $compilerPass) {
            $containerBuilder->addCompilerPass($compilerPass);
        }

        $containerBuilder->setParameter($paramId, 1);

        foreach ($configuration->getDependingConfigurations() as $dependingConfiguration) {
            $this->registerConfiguration($containerBuilder, $dependingConfiguration, $loader);
        }
    }
}
