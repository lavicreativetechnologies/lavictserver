<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

/**
 * Marks database stored items. If the extended CrudEntity is not used, this basically means read only but uniquely identified data.
 */
interface Entity
{
}
