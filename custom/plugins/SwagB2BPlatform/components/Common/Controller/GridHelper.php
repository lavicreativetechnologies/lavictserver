<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Controller;

use DomainException;
use InvalidArgumentException;
use Shopware\B2B\Common\Filter\EqualsFilter;
use Shopware\B2B\Common\Filter\Filter;
use Shopware\B2B\Common\Filter\FilterSubQueryWithLike;
use Shopware\B2B\Common\Filter\LikeFilter;
use Shopware\B2B\Common\Filter\OrFilter;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\SearchStruct;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use function array_key_exists;
use function array_merge;
use function ceil;
use function count;
use function explode;
use function is_array;
use function max;
use function reset;
use function sprintf;
use function str_replace;
use function ucfirst;

class GridHelper
{
    /**
     * @deprecated tag:v4.3 in favor of private static property $perPage
     */
    const PER_PAGE = 10;

    private static $perPage = 10;

    const ALL_FIELD_FILTER = '_all_';

    /**
     * @var GridRepository
     */
    private $gridRepository;

    /**
     * @internal
     * @var array
     */
    private $filterTypes = [
        'eq' => EqualsFilter::class,
        'like' => LikeFilter::class,
    ];

    /**
     * @var ValidationException
     */
    private $validationException;

    public function __construct(GridRepository $gridRepository)
    {
        $this->gridRepository = $gridRepository;
    }

    public function getGridState(
        Request $request,
        SearchStruct $struct,
        array $data,
        int $maxPage,
        int $currentPage = 1,
        int $total = 0
    ): array {
        $gridState = [
            'maxPage' => $maxPage,
            'currentPage' => $currentPage,
            'data' => $data,
            'total' => $total,
            'sortBy' => $request->getParam('sort-by'),
            'filters' => $request->getParam('filters-by'),
            'uriParams' => $this->getGridValues($request),
        ];

        if ($struct->searchTerm) {
            $gridState['searchTerm'] = $struct->searchTerm;
        }

        return $gridState;
    }

    /**
     * @param $struct
     */
    public function extractSearchDataInStoreFront(Request $request, SearchStruct $struct): void
    {
        $this->extractPage($request, $struct);
        $this->extractFilters($request, $struct);
        $this->setOrderBy($request, $struct);
    }

    public function extractSearchDataInBackend(Request $request, SearchStruct $struct): void
    {
        $this->extractLimitAndOffsetInBackend($request, $struct);
        $this->extractFiltersInBackend($request, $struct);
        $this->setOrderByInBackend($request, $struct);
    }

    public function extractSearchDataInAdmin(Request $request, SearchStruct $struct): void
    {
        $this->extractLimitAndOffsetInAdmin($request, $struct);
        $this->extractFiltersInAdmin($request, $struct);
        $this->setOrderByInAdmin($request, $struct);
    }

    public function extractSearchDataInRestApi(Request $request, SearchStruct $struct): void
    {
        $this->extractLimitAndOffset($request, $struct);
        $this->extractFilters($request, $struct);
    }

    public function getMaxPage(int $totalCount): int
    {
        return (int) ceil($totalCount / self::$perPage);
    }

    public function pushValidationException(ValidationException $violations): void
    {
        $this->validationException = $violations;
    }

    /**
     * @return ValidationException
     */
    public function popValidationException()
    {
        $violations = $this->validationException;
        $this->validationException = null;

        return $violations;
    }

    public function hasValidationException(): bool
    {
        return (bool) $this->validationException;
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function setOrderBy(Request $request, SearchStruct $struct): void
    {
        $orderBy = $request->getParam('sort-by', '');
        if (!$orderBy) {
            return;
        }

        $orderByExpression = explode('::', $orderBy);
        if (count($orderByExpression) !== 2) {
            throw new InvalidArgumentException('Incorrect order by argument');
        }

        $struct->orderBy = $orderByExpression[0];
        $struct->orderDirection = $orderByExpression[1];
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function setOrderByInBackend(Request $request, SearchStruct $struct): void
    {
        $orderBy = $request->getParam('sort', []);
        if (!$orderBy) {
            return;
        }

        $struct->orderBy = $orderBy[0]['property'];
        $struct->orderDirection = $orderBy[0]['direction'];
    }

    /**
     * @throws InvalidArgumentException
     * @internal
     */
    protected function setOrderByInAdmin(Request $request, SearchStruct $struct): void
    {
        $orderBy = $request->getParam('sort', []);
        if (!$orderBy) {
            return;
        }

        // SearchStruct only supports order by 1 field
        $firstOrderBy = reset($orderBy);
        if ($firstOrderBy['order'] === FieldSorting::DESCENDING) {
            $struct->orderDirection = 'DESC';
        }

        $struct->orderBy = $firstOrderBy['field'];
    }

    /**
     * @throws InvalidArgumentException
     * @throws DomainException
     * @internal
     */
    protected function extractFilters(Request $request, SearchStruct $struct): void
    {
        foreach ($request->getParam('filters', []) as $filterArray) {
            $fieldName = $this->extractFilterFieldName($filterArray);

            $type = $this->extractFilterType($filterArray);

            if ($this->hasFilterValue($filterArray)) {
                continue;
            }

            $value = $filterArray['value'];

            if ($fieldName === self::ALL_FIELD_FILTER) {
                $struct->searchTerm = $value;
                $struct->filters[] = $this->createAllFieldFilter($value);

                return;
            }

            $struct->filters[] = $this->createFilterClass($type, $fieldName, $value);
        }
    }

    /**
     * @throws InvalidArgumentException
     * @throws DomainException
     * @internal
     */
    protected function extractFiltersInBackend(Request $request, SearchStruct $struct): void
    {
        $filter = $request->getParam('filter', []);

        if (!$filter) {
            return;
        }

        if ($filter[0]['property'] !== 'search') {
            return;
        }

        $value = $filter[0]['value'];

        $struct->searchTerm = $value;
        $struct->filters[] = $this->createAllFieldFilter($value);
    }

    /**
     * @throws InvalidArgumentException
     * @throws DomainException
     * @internal
     */
    protected function extractFiltersInAdmin(Request $request, SearchStruct $struct): void
    {
        $struct->searchTerm = $request->getParam('term');
        if (!$struct->searchTerm) {
            return;
        }

        $struct->filters[] = $this->createAllFieldFilter($struct->searchTerm);
    }

    /**
     * @internal
     */
    protected function extractSearchTermInAdmin(Request $request, SearchStruct $struct): void
    {
        $term = $request->getParam('term', '');
        if (!$term) {
            return;
        }

        $struct->searchTerm = $term;
    }

    /**
     * @internal
     */
    protected function extractPage(Request $request, SearchStruct $struct): void
    {
        $currentPage = ((int) $request->getParam('page', 1)) - 1;

        $struct->offset = $currentPage * self::$perPage;
        $struct->limit = self::$perPage;
    }

    /**
     * @internal
     */
    protected function extractLimitAndOffset(Request $request, SearchStruct $struct): void
    {
        $struct->offset = (int) $request->getParam('offset');
        $struct->limit = (int) $request->getParam('limit');
    }

    /**
     * @internal
     */
    protected function extractLimitAndOffsetInBackend(Request $request, SearchStruct $struct): void
    {
        $struct->offset = (int) $request->getParam('start');
        $struct->limit = (int) $request->getParam('limit');
    }

    /**
     * @internal
     */
    protected function extractLimitAndOffsetInAdmin(Request $request, SearchStruct $struct): void
    {
        $struct->limit = (int) $request->getParam('limit');
        $struct->offset = (((int) $request->getParam('page', 1)) - 1) * $struct->limit;
    }

    /**
     * @internal
     */
    protected function getGridValues(Request $request): array
    {
        $possibleKeys = [
            'sort-by',
            'filters',
            'page',
        ];

        $data = [];
        foreach ($possibleKeys as $key) {
            $value = $request->getParam($key);

            if (!$value) {
                continue;
            }

            $data[$key] = $value;
        }

        return $this->explodePaths($data);
    }

    public function explodePaths(array $multiDimensionalArray, array $pathParts = []): array
    {
        $result = [];
        foreach ($multiDimensionalArray as $key => $value) {
            $currentPathParts = array_merge($pathParts, [$key]);

            if (is_array($value)) {
                $explodedPaths = $this->explodePaths($value, $currentPathParts);

                foreach ($explodedPaths as $explodedKey => $explodedValue) {
                    $result[$explodedKey] = $explodedValue;
                }
                continue;
            }

            $pathName = $currentPathParts[0];
            $countCurrentPathParts = count($currentPathParts);
            for ($i = 1; $i < $countCurrentPathParts; $i++) {
                $pathName .= '[' . $currentPathParts[$i] . ']';
            }

            $result[$pathName] = $value;
        }

        return $result;
    }

    public function getCurrentPage(Request $request): int
    {
        return max((int) $request->getParam('page', 1), 1);
    }

    /**
     * @throws InvalidArgumentException
     * @internal
     */
    protected function extractFilterFieldName(array $filterArray): string
    {
        if (!array_key_exists('field-name', $filterArray)) {
            throw new InvalidArgumentException('Missing required filter parameter "field-name"');
        }

        return $filterArray['field-name'];
    }

    /**
     * @throws DomainException
     * @throws InvalidArgumentException
     * @internal
     */
    protected function extractFilterType(array $filterArray): string
    {
        if (!array_key_exists('type', $filterArray)) {
            throw new InvalidArgumentException('Missing required filter parameter "type"');
        }
        $type = $filterArray['type'];

        if (!array_key_exists($type, $this->filterTypes)) {
            throw new DomainException(sprintf('Invalid required filter type "%s"', $type));
        }

        return $type;
    }

    /**
     * @internal
     */
    protected function hasFilterValue(array $filterArray): bool
    {
        return !array_key_exists('value', $filterArray)
            || $filterArray['value'] === null
            || $filterArray['value'] === '';
    }

    /**
     * @internal
     */
    protected function createFilterClass(string $type, string $fieldName, $value): Filter
    {
        $filterClass = $this->filterTypes[$type];

        return new $filterClass($this->gridRepository->getMainTableAlias(), $fieldName, $value);
    }

    /**
     * @internal
     */
    protected function createAllFieldFilter(string $value): Filter
    {
        $likeFilters = array_merge(
            $this->getFullTextSearchFiledLikeFilters($value),
            $this->getFullTextSearchAdditionalResourcesLikeFilters($value)
        );

        return new OrFilter($likeFilters);
    }

    /**
     * @internal
     */
    protected function getFullTextSearchFiledLikeFilters(string $value): array
    {
        $likeFilters = [];
        foreach ($this->gridRepository->getFullTextSearchFields() as $searchField) {
            $likeFilters[] = new LikeFilter($this->gridRepository->getMainTableAlias(), $searchField, $value);
        }

        return $likeFilters;
    }

    /**
     * @internal
     */
    protected function getFullTextSearchAdditionalResourcesLikeFilters(string $value): array
    {
        $likeFilters = [];

        foreach ($this->gridRepository->getAdditionalSearchResourceAndFields() as $alias => $searchFields) {
            foreach ($searchFields as $tableAlias => $searchField) {
                if (!is_array($searchField)) {
                    $likeFilters[] = new FilterSubQueryWithLike($tableAlias, $alias, $searchField, $value);
                    continue;
                }

                foreach ($searchField as $field) {
                    $likeFilters[] = new FilterSubQueryWithLike($tableAlias, $alias, $field, $value);
                }
            }
        }

        return $likeFilters;
    }

    /**
     * @return array view response
     */
    public function getValidationResponse(string $propertyName): array
    {
        if (!$this->hasValidationException()) {
            return [];
        }

        $validationException = $this->popValidationException();

        $errors = [];

        foreach ($validationException->getViolations() as $violation) {
            $errors[] = [
                'property' => ucfirst($violation->getPropertyPath()),
                'snippetKey' => str_replace(['}', '{', '%', '.', ' '], '', $violation->getMessageTemplate()),
                'messageTemplate' => $violation->getMessageTemplate(),
                'parameters' => $violation->getParameters(),
                'cause' => $violation->getCause(),
            ];
        }

        return [
            $propertyName => $validationException->getEntity(),
            'errors' => $errors,
        ];
    }
}
