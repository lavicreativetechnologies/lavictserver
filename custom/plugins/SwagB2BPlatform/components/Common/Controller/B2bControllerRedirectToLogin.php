<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Controller;

use Exception;

class B2bControllerRedirectToLogin extends B2bControllerRedirectException implements B2bControllerRouteNameProvider
{
    public function __construct(array $params = [], $code = 0, Exception $previous = null)
    {
        parent::__construct('index', 'login', $params, $code, $previous);
    }

    public function getRouteName(): string
    {
        return 'frontend.account.home.page';
    }
}
