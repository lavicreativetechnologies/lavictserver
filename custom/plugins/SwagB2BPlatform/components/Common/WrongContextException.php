<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

use DomainException;

class WrongContextException extends DomainException implements B2BException
{
}
