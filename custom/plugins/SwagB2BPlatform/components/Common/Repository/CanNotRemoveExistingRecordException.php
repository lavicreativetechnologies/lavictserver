<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Repository;

use DomainException;
use Shopware\B2B\Common\B2BException;

class CanNotRemoveExistingRecordException extends DomainException implements B2BException
{
}
