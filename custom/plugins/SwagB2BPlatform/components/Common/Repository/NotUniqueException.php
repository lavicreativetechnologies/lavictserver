<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Repository;

use InvalidArgumentException;
use Shopware\B2B\Common\B2BException;

class NotUniqueException extends InvalidArgumentException implements B2BException
{
}
