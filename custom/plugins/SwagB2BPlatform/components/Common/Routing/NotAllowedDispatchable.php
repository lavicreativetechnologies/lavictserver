<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Routing;

use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\Components\Api\Exception\NotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NotAllowedDispatchable implements Dispatchable
{
    /**
     * @var string
     */
    private $query;

    public function __construct(string $query)
    {
        $this->query = $query;
    }

    /**
     * @throws NotFoundException|NotFoundHttpException
     */
    public function dispatch(Request $request): void
    {
        if (ShopwareVersion::isClassic()) {
            throw new NotFoundException($this->query);
        }
        throw new NotFoundHttpException();
    }
}
