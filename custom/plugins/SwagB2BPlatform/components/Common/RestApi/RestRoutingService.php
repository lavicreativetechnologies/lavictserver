<?php declare(strict_types=1);

namespace Shopware\B2B\Common\RestApi;

use InvalidArgumentException;
use Shopware\B2B\Common\Routing\Dispatchable;
use Shopware\B2B\Common\Routing\Router;
use function mb_strlen;
use function mb_strpos;
use function mb_substr;
use function sprintf;

class RestRoutingService
{
    const PATH_INFO_PREFIX = '/api/b2b';

    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function getDispatchable(string $method, string $pathInfo): Dispatchable
    {
        $subQuery = $this->extractSubRouteQueryString($pathInfo);

        return $this->router->match($method, $subQuery);
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function extractSubRouteQueryString(string $queryString): string
    {
        if (0 !== mb_strpos($queryString, self::PATH_INFO_PREFIX)) {
            throw new InvalidArgumentException(
                sprintf('Trying to create dispatchable with invalid query string prefix, "%s"', $queryString)
            );
        }

        return mb_substr($queryString, mb_strlen(self::PATH_INFO_PREFIX));
    }
}
