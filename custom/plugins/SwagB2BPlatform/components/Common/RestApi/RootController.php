<?php declare(strict_types=1);

namespace Shopware\B2B\Common\RestApi;

use Shopware\B2B\Common\MvcExtension\MvcEnvironmentInterface;
use Shopware\B2B\Common\Routing\Router;
use function ksort;

class RootController
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var MvcEnvironmentInterface
     */
    private $environment;

    public function __construct(
        Router $router,
        MvcEnvironmentInterface $environment
    ) {
        $this->router = $router;
        $this->environment = $environment;
    }

    /**
     * @return string[]
     */
    public function indexAction(): array
    {
        $return = [];
        $routes = $this->router->getRoutes();

        $apiBaseUrl = $this->environment->getPathinfo()
            . RestRoutingService::PATH_INFO_PREFIX;

        /** @var \Shopware\B2B\Common\Routing\Route $route */
        foreach ($routes as $route) {
            $query = $route->getQuery();

            $return[RestRoutingService::PATH_INFO_PREFIX . $query][] = [
                'method' => $route->getMethod(),
                'url' => $apiBaseUrl . $query,
                'params' => $route->getParamOrder(),
            ];
        }
        ksort($return);

        return $return;
    }
}
