<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

use JsonSerializable;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\Core\Framework\Uuid\Exception\InvalidUuidException;
use Shopware\Core\Framework\Uuid\Exception\InvalidUuidLengthException;
use Shopware\Core\Framework\Uuid\Uuid;
use function is_int;
use function mb_strtolower;

abstract class IdValue implements JsonSerializable, ValueInterface
{
    abstract public function getStorageValue();

    abstract public function getValue();

    abstract public function equals(IdValue $value): bool;

    public static function create($value): IdValue
    {
        if ($value === null || $value === '' || $value === false) {
            return new NullIdValue();
        }

        if ($value instanceof IdValue) {
            return $value;
        }

        if (ShopwareVersion::isClassic()) {
            return self::createInClassic($value);
        }

        return self::createInPlatform($value);
    }

    /**
     * @return IdValue[]
     */
    public static function fromHexToBytesList(array $values): array
    {
        return Uuid::fromHexToBytesList($values);
    }

    /**
     * @return Idvalue[]
     */
    public static function createMultiple(array $values): array
    {
        $idValues = [];

        foreach ($values as $value) {
            $idValues[] = self::create($value);
        }

        return $idValues;
    }

    public static function null(): IdValue
    {
        return new NullIdValue();
    }

    /**
     * @internal
     */
    protected static function createInClassic($value): IdValue
    {
        return new IntIdValue((int) $value);
    }

    /**
     * @internal
     */
    protected static function createInPlatform($value): IdValue
    {
        if (is_int($value)) {
            return new IntIdValue((int) $value);
        }

        try {
            $lowerValue = mb_strtolower((string) $value);
            if (Uuid::isValid($lowerValue) || Uuid::isValid(Uuid::fromBytesToHex((string) $value))) {
                return self::createUuidValue((string) $value);
            }
        } catch (InvalidUuidLengthException | InvalidUuidException $exception) {
            // not a valid uuid
        }

        return new IntIdValue((int) $value);
    }

    /**
     * @internal
     */
    protected static function createUuidValue(string $value): UuidIdValue
    {
        if (Uuid::isValid($value)) {
            return new UuidIdValue(mb_strtolower($value));
        }

        return new UuidIdValue(Uuid::fromBytesToHex($value));
    }
}
