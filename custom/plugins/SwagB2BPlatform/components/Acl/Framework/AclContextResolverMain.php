<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;

/**
 * A default implementation for main contexts.
 */
abstract class AclContextResolverMain extends AclContextResolver
{
    public function getQuery(string $aclTableName, IdValue $contextId, QueryBuilder $queryBuilder): AclQuery
    {
        $mainPrefix = $this->getNextPrefix();

        $queryBuilder
            ->select($mainPrefix . '.*')
            ->from($aclTableName, $mainPrefix)
            ->where($mainPrefix . '.entity_id = :p_' . $mainPrefix)
            ->setParameter('p_' . $mainPrefix, $contextId->getStorageValue());

        return (new AclQuery())->fromQueryBuilder($queryBuilder);
    }

    public function isMainContext(): bool
    {
        return true;
    }
}
