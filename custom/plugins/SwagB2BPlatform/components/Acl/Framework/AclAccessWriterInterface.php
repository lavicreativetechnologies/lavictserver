<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface AclAccessWriterInterface
{
    public function addNewSubject(OwnershipContext $context, AclGrantContext $grantContext, IdValue $subjectId, bool $grantable);

    public function testUpdateAllowed(OwnershipContext $ownershipContext, IdValue $subjectId);
}
