<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;

/**
 * Connects ACLTable data and ACLResolver data to a single useful context object.
 */
class AclTableResolverFacade
{
    /**
     * @var string
     */
    private $tableName;

    /**
     * @var object
     */
    private $context;

    /**
     * @var AclContextResolver
     */
    private $contextResolver;

    /**
     * @param object $context
     */
    public function __construct(AclContextResolver $contextResolver, string $tableName, $context)
    {
        $this->tableName = $tableName;
        $this->context = $context;
        $this->contextResolver = $contextResolver;
    }

    /**
     * @throws \Shopware\B2B\Acl\Framework\AclUnsupportedContextException
     */
    public function getId(): IdValue
    {
        return $this->contextResolver
            ->extractId($this->context);
    }

    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @throws \Shopware\B2B\Acl\Framework\AclUnsupportedContextException
     */
    public function getQuery(QueryBuilder $queryBuilder): AclQuery
    {
        return $this->contextResolver
            ->getQuery($this->tableName, $this->getId(), $queryBuilder);
    }
}
