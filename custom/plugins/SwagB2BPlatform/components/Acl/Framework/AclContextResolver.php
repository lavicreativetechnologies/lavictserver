<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;

/**
 * A Context is a valid entity that is either directly (SQL: WHERE) or indirectly (SQL INNER JOIN) a filter to create a ACL-Table subset.
 *
 * These are determined and created through context resolver implementations.
 */
abstract class AclContextResolver
{
    /**
     * @var int
     */
    protected static $counter = 0;

    protected function getCurrentPrefix(): string
    {
        return 'acl_' . self::$counter;
    }

    protected function getNextPrefix(): string
    {
        self::$counter++;

        return $this->getCurrentPrefix();
    }

    abstract public function getQuery(string $aclTableName, IdValue $contextId, QueryBuilder $queryContext): AclQuery;

    /**
     * @param object $context
     */
    abstract public function extractId($context): IdValue;

    abstract public function isMainContext(): bool;
}
