<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class AclAccessWriter implements AclAccessWriterInterface
{
    /**
     * @var AclRepository
     */
    private $aclRepository;

    public function __construct(AclRepository $aclRepository)
    {
        $this->aclRepository = $aclRepository;
    }

    public function addNewSubject(OwnershipContext $context, AclGrantContext $grantContext, IdValue $subjectId, bool $grantable): void
    {
        try {
            $this->aclRepository->allow($context, $subjectId, true);
        } catch (AclUnsupportedContextException $e) {
            //nth
        }

        $this->aclRepository->allow($grantContext->getEntity(), $subjectId, $grantable);
    }

    /**
     * @throws AclOperationNotPermittedException
     */
    public function testUpdateAllowed(OwnershipContext $ownershipContext, IdValue $subjectId): void
    {
        try {
            if ($this->aclRepository->isAllowed($ownershipContext, $subjectId)) {
                return;
            }
        } catch (AclUnsupportedContextException $e) {
            return;
        }

        throw new AclOperationNotPermittedException('Update not allowed');
    }
}
