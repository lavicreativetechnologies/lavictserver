<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\BridgePlatform\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AclRouteBridgeConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [];
    }
}
