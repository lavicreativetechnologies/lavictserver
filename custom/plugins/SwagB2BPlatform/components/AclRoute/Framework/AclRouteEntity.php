<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;

class AclRouteEntity implements Entity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $resource_name;

    /**
     * @var string
     */
    public $privilege_type;

    /**
     * @return AclRouteEntity
     */
    public function fromDatabaseArray(array $data): self
    {
        $this->id = IdValue::create($data['id']);
        $this->resource_name = (string) $data['resource_name'];
        $this->privilege_type = (string) $data['privilege_type'];

        return $this;
    }
}
