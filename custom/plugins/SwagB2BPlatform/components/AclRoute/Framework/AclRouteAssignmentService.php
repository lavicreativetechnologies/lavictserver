<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Framework;

use Shopware\B2B\Acl\Framework\AclOperationNotPermittedException;
use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Acl\Framework\AclUnsupportedContextException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_diff;
use function array_keys;

class AclRouteAssignmentService
{
    /**
     * @var AclRepository
     */
    private $aclRepository;

    /**
     * @var AclRouteRepository
     */
    private $aclRouteRepository;

    /**
     * @var array
     */
    private $routeMapping;

    public function __construct(
        AclRepository $aclRepository,
        AclRouteRepository $aclRouteRepository,
        array $routeMapping
    ) {
        $this->aclRepository = $aclRepository;
        $this->aclRouteRepository = $aclRouteRepository;
        $this->routeMapping = $routeMapping;
    }

    /**
     * @param object $context
     */
    public function allow(OwnershipContext $ownershipContext, $context, IdValue $subjectId, bool $grantable = false): void
    {
        $this->testIsGrantable($ownershipContext, $subjectId);
        $this->aclRepository->allow($context, $subjectId, $grantable);

        try {
            $mappedRouteIds = $this->aclRouteRepository->fetchMappedRouteIds($this->routeMapping, $subjectId);
        } catch (NotFoundException $e) {
            return;
        }

        $mappedRouteIds = $this->filterAlreadyAssigned($context, $mappedRouteIds);

        $this->aclRepository->allowAll($context, $mappedRouteIds);
    }

    /**
     * @param object $context
     */
    public function deny(OwnershipContext $ownershipContext, $context, IdValue $subjectId): void
    {
        $this->testIsGrantable($ownershipContext, $subjectId);
        $this->aclRepository->deny($context, $subjectId);
    }

    /**
     * @param object $context
     */
    public function allowComponent(OwnershipContext $ownershipContext, $context, string $componentName, bool $grantable): void
    {
        $subjectIds = $this->aclRouteRepository
            ->fetchActionIdsByControllerName($ownershipContext, $componentName);

        $this->allowAllSubjectIdsIfAllowed($ownershipContext, $context, $subjectIds, $grantable);
    }

    /**
     * @param object $context
     */
    public function denyComponent(OwnershipContext $ownershipContext, $context, string $componentName): void
    {
        $subjectIds = $this->aclRouteRepository
            ->fetchActionIdsByControllerName($ownershipContext, $componentName);

        $this->denyAllSubjectIdsIfAllowed($ownershipContext, $context, $subjectIds);
    }

    /**
     * @param object $context
     */
    public function allowAll(OwnershipContext $ownershipContext, $context, bool $grantable = false): void
    {
        $subjectIds = $this->aclRouteRepository
            ->fetchAllActionIds($ownershipContext);

        $this->allowAllSubjectIdsIfAllowed($ownershipContext, $context, $subjectIds, $grantable);
    }

    public function denyAll(OwnershipContext $ownershipContext, $context): void
    {
        $subjectIds = $this->aclRouteRepository
            ->fetchAllActionIds($ownershipContext);

        $this->denyAllSubjectIdsIfAllowed($ownershipContext, $context, $subjectIds);
    }

    /**
     * @internal
     * @param object $context
     */
    protected function denyAllSubjectIdsIfAllowed(OwnershipContext $ownershipContext, $context, array $subjectIds): void
    {
        $this->testSubjectIdsGrantable($ownershipContext, $subjectIds);
        $this->aclRepository->denyAll($context, $subjectIds);
    }

    /**
     * @internal
     * @param object $context
     */
    protected function allowAllSubjectIdsIfAllowed(OwnershipContext $ownershipContext, $context, array $subjectIds, bool $grantable): void
    {
        $this->testSubjectIdsGrantable($ownershipContext, $subjectIds);
        $this->aclRepository->allowAll($context, $subjectIds, $grantable);
    }

    protected function testSubjectIdsGrantable(OwnershipContext $ownershipContext, array $subjectIds): void
    {
        foreach ($subjectIds as $subjectId) {
            $this->testIsGrantable($ownershipContext, $subjectId);
        }
    }

    /**
     * @param int|string $subjectId
     * @throws AclOperationNotPermittedException
     */
    protected function testIsGrantable(OwnershipContext $ownershipContext, $subjectId): void
    {
        try {
            $isGrantable = $this->aclRepository->isGrantable($ownershipContext, $subjectId);

            if (!$isGrantable) {
                throw new AclOperationNotPermittedException($ownershipContext->identityClassName . '::' . $ownershipContext->authId . ' is not allowed to assign ' . $subjectId);
            }
        } catch (AclUnsupportedContextException $e) {
            //nth
        }
    }

    /**
     * @param object $context
     * @return int[]
     */
    public function filterAlreadyAssigned($context, array $mappedRouteIds): array
    {
        $alreadyAssignedIds = array_keys($this->aclRepository->fetchAllDirectlyIds($context));
        $mappedRouteIds = array_diff($mappedRouteIds, $alreadyAssignedIds);

        return $mappedRouteIds;
    }
}
