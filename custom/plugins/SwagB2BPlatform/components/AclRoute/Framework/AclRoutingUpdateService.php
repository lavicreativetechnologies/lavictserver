<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use function mb_strtolower;

/**
 * Updater class used during installation and updates, therefore very light on dependencies
 */
class AclRoutingUpdateService
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function addConfig(array $config): void
    {
        foreach ($config as $resourceName => $controllers) {
            foreach ($controllers as $controllerName => $actions) {
                foreach ($actions as $actionName => $privilegeName) {
                    if ($privilegeName !== 'free') {
                        $this->setAclRoute($controllerName, $actionName, $resourceName, $privilegeName);
                    }
                }
            }
        }
    }

    /**
     * @internal
     */
    protected function setAclRoute(string $controller, string $action, string $resourceName, string $privilegeName): void
    {
        $controller = mb_strtolower($controller);
        $action = mb_strtolower($action);

        try {
            $this->connection->insert(
                'b2b_acl_route_privilege',
                [
                    'resource_name' => $resourceName,
                    'privilege_type' => $privilegeName,
                ]
            );
        } catch (DBALException $e) {
            //nth
        }

        $privilegeId = $this->connection->fetchColumn(
            'SELECT id FROM b2b_acl_route_privilege 
             WHERE resource_name = :resourceName AND privilege_type = :privilegeType',
            [
                'resourceName' => $resourceName,
                'privilegeType' => $privilegeName,
            ]
        );

        try {
            $this->connection->insert(
                'b2b_acl_route',
                [
                    'privilege_id' => $privilegeId,
                    'controller' => $controller,
                    'action' => $action,
                ]
            );
        } catch (DBALException $e) {
            //nth
        }

        $this->connection->update(
            'b2b_acl_route',
            [
                'privilege_id' => $privilegeId,
            ],
            [
                'controller' => $controller,
                'action' => $action,
            ]
        );

        $this->connection->exec(
            'DELETE p FROM b2b_acl_route_privilege p
             LEFT JOIN b2b_acl_route r ON r.privilege_id = p.id
             WHERE r.id IS NULL'
        );
    }
}
