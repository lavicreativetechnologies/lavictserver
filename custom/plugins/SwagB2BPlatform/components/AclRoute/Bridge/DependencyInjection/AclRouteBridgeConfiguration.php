<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Bridge\DependencyInjection;

use Shopware\B2B\AclRoute\BridgePlatform\DependencyInjection\AclRouteBridgeConfiguration as PlatformAclRouteBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AclRouteBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformAclRouteBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [];
    }
}
