<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Api;

use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class AssignmentContactController extends ApiAssignmentController
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(
        ContactRepository $contactRepository,
        AclRepository $aclRepository,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        parent::__construct($aclRepository, $authStorageAdapter);
        $this->contactRepository = $contactRepository;
    }

    public function getAllGrantAction(string $contactMail): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->getAllGrant($contactEntity);
    }

    public function getAllAllowedAction(string $contactMail): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->getAllAllowed($contactEntity);
    }

    public function getAllowedAction(string $contactMail, string $subjectId): array
    {
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->getAllowed($contactEntity, $subjectId);
    }

    public function allowAction(string $contactMail, string $subjectId): array
    {
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->allow($contactEntity, $subjectId, false);
    }

    public function allowGrantAction(string $contactMail, string $subjectId): array
    {
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->allow($contactEntity, $subjectId, true);
    }

    public function multipleAllowAction(string $contactMail, Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->multipleAllow($contactEntity, $request->getPost());
    }

    public function multipleDenyAction(string $contactMail, Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->multipleDeny($contactEntity, $request->getPost());
    }

    public function denyAction(string $contactMail, string $subjectId): array
    {
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contactEntity = $this->getContactIdentityByEmail($contactMail, $ownershipContext);

        return $this->deny($contactEntity, $subjectId);
    }

    /**
     * @internal
     */
    protected function getContactIdentityByEmail(string $contactMail, OwnershipContext $ownershipContext): ContactEntity
    {
        return $this->contactRepository->fetchOneByEmail($contactMail, $ownershipContext);
    }
}
