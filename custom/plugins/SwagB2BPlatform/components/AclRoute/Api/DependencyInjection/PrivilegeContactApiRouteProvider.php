<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class PrivilegeContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}',
                'b2b_assignment.api_privilege_contact_controller',
                'getAllAllowed',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/grant',
                'b2b_assignment.api_privilege_contact_controller',
                'getAllGrant',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/privilege/{privilegeId}',
                'b2b_assignment.api_privilege_contact_controller',
                'getAllowed',
                ['contactEmail', 'privilegeId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/privilege/{privilegeId}',
                'b2b_assignment.api_privilege_contact_controller',
                'allow',
                ['contactEmail', 'privilegeId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/privilege/{privilegeId}/grant',
                'b2b_assignment.api_privilege_contact_controller',
                'allowGrant',
                ['contactEmail', 'privilegeId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/allow',
                'b2b_assignment.api_privilege_contact_controller',
                'multipleAllow',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/deny',
                'b2b_assignment.api_privilege_contact_controller',
                'multipleDeny',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/privilege_contact/{contactEmail}/privilege/{privilegeId}',
                'b2b_assignment.api_privilege_contact_controller',
                'deny',
                ['contactEmail', 'privilegeId'],
            ],
        ];
    }
}
