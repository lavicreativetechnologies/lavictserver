<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Api;

use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function count;

abstract class ApiAssignmentController
{
    /**
     * @var AclRepository
     */
    private $aclRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        AclRepository $aclRepository,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->aclRepository = $aclRepository;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    /**
     * @param ContactEntity|RoleEntity $context
     */
    public function getAllGrant($context): array
    {
        $subjects = $this->aclRepository->fetchAllGrantableIds($context);

        return ['success' => true, 'allowed' => $subjects, 'totalCount' => count($subjects)];
    }

    /**
     * @param ContactEntity|RoleEntity $context
     * @return array
     */
    public function getAllAllowed($context)
    {
        $subjects = $this->aclRepository->getAllAllowedIds($context);

        return ['success' => true, 'allowed' => $subjects, 'totalCount' => count($subjects)];
    }

    /**
     * @param ContactEntity|RoleEntity $context
     * @return array
     */
    public function getAllowed($context, IdValue $subjectId)
    {
        $allowed = $this->aclRepository->isAllowed($context, $subjectId);
        $grantable = $this->aclRepository->isGrantable($context, $subjectId);

        return ['success' => true, 'allowed' => $allowed, 'grantable' => $grantable];
    }

    /**
     * @param ContactEntity|RoleEntity $context
     * @return array
     */
    public function allow($context, IdValue $subjectId, bool $grant)
    {
        $this->aclRepository->allow($context, $subjectId, $grant);

        return ['success' => true];
    }

    /**
     * @param ContactEntity|RoleEntity $context
     * @return array
     */
    public function multipleAllow($context, array $subjects)
    {
        $this->aclRepository->allowAll($context, IdValue::createMultiple($subjects));

        return ['success' => true, 'allowed' => $subjects];
    }

    /**
     * @param ContactEntity|RoleEntity $context
     * @return array
     */
    public function multipleDeny($context, array $subjects)
    {
        $this->aclRepository->denyAll($context, IdValue::createMultiple($subjects));

        return ['success' => true, 'denied' => $subjects];
    }

    /**
     * @param ContactEntity|RoleEntity $context
     * @return array
     */
    public function deny($context, IdValue $contingentId)
    {
        $this->aclRepository->deny($context, $contingentId);

        return ['success' => true];
    }

    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()
            ->getOwnershipContext();
    }
}
