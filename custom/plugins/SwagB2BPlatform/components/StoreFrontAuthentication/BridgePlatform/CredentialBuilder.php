<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserRepositoryInterface;

class CredentialBuilder implements CredentialsBuilderInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function createCredentials(array $parameters): AbstractCredentialsEntity
    {
        $entity = new CredentialsEntity();

        $entity->email = $parameters['email'];

        return $entity;
    }

    public function createCredentialsByEmail(string $email = null): AbstractCredentialsEntity
    {
        $entity = new CredentialsEntity();

        $entity->email = $email;

        return $entity;
    }

    public function createCredentialsByUserId(IdValue $userId): AbstractCredentialsEntity
    {
        $user = $this->userRepository->fetchOneById($userId);

        $entity = new CredentialsEntity();
        $entity->email = $user['email'];

        return $entity;
    }
}
