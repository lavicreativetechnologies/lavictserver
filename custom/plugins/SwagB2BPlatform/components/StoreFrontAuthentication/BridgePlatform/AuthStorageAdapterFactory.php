<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\Core\PlatformRequest;

class AuthStorageAdapterFactory
{
    /**
     * @var RequestTransformerDecorator
     */
    private $requestTransformerDecorator;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $apiAuthStorageAdapter;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $shopSessionAuthStorageAdapter;

    public function __construct(
        RequestTransformerDecorator $requestTransformerDecorator,
        AuthStorageAdapterInterface $apiAuthStorageAdapter,
        AuthStorageAdapterInterface $sessionAuthStorageAdapter
    ) {
        $this->requestTransformerDecorator = $requestTransformerDecorator;
        $this->apiAuthStorageAdapter = $apiAuthStorageAdapter;
        $this->shopSessionAuthStorageAdapter = $sessionAuthStorageAdapter;
    }

    public function factory(): AuthStorageAdapterInterface
    {
        if ($this->isSalesChannelRequest()) {
            return $this->shopSessionAuthStorageAdapter;
        }

        return $this->apiAuthStorageAdapter;
    }

    /**
     * @internal
     */
    protected function isSalesChannelRequest(): bool
    {
        $request = $this->requestTransformerDecorator->getInitialRequest();

        if (!$request) {
            return false;
        }

        return $request
            ->attributes
            ->has(PlatformRequest::ATTRIBUTE_SALES_CHANNEL_ID);
    }
}
