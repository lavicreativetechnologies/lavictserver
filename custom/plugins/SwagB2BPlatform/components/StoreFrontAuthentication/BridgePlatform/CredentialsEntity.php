<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;

class CredentialsEntity extends AbstractCredentialsEntity
{
    /**
     * @var IdValue
     */
    public $salesChannelId;

    /**
     * @var bool
     */
    public $customerScope;
}
