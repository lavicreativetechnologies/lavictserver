<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\QuerySubShopExtenderInterface;

class QuerySubShopExtender implements QuerySubShopExtenderInterface
{
    public function __construct()
    {
    }

    public function extendQuery(
        QueryBuilder $query,
        AbstractCredentialsEntity $credentialsEntity,
        string $authAlias
    ): void {
        // @todo implement
    }
}
