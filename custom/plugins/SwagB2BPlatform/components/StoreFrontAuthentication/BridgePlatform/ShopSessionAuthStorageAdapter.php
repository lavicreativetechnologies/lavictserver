<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use RuntimeException;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\NoIdentitySetException;
use Shopware\Core\Checkout\Customer\Event\CustomerLogoutEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ShopSessionAuthStorageAdapter implements AuthStorageAdapterInterface, EventSubscriberInterface
{
    private const IDENTITY_KEY = 'b2b_front_auth_identity';

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(RequestStack $requestStack, ContextProvider $channelContextProvider)
    {
        $this->requestStack = $requestStack;
        $this->contextProvider = $channelContextProvider;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CustomerLogoutEvent::class => 'unsetIdentity',
        ];
    }

    public function unsetIdentity(): void
    {
        $session = $this->getSession();
        $session->remove(self::IDENTITY_KEY);
        $session->save();
    }

    public function setIdentity(Identity $identity): void
    {
        $session = $this->getSession();
        $session->set(self::IDENTITY_KEY, $identity);
    }

    public function getIdentity(): Identity
    {
        $session = $this->getSession();

        if (!$session->has(self::IDENTITY_KEY)) {
            throw new NoIdentitySetException('Session does not have a stored identity');
        }

        return $session->get(self::IDENTITY_KEY);
    }

    public function isAuthenticated(): bool
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        return (bool) $salesChannelContext->getCustomer();
    }

    /**
     * @internal
     */
    protected function getSession(): SessionInterface
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        if (!$masterRequest) {
            // ToDo fix this by a different implementation and a factory like in sw5
            // happens if we are in cli context for example
            throw new RuntimeException('Not in RequestContext.');
        }

        return $masterRequest->getSession();
    }
}
