<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\MysqlRepository;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\BridgePlatform\ContactPasswordProvider;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialQueryExtenderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserRepositoryInterface;
use Shopware\Core\Framework\Uuid\Uuid;
use function array_merge;
use function date;
use function sprintf;

class UserRepository implements UserRepositoryInterface
{
    const TABLE_NAME = 'customer';

    const TABLE_ALIAS = 'customer';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var CredentialQueryExtenderInterface
     */
    private $credentialQueryExtender;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        Connection $connection,
        CredentialQueryExtenderInterface $credentialQueryExtender,
        ContextProvider $contextProvider
    ) {
        $this->connection = $connection;
        $this->credentialQueryExtender = $credentialQueryExtender;
        $this->contextProvider = $contextProvider;
    }

    public function syncUser(array $userData): IdValue
    {
        $check = $this->connection->fetchColumn(
            'SELECT id FROM ' . self::TABLE_NAME . ' WHERE email LIKE :email and sales_channel_id = :salesChannelId LIMIT 1',
            [
                'email' => $userData['email'],
                'salesChannelId' => $userData['sales_channel_id'],
            ]
        );

        if (!$check) {
            $userData['id'] = IdValue::create(Uuid::randomHex());
            $userData['first_login'] = date('Y-m-d');
            $userData['created_at'] = date('Y-m-d');

            $this->connection->insert(
                self::TABLE_NAME,
                $this->toStorageValues($userData)
            );

            return $userData['id'];
        }

        $check = IdValue::create($check);

        $this->connection->update(
            self::TABLE_NAME,
            $this->toStorageValues($userData),
            ['id' => $check->getStorageValue()]
        );

        return $check;
    }

    public function fetchOneById(IdValue $id): array
    {
        $query = $this->connection
            ->createQueryBuilder()->select(['c.password',
                       'c.email',
                       'c.active',
                       'c.language_id AS language',
                       'default_billing_address_id AS default_billing_address_id',
                       'default_shipping_address_id AS default_shipping_address_id',
                       's.salutation_key AS salutation',
                       'c.title',
                       'c.first_name',
                       'c.last_name',
                       'c.first_login',
                       'c.last_login', ])
            ->from('customer', 'c')
            ->leftJoin('c', 'salutation', 's', 'c.salutation_id = s.id')
            ->where('c.id = :id')
            ->setParameter('id', $id->getStorageValue());

        $customerData = $query->execute()->fetch(FetchMode::ASSOCIATIVE);

        if (!$customerData) {
            throw new NotFoundException(sprintf('Customer not found for %s', $id->getValue()));
        }

        return [
            'password' => $customerData['password'],
            'encoder' => ContactPasswordProvider::ENCODER_NAME,
            'email' => $customerData['email'],
            'active' => (bool) $customerData['active'],
            'language' => IdValue::create($customerData['language']),
            'title' => $customerData['title'],
            'salutation' => $customerData['salutation'],
            'firstname' => $customerData['first_name'],
            'lastname' => $customerData['last_name'],
            'default_billing_address_id' => IdValue::create($customerData['default_billing_address_id']),
            'default_shipping_address_id' => IdValue::create($customerData['default_shipping_address_id']),
            'first_login' => $customerData['first_login'],
        ];
    }

    public function checkAddress(IdValue $addressId, string $type, OwnershipContext $context): void
    {
        $addressIdent = $this->getAddressIdentification($addressId);

        if ($addressIdent['b2b_type'] === $type && IdValue::create($addressIdent['customer_id'])->equals($context->shopOwnerUserId)) {
            return;
        }

        if (!$addressIdent['b2b_type']) {
            $this->updateAttributes($addressId, $type);

            return;
        }

        if ($addressIdent['b2b_type'] !== $type) {
            $newAddress = $this->findAddress($context->shopOwnerUserId, $type);

            if (!$newAddress) {
                $newAddress['address_id'] = $this->duplicateAddress(
                    $this->getAddress($addressId),
                    $type
                );
            }

            $this->connection->update(
                'customer',
                ['default_' . $type . '_address_id' => IdValue::create($newAddress['address_id'])->getStorageValue()],
                ['id' => $context->shopOwnerUserId->getStorageValue()]
            );
        }
    }

    /**
     * @internal
     */
    protected function getAddressIdentification(IdValue $id): array
    {
        return $this->getBaseQuery()
            ->select('address.customer_id, data.b2b_type')
            ->where('address.id = :id')
            ->setParameter('id', $id->getStorageValue())
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @internal
     */
    protected function getAddress(IdValue $id): array
    {
        return $this->getBaseQuery()
            ->select('address.*')
            ->where('address.id = :id')
            ->setParameter('id', $id->getStorageValue())
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @internal
     */
    protected function duplicateAddress(array $address, string $type): IdValue
    {
        $id = IdValue::create(Uuid::randomHex());
        $address['id'] = $id->getStorageValue();

        $this->connection->insert(
            'customer_address',
            array_merge(
                $address,
                [
                    'created_at' => (new DateTime())->format('Y-m-d H:i:s'),
                ]
            )
        );

        $this->updateAttributes($id, $type);

        return $id;
    }

    /**
     * @internal
     * @return array|bool
     */
    protected function findAddress(IdValue $customerId, string $type)
    {
        return $this->getBaseQuery()
            ->where('address.customer_id = :customerId')
            ->andWhere('data.b2b_type = :type')
            ->setParameter('customerId', $customerId->getStorageValue())
            ->setParameter('type', $type)
            ->orderBy('address.id', 'ASC')
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @internal
     */
    protected function getBaseQuery(): QueryBuilder
    {
        return $this->connection->createQueryBuilder()
            ->select('*')
            ->from('customer_address', 'address')
            ->leftJoin(
                'address',
                'b2b_customer_address_data',
                'data',
                'address.id = data.address_id'
            );
    }

    public function fetchSalutationIdByEmail(string $email): IdValue
    {
        return IdValue::create(
            $this->connection->createQueryBuilder()
                ->select('salutation_id')
                ->from(self::TABLE_NAME)
                ->where('email = :email')
                ->setParameter('email', $email)
                ->execute()
                ->fetchColumn()
        );
    }

    public function fetchSalutationIdByKey(string $salutationKey): IdValue
    {
        return IdValue::create(
            $this->connection->createQueryBuilder()
                ->select('id')
                ->from('salutation')
                ->where('salutation_key = :key')
                ->setParameter('key', $salutationKey)
                ->execute()
                ->fetchColumn()
        );
    }

    /**
     * @internal
     */
    protected function updateAttributes(IdValue $addressId, string $type): void
    {
        try {
            $this->connection->insert(
                'b2b_customer_address_data',
                [
                    'address_id' => $addressId->getStorageValue(),
                    'b2b_type' => $type,
                    'updated_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
                    'created_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
                ]
            );
        } catch (DBALException $e) {
            $this->connection->update(
                'b2b_customer_address_data',
                [
                    'b2b_type' => $type,
                    'updated_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
                ],
                [
                    'address_id' => $addressId->getStorageValue(),
                ]
            );
        }
    }

    public function isMailAvailable(AbstractCredentialsEntity $credentialsEntity): bool
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME);

        $this->credentialQueryExtender->extendQuery($query, $credentialsEntity);

        return !(bool) $query->execute()->fetchAll();
    }

    public function updateEmail(AbstractCredentialsEntity $credentialsEntity, string $newMail): void
    {
        $query = $this->connection->createQueryBuilder()
            ->update(self::TABLE_NAME)
            ->set('email', ':newMail')
            ->where('guest = 0')
            ->setParameter('newMail', $newMail);

        $this->credentialQueryExtender->extendQuery($query, $credentialsEntity);

        $query->execute();
    }

    /**
     * @param string[] $emails
     */
    public function removeUsersByEmail(array $emails): void
    {
        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('email IN (:emails)')
            ->andWhere('guest = 0')
            ->setParameter('emails', $emails, Connection::PARAM_STR_ARRAY)
            ->execute();
    }

    /**
     * @internal
     */
    protected function toStorageValues(array $data): array
    {
        $storageData = [];
        foreach ($data as $key => $value) {
            if ($value instanceof IdValue) {
                $storageData[$key] = $value->getStorageValue();
                continue;
            }

            $storageData[$key] = $value;
        }

        return $storageData;
    }

    /**
     * @return string[]
     */
    public function fetchOwnerLoginDataByIdentity(Identity $identity): array
    {
        return $this->connection->createQueryBuilder()
            ->select('sales_channel_id')
            ->addSelect('default_payment_method_id')
            ->addSelect('customer_group_id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->join(self::TABLE_ALIAS, 'b2b_store_front_auth', 'b2bStoreFrontAuth', self::TABLE_ALIAS . '.id = UNHEX(b2bStoreFrontAuth.provider_context)')
            ->where('b2bStoreFrontAuth.id = :contextAuthId')
            ->setParameter('contextAuthId', $identity->getContextAuthId()->getStorageValue())
            ->execute()
            ->fetch();
    }
}
