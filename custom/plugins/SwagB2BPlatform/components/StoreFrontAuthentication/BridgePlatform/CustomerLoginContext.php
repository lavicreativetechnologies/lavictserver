<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;

class CustomerLoginContext
{
    /**
     * @var IdValue
     */
    public $salesChannelId;

    /**
     * @var IdValue
     */
    public $customerGroupId;

    /**
     * @var IdValue
     */
    public $paymentId;

    /**
     * @return string
     */
    public $avatar;

    public function __construct(IdValue $salesChannelId, IdValue $customerGroupId, IdValue $paymentId, string $avatar)
    {
        $this->salesChannelId = $salesChannelId;
        $this->customerGroupId = $customerGroupId;
        $this->paymentId = $paymentId;
        $this->avatar = $avatar;
    }
}
