<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Doctrine\DBAL\Query\QueryBuilder;

interface AclAuthReadHelperInterface
{
    public function applyAclVisibility(OwnershipContext $ownershipContext, QueryBuilder $queryBuilder, string $alias = 'aclQuery');
}
