<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

class ApiAuthStorageAdapter implements AuthStorageAdapterInterface
{
    /**
     * @var Identity
     */
    private $identity;

    public function unsetIdentity(): void
    {
        $this->identity = null;
    }

    public function setIdentity(Identity $identity): void
    {
        $this->identity = $identity;
    }

    public function getIdentity(): Identity
    {
        if (!$this->identity) {
            throw new NoIdentitySetException();
        }

        return $this->identity;
    }

    public function isAuthenticated(): bool
    {
        return $this->identity ? true : false;
    }
}
