<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\Repository\NotFoundException;

interface AuthenticationIdentityLoaderInterface
{
    /**
     * @throws NotFoundException
     */
    public function fetchIdentityByEmail(string $email, LoginContextService $contextService, bool $isApi = false): Identity;

    /**
     * @throws NotFoundException
     */
    public function fetchIdentityByAuthentication(StoreFrontAuthenticationEntity $authentication, LoginContextService $contextService, bool $isApi = false): Identity;

    /**
     * @throws NotFoundException
     */
    public function fetchIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity, LoginContextService $contextService, bool $isApi = false): Identity;
}
