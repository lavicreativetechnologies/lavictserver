<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

interface UserRepositoryInterface
{
    public function isMailAvailable(AbstractCredentialsEntity $credentialsEntity): bool;

    public function updateEmail(AbstractCredentialsEntity $credentialsEntity, string $newMail);

    public function syncUser(array $userData): IdValue;

    public function checkAddress(IdValue $addressId, string $type, OwnershipContext $context);

    public function fetchOwnerLoginDataByIdentity(Identity $identity): array;

    /**
     * @param string[] $emails
     */
    public function removeUsersByEmail(array $emails);

    public function fetchOneById(IdValue $id): array;
}
