<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;

interface EntityCreatorInterface
{
    public function createEntityByUserId(IdValue $id, OwnershipContext $ownershipContext): Entity;
}
