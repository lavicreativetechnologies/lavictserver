<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

class LoginContextService
{
    /**
     * @var StoreFrontAuthenticationRepository
     */
    private $authenticationRepository;

    /**
     * @var AvatarRepositoryInterface
     */
    private $avatarRepository;

    public function __construct(
        StoreFrontAuthenticationRepository $authenticationRepository,
        AvatarRepositoryInterface $avatarRepository
    ) {
        $this->authenticationRepository = $authenticationRepository;
        $this->avatarRepository = $avatarRepository;
    }

    public function getAuthId(string $providerClass, IdValue $providerContext, IdValue $contextOwnerId = null): IdValue
    {
        $authId = $this->authenticationRepository->fetchIdByProviderData($providerClass, $providerContext);

        if (!$authId->getValue()) {
            $authId = $this->authenticationRepository
                ->createAuthContextEntry($providerClass, $providerContext, $contextOwnerId);
        }

        return $authId;
    }

    public function getAvatar(IdValue $authId): string
    {
        return $this->avatarRepository->fetchAvatarByAuthId($authId);
    }
}
