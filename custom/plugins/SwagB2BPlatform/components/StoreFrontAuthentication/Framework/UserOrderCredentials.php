<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

class UserOrderCredentials
{
    /**
     * @var string
     */
    public $customerNumber;

    /**
     * @var IdValue
     */
    public $orderUserId;

    /**
     * @var string
     */
    public $orderUserReference;

    public function __construct(
        string $customerNumber,
        IdValue $orderUserId,
        string $orderUserReference
    ) {
        $this->customerNumber = $customerNumber;
        $this->orderUserId = $orderUserId;
        $this->orderUserReference = $orderUserReference;
    }
}
