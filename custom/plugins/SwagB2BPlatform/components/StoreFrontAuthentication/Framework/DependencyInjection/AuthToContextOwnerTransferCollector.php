<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use function mb_stripos;

class AuthToContextOwnerTransferCollector implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $parameters = $container->getParameterBag();

        $tablesNames = [];

        foreach ($parameters->all() as $key => $parameter) {
            if (mb_stripos($key, 'authToContextOwnerTransfer.') === 0) {
                $tablesNames[] = $parameter;
            }
        }

        if (!$tablesNames) {
            return;
        }

        $repo = $container->findDefinition('b2b_front_auth.repository');

        $repo->addArgument($tablesNames);
    }
}
