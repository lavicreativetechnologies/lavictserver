<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

class UserPostalSettings
{
    /**
     * @var string
     */
    public $salutation;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var IdValue
     */
    public $language;

    /**
     * @var string
     */
    public $email;

    public function __construct(
        string $salutation,
        string $title = null,
        string $firstName,
        string $lastName,
        IdValue $language,
        string $email
    ) {
        $this->salutation = $salutation;
        $this->title = $title;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->language = $language;
        $this->email = $email;
    }
}
