<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

class TransferOwnerDataToContextOwnerService implements TransferOwnerDataToContextOwnerInterface
{
    /**
     * @var []TransferOwnerDataToContextOwnerInterface
     */
    private $transferRepositories;

    /**
     * @param []TransferOwnerDataToContextOwnerInterface  $transferRepositories
     */
    public function __construct(array $transferRepositories)
    {
        $this->transferRepositories = $transferRepositories;
    }

    public function transferOwnerDataToContextOwner(IdValue $authId, IdValue $contextOwnerId): void
    {
        foreach ($this->transferRepositories as $transferRepository) {
            $transferRepository->transferOwnerDataToContextOwner($authId, $contextOwnerId);
        }
    }
}
