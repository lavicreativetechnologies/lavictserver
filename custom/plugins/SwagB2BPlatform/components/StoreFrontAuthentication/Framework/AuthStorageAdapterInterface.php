<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

interface AuthStorageAdapterInterface
{
    public function unsetIdentity();

    public function setIdentity(Identity $identity);

    public function getIdentity(): Identity;

    public function isAuthenticated(): bool;
}
