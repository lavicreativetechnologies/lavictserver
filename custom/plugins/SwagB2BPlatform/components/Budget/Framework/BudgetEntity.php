<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Currency\Framework\CurrencyAware;
use Shopware\B2B\OrderClearance\Framework\OrderItemEntity;
use function get_object_vars;
use function property_exists;

class BudgetEntity extends OrderItemEntity implements CrudEntity, CurrencyAware
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $name;

    /**
     * @var IdValue
     */
    public $ownerId;

    /**
     * @var bool
     */
    public $notifyAuthor;

    /**
     * @var int
     */
    public $notifyAuthorPercentage;

    /**
     * @var bool
     */
    public $active;

    /**
     * @var float
     */
    public $amount;

    /**
     * @var string
     */
    public $refreshType;

    /**
     * @var string
     */
    public $fiscalYear;

    /**
     * @var BudgetStatus
     */
    public $currentStatus;

    /**
     * @var float
     */
    public $currencyFactor = self::DEFAULT_FACTOR;

    /**
     * @var array
     */
    public $owner;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->ownerId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'identifier' => $this->identifier,
            'name' => $this->name,
            'owner_id' => $this->ownerId->getStorageValue(),
            'notify_author' => $this->notifyAuthor,
            'notify_author_percentage' => $this->notifyAuthorPercentage,
            'active' => $this->active,
            'amount' => $this->amount,
            'refresh_type' => $this->refreshType,
            'fiscal_year' => $this->fiscalYear,
            'currency_factor' => $this->currencyFactor,
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->identifier = $data['identifier'];
        $this->name = $data['name'];

        if (isset($data['owner_id'])) {
            $this->ownerId = IdValue::create($data['owner_id']);
        }

        $this->notifyAuthor = (bool) $data['notify_author'];
        $this->notifyAuthorPercentage = (int) $data['notify_author_percentage'];
        $this->active = (bool) $data['active'];
        $this->amount = (float) $data['amount'];
        $this->refreshType = $data['refresh_type'];
        $this->fiscalYear = (string) $data['fiscal_year'];
        $this->setCurrencyFactor((float) $data['currency_factor']);

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->ownerId = $this->ownerId ? IdValue::create($this->ownerId) : IdValue::null();
        $this->notifyAuthor = $this->notifyAuthor ? (bool) $this->notifyAuthor : null;
        $this->notifyAuthorPercentage = $this->notifyAuthorPercentage ? (int) $this->notifyAuthorPercentage : null;

        if ($this->active) {
            $this->active = (bool) $this->active;
        }

        if ($this->amount) {
            $this->amount = (float) $this->amount;
        }

        if ($this->fiscalYear) {
            $this->fiscalYear = (string) $this->fiscalYear;
        }

        if ($this->currencyFactor) {
            $this->setCurrencyFactor((float) $this->currencyFactor);
        }
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function getCurrencyFactor(): float
    {
        return $this->currencyFactor;
    }

    public function setCurrencyFactor(float $factor): void
    {
        $this->currencyFactor = $factor;
    }

    public function getAmountPropertyNames(): array
    {
        return [
            'amount',
        ];
    }

    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }
}
