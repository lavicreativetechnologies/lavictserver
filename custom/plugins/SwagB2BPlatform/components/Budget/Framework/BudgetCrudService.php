<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Framework;

use Shopware\B2B\Acl\Framework\AclAccessWriterInterface;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function abs;

class BudgetCrudService extends AbstractCrudService
{
    /**
     * @var BudgetRepository
     */
    private $budgetRepository;

    /**
     * @var BudgetValidationService
     */
    private $validationService;

    /**
     * @var AclAccessWriterInterface
     */
    private $aclAccessWriter;

    public function __construct(
        BudgetRepository $budgetRepository,
        BudgetValidationService $validationService,
        AclAccessWriterInterface $aclAccessWriter
    ) {
        $this->budgetRepository = $budgetRepository;
        $this->validationService = $validationService;
        $this->aclAccessWriter = $aclAccessWriter;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'identifier',
                'name',
                'ownerId',
                'notifyAuthor',
                'notifyAuthorPercentage',
                'active',
                'amount',
                'refreshType',
                'fiscalYear',
                'currencyFactor',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'identifier',
                'name',
                'ownerId',
                'notifyAuthor',
                'notifyAuthorPercentage',
                'active',
                'amount',
                'refreshType',
                'fiscalYear',
                'currencyFactor',
            ]
        );
    }

    public function create(
        CrudServiceRequest $request,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext,
        AclGrantContext $grantContext
    ): BudgetEntity {
        $data = $request->getFilteredData();

        $budget = new BudgetEntity();

        $budget->setData($data);
        $budget->currencyFactor = $currencyContext->currentCurrencyFactor;

        $validation = $this->validationService
            ->createInsertValidation($budget, $ownershipContext);

        $this->testValidation($budget, $validation);

        $budget = $this->budgetRepository
            ->addBudget($budget, $ownershipContext);

        $this->aclAccessWriter->addNewSubject(
            $ownershipContext,
            $grantContext,
            $budget->id,
            true
        );

        return $budget;
    }

    public function update(CrudServiceRequest $request, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): BudgetEntity
    {
        $data = $request->getFilteredData();
        $id = $request->requireIdValue('id');

        $currentBudget = $this->budgetRepository
            ->fetchOneById($id, $currencyContext, $ownershipContext);

        $updatedBudget = new BudgetEntity();
        $updatedBudget->setData($data);

        if (abs($currentBudget->amount - $updatedBudget->amount) > 0.001) {
            $updatedBudget->currencyFactor = $currencyContext->currentCurrencyFactor;
        }

        $this->aclAccessWriter->testUpdateAllowed($ownershipContext, $updatedBudget->id);

        $validation = $this->validationService
            ->createUpdateValidation($updatedBudget, $ownershipContext);

        $this->testValidation($updatedBudget, $validation);

        $this->budgetRepository
            ->updateBudget($updatedBudget, $ownershipContext);

        return $updatedBudget;
    }

    public function remove(IdValue $id, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): BudgetEntity
    {
        $this->aclAccessWriter->testUpdateAllowed($ownershipContext, $id);

        $budget = $this->budgetRepository->fetchOneById($id, $currencyContext, $ownershipContext);

        $this->budgetRepository
            ->removeBudget($budget, $ownershipContext);

        return $budget;
    }
}
