<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Framework;

use DateTime;
use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\MysqlRepository;
use Shopware\B2B\Common\Repository\NotFoundException;
use function sprintf;

class BudgetNotificationRepository
{
    const TABLE_NAME = 'b2b_budget_notify';

    const TABLE_ALIAS = 'notify';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function addNotify(IdValue $budgetId, int $refreshGroup, DateTime $time): void
    {
        $this->connection->insert(
            self::TABLE_NAME,
            [
                'budget_id' => $budgetId->getStorageValue(),
                'refresh_group' => $refreshGroup,
                'time' => $time->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
            ]
        );
    }

    /**
     * @throws NotFoundException
     */
    public function fetchNotifyByIdAndRefreshGroup(IdValue $budgetId, int $refreshGroup): array
    {
        $query = $this->connection->createQueryBuilder();

        $notify = $query->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.budget_id = :budgetId AND ' . self::TABLE_ALIAS . '.refresh_group = :refreshGroup')
            ->setParameters([
                'budgetId' => $budgetId->getStorageValue(),
                'refreshGroup' => $refreshGroup,
            ])
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);

        if (!$notify) {
            throw new NotFoundException(sprintf(
                'No notification found for budget %d and refresh group %d',
                $budgetId->getValue(),
                $refreshGroup
            ));
        }

        return $notify;
    }
}
