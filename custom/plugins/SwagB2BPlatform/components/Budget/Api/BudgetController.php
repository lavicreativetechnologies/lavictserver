<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Api;

use DateTime;
use DomainException;
use InvalidArgumentException;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Acl\Framework\AclGrantContextProviderChain;
use Shopware\B2B\Budget\Framework\BudgetCrudService;
use Shopware\B2B\Budget\Framework\BudgetEntity;
use Shopware\B2B\Budget\Framework\BudgetRepository;
use Shopware\B2B\Budget\Framework\BudgetSearchStruct;
use Shopware\B2B\Budget\Framework\BudgetService;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class BudgetController
{
    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var BudgetRepository
     */
    private $budgetRepository;

    /**
     * @var BudgetCrudService
     */
    private $budgetCrudService;

    /**
     * @var BudgetService
     */
    private $budgetService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var AclGrantContextProviderChain
     */
    private $grantContextProviderChain;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        BudgetRepository $budgetRepository,
        GridHelper $requestHelper,
        BudgetCrudService $budgetCrudService,
        BudgetService $budgetService,
        CurrencyService $currencyService,
        AclGrantContextProviderChain $grantContextProviderChain,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->requestHelper = $requestHelper;
        $this->budgetRepository = $budgetRepository;
        $this->budgetCrudService = $budgetCrudService;
        $this->budgetService = $budgetService;
        $this->currencyService = $currencyService;
        $this->grantContextProviderChain = $grantContextProviderChain;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(Request $request): array
    {
        $search = new BudgetSearchStruct();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $ownerShipContext = $this->getDebtorOwnershipContext();

        $budgets = $this->budgetRepository->fetchList($ownerShipContext, $search, $currencyContext);

        $this->addCurrentStatusToBudget($budgets, $ownerShipContext);

        $totalCount = $this->budgetRepository
            ->fetchTotalCount($ownerShipContext, $search);

        return ['success' => true, 'budgets' => $budgets, 'totalCount' => $totalCount];
    }

    public function getAction(string $budgetId): array
    {
        $budgetId = IdValue::create($budgetId);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $budget = $this->budgetRepository
            ->fetchOneById($budgetId, $currencyContext, $ownershipContext);

        $this->addCurrentStatusToBudget([$budget], $ownershipContext);

        return ['success' => true, 'budget' => $budget];
    }

    public function createAction(Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $aclGrantContext = $this->extractGrantContext($request, $ownershipContext);

        $data = $request->getPost();

        $newRecord = $this->budgetCrudService
            ->createNewRecordRequest($data);

        $budget = $this->budgetCrudService
            ->create($newRecord, $ownershipContext, $currencyContext, $aclGrantContext);

        $this->addCurrentStatusToBudget([$budget], $ownershipContext);

        return ['success' => true, 'budget' => $budget];
    }

    public function updateAction(string $budgetId, Request $request): array
    {
        $budgetId = IdValue::create($budgetId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $data = $request->getPost();
        $data['id'] = $budgetId;

        $currencyContext = $this->currencyService->createCurrencyContext();

        $existingRecord = $this->budgetCrudService
            ->createExistingRecordRequest($data);

        $budget = $this->budgetCrudService
            ->update($existingRecord, $currencyContext, $ownershipContext);

        $this->addCurrentStatusToBudget([$budget], $ownershipContext);

        return ['success' => true, 'budget' => $budget];
    }

    public function getCurrentStatusAction(string $budgetId)
    {
        $budgetId = IdValue::create($budgetId);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $status = $this->budgetService->getBudgetStatus($budgetId, $currencyContext, $ownershipContext);

        return ['success' => true, 'status' => $status];
    }

    public function getStatusAction(string $budgetId, string $date)
    {
        $budgetId = IdValue::create($budgetId);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $status = $this->budgetService->getBudgetStatus(
            $budgetId,
            $currencyContext,
            $ownershipContext,
            DateTime::createFromFormat('Y-m-d', $date)
        );

        return ['success' => true, 'status' => $status];
    }

    public function removeAction(string $budgetId): array
    {
        $budgetId = IdValue::create($budgetId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $budget = $this->budgetCrudService
            ->remove($budgetId, $currencyContext, $ownershipContext);

        return ['success' => true, 'budget' => $budget];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->getDebtorIdentity()->getOwnershipContext();
    }

    /**
     * @internal
     */
    protected function getDebtorIdentity(): Identity
    {
        return $this->authStorageAdapter->getIdentity();
    }

    /**
     * @internal
     * @param BudgetEntity[] $budgets
     */
    protected function addCurrentStatusToBudget(array $budgets, OwnershipContext $ownershipContext): void
    {
        $currencyContext = $this->currencyService->createCurrencyContext();

        foreach ($budgets as $budget) {
            try {
                $budget->currentStatus = $this->budgetService->getBudgetStatus(
                    $budget->id,
                    $currencyContext,
                    $ownershipContext,
                    new DateTime()
                );
            } catch (DomainException $e) {
                //nth
            }
        }
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function extractGrantContext(Request $request, OwnershipContext $ownershipContext): AclGrantContext
    {
        $grantContextIdentifier = $request->requireParam('grantContextIdentifier');

        return $this->grantContextProviderChain->fetchOneByIdentifier($grantContextIdentifier, $ownershipContext);
    }
}
