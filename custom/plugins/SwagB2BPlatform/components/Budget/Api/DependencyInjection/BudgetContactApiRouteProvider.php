<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class BudgetContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}',
                'b2b_budget.api_budget_contact_controller',
                'getAllAllowed',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/grant',
                'b2b_budget.api_budget_contact_controller',
                'getAllGrant',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/budget/{budgetId}',
                'b2b_budget.api_budget_contact_controller',
                'getAllowed',
                ['contactEmail', 'budgetId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/budget/{budgetId}',
                'b2b_budget.api_budget_contact_controller',
                'allow',
                ['contactEmail', 'budgetId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/budget/{budgetId}/grant',
                'b2b_budget.api_budget_contact_controller',
                'allowGrant',
                ['contactEmail', 'budgetId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/allow',
                'b2b_budget.api_budget_contact_controller',
                'multipleAllow',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/deny',
                'b2b_budget.api_budget_contact_controller',
                'multipleDeny',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/budget_contact/{contactEmail}/budget/{budgetId}',
                'b2b_budget.api_budget_contact_controller',
                'deny',
                ['contactEmail', 'budgetId'],
            ],
        ];
    }
}
