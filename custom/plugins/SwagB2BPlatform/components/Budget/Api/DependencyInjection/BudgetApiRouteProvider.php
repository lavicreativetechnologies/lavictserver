<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class BudgetApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget',
                'b2b_budget.api_controller',
                'getList',
                [],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget/{budgetId}',
                'b2b_budget.api_controller',
                'get',
                ['budgetId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/budget',
                'b2b_budget.api_controller',
                'create',
                [],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/budget/{budgetId}',
                'b2b_budget.api_controller',
                'update',
                ['budgetId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/budget/{budgetId}',
                'b2b_budget.api_controller',
                'remove',
                ['budgetId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget/{budgetId}/status',
                'b2b_budget.api_controller',
                'getCurrentStatus',
                ['budgetId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/budget/{budgetId}/status/{date}',
                'b2b_budget.api_controller',
                'getStatus',
                ['budgetId', 'date'],
            ],
        ];
    }
}
