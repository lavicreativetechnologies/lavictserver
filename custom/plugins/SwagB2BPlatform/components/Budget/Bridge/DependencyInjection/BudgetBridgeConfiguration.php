<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Bridge\DependencyInjection;

use Shopware\B2B\Budget\BridgePlatform\DependencyInjection\BudgetBridgeConfiguration as PlatformBudgetBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Order\Framework\DependencyInjection\OrderFrameworkConfiguration;
use Shopware\B2B\OrderClearance\Framework\DependencyInjection\OrderClearanceFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BudgetBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformBudgetBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new OrderFrameworkConfiguration(),
            new OrderClearanceFrameworkConfiguration(),
        ];
    }
}
