<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\BridgePlatform;

use Shopware\B2B\Budget\Framework\BudgetEntity;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class BudgetEntityMailData
{
    /**
     * @var BudgetEntity
     */
    private $budgetEntity;

    public function __construct(BudgetEntity $budgetEntity)
    {
        $this->budgetEntity = $budgetEntity;
    }

    public static function getBudgetMailDataType(): EventDataType
    {
        $budget = new ObjectType();
        $budget->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $budget->add('identifier', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $budget->add('name', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $budget->add('ownerId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $budget->add('notifyAuthor', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $budget->add('notifyAuthorPercentage', new ScalarValueType(ScalarValueType::TYPE_INT));
        $budget->add('active', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $budget->add('amount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $budget->add('refreshType', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $budget->add('fiscalYear', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $budget->add('currentStatus', BudgetStatusMailData::getStatusDataType());
        $budget->add('currencyFactor', new ScalarValueType(ScalarValueType::TYPE_STRING));

        return $budget;
    }

    public function getId(): string
    {
        return (string) $this->budgetEntity->id->getValue();
    }

    public function getIdentifier(): string
    {
        return $this->budgetEntity->identifier;
    }

    public function getName(): string
    {
        return $this->budgetEntity->name;
    }

    public function getOwnerId(): string
    {
        return (string) $this->budgetEntity->ownerId->getValue();
    }

    public function getNotifyAuthor(): bool
    {
        return $this->budgetEntity->notifyAuthor;
    }

    public function getNotifyAuthorPercentage(): int
    {
        return $this->budgetEntity->notifyAuthorPercentage;
    }

    public function isActive(): bool
    {
        return $this->budgetEntity->active;
    }

    public function getAmount(): float
    {
        return $this->budgetEntity->amount;
    }

    public function getRefreshType(): string
    {
        return $this->budgetEntity->refreshType;
    }

    public function getFiscalYear(): string
    {
        return $this->budgetEntity->fiscalYear;
    }

    public function getCurrentStatus(): BudgetStatusMailData
    {
        return new BudgetStatusMailData($this->budgetEntity->currentStatus);
    }
}
