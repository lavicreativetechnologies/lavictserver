<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\BridgePlatform;

use DateTime;
use Shopware\B2B\Budget\Framework\BudgetEntity;
use Shopware\B2B\Budget\Framework\BudgetNotificationRepository;
use Shopware\B2B\Budget\Framework\BudgetRepository;
use Shopware\B2B\Budget\Framework\BudgetService;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use function count;

class BudgetNotificationTaskHandler extends ScheduledTaskHandler
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var BudgetRepository
     */
    private $budgetRepository;

    /**
     * @var BudgetService
     */
    private $budgetService;

    /**
     * @var BudgetNotificationRepository
     */
    private $notificationRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        EntityRepositoryInterface $scheduledTaskRepository,
        AuthenticationService $authenticationService,
        CurrencyService $currencyService,
        BudgetRepository $budgetRepository,
        BudgetService $budgetService,
        BudgetNotificationRepository $notificationRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct($scheduledTaskRepository);

        $this->scheduledTaskRepository = $scheduledTaskRepository;
        $this->authenticationService = $authenticationService;
        $this->currencyService = $currencyService;
        $this->budgetRepository = $budgetRepository;
        $this->budgetService = $budgetService;
        $this->notificationRepository = $notificationRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public static function getHandledMessages(): iterable
    {
        return [BudgetNotificationTask::class];
    }

    public function run(): void
    {
        $currencyContext = $this->currencyService->createCurrencyContext();

        $budgets = $this->budgetRepository->fetchAllBudgets($currencyContext);

        foreach ($budgets as $budget) {
            if ($budget->ownerId instanceof NullIdValue) {
                continue;
            }

            $this->sendNotifyMail($budget, $currencyContext);
        }
    }

    /**
     * @internal
     */
    protected function sendNotifyMail(BudgetEntity $budget, CurrencyContext $currencyContext): void
    {
        $identity = $this->authenticationService->getIdentityByAuthId($budget->ownerId);
        $context = $this->budgetService->prepareMail($budget, $currencyContext, $identity->getOwnershipContext());

        if (count($context) === 0) {
            return;
        }

        $this->eventDispatcher->dispatch(new BudgetNotificationMailEvent($budget, $identity->getPostalSettings()));

        $this->notificationRepository
            ->addNotify($budget->id, $context['refreshGroup'], new DateTime());
    }
}
