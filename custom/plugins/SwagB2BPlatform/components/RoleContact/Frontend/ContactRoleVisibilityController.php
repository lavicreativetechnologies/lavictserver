<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Frontend;

use Shopware\B2B\Acl\Framework\AclAccessExtensionService;
use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Role\Framework\RoleRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class ContactRoleVisibilityController
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var AclRepository
     */
    private $aclRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var AclAccessExtensionService
     */
    private $aclAccessExtensionService;

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        AclRepository $aclRepository,
        RoleRepository $roleRepository,
        ContactRepository $contactRepository,
        GridHelper $gridHelper,
        AclAccessExtensionService $aclAccessExtensionService
    ) {
        $this->authenticationService = $authenticationService;
        $this->aclRepository = $aclRepository;
        $this->roleRepository = $roleRepository;
        $this->gridHelper = $gridHelper;
        $this->aclAccessExtensionService = $aclAccessExtensionService;
        $this->contactRepository = $contactRepository;
    }

    public function indexAction(Request $request): array
    {
        $contactId = $request->requireIdValue('contactId');

        return [
            'contactId' => $contactId->getValue(),
        ];
    }

    /**
     * @return array
     */
    public function treeAction(Request $request)
    {
        $parentId = $request->getIdValue('parentId');
        $contactId = $request->requireIdValue('contactId');

        $ownershipContext = $this->authenticationService
            ->getIdentity()->getOwnershipContext();

        $contact = $this->contactRepository->fetchOneById($contactId, $ownershipContext);

        if ($parentId instanceof NullIdValue) {
            $roles = $this->roleRepository
                ->fetchAclRootRoles($ownershipContext, false);
        } else {
            $roles = $this->roleRepository
                ->fetchChildren($parentId, $ownershipContext);
        }

        $this->aclAccessExtensionService
            ->extendEntitiesWithAssignment($this->aclRepository, $contact, $roles);

        $this->aclAccessExtensionService
            ->extendEntitiesWithIdentityOwnership($this->aclRepository, $ownershipContext, $roles);

        return [
            'contactId' => $contactId->getValue(),
            'roles' => $roles,
        ];
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function assignAction(Request $request): void
    {
        $request->checkPost('index', ['contactId' => $request->getParam('contactId')]);

        $roleId = $request->requireIdValue('roleId');
        $contactId = $request->requireIdValue('contactId');
        $grantable = (bool) $request->getParam('grantable', false);
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $contact = $this->contactRepository->fetchOneById($contactId, $ownershipContext);

        if ($request->getParam('allow', false)) {
            $this->aclRepository->allow($contact, $roleId, $grantable);
        } else {
            $this->aclRepository->deny($contact, $roleId);
        }

        throw new B2bControllerForwardException('index', null, ['contactId' => $contactId->getValue()]);
    }
}
