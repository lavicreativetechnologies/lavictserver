<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Frontend\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\RoleContact\Framework\DependencyInjection\RoleContactFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RoleContactFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new RoleContactFrameworkConfiguration(),
        ];
    }
}
