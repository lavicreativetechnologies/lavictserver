<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Frontend;

use Shopware\B2B\Common\Controller\EmptyForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\RoleContact\Framework\RoleContactAssignmentService;
use Shopware\B2B\RoleContact\Framework\RoleContactRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class ContactRoleAssignmentController
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var RoleContactRepository
     */
    private $roleContactRepository;

    /**
     * @var RoleContactAssignmentService
     */
    private $roleContactAssignmentService;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    public function __construct(
        AuthenticationService $authenticationService,
        RoleContactRepository $roleContactRepository,
        RoleContactAssignmentService $roleContactAssignmentService,
        GridHelper $gridHelper
    ) {
        $this->authenticationService = $authenticationService;
        $this->roleContactRepository = $roleContactRepository;
        $this->roleContactAssignmentService = $roleContactAssignmentService;
        $this->gridHelper = $gridHelper;
    }

    public function indexAction(Request $request): array
    {
        $contactId = $request->requireIdValue('contactId');

        return [
            'contactId' => $contactId->getValue(),
        ];
    }

    public function treeAction(Request $request): array
    {
        $parentId = $request->getIdValue('parentId');
        $contactId = $request->requireIdValue('contactId');

        $ownershipContext = $this->authenticationService
            ->getIdentity()->getOwnershipContext();

        if ($parentId instanceof NullIdValue) {
            $roles = $this->roleContactRepository
                ->fetchRootRoleAssignmentsAndCheckForContactAssignment($contactId, $ownershipContext);
        } else {
            $roles = $this->roleContactRepository
                ->fetchChildrenAndCheckForContactAssignment($parentId, $contactId, $ownershipContext);
        }

        return [
            'contactId' => $contactId->getValue(),
            'roles' => $roles,
        ];
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     * @return array
     */
    public function assignAction(Request $request)
    {
        $request->checkPost('index', ['contactId' => $request->requireParam('contactId')]);

        $contactId = $request->requireIdValue('contactId');
        $roleId = $request->requireIdValue('roleId');

        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        try {
            if ($request->getParam('allow', false)) {
                $this->roleContactAssignmentService
                    ->assign($ownershipContext, $roleId, $contactId);
            } else {
                $this->roleContactAssignmentService
                    ->removeAssignment($ownershipContext, $roleId, $contactId);
            }
        } catch (ValidationException $validationException) {
            $this->gridHelper->pushValidationException($validationException);

            return $this->gridHelper->getValidationResponse('role');
        }

        throw new EmptyForwardException();
    }
}
