<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class RoleContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact/{email}/role',
                'b2b_role_contact.api_role_contact_controller',
                'getList',
                ['email'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contact/{email}/role',
                'b2b_role_contact.api_role_contact_controller',
                'create',
                ['email'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contact/{email}/role',
                'b2b_role_contact.api_role_contact_controller',
                'remove',
                ['email'],
            ],
        ];
    }
}
