<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Role\Framework\AbstractRoleBaseRepository;
use Shopware\B2B\Role\Framework\RoleAclGrantContext;
use Shopware\B2B\Role\Framework\RoleAssignmentEntity;
use Shopware\B2B\Role\Framework\RoleRemoveDependencyValidatorInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\DbalNestedSet\NestedSetQueryFactory;

/**
 * DB-Representation of role:contact assignment
 */
class RoleContactRepository extends AbstractRoleBaseRepository implements RoleRemoveDependencyValidatorInterface
{
    const TABLE_ROLE_CONTACT_NAME = 'b2b_role_contact';
    const TABLE_ROLE_CONTACT_ALIAS = 'role_contact';

    /**
     * @var NestedSetQueryFactory
     */
    private $nestedSetQueryFactory;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    public function __construct(
        Connection $connection,
        NestedSetQueryFactory $nestedSetQueryFactory,
        AclReadHelper $aclReadHelper
    ) {
        parent::__construct($connection, $aclReadHelper);
        $this->nestedSetQueryFactory = $nestedSetQueryFactory;
        $this->connection = $connection;
        $this->aclReadHelper = $aclReadHelper;
    }

    public function removeRoleContactAssignment(IdValue $roleId, IdValue $contactId): void
    {
        $this->connection->delete(
            self::TABLE_ROLE_CONTACT_NAME,
            [
                'role_id' => $roleId->getStorageValue(),
                'debtor_contact_id' => $contactId->getStorageValue(),
            ]
        );
    }

    public function assignRoleContact(IdValue $roleId, IdValue $contactId): void
    {
        $data = [
            'role_id' => $roleId->getStorageValue(),
            'debtor_contact_id' => $contactId->getStorageValue(),
        ];

        $this->connection->insert(
            self::TABLE_ROLE_CONTACT_NAME,
            $data
        );
    }

    public function getActiveRoleIdsByContactId(IdValue $contactId): array
    {
        $roles = $this->connection->fetchAll(
            'SELECT role_id FROM ' . self::TABLE_ROLE_CONTACT_NAME . '
             WHERE debtor_contact_id = :contactId
            ',
            [
                ':contactId' => $contactId->getStorageValue(),
            ]
        );

        return $roles;
    }

    /**
     * @return RoleAssignmentEntity[]
     */
    public function fetchChildrenAndCheckForContactAssignment(
        IdValue $parentId,
        IdValue $contactId,
        OwnershipContext $ownershipContext
    ): array {
        $query = $this->nestedSetQueryFactory
            ->createChildrenQueryBuilder(self::TABLE_ROLE_NAME, self::TABLE_ROLE_ALIAS, 'context_owner_id', $parentId->getStorageValue())
            ->addSelect(self::TABLE_ROLE_ALIAS . '.*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->addSelect(self::TABLE_ROLE_CONTACT_ALIAS . '.id as assignmentId')
            ->leftJoin(
                self::TABLE_ROLE_ALIAS,
                self::TABLE_ROLE_CONTACT_NAME,
                self::TABLE_ROLE_CONTACT_ALIAS,
                self::TABLE_ROLE_ALIAS . '.id = ' . self::TABLE_ROLE_CONTACT_ALIAS . '.role_id and ' . self::TABLE_ROLE_CONTACT_ALIAS . '.debtor_contact_id = :contactId'
            )
            ->andWhere(self::TABLE_ROLE_ALIAS . '.context_owner_id = :contextOwner')
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue())
            ->setParameter('contactId', $contactId->getStorageValue());

        $statement = $query->execute();
        $rolesData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $roles = [];
        foreach ($rolesData as $roleData) {
            $roles[] = (new RoleAssignmentEntity())->fromDatabaseArray($roleData);
        }

        return $roles;
    }

    public function isRoleDebtorContactDebtor(OwnershipContext $ownershipContext, IdValue $roleId, IdValue $contactId): bool
    {
        $query = $this->connection->createQueryBuilder()
            ->select('role.id')
            ->from('b2b_role', 'role')
            ->leftJoin('role', 'b2b_debtor_contact', 'contact', 'contact.context_owner_id = role.context_owner_id')
            ->where('role.id = :roleId AND contact.id = :contactId AND contact.context_owner_id = :contextOwnerId')
            ->setParameter('contactId', $contactId->getStorageValue())
            ->setParameter('roleId', $roleId->getStorageValue())
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue())
            ->execute();

        return (bool) $query->fetch(PDO::FETCH_COLUMN);
    }

    public function extendRoleRemoveDependencyQuery(
        QueryBuilder $queryBuilder,
        RoleAclGrantContext $grantContext,
        string $mainTableAlias,
        string $alias
    ): void {
        $queryBuilder->leftJoin(
            $mainTableAlias,
            self::TABLE_ROLE_CONTACT_NAME,
            $alias,
            "{$alias}.role_id = {$mainTableAlias}.id"
        );
        $queryBuilder->orWhere("{$alias}.id is not null");
    }

    /**
     * @return RoleAssignmentEntity[]
     */
    public function fetchRootRoleAssignmentsAndCheckForContactAssignment(
        IdValue $contactId,
        OwnershipContext $ownershipContext
    ): array {
        $query = $this->createRootRolesQueryBuilder($ownershipContext, false)
            ->addSelect(self::TABLE_ROLE_CONTACT_ALIAS . '.id as assignmentId')
            ->leftJoin(
                self::TABLE_ROLE_ALIAS,
                self::TABLE_ROLE_CONTACT_NAME,
                self::TABLE_ROLE_CONTACT_ALIAS,
                self::TABLE_ROLE_ALIAS . '.id = ' . self::TABLE_ROLE_CONTACT_ALIAS . '.role_id and ' . self::TABLE_ROLE_CONTACT_ALIAS . '.debtor_contact_id = :contactId'
            )
            ->setParameter('contactId', $contactId->getStorageValue());

        $statement = $query->execute();
        $rolesData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $roles = [];
        foreach ($rolesData as $roleData) {
            $roles[] = (new RoleAssignmentEntity())->fromDatabaseArray($roleData);
        }

        return $roles;
    }
}
