<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework\LineItemType;

use Shopware\B2B\Common\ValueInterface;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Promotion\Cart\PromotionProcessor;
use function array_flip;
use function array_key_exists;

abstract class LineItemType implements ValueInterface
{
    private const MAPPING = [
        'product' => 0,
        'credit' => 2,
        'promotion' => 4,
    ];

    /**
     * @var string|int
     */
    private $originalMode;

    /**
     * @param string|int $originalMode
     */
    public function __construct($originalMode)
    {
        $this->originalMode = $originalMode;
    }

    /**
     * @return string|int
     */
    public function getValue()
    {
        return $this->originalMode;
    }

    /**
     * @param string|int $mode
     */
    public static function create($mode): LineItemType
    {
        if ($mode instanceof LineItemType) {
            return $mode;
        }

        if (ShopwareVersion::isClassic()) {
            if (array_key_exists($mode, self::MAPPING)) {
                $mode = self::MAPPING[$mode];
            }

            return self::createFromClassic((int) $mode);
        }

        $flipped = array_flip(self::MAPPING);
        if (array_key_exists($mode, $flipped)) {
            $mode = $flipped[$mode];
        }

        return self::createFromPlatform((string) $mode);
    }

    /**
     * @internal
     */
    protected static function createFromClassic(int $mode): LineItemType
    {
        switch ($mode) {
            case 0:
                return new ProductLineItemType($mode);
            case 2:
                return new VoucherLineItemType($mode);
            case 4:
                return new DiscountLineItemType($mode);
            default:
                return new UnknownLineItemType($mode);
        }
    }

    /**
     * @internal
     */
    protected static function createFromPlatform(string $mode): LineItemType
    {
        switch ($mode) {
            case LineItem::PRODUCT_LINE_ITEM_TYPE:
                return new ProductLineItemType($mode);
            case LineItem::CREDIT_LINE_ITEM_TYPE:
                return new VoucherLineItemType($mode);
            case PromotionProcessor::LINE_ITEM_TYPE:
                return new DiscountLineItemType($mode);
            default:
                return new UnknownLineItemType($mode);
        }
    }
}
