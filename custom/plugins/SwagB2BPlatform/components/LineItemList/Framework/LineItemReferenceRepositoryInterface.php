<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\NotFoundException;

interface LineItemReferenceRepositoryInterface extends GridRepository
{
    /**
     * @return LineItemReference[]
     */
    public function fetchList(IdValue $listId, LineItemReferenceSearchStruct $searchStruct): array;

    public function fetchTotalCount(IdValue $listId, LineItemReferenceSearchStruct $searchStruct): int;

    /**
     * @throws NotFoundException
     */
    public function fetchReferenceById(IdValue $id): LineItemReference;

    public function flipSorting(LineItemReference $lineItemReferenceOne, LineItemReference $lineItemReferenceTwo);

    public function hasReference(string $referenceNumber, IdValue $listId): bool;

    public function getReferenceByReferenceNumberAndListId(string $referenceNumber, IdValue $listId): LineItemReference;

    /**
     * @throws CanNotInsertExistingRecordException
     */
    public function addReference(IdValue $listId, LineItemReference $lineItemReference): LineItemReference;

    public function addVoucherCodeToReferenceById(IdValue $referenceId, string $voucherCode);

    /**
     * @deprecated tag:v4.3.0 - return type will be set to void
     */
    public function updateReference(IdValue $listId, LineItemReference $lineItemReference);

    public function removeReference(IdValue $id);

    public function syncReferences(IdValue $listId, array $references);

    public function removeReferenceByListId(IdValue $id);

    public function getMainTableAlias(): string;

    public function getFullTextSearchFields(): array;

    public function getAdditionalSearchResourceAndFields(): array;

    public function updatePrices(LineItemReference $lineItemReference);

    public function getOwnershipByReferenceId(IdValue $referenceId): IdValue;
}
