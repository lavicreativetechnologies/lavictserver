<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use RuntimeException;
use Shopware\B2B\Common\B2BException;

class InvalidProductFromShopException extends RuntimeException implements B2BException
{
}
