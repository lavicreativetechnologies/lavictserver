<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Currency\Framework\CurrencyAware;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\LineItemList\Framework\LineItemType\UnknownLineItemType;
use Shopware\B2B\ProductName\Framework\ProductNameAware;
use function get_object_vars;
use function is_numeric;
use function property_exists;
use function round;
use function trim;

class LineItemReference implements CrudEntity, ProductNameAware, CurrencyAware
{
    private const AMOUNT_PRECISION = 2;

    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $referenceNumber;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $quantity;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var float
     */
    public $amount;

    /**
     * @var float
     */
    public $amountNet;

    /**
     * @var LineItemType
     */
    public $mode;

    /**
     * @var int
     */
    public $sort;

    /**
     * @var string
     */
    public $voucherCode;

    /**
     * @var float
     */
    public $maxPurchase;

    /**
     * @var float
     */
    public $minPurchase;

    /**
     * @var float
     */
    public $purchaseStep;

    /**
     * @var int
     */
    public $inStock;

    /**
     * @var bool
     */
    public $isLastStock;

    /**
     * @var bool
     */
    public $unavailableBecauseOfMinPurchase;

    /**
     * @var bool
     */
    public $unavailableBecauseOfMaxPurchase;

    /**
     * @var bool
     */
    public $unavailableBecauseOfStock;

    /**
     * @var string
     */
    public $customReferenceNumber;

    /**
     * @var float
     */
    public $currencyFactor = self::DEFAULT_FACTOR;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->mode = new UnknownLineItemType(null);
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function isEditable(): bool
    {
        return $this->mode instanceof ProductLineItemType;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'reference_number' => $this->referenceNumber,
            'quantity' => $this->quantity,
            'comment' => (string) $this->comment,
            'amount' => $this->amount,
            'amount_net' => $this->amountNet,
            'mode' => $this->mode->getValue(),
            'sort' => $this->sort,
            'voucher_code' => $this->voucherCode,
            'currency_factor' => $this->currencyFactor,
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->referenceNumber = (string) $data['reference_number'];
        $this->quantity = (int) $data['quantity'];
        $this->comment = (string) $data['comment'];
        $this->amount = round($data['amount'], self::AMOUNT_PRECISION);
        $this->amountNet = round($data['amount_net'], self::AMOUNT_PRECISION);
        $this->mode = LineItemType::create($data['mode']);
        $this->sort = is_numeric($data['sort']) ? (int) $data['sort'] : null;
        $this->voucherCode = $data['voucher_code'];
        $this->currencyFactor = (float) $data['currency_factor'];

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->mode = LineItemType::create($this->mode);

        if ($this->quantity !== null) {
            $this->quantity = (int) $this->quantity;
        }

        if ($this->amount) {
            $this->amount = round((float) $this->amount, self::AMOUNT_PRECISION);
        }

        if ($this->amountNet) {
            $this->amountNet = round((float) $this->amountNet, self::AMOUNT_PRECISION);
        }

        if ($this->sort !== null) {
            $this->sort = (int) $this->sort;
        }

        if ($this->currencyFactor) {
            $this->currencyFactor = (float) $this->currencyFactor;
        }

        if ($this->referenceNumber !== null) {
            $this->referenceNumber = trim($this->referenceNumber);
        }

        if ($this->customReferenceNumber !== null) {
            $this->customReferenceNumber = trim($this->customReferenceNumber);
        }
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function setProductName(string $name = null): void
    {
        $this->name = $name;
    }

    public function getProductOrderNumber(): string
    {
        return $this->referenceNumber;
    }

    public function getCurrencyFactor(): float
    {
        return $this->currencyFactor;
    }

    public function setCurrencyFactor(float $factor): void
    {
        $this->currencyFactor = $factor;
    }

    public function getAmountPropertyNames(): array
    {
        return [
            'amount',
            'amountNet',
        ];
    }
}
