<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;

class LineItemListRepository
{
    const TABLE_NAME = 'b2b_line_item_list';
    const TABLE_ALIAS = 'lineItemList';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LineItemReferenceRepository
     */
    private $referenceRepository;

    public function __construct(
        Connection $connection,
        LineItemReferenceRepositoryInterface $referenceRepository
    ) {
        $this->connection = $connection;
        $this->referenceRepository = $referenceRepository;
    }

    /**
     * @deprecated tag:v4.4.0 - $currencyContext is no longer used
     */
    public function fetchOneListById(
        IdValue $listId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): LineItemList {
        $listData = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->andWhere(self::TABLE_ALIAS . '.context_owner_id = :contextOwner')
            ->setParameter('id', $listId->getStorageValue())
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue())
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$listData) {
            throw new NotFoundException('Unable to find list by id "' . $listId . '"');
        }

        $list = new LineItemList();
        $list->fromDatabaseArray($listData);
        $list->references = $this->referenceRepository->fetchList($listId, new LineItemReferenceSearchStruct());
        $list->recalculateListAmounts();

        return $list;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException
     */
    public function addList(LineItemList $list, OwnershipContext $ownershipContext): LineItemList
    {
        if (!$list->isNew()) {
            throw new CanNotInsertExistingRecordException('The list provided already exists');
        }

        $this->connection->insert(
            self::TABLE_NAME,
            array_merge(
                $list->toDatabaseArray(),
                ['context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue()]
            )
        );

        $list->id = IdValue::create($this->connection->lastInsertId());

        return $list;
    }

    public function updateListPrices(LineItemList $lineItemList, OwnershipContext $ownershipContext): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            [
                'amount_net' => $lineItemList->amountNet,
                'amount' => $lineItemList->amount,
            ],
            [
                'id' => $lineItemList->id->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );
    }

    /**
     * @throws CanNotRemoveExistingRecordException
     */
    public function removeLineItemList(LineItemList $lineItemList, OwnershipContext $ownershipContext): LineItemList
    {
        if ($lineItemList->isNew()) {
            throw new CanNotRemoveExistingRecordException('The line item list provided does not exist');
        }

        $this->removeLineItemListById($lineItemList->id, $ownershipContext);
        $lineItemList->id = IdValue::null();

        return $lineItemList;
    }

    public function removeLineItemListById(IdValue $listId, OwnershipContext $ownershipContext): void
    {
        $this->connection->delete(
            self::TABLE_NAME,
            [
                'id' => $listId->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );
    }
}
