<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use InvalidArgumentException;
use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Currency\Framework\CurrencyAware;
use function get_object_vars;
use function strcasecmp;

/**
 * @deprecated tag:v4.4.0 - CurrencyAware implementation will be removed
 */
class LineItemList implements CrudEntity, CurrencyAware
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var IdValue
     */
    public $contextOwnerId;

    /**
     * @var LineItemReference[]
     */
    public $references = [];

    /**
     * @var float
     */
    public $amount;

    /**
     * @var float
     */
    public $amountNet;

    /**
     * @var float
     * @deprecated tag:v4.4.0 - will be removed
     */
    public $currencyFactor = self::DEFAULT_FACTOR;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->contextOwnerId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'context_owner_id' => $this->contextOwnerId->getStorageValue(),
            'amount' => $this->amount,
            'amount_net' => $this->amountNet,
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->contextOwnerId = IdValue::create($data['context_owner_id']);
        $this->amount = (float) $data['amount'];
        $this->amountNet = (float) $data['amount_net'];

        return $this;
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getReferenceById(IdValue $id): LineItemReference
    {
        foreach ($this->references as $reference) {
            if ($reference->id->equals($id)) {
                return $reference;
            }
        }

        throw new InvalidArgumentException('The line item reference with id "' . $id . '" is not a part of this entity');
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getReferenceByNumber(string $number): LineItemReference
    {
        foreach ($this->references as $reference) {
            if (strcasecmp($reference->referenceNumber, $number) === 0) {
                return $reference;
            }
        }

        throw new InvalidArgumentException('The line item reference with reference number "' . $number . '" is not a part of this entity');
    }

    /**
     * @deprecated tag:v4.4.0 - will be removed
     */
    public function getCurrencyFactor(): float
    {
        return $this->currencyFactor;
    }

    /**
     * @deprecated tag:v4.4.0 - will be removed
     */
    public function setCurrencyFactor(float $factor): void
    {
        $this->currencyFactor = $factor;
    }

    /**
     * @deprecated tag:v4.4.0 - will be removed
     */
    public function getAmountPropertyNames(): array
    {
        return [
            'amount',
            'amountNet',
        ];
    }

    public function recalculateListAmounts(): void
    {
        $this->amount = 0;
        $this->amountNet = 0;

        foreach ($this->references as $reference) {
            $this->amount += (float) $reference->amount * (int) $reference->quantity;
            $this->amountNet += (float) $reference->amountNet * (int) $reference->quantity;
        }
    }
}
