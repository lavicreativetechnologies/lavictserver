<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\ProductName\Framework\ProductNameService;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_map;

class LineItemReferenceService
{
    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $lineItemReferenceRepository;

    /**
     * @var ProductNameService
     */
    private $productNameService;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    public function __construct(
        LineItemReferenceRepositoryInterface $lineItemReferenceRepository,
        ProductNameService $productNameService,
        ProductServiceInterface $productService
    ) {
        $this->lineItemReferenceRepository = $lineItemReferenceRepository;
        $this->productNameService = $productNameService;
        $this->productService = $productService;
    }

    /**
     * @param $searchStruct
     * @return LineItemReference[]
     * @deprecated tag:v4.1 $ownershipContext is no longer used
     */
    public function fetchLineItemsReferencesWithProductNames(
        IdValue $listId,
        $searchStruct,
        OwnershipContext $ownershipContext
    ): array {
        $lineItemReferences = $this->lineItemReferenceRepository
            ->fetchList($listId, $searchStruct);

        $this->productNameService->translateProductNames($lineItemReferences);

        return $lineItemReferences;
    }

    public function fetchLineItemListProductNames(LineItemList $lineItemList): LineItemList
    {
        $this->productNameService->translateProductNames($lineItemList->references);

        return $lineItemList;
    }

    public function getReferencesWithStock(array $references): array
    {
        $orderNumbers = array_map(
            function (LineItemReference $reference) {
                return $reference->referenceNumber;
            },
            $references
        );

        $stocks = $this->productService->fetchStocksByOrderNumbers($orderNumbers);

        foreach ($references as $reference) {
            if (!isset($stocks[$reference->referenceNumber])) {
                continue;
            }

            $reference->inStock = $stocks[$reference->referenceNumber]['inStock'];
            $reference->isLastStock = $stocks[$reference->referenceNumber]['isLastStock'];
        }

        return $references;
    }
}
