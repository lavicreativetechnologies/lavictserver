<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Currency\Framework\DependencyInjection\CurrencyFrameworkConfiguration;
use Shopware\B2B\LineItemList\BridgePlatform\DependencyInjection\LineItemListBridgeConfiguration as PlatformLineItemListBridgeConfiguration;
use Shopware\B2B\Shop\Framework\DependencyInjection\ShopFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class LineItemListBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformLineItemListBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new ShopFrameworkConfiguration(),
            new CurrencyFrameworkConfiguration(),
        ];
    }
}
