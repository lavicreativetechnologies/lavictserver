<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use InvalidArgumentException;
use Shopware\B2B\Cart\BridgePlatform\CartLineItemListConverter;
use Shopware\B2B\LineItemList\Framework\LineItemCheckoutProviderInterface;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListSource;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use function array_values;
use function in_array;
use function strcasecmp;

class LineItemCheckoutProvider implements LineItemCheckoutProviderInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartLineItemListConverter
     */
    private $cartLineItemListConverter;

    /**
     * @var AuthenticationService
     */
    private $authService;

    public function __construct(
        ContextProvider $contextProvider,
        SalesChannelRepositoryInterface $productRepository,
        CartService $cartService,
        CartLineItemListConverter $cartLineItemListConverter,
        AuthenticationService $authService
    ) {
        $this->contextProvider = $contextProvider;
        $this->productRepository = $productRepository;
        $this->cartService = $cartService;
        $this->cartLineItemListConverter = $cartLineItemListConverter;
        $this->authService = $authService;
    }

    public function createList(LineItemListSource $source): LineItemList
    {
        $source = $this->testSource($source);

        $list = new LineItemList();
        $list->references = $this->extendReferencesFromCart($source);
        $list->recalculateListAmounts();

        return $list;
    }

    public function createListFromCartId(string $token): LineItemList
    {
        $context = $this->contextProvider->getSalesChannelContext();
        $cart = $this->cartService->getCart($token, $context);

        return $this->cartLineItemListConverter
            ->cartToLineItemList($cart, $this->authService->getIdentity()->getOwnershipContext());
    }

    public function updateList(LineItemList $list, LineItemListSource $lineItemListSources): LineItemList
    {
        $source = $this->testSource($lineItemListSources);

        $list->references = $this->extendReferencesFromCart($source, $list->references);
        $list->recalculateListAmounts();

        return $list;
    }

    /**
     * @internal
     */
    protected function extendReferencesFromCart(LineItemCheckoutSource $source, array $references = []): array
    {
        $cart = $source->cart;
        $orderNumbers = [];

        $lineItems = $cart->getLineItems()->getElements();
        $productReferenceIds = $cart->getLineItems()->fmap(function (LineItem $lineItem) {
            if ($lineItem->getType() !== LineItem::PRODUCT_LINE_ITEM_TYPE) {
                return;
            }

            return $lineItem->getReferencedId();
        });

        $context = $this->contextProvider->getSalesChannelContext();
        $products = $this->fetchProducts($productReferenceIds, $context);

        foreach ($lineItems as $lineItem) {
            $product = $products->get($lineItem->getReferencedId());
            if ($product) {
                $orderNumbers[] = $orderNumber = $product->getProductNumber();
            } else {
                $orderNumbers[] = $orderNumber = $lineItem->getReferencedId();
            }

            $reference = $this->findReference($references, $orderNumber);

            $reference->referenceNumber = $orderNumber;
            $reference->quantity = $lineItem->getQuantity();
            $reference->mode = LineItemType::create($lineItem->getType());
            $comment = $lineItem->getExtension(LineItemShopWriterService::ORDER_LINE_ITEM_COMMENT);
            $reference->comment = $comment instanceof OrderLineItemCommentStruct ? $comment->getComment() : null;

            $this->setReferenceAmounts(
                $reference,
                $lineItem->getPrice(),
                $context
            );
        }

        foreach ($references as $index => $reference) {
            if (!in_array($reference->referenceNumber, $orderNumbers, true)) {
                unset($references[$index]);
            }
        }

        return array_values($references);
    }

    /**
     * @internal
     */
    protected function setReferenceAmounts(
        LineItemReference $reference,
        CalculatedPrice $price,
        SalesChannelContext $context
    ): void {
        $isGross = $context
            ->getCurrentCustomerGroup()
            ->getDisplayGross();

        $unitPrice = $price->getUnitPrice();
        $calculatedUnitTaxes = $price->getCalculatedTaxes()->getAmount() / $price->getQuantity();

        if ($isGross) {
            $reference->amount = $unitPrice;
            $reference->amountNet = $unitPrice - $calculatedUnitTaxes;
        } else {
            $reference->amountNet = $unitPrice;
            $reference->amount = $unitPrice + $calculatedUnitTaxes;
        }
    }

    /**
     * @internal
     */
    protected function testSource(LineItemListSource $source): LineItemCheckoutSource
    {
        if (!$source instanceof LineItemCheckoutSource) {
            throw new InvalidArgumentException('Invalid source class provided');
        }

        return $source;
    }

    /**
     * @internal
     */
    protected function findReference(array &$references, string $orderNumber): LineItemReference
    {
        foreach ($references as $reference) {
            if (strcasecmp($reference->referenceNumber, $orderNumber) === 0) {
                return $reference;
            }
        }

        $reference = new LineItemReference();
        $references[] = $reference;

        return $reference;
    }

    /**
     * @internal
     */
    protected function fetchProducts(array $referenceIds, SalesChannelContext $context): ProductCollection
    {
        if (empty($referenceIds)) {
            return new ProductCollection();
        }

        $criteria = new Criteria($referenceIds);
        $criteria->addAssociation('prices');

        return $this->productRepository->search($criteria, $context)->getEntities();
    }
}
