<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\Core\Framework\Struct\Struct;

class OrderLineItemCommentStruct extends Struct
{
    /**
     * @var string|null
     */
    protected $comment;

    public function __construct(?string $comment)
    {
        $this->comment = $comment;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getApiAlias(): string
    {
        return 'b2b_order_line_item_comment';
    }
}
