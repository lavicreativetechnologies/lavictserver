<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class LineItemReferenceMailData
{
    /**
     * @var LineItemReference
     */
    private $reference;

    public function __construct(LineItemReference $reference)
    {
        $this->reference = $reference;
    }

    public static function getReferenceDataType(): EventDataType
    {
        $reference = new ObjectType();
        $reference->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $reference->add('referenceNumber', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $reference->add('name', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $reference->add('quantity', new ScalarValueType(ScalarValueType::TYPE_INT));
        $reference->add('comment', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $reference->add('amount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $reference->add('amountNet', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $reference->add('mode', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $reference->add('sort', new ScalarValueType(ScalarValueType::TYPE_INT));
        $reference->add('voucherCode', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $reference->add('maxPurchase', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $reference->add('minPurchase', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $reference->add('purchaseStep', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $reference->add('inStock', new ScalarValueType(ScalarValueType::TYPE_INT));
        $reference->add('isLastStock', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $reference->add('unavailableBecauseOfMinPurchase', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $reference->add('unavailableBecauseOfMaxPurchase', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $reference->add('unavailableBecauseOfStock', new ScalarValueType(ScalarValueType::TYPE_BOOL));

        return $reference;
    }

    public function getId(): string
    {
        return (string) $this->reference->id->getValue();
    }

    public function getReferenceNumber(): string
    {
        return $this->reference->referenceNumber;
    }

    public function getName(): string
    {
        return $this->reference->name;
    }

    public function getQuantity(): int
    {
        return $this->reference->quantity;
    }

    public function getComment(): string
    {
        return $this->reference->comment;
    }

    public function getAmount(): float
    {
        return $this->reference->amount;
    }

    public function getAmountNet(): float
    {
        return $this->reference->amountNet;
    }

    public function getMode(): string
    {
        return (string) $this->reference->mode->getValue();
    }

    public function getSort(): int
    {
        return $this->reference->sort;
    }

    public function getVoucherCode(): string
    {
        return $this->reference->voucherCode;
    }

    public function getMaxPurchase(): float
    {
        return $this->reference->maxPurchase;
    }

    public function getMinPurchase(): float
    {
        return $this->reference->minPurchase;
    }

    public function getPurchaseStep(): float
    {
        return $this->reference->purchaseStep;
    }

    public function getInStock(): int
    {
        return $this->reference->inStock;
    }

    public function isLastStock(): bool
    {
        return $this->reference->isLastStock;
    }

    public function isUnavailableBecauseOfMinPurchase(): bool
    {
        return $this->reference->unavailableBecauseOfMinPurchase;
    }

    public function isUnavailableBecauseOfMaxPurchase(): bool
    {
        return $this->reference->unavailableBecauseOfMaxPurchase;
    }

    public function isUnavailableBecauseOfStock(): bool
    {
        return $this->reference->unavailableBecauseOfStock;
    }
}
