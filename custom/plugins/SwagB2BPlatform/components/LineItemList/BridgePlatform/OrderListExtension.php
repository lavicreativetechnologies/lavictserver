<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\Core\Framework\Struct\Struct;

class OrderListExtension extends Struct
{
    /**
     * @var IdValue
     */
    public $orderListId;

    /**
     * @var string
     */
    public $orderListName;
}
