<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\B2B\LineItemList\Framework\LineItemListSource;
use Shopware\Core\Checkout\Cart\Cart;

class LineItemCheckoutSource implements LineItemListSource
{
    /**
     * @var Cart
     */
    public $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }
}
