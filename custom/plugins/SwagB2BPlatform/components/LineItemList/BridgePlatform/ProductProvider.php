<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Doctrine\DBAL\Connection;
use InvalidArgumentException;
use NumberFormatter;
use Shopware\B2B\LineItemList\Framework\InvalidProductFromShopException;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Shopware\B2B\LineItemList\Framework\UnsupportedQuantityException;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Content\Product\Aggregate\ProductPrice\ProductPriceEntity;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use function array_keys;
use function array_map;
use function count;
use function current;
use function in_array;
use function numfmt_create;
use function numfmt_parse;
use function sprintf;
use function strcasecmp;

class ProductProvider implements ProductProviderInterface
{
    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var AuthenticationService
     */
    private $storefrontAuthenticationService;

    public function __construct(
        SalesChannelRepositoryInterface $productRepository,
        Connection $connection,
        ContextProvider $contextProvider,
        AuthenticationService $storefrontAuthenticationService
    ) {
        $this->productRepository = $productRepository;
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
        $this->storefrontAuthenticationService = $storefrontAuthenticationService;
    }

    public function updateList(LineItemList $list): void
    {
        $productNumbers = array_map(function (LineItemReference $reference) {
            return $reference->referenceNumber;
        }, $list->references);

        $products = $this->getProductList($productNumbers);

        foreach ($productNumbers as $productNumber) {
            $reference = $this->findReferenceInList($list, $productNumber);

            if (!$reference->mode instanceof ProductLineItemType) {
                continue;
            }

            $this->updateReferenceFromProduct($products, $productNumber, $reference);
        }

        $list->recalculateListAmounts();
    }

    public static function convertPriceToLocale(string $price, string $locale = 'en_EN'): float
    {
        $formatter = numfmt_create($locale, NumberFormatter::DEFAULT_STYLE);

        $formattedPrice = numfmt_parse($formatter, $price);

        if (!$formattedPrice) {
            throw new InvalidArgumentException('Wrong price format given');
        }

        return (float) $formattedPrice;
    }

    public function updateReference(LineItemReference $lineItemReference): void
    {
        $product = $this->getProductList([$lineItemReference->referenceNumber]);

        if (!$product) {
            $productId = $this->getProductIdFromCustomOrderNumber($lineItemReference->referenceNumber);

            if (!$productId) {
                return;
            }

            $product = $this->productRepository
                ->search((new Criteria([$productId]))
                    ->addAssociation('prices'), $this->contextProvider->getSalesChannelContext())->getElements();
        }

        $this->updateReferenceFromProduct($product, $lineItemReference->referenceNumber, $lineItemReference);
    }

    public function isProduct(string $productNumber): bool
    {
        $products = $this->getProductList([$productNumber]);

        return count($products) > 0;
    }

    public function setMaxMinAndSteps(LineItemReference $lineItemReference): void
    {
        $purchaseStep = 1;
        $minPurchase = 1;

        $product = current($this->getProductList([$lineItemReference->referenceNumber]));

        if ($product) {
            if ($max = $product->getMaxPurchase()) {
                $lineItemReference->maxPurchase = $max;
            }

            if ($step = $product->getPurchaseSteps()) {
                $purchaseStep = $step;
            }

            if ($min = $product->getMinPurchase()) {
                $minPurchase = $min;
            }
        }

        $lineItemReference->minPurchase = $minPurchase;
        $lineItemReference->purchaseStep = $purchaseStep;
    }

    public function setMaxMinAndStepsForItems(array $lineItemReferences): void
    {
        $numbers = [];
        foreach ($lineItemReferences as $lineItemReference) {
            $numbers[$lineItemReference->referenceNumber] = $lineItemReference;
        }

        $products = $this->getProductList(array_keys($numbers));

        foreach ($products as $product) {
            $productNumber = $product->getProductNumber();
            if ($stock = $product->getStock()) {
                $numbers[$productNumber]->inStock = $stock;
                $numbers[$productNumber]->isLastStock = $product->getIsCloseout();

                if ($max = $product->getMaxPurchase()) {
                    $numbers[$productNumber]->maxPurchase = $max;
                }

                if ($step = $product->getPurchaseSteps()) {
                    $numbers[$productNumber]->purchaseStep = $step;
                }

                if ($min = $product->getMinPurchase()) {
                    $numbers[$productNumber]->minPurchase = $min;
                }
            }
        }
    }

    /**
     * @return ProductEntity[]
     */
    protected function getProductList(array $productNumbers): array
    {
        $criteria = new Criteria();
        $criteria->addAssociation('prices');
        $criteria->addAssociation(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME);
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_OR, [
            new EqualsAnyFilter('productNumber', $productNumbers),
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsAnyFilter('product.b2bOrderNumber.customOrderNumber', $productNumbers),
                new EqualsFilter(
                    'product.b2bOrderNumber.contextOwnerId',
                    $this->storefrontAuthenticationService->getIdentity()->getContextAuthId()
                ),
            ]),
        ]));

        return $this->productRepository->search(
            $criteria,
            $this->contextProvider->getSalesChannelContext()
        )->getElements();
    }

    protected function findReferenceInList(LineItemList $list, string $productNumber): LineItemReference
    {
        $foundIndex = false;
        foreach ($list->references as $index => $currentReference) {
            if (strcasecmp($currentReference->referenceNumber, $productNumber) === 0) {
                $foundIndex = $index;
                break;
            }
        }

        return $list->references[$foundIndex];
    }

    /**
     * @param ProductEntity[] $products
     */
    protected function updateReferenceFromProduct(array $products, string $productNumber, LineItemReference $reference): void
    {
        $product = null;
        $productId = $this->getProductIdFromCustomOrderNumber($productNumber);

        foreach ($products as $currentProduct) {
            if ($currentProduct->getProductNumber() === $productNumber) {
                $product = $currentProduct;
                break;
            }

            if ($productId === $currentProduct->getId()) {
                $product = $currentProduct;
                break;
            }
        }

        if (!$product) {
            return;
        }

        $reference->referenceNumber = $product->getProductNumber();
        $this->determinePriceForReference($reference, $product);
    }

    protected function getProductIdFromCustomOrderNumber(string $productNumber): string
    {
        return (string) $this->connection
            ->createQueryBuilder()
            ->select('LOWER(HEX(product_id))')
            ->from('b2b_order_number')
            ->where('custom_ordernumber = :productNumber')
            ->setParameter('productNumber', $productNumber)
            ->execute()
            ->fetchColumn();
    }

    protected function determinePriceForReference(LineItemReference $reference, ProductEntity $product): void
    {
        if (!$reference->quantity) {
            throw new UnsupportedQuantityException('The quantity is not supported');
        }

        try {
            $prices = $this->getPriceRuleForQuantity($product, $reference->quantity)
                ->getPrice();
        } catch (InvalidProductFromShopException $exception) {
            $prices = $product->getPrice();
        }

        if (!$prices || !$prices->has($this->contextProvider->getContext()->getCurrencyId())) {
            throw new InvalidProductFromShopException(sprintf('Product %s does not have a valid price', $product->getProductNumber()));
        }

        $price = $prices->get($this->contextProvider->getContext()->getCurrencyId());

        $reference->amount = $price->getGross();
        $reference->amountNet = $price->getNet();
    }

    protected function getPriceRuleForQuantity(ProductEntity $product, int $quantity): ProductPriceEntity
    {
        $prices = $product->getPrices()->getElements();

        return $this->getSuitablePrice($prices, $quantity);
    }

    /**
     * @param ProductPriceEntity[] $prices
     */
    protected function getSuitablePrice(array $prices, int $quantity): ProductPriceEntity
    {
        foreach ($prices as $price) {
            if (($price->getQuantityStart() === null || $price->getQuantityStart() <= $quantity)
                && ($price->getQuantityEnd() === null || $price->getQuantityEnd() >= $quantity)
                && in_array($price->getRuleId(), $this->contextProvider->getSalesChannelContext()->getRuleIds(), true)) {
                return $price;
            }
        }

        throw new InvalidProductFromShopException('Product without prices can not have a suitable price');
    }

    /**
     * @deprecated tag:v4.4.0 - Function will be removed. Use {@see Shopware\B2B\LineItemList\Framework\LineItemList::recalculateListAmounts} instead.
     */
    public function updateListWithoutUpdatingProductReferences(LineItemList $list): void
    {
        $list->recalculateListAmounts();
    }
}
