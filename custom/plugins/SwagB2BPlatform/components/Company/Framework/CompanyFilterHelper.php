<?php declare(strict_types=1);

namespace Shopware\B2B\Company\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use RuntimeException;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use function sprintf;

class CompanyFilterHelper
{
    public function applyFilter(
        AclReadHelper $readHelper,
        CompanyAssignmentFilter $assignmentFilter,
        CompanyFilterStruct $filterStruct,
        QueryBuilder $queryBuilder,
        CompanyInheritanceFilter $companyInheritanceFilter
    ): void {
        if (!$filterStruct->aclGrantContext) {
            return;
        }

        switch ($filterStruct->companyFilterType) {
            case CompanyFilterStruct::TYPE_VISIBILITY:
                $readHelper->applyAclFilter($filterStruct, $queryBuilder);

                return;
            case CompanyFilterStruct::TYPE_ASSIGNMENT:
                $assignmentFilter->applyFilter($filterStruct, $queryBuilder);

                return;
            case CompanyFilterStruct::TYPE_INHERITANCE:
                $companyInheritanceFilter->applyFilter($filterStruct, $queryBuilder);

                return;
        }

        throw new RuntimeException(
            sprintf('Unable to determine the filter method %s.', $filterStruct->companyFilterType)
        );
    }
}
