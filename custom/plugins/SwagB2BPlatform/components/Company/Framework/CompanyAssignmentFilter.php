<?php declare(strict_types=1);

namespace Shopware\B2B\Company\Framework;

use Doctrine\DBAL\Query\QueryBuilder;

interface CompanyAssignmentFilter
{
    public function applyFilter(CompanyFilterStruct $filterStruct, QueryBuilder $queryBuilder);
}
