<?php declare(strict_types=1);

namespace Shopware\B2B\Company\Frontend;

class CompanyController
{
    public function indexAction(): array
    {
        return [];
    }

    public function defaultTabAction(): array
    {
        return [];
    }
}
