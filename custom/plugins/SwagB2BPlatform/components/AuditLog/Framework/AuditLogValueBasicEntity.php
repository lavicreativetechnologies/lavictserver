<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

use function array_keys;
use function get_object_vars;
use function in_array;

abstract class AuditLogValueBasicEntity
{
    public function setData(array $data): void
    {
        $properties = array_keys($this->toArray());

        foreach ($data as $key => $value) {
            if (false === in_array($key, $properties, true)) {
                continue;
            }

            $this->{$key} = $value;
        }
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    abstract public function getTemplateName(): string;

    abstract public function isChanged(): bool;
}
