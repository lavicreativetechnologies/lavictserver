<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use function array_keys;
use function get_object_vars;
use function in_array;

class AuditLogIndexEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var IdValue
     */
    public $auditLogId;

    /**
     * @var string
     */
    public $referenceTable;

    /**
     * @var IdValue
     */
    public $referenceId;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->auditLogId = IdValue::null();
        $this->referenceId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'audit_log_id' => $this->auditLogId->getStorageValue(),
            'reference_table' => $this->referenceTable,
            'reference_id' => $this->referenceId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->auditLogId = IdValue::create($data['audit_log_id']);
        $this->referenceTable = $data['reference_table'];
        $this->referenceId = IdValue::create($data['reference_id']);

        return $this;
    }

    public function setData(array $data): void
    {
        $properties = array_keys($this->toArray());

        foreach ($data as $key => $value) {
            if (false === in_array($key, $properties, true)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->auditLogId = IdValue::create($this->auditLogId);
        $this->referenceId = IdValue::create($this->referenceId);
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
