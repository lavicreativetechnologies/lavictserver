<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

interface BackendProviderInterface
{
    public function getBackendUser(): AuditLogAuthorEntity;
}
