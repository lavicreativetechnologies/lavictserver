<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function array_keys;
use function get_object_vars;
use function in_array;
use function serialize;
use function unserialize;

class AuditLogEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var AuditLogValueBasicEntity
     */
    public $logValue;

    /**
     * @var string
     */
    public $logType;

    /**
     * @var string
     */
    public $eventDate;

    /**
     * @var string
     */
    public $authorHash;

    /**
     * @var Identity
     */
    public $authorIdentity;

    /**
     * @var IdValue
     */
    public $writeWithId;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->writeWithId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'log_value' => serialize($this->logValue),
            'log_type' => $this->logType,
            'author_hash' => $this->authorHash,
            'write_with_id' => $this->writeWithId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->logType = $data['log_type'];
        $this->logValue = unserialize($data['log_value'], [true]);
        $this->eventDate = $data['event_date'];
        $this->authorHash = $data['author_hash'];
        $this->writeWithId = IdValue::create($data['write_with_id']);

        return $this;
    }

    public function setData(array $data): void
    {
        $properties = array_keys($this->toArray());

        foreach ($data as $key => $value) {
            if (false === in_array($key, $properties, true)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->writeWithId = IdValue::create($this->writeWithId);
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
