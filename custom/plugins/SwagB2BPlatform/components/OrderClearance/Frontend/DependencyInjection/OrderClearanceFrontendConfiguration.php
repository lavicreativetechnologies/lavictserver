<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Frontend\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\OrderClearance\Framework\DependencyInjection\OrderClearanceFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderClearanceFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new OrderClearanceFrameworkConfiguration(),
        ];
    }
}
