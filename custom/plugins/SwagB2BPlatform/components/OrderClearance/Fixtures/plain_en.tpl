{include file="string:{config name=emailheaderplain}"}

Hello {$salutation|salutation} {$lastname},

Your order with the order number {$orderNumber} has been accepted!

You can also check the current status of your order on our website under "My Account" - "Orders" at any time.

{include file="string:{config name=emailfooterplain}"}
