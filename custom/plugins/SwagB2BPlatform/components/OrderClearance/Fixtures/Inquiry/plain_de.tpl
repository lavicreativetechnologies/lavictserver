{include file="string:{config name=emailheaderplain}"}

Hallo {$debtorSalutation|salutation} {$debtorLastName},

Ihr Kontakt "{$contactFirstName} {$contactLastName}" hat eine Bestellfreigabe beantragt!

Zum Bearbeiten der Bestellfreigabe können Sie jederzeit auf {config name=shopName} unter "Mein Konto" -
"Bestellungen" im Bereich der Bestellfreigaben diese abrufen.

Im Freigabeprozess haben Sie die Möglichkeit die Bestellung vollständig einzusehen, zu bearbeiten, anzunehmen und abzulehnen.

{include file="string:{config name=emailfooterplain}"}
