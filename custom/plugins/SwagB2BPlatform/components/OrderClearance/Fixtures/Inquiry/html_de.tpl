<div style="font-family:arial; font-size:12px;">
    {include file="string:{config name=emailheaderhtml}"}
    <br/>
    <p>
        Hallo {$debtorSalutation|salutation} {$debtorLastName},<br/>
        <br/>
        Ihr Kontakt "{$contactFirstName} {$contactLastName}" hat eine Bestellfreigabe beantragt!<br/>
        <br/>
        Zum Bearbeiten der Bestellfreigabe können Sie jederzeit auf {config name=shopName} unter "Mein Konto" -
        "Bestellungen" im Bereich der Bestellfreigaben diese abrufen.<br/>
        <br/>
        Im Freigabeprozess haben Sie die Möglichkeit die Bestellung vollständig einzusehen, zu bearbeiten, anzunehmen und abzulehnen.
    </p>
    {include file="string:{config name=emailfooterhtml}"}
</div>
