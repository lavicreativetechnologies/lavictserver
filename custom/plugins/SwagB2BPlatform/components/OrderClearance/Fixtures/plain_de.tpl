{include file="string:{config name=emailheaderplain}"}

Hallo {$salutation|salutation} {$lastname},

Ihre Bestellung mit der Bestellnummer {$orderNumber} wurde akzeptiert!

Den aktuellen Status Ihrer Bestellung können Sie auch jederzeit auf unserer Webseite im Bereich "Mein Konto" - "Bestellungen" abrufen.

{include file="string:{config name=emailfooterplain}"}
