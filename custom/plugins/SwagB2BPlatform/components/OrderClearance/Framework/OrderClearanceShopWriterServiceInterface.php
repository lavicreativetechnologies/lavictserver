<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

interface OrderClearanceShopWriterServiceInterface
{
    public function sendToClearance(OrderClearanceEntity $orderClearance);

    /**
     * @deprecated tag:v4.4.0 Method will return {@see OrderClearanceEntity}
     */
    public function stopOrderClearance();
}
