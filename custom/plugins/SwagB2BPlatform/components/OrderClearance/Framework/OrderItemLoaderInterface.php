<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface OrderItemLoaderInterface
{
    /**
     * @return OrderItemEntity[]
     */
    public function fetchItemsFromStorage(OrderClearanceEntity $itemEntity, OwnershipContext $ownershipContext): array;

    /**
     * @return OrderItemEntity[]
     */
    public function fetchItemsFromBasketArray(OrderClearanceEntity $itemEntity, OwnershipContext $ownershipContext): array;
}
