<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

use Shopware\B2B\Common\Entity;

abstract class OrderItemEntity implements Entity
{
    /**
     * @deprecated tag:v4.2.0 will be removed since it is not used across all implementations
     */
    public $identifier;
}
