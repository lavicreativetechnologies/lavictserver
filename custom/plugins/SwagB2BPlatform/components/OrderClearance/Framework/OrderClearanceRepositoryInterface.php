<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Order\Framework\OrderRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * @deprecated tag:v4.2.0 use OrderClearanceRepository instead
 */
interface OrderClearanceRepositoryInterface extends GridRepository
{
    const TABLE_NAME = 'b2b_order_context';

    const TABLE_ALIAS = 'b2bOrder';

    const CROSS_TABLE_COLUMNS = [
        'contact' => 'debtorContact.email',
    ];

    /**
     * @deprecated tag:v4.2.0 use OrderClearanceRepository::STATE_CLEARANCE_OPEN
     */
    const STATUS_ORDER_CLEARANCE = self::STATE_CLEARANCE_OPEN;

    /**
     * @deprecated tag:v4.2.0 use OrderRepository::STATE_ORDER_OPEN
     */
    const STATUS_ORDER_OPEN = OrderRepository::STATE_ORDER_OPEN;

    /**
     * @deprecated tag:v4.2.0 use OrderClearanceRepository::STATE_CLEARANCE_DENIED
     */
    const STATUS_ORDER_DENIED = self::STATE_CLEARANCE_DENIED;

    const STATE_CLEARANCE_OPEN = 'orderclearance_open';

    const STATE_CLEARANCE_DENIED = 'orderclearance_denied';

    public function fetchOneByOrderContextId(IdValue $orderContextId, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): OrderClearanceEntity;

    /**
     * @deprecated tag:v4.2.0 no longer used
     */
    public function getStatusIdValueByClassicStatus(int $status): IdValue;

    /**
     * @return OrderClearanceEntity[]
     */
    public function fetchAllOrderClearances(Identity $identity, OrderClearanceSearchStruct $searchStruct, CurrencyContext $currencyContext): array;

    public function fetchTotalCount(Identity $identity, OrderClearanceSearchStruct $searchStruct): int;

    public function belongsOrderContextIdToDebtor(Identity $identity, IdValue $orderContextId): bool;

    /**
     * @deprecated tag:v4.2.0 use declineOrderClearance instead
     */
    public function declineOrder(IdValue $orderContextId, string $comment, OwnershipContext $ownershipContext);

    public function sendToOrderClearance(IdValue $orderContextId, OwnershipContext $ownershipContext);

    public function deleteOrder(IdValue $orderContextId, OwnershipContext $ownershipContext);

    public function setStatusToOrderClearance(OrderClearanceEntity $orderClearance);

    public function acceptOrderClearance(OrderClearanceEntity $orderClearance, OwnershipContext $clearingUser): void;

    public function declineOrderClearance(IdValue $orderContextId, string $comment): void;
}
