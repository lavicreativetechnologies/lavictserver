<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use InvalidArgumentException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository as FrameworkOrderClearanceRepository;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface;
use Shopware\Core\Checkout\Order\OrderStates;

/**
 * @deprecated tag:v4.2.0 will be removed use Framework\OrderClearanceRepository instead
 */
class OrderClearanceRepository extends FrameworkOrderClearanceRepository implements OrderClearanceRepositoryInterface
{
    public function getStatusIdValueByClassicStatus(int $status): IdValue
    {
        $statusMap = [
            -3 => self::STATE_CLEARANCE_DENIED,
            -2 => self::STATE_CLEARANCE_OPEN,
            -1 => OrderStates::STATE_CANCELLED,
            0 => OrderStates::STATE_OPEN,
            1 => OrderStates::STATE_IN_PROGRESS,
            2 => OrderStates::STATE_COMPLETED,
            4 => OrderStates::STATE_CANCELLED,
        ];

        if (!isset($statusMap[$status])) {
            throw new InvalidArgumentException('The status with the id ' . $status . ' is wrong!');
        }

        return $this->fetchStatusIdByTechnicalName($statusMap[$status]);
    }

    protected function fetchStatusIdByTechnicalName(string $name): IdValue
    {
        return IdValue::create(
            $this->connection->createQueryBuilder()
                ->select('state.id')
                ->from('state_machine_state', 'state')
                ->innerJoin('state', 'state_machine', 'stateMachine', 'state.state_machine_id = stateMachine.id')
                ->where('state.technical_name = :name')
                ->andWhere('stateMachine.technical_name = :stateMachineName')
                ->setParameter('name', $name)
                ->setParameter('stateMachineName', OrderStates::STATE_MACHINE)
                ->execute()
                ->fetchColumn()
        );
    }
}
