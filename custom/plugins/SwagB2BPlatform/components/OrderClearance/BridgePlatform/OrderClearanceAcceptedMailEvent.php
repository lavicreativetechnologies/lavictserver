<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\BusinessEventInterface;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;
use Shopware\Core\Framework\Event\MailActionInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\EventDispatcher\Event;

class OrderClearanceAcceptedMailEvent extends Event implements BusinessEventInterface, MailActionInterface
{
    public const EVENT_NAME = 'b2b.order.clearance.accepted';

    /**
     * @var OrderClearanceEntity
     */
    private $clearanceEntity;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var SalesChannelContext
     */
    private $salesChannelContext;

    public function __construct(OrderClearanceEntity $clearanceEntity, string $orderNumber, SalesChannelContext $salesChannelContext)
    {
        $this->clearanceEntity = $clearanceEntity;
        $this->orderNumber = $orderNumber;
        $this->salesChannelContext = $salesChannelContext;
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('clearance', OrderClearanceEntityMailData::getOrderContextData())
            ->add('orderNumber', new ScalarValueType(ScalarValueType::TYPE_STRING));
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        $postalSettings = $this->clearanceEntity->userPostalSettings;

        return new MailRecipientStruct([
            $postalSettings->email => $postalSettings->firstName . ' ' . $postalSettings->lastName,
        ]);
    }

    public function getSalesChannelId(): ?string
    {
        return $this->salesChannelContext->getSalesChannel()->getId();
    }

    public function getContext(): Context
    {
        return $this->salesChannelContext->getContext();
    }

    public function getClearance(): OrderClearanceEntityMailData
    {
        return new OrderClearanceEntityMailData($this->clearanceEntity);
    }

    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }
}
