<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use Shopware\B2B\Order\BridgePlatform\OrderContextStateChangedEvent;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Order\OrderStates;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderAuditLogSubscriber implements EventSubscriberInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderClearanceService
     */
    private $orderClearanceService;

    public function __construct(
        AuthenticationService $authenticationService,
        OrderClearanceService $orderClearanceService
    ) {
        $this->authenticationService = $authenticationService;
        $this->orderClearanceService = $orderClearanceService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            OrderContextStateChangedEvent::class => 'writeLogEntry',
        ];
    }

    public function writeLogEntry(OrderContextStateChangedEvent $event): void
    {
        $orderContext = $event->getOrderContext();

        if (!$this->authenticationService->isB2b()) {
            return;
        }

        if (OrderStates::STATE_OPEN !== $event->getNewStatus()) {
            return;
        }

        if (OrderClearanceRepository::STATE_CLEARANCE_OPEN !== $event->getOldStatus()) {
            return;
        }

        $this->orderClearanceService
            ->createOrderAcceptedStatusChangeLogEntry($orderContext->id, $this->authenticationService->getIdentity());
    }
}
