<?php declare(strict_types=1);

namespace Shopware\B2B\Account\Framework;

use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

interface AccountRepositoryInterface
{
    public function hasNewsletter(Identity $identity): bool;
}
