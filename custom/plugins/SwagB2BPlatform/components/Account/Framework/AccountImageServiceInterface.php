<?php declare(strict_types=1);

namespace Shopware\B2B\Account\Framework;

use Shopware\B2B\Common\IdValue;
use Symfony\Component\HttpFoundation\File\File;

interface AccountImageServiceInterface
{
    /**
     * Upload a new avatar image
     */
    public function uploadImage(IdValue $authId, File $uploadedFile): array;

    public function getAvatarByAuthId(IdValue $authId): string;
}
