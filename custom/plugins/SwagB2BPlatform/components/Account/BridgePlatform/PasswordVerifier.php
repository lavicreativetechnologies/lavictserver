<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\UserLoginCredentials;
use Shopware\Core\Checkout\Customer\Password\LegacyEncoder\LegacyEncoderInterface;
use function mb_strlen;
use function password_verify;
use function strcasecmp;

class PasswordVerifier
{
    /**
     * @var iterable|LegacyEncoderInterface[]
     */
    private $encoder;

    /**
     * @var int
     */
    private $minPasswordLength;

    /**
     * @param LegacyEncoderInterface[] $encoder
     */
    public function __construct(
        iterable $encoder,
        int $minPasswordLength
    ) {
        $this->encoder = $encoder;
        $this->minPasswordLength = $minPasswordLength;
    }

    public function verifyPassword(string $currentPassword, UserLoginCredentials $loginCredentials): void
    {
        if (!$this->isPasswordValid($currentPassword, $loginCredentials)) {
            throw new InvalidPasswordException('InvalidCurrentPassword');
        }
    }

    protected function isPasswordValid(string $currentPassword, UserLoginCredentials $loginCredentials): bool
    {
        foreach ($this->encoder as $encoder) {
            if (strcasecmp($encoder->getName(), $loginCredentials->encoder)) {
                continue;
            }

            return $encoder->isPasswordValid($currentPassword, $loginCredentials->password);
        }

        return password_verify($currentPassword, $loginCredentials->password);
    }

    public function verifyNewPasswordConstraints(string $newPassword): void
    {
        if (mb_strlen($newPassword) < $this->minPasswordLength) {
            throw new TooShortPasswordException($this->minPasswordLength);
        }
    }
}
