<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class NewsletterRepository
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function clearNewsletterRecipient(Identity $identity): void
    {
        $this->connection->delete(
            'newsletter_recipient',
            [
                'email' => $identity->getPostalSettings()->email,
                ]
        );
    }

    public function setNewsletterFlag(Identity $identity, bool $newsletter): void
    {
        $this->connection->executeUpdate(
            'UPDATE customer SET newsletter = :newsletter WHERE email = :email',
            [
                'newsletter' => (int) $newsletter,
                'email' => $identity->getPostalSettings()->email,
            ]
        );
    }
}
