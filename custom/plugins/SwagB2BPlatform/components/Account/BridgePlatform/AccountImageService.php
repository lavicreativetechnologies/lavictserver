<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Exception;
use Shopware\B2B\Account\Framework\AccountImageServiceInterface;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AvatarRepositoryInterface;
use Shopware\Core\Content\Media\File\FileSaver;
use Shopware\Core\Content\Media\File\MediaFile;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Content\Media\MediaService;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Symfony\Component\HttpFoundation\File\File;
use function getimagesize;
use function in_array;
use function is_array;
use function mb_strtolower;
use function pathinfo;

class AccountImageService implements AccountImageServiceInterface
{
    const ALLOWED_FILE_EXTENSIONS = [
      'bmp',
      'gif',
      'ico',
      'jpeg',
      'jpg',
      'png',
    ];

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var FileSaver
     */
    private $fileSaver;

    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * @var AvatarRepositoryInterface
     */
    private $avatarRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $mediaRepository;

    public function __construct(
        FileSaver $fileSaver,
        ContextProvider $contextProvider,
        MediaService $mediaService,
        AvatarRepositoryInterface $avatarRepository,
        EntityRepositoryInterface $mediaRepository
    ) {
        $this->fileSaver = $fileSaver;
        $this->contextProvider = $contextProvider;
        $this->mediaService = $mediaService;
        $this->avatarRepository = $avatarRepository;
        $this->mediaRepository = $mediaRepository;
    }

    public function uploadImage(IdValue $authId, File $uploadedFile): array
    {
        if ($this->isImage($uploadedFile)) {
            return $this->createImage($authId, $uploadedFile);
        }

        if (!in_array(mb_strtolower($uploadedFile->getExtension()), self::ALLOWED_FILE_EXTENSIONS, true)) {
            return [
                'success' => false,
                'wrongFileExtension' => true,
            ];
        }

        return ['success' => false];
    }

    /**
     * @internal
     */
    protected function createImage(IdValue $authId, File $file): array
    {
        $context = $this->contextProvider->getContext();
        $mediaFile = new MediaFile($file->getPathname(), $file->getMimeType(), $file->getExtension(), $file->getSize());

        $mediaId = $this->mediaService->createMediaInFolder('b2b', $context, false);
        try {
            $this->fileSaver->persistFileToMedia($mediaFile, pathinfo($file->getFilename(), PATHINFO_FILENAME), $mediaId, $context);
        } catch (Exception $e) {
            return ['success' => false];
        }

        $this->avatarRepository->syncAvatarImage($authId, IdValue::create($mediaId));

        $criteria = new Criteria([$mediaId]);
        /** @var MediaEntity $currentMedia */
        $currentMedia = $this->mediaRepository
            ->search($criteria, $context)
            ->get($mediaId);

        return ['success' => true, 'path' => $currentMedia->getUrl()];
    }

    /**
     * @internal
     */
    protected function isImage(File $file): bool
    {
        $lowerExtension = mb_strtolower($file->getExtension());

        return @is_array(getimagesize($file->getPathname()))
            && in_array($lowerExtension, self::ALLOWED_FILE_EXTENSIONS, true);
    }

    public function getAvatarByAuthId(IdValue $authId): string
    {
        return $this->avatarRepository->fetchAvatarByAuthId($authId);
    }
}
