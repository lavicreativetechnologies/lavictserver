<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Account\Framework\AccountServiceInterface;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginService;
use function password_hash;

class AccountService implements AccountServiceInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LoginService
     */
    private $loginService;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var NewsletterSubscriptionService
     */
    private $newsletterSubscriptionService;

    /**
     * @var PasswordVerifier
     */
    private $passwordVerifier;

    public function __construct(
        AuthenticationService $authenticationService,
        Connection $connection,
        LoginService $loginService,
        CredentialsBuilderInterface $credentialsBuilder,
        NewsletterSubscriptionService $newsletterSubscriptionService,
        ContextProvider $contextProvider,
        PasswordVerifier $passwordVerifier
    ) {
        $this->authenticationService = $authenticationService;
        $this->connection = $connection;
        $this->loginService = $loginService;
        $this->credentialsBuilder = $credentialsBuilder;
        $this->contextProvider = $contextProvider;
        $this->newsletterSubscriptionService = $newsletterSubscriptionService;
        $this->passwordVerifier = $passwordVerifier;
    }

    public function savePassword(string $currentPassword, string $newPassword): void
    {
        $identity = $this->authenticationService->getIdentity();

        $loginCredentials = $identity->getLoginCredentials();

        $this->passwordVerifier->verifyPassword($currentPassword, $loginCredentials);
        $this->passwordVerifier->verifyNewPasswordConstraints($newPassword);

        $encodedPassword = password_hash($newPassword, PASSWORD_DEFAULT);

        $query = $this->connection->createQueryBuilder()
            ->update($identity->getTableName())
            ->set('password', ':encodedPassword')
            ->where('id = :id')
            ->setParameter('id', $identity->getId()->getStorageValue())
            ->setParameter('encodedPassword', $encodedPassword);

        if ($identity->getTableName() === ContactRepository::TABLE_NAME) {
            $query->set('encoder', ':encoder')
                ->setParameter('encoder', 'bcrypt');
        }
        $query->execute();

        $credentials = $this->credentialsBuilder->createCredentialsByEmail($loginCredentials->email);

        $this->loginService->setIdentityFor($credentials);
    }

    public function saveNewsletter(bool $subscribeNewsletter, Identity $identity): void
    {
        if ($subscribeNewsletter) {
            $this->newsletterSubscriptionService->subscribeNewsletter(
                $identity,
                $this->contextProvider->getSalesChannelContext()
            );

            return;
        }

        $this->newsletterSubscriptionService->unsubscribeNewsletter($identity,
            $this->contextProvider->getSalesChannelContext()
        );
    }
}
