<?php declare(strict_types=1);

namespace Shopware\B2B\Account\Frontend\DependencyInjection;

use Shopware\B2B\Account\Framework\DependencyInjection\AccountFrameworkConfiguration;
use Shopware\B2B\Common\Controller\DependencyInjection\ControllerConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Shop\Framework\DependencyInjection\ShopFrameworkConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AccountFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new AccountFrameworkConfiguration(),
            new ControllerConfiguration(),
            new ShopFrameworkConfiguration(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
        ];
    }
}
