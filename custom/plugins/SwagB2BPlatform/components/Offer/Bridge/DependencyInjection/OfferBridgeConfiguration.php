<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Bridge\DependencyInjection;

use Shopware\B2B\Cart\Framework\DependencyInjection\CartFrameworkConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Offer\BridgePlatform\DependencyInjection\OfferBridgeConfiguration as PlatformOfferBridgeConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OfferBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformOfferBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new CartFrameworkConfiguration(),
        ];
    }
}
