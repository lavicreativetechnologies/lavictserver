<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Offer\Framework\OfferStatusChangeNotifierInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class OfferExpiredNotificationTaskHandler extends ScheduledTaskHandler
{
    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferStatusChangeNotifierInterface
     */
    private $offerStatusChangeNotifier;

    public function __construct(
        EntityRepositoryInterface $scheduledTaskRepository,
        CurrencyService $currencyService,
        OfferRepository $offerRepository,
        OfferStatusChangeNotifierInterface $offerStatusChangeNotifier
    ) {
        parent::__construct($scheduledTaskRepository);

        $this->scheduledTaskRepository = $scheduledTaskRepository;
        $this->currencyService = $currencyService;
        $this->offerRepository = $offerRepository;
        $this->offerStatusChangeNotifier = $offerStatusChangeNotifier;
    }

    public static function getHandledMessages(): iterable
    {
        return [OfferExpiredNotificationTask::class];
    }

    public function run(): void
    {
        $currencyContext = $this->currencyService->createCurrencyContext();

        $offers = $this->offerRepository->fetchTodayExpiredOffers($currencyContext);

        foreach ($offers as $offer) {
            $this->offerStatusChangeNotifier->notify($offer);
        }
    }
}
