<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartLineItemListConverter;
use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Offer\Framework\OfferCalculatorInterface;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;

class OfferCalculator implements OfferCalculatorInterface
{
    /**
     * @var CartLineItemListConverter
     */
    private $cartLineItemListConverter;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    /**
     * @var CartService
     */
    private $cartService;

    public function __construct(
        CartLineItemListConverter $cartLineItemListConverter,
        ContextProvider $salesChannelContextProvider,
        CartService $cartService
    ) {
        $this->cartLineItemListConverter = $cartLineItemListConverter;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
        $this->cartService = $cartService;
    }

    public function calculateOffer(
        OfferEntity $offerEntity,
        LineItemList $lineItemList,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OfferEntity {
        $cart = $this->cartLineItemListConverter->lineItemListToCart($lineItemList);

        $offerDestination = new OfferDestination($offerEntity, $lineItemList);
        CartState::extract($cart)->setDestination($offerDestination);

        $context = $this->salesChannelContextProvider->getSalesChannelContext();
        $cart = $this->cartService->recalculate($cart, $context);

        $price = $cart->getPrice();

        $offerEntity->discountAmount = $price->getTotalPrice();
        $offerEntity->discountAmountNet = $price->getTotalPrice() - $price->getCalculatedTaxes()->getAmount();

        $this->cartService->deleteCart($context);

        return $offerEntity;
    }
}
