<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\Core\Checkout\Cart\Event\LineItemQuantityChangedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LineItemQuantityChangedSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            LineItemQuantityChangedEvent::class => 'checkChanged',
        ];
    }

    public function checkChanged(LineItemQuantityChangedEvent $event): void
    {
        if (!$event->getCart()->hasExtension(CartState::NAME)) {
            return;
        }

        $destination = CartState::extract($event->getCart())->getDestination();
        if (!($destination instanceof OfferDestination)) {
            return;
        }

        throw new QuantityChangeNotAllowedException($event->getLineItem()->getId());
    }
}
