<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Offer\Framework\CreateOfferThroughCartInterface;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

/**
 * @deprecated tag:v4.4.0 please use the {@see OfferFromCartCreator} instead
 */
class CreateOfferThroughCart implements CreateOfferThroughCartInterface
{
    /**
     * @var OfferFromCartCreator
     */
    private $offerFromCartCreator;

    public function __construct(
        OfferFromCartCreator $offerFromCartCreator
    ) {
        $this->offerFromCartCreator = $offerFromCartCreator;
    }

    public function createOffer(Identity $identity, CurrencyContext $currencyContext): OfferEntity
    {
        return $this->offerFromCartCreator->createOffer($identity, $currencyContext);
    }
}
