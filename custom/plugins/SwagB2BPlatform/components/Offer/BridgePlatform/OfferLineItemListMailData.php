<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\LineItemList\BridgePlatform\LineItemListMailData;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemReferenceMailData;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceEntity;
use function array_map;

class OfferLineItemListMailData extends LineItemListMailData
{
    /**
     * @var LineItemList
     */
    private $list;

    /**
     * @var LineItemReferenceMailData[]|null
     */
    private $references;

    public function __construct(LineItemList $list)
    {
        $this->list = $list;

        parent::__construct($list);
    }

    public function getReferences(): array
    {
        if (!$this->references) {
            $this->references = array_map(static function (OfferLineItemReferenceEntity $reference) {
                return new OfferLineItemReferenceMailData($reference);
            }, $this->list->references);
        }

        return $this->references;
    }
}
