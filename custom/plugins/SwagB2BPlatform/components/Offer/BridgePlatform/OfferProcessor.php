<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceEntity;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\AbsolutePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\PercentagePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Checkout\Promotion\Aggregate\PromotionDiscount\PromotionDiscountEntity;
use Shopware\Core\Checkout\Promotion\Cart\PromotionCalculator;
use Shopware\Core\Checkout\Promotion\Cart\PromotionProcessor;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\Translation\TranslatorInterface;
use function array_map;

class OfferProcessor implements CartProcessorInterface
{
    private const PERCENTAGE_DISCOUNT_NAME = 'b2b.percentage_discount';
    private const ABSOLUTE_DISCOUNT_NAME = 'b2b.absolute_discount';

    /**
     * @var PromotionCalculator
     */
    private $promotionCalculator;

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var QuantityPriceCalculator
     */
    private $quantityPriceCalculator;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    public function __construct(
        PromotionCalculator $promotionCalculator,
        SalesChannelRepositoryInterface $productRepository,
        QuantityPriceCalculator $quantityPriceCalculator,
        TranslatorInterface $translator,
        ContextProvider $salesChannelContextProvider
    ) {
        $this->promotionCalculator = $promotionCalculator;
        $this->productRepository = $productRepository;
        $this->quantityPriceCalculator = $quantityPriceCalculator;
        $this->translator = $translator;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
    }

    public function process(CartDataCollection $data, Cart $original, Cart $toCalculate, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $this->salesChannelContextProvider->setSalesChannelContext($context);

        if (!$original->hasExtension(CartState::NAME)) {
            return;
        }

        $destination = CartState::extract($original)->getDestination();

        if (!($destination instanceof OfferDestination)) {
            return;
        }

        $offer = $destination->getOfferEntity();

        $lineItemList = $destination->getLineItemList();

        $this->overrideLineItemPrices($toCalculate, $lineItemList, $context);

        $discountLimeItemListCollection = $this->createPromotionList($offer, $context);

        $this->addDiscountItemsToOriginalCartToNotCreateNewPromotionErrors($original, $discountLimeItemListCollection);

        $this->promotionCalculator->calculate(
            $discountLimeItemListCollection,
            $original,
            $toCalculate,
            $context,
            $behavior
        );
    }

    /**
     * @internal
     */
    protected function overrideLineItemPrices(Cart $toCart, LineItemList $lineItemList, SalesChannelContext $context): void
    {
        $cartLineItemCollection = $toCart->getLineItems();

        $referenceNumbers = array_map(function (LineItemReference $reference) {
            return $reference->referenceNumber;
        }, $lineItemList->references);

        $products = $this->getProductsByReferenceNumbers($referenceNumbers, $context);

        foreach ($lineItemList->references as $reference) {
            if (!$reference instanceof OfferLineItemReferenceEntity) {
                continue;
            }

            if (!$reference->discountAmountNet || !isset($products[$reference->referenceNumber])) {
                continue;
            }

            foreach ($cartLineItemCollection->getElements() as $lineItem) {
                if ($products[$reference->referenceNumber]->getId() !== $lineItem->getReferencedId()) {
                    continue;
                }

                if (!($lineItem->getPriceDefinition() instanceof QuantityPriceDefinition)) {
                    continue;
                }

                /** @var QuantityPriceDefinition $definition */
                $definition = $lineItem->getPriceDefinition();
                $newQuantityPriceDefinition = new QuantityPriceDefinition(
                    $reference->discountAmountNet,
                    $definition->getTaxRules(),
                    $context->getContext()->getCurrencyPrecision(),
                    $definition->getQuantity()
                );

                $lineItem->setPriceDefinition($newQuantityPriceDefinition);

                $price = $this->quantityPriceCalculator->calculate($newQuantityPriceDefinition, $context);
                $lineItem->setPrice($price);
            }
        }
    }

    /**
     * @internal
     */
    protected function createPromotionList(OfferEntity $offerEntity, SalesChannelContext $context): LineItemCollection
    {
        $offerDiscounts = [];

        if ($offerEntity->discountValueNet) {
            $offerDiscounts[] = $this->createAbsoluteDiscountItem($offerEntity, $context);
        }

        if ($offerEntity->percentageDiscount) {
            $offerDiscounts[] = $this->createPercentageDiscountItem($offerEntity, $context);
        }

        return new LineItemCollection(
            $offerDiscounts
        );
    }

    /**
     * @internal
     */
    protected function createAbsoluteDiscountItem(OfferEntity $offerEntity, SalesChannelContext $context): LineItem
    {
        $lineItem = $this->createNewPromotionLineItem();

        $lineItem->setLabel($this->translator->trans(self::ABSOLUTE_DISCOUNT_NAME));
        $lineItem->setDescription($this->translator->trans(self::ABSOLUTE_DISCOUNT_NAME));
        $lineItem->setGood(false);
        $lineItem->setRemovable(false);

        $absoluteDiscount = new AbsolutePriceDefinition(
            -$offerEntity->discountValueNet,
            $context->getContext()->getCurrencyPrecision()
        );

        $lineItem->setPriceDefinition($absoluteDiscount);
        $lineItem->setPayload(
            [
                'discountType' => PromotionDiscountEntity::TYPE_ABSOLUTE,
                'discountScope' => PromotionDiscountEntity::SCOPE_CART,
                'promotionId' => Uuid::randomHex(),
                'filter' => [
                    'sorterKey' => null,
                    'applierKey' => null,
                    'usageKey' => null,
                ],
                'exclusions' => [],
                'code' => Uuid::randomHex(),
            ]
        );

        return $lineItem;
    }

    /**
     * @internal
     */
    protected function createPercentageDiscountItem(OfferEntity $offerEntity, SalesChannelContext $context): LineItem
    {
        $lineItem = $this->createNewPromotionLineItem();

        $lineItem->setLabel($this->translator->trans(self::PERCENTAGE_DISCOUNT_NAME));
        $lineItem->setDescription($this->translator->trans(self::PERCENTAGE_DISCOUNT_NAME));
        $lineItem->setGood(false);
        $lineItem->setRemovable(false);

        $percentageDiscount = new PercentagePriceDefinition(
            $offerEntity->percentageDiscount,
            $context->getContext()->getCurrencyPrecision()
        );

        $lineItem->setPriceDefinition($percentageDiscount);
        $lineItem->setPayload(
            [
                'discountType' => PromotionDiscountEntity::TYPE_PERCENTAGE,
                'discountScope' => PromotionDiscountEntity::SCOPE_CART,
                'promotionId' => Uuid::randomHex(),
                'filter' => [
                    'sorterKey' => null,
                    'applierKey' => null,
                    'usageKey' => null,
                ],
                'exclusions' => [],
                'code' => Uuid::randomHex(),
            ]
        );

        return $lineItem;
    }

    /**
     * @internal
     */
    protected function createNewPromotionLineItem(): LineItem
    {
        return new LineItem(
            Uuid::randomHex(),
            PromotionProcessor::LINE_ITEM_TYPE,
            ''
        );
    }

    /**
     * @internal
     * @return ProductEntity[]
     */
    protected function getProductsByReferenceNumbers(array $referenceNumbers, SalesChannelContext $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('productNumber', $referenceNumbers));
        $criteria->addAssociation('prices');

        $products = $this->productRepository->search($criteria, $context)->getElements();

        $formattedProducts = [];

        /** @var ProductEntity $product */
        foreach ($products as $product) {
            $formattedProducts[$product->getProductNumber()] = $product;
        }

        return $formattedProducts;
    }

    /**
     * @internal
     */
    protected function addDiscountItemsToOriginalCartToNotCreateNewPromotionErrors(
        Cart $original,
        LineItemCollection $discountLimeItemListCollection
    ): void {
        $original->addLineItems($discountLimeItemListCollection);
    }
}
