<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Shop\BridgePlatform\DateTimeDataType;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class OfferEntityMailData
{
    /**
     * @var OfferEntity
     */
    private $offerEntity;

    public function __construct(OfferEntity $offerEntity)
    {
        $this->offerEntity = $offerEntity;
    }

    public static function getOfferMailDataType(): EventDataType
    {
        $offer = new ObjectType();
        $offer->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('orderContextId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('listId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('debtorEmail', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('email', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('status', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('authId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $offer->add('discountAmount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $offer->add('discountAmountNet', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $offer->add('discountValueNet', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $offer->add('percentageDiscount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));

        $offer->add('acceptedByAdminAt', DateTimeDataType::getDateTimeData());
        $offer->add('acceptedByUserAt', DateTimeDataType::getDateTimeData());
        $offer->add('changedByAdminAt', DateTimeDataType::getDateTimeData());
        $offer->add('changedByUserAt', DateTimeDataType::getDateTimeData());
        $offer->add('changedStatusAt', DateTimeDataType::getDateTimeData());
        $offer->add('convertedAt', DateTimeDataType::getDateTimeData());
        $offer->add('createdAt', DateTimeDataType::getDateTimeData());
        $offer->add('getDeclinedByAdminAt', DateTimeDataType::getDateTimeData());
        $offer->add('declinedByUserAt', DateTimeDataType::getDateTimeData());
        $offer->add('expiredAt', DateTimeDataType::getDateTimeData());

        return $offer;
    }

    public function getId(): string
    {
        return (string) $this->offerEntity->id->getValue();
    }

    public function getDebtorEmail(): string
    {
        return $this->offerEntity->debtorEmail;
    }

    public function getEmail(): string
    {
        return $this->offerEntity->email;
    }

    public function getStatus(): string
    {
        return $this->offerEntity->status;
    }

    public function getAcceptedByAdminAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->acceptedByAdminAt);
    }

    public function getAcceptedByUserAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->acceptedByUserAt);
    }

    public function getAuthId(): string
    {
        return (string) $this->offerEntity->authId->getValue();
    }

    public function getChangedByAdminAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->changedByAdminAt);
    }

    public function getChangedByUserAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->changedByUserAt);
    }

    public function getChangedStatusAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->changedStatusAt);
    }

    public function getConvertedAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->convertedAt);
    }

    public function getCreatedAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->createdAt);
    }

    public function getDeclinedByAdminAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->declinedByAdminAt);
    }

    public function getDeclinedByUserAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->declinedByUserAt);
    }

    public function getDiscountAmount(): float
    {
        return $this->offerEntity->discountAmount;
    }

    public function getDiscountAmountNet(): float
    {
        return $this->offerEntity->discountAmountNet;
    }

    public function getDiscountValueNet(): float
    {
        return $this->offerEntity->discountValueNet;
    }

    public function getPercentageDiscount(): float
    {
        return $this->offerEntity->percentageDiscount;
    }

    public function getOrderContextId(): string
    {
        return (string) $this->offerEntity->orderContextId->getValue();
    }

    public function getListId(): string
    {
        return (string) $this->offerEntity->listId->getValue();
    }

    public function getExpiredAt(): DateTimeDataType
    {
        return new DateTimeDataType($this->offerEntity->expiredAt);
    }
}
