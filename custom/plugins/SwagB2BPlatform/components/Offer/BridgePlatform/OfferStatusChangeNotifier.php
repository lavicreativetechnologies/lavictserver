<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferLineItemListRepository;
use Shopware\B2B\Offer\Framework\OfferStatusChangeNotifierInterface;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\Core\Framework\Adapter\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OfferStatusChangeNotifier implements OfferStatusChangeNotifierInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var OfferLineItemListRepository
     */
    private $offerLineItemListRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        OrderContextRepository $orderContextRepository,
        OfferLineItemListRepository $offerLineItemListRepository,
        AuthenticationService $authenticationService,
        CurrencyService $currencyService,
        ContextProvider $contextProvider,
        Translator $translator,
        LanguageRepository $languageRepository,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->orderContextRepository = $orderContextRepository;
        $this->offerLineItemListRepository = $offerLineItemListRepository;
        $this->authenticationService = $authenticationService;
        $this->currencyService = $currencyService;
        $this->contextProvider = $contextProvider;
        $this->translator = $translator;
        $this->languageRepository = $languageRepository;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function notify(OfferEntity $offer): void
    {
        if (!$this->authenticationService->isAuthenticated()) {
            $identity = $this->authenticationService->getIdentityByAuthId($offer->authId);
            $this->authStorageAdapter->setIdentity($identity);
        } else {
            $identity = $this->authenticationService->getIdentity();
        }

        $ownershipContext = $identity->getOwnershipContext();

        $orderContext = $this->orderContextRepository->fetchOneOrderContextById(
            $offer->orderContextId,
            $ownershipContext
        );

        $lineItemList = $this->offerLineItemListRepository->fetchOneListById(
            $offer->listId,
            $this->currencyService->createCurrencyContext(),
            $ownershipContext
        );

        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        /** @var DebtorEntity $debtor */
        $debtor = $identity->getOwnershipEntity();
        $postalSettings = $identity->getPostalSettings();

        $language = $postalSettings->language;

        $this->translator->injectSettings(
            $salesChannelContext->getSalesChannel()->getId(),
            $language->getValue(),
            $this->languageRepository->getLocalCodeByLanguageId($language),
            $salesChannelContext->getContext());

        $translatedOfferStatus = $this->translator->trans('b2b.offerState.' . $offer->status);

        $this->translator->resetInjection();

        $this->eventDispatcher->dispatch(new OfferStatusNotifyMailEvent(
            $salesChannelContext,
            $offer,
            $lineItemList,
            $orderContext,
            $debtor,
            $postalSettings,
            $translatedOfferStatus
        ));
    }
}
