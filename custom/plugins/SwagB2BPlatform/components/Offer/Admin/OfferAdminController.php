<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Admin;

use InvalidArgumentException;
use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\IntIdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\Offer\Framework\OfferBackendAuthenticationService;
use Shopware\B2B\Offer\Framework\OfferCalculatorInterface;
use Shopware\B2B\Offer\Framework\OfferCrudService;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceCrudService;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceEntity;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Offer\Framework\OfferSearchStruct;
use Shopware\B2B\Offer\Framework\OfferService;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Checkout\Cart\CartRuleLoader;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Routing\Exception\MissingRequestParameterException;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextService;
use SwagB2bPlatform\Routing\Response\GridStateJsonResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use function array_map;
use function count;
use function get_class;
use function is_subclass_of;

/**
 * @RouteScope(scopes={"api"})
 */
class OfferAdminController
{
    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var OfferCrudService
     */
    private $offerCrudService;

    /**
     * @var LineItemListRepository
     */
    private $listRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var OfferBackendAuthenticationService
     */
    private $authenticationService;

    /**
     * @var GridStateJsonResponseFactory
     */
    private $gridStateJsonResponseFactory;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    /**
     * @var SalesChannelContextFactory
     */
    private $salesChannelContextFactory;

    /**
     * @var OfferCalculatorInterface
     */
    private $offerCalculator;

    /**
     * @var OfferLineItemReferenceCrudService
     */
    private $offerLineItemReferenceCrud;

    /**
     * @var CartRuleLoader
     */
    private $cartRuleLoader;

    public function __construct(
        CurrencyService $currencyService,
        OfferRepository $offerRepository,
        OfferService $offerService,
        OfferCrudService $offerCrudService,
        LineItemListRepository $listRepository,
        AddressRepositoryInterface $addressRepository,
        OfferBackendAuthenticationService $authenticationService,
        GridHelper $gridHelper,
        GridStateJsonResponseFactory $gridStateJsonResponseFactory,
        AuthStorageAdapterInterface $authStorageAdapter,
        OrderContextRepository $orderContextRepository,
        ContextProvider $salesChannelContextProvider,
        SalesChannelContextFactory $salesChannelContextFactory,
        OfferCalculatorInterface $offerCalculator,
        OfferLineItemReferenceCrudService $offerLineItemReferenceCrud,
        CartRuleLoader $cartRuleLoader
    ) {
        $this->currencyService = $currencyService;
        $this->offerRepository = $offerRepository;
        $this->offerService = $offerService;
        $this->offerCrudService = $offerCrudService;
        $this->listRepository = $listRepository;
        $this->addressRepository = $addressRepository;
        $this->authenticationService = $authenticationService;
        $this->gridHelper = $gridHelper;
        $this->gridStateJsonResponseFactory = $gridStateJsonResponseFactory;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->orderContextRepository = $orderContextRepository;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
        $this->salesChannelContextFactory = $salesChannelContextFactory;
        $this->offerCalculator = $offerCalculator;
        $this->offerLineItemReferenceCrud = $offerLineItemReferenceCrud;
        $this->cartRuleLoader = $cartRuleLoader;
    }

    /**
     * @Route("/api/v{version}/_action/offer", name="api.action.b2boffer.list", methods={"GET"})
     */
    public function getListAction(Request $request): JsonResponse
    {
        $context = $this->currencyService->createCurrencyContext();

        $searchStruct = new OfferSearchStruct();

        $this->gridHelper->extractSearchDataInAdmin($request, $searchStruct);

        $offers = array_map(
            function (OfferEntity $offer) use ($context) {
                return $this->enrichOffer($offer, $context);
            },
            $this->offerRepository->fetchBackendList($searchStruct, $context)
        );

        $count = $this->offerRepository->fetchTotalCountForBackend($searchStruct);

        return $this->gridStateJsonResponseFactory->createResponse(
            $this->gridHelper->getGridState(
                $request,
                $searchStruct,
                $offers,
                $this->gridHelper->getMaxPage($count),
                $this->gridHelper->getCurrentPage($request),
                $count
            )
        );
    }

    /**
     * @Route("/api/v{version}/_action/offer/{offerId}", name="api.action.b2boffer.detail", methods={"GET"})
     */
    public function getAction(Request $request): JsonResponse
    {
        $offerId = $request->getIdValue('offerId');
        $identity = $this->authenticationService->getIdentityByOfferId($offerId);

        $context = $this->currencyService->createCurrencyContext();

        $offer = $this->offerRepository->fetchOfferById($offerId, $context, $identity->getOwnershipContext());

        return new JsonResponse(['success' => true, 'data' => $this->enrichOffer($offer, $context)]);
    }

    /**
     * @Route("/api/v{version}/_action/offer/{offerId}/update", name="api.action.b2boffer.update", methods={"POST"})
     */
    public function updateAction(Request $request): JsonResponse
    {
        $offerId = $request->getIdValue('offerId');
        $orderContextId = $request->getIdValue('orderContextId');

        $identity = $this->authenticationService->getIdentityByOfferId($offerId);

        $this->setSalesChannelContextWithCustomer($identity);
        $this->authStorageAdapter->setIdentity($identity);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $orderContext = $this->orderContextRepository
            ->fetchOneOrderContextById($orderContextId, $identity->getOwnershipContext());

        $offer = $this->offerRepository->fetchOfferById(
            $offerId,
            $currencyContext,
            $identity->getOwnershipContext()
        );

        if (!empty($request->getParam('comment'))) {
            $this->offerService->saveComment(
                (string) $request->getParam('comment'),
                $orderContext,
                $identity,
                true
            );
        }

        $this->offerService->updateExpiredDate(
            $request->getParam('expirationDate', ''),
            $offer,
            $identity
        );

        $positions = $request->getParam('positions', []);
        foreach ($positions as $position) {
            if (isset($position['created'])) {
                $createOfferLineItem = $this->offerLineItemReferenceCrud->createCreateCrudRequest($position);

                $this->offerLineItemReferenceCrud->addLineItem(
                    $offer->listId,
                    $offer->id,
                    $createOfferLineItem,
                    $currencyContext,
                    $identity,
                    true
                );
                continue;
            }

            if ($position['quantity'] === 0) {
                $this->offerLineItemReferenceCrud->deleteLineItem(
                    $offer->id,
                    $offer->listId,
                    IdValue::create($position['id']),
                    $currencyContext,
                    $identity,
                    true
                );
                continue;
            }

            $updateRequest = $this->offerLineItemReferenceCrud->createUpdateCrudRequest($position);
            $this->offerLineItemReferenceCrud->updateLineItem(
                $offer->listId,
                $offerId,
                $updateRequest,
                $currencyContext,
                $identity,
                true
            );
        }

        $offer = [
            'id' => $offerId,
            'discountValueNet' => (float) $request->getParam('discountAbsolute') ?: 0.0,
            'percentageDiscount' => (float) $request->getParam('discountPercentage') ?: 0.0,
        ];

        $request = $this->offerCrudService->createExistingRecordRequest($offer);
        $this->offerCrudService->update($request, $currencyContext, $identity, true);

        $offer = $this->offerService->acceptOffer(
            $offerId,
            $this->currencyService->createCurrencyContext(),
            $identity
        );

        return new JsonResponse(['success' => true, 'offer' => $offer]);
    }

    /**
     * @Route("/api/v{version}/_action/offer/{offerId}/delete", name="api.action.b2boffer.delete", methods={"POST"})
     */
    public function deleteAction(Request $request): JsonResponse
    {
        $offerId = $request->getIdValue('offerId');
        $recordRequest = $this->offerCrudService->createExistingRecordRequest([
            'id' => $offerId,
        ]);

        $context = $this->currencyService->createCurrencyContext();
        $identity = $this->authenticationService->getIdentityByOfferId($offerId);
        $this->authStorageAdapter->setIdentity($identity);

        $offer = $this->offerCrudService->remove($recordRequest, $context, $identity->getOwnershipContext());

        return new JsonResponse(['success' => true, 'data' => $offer]);
    }

    /**
     * @Route("/api/v{version}/_action/offer/bulk-delete", name="api.action.b2boffer.bulkDelete", methods={"POST"})
     */
    public function bulkDeleteAction(Request $request): JsonResponse
    {
        $offerIds = $request->getParam('offerIds');
        $context = $this->currencyService->createCurrencyContext();

        foreach ($offerIds as $value) {
            $offerId = IdValue::create($value);

            $recordRequest = $this->offerCrudService->createExistingRecordRequest([
                'id' => $offerId,
            ]);

            $identity = $this->authenticationService->getIdentityByOfferId($offerId);
            $this->authStorageAdapter->setIdentity($identity);

            $this->offerCrudService->remove($recordRequest, $context, $identity->getOwnershipContext());
        }

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/api/v{version}/_action/offer/{offerId}/decline", name="api.action.b2boffer.decline", methods={"POST"})
     */
    public function declineAction(Request $request): JsonResponse
    {
        $offerId = $request->getIdValue('offerId');
        $identity = $this->authenticationService->getIdentityByOfferId($offerId);
        $this->authStorageAdapter->setIdentity($identity);

        $offer = $this->offerService->declineOffer(
            $offerId,
            $this->currencyService->createCurrencyContext(),
            $identity
        );

        return new JsonResponse(['success' => true, 'updatedStatus' => $offer->status]);
    }

    /**
     * @Route("/api/v{version}/_action/offer/{offerId}/accept", name="api.action.b2boffer.accept", methods={"POST"})
     */
    public function acceptAction(Request $request): JsonResponse
    {
        $offerId = $request->getIdValue('offerId');
        $identity = $this->authenticationService->getIdentityByOfferId($offerId);
        $this->authStorageAdapter->setIdentity($identity);
        $this->setSalesChannelContextWithCustomer($identity);

        $offer = $this->offerService->acceptOffer(
            $offerId,
            $this->currencyService->createCurrencyContext(),
            $identity
        );

        return new JsonResponse(['success' => true, 'updatedStatus' => $offer->status]);
    }

    /**
     * @Route("/api/v{version}/_action/offer/preview", name="api.action.b2boffer.preview", methods={"POST"})
     */
    public function previewAction(Request $request): JsonResponse
    {
        $payload = $request->getPost();
        $offerId = IntIdValue::create($payload['offerId']);
        $orderContextId = IdValue::create($payload['orderContextId']);

        if ($offerId instanceof NullIdValue) {
            throw new MissingRequestParameterException('offerId', 'payload.offerId');
        }

        if ($orderContextId instanceof NullIdValue) {
            throw new MissingRequestParameterException('orderContextId', 'payload.orderContextId');
        }

        $identity = $this->authenticationService->getIdentityByOfferId($offerId);
        $this->setSalesChannelContextWithCustomer($identity);
        $this->authStorageAdapter->setIdentity($identity);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $identity->getOwnershipContext();

        $positions = $request->getParam('positions');

        $lineItemList = new LineItemList();
        foreach ($positions as $position) {
            $lineItemReference = new OfferLineItemReferenceEntity();
            $lineItemReference->setData($position);
            $lineItemList->references[] = $lineItemReference;
        }

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        $offer->discountValueNet = (float) ($payload['discountAbsolute'] ?? $offer->discountValueNet);
        $offer->percentageDiscount = (float) ($payload['discountPercentage'] ?? $offer->discountValueNet);

        $offer = $this->offerCalculator->calculateOffer($offer, $lineItemList, $currencyContext, $ownershipContext);

        return new JsonResponse(['success' => true, 'data' => $this->enrichOffer($offer, $currencyContext)]);
    }

    /**
     * @internal
     */
    protected function enrichOffer(OfferEntity $offer, CurrencyContext $context): array
    {
        $offerArray = $offer->toArray();
        $identity = $this->authenticationService->getIdentityByOfferId($offer->id);
        $this->authStorageAdapter->setIdentity($identity);

        $list = $this->listRepository->fetchOneListById($offer->listId, $context, $identity->getOwnershipContext());

        $offerArray['listAmount'] = $list->amount;
        $offerArray['listAmountNet'] = $list->amountNet;
        $offerArray['listPositionCount'] = count($list->references);

        try {
            $debtor = $this->offerService->fetchDebtorByOfferId(
                $offer->id,
                $this->currencyService->createCurrencyContext(),
                $identity->getOwnershipContext()
            );

            $address = $this->addressRepository->fetchOneById($debtor->default_billing_address_id, $identity);
            $offerArray['debtor'] = [
                'email' => $debtor->email,
                'firstName' => $address->firstname,
                'lastName' => $address->lastname,
                'company' => $address->company,
                'id' => $debtor->id->getValue(),
            ];
        } catch (NotFoundException $e) {
            $offerArray['debtor'] = null;
        }

        return $offerArray;
    }

    /**
     * @internal
     */
    protected function setSalesChannelContextWithCustomer(Identity $identity): void
    {
        $entity = $identity->getEntity();

        if ($entity instanceof DebtorEntity && !is_subclass_of($entity, DebtorEntity::class)) {
            $subshopID = $entity->subshopID;
        } elseif ($entity instanceof ContactEntity && !is_subclass_of($entity, ContactEntity::class)) {
            $subshopID = $entity->debtor->subshopID;
        } else {
            throw new InvalidArgumentException('Unknown identity type "' . get_class($identity) . '"');
        }

        $salesChannelContext = $this->salesChannelContextFactory->create(
            Uuid::randomHex(),
            $subshopID->getValue(),
            [
                SalesChannelContextService::CUSTOMER_ID => $identity->getOwnershipContext()->shopOwnerUserId->getValue(),
            ]
        );

        $this->cartRuleLoader->loadByToken($salesChannelContext, $salesChannelContext->getToken());
        $this->salesChannelContextProvider->setSalesChannelContext($salesChannelContext);
    }
}
