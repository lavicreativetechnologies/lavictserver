<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Admin\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Offer\Framework\DependencyInjection\OfferFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OfferAdminConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/admin-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new OfferFrameworkConfiguration(),
        ];
    }
}
