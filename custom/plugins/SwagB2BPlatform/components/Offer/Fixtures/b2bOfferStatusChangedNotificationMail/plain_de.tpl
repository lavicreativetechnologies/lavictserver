{include file="string:{config name=emailheaderhtml}"}

Hallo {$addressee.name},

der Status {if $context.isToEnquirer}Deines Angebotes{else}des Angebots von {$offer.enquirer}{/if} bei {config name=shopName} hat sich geändert.
Er ist jetzt "{$offer.status}".
{if !$offer.finished}Bitte logge Dich in Deinen {if $context.isToAdmin}Admin-{/if}Account ein und prüfe das Angebot unter {if $context.isToAdmin}"Kunden" - "Angebote"{else}"Mein Konto" - "Dashboard" - "Angebote"{/if}{/if}

{include file="string:{config name=emailfooterhtml}"}
