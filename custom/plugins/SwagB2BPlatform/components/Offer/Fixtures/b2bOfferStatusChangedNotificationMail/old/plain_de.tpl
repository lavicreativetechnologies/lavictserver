{include file="string:{config name=emailheaderhtml}"}

Hallo {$addressee.name},

der Status {if $context.isToEnquirer}deines Angebotes{else}des Angebots von{$offer.enquirer}{/if} bei {config name=shopName} hat sich ge&auml;ndert.
Er ist jetzt "{s name="{$offer.status}"}{$offer.status}{/s}".
{if !$offer.finished}Bitte logge dich in deinen {if $context.isToAdmin}Admin-{/if}Account ein und checke das Angebot unter {if $context.isToAdmin}"Kunden" - "Angebote"{else}"Mein Konto" - "Dashboard" - "Angebote"{/if}{/if}

{include file="string:{config name=emailfooterhtml}"}
