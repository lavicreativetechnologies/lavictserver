{include file="string:{config name=emailheaderhtml}"}

Hello {$addressee.name},

the status of {if $context.isToEnquirer}your offer{else}the offer by {$offer.enquirer}{/if} at {config name=shopName} has changed.
It is now "{s name="{$offer.status}"}{$offer.status}{/s}".
{if !$offer.finished}Please log into your {if $context.isToAdmin}admin-{/if}account and check the offer under {if $context.isToAdmin}"Customers" - "Offers"{else}"My account" - "Dashboard" - "Offers"{/if}{/if}

{include file="string:{config name=emailfooterhtml}"}
