<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogValueDiffEntity;
use Shopware\B2B\Currency\Framework\CurrencyAware;

class AuditLogDiscountEntity extends AuditLogValueDiffEntity implements CurrencyAware
{
    /**
     * @var float
     */
    public $currencyFactor;

    /**
     * @var bool
     */
    public $oldDiscountWasAbsolute;

    /**
     * @var bool
     */
    public $newDiscountIsAbsolute;

    public function getCurrencyFactor(): float
    {
        return $this->currencyFactor;
    }

    public function setCurrencyFactor(float $factor): void
    {
        $this->currencyFactor = $factor;
    }

    public function getAmountPropertyNames(): array
    {
        return [
            'newValue',
            'oldValue',
        ];
    }

    public function getTemplateName(): string
    {
        return 'OfferDiscount';
    }

    public function isChanged(): bool
    {
        if (!isset($this->oldDiscountWasAbsolute, $this->newDiscountIsAbsolute)) {
            return parent::isChanged();
        }

        if ($this->oldDiscountWasAbsolute === $this->newDiscountIsAbsolute) {
            return parent::isChanged();
        }

        return true;
    }
}
