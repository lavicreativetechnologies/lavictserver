<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use DateTime;
use InvalidArgumentException;
use Shopware\B2B\AuditLog\Framework\AuditLogValueBasicEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueOrderCommentEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function nl2br;

class OfferService
{
    /**
     * @deprecated tag:v4.2.0 use OfferContextRepository::STATE_OFFER instead
     */
    public const ORDER_STATUS = OfferContextRepository::STATE_OFFER;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferShopWriterServiceInterface
     */
    private $offerShopWriterService;

    /**
     * @var DebtorRepositoryInterface
     */
    private $debtorRepository;

    /**
     * @var OfferAuditLogService
     */
    private $auditLog;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var OfferStatusChangeNotifierInterface
     */
    private $notifier;

    /**
     * @var OfferCalculatorInterface
     */
    private $offerCalculator;

    public function __construct(
        OfferRepository $offerRepository,
        OfferShopWriterServiceInterface $offerShopWriterService,
        DebtorRepositoryInterface $debtorRepository,
        OfferAuditLogService $auditLog,
        OrderRepositoryInterface $orderRepository,
        OrderContextRepository $orderContextRepository,
        OfferStatusChangeNotifierInterface $notifier,
        OfferCalculatorInterface $offerCalculator
    ) {
        $this->offerRepository = $offerRepository;
        $this->offerShopWriterService = $offerShopWriterService;
        $this->auditLog = $auditLog;
        $this->debtorRepository = $debtorRepository;
        $this->orderRepository = $orderRepository;
        $this->orderContextRepository = $orderContextRepository;
        $this->notifier = $notifier;
        $this->offerCalculator = $offerCalculator;
    }

    public function createOfferThroughCheckoutSource(
        Identity $identity,
        CurrencyContext $currencyContext,
        OrderContext $orderContext,
        LineItemList $lineItemList
    ): OfferEntity {
        $offer = new OfferEntity();
        $offer->updateDates(['createdAt']);

        $offer->discountAmount = $lineItemList->amount;
        $offer->discountAmountNet = $lineItemList->amountNet;

        $offer->authId = $identity->getOwnershipContext()->authId;
        $offer->email = $identity->getEntity()->email;
        $offer->debtorEmail = $identity->getOwnershipContext()->shopOwnerEmail;
        $offer->orderContextId = $orderContext->id;
        $offer->currencyFactor = $currencyContext->currentCurrencyFactor;
        $offer->listId = $lineItemList->id;

        $this->offerRepository->addOffer($offer);

        $this->offerRepository->updateOfferDates($offer);

        return $offer;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function sendOfferToAdmin(IdValue $offerId, CurrencyContext $currencyContext, Identity $identity): void
    {
        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        if (!$offer->discountAmount) {
            throw new InvalidArgumentException('The offer is not valid. The offer amount is equal zero.');
        }

        $this->updateOfferStatus($offer, $identity, ['acceptedByUserAt'], ['declinedByAdminAt', 'declinedByUserAt']);
    }

    public function declineOfferByUser(IdValue $offerId, CurrencyContext $currencyContext, Identity $identity): OfferEntity
    {
        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        $offer->updateDates(['declinedByUserAt']);
        $offer->removeDates(['acceptedByAdminAt', 'declinedByAdminAt', 'acceptedByUserAt']);

        $this->updateOfferStatus($offer, $identity, ['declinedByUserAt'], ['acceptedByAdminAt', 'declinedByAdminAt', 'acceptedByUserAt']);

        return $offer;
    }

    public function declineOffer(IdValue $offerId, CurrencyContext $currencyContext, Identity $identity): OfferEntity
    {
        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        $offer->updateDates(['declinedByAdminAt']);
        $offer->removeDates(['acceptedByUserAt', 'acceptedByAdminAt']);

        $this->updateOfferStatus($offer, $identity, ['declinedByAdminAt'], ['acceptedByUserAt', 'acceptedByAdminAt'], true);

        return $offer;
    }

    public function acceptOffer(IdValue $offerId, CurrencyContext $currencyContext, Identity $identity): OfferEntity
    {
        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        if ($offer->discountAmountNet < 0) {
            throw new DiscountGreaterThanAmountException();
        }

        $this->updateOfferStatus($offer, $identity, ['acceptedByAdminAt'], ['declinedByAdmin', 'acceptedByUser'], true);

        return $offer;
    }

    protected function updateOfferStatus(OfferEntity $offer, Identity $identity, array $datesToUpdate, array $datesToRemove, bool $isBackend = false): void
    {
        $oldStatus = $offer->status;

        $offer->updateDates($datesToUpdate);
        $offer->removeDates($datesToRemove);

        $this->offerRepository->updateOfferDates($offer);
        $newStatus = $offer->status;

        $this->createOfferStatusChangeLogEntry($offer->orderContextId, $identity, $newStatus, $oldStatus, $isBackend);
        $this->notifier->notify($offer);
    }

    public function updateOfferPrices(
        IdValue $offerId,
        LineItemList $lineItemList,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OfferEntity {
        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        $this->updateOffer($offer, $lineItemList, $currencyContext, $ownershipContext);

        $this->offerRepository
            ->updateOfferPrices($offer);

        return $offer;
    }

    public function convertOffer(IdValue $offerId, CurrencyContext $currencyContext, OwnershipContext $context): void
    {
        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $context);

        $orderContext = $this->orderContextRepository
            ->fetchOneOrderContextById($offer->orderContextId, $context);

        $this->offerShopWriterService
            ->sendToCheckout($orderContext);
    }

    public function createOfferCreatedStatusChangeLogEntry(IdValue $orderContextId, Identity $identity): void
    {
        $auditLogValue = $this
            ->createOfferAuditLogValue(
                OfferEntity::STATE_CONVERTED,
                OfferEntity::STATE_ACCEPTED_OF_BOTH
            );

        $this->auditLog->createOfferAuditLog(
            $orderContextId,
            $auditLogValue,
            $identity,
            false
        );
    }

    public function createOfferDiscountLogEntry(
        IdValue $orderContextId,
        Identity $identity,
        float $newDiscount,
        float $oldDiscount,
        CurrencyContext $currencyContext,
        bool $isBackend,
        bool $oldDiscountWasAbsolute = true,
        bool $newDiscountIsAbsolute = true
    ): void {
        $auditLogValue = $this
            ->createDiscountAuditLogValue(
                (string) $newDiscount,
                (string) $oldDiscount,
                $currencyContext,
                $oldDiscountWasAbsolute,
                $newDiscountIsAbsolute
            );

        $this->auditLog->createOfferAuditLog(
            $orderContextId,
            $auditLogValue,
            $identity,
            $isBackend
        );
    }

    public function createOfferStatusChangeLogEntry(
        IdValue $orderContextId,
        Identity $identity,
        string $newStatus,
        string $oldStatus,
        bool $isBackend
    ): void {
        $auditLogValue = $this->createOfferAuditLogValue(
            $newStatus,
            $oldStatus
        );

        $this->auditLog->createOfferAuditLog(
            $orderContextId,
            $auditLogValue,
            $identity,
            $isBackend
        );
    }

    public function createOfferExpirationChangeLogEntry(
        IdValue $orderContextId,
        Identity $identity,
        DateTime $newValue,
        bool $isBackend
    ): void {
        $auditLogValue = new AuditLogExpirationDate();
        $auditLogValue->newValue = $newValue;

        $this->auditLog->createOfferAuditLog(
            $orderContextId,
            $auditLogValue,
            $identity,
            $isBackend
        );
    }

    /**
     * @internal
     */
    protected function createOfferAuditLogValue(string $newValue, string $oldValue): AuditLogValueBasicEntity
    {
        $auditLogValue = new AuditLogValueOfferDiffEntity();
        $auditLogValue->newValue = $newValue;
        $auditLogValue->oldValue = $oldValue;

        return $auditLogValue;
    }

    /**
     * @internal
     */
    protected function createDiscountAuditLogValue(
        string $newValue,
        string $oldValue,
        CurrencyContext $currencyContext,
        bool $oldDiscountWasAbsolute = true,
        bool $newDiscountIsAbsolute = true
    ): AuditLogValueBasicEntity {
        $auditLogValue = new AuditLogDiscountEntity();
        $auditLogValue->newValue = $newValue;
        $auditLogValue->oldValue = $oldValue;
        $auditLogValue->oldDiscountWasAbsolute = $oldDiscountWasAbsolute;
        $auditLogValue->newDiscountIsAbsolute = $newDiscountIsAbsolute;
        $auditLogValue->setCurrencyFactor($currencyContext->currentCurrencyFactor);

        return $auditLogValue;
    }

    public function stopOffer(): void
    {
        $this->offerShopWriterService->stopCheckout();
    }

    public function fetchDebtorByOfferId(
        IdValue $offerId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): DebtorEntity {
        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        return $this->debtorRepository->fetchOneByEmail($offer->debtorEmail);
    }

    /**
     * @deprecated v4.4.0 Please use the OfferCalculatorInstead
     */
    public function updateOffer(
        OfferEntity $offerEntity,
        LineItemList $list,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): void {
        $this->offerCalculator->calculateOffer($offerEntity, $list, $currencyContext, $ownershipContext);
    }

    public function updateExpiredDate(string $expiredDate, OfferEntity $offer, Identity $identity): OfferEntity
    {
        if (!$expiredDate) {
            $offer->removeDates(['expiredAt']);
        } else {
            $offer->setDates(['expiredAt' => new DateTime($expiredDate)]);

            $this->createOfferExpirationChangeLogEntry($offer->orderContextId, $identity, $offer->expiredAt, true);
        }

        return $this->offerRepository->updateOfferDates($offer);
    }

    public function saveComment(string $comment, OrderContext $orderContext, Identity $identity, bool $isBackend): void
    {
        $comment = nl2br($comment);

        if ($comment === $orderContext->comment || (!$isBackend && !$comment)) {
            return;
        }

        $this->orderRepository->setOrderCommentByOrderContextId($orderContext->id, $comment);

        $auditLogValue = new AuditLogValueOrderCommentEntity();
        $auditLogValue->oldValue = $orderContext->comment;
        $auditLogValue->newValue = $comment;

        $this->auditLog->createOfferAuditLog(
            $orderContext->id,
            $auditLogValue,
            $identity,
            $isBackend
        );
    }
}
