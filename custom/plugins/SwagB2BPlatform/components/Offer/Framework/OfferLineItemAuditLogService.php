<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogValueBasicEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemAddEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemQuantityEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemRemoveEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\Order\Framework\OrderAuditLogService;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\ProductName\Framework\ProductNameService;
use Shopware\B2B\Shop\Framework\RoundingInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class OfferLineItemAuditLogService
{
    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderAuditLogService
     */
    private $orderClearanceAuditLogService;

    /**
     * @var ProductNameService
     */
    private $productNameService;

    /**
     * @var RoundingInterface
     */
    private $rounding;

    public function __construct(
        OrderContextRepository $orderContextRepository,
        AuthenticationService $authenticationService,
        OrderAuditLogService $orderClearanceAuditLogService,
        ProductNameService $productNameService,
        RoundingInterface $rounding
    ) {
        $this->orderContextRepository = $orderContextRepository;
        $this->authenticationService = $authenticationService;
        $this->orderClearanceAuditLogService = $orderClearanceAuditLogService;
        $this->productNameService = $productNameService;
        $this->rounding = $rounding;
    }

    public function createDeleteLineItem(
        IdValue $listId,
        LineItemReference $item,
        bool $isBackend = false
    ): void {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemRemoveEntity();
        $auditLogValue->oldValue = $item->id;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue, $isBackend);
    }

    public function createAddLineItem(
        IdValue $listId,
        OfferLineItemReferenceEntity $item,
        bool $isBackend = false
    ): void {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemAddEntity();
        $auditLogValue->newValue = (int) $item->quantity;
        $auditLogValue->oldValue = $item->comment;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue, $isBackend);
    }

    public function createLineItemPriceChange(
        IdValue $listId,
        OfferLineItemReferenceEntity $item,
        float $price,
        CurrencyContext $currencyContext,
        bool $isBackend = false
    ): void {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemPriceEntity();
        $auditLogValue->oldValue = $this->rounding->round((float) $item->discountAmountNet);
        $auditLogValue->newValue = $price;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;
        $auditLogValue->setCurrencyFactor($currencyContext->currentCurrencyFactor);

        $this->createAuditLogEntry($listId, $auditLogValue, $isBackend);
    }

    public function createLineItemChange(
        IdValue $listId,
        OfferLineItemReferenceEntity $item,
        int $newQuantity,
        bool $isBackend = false
    ): void {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemQuantityEntity();
        $auditLogValue->oldValue = (int) $item->quantity;
        $auditLogValue->newValue = $newQuantity;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue, $isBackend);
    }

    /**
     * @internal
     */
    protected function getProductName(OfferLineItemReferenceEntity $lineItemReference): string
    {
        $this->productNameService->translateProductNames([$lineItemReference]);

        return $lineItemReference->name;
    }

    /**
     * @internal
     * @param $auditLogValue
     */
    protected function createAuditLogEntry(
        IdValue $listId,
        AuditLogValueBasicEntity $auditLogValue,
        bool $isBackend
    ): void {
        $references = $this->orderContextRepository
            ->fetchAuditLogReferencesByListId($listId);

        if ($isBackend) {
            $this->orderClearanceAuditLogService->addBackendLogFromLineItemAuditLogService(
                $auditLogValue,
                $references
            );

            return;
        }

        $identity = $this->authenticationService
            ->getIdentity();
        $this->orderClearanceAuditLogService->addLogFromLineItemAuditLogService(
            $auditLogValue,
            $references,
            $identity
        );
    }
}
