<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogValueDiffEntity;
use Shopware\B2B\Currency\Framework\CurrencyAware;
use Shopware\B2B\ProductName\Framework\ProductNameAware;

class AuditLogValueLineItemPriceEntity extends AuditLogValueDiffEntity implements CurrencyAware, ProductNameAware
{
    /**
     * @var float
     */
    public $currencyFactor;

    /**
     * @var string
     */
    public $orderNumber;

    /**
     * @var string
     */
    public $productName;

    public function getTemplateName(): string
    {
        return 'OfferLineItemPriceChange';
    }

    public function getCurrencyFactor(): float
    {
        return $this->currencyFactor;
    }

    public function setCurrencyFactor(float $factor): void
    {
        $this->currencyFactor = $factor;
    }

    public function getAmountPropertyNames(): array
    {
        return [
            'newValue',
            'oldValue',
        ];
    }

    /**
     * @param string $name
     */
    public function setProductName(string $name = null): void
    {
        $this->productName = $name;
    }

    public function getProductOrderNumber(): string
    {
        return $this->orderNumber;
    }
}
