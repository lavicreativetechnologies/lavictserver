<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface OfferCalculatorInterface
{
    public function calculateOffer(
        OfferEntity $offerEntity,
        LineItemList $lineItemList,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OfferEntity;
}
