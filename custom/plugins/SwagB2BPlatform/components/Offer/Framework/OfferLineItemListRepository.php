<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyCalculator;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function sprintf;

class OfferLineItemListRepository
{
    const TABLE_NAME = 'b2b_line_item_list';
    const TABLE_ALIAS = 'lineItemList';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var OfferLineItemReferenceRepository
     */
    private $referenceRepository;

    /**
     * @var CurrencyCalculator
     */
    private $currencyCalculator;

    public function __construct(
        Connection $connection,
        OfferLineItemReferenceRepository $referenceRepository,
        CurrencyCalculator $currencyCalculator
    ) {
        $this->connection = $connection;
        $this->referenceRepository = $referenceRepository;
        $this->currencyCalculator = $currencyCalculator;
    }

    /**
     * @param $listId
     * @throws NotFoundException
     */
    public function fetchOneListById(IdValue $listId, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): LineItemList
    {
        $listDataQuery = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id');

        $listDataQuery->andWhere(self::TABLE_ALIAS . '.context_owner_id = :ownerId')
          ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());

        $listData = $listDataQuery->setParameter('id', $listId->getStorageValue())
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$listData) {
            throw new NotFoundException('Unable to find list by id "' . $listId . '"');
        }

        $list = new LineItemList();
        $list->fromDatabaseArray($listData);
        $list->references = $this->referenceRepository->fetchAllForList($listId, $ownershipContext);
        $this->currencyCalculator->recalculateAmount($list, $currencyContext);

        return $list;
    }

    public function fetchAmountNetFromListByOfferId(IdValue $offerId): float
    {
        $amountNet = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.amount_net')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_offer', 'offer', self::TABLE_ALIAS . '.id = offer.list_id')
            ->where('offer.id = :id')
            ->setParameter('id', $offerId->getStorageValue())
            ->execute()->fetchColumn();

        if (!$amountNet) {
            throw new NotFoundException(sprintf('Offer not found for %s', $offerId));
        }

        return (float) $amountNet;
    }
}
