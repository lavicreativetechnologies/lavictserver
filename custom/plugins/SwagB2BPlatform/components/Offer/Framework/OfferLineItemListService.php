<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OfferLineItemListService
{
    /**
     * @var LineItemListRepository
     */
    private $listRepository;

    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $referenceRepository;

    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    public function __construct(
        LineItemListRepository $listRepository,
        LineItemReferenceRepositoryInterface $referenceRepository,
        ProductProviderInterface $productProvider
    ) {
        $this->listRepository = $listRepository;
        $this->referenceRepository = $referenceRepository;
        $this->productProvider = $productProvider;
    }

    public function updateListPricesById(
        IdValue $listId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): LineItemList {
        $lineItemList = $this->listRepository
            ->fetchOneListById($listId, $currencyContext, $ownershipContext);

        $this->updateListPrices($lineItemList, $ownershipContext);

        return $lineItemList;
    }

    public function updateListPrices(
        LineItemList $lineItemList,
        OwnershipContext $ownershipContext
    ): void {
        $lineItemList->recalculateListAmounts();

        foreach ($lineItemList->references as $reference) {
            if (!$reference->mode instanceof ProductLineItemType) {
                continue;
            }

            $this->referenceRepository
                ->updatePrices($reference);
        }

        $this->listRepository
            ->updateListPrices($lineItemList, $ownershipContext);
    }
}
