<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OfferValidationService
{
    const DISCOUNT_GREATER_THAN_AMOUNT = 'DiscountGreaterThanAmount';

    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var OfferLineItemReferenceRepository
     */
    private $referenceRepository;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator,
        OfferLineItemReferenceRepository $referenceRepository
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
        $this->referenceRepository = $referenceRepository;
    }

    public function createUpdateValidation(OfferEntity $offer, OwnershipContext $ownershipContext): Validator
    {
        return $this->createCrudValidation($offer, $ownershipContext)
            ->validateThat('id', $offer->id)
                ->isIdValue()
            ->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function createCrudValidation(OfferEntity $offer, OwnershipContext $ownershipContext): ValidationBuilder
    {
        $validationBuilder = $this->validationBuilder
            ->validateThat('currencyFactor', $offer->currencyFactor)
            ->isNumeric();

        if ($this->isPercentageDiscount($offer)) {
            return $this->createPercentageDiscountValidation($validationBuilder, $offer);
        }

        return $this->createAbsoluteDiscountValidation($validationBuilder, $offer, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function createPercentageDiscountValidation(
        ValidationBuilder $validationBuilder,
        OfferEntity $offer
    ): ValidationBuilder {
        return $validationBuilder
            ->validateThat('percentageDiscount', $offer->percentageDiscount)
            ->isNumeric()
            ->isGreaterEqualThan(0)
            ->isLessThan(100);
    }

    /**
     * @internal
     */
    protected function createAbsoluteDiscountValidation(
        ValidationBuilder $validationBuilder,
        OfferEntity $offer,
        OwnershipContext $ownershipContext
    ): ValidationBuilder {
        return $validationBuilder->validateThat('discountValueNet', $offer->discountValueNet)
            ->isNumeric()
            ->isGreaterEqualThan(0)
            ->withCallback(
                function ($value) use ($offer, $ownershipContext): bool {
                    $references = $this->referenceRepository->fetchAllForList($offer->listId, $ownershipContext);

                    $amount = 0;
                    foreach ($references as $reference) {
                        $amount += ($reference->discountAmountNet / $reference->discountCurrencyFactor) * $reference->quantity;
                    }

                    $discount = ($value / $offer->currencyFactor);

                    return $discount < $amount;
                },
                'Discount can not be greater than the sum of reference discount amounts',
                self::DISCOUNT_GREATER_THAN_AMOUNT
            );
    }

    /**
     * @internal
     */
    protected function isPercentageDiscount(OfferEntity $offer): bool
    {
        return $offer->percentageDiscount && !$offer->discountValueNet;
    }
}
