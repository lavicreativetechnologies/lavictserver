<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogIndexEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogService;
use Shopware\B2B\AuditLog\Framework\AuditLogValueBasicEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function get_class;

class OfferAuditLogService
{
    /**
     * @var AuditLogService
     */
    private $auditLogService;

    public function __construct(AuditLogService $auditLogService)
    {
        $this->auditLogService = $auditLogService;
    }

    public function createOfferAuditLog(
        IdValue $referenceId,
        AuditLogValueBasicEntity $auditLogValue,
        Identity $identity,
        bool $isBackend
    ): AuditLogEntity {
        $auditLog = $this->createLogEntity($auditLogValue);

        $auditLogIndex = new AuditLogIndexEntity();
        $auditLogIndex->referenceId = $referenceId;
        $auditLogIndex->referenceTable = OrderContextRepository::TABLE_NAME;

        if ($isBackend) {
            return $this->auditLogService->createBackendAuditLog($auditLog, [$auditLogIndex]);
        }

        return $this->auditLogService->createAuditLog($auditLog, $identity, [$auditLogIndex]);
    }

    /**
     * @internal
     */
    protected function createLogEntity(AuditLogValueBasicEntity $auditLogValue): AuditLogEntity
    {
        $auditLog = new AuditLogEntity();
        $auditLog->logValue = $auditLogValue;
        $auditLog->logType = get_class($auditLogValue);

        return $auditLog;
    }
}
