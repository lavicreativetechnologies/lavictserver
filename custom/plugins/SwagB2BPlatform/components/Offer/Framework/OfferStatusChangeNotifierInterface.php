<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

interface OfferStatusChangeNotifierInterface
{
    const OFFER_STATUS_CHANGED_EVENT = __CLASS__ . '::offerStatusChanged';

    public function notify(OfferEntity $offer);
}
