<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Shopware\B2B\Shop\Framework\RoundingInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class OfferLineItemReferenceCrudService extends AbstractCrudService
{
    /**
     * @var OfferLineItemReferenceRepository
     */
    private $offerLineItemReferenceRepository;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var OfferLineItemReferenceValidationService
     */
    private $validationService;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferLineItemListRepository
     */
    private $offerLineItemListRepository;

    /**
     * @var TaxProviderInterface
     */
    private $taxProvider;

    /**
     * @var OfferLineItemAuditLogService
     */
    private $offerLineItemAuditLogService;

    /**
     * @var RoundingInterface
     */
    private $rounding;

    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    public function __construct(
        OfferLineItemReferenceRepository $offerLineItemReferenceRepository,
        LineItemListService $lineItemListService,
        OfferService $offerService,
        OfferRepository $offerRepository,
        OfferLineItemReferenceValidationService $offerLineItemReferenceValidationService,
        OfferLineItemListRepository $offerLineItemListRepository,
        TaxProviderInterface $taxProvider,
        OfferLineItemAuditLogService $offerLineItemAuditLogService,
        RoundingInterface $rounding,
        ProductProviderInterface $productProvider
    ) {
        $this->offerLineItemReferenceRepository = $offerLineItemReferenceRepository;
        $this->lineItemListService = $lineItemListService;
        $this->offerService = $offerService;
        $this->offerRepository = $offerRepository;
        $this->validationService = $offerLineItemReferenceValidationService;
        $this->offerLineItemListRepository = $offerLineItemListRepository;
        $this->taxProvider = $taxProvider;
        $this->offerLineItemAuditLogService = $offerLineItemAuditLogService;
        $this->rounding = $rounding;
        $this->productProvider = $productProvider;
    }

    public function createCreateCrudRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest($data, [
            'referenceNumber',
            'quantity',
            'comment',
            'discountAmountNet',
        ]);
    }

    public function createUpdateCrudRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest($data, [
            'id',
            'referenceNumber',
            'quantity',
            'comment',
            'discountAmountNet',
        ]);
    }

    public function updateLineItem(
        IdValue $listId,
        IdValue $offerId,
        CrudServiceRequest $request,
        CurrencyContext $currencyContext,
        Identity $identity,
        bool $isBackend = false
    ): LineItemReference {
        $data = $request->getFilteredData();
        $data['id'] = IdValue::create($data['id']);

        $currentItem = $this->offerLineItemReferenceRepository
            ->fetchReferenceById($data['id'], $identity->getOwnershipContext());

        /** @var OfferLineItemReferenceEntity $updatedItem */
        $updatedItem = clone $currentItem;
        $updatedItem->setData($data);

        $validations = $this->validationService
            ->createUpdateValidation($updatedItem, $offerId);

        $this->testValidation($updatedItem, $validations);

        $this->productProvider->updateReference($updatedItem);

        $updatedItem->discountAmount = $updatedItem->discountAmountNet * $this->taxProvider->getProductTax($updatedItem);
        $updatedItem->discountCurrencyFactor = $currencyContext->currentCurrencyFactor;

        $this->offerLineItemReferenceRepository
            ->updateReference($listId, $updatedItem);

        $list = $this->offerLineItemListRepository->fetchOneListById($listId, $currencyContext, $identity->getOwnershipContext());

        $this->lineItemListService
            ->updateListPricesById($listId, $currencyContext, $identity->getOwnershipContext());

        $this->offerService->updateOfferPrices($offerId, $list, $currencyContext, $identity->getOwnershipContext());

        if ($currentItem->quantity !== $updatedItem->quantity) {
            $this->offerLineItemAuditLogService->createLineItemChange($listId, $currentItem, $updatedItem->quantity, $isBackend);
        }

        if ($this->rounding->round((float) $currentItem->discountAmountNet) !== $this->rounding->round((float) $updatedItem->discountAmountNet)) {
            $this->offerLineItemAuditLogService->createLineItemPriceChange($listId, $currentItem, (float) $updatedItem->discountAmountNet, $currencyContext, $isBackend);
        }

        if ($isBackend) {
            $this->setAdminChange($offerId);
        } else {
            $this->setUserChange($offerId);
        }

        return $list->getReferenceById($updatedItem->id);
    }

    public function addLineItem(
        IdValue $listId,
        IdValue $offerId,
        CrudServiceRequest $request,
        CurrencyContext $currencyContext,
        Identity $identity,
        bool $isBackend = false
    ): LineItemReference {
        $lineItemReference = new OfferLineItemReferenceEntity();
        $lineItemReference->setData($request->getFilteredData());

        $lineItemReference->mode = LineItemType::create(0);

        $validations = $this->validationService
            ->createInsertValidation($lineItemReference, $listId);

        $this->testValidation($lineItemReference, $validations);

        $this->productProvider->updateReference($lineItemReference);

        $tax = $this->taxProvider->getProductTax($lineItemReference);

        $lineItemReference->discountAmount = $lineItemReference->discountAmountNet * $tax;

        $lineItemReference = $this->offerLineItemReferenceRepository
            ->addReference($listId, $lineItemReference);

        $this->lineItemListService->updateListPricesById($listId, $currencyContext, $identity->getOwnershipContext());

        if (!$lineItemReference->discountAmount) {
            $lineItemReference = $this->offerLineItemReferenceRepository
                ->setDefaultPricesForDiscountForLineItemReferenceId($lineItemReference->id, $identity->getOwnershipContext());
        }

        $list = $this->offerLineItemListRepository->fetchOneListById($listId, $currencyContext, $identity->getOwnershipContext());

        $this->offerService->updateOfferPrices($offerId, $list, $currencyContext, $identity->getOwnershipContext());

        $this->offerLineItemAuditLogService->createAddLineItem($listId, $lineItemReference, $isBackend);

        if ($isBackend) {
            $this->setAdminChange($offerId);
        } else {
            $this->setUserChange($offerId);
        }

        return $list->getReferenceById($lineItemReference->id);
    }

    public function deleteLineItem(
        IdValue $offerId,
        IdValue $listId,
        IdValue $lineItemId,
        CurrencyContext $currencyContext,
        Identity $identity,
        bool $isBackend = false
    ): LineItemReference {
        $context = null;
        if ($identity) {
            $context = $identity->getOwnershipContext();
        }

        $currentItem = $this->offerLineItemReferenceRepository
            ->fetchReferenceById($lineItemId, $context);

        $this->offerLineItemReferenceRepository
            ->removeReference($lineItemId);

        $this->lineItemListService
            ->updateListPricesById($listId, $currencyContext, $identity->getOwnershipContext());

        $list = $this->offerLineItemListRepository->fetchOneListById($listId, $currencyContext, $context);

        $this->offerService->updateOfferPrices($offerId, $list, $currencyContext, $identity->getOwnershipContext());

        $this->offerLineItemAuditLogService->createDeleteLineItem($listId, $currentItem, $isBackend);

        $currentItem->id = IdValue::null();
        $currentItem->quantity = 0;

        if ($isBackend) {
            $this->setAdminChange($offerId);
        } else {
            $this->setUserChange($offerId);
        }

        return $currentItem;
    }

    /**
     * @internal
     */
    protected function setAdminChange(IdValue $offerId): void
    {
        $offer = new OfferEntity();
        $offer->id = $offerId;
        $offer->updateDates(['changedByAdminAt']);
        $offer->removeDates(['acceptedByUserAt']);

        $this->offerRepository->updateOfferDates($offer);
    }

    /**
     * @internal
     */
    protected function setUserChange(IdValue $offerId): void
    {
        $offer = new OfferEntity();
        $offer->id = $offerId;
        $offer->updateDates(['changedByUserAt']);
        $offer->removeDates(['acceptedByAdminAt']);

        $this->offerRepository->updateOfferDates($offer);
    }
}
