<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface TaxProviderInterface
{
    /**
     * @deprecated v4.4.0 Please use the {@see OfferCalculator} instead
     */
    public function getDiscountTax(IdValue $lineItemListId, OwnershipContext $ownershipContext): float;

    public function getProductTax(OfferLineItemReferenceEntity $reference): float;
}
