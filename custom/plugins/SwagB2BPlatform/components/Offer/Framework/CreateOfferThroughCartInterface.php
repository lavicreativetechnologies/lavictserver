<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

/**
 * @deprecated tag:v4.4.0 please use {@see OfferFromCartCreatorInterface} instead.
 */
interface CreateOfferThroughCartInterface extends OfferFromCartCreatorInterface
{
}
