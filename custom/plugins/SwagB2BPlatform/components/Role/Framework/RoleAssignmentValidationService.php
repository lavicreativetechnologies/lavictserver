<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Framework;

use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function count;
use function in_array;

class RoleAssignmentValidationService
{
    const CAUSE_AT_LEAST_ONE_ROLE = 'AtLeastOneRole';
    const CAUSE_ROOT_ROLE = 'RootRole';

    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var AclRepository
     */
    private $aclRepository;

    /**
     * @var string
     */
    private $entityType;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator,
        AclRepository $aclRepository,
        string $entityType
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
        $this->aclRepository = $aclRepository;
        $this->entityType = $entityType;
    }

    public function createAllowValidation(RoleEntity $roleEntity): Validator
    {
        return $this->validationBuilder
            ->validateThat('Level', $roleEntity->level)
            ->withCallback(
                function ($value): bool {
                    return $value > 0;
                },
                'This ' . $this->entityType . ' cannot be assigned to the company role.',
                self::CAUSE_ROOT_ROLE
            )
            ->getValidator($this->validator);
    }

    public function createDenyValidation(RoleEntity $roleEntity, IdValue $subjectId): Validator
    {
        return $this->validationBuilder
            ->validateThat('Dependencies', $subjectId->getValue())
            ->withCallback(
                function ($subjectId) use ($roleEntity) {
                    $ids = $this->aclRepository->getAllAssignedIdsBySubjectId($roleEntity, IdValue::create($subjectId));

                    return in_array($roleEntity->id, $ids, false) && count($ids) > 1;
                },
                'This ' . $this->entityType . ' must be at least assigned to one role.',
                self::CAUSE_AT_LEAST_ONE_ROLE
            )
            ->getValidator($this->validator);
    }
}
