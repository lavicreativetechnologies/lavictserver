# CHANGELOG for B2B-Suite

This changelog references changes done in B2B-Suite versions.
[View all changes from B2B-Suite online.](https://docs.enterprise.shopware.com/b2b-suite/changelog/)

## 4.3.3

### Improvements

* Changed the look of the company page on smartphones - B2B-115
* The snippet that is displayed to a sales representative without a client has been changed - B2B-107
* Added compatibility for Shopware 6.3.5

### Deprecations

* Added offer status update email - B2B-418
* Deprecated dynamic variables `languageId` and `salutationId` of `Shopware\B2B\Debtor\Framework\DebtorEntity` and `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntityFactory` - B2B-418
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceShopWriterServiceInterface::stopOrderClearance`, method will return `Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity` in 4.4.0 - B2B-401

## 4.3.2

### Fixes

* Fixed login problems while using Elasticsearch - B2B-376
* Fixed file upload in fast order - B2B-414
* Fixed a bug that prevented changing the order status in the Administration - B2B-416
* Fixed a bug which affects the usability of the fast order page - B2B-375

### Improvements

* Added a click handler to the backdrop of confirm boxes, which cancels the modal - B2B-412

### Deprecations

* Deprecated `Shopware\B2B\Shop\Framework\SessionStorageInterface`, interface will be renamed to `Shopware\B2B\Shop\Framework\StorageInterface` in 4.4.0 - B2B-402
* Deprecated `Shopware\B2B\Shop\BridgePlatform\SessionStorage`, interface will be renamed to `Shopware\B2B\Shop\BridgePlatform\Storage` in 4.4.0 - B2B-402

## 4.3.1

### Additions

* Completion of the order reference number and requested delivery date functionalities in SwagB2bPlatform - B2B-194

### Improvements

* Changed the offer-status messages - B2B-330
* Changed the table header from "Sales Representative Clients" to "Clients" - B2B-377

### Fixes

* Fixes translation bug after trying to change a password with a too short new password - B2B-129
* Fixes compatibility issues with Internet Explorer 11 and Edge - B2B-249
* Fixes compatibility issue with the plugin "SwagCustomizedProducts" where the theme could not be compiled when activating "SwagCustomizedProducts" - NEXT-7846

### Changes

* The class `app\storefront\src\js\classes\B2bPluginClass.ts` has been moved to the method `app\storefront\src\js\utility\getB2bPluginClass.ts` - B2B-249
* The SCSS mixin `border-radius` was removed from `app\storefront\src\styles\abstracts\mixins\_utility.scss`. This mixin had a naming conflict with the `border-radius` mixin from Bootstrap of the Shopware core. - NEXT-7846
* The SCSS mixin `animation` was removed from `app\storefront\src\styles\abstracts\mixins\_utility.scss`. The mixin is unused and only applies one property which can be done natively without the mixin. - NEXT-7846
* The SCSS mixin `opacity` was removed from `app\storefront\src\styles\abstracts\mixins\_utility.scss`. The mixin only applies one property which can be done natively without the mixin. - NEXT-7846

### Deprecations

* Deprecated variable 'parameter' used to format messages, will be removed in 4.4.0, use 'parameters' instead - B2B-129
    * `Shop/BridgePlatform/MessageFormatter.php` in function formatSessionMessage() - B2B-129
    * `SwagB2bPlatform/Resources/views/storefront/_partials/_b2bmessage/controller/_b2bmessage-set.html.twig` - B2B-129
* Deprecated `Shopware\B2B\Common\MvcExtension\MvcEnvironmentInterface`, class will be removed in 4.4.0 without replacement - B2B-354
* Deprecated `SwagB2bPlatform\Extension\MvcEnvironment`, class will be removed in 4.4.0 without replacement - B2B-354
* Deprecated `SwagB2bPlugin\Extension\MvcEnvironment`, class will be removed in 4.4.0 without replacement - B2B-354

## 4.3.0

### Improvements

* Added sales representative client search in the administration - B2B-247

### Changes

* The method `\Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository::fetchCombinedClientListTotalCount` got an optional second argument `SearchStruct`, which will be required with 4.4.0.
* The service `\Shopware\B2B\Shop\BridgePlatform\SalesChannelContextProvider` has been renamed into `\Shopware\B2B\Shop\BridgePlatform\ContextProvider`.
* The template block `frontend_index_content_b2b` was replaced with `b2blayout_content` in `components/SwagB2bPlatform/Resources/views/storefront/frontend/b2bacl/error.html.twig` - B2B-228
* Offers are now filtered by acl contact visibility. - B2B-234

### Fixes

* Fixed a problem where ordering with items from excluded categories was possible, despite contingent rules - ENT-2488
* Fixed ignored contingent rules - ENT-2386
* Fixed a bug when fetching a debtor's order lists through the OrderList API, which led to an authentication exception - ENT-2494
* Fixed incorrect amounts when duplicating order lists for certain currencies - ENT-2477
* Fixed installation when english is not the default language - ENT-2512
* Fixed order list export when name contains slashes - ENT-2476
* Fixed "Identity not set" error when processing an offer - ENT-2548
* Fixed a problem that prevented B2B Suite templates from being extended by other plugins - B2B-253
* Fixed wrong order amounts because of assigned net customer group - B2B-114
* Fixed error that you can assign a custom order number to an already existing product number - ENT-2156
* Fixed error in order overview from removing a contact with an order - ENT-2295
* Fixed a problem with the login - B2B-296
* Fixed a problem with guest orders - B2B-320

### Additions

* Remove a contact triggers shopware user data deletion and transfer offer and order data to the debtor - ENT-2259
* Fixed elastic search incompatibility with custom order numbers and in stock - ENT-2526
* Offer module for Shopware 6 has been added - B2B-154

### Deprecations

* Deprecated `\Shopware\B2B\LineItemList\Bridge\ProductProvider::updateListWithoutUpdatingProductReferences` Function will be removed with v4.4.0 - Use `\Shopware\B2B\LineItemList\Framework\LineItemList::recalculateListAmounts` instead
* Deprecated `\Shopware\B2B\LineItemList\BridgePlatform\ProductProvider::updateListWithoutUpdatingProductReferences` Function will be removed with v4.4.0 - Use `\Shopware\B2B\LineItemList\Framework\LineItemList::recalculateListAmounts` instead
* Deprecated `\Shopware\B2B\LineItemList\Framework\ProductProviderInterface::updateListWithoutUpdatingProductReferences` Function will be removed with v4.4.0 - Use `\Shopware\B2B\LineItemList\Framework\LineItemList::recalculateListAmounts` instead
* Deprecated `\Shopware\B2B\LineItemList\BridgePlatform\LineItemListMailData::updateListWithoutUpdatingProductReferences` Function will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::updateListWithoutUpdatingProductReferences` Implementation of `CurrencyAware` will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList` Variable `$currency_factor` will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::getCurrencyFactor` Function will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::setCurrencyFactor` Function will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::getAmountPropertyNames` Function will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemListRepository::fetchOneListById` Parameter `$currencyContext` is no longer used and will be removed with v4.4.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemListService::addUpAmountsIntoListById` Function will be removed with v4.4.0
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::fetchList` Parameter `$currencyContext` will be removed with v4.4.0
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::fetchOneById` Parameter `$currencyContext` will be removed with v4.4.0
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::fetchDefaultOrderList` Parameter `$currencyContext` will be removed with v4.4.0
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::setDefaultOrderList` Parameter `$currencyContext` will be removed with v4.4.0
* Deprecated `\Shopware\B2B\OrderList\Framework\RemoteBoxService::createLineItemListFromProductsRequest` Parameter `$ownershipContext` is no longer used and will be removed with v4.4.0
* Deprecated `\Shopware\B2B\Shop\Framework\RoundingInterface::round` Default value for parameter `$quantity` will be 1 with v4.4.0
* Deprecated `\Shopware\B2B\Offer\Framework\CreateOfferThroughCartInterface`. It will be removed with v4.4.0. Please use instead `\Shopware\B2B\Offer\Framework\OfferFromCartCreatorInterface`.
* Deprecated `\Shopware\B2B\Offer\Bridge\CreateOfferThroughCart`. It will be removed with v4.4.0. Please use instead `\Shopware\B2B\Offer\Bridge\OfferFromCartCreator`.
* Deprecated `\Shopware\B2B\Offer\BridgePlatform\CreateOfferThroughCart`. It will be removed with v4.4.0. Please use instead `\Shopware\B2B\Offer\BridgePlatform\OfferFromCartCreator`.
* Deprecated the service `b2b_offer.create_offer_through_cart`. It will be removed with v4.4.0. Please use instead `b2b_offer.offer_from_cart_creator`.
* Deprecated `\Shopware\B2B\Offer\Framework\CreateOfferThroughCartInterface`. It will be removed with v4.4.4. Please use instead `\Shopware\B2B\Offer\Framework\OfferFromCartCreatorInterface`.
* Deprecated `\Shopware\B2B\Offer\Bridge\CreateOfferThroughCart`. It will be removed with v4.4.4. Please use instead `\Shopware\B2B\Offer\Bridge\OfferFromCartCreator`.
* Deprecated `\Shopware\B2B\Offer\BridgePlatform\CreateOfferThroughCart`. It will be removed with v4.4.4. Please use instead `\Shopware\B2B\Offer\BridgePlatform\OfferFromCartCreator`.
* Deprecated the service `b2b_offer.offer_from_cart_creator`. It will be removed with v4.4.4. Please use instead `b2b_offer.offer_from_cart_creator`.
* Deprecated validation rules inside `\Shopware\B2B\Common\Validator\ValidationBuilder`. These methods will get the return type `self` with v4.4.0.
* Deprecated `\Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface::fetchIdByCustomOrderNumber`. Parameter `$ownershipContext` will be required as of v4.4.0
* Deprecated `\Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface::fetchIdByProductDetailsId`. Parameter `$ownershipContext` will be required as of v4.4.0

### Removals

* Database column `currency_factor` has been removed from table `b2b_line_item_list`
* Dropped support for Shopware 6.1

## 4.2.0

### Changes

* `Shopware\B2B\OrderNumber\Framework\OrderNumberCrudService::createCsvImport` returns now a `Shopware\B2B\OrderNumber\Framework\OrderNumberFileEntity` object instead of void
* The `$owner` property of `OrderEntity` moved to parent class `OrderContext` - ENT-2270

### Fixes

* Sync addresses to B2B-Context - ENT-2388

### Improvements

* Alter all database tables to use the `utf8mb4_unicode_ci` collation
* Migration of the InStock module for SwagB2bPlatform - ENT-2284
* Migration of the OrderNumber module for SwagB2bPlatform - ENT-2247
* Migration of the SalesRepresentative module for SwagB2bPlatform - ENT-2246

### Deprecations

* Deprecated `\Shopware\B2B\OrderNumber\Framework\LineItemReferenceRepositoryDecorator::updateReference` return type will be set to void with v4.3.0
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface::updateReference` return type will be set to void with v4.3.0
* Deprecated `\Shopware\B2B\Order\Framework\OrderOwnerRepository` will be removed with v4.3.0 (unused) - ENT-2270
* Deprecated `\Shopware\B2B\Order\Framework\OrderOwnerService::createOrderOwner` will be removed with v4.3.0 - use `\Shopware\B2B\Order\Framework\OrderOwnerService::createOrderContextOwner` instead - ENT-2270
* Deprecated `\Shopware\B2B\OrderSalesRepresentative\Framework\OrderSalesRepresentativeSubscriberInterface` will be removed with v.4.3.0 - use `\Shopware\B2B\OrderSalesRepresentative\Framework\OrderSalesRepresentativeService` instead - ENT-2270

### Removals

* Removed Database table `b2b_sales_representative_orders` - ENT-2270

## 4.1.1

### Improvements

* Percentage field in budgets is only required if the notify option is selected - ENT-2321

### Deprecations

* Deprecated `\Shopware\B2B\InStock\Framework\InStockEntity::$articlesDetailsId` will be removed with v4.3.0 - use the variable $productId instead
* Deprecated `\Shopware\B2B\InStock\Bridge\InStockBridgeRepository::getCheckBasketQuantitiesData` return type will be set to array with v4.3.0
* Deprecated `\Shopware\B2B\InStock\Bridge\ListProductGatewayDecorator::getList` return type will be set to array with v4.3.0
* Deprecated `\Shopware\B2B\OrderNumber\Framework\OrderNumberEntity::$productDetailsId` will be removed with v4.3.0 - instead use the variable $productId

### Removals

* Removed unused Database table `b2b_prices`
* Removed `\Shopware\B2B\Shop\Bridge\TranslationService` and added `\Shopware\B2B\OrderNumber\Bridge{Platform}\OrderNumberTranslationService` instead

## 4.1.0

### Improvements

* Orders waiting to be cleared no longer create orders in Shopware and therefore doesn't block stock. - ENT-2303
* Improved the sync between order and the B2B-Account greatly. - ENT-2303
* The state machine is no longer used to represent order clearance - ENT-2303
* The Checkout view now clearly states if it is in clearance mode - ENT-2303
* Clearing an order can now be aborted from the checkout view - ENT-2303

### Fixes

* Order clearance no longer overwrites the ordering user. - ENT-2303
* All state changes are synced to the B2B order - ENT-2303
* Fixed a bug in the newsletter subscription - ENT-2325

### Deprecations

* Deprecated `\Shopware\B2B\Order\Framework\OrderResponsibleEntity` use the order entity to get this information.
* Deprecated `\Shopware\B2B\Order\Framework\OrderContextService::getOrderResponsibleEntity()` use the order entity to get this information.
* Deprecated `\Shopware\B2B\Order\Framework\OrderEntity::responsible` in favor of field in `\Shopware\B2B\Order\Framework\OrderEntity::clearedBy`
* Deprecated `Shopware\B2B\Order\Framework\AcceptedOrderClearanceRepository` in favor of field in `Shopware\B2B\Order\Framework\OrderContext`
* Deprecated `\Shopware\B2B\OrderClearance\Framework\OrderItemEntity::$identifier` will be removed with v4.2.0 since it is not used across all implementations
* Deprecated `\Shopware\B2B\Budget\BridgePlatform\BudgetStatusMailData::getIdentifier()` will be removed with v4.2.0 - unused
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface` use `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository` instead
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::STATUS_ORDER_CLEARANCE` use `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository::STATE_CLEARANCE_OPEN` instead
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::STATUS_ORDER_DENIED` use `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository::STATE_CLEARANCE_DENIED` instead
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::STATUS_ORDER_OPEN` use `Shopware\B2B\Order\Framework\OrderRepository::STATE_ORDER_OPEN` instead
* Deprecated `Shopware\B2B\Order\Framework\OrderContextRepository::syncFinishOrder` use `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository::acceptOrderClearance` instead
* Deprecated `Shopware\B2B\Cart\Framework\CartHistoryRepositoryInterface` use `Shopware\B2B\Cart\Framework\CartHistoryRepository` instead
* Deprecated `Shopware\B2B\Offer\Framework\OfferContextRepository::STATUS_OFFER` use `Shopware\B2B\Offer\Framework\OfferContextRepository::STATE_OFFER` instead
* Deprecated `Shopware\B2B\Offer\Framework\OfferService::ORDER_STATUS` use `Shopware\B2B\Offer\Framework\OfferContextRepository::STATE_OFFER` instead
* Deprecated `Shopware\B2B\Order\Framework\OrderRepositoryInterface` use `Shopware\B2B\Order\Framework\OrderRepository` instead
* Deprecated `Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface` use `Shopware\B2B\Order\Framework\OrderContext::isEditable()` or `Shopware\B2B\Order\Framework\OrderContext::isOrdered()` instead
* Deprecated `Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::updateStatusByName` without replacement
* Deprecated `Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::updateStatus` without replacement
* Deprecated `Shopware\B2B\Statistic\Framework\StatisticRepository::createEqualsStatesFilter` use `\Shopware\B2B\Statistic\Framework\StatisticRepository::createEqualsStateNameFilter` instead
* Deprecated `\Shopware\B2B\Statistic\Framework\StatisticRepositoryInterface` use `\Shopware\B2B\Statistic\Framework\StatisticRepository` instead

## 4.0.2

### Improvements

* set min value from product search quantity input field to 1 if no min purchase value is set

## 4.0.1

### Improvements

* adding debtor checkbox in customer administration view

## 4.0.0

### Changes

* `Shopware\B2B\Common\MvcExtension\Request::requireFileParam()` returns now a `Symfony\Component\HttpFoundation\File\File` object - ENT-2182
* `Shopware\B2B\Common\MvcExtension\Request::getFiles()` returns now an array of `Symfony\Component\HttpFoundation\File\File` objects - ENT-2182
* `OrderStatusInterpreterServiceInterface::isOpen` `$context` parameter is now of type `Shopware\B2B\Order\Framework\OrderContext`

### Deprecations

* Increase deprecation version of `ShopwareCoreConfiguration` to v5.0.0

### Removals

* Removed deprecated `AclRoutingUpdateService::create` function
* Removed deprecated `CartStateSessionValidationInterface` interface
* Removed deprecated `AuthenticationServiceDecorator` service
* Removed deprecated `MigrationCollectionLoader::create` function
* Removed deprecated `MigrationRuntime::create` function
* Removed deprecated `EnlightRequest` class
* Removed deprecated `RoutingInterceptor` class
* Removed deprecated `ValidationException::hasViolationsForFieldWithCause` function
* Removed deprecated `ContactIdentity::getLoginContext` function
* Removed deprecated `ContactPasswordActivationEntity::type` property
* Removed deprecated `ContactPasswordActivationRepositoryInterface` interface
* Removed deprecated `LineItemReferenceRepository::fetchAllForList` function
* Removed deprecated `LineItemReferenceRepository::addVoucherCodeToReferanceById` function
* Removed deprecated `LineItemReferenceRepositoryInterface::fetchAllForList` function
* Removed deprecated `LineItemReferenceRepositoryInterface::addVoucherCodeToReferanceById` function
* Removed deprecated `LineItemReferenceService::mapCustomOrderNumbers` function
* Removed deprecated `OfferValidationService::createInsertValidation` function
* Removed deprecated `LineItemReferenceRepositoryDecorator::fetchAllForList` function
* Removed deprecated `LineItemReferenceRepositoryDecorator::addVoucherCodeToReferanceById` function
* Removed deprecated `OrderNumberService::validateOrderNumber` function
* Removed deprecated `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeRepository` class
* Removed deprecated `StoreFrontAuthenticationRepository::syncAvatarImage` function
* Removed deprecated `StoreFrontAuthenticationRepository::fetchAvatarById` function
* Removed deprecated `$basketArray` parameter of `ClearanceBudgetItemLoader::fetchItemsFromBasketArray`
* Removed deprecated return type of `FastOrderService::produceCart`
* Removed deprecated return type of `LineItemShopWriterServiceInterface::triggerCart`
* Removed deprecated `$basketArray` parameter of `OrderItemLoaderInterface::fetchItemsFromBasketArray`
* Removed deprecated default `$grantable` value of `RoleContactAclAccessWriter::addNewSubject`
* Removed deprecated default `$grantable` value of `RoleContingentGroupAclAccessWriter::addNewSubject`
* Removed deprecated `SalesRepresentativeEntity::fromDatabaseArray` function
* Removed deprecated `SalesRepresentativeService::isSalesRepresentativeClient` function
* Removed deprecated `Shopware\B2B\OrderClearance\Bridge\OrderItemLoaderInterface` interface
* Removed deprecated `Shopware\B2B\Order\Bridge\OrderOwnerRepository` service
* Removed deprecated `Shopware\B2B\Order\Bridge\DebtorAssigner` service
* Removed deprecated `b2b_sales_representative.client_authentication_repository` tag


## 3.0.6

### Fixes

* Show correct version in package - B2B-282
* Fixed error in API with the parameter `useNumberAsId` - B2B-76
* Fixed infinite loading indicator in the role permission management - B2B-334
* Fixed API URL encoding - ENT-2565

## 3.0.5

### Improvements

* Gross amounts will be hidden if they are deactivated in the customer group settings - ENT-1881

### Fixes

* Migrate old debtor data from version 3.0.0 and lower if old data exists.
* Prevent update error from 3.0.1 to 3.0.2 if 2 debtor with same provider_context exists
* Fixed clone attributes - ENT-2376
* Fix exception when trying to go back to the customer overview as sales representative - ENT-2398
* Added missing variant description to fast order AJAX search - ENT-2112
* Fixed deleted order request when offer goes back to clearance - ENT-2023
* Fixed error in order overview from removing a contact with an order - ENT-2295

### Additions

* Remove a contact triggers shopware user data deletion and transfer offer and order data to the debtor - ENT-2259

## 3.0.4

### Fixes

* Changed sales representative provider key to SalesRepresentativeRepositoryInterface.
* Fixed error messages in the budget management.

### Improvements

* Stock values in the b2b_in_stocks table are no longer pseudo values - ENT-1916
* Orders will be blocked by products without enough stocks in the b2b_in_stocks table - ENT-1916

## 3.0.3

### Fixes

* Newsletter subscription creates now a newsletter recipient - ENT-2294
* Pin phpdocumentor/reflection-docblock Version
* Fix line item sorting
* Fix contact being created for debtor
* Fix line items for numeric order numbers

### Improvements

* Contacts cannot remove himself - ENT-2229

## 3.0.2

### Fixes

* Fixed newsletter registration

## 3.0.1

### Improvements

* Improve backend sales representative client assignment
* Add debtor to b2b_store_front_auth table after create in backend
* Enable actions column for more offer states

### Fixes

* Fixed performance problems for sales representative login with many clients
* Fixed order number import format
* Fix issue with numeric ordernumbers

### Deprecations

* Deprecated `Shopware\B2B\LineItemList\Framework\LineItemShopWriterServiceInterface::triggerCart` return type will be void in Version 4.0.0
* Deprecated `Shopware\B2B\LineItemList\Bridge\LineItemShopWriterService::triggerCart` return type will be void in Version 4.0.0
* Deprecated `Shopware\B2B\FastOrder\Framework\FastOrderService::produceCart` return type will be void in Version 4.0.0
* Deprecated `Shopware\B2B\Order\Bridge\OrderOwnerRepository` class will be moved into framework in Version 4.0.0
* Deprecated `Shopware\B2B\Order\Framework\OrderOwnerRepositoryInterface` interface will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity::fromDatabaseArray` will be removed in 4.0.0
* Deprecated `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity::clients` will be removed in 4.0.0
* Deprecated `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeService::isSalesRepresentativeClient` will be removed in 4.0.0 use `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository::isSalesRepresentativeClient` instead
* Deprecated `Shopware\B2B\Order\Framework\ShopOrderDetailsRepositoryInterface::fetchAllProductsByOrderId` will be exchanged with `createListByOrderId(int $orderId, OwnershipContext $ownershipContext): LineItemList` in Version 4.0.0
* Deprecated `Shopware\B2B\Order\Bridge\ShopOrderDetailsRepository::fetchAllProductsByOrderId` will be exchanged with `createListByOrderId(int $orderId, OwnershipContext $ownershipContext): LineItemList` in Version 4.0.0
* Deprecated `Shopware\B2B\LineItemList\Framework\LineItemListService::createListByShopOrderDetailsArray` will be moved to `Shopware\B2B\Order\Bridge\ShopOrderDetailsRepository::fetchAllProductsByOrderId` in Version 4.0.0
* Deprecated `Shopware\B2B\LineItemList\Framework\LineItemReference::fromShopOrderDetails` will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\Order\Bridge\OrderCheckoutCode` class will be moved into framework in Version 4.0.0
* Deprecated `Shopware\B2B\Order\Bridge\DebtorAssigner` class will be removed in Version 4.0.0, functionality will be merged into `Shopware\B2B\Order\Bridge\AuthAssigner`
* Deprecated `Shopware\B2B\Order\Bridge\OrderChangeQueueRepository::setRequestUid` will be removed in Version 4.0.0, use `setRequestUuid` instead
* Deprecated `Shopware\B2B\Order\Bridge\OrderChangeTrigger::setRequestUid` will be removed in Version 4.0.0, use `setRequestUuid` instead
* Deprecated `Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface::isOpen` parameter type will be changed to `Shopware\B2B\Order\Framework\OrderContext` in Version 4.0.0
* Deprecated `Shopware\B2B\Order\Framework\OrderContext::isEditable` will be removed in Version 4.0.0, use `Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface` instead
* Deprecated `Shopware\B2B\Budget\Bridge\ClearanceBudgetItemLoader` will be moved into the framework in Version 4.0.0
* Deprecated `Shopware\B2B\Budget\Bridge\ClearanceBudgetItemLoader::fetchItemsFromBasketArray` $basketArray will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\OrderClearance\Bridge\OrderItemLoaderInterface` will be moved into the framework in Version 4.0.0
* Deprecated `Shopware\B2B\OrderClearance\Bridge\OrderItemLoaderInterface::fetchItemsFromBasketArray` $basketArray will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\OrderClearance\Bridge\OrderClearanceEntityFactory::extendSessionOrderWithItems` $basket will be removed in Version 4.0.0
* Deprecated tag `b2b_sales_representative.client_authentication_repository` will be removed in Version 4.0.0, use `b2b_sales_representative.client_authentication_loader` instead
* Deprecated `Shopware\B2B\Acl\Framework\AclAccessWriter::addNewSubject` $grantable default value will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\Acl\Framework\AclAccessWriterInterface::addNewSubject` $grantable default value will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\RoleContact\Framework\RoleContactAclAccessWriter::addNewSubject` $grantable default value will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\RoleContingentGroup\Framework\RoleContingentGroupAclAccessWriter::addNewSubject` $grantable default value will be removed in Version 4.0.0
* Deprecated `Shopware\B2B\Role\Framework\RoleRepository::insetRole` use `Shopware\B2B\Role\Framework\RoleRepository::insertRole` instead
* Deprecated `Shopware\B2B\ContingentRule\Framework\ContingentRuleService::validateContingentRules` function will be removed in Version 4.1.0
* Deprecated `Shopware\B2B\ContingentRule\Framework\ContingentRuleService::getInvalidContingentRuleOrderNumberIds` function will be removed in Version 4.1.0
* Deprecated `Shopware\B2B\ContingentRule\Framework\ContingentRuleService::checkPostForValidOrderNumber` function will be removed in Version 4.1.0

## 3.0.0

### Improvements

* New sales representative backend view for client selection - ENT-1673
* Extend the AddressRepositoryInterface and its usage
* Adaption of the contingent and budget view in the checkout - ENT-1912
* Additional filter options - ENT-1914
* Adaption of the snippets - ENT-1970
* Order reference number and requested delivery time in the sOrder template mail - ENT-1945
* Added a placeholder for undefined responsibles into the budget view - NTR
* Changing password will now require a password of configurable length - ENT-1959
* As debtor changing the password of one of your contacts will now reset its locked state - ENT-1869
* Contact password activation now uses its own table - ENT-1354
* Tree navigation for role management highlights newly created roles - ENT-2107

### Fixes

* Fixed a cart failure because of a timeout in the offer process - ENT-1770
* Fixed a bug in the budget view - ENT-2090
* Fixed defective migration script - ENT-1991
* Fixed a bug by sending offer notification mail to admin although it is not configured - ENT-2132
* Fixed fast order search request - ENT-1947

### Additions

* Added contingent restriction rule to determine purchaseable products - ENT-1329
* Add sales representative mapping to order context - ENT-1676, ENT-1302
* Add php 7.2 support - ENT-2031
* Add shopware 5.6 support - ENT-2031
* Add `Shopware\B2B\Common\MvcExtension\MvcEnvironmentInterface` to use by the REST-API
* Additional API endpoint for converting shop orders into the B2B environment - ENT-1787

### Changes

* Moved `Shopware\B2B\Common\RestApi\ApiControllerSubscriber` to `SwagB2bPlugin\Subscriber`
* Moved `Shopware\B2B\Common\RestApi\SwgaB2BController` to `SwagB2bPlugin\Controllers\Api`
* Encapsulation of the UserLoginContext from the framework - ENT-2109
* Dropped PHP 7.0 support - ENT-2129
* Snippet changes - NTR

### Removals

* Removed price component. Please use the SwagEnterprisePricingEngine.
* Dropped support for shopware 5.3.x - ENT-2031
* UserLoginContext of the framework is now deprecated and will be removed in 4.0. Please use this from the bridge.

### Deprecations

* Deprecated all usages of `Enlight` and `Shopware()` in Common\
* Deprecated `Shopware\B2B\Common\Backend\ControllerProxy` in favor of `SwgB2bPlugin\Extension\BackendControllerProxy` for version 4.0.0
* Deprecated `Shopware\B2B\Common\Frontend\ControllerProxy` in favor of `SwgB2bPlugin\Extension\FrontendControllerProxy` for version 4.0.0
* Deprecated `Shopware\B2B\Common\MvcExtension\EnlightRequest` in favor of `SwgB2bPlugin\Extension\EnlightRequest` for version 4.0.0
* Deprecated `Shopware\B2B\Common\MvcExtension\MvcEnvironment` in favor of `SwgB2bPlugin\Extension\MvcEnvironment` for version 4.0.0
* Deprecated `Shopware\B2B\Common\MvcExtension\RoutingInterceptor` in favor of `SwgB2bPlugin\Extension\RoutingInterceptor` for version 4.0.0
* Deprecated `Shopware\B2B\Common\Migration\MigrationCollectionLoader::create()` in favor of its constructor for version 4.0.0
* Deprecated `Shopware\B2B\Common\Migration\MigrationRuntime::create()` in favor of its constructor for version 4.0.0
* Deprecated `Shopware\B2B\Common\ShopwareCore` without replacement since it was no longer used

## 2.0.6

### Fixes

* Fixed incorrect namespace

## 2.0.5

### Fixes

* Updated used library dbal-nested-set
* Fixed migration for not english or german standard locales

## 2.0.4

### Fixes

* Fix for negative tax values - ENT-1946

## 2.0.3

### Improvements

* Add jQuery-Plugins to StateManager - ENT-1446
* B2B template mails by default in english - ENT-1747
* Deleted products are displayed in orders, order releases and order lists - ENT-1560
* Role detail view divided into "Visibility" and "Account" - ENT-1641
* Action column in the order lists view has been increased - ENT-1785
* Account section in role editing renamed to "Assignments" - ENT-1771
* Added newsletter subscription management - ENT-1696
* Orders from debtors and sales representatives are highlighted by icons - ENT-1757
* Budget net values are shown more clearly - ENT-1518
* Validation of unavailable products in order clearance - ENT-1578
* Column numbers starts with 0 in CSV, XLS and XLSX files - ENT-1731
* Orders by sales representatives are displayed more clearly - ENT-1302
* Contacts and order responsibles will be shown in the order overview - ENT-1733
* Clients view of the sales representative includes the company and can then be filtered - ENT-1218

### Fixes

* Bug fix for mobile product search in the modules fast order and custom product numbers - ENT-1685
* Vouchers will be considered in the order clearance - ENT-1663
* Remove context owner data when the debtor will be deleted - ENT-1658
* Add entry for b2b_store_front_auth by adding or updating a debtor - ENT-1703
* Unauthorized storage of order data fixed - ENT-1722
* Fatal error when clicking on "No lists available" (order lists) fixed - ENT-1767
* Budget pre selection is taken into account in the order lists - ENT-1776
* Invalid product numbers can no longer be stored in the contingent restriction - ENT-1328
* Fixed bug when saving client assignments in the sales representative view in the backend - ENT-1735

### Additions

* Get order contexts from API - ENT-1695
* Sales representative clients API - ENT-1694
* Get order contexts from API - ENT-1695
* Visibility of orders by contacts for contacts by matching permissions - ENT-1623
* Add entry for b2b_store_front_auth by adding or updating a debtor - ENT-1703
* Show customer number in b2baccount - external MR from Gitlab
* Show customer address in the b2baccount
* Added column with responsible of the budgets into budget grid - ENT-1517
* Send email when accepted order clearance - ENT-1553
* Added column with customers into order view - ENT-1720
* Notification for pending order clearance - ENT-1554
* Latest order list as default - ENT-896
* Added column with product availability into order clearance detail grid - ENT-1561
* Avatars in the contact overview - ENT-1299
* Order lists can be export as CSV files - ENT-1613
* API endpoints for the order number component - ENT-1774
* Added `orderReference` and `requestedDeliveryDate` to sORDER Mail - ENT-1782
* Percentage discounts in the offer process - ENT-1898

### Deprecations

* Deprecated price component. Please use the SwagEnterprisePricingEngine

## 2.0.2

### Improvements

* Auditlog divided ENT-1618
* Restyling orderlists component ENT-1622
* Use contact id instead of email in frontend controllers
* Scrollable ajax product search results ENT-1568
* Scrollable B2B-navigation in the tablet view ENT-1671

### Fixes

* Correction of the english snippets ENT-1625
* Fix add product to order list ENT-1621
* Fix incorrect sorting by date ENT-1569
* Fix a fatal error when adding an invalid productnumber ENT-1640
* Corrected snippets and fixed an incorrect forwarding when adding products to the cart ENT-1636
* Customer group related prices ENT-1552
* Show error message if a sales representative client has no password ENT-1675
* Fix infinite loading indicators ENT-1688
* Rights are no longer given unexpectedly ENT-1697
* Error due to preselection of insufficient budget fixed. ENT-1687
* Fix navigation for firefox ENT-1690

### Additions

* Warning when overwriting productnumbers by file upload ENT-1620
* Added "add item" event to fast-order
* JavaScript Events for the fastorder module ENT-1652
* JavaScript Events for the custom product numbers module ENT-1653

## 2.0.1

### Fixes

* Fix product quantity update in the order clearance module
* Compatibility with ES
* Custom ordernumber product name searchable
* Fix offer request submission from cart

## 2.0.0

### Additions

* Hierarchies
* Request for quotation
* ProductNameAwareInterface for easy translation of the product name
* Debtor as contact person for Budgets ENT-1335
* Changed visibility of the wish list button in the product detail view
* Changed FrontendAccountFirewall $routes property from private to protected
* Performance improvements for the AuthenticationService
* Usage of ContextServiceInterface instead of ContextService

### Fixes

* Display variants in product search ENT-1427
* View all order lists ENT-1527
* Move AddressRepository to Bridge Namespace ENT-1555
* Support of required fields in billing/shipping addresses ENT-1313
* BudgetNotify Cron ENT-1591
* Fixed budget checkout handling
* Fixed "add another contact"
* Several fixes: ENT-1438, ENT-1549
* Fixed password reset function

### Removals

* Shopware\B2B\AclRoute\Frontend\AssignmentController::gridAction()
* Shopware\B2B\Address\Frontend\ContactAddressController::billingAction()
* Shopware\B2B\Address\Frontend\ContactAddressController::shippingAction()
* Shopware\B2B\Budget\Framework\BudgetRepository::fetchBudgetContactList()
* Shopware\B2B\Budget\Frontend\BudgetController::editAction()
* Shopware\B2B\Budget\Frontend\ContactBudgetController::gridAction()
* Shopware\B2B\Contact\Frontend\ContactContactVisibilityController::gridAction()
* Shopware\B2B\Contact\Frontend\ContactController::indexAction()->contactGrid view Variable
* Shopware\B2B\ContingentGroupContact\Frontend\ContactContingentController::gridAction()
* Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeFactory::getAllTypeNames()
* Shopware\B2B\FastOrder\Frontend\FastOrderController::processProductsAction()
* Shopware\B2B\FastOrder\Frontend\FastOrderController::processItemsFromListingAction()
* Shopware\B2B\Order\Bridge\OrderRepository::setRequestedDeliveryDateByOrderContextId()
* Shopware\B2B\Order\Bridge\OrderRepository::setOrderReferenceNumber()
* Shopware\B2B\Order\Bridge\OrderRepository::updateRequestedDeliveryDate()
* Shopware\B2B\Order\Bridge\OrderRepository::setRequestedDeliveryDate()
* Shopware\B2B\Order\Bridge\ShopOrderRepository::setRequestedDeliveryDate()
* Shopware\B2B\Order\Bridge\ShopOrderRepository::updateOrderReferenceNumber()
* Shopware\B2B\Order\Framework\OrderContextRepository::setOrderNumber()
* Shopware\B2B\Order\Framework\OrderRepositoryInterface::setOrderCommentByOrderContextId()
* Shopware\B2B\Order\Framework\OrderRepositoryInterface::setOrderCommentByOrderContextId()
* Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::setRequestedDeliveryDate()
* Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::setOrderCommentByOrderContextId()
* Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::updateOrderReferenceNumber()
* Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::acceptOrder()
* Shopware\B2B\OrderList\Framework\OrderListService::createLineItemListFromProductsRequest()
* Shopware\B2B\OrderList\Framework\ContactOrderListController::gridAction()
* Shopware\B2B\OrderList\Framework\RoleOrderListController::gridAction()
* Shopware\B2B\RoleContact\Frontend\RoleContactVisibilityController::gridAction()
* Shopware\B2B\StoreFrontAuthentication\Bridge\CredentialsBuilder
* Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface
* Shopware\B2B\StoreFrontAuthentication\Bridge\UserRepository::syncContact()

## 1.5.1

### Additions

* Performance improvements for the AuthenticationService

### Fixes

* Changed visibility of the wish list button in the product detail view
* Fixed budget checkout handling
* Fixed "add another contact"
* Changed FrontendAccountFirewall $routes property from private to protected
* Usage of ContextServiceInterface instead of ContextService
* Fixed password reset function

## 1.5.0

### Additions

* Added a profile page for contacts, debtors and sales representatives
* Added a cronjob and cli command for order sync implemented
* Added html min values for number inputs
* Added default billing and shipping address
* Added sorting for order list items

### Fixes

* Improved API contact creation: get context owner id from debtor email
* Improved fast order upload
* Improved default styling
* Added missing tooltips to anchor tags and icons
* Fixed budget selection in checkout
* Fixed pagination in customer overview
* Optimized snippets
* Fixed order reference number and requested deliver date handling
* Fixed price handling for show net prices in frontend
* Fixed exception if order has no shipping method

### Removals

* Removed order attribute b2b_requested_delivery_date
* Removed order attribute b2b_order_reference
* Removed order attribute b2b_clearance_comment
* Removed user attribute b2b_sales_representative_media_id

## 1.4.2

### Fixes

* Fixed backend customer listing
    * pagination
    * listing
    * sorting
* Fixed customer creation by API for debtors

## 1.4.1

### Additions

* Added improvements for the docker environment

### Fixes

* Fixed cart history calculation
* Fixed datepicker handling
* Added contingent group grid reload after detail save
* Fixed role visibility after creation by contact
* Fixed contact visibility after creation by contact

### Removals

* Didn't clear the basket in produceCart method

## 1.4.0

### Additions

* Added PHP 7.1 support
* Added ACL classes to modal save buttons
* Added contact visibility permission to contact after contact creation
* Added contact visibility permission to contact after role creation
* Added mapped route assignment for allow process at permission management
  * `b2b_acl_route.route_mapping` - mapping DI variable

### Fixes

* Fixed wrong date declaration for `components/Statistic/Framework/Statistic` entity
* Fixed clearance decline error
* Fixed role grid reload after change in role detail modal
* Fixed `swDatePicker` duplication at statistics

### Removals

* Removed composer dependency `roave/security-advisories`

## 1.3.1

### Fixes

* Fixed migration for debtors without sales representatives

## 1.3.0

### Additions

* Added Shopware 5.3.3 support

## 1.2.0

### Additions

* Added Shopware 5.2 support
* Added support of multiple currencies

## 1.0.0

* Initial release
