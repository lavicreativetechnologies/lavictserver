# CHANGELOG für B2B-Suite

Diese Changelog referenziert Änderungen zu den B2B-Suite Versionen.
[Sehe alle Änderungen der B2B-Suite online.](https://docs.enterprise.shopware.com/b2b-suite/changelog/)

## 4.3.3

### Verbesserungen

* Aussehen der Firma-Seite auf dem Handy geändert - B2B-115
* Das Snippet, welches einem Außendienstmitarbeiter ohne Klienten angezeigt wird, wurde geändert - B2B-107
* Kompatibilität zu Shopware 6.3.5 hergestellt

### Deprecations

* Offer Status E-Mail wurde hinzugefügt - B2B-418
* Deprecated die dynamischen Variablen `languageId` und `salutationId` in `Shopware\B2B\Debtor\Framework\DebtorEntity` in `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntityFactory` - B2B-418
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceShopWriterServiceInterface::stopOrderClearance`, Methode wird eine `Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity` in 4.4.0 zurückliefern - B2B-401

## 4.3.2

### Fixes

* Fehler beim Login mit aktivierter Elasticsearch behoben - B2B-376
* Fehler beim Hochladen von Dateien in Schnellbestellung behoben - B2B-414
* Fehler behoben, der das Ändern des Bestellstatus in der Administration verhindert hat - B2B-416
* Es wurde ein Fehler behoben, welcher die Benutzbarkeit der Schnellbestellung-Seite betrifft - B2B-375

### Verbesserungen

* Dialogfenster werden nun beim Klick in den Hintergrund geschlossen - B2B-412

### Deprecations

* Deprecated `Shopware\B2B\Shop\Framework\SessionStorageInterface`, Interface wird in 4.4.0 zu `Shopware\B2B\Shop\Framework\StorageInterface` umbenannt - B2B-402
* Deprecated `Shopware\B2B\Shop\BridgePlatform\SessionStorage`, Klasse wird in 4.4.0 zu `Shopware\B2B\Shop\BridgePlatform\Storage` umbenannt und nutzt neue Konstruktor-Parameter - B2B-402

## 4.3.1

### Neue Funktionen

* Vervollständigung der Funktionalitäten der Bestellreferenznummer und des Wunschliefertermins in SwagB2bPlatform - B2B-194

### Verbesserungen

* Änderung der Offer-Status-Benachrichtigungen - B2B-330
* Tabellenüberschrift von "Außendienstmitarbeiter Klienten" / "Außendienstler Kunden" zu "Klienten" geändert - B2B-377

### Fixes

* Fehlermeldung, beim Versuch ein zu kurzes Passwort zu ändern, wird nun übersetzt - B2B-129
* Behebt Kompatibilitätsprobleme mit dem Internet Explorer 11 und Edge - B2B-249
* Behebt ein Kompatibilitätsproblem mit dem Plugin "SwagCustomizedProducts" wodurch das Theme nicht kompiliert werden konnte, wenn "SwagCustomizedProducts" aktiviert wird. - NEXT-7846

### Changes

* Die Klasse `app\storefront\src\js\classes\B2bPluginClass.ts` wurde in die Methode `app\storefront\src\js\utility\getB2bPluginClass.ts` verschoben - B2B-249
* Das SCSS Mixin `border-radius` wurde entfernt aus `app\storefront\src\styles\abstracts\mixins\_utility.scss`. Das Mixin hatte einen Namenskonflikt mit dem bereits existierenden `border-radius` Mixin von Bootstrap aus dem Shopware Core. - NEXT-7846
* Das SCSS Mixin `animation` wurde entfernt aus `app\storefront\src\styles\abstracts\mixins\_utility.scss`. Das Mixin wird nicht verwendet und setzt nur eine CSS-Eigenschaft, was auch über die native Eigenschaft möglich ist, ohne das Mixin zu verwenden. - NEXT-7846
* Das SCSS Mixin `opacity` wurde entfernt aus `app\storefront\src\styles\abstracts\mixins\_utility.scss`. Das Mixin setzt nur eine CSS-Eigenschaft, was auch über die native Eigenschaft möglich ist, ohne das Mixin zu verwenden. - NEXT-7846

### Deprecations

* Deprecated Variable 'parameter' genutzt um Nachrichten zu formatieren, wird in 4.4.0 entfernt, anstelle dessen wird 'parameters' genutzt - B2B-129
    * `Shop/BridgePlatform/MessageFormatter.php` in der Funktion formatSessionMessage() - B2B-129
    * `SwagB2bPlatform/Resources/views/storefront/_partials/_b2bmessage/controller/_b2bmessage-set.html.twig` - B2B-129
* Deprecated `Shopware\B2B\Common\MvcExtension\MvcEnvironmentInterface`, Klasse wird in 4.4.0 ohne Ersatz entfernt - B2B-354
* Deprecated `SwagB2bPlatform\Extension\MvcEnvironment`, Klasse wird in 4.4.0 ohne Ersatz entfernt - B2B-354
* Deprecated `SwagB2bPlugin\Extension\MvcEnvironment`, Klasse wird in 4.4.0 ohne Ersatz entfernt - B2B-354

## 4.3.0

### Verbesserungen

* Durchsuchen der Außendienstmitarbeiten-Kunden in der Administration - B2B-247

### Änderungen

* Die Methode `\Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository::fetchCombinedClientListTotalCount` hat ein zweites optionalen Argument `SearchStruct` dazu bekommen, der ab 4.4.0 benötigt wird.
* Der Service `\Shopware\B2B\Shop\BridgePlatform\SalesChannelContextProvider` wurde in `\Shopware\B2B\Shop\BridgePlatform\ContextProvider` umbenannt.
* Im Template `components/SwagB2bPlatform/Resources/views/storefront/frontend/b2bacl/error.html.twig` wurde der Block `frontend_index_content_b2b` mit `b2blayout_content` ersetzt - B2B-228

### Fehlerbehebung

* Problem behoben, durch das eine Bestellung mit Artikeln aus ausgeschlossenen Kategorien, trotz Kontingentsbeschränkungen, möglich war - ENT-2488
* Problem mit ignorierten Kontingenten behoben - ENT-2386
* Fehler beim Abrufen der Bestelllisten eines Debitors über die OrderList-API behoben, der zu einer Authentifizierungs-Exception führte - ENT-2494
* Falsche Beträge beim Duplizieren von Bestelllisten bei bestimmten Währungen behoben - ENT-2477
* Installationsfehler behoben, wenn englisch nicht als Standardsprache ausgewählt ist - ENT-2512
* Bestelllisten-Export behoben, sofern der Name Slashes enthält - ENT-2476
* Fehler "Identität nicht gesetzt" behoben, sofern ein Angebot verarbeitet wird - ENT-2548
* Problem beim Erweitern vom B2B-Suite durch andere Plugins behoben - B2B-253
* Falsche Bestellbeträge aufgrund der zugewiesenen Nettokundengruppe behoben - B2B-114
* Fehler behoben, dass man eine bereits existierende Produktnummer als eigene Bestellnummer anlegen kann - ENT-2156
* Fehler in der Bestellübersicht behoben, wenn ein Kontakt mit einer Bestellung gelöscht wurde - ENT-2295
* Fehler beim Login behoben - B2B-296
* Fehler mit den Gastbestellungen behoben - B2B-320

### Änderungen

* Löschen eines Kontaktes löscht die Kundendaten in Shopware und überträgt die Bestellungen und Angebote zum Debitor - ENT-2259
* Fehler mit Elasticsearch und den individuellen Bestellnummern bzw. InStock behoben - ENT-2526
* Angebote werden jetzt auch durch die Kontakt-Sichtbarkeit gefiltert - B2B-234
* Angebots-Modul für Shopware 6 wurde hinzugefügt - B2B-154

### Deprecations

* Deprecated `\Shopware\B2B\LineItemList\Bridge\ProductProvider::updateListWithoutUpdatingProductReferences` Funktion wird mit v4.4.0 entfernt - Benutze `\Shopware\B2B\LineItemList\Framework\LineItemList::recalculateListAmounts` stattdessen.
* Deprecated `\Shopware\B2B\LineItemList\BridgePlatform\LineItemListMailData::updateListWithoutUpdatingProductReferences` Funktion wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::updateListWithoutUpdatingProductReferences` Implementierung von `CurrencyAware` wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList` Variable `$currency_factor` wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::getCurrencyFactor` Funktion wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::setCurrencyFactor` Funktion wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemList::getAmountPropertyNames` Funktion wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemListRepository::fetchOneListById` Parameter `$currencyContext` wird nicht mehr verwendet und wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemListService::addUpAmountsIntoListById` Funktion wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::fetchList` Parameter `$currencyContext` wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::fetchOneById` Parameter `$currencyContext` wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::fetchDefaultOrderList` Parameter `$currencyContext` wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\OrderList\Framework\OrderListRepository::setDefaultOrderList` Parameter `$currencyContext` wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\OrderList\Framework\RemoteBoxService::createLineItemListFromProductsRequest` Parameter `$ownershipContext` wird nicht mehr verwendet und wird mit v4.4.0 entfernt
* Deprecated `\Shopware\B2B\Shop\Framework\RoundingInterface::round` Standardwert für den Parameter `$quantity` wird zu 1 mit v4.4.0
* Deprecated `\Shopware\B2B\Offer\Framework\CreateOfferThroughCartInterface`. Es wird mit v4.4.0 entfernt. Bitte nutze anstelle `\Shopware\B2B\Offer\Framework\OfferFromCartCreatorInterface`.
* Deprecated `\Shopware\B2B\Offer\Bridge\CreateOfferThroughCart`Es wird mit with v4.4. entfernt. Bitte nutze anstelle `\Shopware\B2B\Offer\Bridge\OfferFromCartCreator`.
* Deprecated `\Shopware\B2B\Offer\BridgePlatform\CreateOfferThroughCart`. Es wird mit v4.4.0 entfernt. Bitte nutze anstelle `\Shopware\B2B\Offer\BridgePlatform\OfferFromCartCreator`.
* Deprecated den Service `b2b_offer.create_offer_through_cart`. Es wird mit v4.4.0 entfernt. Bitte nutze anstelle `b2b_offer.offer_from_cart_creator`.
* Deprecated `\Shopware\B2B\Offer\BridgePlatform\CreateOfferThroughCart`. Es wird mit v4.4.4 entfernt. Bitte nutze anstelle `\Shopware\B2B\Offer\BridgePlatform\OfferFromCartCreator`.
* Deprecated the service `b2b_offer.offer_from_cart_creator`. Es wird mit v4.4.4 entfernt. Bitte nutze anstelle `b2b_offer.offer_from_cart_creator`.
* Deprecated Validierungsregeln in `\Shopware\B2B\Common\Validator\ValidationBuilder`. Diese Methoden werden mit v4.4.0 den return type `self` erhalten.
* Deprecated `\Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface::fetchIdByCustomOrderNumber`. Parameter `$ownershipContext` wird mit v4.4.0 verpflichtend
* Deprecated `\Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface::fetchIdByProductDetailsId`. Parameter `$ownershipContext` wird mit v4.4.0 verpflichtend

### Entfernungen

* Datenbankspalte `currency_factor` wurde aus der Tabelle `b2b_line_item_list` entfernt
* Unterstützung für Shopware 6.1 entfernt

## 4.2.0

### Änderungen

* `Shopware\B2B\OrderNumber\Framework\OrderNumberCrudService::createCsvImport` gibt nun ein `Shopware\B2B\OrderNumber\Framework\OrderNumberFileEntity` Objekt statt void zurück
* Die Eigenschaft `$owner` von `OrderEntity` in die Klasse `OrderContext` verschoben - ENT-2270

### Fehlerbehebung

* Synchronisiere Adressen in den B2B-Kontext - ENT-2388

### Verbesserungen

* Alle Datenbank-Tabellen verwenden nun `utf8mb4_unicode_ci` als Kollation
* Migration des InStock-Moduls für SwagB2bPlatform - ENT-2284
* Migration des OrderNumber-Moduls für SwagB2bPlatform - ENT-2247
* Migration des SalesRepresentative-Moduls für SwagB2bPlatform - ENT-2246

### Deprecations

* Deprecated `\Shopware\B2B\OrderNumber\Framework\LineItemReferenceRepositoryDecorator::updateReference` Rückgabetyp wird mit v4.3.0 als void festgelegt 
* Deprecated `\Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface::updateReference` Rückgabetyp wird mit v4.3.0 als void festgelegt
* Deprecated `\Shopware\B2B\Order\Framework\OrderOwnerRepository` wird mit v4.3.0 entfernt - ENT-2270
* Deprecated `\Shopware\B2B\Order\Framework\OrderOwnerService::createOrderOwner` wird mit v4.3.0 entfernt - Nutze `\Shopware\B2B\Order\Framework\OrderOwnerService::createOrderContextOwner` stattdessen - ENT-2270

### Entfernungen

* Datenbanktabelle `b2b_sales_representative_orders` entfernt - ENT-2270

## 4.1.1

### Verbesserungen

* Das Prozentfeld in den Budgets ist nur erforderlich, wenn die Benachrichtigungsoption ausgewählt ist - ENT-2321

### Deprecations

* Deprecated `\Shopware\B2B\InStock\Framework\InStockEntity::$articlesDetailsId` wird mit v4.3.0 entfernt - nutze die Variable $productId stattdessen
* Deprecated `\Shopware\B2B\InStock\Bridge\InStockBridgeRepository::getCheckBasketQuantitiesData` Rückgabetyp wird mit v4.3.0 als Array festgelegt
* Deprecated `\Shopware\B2B\InStock\Bridge\ListProductGatewayDecorator::getList` Rückgabetyp wird mit v4.3.0 als Array festgelegt
* Deprecated `\Shopware\B2B\OrderNumber\Framework\OrderNumberEntity::$productDetailsId` wird mit v4.3.0 entfernt - nutze stattdessen die Variable $productId  

### Entfernungen

* Ungenutzte Datenbanktabelle `b2b_prices` entfernt
* `\Shopware\B2B\Shop\Bridge\TranslationService` entfernt und stattdessen `\Shopware\B2B\OrderNumber\Bridge{Platform}\OrderNumberTranslationService` hinzugefügt

## 4.1.0

### Verbesserungen

* Offene Freigaben erstellen keine Bestellung mehr und blockieren keine Bestände - ENT-2303
* Es werden nun mehr Daten aus der Bestellung im B2B-Account angezeigt - ENT-2303
* Der Zustandsautomat wird nicht mehr für die Freigaben benutzt - ENT-2303
* Die Bestellansicht zeigt nun ob man sich in einer Freigabe befindet - ENT-2303
* Die Bestellfreigabe kann nun in der Betsellansicht abgebrochen werden - ENT-2303

### Fehlerbehebung

* Bestellfreigaben überschreiben nicht länger den Besteller - ENT-2303
* Änderungen des Zustands der Bestellung werden im B2B-Account nun alle angezeigt - ENT-2303
* Fehler im Newsletter-Abonnement wurde behoben - ENT-2325

### Deprecations

* Deprecated `\Shopware\B2B\Order\Framework\OrderResponsibleEntity` benutze die OrderEntity zum Abrufen der Informationen.
* Deprecated `\Shopware\B2B\Order\Framework\OrderContextService::getOrderResponsibleEntity()` benutze die OrderEntity zum Abrufen der Informationen.
* Deprecated `\Shopware\B2B\Order\Framework\OrderEntity::responsible` zugunsten des Feldes in `\Shopware\B2B\Order\Framework\OrderEntity::clearedBy`
* Deprecated `Shopware\B2B\Order\Framework\AcceptedOrderClearanceRepository` zugunsten des Feldes in `Shopware\B2B\Order\Framework\OrderContext`
* Deprecated `\Shopware\B2B\OrderClearance\Framework\OrderItemEntity::$identifier` wird mit v4.2.0 entfernt, da es nicht in allen Implementierungen verwendet wird
* Deprecated `\Shopware\B2B\Budget\BridgePlatform\BudgetStatusMailData::getIdentifier()` wird mit v4.2.0 entfernt - keine Verwendung
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface` nutze `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository` stattdessen
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::STATUS_ORDER_CLEARANCE` nutze `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository::STATE_CLEARANCE_OPEN` stattdessen
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::STATUS_ORDER_DENIED` nutze `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository::STATE_CLEARANCE_DENIED` stattdessen
* Deprecated `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::STATUS_ORDER_OPEN` nutze `Shopware\B2B\Order\Framework\OrderRepository::STATE_ORDER_OPEN` stattdessen
* Deprecated `Shopware\B2B\Order\Framework\OrderContextRepository::syncFinishOrder` nutze `Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository::acceptOrderClearance` stattdessen
* Deprecated `Shopware\B2B\Cart\Framework\CartHistoryRepositoryInterface` nutze `Shopware\B2B\Cart\Framework\CartHistoryRepository` stattdessen
* Deprecated `Shopware\B2B\Offer\Framework\OfferContextRepository::STATUS_OFFER` nutze `Shopware\B2B\Offer\Framework\OfferContextRepository::STATE_OFFER` stattdessen
* Deprecated `Shopware\B2B\Offer\Framework\OfferService::ORDER_STATUS` nutze `Shopware\B2B\Offer\Framework\OfferContextRepository::STATE_OFFER` stattdessen
* Deprecated `Shopware\B2B\Order\Framework\OrderRepositoryInterface` nutze `Shopware\B2B\Order\Framework\OrderRepository` stattdessen
* Deprecated `Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface` nutze `Shopware\B2B\Order\Framework\OrderContext::isEditable()` oder `Shopware\B2B\Order\Framework\OrderContext::isOrdered()` stattdessen
* Deprecated `Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::updateStatusByName` ohne Ersatz
* Deprecated `Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::updateStatus` ohne Ersatz
* Deprecated `Shopware\B2B\Statistic\Framework\StatisticRepository::createEqualsStatesFilter` nutze `\Shopware\B2B\Statistic\Framework\StatisticRepository::createEqualsStateNameFilter` stattdessen
* Deprecated `\Shopware\B2B\Statistic\Framework\StatisticRepositoryInterface` nutze `\Shopware\B2B\Statistic\Framework\StatisticRepository` stattdessen

## 4.0.2

### Verbesserungen

* Minimum value des Anzahl Input Feld der Produktsuche auf 1 gesetzt wenn keine Mindestbestellmenge gesetzt ist

## 4.0.1

### Verbesserungen

* Debitor Checkbox in die Administration hinzugefügt

## 4.0.0

### Änderungen

* `Shopware\B2B\Common\MvcExtension\Request::requireFileParam()` gibt ein `Symfony\Component\HttpFoundation\File\File` Objekt zurück - ENT-2182
* `Shopware\B2B\Common\MvcExtension\Request::getFiles()` gibt ein array von `Symfony\Component\HttpFoundation\File\File` Objekten zurück - ENT-2182
* `OrderStatusInterpreterServiceInterface::isOpen` `$context` Parameter ist jetzt vom Typ `Shopware\B2B\Order\Framework\OrderContext`

### Veraltet

* `ShopwareCoreConfiguration` Version auf v5.0.0 erhöht

### Entfernungen

* Veraltete Funktion `AclRoutingUpdateService::create` entfernt
* Veraltetes Interface `CartStateSessionValidationInterface` entfernt
* Veralteten Service `AuthenticationServiceDecorator` entfernt
* Veraltete Funktion `MigrationCollectionLoader::create` entfernt
* Veraltete Funktion `MigrationRuntime::create` entfernt
* Veraltete Klasse `EnlightRequest` entfernt
* Veraltete Klasse `RoutingInterceptor` entfernt
* Veraltete Funktion `ValidationException::hasViolationsForFieldWithCause` entfernt
* Veraltete Funktion `ContactIdentity::getLoginContext` entfernt
* Veraltete Eigenschaft `ContactPasswordActivationEntity::type` entfernt
* Veraltetes Interface `ContactPasswordActivationRepositoryInterface` entfernt
* Veraltete Funktion `LineItemReferenceRepository::fetchAllForList` entfernt
* Veraltete Funktion `LineItemReferenceRepository::addVoucherCodeToReferanceById` entfernt
* Veraltete Funktion `LineItemReferenceRepositoryInterface::fetchAllForList` entfernt
* Veraltete Funktion `LineItemReferenceRepositoryInterface::addVoucherCodeToReferanceById` entfernt
* Veraltete Funktion `LineItemReferenceService::mapCustomOrderNumbers` entfernt
* Veraltete Funktion `OfferValidationService::createInsertValidation` entfernt
* Veraltete Funktion `LineItemReferenceRepositoryDecorator::fetchAllForList` entfernt
* Veraltete Funktion `LineItemReferenceRepositoryDecorator::addVoucherCodeToReferanceById` entfernt
* Veraltete Funktion `OrderNumberService::validateOrderNumber` entfernt
* Veraltete Klasse `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeRepository` entfernt
* Veraltete Funktion `StoreFrontAuthenticationRepository::syncAvatarImage` entfernt
* Veraltete Funktion `StoreFrontAuthenticationRepository::fetchAvatarById` entfernt
* Veralteten Parameter `$basketArray` der Funktion `ClearanceBudgetItemLoader::fetchItemsFromBasketArray` entfernt
* Veralteten Rückgabewert von `FastOrderService::produceCart` entfernt
* Veraltete Rückgabewert von `LineItemShopWriterServiceInterface::triggerCart` entfernt
* Veralteten Parameter `$basketArray` von `OrderItemLoaderInterface::fetchItemsFromBasketArray` entfernt
* Veralteten Standardwert `$grantable` von `RoleContactAclAccessWriter::addNewSubject` entfernt
* Veralteten Standardwert `$grantable` von `RoleContingentGroupAclAccessWriter::addNewSubject` entfernt
* Veraltete Funktion `SalesRepresentativeEntity::fromDatabaseArray` entfernt
* Veraltete Funktion `SalesRepresentativeService::isSalesRepresentativeClient` entfernt
* Veraltetes Interface `Shopware\B2B\OrderClearance\Bridge\OrderItemLoaderInterface` entfernt
* Veralteten Service `Shopware\B2B\Order\Bridge\OrderOwnerRepository` entfernt
* Veralteten Service `Shopware\B2B\Order\Bridge\DebtorAssigner` entfernt
* Veraltetes Tag `b2b_sales_representative.client_authentication_repository` entfernt


## 3.0.1

### Verbesserungen

* Backend Zuweisung von Außendienstler Klienten verbessert
* Debitor nach Erstellung im Backend in die Tabelle b2b_store_front_auth hinzugefügt
* Actions Spalte bei mehreren Angebotsstatus aktiviert

### Fehlerbehebung

* Performance Probleme beim Außendienster mit vielen Klienten behoben.
* Import Format von Eigenen Bestellnummern gefixt

### Veraltet

* `Shopware\B2B\LineItemList\Framework\LineItemShopWriterServiceInterface::triggerCart` Rückgabewert wird void sein in Version 4.0.0
* `Shopware\B2B\LineItemList\Bridge\LineItemShopWriterService::triggerCart` Rückgabewert wird void sein in Version 4.0.0
* `Shopware\B2B\FastOrder\Framework\FastOrderService::produceCart` Rückgabewert wird void sein in Version 4.0.0
* `Shopware\B2B\Order\Bridge\OrderOwnerRepository` Klasse wird ins Framework verschoben in Version 4.0.0
* `Shopware\B2B\Order\Framework\OrderOwnerRepositoryInterface` Interface wird in Version 4.0.0 entfernt
* `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity::fromDatabaseArray` wird in Version 4.0.0 entfernt
* `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity::clients` wird in Version 4.0.0 entfernt
* `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeService::isSalesRepresentativeClient` wird in Version 4.0.0 entfernt, nutzt `Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository::isSalesRepresentativeClient` stattdessen
* `Shopware\B2B\Order\Framework\ShopOrderDetailsRepositoryInterface::fetchAllProductsByOrderId` wird ausgetauscht durch `createListByOrderId(int $orderId, OwnershipContext $ownershipContext): LineItemList` in Version 4.0.0
* `Shopware\B2B\Order\Bridge\ShopOrderDetailsRepository::fetchAllProductsByOrderId` wird ausgetauscht durch `createListByOrderId(int $orderId, OwnershipContext $ownershipContext): LineItemList` in Version 4.0.0
* `Shopware\B2B\LineItemList\Framework\LineItemListService::createListByShopOrderDetailsArray` will be moved to `Shopware\B2B\Order\Bridge\ShopOrderDetailsRepository::fetchAllProductsByOrderId` in Version 4.0.0
* `Shopware\B2B\LineItemList\Framework\LineItemReference::fromShopOrderDetails` wird in Version 4.0.0 entfernt
* `Shopware\B2B\Order\Bridge\OrderCheckoutCode` Klasse wird ins Framework verschoben in Version 4.0.0
* `Shopware\B2B\Order\Bridge\DebtorAssigner` Klasse wird in Version 4.0.0 entfernt. Die Funktionalität wird in `Shopware\B2B\Order\Bridge\AuthAssigner` zusammen geführt
* `Shopware\B2B\Order\Bridge\OrderChangeQueueRepository::setRequestUid` wird in Version 4.0.0 entfernt, nutzt `setRequestUuid` stattdessen
* `Shopware\B2B\Order\Bridge\OrderChangeTrigger::setRequestUid` wird in Version 4.0.0 entfernt, nutzt `setRequestUuid` stattdessen
* `Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface::isOpen` Parameter Typ wird zu `Shopware\B2B\Order\Framework\OrderContext` geändert in Version 4.0.0
* `Shopware\B2B\Order\Framework\OrderContext::isEditable` wird in Version 4.0.0 entfernt, nutzt `Shopware\B2B\Order\Framework\OrderStatusInterpreterServiceInterface` stattdessen
* `Shopware\B2B\Budget\Bridge\ClearanceBudgetItemLoader` wird ins Framework verschoben in Version 4.0.0
* `Shopware\B2B\Budget\Bridge\ClearanceBudgetItemLoader::fetchItemsFromBasketArray` $basketArray wird in Version 4.0.0 entfernt
* `Shopware\B2B\OrderClearance\Bridge\OrderItemLoaderInterface` wird ins Framework verschoben in Version 4.0.0
* `Shopware\B2B\OrderClearance\Bridge\OrderItemLoaderInterface::fetchItemsFromBasketArray` $basketArray wird in Version 4.0.0 entfernt
* `Shopware\B2B\OrderClearance\Bridge\OrderClearanceEntityFactory::extendSessionOrderWithItems` $basket wird in Version 4.0.0 entfernt
* Tag `b2b_sales_representative.client_authentication_repository` wird in Version 4.0.0 entfernt, nutzt `b2b_sales_representative.client_authentication_loader` stattdessen
* `Shopware\B2B\Acl\Framework\AclAccessWriter::addNewSubject` $grantable Standardwert wird in Version 4.0.0 entfernt
* `Shopware\B2B\Acl\Framework\AclAccessWriterInterface::addNewSubject` $grantable Standardwert wird in Version 4.0.0 entfernt
* `Shopware\B2B\RoleContact\Framework\RoleContactAclAccessWriter::addNewSubject` $grantable Standardwert wird in Version 4.0.0 entfernt
* `Shopware\B2B\RoleContingentGroup\Framework\RoleContingentGroupAclAccessWriter::addNewSubject` $grantable Standardwert wird in Version 4.0.0 entfernt

## 3.0.0

### Verbesserungen

* Neue Backend Ansicht zur Auswahl von Außendienstler-Kunden - ENT-1673
* Erweiterung des AddressRepositoryInterfaces und dessen Verwendung
* Anpassung der Kontingent- und Budget-Anzeige im Checkout - ENT-1912
* Zusätzliche Filteroptionen - ENT-1914
* Anpassung der Snippets - ENT-1970
* Bestellreferenznummer und Wunschliefertermin in der sOrder template mail - ENT-1945
* Platzhalter für undefinierte Verantwortliche in der Budget-Ansicht hinzugefügt - NTR
* Passwort ändern greift nun auf die im Core hinterlegte minimale Passwortlänge zu - ENT-1959
* Als Debtor das Passwort ändern setzt das gesperrte Kundenkonto zurück - ENT-1869
* Die Kontakt Passwortaktivierung verwendet jetzt eine eigene Tabelle - ENT-1354
* Baumnavigation der Rollenverwaltung markiert neu erstellte Rollen - ENT-2107

### Fehlerbehebung

* Behebung eines Warenkorbfehlers bei einer automatischen Abmeldung im Angebotsprozess - ENT-1770
* Fehler in der Budget-Anzeige behoben - ENT-2090
* Fehlerhaftes Migrationsscript behoben - ENT-1991
* Behebung eines Fehlers, durch das Versenden einer Angebotsbenachrichtigungsmail an den Admin, ohne es konfiguriert zu haben - ENT-2132
* Fehler bei Suchanfragen für Schnellbestellungen behoben - ENT-1947

### neue Funktionen

* Kontingent Regel zum Definieren von kaufbaren Artikeln hinzugefügt - ENT-1329
* Zuordnung von Außendienstmitarbeiter zum Kontext der Bestellung - ENT-1676, ENT-1302
* PHP 7.2 support - ENT-2031
* Shopware 5.6 support - ENT-2031
* `Shopware\B2B\Common\MvcExtension\MvcEnvironmentInterface` für die REST-API hinzugefügt
* Zusätzlicher API-Endpunkt für das Konvertieren von Shop-Bestellungen in das B2B-Umfeld - ENT-1787

### Änderungen

* `Shopware\B2B\Common\RestApi\ApiControllerSubscriber` in `SwagB2bPlugin\Subscriber` verschoben
* `Shopware\B2B\Common\RestApi\SwgaB2BController` in `SwagB2bPlugin\Controllers\Api` verschoben
* Abkapslung des UserLoginContexts aus dem Framework - ENT-2109
* Aufhebung des PHP 7.0-Support - ENT-2129
* Snippet Anpassungen - NTR

### Entfernungen

* Preis Komponente entfernt. Bitte das Plugin SwagEnterprisePricingEngine nutzen.
* Shopware 5.3.x support eingestellt - ENT-2031
* UserLoginContext aus dem Framework ist nun veraltet und wird in der 4.0 entfernt. Bitte benutze ihn aus der Bridge.

### Veraltet

* all usages of `Enlight` and `Shopware()` in Common\
* `Shopware\B2B\Common\Backend\ControllerProxy` in favor of `SwgB2bPlugin\Extension\BackendControllerProxy` for version 4.0.0
* `Shopware\B2B\Common\Frontend\ControllerProxy` in favor of `SwgB2bPlugin\Extension\FrontendControllerProxy` for version 4.0.0
* `Shopware\B2B\Common\MvcExtension\EnlightRequest` in favor of `SwgB2bPlugin\Extension\EnlightRequest` for version 4.0.0
* `Shopware\B2B\Common\MvcExtension\MvcEnvironment` in favor of `SwgB2bPlugin\Extension\MvcEnvironment` for version 4.0.0
* `Shopware\B2B\Common\MvcExtension\RoutingInterceptor` in favor of `SwgB2bPlugin\Extension\RoutingInterceptor` for version 4.0.0
* `Shopware\B2B\Common\Migration\MigrationCollectionLoader::create()` in favor of its constructor for version 4.0.0
* `Shopware\B2B\Common\Migration\MigrationRuntime::create()` in favor of its constructor for version 4.0.0
* `Shopware\B2B\Common\ShopwareCore` without replacement since it was no longer used

## 2.0.6

### Fehlerbehebung

* Fehlerhaften Namespace korrigiert

## 2.0.5

### Fehlerbehebung

* Update der Bibliothek dbal-nested-set
* Migrationsproblem behoben bei nicht deutschen oder englischen Standardsprachen

## 2.0.4

### Fehlerbehebung

* Fix für negative Mehrwertsteuer im Angebotsmodul - ENT-1946

## 2.0.3

### Verbesserungen

* jQuery-Plugins zum StateManager hinzugefügt - ENT-1446
* B2B-Template-Mails im Standart auf Englisch - ENT-1747
* Gelöschte Artikel werden als gelöscht in den Bestellungen, Bestellfreigaben und Bestelllisten angezeigt - ENT-1560
* Rollendetailansicht in "Sichtbarkeit" und "Account" aufgeteilt - ENT-1641
* Aktionsspalte in der Bestelllistenansicht wurde vergrößert - ENT-1785
* Account-Abschnitt in der Rollen-Editierung in "Zuordnungen" umbenannt - ENT-1771
* Newsletter-Abonnementverwaltung hinzugefügt - ENT-1696
* Bestellungen von Debitoren und Außendienstmitarbeitern werden durch Icons hervorgehoben - ENT-1757
* Budget-Nettowerte werden deutlicher dargestellt - ENT-1518
* Validierung von nicht verfügbaren Artikeln in der Bestellfreigabe - ENT-1578
* Spaltennummern beginnen in CSV-, XLS- und XLSX-Dateien mit 0 - ENT-1731
* Bestellungen von Außendienstmitarbeitern werden deutlicher dargestellt - ENT-1302
* Kontakte und Bestellverantwortliche werden in der Bestellübersicht angezeigt - ENT-1733
* Klientenansicht beim Außendienstmitarbeiter enthält das Unternehmen und lässt sich danach filtern - ENT-1218

### Fehlerbehebung

* Fehlerbehebung der mobilen Produktsuche in den Modulen Schnellbestellung und Benutzerdefinierte Artikelnummern - ENT-1685
* Gutscheine werden im Freigabeprozess berücksichtigt - ENT-1663
* Entfernung von Daten des Context-Owners, wenn der Debitor gelöscht wird - ENT-1658
* Unerlaubtes Speichern von Bestelldaten behoben - ENT-1722
* Fataler Fehler beim Klicken auf "Keine Listen verfügbar" (Bestelllisten) behoben - ENT-1767
* Budget-Vorauswahl wird in den Bestelllisten berücksichtigt - ENT-1776
* Ungültige Artikelnummern können nicht mehr in der Kontingentbeschränkung gespeichert werden - ENT-1328
* Fehler beim Speichern von Klientenzuweisungen in der Aussendienstler-Ansicht im Backend behoben - ENT-1735

### neue Funktionen

* Abrufen von Bestellungen über die API - ENT-1695
* Außendienstmitarbeiter-Kunden-API - ENT-1694
* Sichtbarkeit von Bestellungen von Kontakten für Kontakte bei passenden Rechten - ENT-1623
* Fügt einen b2b_store_front_auth Eintrag hinzu, wenn ein Debitor hinzugefügt oder aktualisiert wird - ENT-1703
* Zeigt die Kundennummer im B2B-Account - externer MR von Gitlab
* Spalte mit Budgetverantwortlichen in der Budgetansicht hinzugefügt- ENT-1517
* E-Mail senden, wenn Bestellfreigabe akzeptiert wird - ENT-1553
* Spalte mit Kunden in die Bestellübersicht hinzugefügt - ENT-1720
* Benachrichtigung für ausstehende Bestellfreigaben - ENT-1554
* Letzte Bestellliste als Standart - ENT-896
* Spalte mit der Verfügbarkeit der Artikel in die Detailansicht der Bestellfreigaben hinzugefügt - ENT-1561
* Avatare in der Kontaktübersicht - ENT-1299
* Bestelllisten lassen sich als CSV-Dateien exportieren - ENT-1613
* API-Endpunkte für die Artikelnummer-Komponente - ENT-1774
* `orderReference` zur `requestedDeliveryDate` sORDER Mailvorlage hinzugefügt - ENT-1782
* Prozentuale Rabatte im Angebotsprozess - ENT-1898

### Veraltet

* Preis Komponente ist veraltet. Bitte nutzt das Plugin SwagEnterprisePricingEngine

## 2.0.2

### Verbesserungen

* Auditlog geteilt ENT-1618
* Neugestaltung der Bestelllisten ENT-1622
* Nutzung von Kontakt ID anstatt der E-Mail Adresse in Frontend Controllers
* Ergebnisse der Ajax Produktsuche sind scrollbar ENT-1568
* Scrollbare B2B-Navigation in der Tablet-Ansicht ENT-1671

### Fehlerbehebung

* Korrektur der englischen Snippets ENT-1625
* Fehler beim Hinzufügen von Artikeln in Bestelllisten behoben ENT-1621
* Fehlerhafte Sortierung nach Datum behoben ENT-1569
* Behebung eines fatalen Fehlers beim Hinzufügen einer ungültigen Artikelnummer ENT-1640
* Korrektur von Snippets und Behebung einer fehlerhaften Weiterleitung beim Hinzufügen von Artikeln in den Warenkorb ENT-1636
* Zeigt eine Fehlermeldung an, wenn ein Außendienstler-Kunde kein Passwort hat
* Unendlich drehende Ladeanzeigen wurden behoben ENT-1688
* Rechte werden nicht mehr unerwaretet vergeben ENT-1697
* Zeigt eine Fehlermeldung an, wenn ein Außendienstler-Kunde kein Passwort hat ENT-1675
* Fehlerbehebung der Navigation in Firefox ENT-1690

### neue Funktionen

* Warnung beim Überschreiben von Artikelnummern per Datei-Upload ENT-1620
* Neues Event beim Hinzufügen von Artikeln in der Schnellbestellung
* JavaScript Events für das Modul Schnellbestellungen ENT-1652
* JavaScript Events für das Modul Eigene Bestellnummern ENT-1653
* Kundengruppen spezifische Preise ENT-1552

## 2.0.1

### Fehlerbehebung

* Kompatibilität mit ES ENT-1615
* Eigene Artikelnummern nach Artikelnamen durchsuchbar
* Fehler beim Ändern der Artikelanzahl im Freigabemodul behoben
* Fehler beim Anfragen eines Angebotes im Warenkorb
* Shopware 5.5 Kompatibilität ENT-1624

## 2.0.0

### neue Funktionen

* Hierachien
* Angebotsfunktion
* ProductNameAwareInterface für einfache Übersetzung der Produktnamen
* Debitoren können nun die Kontaktperson für ein Budget sein ENT-1335
* Merkzettelbutton wird bei aktivem b2b Kontext ausgeblendet
* FrontendAccountFirewall $routes Property von private zu protected geändert
* Performance Optimierung im AuthenticationService
* Verwendung von ContextServiceInterface anstatt ContextService

### Fehlerbehebung

* Varianten werden in der Produktsuche angezeigt ENT-1427
* Darstellung aller Bestelllsiten ENT-1527
* AddressRepository wurde ins Bridge Namespace verlegt ENT-1555
* Unterstützung von Pflichtfeldern bei Rechnungs- und Lieferadressen ENT-1313
* BudgetBenachrichtiguns Cronjob ENT-1591
* Budget Checkout Handling
* Weiterer Kontakt hinzufügen
* Instandsetzung Passwortvergessen Funktion
* weitere fixes: ENT-1438, ENT-1549

### Entfernungen

* Shopware\B2B\AclRoute\Frontend\AssignmentController::gridAction()
* Shopware\B2B\Address\Frontend\ContactAddressController::billingAction()
* Shopware\B2B\Address\Frontend\ContactAddressController::shippingAction()
* Shopware\B2B\Budget\Framework\BudgetRepository::fetchBudgetContactList()
* Shopware\B2B\Budget\Frontend\BudgetController::editAction()
* Shopware\B2B\Budget\Frontend\ContactBudgetController::gridAction()
* Shopware\B2B\Contact\Frontend\ContactContactVisibilityController::gridAction()
* Shopware\B2B\Contact\Frontend\ContactController::indexAction()->contactGrid view Variable
* Shopware\B2B\ContingentGroupContact\Frontend\ContactContingentController::gridAction()
* Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeFactory::getAllTypeNames()
* Shopware\B2B\FastOrder\Frontend\FastOrderController::processProductsAction()
* Shopware\B2B\FastOrder\Frontend\FastOrderController::processItemsFromListingAction()
* Shopware\B2B\Order\Bridge\OrderRepository::setRequestedDeliveryDateByOrderContextId()
* Shopware\B2B\Order\Bridge\OrderRepository::setOrderReferenceNumber()
* Shopware\B2B\Order\Bridge\OrderRepository::updateRequestedDeliveryDate()
* Shopware\B2B\Order\Bridge\OrderRepository::setRequestedDeliveryDate()
* Shopware\B2B\Order\Bridge\ShopOrderRepository::setRequestedDeliveryDate()
* Shopware\B2B\Order\Bridge\ShopOrderRepository::updateOrderReferenceNumber()
* Shopware\B2B\Order\Framework\OrderContextRepository::setOrderNumber()
* Shopware\B2B\Order\Framework\OrderRepositoryInterface::setOrderCommentByOrderContextId()
* Shopware\B2B\Order\Framework\OrderRepositoryInterface::setOrderCommentByOrderContextId()
* Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::setRequestedDeliveryDate()
* Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::setOrderCommentByOrderContextId()
* Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface::updateOrderReferenceNumber()
* Shopware\B2B\OrderClearance\Framework\OrderClearanceRepositoryInterface::acceptOrder()
* Shopware\B2B\OrderList\Framework\OrderListService::createLineItemListFromProductsRequest()
* Shopware\B2B\OrderList\Framework\ContactOrderListController::gridAction()
* Shopware\B2B\OrderList\Framework\RoleOrderListController::gridAction()
* Shopware\B2B\RoleContact\Frontend\RoleContactVisibilityController::gridAction()
* Shopware\B2B\StoreFrontAuthentication\Bridge\CredentialsBuilder
* Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface
* Shopware\B2B\StoreFrontAuthentication\Bridge\UserRepository::syncContact()

## 1.5.1 

### neue Funktionen

* Performance optimiert für den AuthenticationService

### Fehlerbehebung

* Changed visibility of the wish list button in the product detail view
* Fixed budget checkout handling
* Fixed "add another contact"
* Changed FrontendAccountFirewall $routes property from private to protected
* Usage of ContextServiceInterface instead of ContextService
* Fixed password reset Funktion

## 1.5.0 

### neue Funktionen

* Profilseite für Kontakte, Debitoren und Ausßendienstmitarbeiter neue Funktionen
* Cronjob und CLI-Befehl für die Bestellsynchronisation implementiert
* HTML minimum Eingabe für Nummerfelder neue Funktionen
* Standard Design verbessert
* Standard Liefer- und Rechnungsadressen neue Funktionen
* API Kontakt erstellen verbessert: context owner id wird anhand der Debitor E-Mail Adresse ausgelesen
* Sortierung von Produkten in Besteslllisten neue Funktionen
* Schnellbestellung-Upload verbessert

### Fehlerbehebung

* Fehlende Hinweise bei Anchor Tags und Icons neue Funktionen
* Budgetauswahl im Checkout gefixt
* Pagination in der Kundenübersicht gefixt
* Textbausteine optimiert
* Handhabung von Bestell-Referenznummer und Wunschlieferdatum Fehlerbehebung
* Handhabung der Preise bei Shops mit Nettoausgabe behohben
* Fehlerausgabe bei Bestellungen ohne Liefermethode Fehlerbehebung

### Entfernungen

* Bestellungsattribut b2b_requested_delivery_date gelöscht
* Bestellungsattribut b2b_order_reference gelöscht
* Bestellungsattribut b2b_clearance_comment gelöscht
* Benutzerattribut b2b_sales_representative_media_id gelöscht

## 1.4.2

### Fehlerbehebung

* Backend Kundenübersicht, Paginierung und Sortierung
* Anlegen von Kunden Debitoren über die API

## 1.4.1

### neue Funktionen

* Verbesserungen der Docker-Umgebung

### Fehlerbehebung

* Berechnung des historischen Warenkorbs korrigiert
* Darstellung der Datumsauswahl angepasst
* Kontingentgruppen Grid Handling korrigiert
* Automatische Rollensichtbarkeit nach dem Erstellen umgesetzt
* Automatische Kontaktsichtbarkeit nach dem Erstellen umgesetzt

### Entfernungen

* Warenkorb wird nicht mehr geleert, wenn man eine Schnellbestellung, Bestelliste in den Warenkorb legt

## 1.4.0

### neue Funktionen

* PHP 7.1 Support
* ACL CSS Klassen neue Funktionen, um Buttons bei nicht vorhandenen Berechtigungen auszublenden
* Automatische Kontaktsichtbarkeit beim Hinzufügen eines Kontaktes
* Automatische Rollensichtbarkeit beim Hinzufügen einer Rolle
* Optmierte Berechtigungsvergabe
  * `b2b_acl_route.route_mapping` - Mapping DI Variable

## Fehlerbehebung

* Falsche Datumsangabe in `components/Statistic/Framework/Statistic` entity Fehlerbehebung
* Freigabe Fehlermeldung Fehlerbehebung
* Rollengrid wird nun korrekt neu gelanden nach Änderungen im Rollenmodal
* Duplizierung der Datumsauswahl in den Statistiken Fehlerbehebung

## entfernte Methoden

* Composer Abhängigkeit `roave/security-advisories` entfernte Methoden

## 1.3.1

### Fehlerbehebung

* Problembehebung einer Migration für Debitoren ohne Außendienstmitarbeiter

## 1.3.0

### neue Funktionen

* Shopware 5.3.3 Kompatibilität neue Funktionen

## 1.2.0

### neue Funktionen

* Shopware 5.2 Kompatibilität
* Unterstützung von mehreren Währungen

## 1.0.0

* Initiales Release
