import DiscountBarPlugin from './script/plugin/discount-bar.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('DiscountBar', DiscountBarPlugin, '[data-discount-bar]');

// Necessary for the webpack hot module reloading server
if (module.hot) {
    module.hot.accept();
}
