import Plugin from 'src/plugin-system/plugin.class';
import CookieStorage from 'src/helper/storage/cookie-storage.helper';

export default class DiscountBarPlugin extends Plugin {

    static options = {
        cookieExpiration: 30,
        cookieName: 'cookie-discount-bar-hide',
        buttonSelector: '.js-discount-bar-cookie-button'
    };


    init() {
        this._cookieButton = this.el.querySelector(this.options.buttonSelector);
        if (!this._cookieButton) {
            return;
        }

        this._cookieButton.addEventListener('click', this._onClick.bind(this));
    }

    _onClick() {
        this._setCookie();
        this._hideDiscountBar();
    }

    _setCookie() {
        CookieStorage.setItem(this.options.cookieName, '1', this.options.cookieExpiration);
    }

    _hideDiscountBar() {
        this.el.style.display = 'none';
    }
}
