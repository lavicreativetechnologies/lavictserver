<?php declare(strict_types=1);

namespace Ies\DiscountBar\Subscriber;

use Shopware\Storefront\Event\StorefrontRenderEvent;
use Shopware\Storefront\Page\Navigation\NavigationPage;
use Shopware\Storefront\Page\Product\ProductPage;
use Shopware\Storefront\Page\Search\SearchPage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class StorefrontSubscriber implements EventSubscriberInterface
{
    public const DISCOUNT_BAR_COOKIE_NAME = 'cookie-discount-bar-hide';

    private const SHOW_DISCOUNT_BAR = 'showDiscountBar';

    private const DISCOUNT_BAR_PAGE_WHITELIST = [
        ProductPage::class,
        NavigationPage::class,
        SearchPage::class,
    ];

    public static function getSubscribedEvents(): array
    {
        return [
            StorefrontRenderEvent::class => 'onStorefrontRender',
        ];
    }

    public function onStorefrontRender(StorefrontRenderEvent $event): void
    {
        if (!$this->inPageWhiteList($event) ||
            $this->isHomePage($event) ||
            $this->hasCookie($event)
        ) {
            return;
        }

        $event->setParameter(self::SHOW_DISCOUNT_BAR, true);
    }

    private function inPageWhiteList(StorefrontRenderEvent $event): bool
    {
        $parameters = $event->getParameters();

        return array_key_exists('page', $parameters) && in_array(get_class($parameters['page']), self::DISCOUNT_BAR_PAGE_WHITELIST);
    }

    private function isHomePage(StorefrontRenderEvent $event): bool
    {
        return $event->getRequest()->attributes->get('_route') === 'frontend.home.page';
    }

    private function hasCookie(StorefrontRenderEvent $event): bool
    {
        return $event->getRequest()->cookies->has(self::DISCOUNT_BAR_COOKIE_NAME);
    }

    private function parameterExisting(StorefrontRenderEvent $event): bool
    {
        $parameters = $event->getParameters();

        return array_key_exists(self::SHOW_DISCOUNT_BAR, $parameters) && $parameters[self::SHOW_DISCOUNT_BAR];
    }
}
