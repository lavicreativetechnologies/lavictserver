<?php declare(strict_types=1);

namespace FeldmannCutOff;

use Shopware\Core\Framework\Plugin;
use FeldmannCutOff\Installer\CustomFieldInstaller;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;

class FeldmannCutOff extends Plugin
{
    public function install(InstallContext $context): void
    {
        (new CustomFieldInstaller($this->container))->install($context);
    }

    public function update(UpdateContext $context): void
    {
        (new CustomFieldInstaller($this->container))->update($context);
    }

    public function activate(ActivateContext $context): void
    {
        (new CustomFieldInstaller($this->container))->activate($context);
    }

    public function deactivate(DeactivateContext $context): void
    {
        (new CustomFieldInstaller($this->container))->deactivate($context);
    }

    public function uninstall(UninstallContext $context): void
    {
        (new CustomFieldInstaller($this->container))->uninstall($context);
    }
}
