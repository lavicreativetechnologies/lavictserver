<?php declare(strict_types=1);

namespace FeldmannCutOff\Storefront\Controller;

use DateTime;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Routing\Annotation\Since;
use Shopware\Core\Framework\Routing\Exception\MissingRequestParameterException;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationList;
use Shopware\Core\System\SystemConfig\SystemConfigService;


/**
 * @RouteScope(scopes={"storefront"})
 */
class CutoffPriceController extends StorefrontController
{
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(
        SystemConfigService $systemConfigService
    )
    {
        $this->systemConfigService = $systemConfigService;
    }

    /**
     * @Since("6.0.0.0")
     * @Route("/feldmann-cutoff/price", name="frontend.feldmann-cutoff.price", methods={"POST"}, defaults={"XmlHttpRequest"=true})
     * @throws \Exception
     */
    public function changeCutPrice(RequestDataBag $requestDataBag, Request $request, SalesChannelContext $context): ?Response
    {
        $length = $request->request->get('length');

        $cutoffPrice = 0.00;
        if($context->getCurrency()->getIsoCode() === 'EUR')
        {
            $cutoffPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceEUR');

        }
        if($context->getCurrency()->getIsoCode() === 'CHF')
        {
            $cutoffPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceCHF');

        }
        if($context->getCurrency()->getIsoCode() === 'USD')
        {
            $cutoffPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceUSD');

        }
        if($context->getCurrency()->getIsoCode() === 'GBP')
        {
            $cutoffPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceGBP');

        }
        $mainPrice = $cutoffPrice * $length;

        return $this->renderStorefront('@Storefront/storefront/component/feldmann-cutoff-price.html.twig', [
            'cutOffPrice' => $mainPrice
        ]);
    }
}
