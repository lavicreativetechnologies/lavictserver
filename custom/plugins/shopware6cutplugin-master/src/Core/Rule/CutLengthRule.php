<?php declare(strict_types=1);

namespace FeldmannCutOff\Core\Rule;

use SebastianBergmann\Comparator\ArrayComparator;
use Shopware\Core\Checkout\Cart\Rule\CartRuleScope;
use Shopware\Core\Checkout\Cart\Rule\LineItemScope;
use Shopware\Core\Framework\Rule\Exception\UnsupportedOperatorException;
use Shopware\Core\Framework\Rule\Rule;
use Shopware\Core\Framework\Rule\RuleScope;
use Shopware\Core\Framework\Util\FloatComparator;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class CutLengthRule extends Rule
{
    public const OPERATOR_NGT = '!>';
    /**
     * @var float
     */
    protected $maxlength;

    /**
     * @var string
     */
    protected $operator;

    public function __construct(string $operator = self:: OPERATOR_GT, ?float $maxlength = null)
    {
        parent::__construct();

        $this->operator = $operator;
        $this->maxlength = (float) $maxlength;
    }

    public function getName(): string
    {
        return 'cut_length_attribute';
    }

    /**
     * @throws UnsupportedOperatorException
     */
    public function match(RuleScope $scope): bool
    {
        if (!$scope instanceof CartRuleScope) {
            return false;
        }

        $cartCutData = $scope->getCart()->getLineItems()->getPayload();

        $output = array_column($cartCutData, 'cutOff');

        $merge_output = array();
        foreach ($output as $v){
            $merge_output = array_merge($merge_output, array_values($v)) ;
        }

        switch ($this->operator) {
            case self::OPERATOR_GTE:
                for ($i=0;$i<count($merge_output);$i++){
                    if ($merge_output[$i] >= $this->maxlength){
                        return true;
                    }
                }
                return false;

            case self::OPERATOR_LTE:
                for ($i=0;$i<count($merge_output);$i++){
                    if ($merge_output[$i] <= $this->maxlength){
                        return true;
                    }
                }
                return false;

            case self::OPERATOR_GT:
                for ($i=0;$i<count($merge_output);$i++){
                    if ($merge_output[$i] > $this->maxlength){
                        return true;
                    }
                }
                return false;

            case self::OPERATOR_LT:
                for ($i=0;$i<count($merge_output);$i++){
                    if ($merge_output[$i] < $this->maxlength){
                        return true;
                    }
                }
                return false;

            case self::OPERATOR_EQ:
                for ($i=0;$i<count($merge_output);$i++){
                    if ($merge_output[$i] == $this->maxlength){
                        return true;
                    }
                }
                return false;

            case self::OPERATOR_NEQ:
                for ($i=0;$i<count($merge_output);$i++){
                    if ($merge_output[$i] != $this->maxlength){
                        return true;
                    }
                }
                return false;

            case self::OPERATOR_NGT:
                $maxVal = max($merge_output);
                if (!($maxVal > $this->maxlength)){
                    return true;
                }
                return false;

            default:
                throw new UnsupportedOperatorException($this->operator, self::class);
        }
    }

    public function getConstraints(): array
    {
        return [
            'maxlength' => [new NotBlank(), new Type('numeric')],
            'operator' => [
                new NotBlank(),
                new Choice(
                    [
                        self::OPERATOR_EQ,
                        self::OPERATOR_LTE,
                        self::OPERATOR_GTE,
                        self::OPERATOR_NEQ,
                        self::OPERATOR_GT,
                        self::OPERATOR_LT,
                        self::OPERATOR_NGT,
                    ]
                ),
            ],
        ];
    }
}
