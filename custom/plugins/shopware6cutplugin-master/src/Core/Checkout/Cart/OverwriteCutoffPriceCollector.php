<?php declare(strict_types=1);

namespace FeldmannCutOff\Core\Checkout\Cart;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartDataCollectorInterface;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\Delivery\DeliveryBuilder;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\AmountCalculator;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Checkout\Cart\Processor;
use Shopware\Core\Checkout\Cart\Transaction\TransactionProcessor;
use Shopware\Core\Checkout\Cart\Validator;
use Shopware\Core\Content\Product\Aggregate\ProductPrice\ProductPriceEntity;
use Shopware\Core\Content\Product\Cart\ProductCartProcessor;
use Shopware\Core\Content\Product\Cart\ProductFeatureBuilder;
use Shopware\Core\Content\Product\Cart\ProductGatewayInterface;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Price\ProductPriceDefinitionBuilderInterface;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\Price;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\PriceRuleEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;

class OverwriteCutoffPriceCollector implements CartDataCollectorInterface
{
    /**
     * @var QuantityPriceCalculator
     */
    private $calculator;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $shippingMethodRepository;

    public function __construct(
        QuantityPriceCalculator $calculator,
        SystemConfigService $systemConfigService,
        SalesChannelRepositoryInterface $productRepository,
        EntityRepositoryInterface $shippingMethodRepository
    ) {
        $this->calculator = $calculator;
        $this->systemConfigService = $systemConfigService;
        $this->productRepository = $productRepository;
        $this->shippingMethodRepository = $shippingMethodRepository;
    }


    public function collect(CartDataCollection $data, Cart $original, SalesChannelContext $context, CartBehavior $behavior): void
    {
        // get all product ids of current cart
        $products = $original->getLineItems()->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE);

        if ($products->count() === 0) {
            return;
        }

        $productGroupEntities = $this->getProductCollection($products, $context);

        foreach ($products as $lineItem) {

            if ($lineItem->hasPayloadValue('cutOff')){

                $priceDefinition = $this->newCutoffPrice($lineItem,$productGroupEntities,$context);
                $lineItem->setPriceDefinition($priceDefinition);
            }

        }

        // shipping code



    }

    private function newCutoffPrice(LineItem $lineItem,ProductCollection $productGroupEntities, SalesChannelContext $context): QuantityPriceDefinition
    {
        $payloadData = $lineItem->getPayloadValue('cutOff');
        $oneCutPrice = $this->getOneCutoffPrice($context);
        $totalCuts = count($payloadData) - 1;

        $productData = $productGroupEntities->get($lineItem->getReferencedId());

        $price = $lineItem->getPriceDefinition()->getPrice();
        $newPrice = $price + $oneCutPrice * $totalCuts;
        $currency = $context->getCurrency();

        return new QuantityPriceDefinition(
            $newPrice,
            $context->buildTaxRules($productData->getTaxId()),
            $currency->getDecimalPrecision(),
            $lineItem->getQuantity(),
            true
        );
    }

    private function getProductCollection(LineItemCollection $products, SalesChannelContext $context): ProductCollection
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('id', $products->getReferenceIds()));

        return $this->productRepository->search($criteria, $context)->getEntities();

    }

    private function getOneCutoffPrice(SalesChannelContext $context)
    {
        $defaultCurrency = $context->getCurrency()->getIsoCode();
        if ($defaultCurrency == 'EUR'){
            $oneCutPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceEUR');
        }
        elseif ($defaultCurrency == 'CHF'){
            $oneCutPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceCHF');
        }
        elseif ($defaultCurrency == 'USD'){
            $oneCutPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceUSD');
        }
        elseif ($defaultCurrency == 'GBP'){
            $oneCutPrice = $this->systemConfigService->get('FeldmannCutOff.config.CutPriceGBP');
        }
        else{
            $oneCutPrice = null;
        }
        return $oneCutPrice;
    }
}
