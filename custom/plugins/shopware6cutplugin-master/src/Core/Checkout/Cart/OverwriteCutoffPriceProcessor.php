<?php declare(strict_types=1);

namespace FeldmannCutOff\Core\Checkout\Cart;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartDataCollectorInterface;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\Delivery\DeliveryBuilder;
use Shopware\Core\Checkout\Cart\Delivery\DeliveryCalculator;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Content\Product\Aggregate\ProductPrice\ProductPriceEntity;
use Shopware\Core\Content\Product\Cart\ProductFeatureBuilder;
use Shopware\Core\Content\Product\Cart\ProductGatewayInterface;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Price\ProductPriceDefinitionBuilderInterface;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\Price;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\PriceRuleEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\Content\Product\Cart\ProductCartProcessor;

class OverwriteCutoffPriceProcessor implements CartProcessorInterface
{
    /**
     * @var QuantityPriceCalculator
     */
    private $calculator;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $shippingMethodRepository;

    /**
     * @var DeliveryCalculator
     */
    protected $deliveryCalculator;

    /**
     * @var DeliveryBuilder
     */
    protected $builder;

    public function __construct(
        QuantityPriceCalculator $calculator,
        SystemConfigService $systemConfigService,
        SalesChannelRepositoryInterface $productRepository,
        EntityRepositoryInterface $shippingMethodRepository,
        DeliveryCalculator $deliveryCalculator,
        DeliveryBuilder $builder
    ) {
        $this->calculator = $calculator;
        $this->systemConfigService = $systemConfigService;
        $this->productRepository = $productRepository;
        $this->shippingMethodRepository = $shippingMethodRepository;
        $this->deliveryCalculator = $deliveryCalculator;
        $this->builder = $builder;
    }

    public function process(CartDataCollection $data, Cart $original, Cart $toCalculate, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $productLineItems = $original->getLineItems()->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE);

        if ($productLineItems->count() === 0) {
            return;
        }

        foreach ($productLineItems as $productLineItem) {
            if ($productLineItem->hasPayloadValue('cutOff')) {

                $priceDefinition = $productLineItem->getPriceDefinition();
                if ($priceDefinition === null) {
                    continue;
                }

                $price = $this->calculator->calculate($priceDefinition, $context);
                $productLineItem->setPrice($price);
            }
        }

//        $deliveries = $this->builder->build($toCalculate, $data, $context, $behavior);
//
//        $delivery = $deliveries->first();
//
//        $maxCutShippingMethod = $this->systemConfigService->get('FeldmannCutOff.config.shippingForHigh');
//        $criteria = new Criteria();
//        $criteria->addFilter(new EqualsFilter('id',$maxCutShippingMethod));
//        $shippingData = $this->shippingMethodRepository->search($criteria,$context->getContext())->first();
//        $key = 'shipping-method-'.$maxCutShippingMethod;
//        $data->set($key, $shippingData);
//
//        $delivery->setShippingMethod($shippingData);
//
//        $this->deliveryCalculator->calculate($data, $toCalculate, $deliveries, $context);
//
//        $toCalculate->setDeliveries($deliveries);
    }
}
