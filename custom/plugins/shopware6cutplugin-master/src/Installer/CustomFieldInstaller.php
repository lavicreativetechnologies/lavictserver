<?php

declare(strict_types=1);

namespace FeldmannCutOff\Installer;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\CustomField\Aggregate\CustomFieldSet\CustomFieldSetEntity;
use Shopware\Core\System\CustomField\CustomFieldEntity;
use Shopware\Core\System\CustomField\Aggregate\CustomFieldSetRelation\CustomFieldSetRelationEntity;

class CustomFieldInstaller implements InstallerInterface
{
    /** @var EntityRepositoryInterface */
    private $customFieldRepository;

    /** @var EntityRepositoryInterface */
    private $customFieldSetRepository;

    /** @var EntityRepositoryInterface */
    private $customFieldSetRelationRepository;

    /** @var array */
    private $customFields;

    /** @var array */
    private $customFieldSets;

    public function __construct(ContainerInterface $container)
    {
        $this->customFieldSetRepository = $container->get('custom_field_set.repository');
        $this->customFieldRepository    = $container->get('custom_field.repository');

        $this->customFieldSetRelationRepository    = $container->get('custom_field_set_relation.repository');

        $this->customFieldSets = [
            [
                'name'   => 'cut_off_configure',
                'config' => [
                    'label' => [
                        'en-GB' => 'Cut off configure',
                        'de-DE' => 'Cut off configure',
                    ],
                ],
                'relation' => [
                    'entityName' => 'product',
                ],
            ],
        ];

        $this->customFields = [
            [
                'name'             => 'cut_active',
                'type'             => CustomFieldTypes::BOOL,
            ],
        ];
    }

    public function install(InstallContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->upsertCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->upsertCustomField($customField, $context->getContext());
        }
    }

    public function update(UpdateContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->upsertCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->upsertCustomField($customField, $context->getContext());
        }
    }

    public function uninstall(UninstallContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->deactivateCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->deactivateCustomField($customField, $context->getContext());
        }
    }

    public function activate(ActivateContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->upsertCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->upsertCustomField($customField, $context->getContext());
        }
    }

    public function deactivate(DeactivateContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->deactivateCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->deactivateCustomField($customField, $context->getContext());
        }
    }

    private function upsertCustomField(array $customField, Context $context): void
    {
        $Field = $this->getCustomField($customField['name'], $context);

        foreach ($this->customFieldSets as $customFieldSet) {
            $FieldSetId = $this->getCustomFieldSet($customFieldSet['name'], $context);
        }
        if(isset($Field)){
            $data = [
                'id'               => $Field->getId(),
                'name'             => $customField['name'],
                'type'             => $customField['type'],
                'active'           => true,
                'customFieldSetId' => $FieldSetId->getId(),
            ];
        }
        else{
            $data = [
                'name'             => $customField['name'],
                'type'             => $customField['type'],
                'active'           => true,
                'customFieldSetId' => $FieldSetId->getId(),
            ];
        }
        $this->customFieldRepository->upsert([$data], $context);
    }

    private function deactivateCustomField(array $customField, Context $context): void
    {
        $Field = $this->getCustomField($customField['name'], $context);
        foreach ($this->customFieldSets as $customFieldSet) {
            $FieldSetId = $this->getCustomFieldSet($customFieldSet['name'], $context);
        }

        if($Field){
            $data = [
                'id'               => $Field->getId(),
                'name'             => $customField['name'],
                'type'             => $customField['type'],
                'active'           => false,
                'customFieldSetId' => $FieldSetId->getId(),
            ];

            $this->customFieldRepository->upsert([$data], $context);
        }
    }

    private function upsertCustomFieldSet(array $customFieldSet, Context $context): void
    {

        $FieldSet = $this->getCustomFieldSet($customFieldSet['name'], $context);

        if(isset($FieldSet)){
            $set = $this->getCustomFieldSetRelation($FieldSet->getId(),$context);
            $data = [
                'id'        => $FieldSet->getId(),
                'name'      => $customFieldSet['name'],
                'config'    => $customFieldSet['config'],
                'active'    => true,
                'relations' => [
                    [
                        'id'         => $set->getId(),
                        'entityName' => $customFieldSet['relation']['entityName'],
                    ],
                ],
            ];
        }
        else{
            $data = [
                'name'      => $customFieldSet['name'],
                'config'    => $customFieldSet['config'],
                'active'    => true,
                'relations' => [
                    [
                        'entityName' => $customFieldSet['relation']['entityName'],
                    ],
                ],
            ];
        }

        $this->customFieldSetRepository->upsert([$data], $context);
    }

    private function deactivateCustomFieldSet(array $customFieldSet, Context $context): void
    {
        $FieldSet = $this->getCustomFieldSet($customFieldSet['name'], $context);
        if($FieldSet){
            $set = $this->getCustomFieldSetRelation($FieldSet->getId(),$context);
            $data = [
                'id'        => $FieldSet->getId(),
                'name'      => $customFieldSet['name'],
                'config'    => $customFieldSet['config'],
                'active'    => false,
                'relations' => [
                    [
                        'id'         => $set->getId(),
                        'entityName' => $customFieldSet['relation']['entityName'],
                    ],
                ],
            ];
            $this->customFieldSetRepository->upsert([$data], $context);
        }
    }

    /**
     * @param string $fieldSetName
     * @param Context $context
     * @return CustomFieldSetEntity|null
     */
    protected function getCustomFieldSet(string $fieldSetName, Context $context)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $fieldSetName));

        return $this->customFieldSetRepository
            ->search($criteria, $context)->first();
    }

    /**
     * @param string $fieldName
     * @param Context $context
     * @return CustomFieldEntity|null
     */
    protected function getCustomField(string $fieldName, Context $context)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $fieldName));

        return $this->customFieldRepository
            ->search($criteria, $context)->first();
    }

    /**
     * @param string $fieldRelationId
     * @param Context $context
     * @return CustomFieldSetRelationEntity|null
     */
    protected function getCustomFieldSetRelation(string $fieldRelationId, Context $context)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('customFieldSetId', $fieldRelationId));

        return $this->customFieldSetRelationRepository
            ->search($criteria, $context)->first();
    }
}
