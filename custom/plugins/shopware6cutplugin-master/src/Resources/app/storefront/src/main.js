import FeldmannPlugin from './feldmann-slider-plugin/feldmann-slider-plugin.plugin';
import FeldmannLengthDataPlugin from './feldmann-slider-plugin/feldmann-length-data.plugin';
import FeldmannSliderColorPlugin from './feldmann-slider-plugin/slider-color-change.plugin';
import FeldmannSliderKeyboard from './feldmann-slider-plugin/slider-keyboard-values.plugin';
import FeldmannCutOffButton from './feldmann-slider-plugin/feldmann-cutOff-price.plugin';

const PluginManager = window.PluginManager;
PluginManager.register('FeldmannPlugin', FeldmannPlugin,'[data-cutoff-rangSlider]');
PluginManager.register('FeldmannLengthDataPlugin', FeldmannLengthDataPlugin,'[data-cutoff-length]');
PluginManager.register('FeldmannSliderColorPlugin', FeldmannSliderColorPlugin,'[data-color-change]');
PluginManager.register('FeldmannSliderKeyboard', FeldmannSliderKeyboard,'[data-color-change]');
PluginManager.register('FeldmannCutOffButton', FeldmannCutOffButton,'[data-feldmann-button]');
