import Plugin from 'src/plugin-system/plugin.class';
import HttpClient from 'src/service/http-client.service';
import DomAccess from 'src/helper/dom-access.helper';

export default class FeldmannCutOffPricePlugin extends Plugin {

    init() {
        this.client = new HttpClient();
        this.notificationDisplayHolder = DomAccess.querySelector(document, '.product-detail-cutoff-price-container');

        this._registerEvents();
    }

    _registerEvents() {
        this.el.addEventListener('click', this._formSubmit.bind(this));
    }
    /**
     * On submitting the form the OffCanvas shall open, the product has to be posted
     * against the storefront api and after that the current cart template needs to
     * be fetched and shown inside the OffCanvas
     * @param {Event} event
     * @private
     */
    _formSubmit(event) {
        const requestUrl = document.querySelector(".feldmann-cut-url").getAttribute('data-url');
        const requestToken = document.querySelector(".feldmann-cut-url").getAttribute('data-token');
        const rangeSliderLength = document.querySelectorAll(".noUi-handle:not(.noUi-handle-lower):not(.noUi-handle-upper)").length;
        const formData = new FormData(); // Currently empty
        formData.append('length', rangeSliderLength);
        formData.append('_csrf_token', requestToken);


        this.client.post(requestUrl, formData, this.onTemplateReceived.bind(this));

    }
    /**
     * After getting the price information from the server, this callback renders the returned html template to
     * the holder element.
     *
     * @param {String} data
     * @returns {void}
     */
    onTemplateReceived(data) {

        this.notificationDisplayHolder.innerHTML = data;


    }
}
