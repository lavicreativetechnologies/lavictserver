import Plugin from 'src/plugin-system/plugin.class';

export default class FeldmannSliderKeyboard extends Plugin {

    init() {
        this._registerEvents();
    }
    _registerEvents() {
        let handle = '';
        let inputNumber = document.getElementById('input-number');
        const sliderElement = document.getElementById('range');
        let maxLength = jQuery.parseJSON(document.querySelector('#maxLength').getAttribute('data-feldmann-max-length'));

        document.addEventListener('dblclick', function (e) {
            if (e.target && e.target.className === 'noUi-tooltip') {
                inputNumber.removeAttribute("disabled");
                inputNumber.focus();
                let value = e.target.parentElement.getAttribute('aria-valuetext');
                handle = e.target.parentElement.getAttribute('data-handle');
                inputNumber.value = value;
            }
        });

        inputNumber.addEventListener('change', function () {
            let numMax = document.querySelector('#input-number');

            if (numMax.value > parseInt(maxLength)){
                numMax.value = parseInt(maxLength)-1;
            }
            const rangeSlider = document.querySelectorAll('.noUi-base .noUi-origin');
            const myArray = [];
            for(let i = 0; i < rangeSlider.length; ++i){
                if(i == handle)
                {
                    myArray.push(parseInt(this.value));
                }
                else {
                    myArray.push(null);
                }
            }
            sliderElement.noUiSlider.set(myArray);
        });
    }
}
