import Plugin from 'src/plugin-system/plugin.class';

export default class FeldmannSliderColorPlugin extends Plugin {
    static options = {
        move : false
    };

    init() {
        this._registerEvents();
    }
    _registerEvents() {
        // color change
        document.addEventListener('mouseover', function (e) {
            if (e.target && e.target.className === 'noUi-tooltip') {
                e.target.style.backgroundImage = "url('/bundles/feldmanncutoff/assets/images/green_balloon.svg')";
            }
        });
        //color get back on mouse out
        document.addEventListener('mouseout', function (e) {
            if (e.target && e.target.className === 'noUi-tooltip') {
                e.target.style.backgroundImage = "url('/bundles/feldmanncutoff/assets/images/blue_balloon.svg')";
            }
        });
    }


}
