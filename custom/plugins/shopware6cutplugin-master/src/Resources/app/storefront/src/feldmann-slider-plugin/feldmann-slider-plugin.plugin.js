import Plugin from 'src/plugin-system/plugin.class';
export default class FeldmannPlugin extends Plugin {
    init() {
        var maxLength = jQuery.parseJSON(document.querySelector('#maxLength').getAttribute('data-feldmann-max-length'));
        var convertedMaxLength = parseInt(maxLength);
        let maxCut = document.querySelector('#maxCuts').getAttribute('data-feldmann-max-cuts');
        const sliderElement = document.getElementById('range');
        const resultElement = document.getElementById('result');
        const addBtn = document.getElementById('addBtn');
        const removeBtn = document.getElementById('removeBtn');

        document.querySelector('#input-number').setAttribute('max',convertedMaxLength-1);

        const defaultOptions = {
            connect: true,
            step:1,
            keyboardSupport: true,      // Default true
            keyboardDefaultStep: 5,     // Default 10
            keyboardPageMultiplier: 100,
            keyboardMultiplier:50,
            range: {
                'min': 0,
                'max': convertedMaxLength
            }
        }

        let partsCount = 1;
        let slider = null;
        let oldArr = Array();

        // for add Button
        function createSlider () {
            const parts = getEqualParts(partsCount);

            if (slider) {
                slider.destroy()
            }

            slider = noUiSlider.create(
                sliderElement,
                {...defaultOptions, ...parts}
            );

        }

        function getEqualParts (number) {
            let start = [];
            let i = 0;
            let fv = 0;
            let cutx = 0;
            let divsor = 2;

            oldArr = [];

            document.querySelectorAll(".noUi-handle:not(.noUi-handle-lower)").forEach(
                el => oldArr.push(parseInt(el.getAttribute('aria-valuetext')))
            );

            if (oldArr.length > 0){
                if (oldArr[0] == convertedMaxLength){
                    for (i; i <= convertedMaxLength; i += convertedMaxLength/number) {
                        start.push(i);
                    }
                }
                // divide by 2 if already one position is there
                else{
                    start.push(0);
                    for (let x = 0; x < oldArr.length - 2; x++){
                        start.push(oldArr[x]);
                    }
                    fv = parseInt(oldArr[oldArr.length - 2]);
                    cutx = convertedMaxLength-fv;
                    for (i=fv; i <= convertedMaxLength; i += cutx/divsor){
                        start.push(i);
                    }
                }
            }
            else{
                for (i; i <= convertedMaxLength; i += convertedMaxLength/number) {
                    start.push(i);
                }
            }

            return {
                start: start,
                tooltips: start.map(() => true),
                format: wNumb({
                    decimals: 0, // default is 2
                })
            }
        }

        //for remove Button
        function createSliderRemove () {
            const parts = getEqualPartsRemove(partsCount);

            if (slider) {
                slider.destroy()
            }

            slider = noUiSlider.create(
                sliderElement,
                {...defaultOptions, ...parts}
            );
        }

        function getEqualPartsRemove (number) {
            let start = [];

            oldArr = [];
            oldArr.push(0);
            document.querySelectorAll(".noUi-handle:not(.noUi-handle-lower)").forEach(
                el => oldArr.push(parseInt(el.getAttribute('aria-valuetext')))
            );

            oldArr.splice(number, 1);
            start = oldArr;

            return {
                start: start,
                tooltips: start.map(() => true),
                format: wNumb({
                    decimals: 0, // default is 2
                })
            }
        }

        function addPart() {
            if (partsCount > maxCut){
                return;
            }
            partsCount++;
            createSlider();
        }
        function removePart() {
            if(partsCount <= 1){
                return;
            }
            partsCount--;
            createSliderRemove();
        }

        addBtn.addEventListener('click', addPart, false)
        removeBtn.addEventListener('click', removePart, false)

        createSlider();
    }
}
