import Plugin from 'src/plugin-system/plugin.class';

export default class FeldmannLengthDataPlugin extends Plugin {
    init() {
        this._registerEvents();
    }

    _registerEvents() {
        let arr = Array();

        this.el.addEventListener('click', async () => {
            const lengthItems = document.querySelectorAll(".noUi-handle:not(.noUi-handle-lower)");
            arr = [];
            await lengthItems.forEach(el => {
                arr.push(el.getAttribute('aria-valuetext'));
            });

            document.querySelector("#lengthParams").setAttribute('value',arr);
        });
    }
}
