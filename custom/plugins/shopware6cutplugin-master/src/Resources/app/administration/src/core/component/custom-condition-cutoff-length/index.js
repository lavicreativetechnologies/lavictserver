import template from './custom-condition-cutoff-length.html.twig';

Shopware.Component.extend('custom-condition-cutoff-length', 'sw-condition-base', {
    template,

    computed: {
        operators() {
            const operators = this.conditionDataProviderService.getOperatorSet('number');
            operators.push({identifier: "!>",label: "global.sw-condition.notGreater"});

            return operators;
        },

        maxlength: {
            get() {
                this.ensureValueExist();
                return this.condition.value.maxlength;
            },
            set(maxlength) {
                this.ensureValueExist();
                this.condition.value = { ...this.condition.value, maxlength };
            }
        }
    }
});
