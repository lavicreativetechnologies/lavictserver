import '../core/component/custom-condition-cutoff-length';
Shopware.Application.addServiceProviderDecorator('ruleConditionDataProviderService', (ruleConditionService) => {
    ruleConditionService.addCondition('cut_length_attribute', {
        component: 'custom-condition-cutoff-length',
        label: 'Cutoff length in cart',
        scopes: ['global']
    });
    return ruleConditionService;
});
