<?php declare(strict_types=1);

namespace FeldmannCutOff\Service;

use FeldmannCutOff\Service\CutoffPriceHandler;
use phpDocumentor\Reflection\Types\Integer;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItemFactoryRegistry;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Shopware\Storefront\Framework\Routing\StorefrontResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Checkout\Cart\Cart;
use function React\Promise\all;

/**
 * @RouteScope(scopes={"storefront"})
 */
class CutoffPriceController extends StorefrontController
{
    /**
     * @var CutoffPriceHandler
     */
    private $factory;

    /**
     * @var CartService
     */
    private $cartService;

    public function __construct(CutoffPriceHandler $factory, CartService $cartService)
    {
        $this->factory = $factory;
        $this->cartService = $cartService;
    }

    /**
     * @Route("/cutoffAdd", name="frontend.cutoff", methods={"POST"},defaults={"XmlHttpRequest"=true})
     */
    public function add(Cart $cart, SalesChannelContext $context,RequestDataBag $requestDataBag, Request $request): Response
    {
        $lengthItems = $request->request->get('lengthParams');
        $lengthItemArray = array();

        $arr = explode(",",$lengthItems);
        $res = array();
        $x = ((int) $arr[0]);
        
        array_push($res, $x);

        for ($i=0; $i < count($arr) - 1; $i++) {
            $values = $arr[$i+1] - $arr[$i];
            array_push($res, $values);
        }
        $lengthItemArray['cutOff'] = $res;

        $lineItems = $request->request->get('lineItems', []);
        /** @var array|false $fetchLineItems */
        $fetchLineItems = reset($lineItems);
        $referenceId = $fetchLineItems['referencedId'];
        $quantity = $fetchLineItems['quantity'];

        // Create product line item
        $lineItem = $this->factory->create([
            'id' => Uuid::randomHex(),
            'type' => LineItem::PRODUCT_LINE_ITEM_TYPE, // Results in 'product'
            'referencedId' => $referenceId, // this is not a valid UUID, change this to your actual ID!
            'quantity' => $quantity
        ], $context);

        $lineItem->setRemovable(true);
        $lineItem->setStackable(true);
        $lineItem->setPayload($lengthItemArray);

        $this->cartService->add($cart, $lineItem, $context);

        return $this->finishAction($cart, $request);
    }
    private function finishAction(Cart $cart, Request $request): \Symfony\Component\HttpFoundation\Response
    {
        return $this->createActionResponse($request);
    }
}
