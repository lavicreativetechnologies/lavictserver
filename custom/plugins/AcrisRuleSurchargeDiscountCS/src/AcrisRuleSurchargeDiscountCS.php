<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;

class AcrisRuleSurchargeDiscountCS extends Plugin
{
    public function uninstall(UninstallContext $context): void
    {
        if ($context->keepUserData()) {
            return;
        }
        $this->cleanupDatabase();
    }

    private function cleanupDatabase(): void
    {
        $connection = $this->container->get(Connection::class);
        $connection->executeQuery('DROP TABLE IF EXISTS acris_rule_s_d_sales_channel');
        $connection->executeQuery('DROP TABLE IF EXISTS acris_rule_s_d_rules');
        $connection->executeQuery('DROP TABLE IF EXISTS acris_rule_s_d_translation');
        $connection->executeQuery('DROP TABLE IF EXISTS acris_rule_s_d');
    }
}
