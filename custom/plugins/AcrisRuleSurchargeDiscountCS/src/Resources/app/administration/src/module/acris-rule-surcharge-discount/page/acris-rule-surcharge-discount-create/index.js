const { Component } = Shopware;
const utils = Shopware.Utils;

import template from './acris-rule-surcharge-discount-create.html.twig';

Component.extend('acris-rule-surcharge-discount-create', 'acris-rule-surcharge-discount-detail', {
    template,

    beforeRouteEnter(to, from, next) {
        if (to.name.includes('acris.rule.surcharge.discount.create') && !to.params.id) {
            to.params.id = utils.createId();
            to.params.newItem = true;
        }

        next();
    },

    methods: {
        getEntity() {
            this.item = this.repository.create(Shopware.Context.api);
        },

        createdComponent() {
            if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                Shopware.State.commit('context/resetLanguageToDefault');
            }

            this.$super('createdComponent');
        },

        saveFinish() {
            this.isSaveSuccessful = false;
            this.$router.push({ name: 'acris.rule.surcharge.discount.detail', params: { id: this.item.id } });
        },

        onClickSave() {
            this.isLoading = true;
            const titleSaveError = this.$tc('acris-rule-surcharge-discount.detail.titleNotificationError');
            const messageSaveError = this.$tc('acris-rule-surcharge-discount.detail.messageSaveError');
            const titleSaveSuccess = this.$tc('acris-rule-surcharge-discount.detail.titleNotificationSuccess');
            const messageSaveSuccess = this.$tc('acris-rule-surcharge-discount.detail.messageSaveSuccess');

            this.repository
                .save(this.item, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    this.createNotificationSuccess({
                        title: titleSaveSuccess,
                        message: messageSaveSuccess
                    });
                    this.$router.push({ name: 'acris.rule.surcharge.discount.detail', params: { id: this.item.id } });
                }).catch(() => {
                this.isLoading = false;
                this.createNotificationError({
                    title: titleSaveError,
                    message: messageSaveError
                });
            });
        }
    }
});
