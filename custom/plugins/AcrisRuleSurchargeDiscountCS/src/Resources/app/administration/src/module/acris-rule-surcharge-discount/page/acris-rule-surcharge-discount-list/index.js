import template from './acris-rule-surcharge-discount-list.html.twig';

const {Component, Mixin} = Shopware;
const { Criteria } = Shopware.Data;

Component.register('acris-rule-surcharge-discount-list', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('listing'),
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    data() {
        return {
            items: null,
            isLoading: false,
            showDeleteModal: false,
            repository: null,
            total: 0
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        entityRepository() {
            return this.repositoryFactory.create('acris_rule_s_d');
        },

        columns() {
            return this.getColumns();
        }
    },

    methods: {
        getList() {
            this.isLoading = true;
            const criteria = new Criteria(this.page, this.limit);
            criteria.setTerm(this.term);

            this.entityRepository.search(criteria, Shopware.Context.api).then((items) => {
                this.total = items.total;
                this.items = items;
                this.isLoading = false;

                return items;
            }).catch(() => {
                this.isLoading = false;
            });
        },

        onDelete(id) {
            this.showDeleteModal = id;
        },

        onCloseDeleteModal() {
            this.showDeleteModal = false;
        },

        getColumns() {
            return [{
                property: 'internalId',
                inlineEdit: 'string',
                label: 'acris-rule-surcharge-discount.list.columnInternalId',
                routerLink: 'acris.rule.surcharge.discount.detail',
                allowResize: true,
                primary: true
            }, {
                property: 'internalName',
                inlineEdit: 'string',
                label: 'acris-rule-surcharge-discount.list.columnInternalName',
                routerLink: 'acris.rule.surcharge.discount.detail',
                allowResize: true,
                primary: true
            }, {
                property: 'displayName',
                inlineEdit: 'string',
                label: 'acris-rule-surcharge-discount.list.columnDisplayName',
                routerLink: 'acris.rule.surcharge.discount.detail',
                allowResize: true
            }, {
                property: 'active',
                label: 'acris-rule-surcharge-discount.list.columnActive',
                inlineEdit: 'boolean',
                allowResize: true,
                align: 'center'
            }, {
                property: 'type',
                inlineEdit: 'string',
                label: 'acris-rule-surcharge-discount.list.columnType',
                routerLink: 'acris.rule.surcharge.discount.detail',
                allowResize: true
            }, {
                property: 'value',
                inlineEdit: 'string',
                label: 'acris-rule-surcharge-discount.list.columnValue',
                routerLink: 'acris.rule.surcharge.discount.detail',
                allowResize: true
            }];
        },

        onChangeLanguage(languageId) {
            this.getList(languageId);
        },
    }
});

