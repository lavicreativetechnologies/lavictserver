import './page/acris-rule-surcharge-discount-list';
import './page/acris-rule-surcharge-discount-create';
import './page/acris-rule-surcharge-discount-detail';
import './acris-settings-item.scss';
import deDE from "./snippet/de-DE";
import enGB from "./snippet/en-GB";

const { Module } = Shopware;

Module.register('acris-rule-surcharge-discount', {
    type: 'plugin',
    name: 'acris-rule-surcharge-discount',
    title: 'acris-rule-surcharge-discount.general.mainMenuItemGeneral',
    description: 'acris-rule-surcharge-discount.general.description',
    color: '#9AA8B5',
    icon: 'default-symbol-rule',
    favicon: 'icon-module-settings.png',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        index: {
            component: 'acris-rule-surcharge-discount-list',
            path: 'index',
            meta: {
                parentPath: 'sw.settings.index'
            }
        },
        detail: {
            component: 'acris-rule-surcharge-discount-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'acris.rule.surcharge.discount.index'
            }
        },
        create: {
            component: 'acris-rule-surcharge-discount-create',
            path: 'create',
            meta: {
                parentPath: 'acris.rule.surcharge.discount.index'
            }
        }
    },

    settingsItem: [
        {
            name:   'acris-rule-surcharge-discount',
            to:     'acris.rule.surcharge.discount.index',
            label:  'acris-rule-surcharge-discount.general.mainMenuItemGeneral',
            group:  'plugins',
            icon:   'default-symbol-rule'
        }
    ]
});
