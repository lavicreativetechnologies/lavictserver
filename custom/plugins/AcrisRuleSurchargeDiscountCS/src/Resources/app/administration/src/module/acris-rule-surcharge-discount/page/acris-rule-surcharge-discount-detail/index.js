const { Component } = Shopware;
const { Criteria } = Shopware.Data;
const { Mixin } = Shopware;
import DiscountHandler from './handler';

const discountHandler = new DiscountHandler();

import template from './acris-rule-surcharge-discount-detail.html.twig';

Component.register('acris-rule-surcharge-discount-detail', {
    template,

    inject: ['repositoryFactory', 'context'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    data() {
        return {
            item: null,
            isLoading: false,
            processSuccess: false,
            repository: null,
            isSaveSuccessful: false,
            currencies: [],
            defaultCurrency: null,
            currencySymbol: null
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    created() {
        this.createdComponent();
    },

    computed: {
        valueSuffix() {
            return discountHandler.getValueSuffix(this.item.type, this.currencySymbol);
        },

        currencyRepository() {
            return this.repositoryFactory.create('currency');
        },
    },

    methods: {
        createdComponent(){
            this.repository = this.repositoryFactory.create('acris_rule_s_d');
            this.getEntity();

            this.currencyRepository.search(new Criteria(), Shopware.Context.api).then((response) => {
                this.currencies = response;
                this.defaultCurrency = this.currencies.find(currency => currency.isSystemDefault);
                this.currencySymbol = this.defaultCurrency.symbol;
            });
        },

        getEntity() {
            const criteria = new Criteria();
            criteria.addAssociation('salesChannels');
            criteria.addAssociation('rules');

            this.repository
                .get(this.$route.params.id, Shopware.Context.api, criteria)
                .then((entity) => {
                    this.item = entity;
                });
        },

        onClickSave() {
            this.isLoading = true;
            const titleSaveError = this.$tc('acris-rule-surcharge-discount.detail.titleNotificationError');
            const messageSaveError = this.$tc(
                'acris-rule-surcharge-discount.detail.messageSaveError'
            );
            const titleSaveSuccess = this.$tc('acris-rule-surcharge-discount.detail.titleNotificationSuccess');
            const messageSaveSuccess = this.$tc(
                'acris-rule-surcharge-discount.detail.messageSaveSuccess'
            );

            this.isSaveSuccessful = false;
            this.isLoading = true;

            this.repository
                .save(this.item, Shopware.Context.api)
                .then(() => {
                    this.getEntity();
                    this.isLoading = false;
                    this.processSuccess = true;
                    this.createNotificationSuccess({
                        title: titleSaveSuccess,
                        message: messageSaveSuccess
                    });
                }).catch(() => {
                this.isLoading = false;
                this.createNotificationError({
                    title: titleSaveError,
                    message: messageSaveError
                });
            });
        },

        saveFinish() {
            this.processSuccess = false;
        },

        onChangeLanguage() {
            this.getEntity();
        },
    }
});
