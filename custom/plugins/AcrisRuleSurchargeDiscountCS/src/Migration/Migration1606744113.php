<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1606744113 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1606744113;
    }

    public function update(Connection $connection): void
    {
        $query = <<<SQL
            CREATE TABLE `acris_rule_s_d_translation` (
                `display_name` VARCHAR(255) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                `acris_rule_s_d_id` BINARY(16) NOT NULL,
                `language_id` BINARY(16) NOT NULL,
                PRIMARY KEY (`acris_rule_s_d_id`,`language_id`),
                KEY `fk.acris_rule_s_d_translation.acris_rule_s_d_id` (`acris_rule_s_d_id`),
                KEY `fk.acris_rule_s_d_translation.language_id` (`language_id`),
                CONSTRAINT `fk.acris_rule_s_d_translation.acris_rule_s_d_id` FOREIGN KEY (`acris_rule_s_d_id`) REFERENCES `acris_rule_s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.acris_rule_s_d_translation.language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;

        $connection->executeQuery($query);

        $this->addColumnToTable($connection, 'acris_rule_s_d', 'internal_id');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    protected function addColumnToTable(Connection $connection, string $entity, string $propertyName): void
    {
        $sql = str_replace(
            ['#table#', '#column#'],
            [$entity, $propertyName],
            "ALTER TABLE `#table#` ADD COLUMN `#column#` VARCHAR(255) NULL"
        );

        $connection->executeUpdate($sql);
    }
}
