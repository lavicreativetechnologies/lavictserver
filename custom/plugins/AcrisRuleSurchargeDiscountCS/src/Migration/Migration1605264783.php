<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1605264783 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1605264783;
    }

    public function update(Connection $connection): void
    {
        $query = <<<SQL
            CREATE TABLE `acris_rule_s_d` (
                `id` BINARY(16) NOT NULL,
                `internal_name` VARCHAR(255) NOT NULL,
                `display_name` VARCHAR(255) NOT NULL,
                `active` TINYINT(1) NULL DEFAULT '0',
                `type` VARCHAR(255) NOT NULL,
                `value` VARCHAR(255) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;

        $connection->executeQuery($query);

        $query = <<<SQL
            CREATE TABLE `acris_rule_s_d_sales_channel` (
                `surcharge_discount_id` BINARY(16) NOT NULL,
                `sales_channel_id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                PRIMARY KEY (`surcharge_discount_id`,`sales_channel_id`),
                KEY `fk.acris_rule_s_d_sales_channel.surcharge_discount_id` (`surcharge_discount_id`),
                KEY `fk.acris_rule_s_d_sales_channel.sales_channel_id` (`sales_channel_id`),
                CONSTRAINT `fk.acris_rule_s_d_sales_channel.surcharge_discount_id` FOREIGN KEY (`surcharge_discount_id`) REFERENCES `acris_rule_s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.acris_rule_s_d_sales_channel.sales_channel_id` FOREIGN KEY (`sales_channel_id`) REFERENCES `sales_channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;

        $connection->executeQuery($query);

        $query = <<<SQL
            CREATE TABLE `acris_rule_s_d_rules` (
                `surcharge_discount_id` BINARY(16) NOT NULL,
                `rule_id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                PRIMARY KEY (`surcharge_discount_id`,`rule_id`),
                KEY `fk.acris_rule_s_d_rules.surcharge_discount_id` (`surcharge_discount_id`),
                KEY `fk.acris_rule_s_d_rules.rule_id` (`rule_id`),
                CONSTRAINT `fk.acris_rule_s_d_rules.surcharge_discount_id` FOREIGN KEY (`surcharge_discount_id`) REFERENCES `acris_rule_s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.acris_rule_s_d_rules.rule_id` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;

        $connection->executeQuery($query);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
