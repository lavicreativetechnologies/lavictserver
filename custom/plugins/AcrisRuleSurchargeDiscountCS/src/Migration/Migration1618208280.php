<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1618208280 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1618208280;
    }

    public function update(Connection $connection): void
    {
        $query = <<<SQL
            ALTER TABLE `acris_rule_s_d`
                DROP COLUMN `display_name`;
SQL;

        $connection->executeQuery($query);

    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
