<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Custom;

use Acris\RuleSurchargeDiscount\Custom\Aggregate\SurchargeDiscountRulesDefinition;
use Acris\RuleSurchargeDiscount\Custom\Aggregate\SurchargeDiscountSalesChannelDefinition;
use Acris\RuleSurchargeDiscount\Custom\Aggregate\SurchargeDiscountTranslationDefinition;
use Shopware\Core\Content\Rule\RuleDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\SearchRanking;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FloatField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\SalesChannel\SalesChannelDefinition;

class SurchargeDiscountDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'acris_rule_s_d';

    public const TYPE_PERCENTAGE = 'percentage';

    public const TYPE_ABSOLUTE = 'absolute';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return SurchargeDiscountCollection::class;
    }

    public function getEntityClass(): string
    {
        return SurchargeDiscountEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey(), new Required()),
            (new StringField('internal_name','internalName'))->addFlags(new Required(), new SearchRanking(SearchRanking::MIDDLE_SEARCH_RANKING)),
            (new StringField('internal_id','internalId'))->addFlags(new SearchRanking(SearchRanking::MIDDLE_SEARCH_RANKING)),
            (new TranslatedField('displayName')),
            new BoolField('active', 'active'),
            (new StringField('type','type'))->addFlags(new Required(), new SearchRanking(SearchRanking::MIDDLE_SEARCH_RANKING)),
            (new FloatField('value','value'))->addFlags(new Required(), new SearchRanking(SearchRanking::MIDDLE_SEARCH_RANKING)),

            new TranslationsAssociationField(SurchargeDiscountTranslationDefinition::class, 'acris_rule_s_d_id'),
            new ManyToManyAssociationField('salesChannels', SalesChannelDefinition::class, SurchargeDiscountSalesChannelDefinition::class, 'surcharge_discount_id', 'sales_channel_id'),
            new ManyToManyAssociationField('rules', RuleDefinition::class, SurchargeDiscountRulesDefinition::class, 'surcharge_discount_id', 'rule_id')
        ]);
    }
}
