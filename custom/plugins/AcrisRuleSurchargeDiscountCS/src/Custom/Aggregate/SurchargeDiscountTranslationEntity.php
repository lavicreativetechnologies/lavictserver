<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Custom\Aggregate;

use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountEntity;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use Shopware\Core\System\Language\LanguageEntity;

class SurchargeDiscountTranslationEntity extends TranslationEntity
{
    /**
     * @var string
     */
    protected $displayName;

    /**
     * @var string
     */
    protected $surchargeDiscountId;

    /**
     * @var SurchargeDiscountEntity
     */
    protected $surchargeDiscount;

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName(string $displayName): void
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getSurchargeDiscountId(): string
    {
        return $this->surchargeDiscountId;
    }

    /**
     * @param string $surchargeDiscountId
     */
    public function setSurchargeDiscountId(string $surchargeDiscountId): void
    {
        $this->surchargeDiscountId = $surchargeDiscountId;
    }

    /**
     * @return SurchargeDiscountEntity
     */
    public function getSurchargeDiscount(): SurchargeDiscountEntity
    {
        return $this->surchargeDiscount;
    }

    /**
     * @param SurchargeDiscountEntity $surchargeDiscount
     */
    public function setSurchargeDiscount(SurchargeDiscountEntity $surchargeDiscount): void
    {
        $this->surchargeDiscount = $surchargeDiscount;
    }

    /**
     * @return string
     */
    public function getLanguageId(): string
    {
        return $this->languageId;
    }

    /**
     * @param string $languageId
     */
    public function setLanguageId(string $languageId): void
    {
        $this->languageId = $languageId;
    }

    /**
     * @return LanguageEntity|null
     */
    public function getLanguage(): ?LanguageEntity
    {
        return $this->language;
    }

    /**
     * @param LanguageEntity|null $language
     */
    public function setLanguage(?LanguageEntity $language): void
    {
        $this->language = $language;
    }
}
