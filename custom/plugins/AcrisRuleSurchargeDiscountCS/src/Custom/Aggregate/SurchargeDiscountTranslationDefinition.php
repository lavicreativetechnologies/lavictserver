<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Custom\Aggregate;

use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\SearchRanking;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;


class SurchargeDiscountTranslationDefinition extends EntityTranslationDefinition
{
    public function getEntityName(): string
    {
        return 'acris_rule_s_d_translation';
    }

    public function getCollectionClass(): string
    {
        return SurchargeDiscountTranslationCollection::class;
    }

    public function getEntityClass(): string
    {
        return SurchargeDiscountTranslationEntity::class;
    }

    public function getParentDefinitionClass(): string
    {
        return SurchargeDiscountDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('display_name','displayName'))->addFlags(new Required(), new SearchRanking(SearchRanking::MIDDLE_SEARCH_RANKING))
        ]);
    }
}
