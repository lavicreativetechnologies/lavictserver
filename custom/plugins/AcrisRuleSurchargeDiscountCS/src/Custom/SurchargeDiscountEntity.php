<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Custom;

use Acris\RuleSurchargeDiscount\Custom\Aggregate\SurchargeDiscountTranslationCollection;
use Shopware\Core\Content\Rule\RuleCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\SalesChannel\SalesChannelCollection;

class SurchargeDiscountEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $internalName;

    /**
     * @var string|null
     */
    protected $internalId;

    /**
     * @var bool
     */
    protected $active;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var float
     */
    protected $value;

    /**
     * @var SalesChannelCollection|null
     */
    protected $salesChannels;

    /**
     * @var RuleCollection|null
     */
    protected $rules;

    /**
     * @var SurchargeDiscountTranslationCollection|null
     */
    protected $translations;

    /**
     * @return string
     */
    public function getInternalName(): string
    {
        return $this->internalName;
    }

    /**
     * @param string $internalName
     */
    public function setInternalName(string $internalName): void
    {
        $this->internalName = $internalName;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    /**
     * @return SalesChannelCollection|null
     */
    public function getSalesChannels(): ?SalesChannelCollection
    {
        return $this->salesChannels;
    }

    /**
     * @param SalesChannelCollection|null $salesChannels
     */
    public function setSalesChannels(?SalesChannelCollection $salesChannels): void
    {
        $this->salesChannels = $salesChannels;
    }

    /**
     * @return RuleCollection|null
     */
    public function getRules(): ?RuleCollection
    {
        return $this->rules;
    }

    /**
     * @param RuleCollection|null $rules
     */
    public function setRules(?RuleCollection $rules): void
    {
        $this->rules = $rules;
    }

    /**
     * @return SurchargeDiscountTranslationCollection|null
     */
    public function getTranslations(): ?SurchargeDiscountTranslationCollection
    {
        return $this->translations;
    }

    /**
     * @param SurchargeDiscountTranslationCollection|null $translations
     */
    public function setTranslations(?SurchargeDiscountTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string|null
     */
    public function getInternalId(): ?string
    {
        return $this->internalId;
    }

    /**
     * @param string|null $internalId
     */
    public function setInternalId(?string $internalId): void
    {
        $this->internalId = $internalId;
    }
}
