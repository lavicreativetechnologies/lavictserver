<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Custom;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(SurchargeDiscountEntity $entity)
 * @method void              set(string $key, SurchargeDiscountEntity $entity)
 * @method SurchargeDiscountEntity[]    getIterator()
 * @method SurchargeDiscountEntity[]    getElements()
 * @method SurchargeDiscountEntity|null get(string $key)
 * @method SurchargeDiscountEntity|null first()
 * @method SurchargeDiscountEntity|null last()
 */
class SurchargeDiscountCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return SurchargeDiscountEntity::class;
    }
}
