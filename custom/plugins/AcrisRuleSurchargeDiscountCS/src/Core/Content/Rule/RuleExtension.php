<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\System\SalesChannel;

use Acris\Configurator\Custom\Aggregate\ConfiguratorProductStreamsDefinition;
use Acris\Configurator\Custom\ConfiguratorDefinition;
use Acris\RuleSurchargeDiscount\Custom\Aggregate\SurchargeDiscountRulesDefinition;
use Acris\RuleSurchargeDiscount\Custom\Aggregate\SurchargeDiscountSalesChannelDefinition;
use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountDefinition;
use Shopware\Core\Content\ProductStream\ProductStreamDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\SalesChannel\SalesChannelDefinition;

class RuleExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new ManyToManyAssociationField(
                'surchargeDiscount',
                SurchargeDiscountDefinition::class,
                SurchargeDiscountRulesDefinition::class,
                'rule_id',
                'surcharge_discount_id'
            ))->addFlags(new Inherited())
        );

    }

    public function getDefinitionClass(): string
    {
        return SalesChannelDefinition::class;
    }
}
