<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\Group\LineItemGroupBuilder;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class SurchargeDiscountProcessor implements CartProcessorInterface
{
    public const DATA_KEY = 'surchargeDiscounts';
    public const LINE_ITEM_TYPE = 'surchargeDiscount';
    public const SURCHARGE_DISCOUNT_SCOPE = 'cart';
    public const SCOPE_SET = 'set';
    public const SCOPE_SETGROUP = 'setgroup';

    /**
     * @var SurchargeDiscountCalculator
     */
    private $surchargeDiscountCalculator;

    /**
     * @var LineItemGroupBuilder
     */
    private $groupBuilder;

    public function __construct(SurchargeDiscountCalculator $surchargeDiscountCalculator, LineItemGroupBuilder $groupBuilder)
    {
        $this->surchargeDiscountCalculator = $surchargeDiscountCalculator;
        $this->groupBuilder = $groupBuilder;
    }

    /**
     * @param CartDataCollection $data
     * @param Cart $original
     * @param Cart $calculated
     * @param SalesChannelContext $context
     * @param CartBehavior $behavior
     */
    public function process(CartDataCollection $data, Cart $original, Cart $calculated, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $calculated->getData()->set(LineItemGroupBuilder::class, $this->groupBuilder);

        if (!$data->has(self::DATA_KEY)) {
            return;
        }

        /** @var LineItemCollection $discountLineItems */
        $discountLineItems = $data->get(self::DATA_KEY);

        $this->surchargeDiscountCalculator->calculate(
            new LineItemCollection($discountLineItems),
            $original,
            $calculated,
            $context,
            $behavior
        );
    }
}
