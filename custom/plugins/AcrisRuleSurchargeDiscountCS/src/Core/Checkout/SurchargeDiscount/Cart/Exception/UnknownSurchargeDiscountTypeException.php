<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart\Exception;

use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountEntity;
use Shopware\Core\Framework\ShopwareHttpException;
use Symfony\Component\HttpFoundation\Response;

class UnknownSurchargeDiscountTypeException extends ShopwareHttpException
{
    public function __construct(SurchargeDiscountEntity $discount)
    {
        parent::__construct(
            'Unknown surcharge discount type detected: {{ type }}',
            ['type' => $discount->getType()]
        );
    }

    public function getErrorCode(): string
    {
        return 'CHECKOUT__UNKNOWN_SURCHARGE_DISCOUNT_TYPE';
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
