<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use Symfony\Component\HttpFoundation\Response;

class SurchargeDiscountCalculatorNotFoundException extends ShopwareHttpException
{
    public function __construct(string $type)
    {
        parent::__construct('Surcharge Discount Calculator "{{ type }}" has not been found!', ['type' => $type]);
    }

    public function getErrorCode(): string
    {
        return 'CHECKOUT__SURCHARGE_DISCOUNT_CALCULATOR_NOT_FOUND';
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
