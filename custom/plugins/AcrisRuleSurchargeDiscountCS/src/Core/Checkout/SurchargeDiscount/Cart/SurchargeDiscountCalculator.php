<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart;

use Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart\Calculator\SurchargeDiscountAbsoluteCalculator;
use Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart\Calculator\SurchargeDiscountPercentageCalculator;
use Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart\Exception\SurchargeDiscountCalculatorNotFoundException;
use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountDefinition;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\LineItem\Group\LineItemGroupBuilder;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItemFlatCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItemQuantitySplitter;
use Shopware\Core\Checkout\Cart\Price\AbsolutePriceCalculator;
use Shopware\Core\Checkout\Cart\Price\AmountCalculator;
use Shopware\Core\Checkout\Cart\Price\PercentagePriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\PriceCollection;
use Shopware\Core\Checkout\Cart\Tax\Struct\CalculatedTaxCollection;
use Shopware\Core\Checkout\Cart\Tax\Struct\TaxRuleCollection;
use Shopware\Core\Checkout\Promotion\Cart\Discount\Composition\DiscountCompositionBuilder;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountCalculatorResult;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountLineItem;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountPackage;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountPackageCollection;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountPackager;
use Shopware\Core\Checkout\Promotion\Cart\Discount\Filter\AdvancedPackageFilter;
use Shopware\Core\Checkout\Promotion\Cart\PromotionCartInformationTrait;
use Shopware\Core\Checkout\Promotion\Exception\InvalidScopeDefinitionException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * Cart Promotion Calculator
 */
class SurchargeDiscountCalculator
{
    use PromotionCartInformationTrait;

    /**
     * @var AmountCalculator
     */
    private $amountCalculator;

    /**
     * @var AbsolutePriceCalculator
     */
    private $absolutePriceCalculator;

    /**
     * @var LineItemGroupBuilder
     */
    private $groupBuilder;

    /**
     * @var AdvancedPackageFilter
     */
    private $advancedFilter;

    /**
     * @var LineItemQuantitySplitter
     */
    private $lineItemQuantitySplitter;

    /**
     * @var DiscountCompositionBuilder
     */
    private $discountCompositionBuilder;

    /**
     * @var PercentagePriceCalculator
     */
    private $percentagePriceCalculator;

    /**
     * @var DiscountPackager
     */
    private $cartScopeDiscountPackager;

    /**
     * @var DiscountPackager
     */
    private $setGroupScopeDiscountPackager;

    /**
     * @var DiscountPackager
     */
    private $setScopeDiscountPackager;

    public function __construct(
        AmountCalculator $amountCalculator,
        AbsolutePriceCalculator $absolutePriceCalculator,
        LineItemGroupBuilder $groupBuilder,
        DiscountCompositionBuilder $compositionBuilder,
        AdvancedPackageFilter $filter,
        LineItemQuantitySplitter $lineItemQuantitySplitter,
        PercentagePriceCalculator $percentagePriceCalculator,
        DiscountPackager $cartScopeDiscountPackager,
        DiscountPackager $setGroupScopeDiscountPackager,
        DiscountPackager $setScopeDiscountPackager
    ) {
        $this->amountCalculator = $amountCalculator;
        $this->absolutePriceCalculator = $absolutePriceCalculator;
        $this->groupBuilder = $groupBuilder;
        $this->discountCompositionBuilder = $compositionBuilder;
        $this->advancedFilter = $filter;
        $this->lineItemQuantitySplitter = $lineItemQuantitySplitter;
        $this->percentagePriceCalculator = $percentagePriceCalculator;
        $this->cartScopeDiscountPackager = $cartScopeDiscountPackager;
        $this->setGroupScopeDiscountPackager = $setGroupScopeDiscountPackager;
        $this->setScopeDiscountPackager = $setScopeDiscountPackager;
    }

    public function calculate(LineItemCollection $discountLineItems, Cart $original, Cart $calculated, SalesChannelContext $context, CartBehavior $behaviour): void
    {
        /* @var LineItem $discountLineItem */
        foreach ($discountLineItems as $discountItem) {

            $result = $this->calculateDiscount($discountItem, $calculated, $context);
            if (abs($result->getPrice()->getTotalPrice()) === 0.0) {
                continue;
            }
            $discountItem->setPrice($result->getPrice());

            $discountItem->setPayloadValue(
                'composition',
                $this->discountCompositionBuilder->buildCompositionPayload($result->getCompositionItems())
            );

            $calculated->addLineItems(new LineItemCollection([$discountItem]));

            $this->calculateCart($calculated, $context);
        }
    }

    /**
     * @param LineItem $lineItem
     * @param Cart $calculatedCart
     * @param SalesChannelContext $context
     * @return DiscountCalculatorResult
     */
    private function calculateDiscount(LineItem $lineItem, Cart $calculatedCart, SalesChannelContext $context): DiscountCalculatorResult
    {
        $discount = new DiscountLineItem(
            $lineItem->getLabel(),
            $lineItem->getPriceDefinition(),
            $lineItem->getPayload(),
            $lineItem->getReferencedId()
        );

        switch ($discount->getScope()) {
            case SurchargeDiscountProcessor::SURCHARGE_DISCOUNT_SCOPE:
                $packager = $this->cartScopeDiscountPackager;

                break;

            case SurchargeDiscountProcessor::SCOPE_SET:
                $packager = $this->setScopeDiscountPackager;

                break;

            case SurchargeDiscountProcessor::SCOPE_SETGROUP:
                $packager = $this->setGroupScopeDiscountPackager;

                break;

            default:
                throw new InvalidScopeDefinitionException($discount->getScope());
        }

        $packages = $packager->getMatchingItems($discount, $calculatedCart, $context);
        if ($packages->count() <= 0) {
            return new DiscountCalculatorResult(
                new CalculatedPrice(0, 0, new CalculatedTaxCollection(), new TaxRuleCollection(), 1),
                []
            );
        }

        if ($packager->getResultContext() === DiscountPackager::RESULT_CONTEXT_LINEITEM) {
            $packages = $packages->splitPackages();
        }

        $packages = $this->enrichPackagesWithCartData($packages, $calculatedCart, $context);

        $packages = $this->advancedFilter->filter($discount->getFilterSorterKey(), $discount->getFilterApplierKey(), $discount->getFilterUsageKey(), $packages);

        if ($packager->getResultContext() === DiscountPackager::RESULT_CONTEXT_LINEITEM) {
            $packages = new DiscountPackageCollection(
                [new DiscountPackage($packages->getAllLineMetaItems())]
            );
        }

        $packages = $this->enrichPackagesWithCartData($packages, $calculatedCart, $context);

        switch ($discount->getType()) {
            case SurchargeDiscountDefinition::TYPE_ABSOLUTE:
                $calculator = new SurchargeDiscountAbsoluteCalculator($this->absolutePriceCalculator);
                break;

            case SurchargeDiscountDefinition::TYPE_PERCENTAGE:
                $calculator = new SurchargeDiscountPercentageCalculator($this->absolutePriceCalculator, $this->percentagePriceCalculator);

                break;

            default:
                throw new SurchargeDiscountCalculatorNotFoundException($discount->getType());
        }
        $result = $calculator->calculate($discount, $packages, $context);

        $aggregatedCompositionItems = $this->discountCompositionBuilder->aggregateCompositionItems($result->getCompositionItems());
        $result = new DiscountCalculatorResult($result->getPrice(), $aggregatedCompositionItems);

        $maxDiscountValue = $this->getMaxDiscountValue($calculatedCart, $context);

        if (abs($result->getPrice()->getTotalPrice()) > abs($maxDiscountValue) && $result->getPrice()->getTotalPrice() < 0) {
            $result = $this->limitDiscountResult($maxDiscountValue, $packages->getAffectedPrices(), $result, $context);
        }
        return $result;
    }

    private function getMaxDiscountValue(Cart $cart, SalesChannelContext $context): float
    {
        if ($context->getTaxState() === CartPrice::TAX_STATE_NET) {
            return $cart->getPrice()->getNetPrice();
        }

        return $cart->getPrice()->getTotalPrice();
    }

    private function limitDiscountResult(float $maxDiscountValue, PriceCollection $priceCollection, DiscountCalculatorResult $originalResult, SalesChannelContext $context): DiscountCalculatorResult
    {
        $price = $this->absolutePriceCalculator->calculate(
            -abs($maxDiscountValue),
            $priceCollection,
            $context
        );

        $adjustedItems = $this->discountCompositionBuilder->adjustCompositionItemValues($price, $originalResult->getCompositionItems());

        return new DiscountCalculatorResult($price, $adjustedItems);
    }

    private function calculateCart(Cart $cart, SalesChannelContext $context): void
    {
        $amount = $this->amountCalculator->calculate(
            $cart->getLineItems()->getPrices(),
            $cart->getDeliveries()->getShippingCosts(),
            $context
        );

        $cart->setPrice($amount);
    }

    private function enrichPackagesWithCartData(DiscountPackageCollection $result, Cart $cart, SalesChannelContext $context): DiscountPackageCollection
    {
        foreach ($result as $package) {
            $cartItemsForUnit = new LineItemFlatCollection();

            foreach ($package->getMetaData() as $item) {
                /** @var LineItem $cartItem */
                $cartItem = $cart->get($item->getLineItemId());

                $qtyItem = $this->lineItemQuantitySplitter->split($cartItem, $item->getQuantity(), $context);

                $cartItemsForUnit->add($qtyItem);
            }

            $package->setCartItems($cartItemsForUnit);
        }

        return $result;
    }
}
