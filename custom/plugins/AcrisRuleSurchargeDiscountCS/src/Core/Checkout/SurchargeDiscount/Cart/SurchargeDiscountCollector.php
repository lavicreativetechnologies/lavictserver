<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart;

use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountCollection;
use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountEntity;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartDataCollectorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Promotion\Cart\PromotionCartInformationTrait;
use Shopware\Core\Checkout\Promotion\Cart\PromotionProcessor;
use Shopware\Core\Checkout\Promotion\Gateway\PromotionGatewayInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class SurchargeDiscountCollector implements CartDataCollectorInterface
{
    use PromotionCartInformationTrait;

    /**
     * @var PromotionGatewayInterface
     */
    private $gateway;

    /**
     * @var SurchargeDiscountItemBuilder
     */
    private $itemBuilder;

    /**
     * @var EntityRepositoryInterface
     */
    private $surchargeDiscountRepository;

    public function __construct(PromotionGatewayInterface $gateway, SurchargeDiscountItemBuilder $itemBuilder, EntityRepositoryInterface $surchargeDiscountRepository)
    {
        $this->gateway = $gateway;
        $this->itemBuilder = $itemBuilder;
        $this->surchargeDiscountRepository = $surchargeDiscountRepository;
    }

    /**
     * @param CartDataCollection $data
     * @param Cart $cart
     * @param SalesChannelContext $context
     * @param CartBehavior $behavior
     */
    public function collect(CartDataCollection $data, Cart $cart, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('active', true));
        $criteria->addSorting(new FieldSorting('value',FieldSorting::ASCENDING, true));
        $criteria->addAssociation('salesChannels');
        $criteria->addAssociation('rules');

        $surchargeDiscountResult = $this->surchargeDiscountRepository->search($criteria, $context->getContext());
        $discountLineItems = [];
        $lineItems = $this->buildDiscountLineItems($surchargeDiscountResult->getEntities(), $cart, $context);
        /** @var LineItem $nested */
        foreach ($lineItems as $nested) {
            $discountLineItems[] = $nested;
        }

        if (count($discountLineItems) > 0) {
            $data->set(SurchargeDiscountProcessor::DATA_KEY, new LineItemCollection($discountLineItems));
        } else {
            $data->remove(SurchargeDiscountProcessor::DATA_KEY);
        }
    }

    /**
     * @param SurchargeDiscountEntity $promotion
     * @param Cart $cart
     * @param SalesChannelContext $context
     * @return array
     */
    private function buildDiscountLineItems(SurchargeDiscountCollection $collection, Cart $cart, SalesChannelContext $context): array
    {
        $lineItems = [];
        foreach ($collection->getElements() as $surchargeDiscount) {
            $itemIds = $this->getAllLineItemIds($cart);
            $salesChannelId = $context->getSalesChannel()->getId();
            $rules = $context->getRuleIds();
            $surchargeDiscountSalesChannels = $surchargeDiscount->getSalesChannels();
            $surchargeDiscountRules = $surchargeDiscount->getRules();

            $surchargeDiscountSalesChannelExists = false;
            foreach ($surchargeDiscountSalesChannels as $key => $surchargeDiscountSalesChannel) {
                if ($salesChannelId === $key) {
                    $surchargeDiscountSalesChannelExists = true;
                }
            }
            if (!$surchargeDiscountSalesChannelExists) continue;

            $surchargeDiscountRulesExists = false;

            foreach ($surchargeDiscountRules as $key => $surchargeDiscountRule) {
                if (!empty($rules) && is_array($rules) && in_array($key, $rules)) {
                    $surchargeDiscountRulesExists = true;
                }
            }
            if (!$surchargeDiscountRulesExists) continue;

            if (count($itemIds) <= 0) {
                continue;
            }

            /* @var LineItem $discountItem */
            $discountItem = $this->itemBuilder->buildLineItem(
                $surchargeDiscount,
                $context->getContext()->getCurrencyPrecision(),
                $context->getCurrency()->getId()
            );

            $lineItems[] = $discountItem;
        }

        return $lineItems;
    }

    private function getAllLineItemIds(Cart $cart): array
    {
        return $cart->getLineItems()->fmap(
            static function (LineItem $lineItem) {
                if ($lineItem->getType() === PromotionProcessor::LINE_ITEM_TYPE) {
                    return null;
                }

                return $lineItem->getId();
            }
        );
    }
}
