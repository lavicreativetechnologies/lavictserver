<?php declare(strict_types=1);

namespace Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart;

use Acris\RuleSurchargeDiscount\Core\Checkout\SurchargeDiscount\Cart\Exception\UnknownSurchargeDiscountTypeException;
use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountDefinition;
use Acris\RuleSurchargeDiscount\Custom\SurchargeDiscountEntity;
use Shopware\Core\Checkout\Cart\Exception\InvalidPayloadException;
use Shopware\Core\Checkout\Cart\Exception\InvalidQuantityException;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\Struct\AbsolutePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\PercentagePriceDefinition;
use Shopware\Core\Checkout\Promotion\Exception\UnknownPromotionDiscountTypeException;

class SurchargeDiscountItemBuilder
{
    /**
     * @throws InvalidPayloadException
     * @throws InvalidQuantityException
     * @throws UnknownPromotionDiscountTypeException
     */
    public function buildLineItem(SurchargeDiscountEntity $surchargeDiscountEntity, int $currencyPrecision, string $currencyId): LineItem
    {
        $targetFilter = null;
        $promotionValue = $surchargeDiscountEntity->getValue();
        switch ($surchargeDiscountEntity->getType()) {
            case SurchargeDiscountDefinition::TYPE_ABSOLUTE:
                $promotionValue = $surchargeDiscountEntity->getValue();
                $promotionDefinition = new AbsolutePriceDefinition($promotionValue, $currencyPrecision, $targetFilter);

                break;

            case SurchargeDiscountDefinition::TYPE_PERCENTAGE:
                $promotionDefinition = new PercentagePriceDefinition($promotionValue, $currencyPrecision, $targetFilter);

                break;

            default:
                $promotionDefinition = null;
        }

        if ($promotionDefinition === null) {
            throw new UnknownSurchargeDiscountTypeException($surchargeDiscountEntity);
        }
        $promotionItem = new LineItem($surchargeDiscountEntity->getId(), SurchargeDiscountProcessor::LINE_ITEM_TYPE);
        if (!empty($surchargeDiscountEntity) && !empty($surchargeDiscountEntity->getTranslated()) && array_key_exists('displayName', $surchargeDiscountEntity->getTranslated()) && !empty($surchargeDiscountEntity->getTranslated()['displayName'])) {
            $promotionItem->setLabel($surchargeDiscountEntity->getTranslated()['displayName']);
            $promotionItem->setDescription($surchargeDiscountEntity->getTranslated()['displayName']);
        }
        $promotionItem->setGood(false);
        $promotionItem->setRemovable(true);
        $promotionItem->setPriceDefinition($promotionDefinition);
        $promotionItem->setPayload(
            $this->buildPayload(
                $surchargeDiscountEntity
            )
        );

        $promotionItem->setReferencedId('');

        return $promotionItem;
    }

    /**
     * @param SurchargeDiscountEntity $surchargeDiscountEntity
     * @return array
     */
    private function buildPayload(SurchargeDiscountEntity $surchargeDiscountEntity): array
    {
        $payload = [];
        if (!empty($surchargeDiscountEntity->getInternalId())) {
            $payload['internalId'] = $surchargeDiscountEntity->getInternalId();
        }

        $payload['surchargeDiscountId'] = $surchargeDiscountEntity->getId();

        $payload['discountType'] = $surchargeDiscountEntity->getType();

        $payload['value'] = (string) $surchargeDiscountEntity->getValue();

        $payload['discountScope'] = SurchargeDiscountProcessor::SURCHARGE_DISCOUNT_SCOPE;

        $payload['exclusions'] = [];

        $payload['customFields'] = [];

        $payload['filter'] = [
            'sorterKey' => null,
            'applierKey' => null,
            'usageKey' => null,
        ];

        return $payload;
    }
}
