# 1.0.7
- Problem beim Anlegen von Zu-/Abschlägen in der Verwaltung behoben.

# 1.0.6
- Optimieren Sie Snippets.

# 1.0.5
- Hilfetext für Aufpreis-/Rabatt-Wertfeld hinzugefügt.

# 1.0.4
- Optimieren Sie die Kassenvorlage in der Storefront.

# 1.0.3
- Optimieren Sie die Plugin-Kompatibilität.

# 1.0.2
- Validierung für Aufpreis-/Rabattberechnung hinzugefügt.

# 1.0.1
- Zuschlag / Rabatt im Schaufenster optimieren.

# 1.0.0
- Veröffentlichung
