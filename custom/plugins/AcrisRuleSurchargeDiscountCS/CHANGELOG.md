# 1.0.7
- Fix problem on creating of surcharge/discount at administration.

# 1.0.6
- Optimize snippets.

# 1.0.5
- Added help text for surcharge/discount value field.

# 1.0.4
- Optimize checkout template at storefront.

# 1.0.3
- Optimize plugin compatibility.

# 1.0.2
- Added validation for surcharge/discount calculation.

# 1.0.1
- Optimize surcharge / discount at storefront.

# 1.0.0
- Release
