# CommissionField SW6
## About CommissionField SW6
This Plugin adds a commission field to every article in the basket for Shopware 6.

## Features
### Frontend
Adds input field commission number for each article to basket and checkout. The input is saved automatically, so the customer don`t need to hit the save button (but he can). To clear the commission field, an additional button is displayed.

### Documents
The commission content will be displayed in the documents under the article name.

### Snippets
You can of course change every snippet in the snippet manager by shopware.

## Requirements
* PHP >= 7.2
* Shopware >= 6.1.0

## Installation
Download and install plugin via the Shopware plugin manager.

To display the commission field in the order confirmation mail, you need to set the following snippet to the E-Mail template:

`Commission: {{ lineItem.customFields.ies_commission }}`

## License

Please see [License File](LICENSE) for more information.
