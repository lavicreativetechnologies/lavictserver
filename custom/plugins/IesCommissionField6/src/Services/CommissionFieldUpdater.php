<?php declare(strict_types=1);

namespace Ies\CommissionField\Services;

use Ies\CommissionField\Struct\CommissionFieldData;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartPersister;
use Shopware\Core\Checkout\Cart\CartPersisterInterface;
use Shopware\Core\Checkout\Cart\Exception\LineItemNotFoundException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CommissionFieldUpdater
{
    /**
     * @var CartPersisterInterface
     */
    private $cartPersister;

    public function __construct(CartPersister $cartPersister)
    {
        $this->cartPersister = $cartPersister;
    }

    public function updateCommissionField(Cart $cart, string $id, ?string $iesCommission, SalesChannelContext $context)
    {
        if (!$lineItem = $cart->getLineItems()->get($id)) {
            throw new LineItemNotFoundException($id);
        }

        $lineItem->setExtensions(['ies_commission' => new CommissionFieldData($iesCommission)]);
        $this->cartPersister->save($cart, $context);
    }
}
