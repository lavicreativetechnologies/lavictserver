<?php declare(strict_types=1);

namespace Ies\CommissionField\Struct;

use Shopware\Core\Framework\Struct\Struct;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CommissionFieldData extends Struct
{
    /**
     * @var string|null
     */
    protected $content;

    public function __construct(?string $content = null)
    {
        $this->content = $content;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }
}
