<?php declare(strict_types=1);

namespace Ies\CommissionField\Controller;

use Ies\CommissionField\Services\CommissionFieldUpdater;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"storefront"})
 */
class CartLineItemController extends StorefrontController
{
    /**
     * @var CommissionFieldUpdater
     */
    private $commissionFieldUpdater;

    public function __construct(CommissionFieldUpdater $commissionFieldUpdater)
    {
        $this->commissionFieldUpdater = $commissionFieldUpdater;
    }

    /**
     * @Route("/checkout/line-item/update-commission/{id}", name="frontend.checkout.line-item.update-commission", defaults={"XmlHttpRequest": true}, methods={"POST"})
     */
    public function updateCommission(Cart $cart, string $id, Request $request, SalesChannelContext $salesChannelContext): JsonResponse
    {
        $iesCommission = $request->get('ies_commission');
        $this->commissionFieldUpdater->updateCommissionField($cart, $id, $iesCommission, $salesChannelContext);

        return new JsonResponse(['ies_commission' => $iesCommission]);
    }
}
