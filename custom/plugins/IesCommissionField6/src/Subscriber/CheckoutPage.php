<?php declare(strict_types=1);

namespace Ies\CommissionField\Subscriber;

use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Event\StorefrontRenderEvent;
use Shopware\Storefront\Page\Checkout\Cart\CheckoutCartPage;
use Shopware\Storefront\Page\Checkout\Confirm\CheckoutConfirmPage;
use Shopware\Storefront\Page\Checkout\Finish\CheckoutFinishPage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CheckoutPage implements EventSubscriberInterface
{
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
    }

    public static function getSubscribedEvents()
    {
        return [
            StorefrontRenderEvent::class => 'onRenderStoreFront',
        ];
    }

    public function onRenderStoreFront(StorefrontRenderEvent $event)
    {
        $parameters = $event->getParameters();
        if (array_key_exists('page', $parameters) && $this->isCheckoutPage($parameters['page'])) {
            $config = $this->systemConfigService->get('IesCommissionField6.config.IesCommissionFieldDisplayButtons');
            $event->setParameter('iesCommissionDisplayButtons', $config);
        };
    }

    private function isCheckoutPage($page): array
    {
        $checkoutPages = [
            CheckoutCartPage::class,
            CheckoutConfirmPage::class,
            CheckoutFinishPage::class,
        ];

        return array_filter($checkoutPages, function ($checkoutPage) use ($page) {
            return $page instanceof $checkoutPage;
        });
    }
}
