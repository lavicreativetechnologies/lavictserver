<?php declare(strict_types=1);

namespace Ies\CommissionField\Subscriber;

use Shopware\Core\Checkout\Cart\Order\CartConvertedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CheckoutConfirm implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            CartConvertedEvent::class => 'onCartConverted',
        ];
    }

    public function onCartConverted(CartConvertedEvent $event)
    {
        $convertedCart = $event->getConvertedCart();
        $convertedCart = $this->addCommissionFieldsToConvertedCart($convertedCart, $event->getCart()->getLineItems());

        $event->setConvertedCart($convertedCart);
    }

    private function addCommissionFieldsToConvertedCart(array $convertedCart, LineItemCollection $cartLineItems): array
    {
        foreach ($convertedCart['lineItems'] as $index => $convertedLineItem) {
            $cartLineItem = $cartLineItems->get($convertedLineItem['identifier']);
            if ($iesCommissionField = $this->getIesCommissionContent($cartLineItem)) {
                $convertedCart['lineItems'][$index]['customFields']['ies_commission'] = $iesCommissionField;
            }
        }

        return $convertedCart;
    }

    private function getIesCommissionContent(?LineItem $cartLineItem): ?string
    {
        if (!$cartLineItem->hasExtension('ies_commission')) {
            return null;
        }

        return $cartLineItem->getExtension('ies_commission')->getContent();
    }
}
