import template from './sw-order-line-items-grid.html.twig';

const { Component } = Shopware;

Component.override('sw-order-line-items-grid', {
    template,

    computed: {
        getLineItemColumns() {
            const columnDefinitions = this.$super('getLineItemColumns');

            columnDefinitions.splice(1, 0, {
                property: 'commission',
                dataIndex: 'commission',
                label: 'sw-order.detailBase.columnCommissionName',
                allowResize: false,
                width: '120px'
            });

            return columnDefinitions;
        }
    }
});
