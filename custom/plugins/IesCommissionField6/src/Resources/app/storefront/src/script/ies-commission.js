import Plugin from 'src/plugin-system/plugin.class';
import HttpClient from 'src/service/http-client.service';
import DomAccess from 'src/helper/dom-access.helper';
import FormSerializeUtil from 'src/utility/form/form-serialize.util';
import Iterator from 'src/helper/iterator.helper';
import ElementLoadingIndicatorUtil from 'src/utility/loading-indicator/element-loading-indicator.util';
import ElementReplaceHelper from 'src/helper/element-replace.helper';

export default class IesCommissionPlugin extends Plugin {

    static options = {
        replaceSelectors: false,
        submitOnChange: false,

        saveButtonSelector: '.ies-commission-save',
        clearButtonSelector: '.ies-commission-clear',
        commissionFieldSelector: '.ies-commission-field',
    };

    init() {
        this._client = new HttpClient(window.accessKey, window.contextToken);
        this._form = this.el;
        this._commissionField = DomAccess.querySelector(this.el, this.options.commissionFieldSelector);
        if (!this.options.submitOnChange) {
            this._saveButton = DomAccess.querySelector(this.el, this.options.saveButtonSelector);
            this._clearButton = DomAccess.querySelector(this.el, this.options.clearButtonSelector);
        }

        this._registerEvents();
    }

    _registerEvents() {
        if (this.options.submitOnChange) {
            this._commissionField.addEventListener('change', this._handleSubmit.bind(this))
        } else {
            this._saveButton.addEventListener('click', this._handleSubmit.bind(this));
            this._clearButton.addEventListener('click', this._handleSubmit.bind(this));
        }
    }

    _handleSubmit(event) {
        event.preventDefault();
        if (this._form.checkValidity() === false) {
            return;
        }

        if (event.currentTarget === this._clearButton) {
            this._commissionField.value = '';
        }

        this.$emitter.publish('beforeSubmit');
        this._fireRequest();
    }

    _fireRequest() {
        this._createLoadingIndicators();
        const action = DomAccess.getAttribute(this._form, 'action');
        this.$emitter.publish('beforeFireRequest');
        this._client.post(action, this._getFormData(), this._onAfterAjaxSubmit.bind(this));
    }

    _getFormData() {
        return FormSerializeUtil.serialize(this._form);
    }

    _onAfterAjaxSubmit(response) {
        if (this.options.replaceSelectors) {
            this._removeLoadingIndicators();
            ElementReplaceHelper.replaceFromMarkup(response, this.options.replaceSelectors, false);
            window.PluginManager.initializePlugins();
        }

        this.$emitter.publish('onAfterAjaxSubmit', { response });
    }

    _createLoadingIndicators() {
        if (this.options.replaceSelectors) {
            Iterator.iterate(this.options.replaceSelectors, (selector) => {
                const elements = DomAccess.querySelectorAll(document, selector);
                Iterator.iterate(elements, ElementLoadingIndicatorUtil.create);
            });
        }

        this.$emitter.publish('createLoadingIndicators');
    }

    _removeLoadingIndicators() {
        Iterator.iterate(this.options.replaceSelectors, (selector) => {
            const elements = DomAccess.querySelectorAll(document, selector);
            Iterator.iterate(elements, ElementLoadingIndicatorUtil.remove);
        });

        this.$emitter.publish('removeLoadingIndicators');
    }
}
