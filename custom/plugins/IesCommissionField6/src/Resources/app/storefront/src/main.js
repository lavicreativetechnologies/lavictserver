import IesCommissionPlugin from './script/ies-commission';

const PluginManager = window.PluginManager;

PluginManager.register('IesCommission', IesCommissionPlugin, '[data-ies-commission]');

if (module.hot) {
    module.hot.accept();
}
