# CHANGELOG

- v1.1.3
    - added condition to display commission only at product types
- v1.1.2
    - remove js files with old names to avoid conflict
- v1.1.1
    - add bigger plugin icon
    - added font/backend static css and js
- v1.1.0
    - add shopware/storefront as dependency
- v1.0.4
    - change name to match shopware technical name
- v1.0.3
    - fix shopware version in composer.json
- v1.0.2
    - fix storefront subscriber error, check page parameter only if it exists
- v1.0.1
    - Added backend commission view
- v1.0.0
    - Plugin Init
