<?php declare(strict_types=1);

namespace FeldmannTheme\tests\Twig;

use FeldmannTheme\Twig\SortOrderLineItemsExtension;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemCollection;
use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemEntity;

require_once __DIR__ . '/../../src/Twig/SortOrderLineItemsExtension.php';

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2019 isento eCommerce solutions GmbH
 */
class SortOrderLineItemsExtensionTest extends TestCase
{
    public function testCanBasiclySortEverything()
    {
        $extension = new SortOrderLineItemsExtension();

        $emptyArray = $extension->sort([]);
        $intArray = $extension->sort([1, 2, 3]);
        $stringArray = $extension->sort(['1', '2', '3']);
        $objectArray = $extension->sort([new \stdClass(), new \stdClass(), new \stdClass()]);

        $this->assertEquals([], $emptyArray);
        $this->assertEquals([1, 2, 3], $intArray);
        $this->assertEquals(['1', '2', '3'], $stringArray);
        $this->assertEquals([new \stdClass(), new \stdClass(), new \stdClass()], $objectArray);
    }

    public function testCanSortOrderLineItemCollection()
    {
        $extension = new SortOrderLineItemsExtension();

        $collection = new OrderLineItemCollection([
            $this->createOrderLineItem(5),
            $this->createOrderLineItem(1),
            $this->createOrderLineItem(3),
            $this->createOrderLineItem(20),
            $this->createOrderLineItem(4),
        ]);

        $sorted = $extension->sort($collection);
        $sortedElements = array_values($sorted->getElements());

        $this->assertEquals($collection->get('5')->getPosition(), $sortedElements[3]->getPosition());
        $this->assertEquals($collection->get('1')->getPosition(), $sortedElements[0]->getPosition());
        $this->assertEquals($collection->get('3')->getPosition(), $sortedElements[1]->getPosition());
        $this->assertEquals($collection->get('20')->getPosition(), $sortedElements[4]->getPosition());
        $this->assertEquals($collection->get('4')->getPosition(), $sortedElements[2]->getPosition());
    }

    private function createOrderLineItem(int $position): OrderLineItemEntity
    {
        $orderLineItem = new OrderLineItemEntity();
        $orderLineItem->setUniqueIdentifier((string)$position);
        $orderLineItem->setPosition($position);

        return $orderLineItem;
    }
}
