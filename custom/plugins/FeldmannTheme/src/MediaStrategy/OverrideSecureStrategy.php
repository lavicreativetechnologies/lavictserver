<?php

namespace FeldmannTheme\MediaStrategy;

use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Content\Media\Pathname\PathnameStrategy\AbstractPathNameStrategy;
use Shopware\Core\Content\Media\Aggregate\MediaThumbnail\MediaThumbnailEntity;

class OverrideSecureStrategy extends AbstractPathNameStrategy
{
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'override_secure';
    }

    /**
     * {@inheritdoc}
     */
    public function generatePathHash(MediaEntity $media, ?MediaThumbnailEntity $thumbnail = null): ?string
    {
        return $this->generateMd5Path($media->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function generatePathCacheBuster(MediaEntity $media, ?MediaThumbnailEntity $thumbnail = null): ?string
    {
        $createdAt = $media->getCreatedAt();

        if ($createdAt === null) {
            return null;
        }

        return (string) $createdAt->getTimestamp();
    }
}
