<?php declare(strict_types=1);

namespace FeldmannTheme\MediaStrategy;

use Shopware\Core\Content\Media\Aggregate\MediaThumbnail\MediaThumbnailEntity;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Content\Media\Pathname\UrlGeneratorInterface;

class UrlGenerator implements UrlGeneratorInterface
{
    private $innerUrlGenerator;
    
    public function __construct(UrlGeneratorInterface $innerUrlGenerator)
    {
        $this->innerUrlGenerator = $innerUrlGenerator;
    }

    public function getRelativeMediaUrl(MediaEntity $media): string
    {
        return $this->innerUrlGenerator->getRelativeMediaUrl($media);
    }

    public function getAbsoluteMediaUrl(MediaEntity $media): string
    {
        $cacheBuster = $media->getUploadedAt() ? '?cb=' . $media->getUploadedAt()->getTimestamp() : '';

        return $this->innerUrlGenerator->getAbsoluteMediaUrl($media) . $cacheBuster;
    }

    public function getRelativeThumbnailUrl(MediaEntity $media, MediaThumbnailEntity $thumbnail): string
    {
        return $this->innerUrlGenerator->getRelativeThumbnailUrl($media, $thumbnail);
    }

    public function getAbsoluteThumbnailUrl(MediaEntity $media, MediaThumbnailEntity $thumbnail): string
    {
        $cacheBuster = $media->getUploadedAt() ? '?cb=' . $media->getUploadedAt()->getTimestamp() : '';

        return $this->innerUrlGenerator->getAbsoluteThumbnailUrl($media, $thumbnail) . $cacheBuster;
    }
}
