<?php declare(strict_types=1);

namespace FeldmannTheme\Storefront\Controller;

use FeldmannTheme\Services\CategoryVisibility;
use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Content\Category\Exception\CategoryNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Framework\Cache\Annotation\HttpCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Storefront\Controller\NavigationController as NavigationControllerInner;

/**
 * @RouteScope(scopes={"storefront"})
 */
class NavigationController extends NavigationControllerInner
{
    private $navigationControllerInner;

    private $categoryRepository;

    private $categoryVisibility;

    public function __construct(
        NavigationControllerInner $navigationControllerInner,
        SalesChannelRepositoryInterface $categoryRepository,
        CategoryVisibility $categoryVisibility
    ) {
        $this->navigationControllerInner = $navigationControllerInner;
        $this->categoryRepository = $categoryRepository;
        $this->categoryVisibility = $categoryVisibility;
    }

    /**
     * @HttpCache()
     * @Route("/", name="frontend.home.page", options={"seo"="true"}, methods={"GET"})
     */
    public function home(Request $request, SalesChannelContext $context): ?Response
    {
        return $this->navigationControllerInner->home($request, $context);
    }

    /**
     * @HttpCache()
     * @Route("/navigation/{navigationId}", name="frontend.navigation.page", options={"seo"=true}, methods={"GET"})
     */
    public function index(SalesChannelContext $context, Request $request): Response
    {
        $navigationId = $request->get('navigationId', $context->getSalesChannel()->getNavigationCategoryId());
        $category = $this->loadCategory($navigationId, $context);
        if (!$this->categoryVisibility->isVisibleForUser($category, $context)) {
            return $this->redirectToRoute('frontend.home.page');
        }

        return $this->navigationControllerInner->index($context, $request);
    }

    /**
     * @HttpCache()
     * @Route("/widgets/menu/offcanvas", name="frontend.menu.offcanvas", methods={"GET"}, defaults={"XmlHttpRequest"=true})
     */
    public function offcanvas(Request $request, SalesChannelContext $context): Response
    {
        return $this->navigationControllerInner->offcanvas($request, $context);
    }

    private function loadCategory(string $categoryId, SalesChannelContext $context): CategoryEntity
    {
        $criteria = new Criteria([$categoryId]);

        $category = $this->categoryRepository
            ->search($criteria, $context)
            ->get($categoryId);

        if (!$category) {
            throw new CategoryNotFoundException($categoryId);
        }

        return $category;
    }
}
