<?php declare(strict_types=1);

namespace FeldmannTheme\Storefront\Controller\B2bPlatform;

use Shopware\B2B\AclRoute\Framework\AclRouteAssignmentService;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Role\Framework\RoleCrudService;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\Role\Framework\RoleRepository;
use Shopware\B2B\Role\Framework\RoleService;
use Shopware\B2B\Role\Frontend\RoleController as RoleControllerInner;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class RoleController extends RoleControllerInner
{
    const STANDARD_ROLE = 'standard';

    const ACL_COMPONENT_CONTACT = 'contact';

    private RoleControllerInner $roleControllerInner;

    private RoleCrudService $roleCrudService;

    private RoleRepository $roleRepository;

    private AclRouteAssignmentService $aclRouteAssignmentService;

    public function __construct(
        RoleControllerInner $roleControllerInner,
        AuthenticationService $authenticationService,
        RoleRepository $roleRepository,
        RoleCrudService $roleCrudService,
        GridHelper $gridHelper,
        RoleService $roleService,
        AclRouteAssignmentService $aclRouteAssignmentService
    ) {
        parent::__construct($authenticationService, $roleRepository, $roleCrudService, $gridHelper, $roleService);

        $this->roleControllerInner = $roleControllerInner;
        $this->roleCrudService = $roleCrudService;
        $this->roleRepository = $roleRepository;
        $this->aclRouteAssignmentService = $aclRouteAssignmentService;
    }

    public function subtreeAction(Request $request): array
    {
        $ownershipContext = $this->getOwnershipContext();
        $root = $this->roleRepository->fetchRoot($ownershipContext);
        if (!$this->hasStandardRole($root, $ownershipContext)) {
            $standardRole = $this->createStandardRole($root, $ownershipContext);
            $this->aclRouteAssignmentService->allowAll($ownershipContext, $standardRole, true);
            $this->aclRouteAssignmentService->denyComponent($ownershipContext, $standardRole, self::ACL_COMPONENT_CONTACT);
        }

        return $this->roleControllerInner->subtreeAction($request);
    }

    private function hasStandardRole(RoleEntity $root, OwnershipContext $ownershipContext): bool
    {
        $children = $this->roleRepository->fetchChildren($root->id, $ownershipContext);
        foreach ($children as $child) {
            if ($child->name === self::STANDARD_ROLE) {
                return true;
            }
        }

        return false;
    }

    private function createStandardRole(RoleEntity $root, OwnershipContext $ownershipContext): RoleEntity
    {
        $standardRole = [
            'name' => self::STANDARD_ROLE,
            'parentId' => $root->id,
        ];
        $serviceRequest = $this->roleCrudService->createNewRecordRequest($standardRole);

        return $this->roleCrudService->create($serviceRequest, $ownershipContext);
    }
}
