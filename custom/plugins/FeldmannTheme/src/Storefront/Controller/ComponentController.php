<?php declare(strict_types=1);

namespace FeldmannTheme\Storefront\Controller;

use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"storefront"})
 */
class ComponentController extends StorefrontController
{
    /**
     * @Route("/isento/components", name="frontend.isento.components", options={"seo"="false"}, methods={"GET"})
     */
    public function componentsPage(Request $request, RequestDataBag $data): Response
    {
        if ('prod' === $this->container->getParameter('kernel.environment')) {
            return $this->redirectToRoute('frontend.home.page');
        }

        return $this->renderStorefront('@Storefront/storefront/isento/index.html.twig');
    }
}
