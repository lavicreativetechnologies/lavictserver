<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace FeldmannTheme\Core\Rule;

use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Rule\CartRuleScope;
use Shopware\Core\Checkout\Cart\Rule\LineItemCustomFieldRule;
use Shopware\Core\Checkout\Cart\Rule\LineItemScope;
use Shopware\Core\Framework\Rule\Exception\UnsupportedOperatorException;
use Shopware\Core\Framework\Rule\RuleScope;
use Shopware\Core\System\CustomField\CustomFieldTypes;

/**
 * shopware bugfix, cart error when
 *      - LineItemCustomFieldRule in rule builder applied and
 *      - promotion triggered
 * https://issues.shopware.com/issues/NEXT-13255
 */
class LineItemProductCustomFieldRule extends LineItemCustomFieldRule
{
    public function getName(): string
    {
        return 'cartLineItemProductCustomField';
    }

    public function match(RuleScope $scope): bool
    {
        if ($scope instanceof LineItemScope) {
            return $this->isCustomFieldValid($scope->getLineItem());
        }

        if (!$scope instanceof CartRuleScope) {
            return false;
        }

        foreach ($scope->getCart()->getLineItems() as $lineItem) {
            if ($this->isCustomFieldValid($lineItem)) {
                return true;
            }
        }

        return false;
    }

    private function isCustomFieldValid(LineItem $lineItem): bool
    {
        /**
         * Shopware Bugfix NEXT-13255, this rule should only be applied to product item
         */
        if ($lineItem->getType() !== LineItem::PRODUCT_LINE_ITEM_TYPE) {
            return false;
        }

        $customFields = $lineItem->getPayloadValue('customFields');

        $actual = $this->getValue($customFields, $this->renderedField);
        if ($actual === null) {
            return false;
        }

        $expected = $this->getExpectedValue($this->renderedFieldValue, $this->renderedField);

        switch ($this->operator) {
            case self::OPERATOR_NEQ:
                return $actual !== $expected;
            case self::OPERATOR_GTE:
                return $actual >= $expected;
            case self::OPERATOR_LTE:
                return $actual <= $expected;
            case self::OPERATOR_EQ:
                return $actual === $expected;
            case self::OPERATOR_GT:
                return $actual > $expected;
            case self::OPERATOR_LT:
                return $actual < $expected;
            default:
                throw new UnsupportedOperatorException($this->operator, self::class);
        }
    }

    private function getValue(array $customFields, array $renderedField)
    {
        if (\in_array($renderedField['type'], [CustomFieldTypes::BOOL, CustomFieldTypes::SWITCH], true)) {
            if (!empty($customFields) && \array_key_exists($this->renderedField['name'], $customFields)) {
                return $customFields[$renderedField['name']];
            }

            return false;
        }

        if (!empty($customFields) && \array_key_exists($this->renderedField['name'], $customFields)) {
            return $customFields[$renderedField['name']];
        }

        return null;
    }

    private function getExpectedValue($renderedFieldValue, array $renderedField)
    {
        if (\in_array($renderedField['type'], [CustomFieldTypes::BOOL, CustomFieldTypes::SWITCH], true)) {
            return $renderedFieldValue ?? false; // those fields are initialized with null in the rule builder
        }

        return $renderedFieldValue;
    }
}
