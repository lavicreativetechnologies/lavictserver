<?php declare(strict_types=1);

namespace FeldmannTheme\Core\Content\ContactForm\Validation;

use Shopware\Core\Content\ContactForm\Validation\ContactFormValidationFactory as InnerFactory;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Validation\DataValidationDefinition;
use Shopware\Core\Framework\Validation\DataValidationFactoryInterface;
use Shopware\Core\Framework\Validation\ValidationServiceInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactFormValidationFactory implements DataValidationFactoryInterface
{
    private $innerFactory;

    public function __construct(InnerFactory $innerFactory)
    {
        $this->innerFactory = $innerFactory;
    }

    public function create(SalesChannelContext $context): DataValidationDefinition
    {
        $definition = $this->innerFactory->create($context);

        return $this->addAdditionValidators($definition);
    }

    public function update(SalesChannelContext $context): DataValidationDefinition
    {
        $definition = $this->innerFactory->update($context);

        return $this->addAdditionValidators($definition);
    }

    private function addAdditionValidators(DataValidationDefinition $definition): DataValidationDefinition
    {
        $definition->add('company', new NotBlank());
        $definition->add('privacy', new NotBlank());

        return $definition;
    }
}
