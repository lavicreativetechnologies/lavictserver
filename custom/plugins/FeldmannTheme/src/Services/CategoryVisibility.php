<?php declare(strict_types=1);

namespace FeldmannTheme\Services;

use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CategoryVisibility
{
    private const BOTH = 'both';
    private const ONLY_VISITOR = 'visitor';
    private const ONLY_USER = 'user';

    public function isVisibleForUser(CategoryEntity $category, SalesChannelContext $salesChannelContext): bool
    {
        $customFields = $category->getCustomFields() ?: [];
        if (!array_key_exists('custom_category_fields_navigation_user_visibility', $customFields)) {
            return true;
        }

        $userVisibility = $customFields['custom_category_fields_navigation_user_visibility'];
        if (!$userVisibility || $userVisibility === self::BOTH) {
            return true;
        }

        $customer = $salesChannelContext->getCustomer();
        if ($userVisibility === self::ONLY_USER && $customer) {
            return true;
        }

        if ($userVisibility === self::ONLY_VISITOR && !$customer) {
            return true;
        }

        return false;
    }
}
