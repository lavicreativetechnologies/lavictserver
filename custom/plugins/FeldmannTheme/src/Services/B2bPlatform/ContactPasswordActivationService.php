<?php declare(strict_types=1);

namespace FeldmannTheme\Services\B2bPlatform;

use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationEntity;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationServiceInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\System\SystemConfig\SystemConfigService;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class ContactPasswordActivationService implements ContactPasswordActivationServiceInterface
{
    private ContactPasswordActivationServiceInterface $contactPasswordActivationServiceInner;

    private SystemConfigService $systemConfigService;

    private ContextProvider $contextProvider;

    public function __construct(
        ContactPasswordActivationServiceInterface $contactPasswordActivationServiceInner,
        SystemConfigService $systemConfigService,
        ContextProvider $contextProvider
    ) {
        $this->contactPasswordActivationServiceInner = $contactPasswordActivationServiceInner;
        $this->systemConfigService = $systemConfigService;
        $this->contextProvider = $contextProvider;
    }

    public function sendPasswordActivationEmail(ContactEntity $contact): void
    {
        $salesChannelId = $this->contextProvider->getSalesChannelContext()->getSalesChannel()->getId();
        $contact->email = $this->systemConfigService->get('core.basicInformation.email', $salesChannelId);

        $this->contactPasswordActivationServiceInner->sendPasswordActivationEmail($contact);
    }

    public function getValidActivationByHash(string $hash): ?ContactPasswordActivationEntity
    {
        return $this->contactPasswordActivationServiceInner->getValidActivationByHash($hash);
    }

    public function removeActivation(ContactPasswordActivationEntity $activationEntity): ContactPasswordActivationEntity
    {
        return $this->contactPasswordActivationServiceInner->removeActivation($activationEntity);
    }

    public function cleanActivations()
    {
        $this->contactPasswordActivationServiceInner->cleanActivations();
    }
}
