<?php declare(strict_types=1);

namespace FeldmannTheme;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Shopware\Storefront\Framework\ThemeInterface;
use Swag\EnterpriseSearch\SwagEnterpriseSearch;
use SwagB2bPlatform\SwagB2bPlatform;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class FeldmannTheme extends Plugin implements ThemeInterface
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/Resources/config'));

        $activeBundles = $container->getParameter('kernel.bundles');
        if (array_key_exists('Elasticsearch', $activeBundles)) {
            // Adds elastic search ext
            $loader->load('elastic_search.xml');
        }

        // Adds b2b suite ext
        $activePlugins = $container->getParameter('kernel.active_plugins');
        if (isset($activePlugins[SwagB2bPlatform::class])) {
            $loader->load('b2b_suite.xml');
        }

        if (isset($activePlugins[SwagEnterpriseSearch::class])) {
            $loader->load('enterprise_search.xml');
        }

        if ($container->hasParameter('elasticsearch.index.config')) {
            // Update elastic search settings
            $config = $container->getParameter('elasticsearch.index.config');
            $config['settings']['index']['mapping.total_fields.limit'] = 100000;
            $container->setParameter('elasticsearch.index.config', $config);
        }
    }

    public function update(UpdateContext $updateContext): void
    {
        if (version_compare('1.0.6', $updateContext->getCurrentPluginVersion(), '>')) {
            $customFieldSetRepository = $this->container->get('custom_field_set.repository');
            $customFieldSetRepository->upsert($this->getCustomFieldSet(), $updateContext->getContext());

            /** @var EntityRepositoryInterface $customFieldRepository */
            $customFieldRepository = $this->container->get('custom_field.repository');
            $customFieldRepository->upsert($this->getCustomFields(), $updateContext->getContext());
        }
    }

    public function uninstall(Plugin\Context\UninstallContext $context): void
    {
        /** @var EntityRepositoryInterface $customFieldRepository */
        $customFieldRepository = $this->container->get('custom_field.repository');
        $customFieldRepository->delete([['id' => 'd600d28fb0234931924678cae9faaa25']], $context->getContext());
    }

    protected function getCustomFieldSet(): array
    {
        return [
            [
                'id' => 'd600d28fb0234931924678cae9faaa25',
                'name' => 'isento_product',
                'config' => [
                    'label' => [
                        'de-DE' => 'Isento Produkt',
                        'en-GB' => 'Isento Product',
                    ],
                ],
                'relations' => [
                    [
                        'id' => '2818b8c554934373b5adad1e1e45946f',
                        'entityName' => 'product',
                    ],
                ]
            ]
        ];
    }

    protected function getCustomFields(): array
    {
        return [
            [
                'id' => '7f68cdbed4a3421e93cb571d080e72ea',
                'name' => 'ies_shipping_by_spedition',
                'type' => CustomFieldTypes::TEXT,
                'active' => true,
                'config' => [
                    'translated' => true,
                    'label' => [
                        'en-GB' => 'Shipping by Spedition',
                        'de-DE' => 'Versand via Spedition',
                    ]
                ],
                'customFieldSetId' => 'd600d28fb0234931924678cae9faaa25'
            ],
        ];
    }
}
