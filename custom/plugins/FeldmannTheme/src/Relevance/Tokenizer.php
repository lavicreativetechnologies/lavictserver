<?php declare(strict_types=1);

namespace FeldmannTheme\Relevance;

use Swag\EnterpriseSearch\Relevance\Tokenizer as ElasticTokenizer;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class Tokenizer extends ElasticTokenizer
{
    public function tokenize(string $term): array
    {
        $term = mb_strtolower(html_entity_decode($term), 'UTF-8');
        $term = trim(preg_replace("/[^\pL_\-0-9]/u", ' ', $term));
        $term = str_replace('<', ' <', $term);
        $term = strip_tags($term);

        $tags = explode(' ', $term);

        $tags = array_filter($tags);

        return array_unique($tags);
    }
}
