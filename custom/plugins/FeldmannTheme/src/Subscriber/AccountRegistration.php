<?php declare(strict_types=1);

namespace FeldmannTheme\Subscriber;

use Shopware\Core\Framework\Validation\BuildValidationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AccountRegistration implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'framework.validation.customer.create' => 'onCustomerDataValidation',
        ];
    }

    public function onCustomerDataValidation(BuildValidationEvent $event): void
    {
        $event->getDefinition()->add('privacy', new NotBlank());
    }
}
