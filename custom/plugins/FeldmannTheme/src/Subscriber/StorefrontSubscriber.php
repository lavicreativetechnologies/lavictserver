<?php declare(strict_types=1);

namespace FeldmannTheme\Subscriber;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Storefront\Event\StorefrontRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class StorefrontSubscriber implements EventSubscriberInterface
{
    private const SHOW_PRICES = 'showPrice';

    public static function getSubscribedEvents(): array
    {
        return [
            StorefrontRenderEvent::class => 'onStorefrontRender',
        ];
    }

    public function onStorefrontRender(StorefrontRenderEvent $event): void
    {
        $context = $event->getSalesChannelContext();
        $customer = $context->getCustomer();

        $showPrices = $this->isValidCustomerGroup($customer);

        $event->setParameter(static::SHOW_PRICES, $showPrices);
    }

    protected function isValidCustomerGroup(?CustomerEntity $customer): bool
    {
        if ($customer === null || $customer->getGuest()) {
            return false;
        }

        // @todo: add customer group check if needed

        return true;
    }
}
