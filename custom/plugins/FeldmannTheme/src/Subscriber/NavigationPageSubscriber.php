<?php declare(strict_types=1);

namespace FeldmannTheme\Subscriber;

use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Content\Category\Tree\Tree;
use Shopware\Storefront\Page\Navigation\NavigationPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class NavigationPageSubscriber implements EventSubscriberInterface
{
    public const NAVIGATION_FLAG_IS_SALE = 'is-sale';

    public const NAVIGATION_FLAG_IS_CMS_LINK = 'is-cms-link';

    public const NAVIGATION_FLAG_IS_CMS_MAIN_LINK = 'is-cms-main-link';

    public static function getSubscribedEvents(): array
    {
        return [
            NavigationPageLoadedEvent::class => 'onNavigationPageLoaded',
        ];
    }

    public function onNavigationPageLoaded(NavigationPageLoadedEvent $event): void
    {
        if (!$navigationId = $event->getRequest()->get('navigationId')) {
            return;
        }

        $page = $event->getPage();

        $navigation = $page->getHeader()->getNavigation();
        if ($this->isPageFooter($navigation, $navigationId)) {
            $navigation = $page->getFooter()->getNavigation();
        }

        $activeCategory = $navigation->getActive();
        $subCategories = $this->loadSubCategories($navigation, $activeCategory);

        $event->getPage()->setExtensions([
            'subCategories' => $subCategories
        ]);
    }

    protected function loadSubCategories(Tree $tree, CategoryEntity $categoryEntity): array
    {
        $children = $tree->getChildren($categoryEntity->getId());

        if ($children->getActive()->getChildCount() <= 0) {
            $children = $tree->getChildren($categoryEntity->getParentId());
        }

        if ($children === null) {
            return [];
        }

        return $children->getTree();
    }

    private function isPageFooter(Tree $position, ?string $navigationId): bool
    {
        $children = $position->getChildren($navigationId);
        return $children === null;
    }
}
