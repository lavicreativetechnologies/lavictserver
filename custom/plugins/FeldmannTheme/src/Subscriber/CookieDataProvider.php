<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace FeldmannTheme\Subscriber;

use Shopware\Storefront\Event\StorefrontRenderEvent;
use Shopware\Storefront\Framework\Cookie\CookieProviderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class CookieDataProvider implements EventSubscriberInterface
{
    private $cookieProvider;

    public function __construct(CookieProviderInterface $cookieProvider)
    {
        $this->cookieProvider = $cookieProvider;
    }

    public static function getSubscribedEvents()
    {
        return [StorefrontRenderEvent::class => 'onRequest'];
    }

    public function onRequest(StorefrontRenderEvent $event)
    {
        $isAjax = $event->getRequest()->isXmlHttpRequest();
        if ($isAjax) {
            return;
        }

        $event->setParameter('cookieGroups', $this->cookieProvider->getCookieGroups());
    }
}
