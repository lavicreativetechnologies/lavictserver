<?php declare(strict_types=1);

namespace FeldmannTheme\Subscriber;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\Product\Events\ProductSearchCriteriaEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class ListingSubscriber implements EventSubscriberInterface
{
    private const DEFAULT_LISTING_LIMIT = 24;

    public static function getSubscribedEvents(): array
    {
        return [
            ProductListingCriteriaEvent::class => 'onProductListingCriteria',
            ProductSearchCriteriaEvent::class => 'onProductListingCriteria',
        ];
    }

    public function onProductListingCriteria(ProductListingCriteriaEvent $event): void
    {
        $request = $event->getRequest();
        $session = $request->getSession();

        $defaultLimit = $session->get('listingLimit', static::DEFAULT_LISTING_LIMIT);
        $limit = $request->get('limit', $defaultLimit);

        $session->set('listingLimit', $limit);

        $event->getRequest()->query->set('limit', $limit);
    }
}
