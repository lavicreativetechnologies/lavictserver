<?php declare(strict_types=1);

namespace FeldmannTheme\Subscriber;

use FeldmannTheme\Event\OrderEmailToShopManagerEvent;
use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CheckoutOrderSubscriber implements EventSubscriberInterface
{
    private $eventDispatcher;

    private $systemConfigService;

    public function __construct(EventDispatcherInterface $eventDispatcher, SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
        $this->eventDispatcher = $eventDispatcher;
    }

    public static function getSubscribedEvents()
    {
        return [
            CheckoutOrderPlacedEvent::class => 'onCheckoutOrderPlaced',
        ];
    }

    public function onCheckoutOrderPlaced(CheckoutOrderPlacedEvent $event)
    {
        $salesChannelId = $event->getSalesChannelId();
        $recipient = $this->systemConfigService->get('core.basicInformation.email', $salesChannelId);
        $recipientStruct = new MailRecipientStruct([$recipient => $recipient]);
        $orderEmailToShopManagerEvent = new OrderEmailToShopManagerEvent(
            $event->getContext(),
            $event->getOrder(),
            $salesChannelId,
            $recipientStruct
        );

        $this->eventDispatcher->dispatch($orderEmailToShopManagerEvent);
    }
}
