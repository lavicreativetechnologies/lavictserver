<?php declare(strict_types=1);

namespace FeldmannTheme\Subscriber;

use Shopware\Core\Checkout\Document\Aggregate\DocumentType\DocumentTypeEntity;
use Shopware\Core\Checkout\Document\DocumentCollection;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Storefront\Page\Account\Order\AccountOrderPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class AccountOrderSubscriber implements EventSubscriberInterface
{
    private $documentRepository;

    private $documentTypeRepository;

    public function __construct(
        EntityRepositoryInterface $documentRepository,
        EntityRepositoryInterface $documentTypeRepository
    ) {
        $this->documentRepository = $documentRepository;
        $this->documentTypeRepository = $documentTypeRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AccountOrderPageLoadedEvent::class => 'onOrderPageLoaded',
        ];
    }

    public function onOrderPageLoaded(AccountOrderPageLoadedEvent $event): void
    {
        $page = $event->getPage();
        $context = $event->getContext();

        $documentType = $this->getDocumentTypeByName('invoice', $context);
        $documentTypeId = $documentType->getId();

        $orders = $page->getOrders();

        /** @var OrderEntity $order */
        foreach ($orders as $order) {
            /** @var DocumentCollection $documents */
            $documents = $this->getDocuments($documentTypeId, $order->getId(), $context);

            $order->setDocuments($documents);
        }

        $event->getPage()->setOrders($orders);
    }

    protected function getDocuments(string $documentTypeId, string $orderId, Context $context): EntityCollection
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('documentTypeId', $documentTypeId))
            ->addFilter(new EqualsFilter('orderId', $orderId));

        return $this->documentRepository->search($criteria, $context)->getEntities();
    }

    private function getDocumentTypeByName(string $documentType, Context $context): ?DocumentTypeEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('technicalName', $documentType));

        return $this->documentTypeRepository->search($criteria, $context)->first();
    }
}
