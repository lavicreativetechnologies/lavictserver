import StickyHeaderPlugin from './script/plugin/sticky-header.plugin';
import ListingLimitPlugin from './script/plugin/listing/listing-limit.plugin';
import FilterToggle from './script/plugin/listing/filter-toogle.plugin';
import ShareActions from './script/plugin/detail/share-actions.plugin';
import CmsLinkScrollPlugin from './script/plugin/main-menu/cms-link-scroll';
import OffcanvasMenuExtensionPlugin from './script/plugin/main-menu/offcanvas-menu.plugin';
import CmsGlobalScrollPlugin from './script/plugin/cms-global-scroll';
import CookiePlugin from './script/plugin/cookie.plugin';
import ListingPaginationExtensionPlugin from './script/plugin/listing/listing-pagination.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('StickyHeader', StickyHeaderPlugin, document);
PluginManager.register('ListingLimit', ListingLimitPlugin, '[data-listing-limit]');
PluginManager.register('FilterToggle', FilterToggle, '[data-filter-toggle]');
PluginManager.register('ShareActions', ShareActions, '[data-share-actions]');
PluginManager.register('CmsLinkScroll', CmsLinkScrollPlugin, '[data-cms-link-scroll]');
PluginManager.register('CmsGlobalScroll', CmsGlobalScrollPlugin, document);
PluginManager.register('IesCookiePlugin', CookiePlugin, '[data-ies-cookie-plugin]');

PluginManager.override('ListingPagination', ListingPaginationExtensionPlugin, '[data-listing-pagination]');
PluginManager.override('OffcanvasMenu', OffcanvasMenuExtensionPlugin, '[data-offcanvas-menu]');

// Necessary for the webpack hot module reloading server
if (module.hot) {
    module.hot.accept();
}
