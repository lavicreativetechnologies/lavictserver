import Plugin from 'src/plugin-system/plugin.class';

export default class FilterTogglePlugin extends Plugin {
    static options = {
        toggleClass: 'filter-toggle-btn'
    };

    init() {
        this.trigger = document.getElementsByClassName(this.options.toggleClass);
        this._registerEvents();
    }

    _registerEvents() {
                console.log(this.trigger.length);

        for (let i = 0; i < this.trigger.length; i++) {
            this.trigger[i].addEventListener('click', this.onClick.bind(this));
        }
    }

    onClick() {
        for (let i = 0; i < this.trigger.length; i++) {
            this.trigger[i].classList.toggle('active');
        }

        this.el.classList.toggle('active');
    }
}
