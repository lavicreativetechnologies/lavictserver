import Plugin from 'src/plugin-system/plugin.class';

export default class StickyHeaderPlugin extends Plugin {

    static options = {
        offsetTriggerSticky: 200,

        stickyClassName: 'shrink'
    };

    init() {
        this._registerEvents();
    }

    _registerEvents() {
        document.addEventListener('scroll', () => {
            if (window.pageYOffset > this.options.offsetTriggerSticky) {
                this._addSticky();
            } else {
                this._removeSticky()
            }
        });
    }

    _addSticky() {
        if (this._hasSticky()) {
            return;
        }

        document.body.classList.add(this.options.stickyClassName);
    }

    _removeSticky() {
        if (!this._hasSticky()) {
            return;
        }

        document.body.classList.remove(this.options.stickyClassName);
    }

    _hasSticky() {
        return document.body.classList.contains(this.options.stickyClassName);
    }
}
