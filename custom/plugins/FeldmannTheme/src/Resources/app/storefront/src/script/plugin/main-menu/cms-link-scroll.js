import Plugin from 'src/plugin-system/plugin.class';
import Scroller from '../../helper/scroller.helper';
import DomAccess from 'src/helper/dom-access.helper';
import Iterator from 'src/helper/iterator.helper';
import OffCanvas from 'src/plugin/offcanvas/offcanvas.plugin';

export default class CmsLinkScrollPlugin extends Plugin {

    static options = {
        isOffcanvas: false,
        hasRedirect: '',
        redirectUrl: '',
        cmsSelector: '',
        flyoutActiveCls: 'is-open',
        flyoutIdDataAttribute: 'data-flyout-menu-id',
        triggerDataAttribute: 'data-flyout-menu-trigger'
    };

    init() {
        this._registerEvents();
    }

    _registerEvents() {
        this.el.addEventListener('click', this._onClick.bind(this));
    }

    _onClick(event) {
        event.preventDefault();

        const cmsSelector = this._sanitizeCssName(this.options.cmsSelector);

        if (this.options.hasRedirect) {
            const nextUrl = this.options.redirectUrl;
            const currentUrl = window.location.href.replace(window.location.hash, '');

            window.location.href = nextUrl + '#' + cmsSelector;

            if (this.options.isOffcanvas && nextUrl === currentUrl) {
                window.location.reload();
            }

            return false;
        }

        if (this.options.isOffcanvas) {
            OffCanvas.close();
        } else {
            this._closeAllFlyouts();
        }

        Scroller.scrollToElement(cmsSelector);
    }

    _sanitizeCssName(name) {
        let selector = name.split(' ').join('_');
        selector = selector.replace(/&/g, 'und');
        selector = selector.replace(/ä/g, 'ae');
        selector = selector.replace(/ö/g, 'oe');
        selector = selector.replace(/ü/g, 'ue');
        selector = selector.replace(/Ä/g, 'Ae');
        selector = selector.replace(/Ö/g, 'Oe');
        selector = selector.replace(/Ü/g, 'Ue');
        selector = selector.replace(/ß/g, 'ss');

        return selector;
    }

    _closeAllFlyouts() {
        const flyoutEls = document.querySelectorAll(`[${this.options.flyoutIdDataAttribute}]`);

        Iterator.iterate(flyoutEls, el => {
            const triggerEl = this._retrieveTriggerEl(el);
            this._closeFlyout(el, triggerEl);
        });
    }

    _closeFlyout(flyoutEl, triggerEl) {
        if (this._isOpen(triggerEl)) {
            flyoutEl.classList.remove(this.options.flyoutActiveCls);
            triggerEl.classList.remove(this.options.flyoutActiveCls);
        }
    }

    _retrieveTriggerEl(el) {
        const flyoutId = DomAccess.getDataAttribute(el, this.options.flyoutIdDataAttribute, false);
        return document.querySelector(`[${this.options.triggerDataAttribute}='${flyoutId}']`);
    }

    _isOpen(el) {
        return el.classList.contains(this.options.flyoutActiveCls);
    }
}
