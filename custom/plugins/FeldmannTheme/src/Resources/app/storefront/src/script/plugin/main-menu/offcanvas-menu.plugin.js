import OffcanvasMenuPlugin from 'src/plugin/main-menu/offcanvas-menu.plugin';

export default class OffcanvasMenuExtensionPlugin extends OffcanvasMenuPlugin {
    _registerEvents () {
        window.PluginManager.initializePlugin('CmsLinkScroll', '[data-cms-link-scroll]');

        super._registerEvents();
    }
}
