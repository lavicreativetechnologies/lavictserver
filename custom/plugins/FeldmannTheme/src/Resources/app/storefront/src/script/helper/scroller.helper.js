export default class Scroller {

    static options = {
        fixedHeaderSelector: '.header-main'
    };

    /**
     * @todo: refactor this! this is not a helper.
     * @param selector
     */
    static scrollToElement(selector) {
        const cms = document.querySelectorAll('.is-cms-' + selector);
        const target = cms[0];
        if (!target) {
            return;
        }

        const fixedHeader = document.querySelector(this.options.fixedHeaderSelector);
        const rect = target.getBoundingClientRect();
        let offset = rect.top + window.scrollY;
        if (fixedHeader) {
            const headerRect = fixedHeader.getBoundingClientRect();
            offset -= headerRect.height;
        }

        window.scrollTo({
            top: offset,
            behavior: 'smooth'
        });
    }
}
