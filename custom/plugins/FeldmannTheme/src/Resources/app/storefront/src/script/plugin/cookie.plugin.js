import Plugin from 'src/plugin-system/plugin.class';
import CookieStorage from 'src/helper/storage/cookie-storage.helper';

export default class CookiePlugin extends Plugin {
    static options = {
        /**
         * Button selector to accept all cookies at once
         */
        acceptAllButtonSelector: '.js-cookie-accept-all-button'
    };

    init() {
        this._acceptAllButton = this.el.querySelector(this.options.acceptAllButtonSelector);
        this.cookieConfigurationPlugin = this._getCookieConfigurationPlugin();

        this._registerEvents(this.el);
    }

    _registerEvents() {
        const { submitEvent, buttonSubmitSelector, wrapperToggleSelector } = this.cookieConfigurationPlugin.options;

        if (this._acceptAllButton) {
            this._acceptAllButton.addEventListener(submitEvent, this.acceptAllCookies.bind(this));
        }

        const button = this.el.querySelector(buttonSubmitSelector);
        if (button) {
            button.addEventListener(submitEvent, this._handleSubmit.bind(this));
        }

        const wrapperTrigger = Array.from(this.el.querySelectorAll(wrapperToggleSelector));
        wrapperTrigger.forEach(trigger => {
            trigger.addEventListener(submitEvent, this.cookieConfigurationPlugin._handleWrapperTrigger.bind(this.cookieConfigurationPlugin));
        });

        const checkboxes = Array.from(this.el.querySelectorAll('input[type="checkbox"]'));
        checkboxes.forEach(checkbox => {
            checkbox.addEventListener(submitEvent, this.cookieConfigurationPlugin._handleCheckbox.bind(this.cookieConfigurationPlugin));
        });
    }

    /**
     * This is almost the same as in cookie-configuration.plugin, just without the offcanvas
     *
     * Get cookies passed to the configuration template
     * Can be filtered by "all", "active" or "inactive"
     *
     * Always excludes "required" cookies, since they are assumed to be set separately.
     *
     * @param type
     * @returns {Array}
     * @private
     */
    _getCookies(type = 'all') {
        const { cookieSelector } = this.cookieConfigurationPlugin.options;

        return Array.from(this.el.querySelectorAll(cookieSelector)).filter(cookieInput => {
            switch (type) {
                case 'all': return true;
                case 'active': return this.cookieConfigurationPlugin._isChecked(cookieInput);
                case 'inactive': return !this.cookieConfigurationPlugin._isChecked(cookieInput);
                default: return false;
            }
        }).map(filteredInput => {
            const { cookie, cookieValue, cookieExpiration, cookieRequired } = filteredInput.dataset;
            return { cookie, value: cookieValue, expiration: cookieExpiration, required: cookieRequired };
        });
    }

    _getCookieConfigurationPlugin() {
        const cookieConfigurationPlugins = window.PluginManager.getPluginInstances(
            'CookieConfiguration'
        );

        if (!cookieConfigurationPlugins) {
            console.error('CookieConfiguration is missing!');
            return null;
        }

        return cookieConfigurationPlugins[0];
    }

    /**
     * Event handler for the 'Save' button inside the offCanvas
     *
     * Removes unselected cookies, if already set
     * Sets or refreshes selected cookies
     *
     * @private
     */
    _handleSubmit() {
        const activeCookies = this._getCookies('active');
        const inactiveCookies = this._getCookies('inactive');
        const inactiveCookieNames = [];

        inactiveCookies.forEach(({ cookie }) => {
            inactiveCookieNames.push(cookie);

            if (CookieStorage.getItem(cookie)) {
                CookieStorage.removeItem(cookie);
            }
        });


        const activeCookieNames = this._activateCookies(activeCookies);

        this.cookieConfigurationPlugin._handleUpdateListener(activeCookieNames, inactiveCookieNames);
        this.cookieConfigurationPlugin._hideCookieBar();
    }

    acceptAllCookies() {
        const allCookies = this._getCookies('all');
        const cookieNames = this._activateCookies(allCookies);

        this.cookieConfigurationPlugin._handleUpdateListener(cookieNames, []);
        this.cookieConfigurationPlugin._hideCookieBar();
    }

    _activateCookies(cookies) {
        const { cookiePreference } = this.cookieConfigurationPlugin.options;
        const cookieNames = []

        cookies.forEach(({ cookie, value, expiration }) => {
            cookieNames.push(cookie);

            if (cookie && value) {
                CookieStorage.setItem(cookie, value, expiration);
            }
        });

        CookieStorage.setItem(cookiePreference, '1', '30');

        return cookieNames;
    }
}
