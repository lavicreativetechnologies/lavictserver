import FilterBasePlugin from 'src/plugin/listing/filter-base.plugin';
import DomAccess from 'src/helper/dom-access.helper';
import deepmerge from 'deepmerge';

export default class ListingLimitPlugin extends FilterBasePlugin {

    static options = deepmerge(FilterBasePlugin.options, {
        limit: null
    });

    init() {
        this.select = this.el.querySelector('select');

        this._registerEvents();
    }

    _registerEvents() {
        this.select.addEventListener('change', this.onChangeLimit.bind(this));
    }

    onChangeLimit(event) {
        this.options.limit = event.target.value;
        this.listing.changeListing();

        this.scrollToTop();
    }

    scrollToTop() {
        const parentFilterPanelSelector = DomAccess.querySelector(document, this.options.parentFilterPanelSelector);

        const rect = parentFilterPanelSelector.getBoundingClientRect();
        const offset = rect.top + window.scrollY - 140;

        window.scrollTo({
            top: offset,
            behavior: 'smooth'
        });
    }

    reset() {}

    resetAll() {}

    /**
     * @return {Object}
     */
    getValues() {
        if (this.options.limit === null) {
            return {};
        }

        return { limit: this.options.limit };
    }

    afterContentChange() {
        this.listing.deregisterFilter(this);
    }

    /**
     * @return {Array}
     */
    getLabels() {
        return [];
    }
}
