import Plugin from 'src/plugin-system/plugin.class';

export default class ShareActions extends Plugin {

    static options = {
        shareActionsContainerSelector: '.share-action-container'
    };

    init() {
        this._initElements();
        this._registerEvents();
    }

    _initElements() {
        this._shareActionsContainer = document.querySelector(this.options.shareActionsContainerSelector);
        this._twitterInit = false;
    }

    _registerEvents() {
        this.el.addEventListener('click', this._onClick.bind(this));
    }

    _onClick() {
        this._toggleShareActions(this._shareActionsContainer);
        this._initTwitter();
    }

    _toggleShareActions(node) {
        if (node.classList.contains('d-none')) {
            node.classList.remove('d-none');
        } else {
            node.classList.add('d-none');
        }
    }

    _initTwitter() {
        if (this._shareActionsContainer.classList.contains('d-none')) {
            return;
        }

        if (this._twitterInit) {
            return;
        }

        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://platform.twitter.com/widgets.js';

        document.getElementsByTagName('head')[0].appendChild(script);
        this._twitterInit = true;
    }
}
