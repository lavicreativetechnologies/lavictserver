import ListingPaginationBasePlugin from 'src/plugin/listing/listing-pagination.plugin';

export default class ListingPaginationPlugin extends ListingPaginationBasePlugin {
    onChangePage(event) {
        super.onChangePage(event);

        this.scrollToTop();
    }

    scrollToTop() {
        const rect = this.el.getBoundingClientRect();
        const offset = rect.top + window.scrollY - 140;

        window.scrollTo({
            top: offset,
            behavior: 'smooth'
        });
    }
}
