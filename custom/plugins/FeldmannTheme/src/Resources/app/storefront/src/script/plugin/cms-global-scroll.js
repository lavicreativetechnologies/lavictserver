import Plugin from 'src/plugin-system/plugin.class';
import Scroller from '../helper/scroller.helper';
import Debouncer from 'src/helper/debouncer.helper';

export default class CmsGlobalScrollPlugin extends Plugin {
    init() {
        window.addEventListener('load', Debouncer.debounce(this._scroll.bind(this), 1));
    }

    _scroll() {
        const locationHash = window.location.hash;
        if (!locationHash) {
            return;
        }

        Scroller.scrollToElement(locationHash.substr(1));
    }
}
