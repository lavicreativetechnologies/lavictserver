import template from './sw-cms-block-feldmann-sub-categories.html.twig';
import './sw-cms-block-feldmann-sub-categories.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-feldmann-sub-categories', {
    template
});
