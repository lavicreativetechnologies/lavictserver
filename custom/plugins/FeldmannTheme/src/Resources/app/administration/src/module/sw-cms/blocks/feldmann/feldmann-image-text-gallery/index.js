import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'feldmann-image-text-gallery',
    label: 'Image text gallery',
    category: 'feldmann',
    component: 'sw-cms-block-feldmann-image-text-gallery',
    previewComponent: 'sw-cms-preview-feldmann-image-text-gallery',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        'left-image': {
            type: 'image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' }
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_camera_large.jpg'
                    }
                }
            }
        },
        'left-text': {
            type: 'text',
            default: {
                config: {
                    content: {
                        source: 'static',
                        value: `
                            <span style="font-size: 20px; font-weight: bold; color: #fff;">Lorem Ipsum dolor</span>
                        `.trim()
                    }
                }
            }
        },
        'center-image': {
            type: 'image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' }
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_plant_large.jpg'
                    }
                }
            }
        },
        'center-text': {
            type: 'text',
            default: {
                config: {
                    content: {
                        source: 'static',
                        value: `
                            <span style="font-size: 20px; font-weight: bold; color: #fff;">Lorem Ipsum dolor</span>
                        `.trim()
                    }
                }
            }
        },
        'right-image': {
            type: 'image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' }
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_glasses_large.jpg'
                    }
                }
            }
        },
        'right-text': {
            type: 'text',
            default: {
                config: {
                    content: {
                        source: 'static',
                        value: `
                            <span style="font-size: 20px; font-weight: bold; color: #fff;">Lorem Ipsum dolor</span>
                        `.trim()
                    }
                }
            }
        }
    }
});
