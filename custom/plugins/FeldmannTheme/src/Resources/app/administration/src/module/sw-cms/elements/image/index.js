import './component';
import './config';

const { Service } = Shopware;

let config = Service('cmsService').getCmsElementConfigByName('image');

config.defaultConfig.textActive = {
    source: 'static',
    value: false
};

config.defaultConfig.text = {
    source: 'static',
    value: `
        <h3>Lorem ipsum</h3>
        <p>Lorem Ipsum dolor amet!</p>
    `.trim()
};

config.defaultConfig.horizontalAlign = {
    source: 'static',
    value: null
};

config.defaultConfig.maxWidth = {
    source: 'static',
    value: null
};

config.defaultConfig.lightbox = {
    source: 'static',
    value: false
};

config.defaultConfig.lightboxText = {
    source: 'static',
    value: ''
};
