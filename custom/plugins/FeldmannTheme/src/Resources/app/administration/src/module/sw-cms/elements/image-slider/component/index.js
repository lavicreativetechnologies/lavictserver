import template from './sw-cms-el-image-slider.html.twig';
import './sw-cms-el-image-slider.scss';

const { Component } = Shopware;

Component.override('sw-cms-el-image-slider', {
    template
});
