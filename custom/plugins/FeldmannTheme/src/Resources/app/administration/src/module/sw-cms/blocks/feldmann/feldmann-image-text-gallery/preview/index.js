import template from './sw-cms-preview-feldmann-image-text-gallery.html.twig';
import './sw-cms-preview-feldmann-image-text-gallery.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-feldmann-image-text-gallery', {
    template
});
