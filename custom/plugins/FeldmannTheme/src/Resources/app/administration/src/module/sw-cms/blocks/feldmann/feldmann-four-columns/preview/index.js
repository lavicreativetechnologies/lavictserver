import template from './sw-cms-preview-feldmann-four-columns.html.twig';
import './sw-cms-preview-feldmann-four-columns.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-feldmann-four-columns', {
    template
});
