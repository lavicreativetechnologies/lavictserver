import './component';
import './preview';

const imageConfig = {
    type: 'image',
    default: {
        config: {
            displayMode: { source: 'static', value: 'standard' }
        },
        data: {
            media: {
                url: '/administration/static/img/cms/preview_glasses_small.jpg'
            }
        }
    }
};

const textConfig = {
    type: 'text',
    default: {
        config: {
            content: {
                source: 'static',
                value: '<p>Lorem ipsum</p>'
            }
        }
    }
};

Shopware.Service('cmsService').registerCmsBlock({
    name: 'feldmann-six-columns',
    label: 'Six columns',
    category: 'feldmann',
    component: 'sw-cms-block-feldmann-six-columns',
    previewComponent: 'sw-cms-preview-feldmann-six-columns',
    defaultConfig: {
        marginBottom: '10px',
        marginTop: '10px',
        marginLeft: '10px',
        marginRight: '10px',
        sizingMode: 'boxed'
    },
    slots: {
        colOneImage: imageConfig,
        colOneText: textConfig,

        colTwoImage: imageConfig,
        colTwoText: textConfig,

        colThreeImage: imageConfig,
        colThreeText: textConfig,

        colFourImage: imageConfig,
        colFourText: textConfig,

        colFiveImage: imageConfig,
        colFiveText: textConfig,

        colSixImage: imageConfig,
        colSixText: textConfig
    }
});
