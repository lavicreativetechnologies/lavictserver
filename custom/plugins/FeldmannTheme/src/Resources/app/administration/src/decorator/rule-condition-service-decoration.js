Shopware.Application.addServiceProviderDecorator('ruleConditionDataProviderService', (ruleConditionService) => {
    ruleConditionService.addCondition('single_position_attribute', {
        component: 'sw-condition-line-item-custom-field',
        label: 'Einzelne Position mit Attribut',
        scopes: ['lineItem']
    });

    ruleConditionService.addCondition('cartLineItemProductCustomField', {
        component: 'sw-condition-line-item-custom-field',
        label: 'Produkt Positionen mit Attribut',
        scopes: ['lineItem']
    });

    return ruleConditionService;
});
