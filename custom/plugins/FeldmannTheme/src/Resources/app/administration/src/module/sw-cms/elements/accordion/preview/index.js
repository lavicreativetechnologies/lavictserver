import template from './sw-cms-el-preview-accordion.html.twig';
import './sw-cms-al-preview-accordion.scss';

Shopware.Component.register('sw-cms-el-preview-accordion', {
    template
});
