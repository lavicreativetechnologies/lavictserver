import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'accordion-video',
    label: 'sw-cms.elements.accordion-video.label',
    component: 'sw-cms-el-accordion-video',
    configComponent: 'sw-cms-el-config-accordion-video',
    previewComponent: 'sw-cms-el-preview-accordion-video',

    defaultConfig: {
        accordionVideoItems: {
            source: 'static',
            value: []
        },
        accordionTitle : {
            source: 'static',
            value: ''
        }
    }
});
