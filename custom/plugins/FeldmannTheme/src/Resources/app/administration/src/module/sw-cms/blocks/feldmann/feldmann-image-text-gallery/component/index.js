import template from './sw-cms-block-feldmann-image-text-gallery.html.twig';
import './sw-cms-block-feldmann-image-text-gallery.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-feldmann-image-text-gallery', {
    template
});
