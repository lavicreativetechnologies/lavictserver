import template from './sw-cms-el-accordion.html.twig';
import './sw-cms-el-accordion.scss';

const {Component, Mixin} = Shopware;

Component.register('sw-cms-el-accordion', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        accordionItems() {
            if (this.element.config && this.element.config.accordionItems && this.element.config.accordionItems.value && this.element.config.accordionItems.value.length > 0) {
                return this.element.config.accordionItems.value;
            }

            if (this.element.data && this.element.data.accordionItems && this.element.data.accordionItems.value && this.element.data.accordionItems.value.length > 0) {
                return this.element.data.accordionItems.value;
            }

            return [
                {title: 'Accordion 1', content: 'Accordionpaket'}
            ];
        }
    },

    watch: {
        'element.data.accordionItems': {
            handler() {
                console.log('watch');
                console.log(this.accordionItems.value);
            },
            deep: true
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('accordion');
            this.initElementData('accordion');
        }
    }
});
