import template from './sw-cms-preview-feldmann-hp-grid.html.twig';
import './sw-cms-preview-feldmann-hp-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-feldmann-hp-grid', {
    template
});
