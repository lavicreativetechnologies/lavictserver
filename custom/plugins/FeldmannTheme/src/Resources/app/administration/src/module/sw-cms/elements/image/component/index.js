import template from './sw-cms-el-image.html.twig';
import './sw-cms-el-image.scss';

const { Component } = Shopware;

Component.override('sw-cms-el-image', {
    template,

    computed: {
        displayModeClass() {
            let modeClass = this.$super('displayModeClass');

            if (modeClass !== null) {
                return modeClass;
            }

            if (this.element.config.horizontalAlign.value) {
                return `is-position-${this.element.config.horizontalAlign.value}`;
            }

            return modeClass;
        },

        imgStyles() {
            const maxWidth = this.element.config.maxWidth.value;

            return {
                'align-self': !this.element.config.verticalAlign.value ? null : this.element.config.verticalAlign.value,
                'max-width': maxWidth ? maxWidth : 'auto'
            }
        }
    }
});
