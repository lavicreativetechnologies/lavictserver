// Decorator
import './decorator/rule-condition-service-decoration';

// Extensions
import './module/sw-cms/component/sw-cms-sidebar';
import './module/sw-cms/elements/image';
import './module/sw-cms/elements/image-slider';

// Blocks
import './module/sw-cms/blocks/feldmann/feldmann-hp-grid';
import './module/sw-cms/blocks/feldmann/feldmann-image-text-gallery';
import './module/sw-cms/blocks/feldmann/feldmann-four-columns';
import './module/sw-cms/blocks/feldmann/feldmann-six-columns';
import './module/sw-cms/blocks/feldmann/feldmann-sub-categories';
import './module/sw-cms/blocks/feldmann/feldmann-accordion';

// Elements
import './module/sw-cms/elements/accordion';
import './module/sw-cms/elements/accordion-video';

// Translations
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
