import template from './sw-cms-el-preview-accordion-video.html.twig';
import './sw-cms-al-preview-accordion-video.scss';

Shopware.Component.register('sw-cms-el-preview-accordion-video', {
    template
});
