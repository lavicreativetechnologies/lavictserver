import './component';
import './preview';

const imageConfig = {
    type: 'image',
    default: {
        config: {
            displayMode: { source: 'static', value: 'standard' }
        },
        data: {
            media: {
                url: '/administration/static/img/cms/preview_glasses_small.jpg'
            }
        }
    }
};

const textConfig = {
    type: 'text',
    default: {
        config: {
            content: {
                source: 'static',
                value: '<p align="center" style="margin-top: 7px; line-height: 24px;">\n' +
                    '    <b>Person</b><br>\n' +
                    '    <span style="font-size: 13px">Title</span> <br>\n' +
                    '    <span style="font-size: 13px">E-Mail: <a href="mailto:test@test.de">test@test.de</a></span>\n' +
                    '</p>'
            }
        }
    }
};

Shopware.Service('cmsService').registerCmsBlock({
    name: 'feldmann-four-columns',
    label: 'Four columns',
    category: 'feldmann',
    component: 'sw-cms-block-feldmann-four-columns',
    previewComponent: 'sw-cms-preview-feldmann-four-columns',
    defaultConfig: {
        marginBottom: '10px',
        marginTop: '10px',
        marginLeft: '10px',
        marginRight: '10px',
        sizingMode: 'boxed'
    },
    slots: {
        colOneImage: imageConfig,
        colOneText: textConfig,

        colTwoImage: imageConfig,
        colTwoText: textConfig,

        colThreeImage: imageConfig,
        colThreeText: textConfig,

        colFourImage: imageConfig,
        colFourText: textConfig
    }
});
