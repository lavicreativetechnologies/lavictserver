import template from './sw-cms-block-feldmann-four-columns.html.twig';
import './sw-cms-block-feldmann-four-columns.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-feldmann-four-columns', {
    template
});
