import template from './sw-cms-block-feldmann-accordion.html.twig';
import './sw-cms-block-feldmann-accordion.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-feldmann-accordion', {
    template
});
