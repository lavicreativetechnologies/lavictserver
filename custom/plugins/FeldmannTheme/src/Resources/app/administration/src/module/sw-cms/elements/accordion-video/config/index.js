import template from './sw-cms-el-config-accordion-video.html.twig';
import './sw-cms-el-config-accordion-video.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-accordion-video', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    data() {
        return {
            accordionVideoItems: {
                value: []
            }
        }
    },

    created() {
        this.createdComponent();
    },

    computed: {
        items() {
            if (this.element.config && this.element.config.accordionVideoItems && this.element.config.accordionVideoItems.value) {
                return this.element.config.accordionVideoItems.value;
            }

            return [this._initItem()];
        }
    },

    methods: {
        createdComponent() {
            this.initElementConfig('accordion-video');
        },

        addRow() {
            this.element.config.accordionVideoItems.value.push(this._initItem());

            this.$set(this.element.data, 'accordionVideoItems', this.element.config.accordionVideoItems);
        },

        deleteRow(index) {
            this.element.config.accordionVideoItems.value.splice(index, 1);

            this.$set(this.element.data, 'accordionVideoItems', this.element.config.accordionVideoItems);
        },

        onTitleChange(title) {
            if (title !== this.element.config.accordionTitle.value) {
                this.element.config.accordionTitle.value = title;
                this.$emit('element-update', this.element);
            }
        },

        onYoutubeIdChange(data, index) {
            let before = this.element.config.accordionVideoItems.value[index];

            this.element.config.accordionVideoItems.value[index] = this._updateItem(before.name, data);

            this.$set(this.element.data, 'accordionVideoItems.value['+index+']', this.element.config.accordionVideoItems.value[index]);
        },

        onNameChange(data, index) {
            let before = this.element.config.accordionVideoItems.value[index];

            this.element.config.accordionVideoItems.value[index] = this._updateItem(data, before.youtubeId);

            this.$set(this.element.data, 'accordionVideoItems.value['+index+']', this.element.config.accordionVideoItems.value[index]);
        },

        _initItem() {
            return {name: '', youtubeId: ''}
        },

        _updateItem(name, youtubeId) {
            return {name: name, youtubeId: youtubeId}
        }
    }
});
