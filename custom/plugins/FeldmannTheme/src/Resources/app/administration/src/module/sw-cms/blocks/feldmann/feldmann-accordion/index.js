import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'feldmann-accordion',
    label: 'feldmann accordion',
    category: 'feldmann',
    component: 'sw-cms-block-feldmann-accordion',
    previewComponent: 'sw-cms-preview-feldmann-accordion',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '0',
        marginRight: '0',
        sizingMode: 'boxed'
    },
    slots: {
        accordion: {
            type: 'accordion'
        }
    }
});
