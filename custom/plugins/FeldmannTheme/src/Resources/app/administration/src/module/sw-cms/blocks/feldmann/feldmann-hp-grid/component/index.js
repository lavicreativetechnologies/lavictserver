import template from './sw-cms-block-feldmann-hp-grid.html.twig';
import './sw-cms-block-feldmann-hp-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-feldmann-hp-grid', {
    template
});
