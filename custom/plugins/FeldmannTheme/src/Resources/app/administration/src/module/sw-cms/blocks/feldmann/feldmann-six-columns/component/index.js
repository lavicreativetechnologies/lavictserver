import template from './sw-cms-block-feldmann-six-columns.html.twig';
import './sw-cms-block-feldmann-six-columns.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-feldmann-six-columns', {
    template
});
