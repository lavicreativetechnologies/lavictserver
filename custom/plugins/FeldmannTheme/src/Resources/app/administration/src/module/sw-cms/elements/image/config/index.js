import template from './sw-cms-el-config-image.html.twig';

const { Component } = Shopware;

Component.override('sw-cms-el-config-image', {
    template
});
