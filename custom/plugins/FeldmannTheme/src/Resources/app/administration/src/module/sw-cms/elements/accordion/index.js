import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'accordion',
    label: 'sw-cms.elements.accordion.label',
    component: 'sw-cms-el-accordion',
    configComponent: 'sw-cms-el-config-accordion',
    previewComponent: 'sw-cms-el-preview-accordion',

    defaultConfig: {
        accordionItems: {
            source: 'static',
            value: []
        }
    }
});
