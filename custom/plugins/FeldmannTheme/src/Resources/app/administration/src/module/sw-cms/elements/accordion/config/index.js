import template from './sw-cms-el-config-accordion.html.twig';
import './sw-cms-el-config-accordion.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-accordion', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    data() {
        return {
            accordionItems: {
                value: []
            }
        }
    },

    created() {
        this.createdComponent();
    },

    computed: {
        items() {
            if (this.element.config && this.element.config.accordionItems && this.element.config.accordionItems.value) {
                return this.element.config.accordionItems.value;
            }

            return [this._initItem()];
        }
    },

    methods: {
        createdComponent() {
            this.initElementConfig('accordion');
        },

        addRow() {
            this.element.config.accordionItems.value.push(this._initItem());

            this.$set(this.element.data, 'accordionItems', this.element.config.accordionItems);
        },

        deleteRow(index) {
            this.element.config.accordionItems.value.splice(index, 1);

            this.$set(this.element.data, 'accordionItems', this.element.config.accordionItems);
        },

        onContentChange(data, index) {
            let before = this.element.config.accordionItems.value[index];

            this.element.config.accordionItems.value[index] = this._updateContent(before, data);

            this.$set(this.element.data, 'accordionItems.value['+index+']', this.element.config.accordionItems.value[index]);
        },

        onContentBlur(data, index) {
            this.onContentChange(data, index);
        },

        onTitleChange(data, index) {
            let before = this.element.config.accordionItems.value[index];

            this.element.config.accordionItems.value[index] = this._updateTitle(before, data);

            this.$set(this.element.data, 'accordionItems.value['+index+']', this.element.config.accordionItems.value[index]);
        },

        _initItem() {
            return {title: '', content: '', id: this._newId()}
        },

        _updateTitle(oldItem, title) {
            return this._updateItem(oldItem.id, title, oldItem.content);
        },

        _updateContent(oldItem, content) {
            return this._updateItem(oldItem.id, oldItem.title, content);
        },

        _updateItem(id, title, content) {
            return {id: id, title: title, content: content}
        },

        _newId () {
            return '_' + Math.random().toString(36).substr(2, 9);
        }
    }
});
