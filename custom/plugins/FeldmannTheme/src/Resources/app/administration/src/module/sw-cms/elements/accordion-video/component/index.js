import template from './sw-cms-el-accordion-video.html.twig';
import './sw-cms-el-accordion-video.scss';

const {Component, Mixin} = Shopware;

Component.register('sw-cms-el-accordion-video', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        accordionVideoItems() {
            if (this.element.config && this.element.config.accordionVideoItems && this.element.config.accordionVideoItems.value && this.element.config.accordionVideoItems.value.length > 0) {
                return this.element.config.accordionVideoItems.value;
            }

            if (this.element.data && this.element.data.accordionVideoItems && this.element.data.accordionVideoItems.value && this.element.data.accordionVideoItems.value.length > 0) {
                return this.element.data.accordionVideoItems.value;
            }

            return [
                {name: 'Video 1', youtubeId: 'youtube-id1'},
                {name: 'Video 2', youtubeId: 'youtube-id2'},
                {name: 'Video 3', youtubeId: 'youtube-id3'}
            ];
        },

        title() {
            return this.element.config.accordionTitle.value;
        }
    },

    watch: {
        'element.data.accordionVideoItems': {
            handler() {
                console.log('watch');
                console.log(this.accordionVideoItems.value);
            },
            deep: true
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('accordion-video');
            this.initElementData('accordion-video');
        }
    }
});
