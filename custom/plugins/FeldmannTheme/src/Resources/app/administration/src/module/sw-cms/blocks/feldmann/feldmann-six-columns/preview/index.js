import template from './sw-cms-preview-feldmann-six-columns.html.twig';
import './sw-cms-preview-feldmann-six-columns.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-feldmann-six-columns', {
    template
});
