import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'feldmann-sub-categories',
    label: 'Sub Categories',
    category: 'feldmann',
    component: 'sw-cms-block-feldmann-sub-categories',
    previewComponent: 'sw-cms-preview-feldmann-sub-categories',

    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {}
});
