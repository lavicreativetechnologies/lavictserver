import template from './sw-cms-preview-feldmann-accordion.html.twig';
import './sw-cms-preview-feldmann-accordion.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-feldmann-accordion', {
    template
});
