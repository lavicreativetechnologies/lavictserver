import template from './sw-cms-preview-feldmann-sub-categories.html.twig';
import './sw-cms-preview-feldmann-sub-categories.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-feldmann-sub-categories', {
    template
});
