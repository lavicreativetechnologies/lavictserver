<?php declare(strict_types=1);

namespace FeldmannTheme\ScheduledTask;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class VersionCommitDeleteTaskHandler extends ScheduledTaskHandler
{
    private $connection;

    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, Connection $connection)
    {
        parent::__construct($scheduledTaskRepository);

        $this->connection = $connection;
    }

    public static function getHandledMessages(): iterable
    {
        return [VersionCommitDeleteTask::class];
    }

    public function run(): void
    {
        $this->deleteAllVersionCommit();
    }

    public function deleteAllVersionCommit()
    {
        $platform = $this->connection->getDatabasePlatform();

        $this->connection->executeUpdate($platform->getTruncateTableSQL('version_commit_data', true));
        $this->connection->executeUpdate('DELETE FROM version_commit');
    }
}
