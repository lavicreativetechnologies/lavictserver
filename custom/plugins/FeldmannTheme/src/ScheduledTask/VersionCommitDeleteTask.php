<?php declare(strict_types=1);

namespace FeldmannTheme\ScheduledTask;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class VersionCommitDeleteTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'ies.version_commit_delete';
    }

    public static function getDefaultInterval(): int
    {
        return 86400;
    }
}
