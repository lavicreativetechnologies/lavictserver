<?php declare(strict_types=1);

namespace FeldmannTheme\Event;

use Shopware\Core\Checkout\Order\OrderDefinition;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\BusinessEventInterface;
use Shopware\Core\Framework\Event\EventData\EntityType;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\MailActionInterface;
use Symfony\Contracts\EventDispatcher\Event;

class OrderEmailToShopManagerEvent extends Event implements BusinessEventInterface, MailActionInterface
{
    public const EVENT_NAME = 'ies.order_email.shop_manager';

    private $order;

    private $context;

    private $mailRecipientStruct;

    private $salesChannelId;

    public function __construct(Context $context, OrderEntity $order, string $salesChannelId, MailRecipientStruct $mailRecipientStruct)
    {
        $this->order = $order;
        $this->context = $context;
        $this->mailRecipientStruct = $mailRecipientStruct;
        $this->salesChannelId = $salesChannelId;
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getOrder(): OrderEntity
    {
        return $this->order;
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('order', new EntityType(OrderDefinition::class));
    }

    public function getContext(): Context
    {
        return $this->context;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        return $this->mailRecipientStruct;
    }

    public function getSalesChannelId(): ?string
    {
        return $this->salesChannelId;
    }
}
