<?php declare(strict_types=1);

namespace FeldmannTheme\Twig;

use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemCollection;
use Shopware\Core\Framework\Struct\Collection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class SortOrderLineItemsExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('sortOrderLineItems', [$this, 'sort']),
        ];
    }

    public function sort($collection)
    {
        if (!$collection instanceof OrderLineItemCollection) {
            return $collection;
        }

        $elements = $collection->getElements();

        usort($elements, function($a, $b) {
            return $a->getPosition() > $b->getPosition();
        });

        return new OrderLineItemCollection($elements);
    }
}
