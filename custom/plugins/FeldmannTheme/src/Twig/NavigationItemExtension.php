<?php declare(strict_types=1);

namespace FeldmannTheme\Twig;

use FeldmannTheme\Services\CategoryVisibility;
use Shopware\Core\Content\Category\Tree\TreeItem;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NavigationItemExtension extends AbstractExtension
{
    private $categoryVisibility;

    public function __construct(CategoryVisibility $categoryVisibility)
    {
        $this->categoryVisibility = $categoryVisibility;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('hasVisibleChildItem', [$this, 'hasVisibleChildItem']),
            new TwigFunction('showNavigationItem', [$this, 'showNavigationItem']),
        ];
    }

    public function hasVisibleChildItem(TreeItem $treeItem): bool
    {
        $children = $treeItem->getChildren();
        /** @var TreeItem $child */
        foreach ($children as $child) {
            $customFields = (array) $child->getCategory()->getTranslation('customFields');
            if (!array_key_exists('custom_category_fields_navigation_hide', $customFields) || !$customFields['custom_category_fields_navigation_hide']) {
                return true;
            }
        }
        return false;
    }

    public function showNavigationItem(TreeItem $treeItem, SalesChannelContext $context): bool
    {
        $customFields = $treeItem->getCategory()->getTranslation('customFields');
        if (!$customFields || !is_array($customFields)) {
            return true;
        }

        $isHidden = array_key_exists('custom_category_fields_navigation_hide', $customFields) && $customFields['custom_category_fields_navigation_hide'];
        if ($isHidden) {
            return false;
        }

        return $this->categoryVisibility->isVisibleForUser($treeItem->getCategory(), $context);
    }
}
