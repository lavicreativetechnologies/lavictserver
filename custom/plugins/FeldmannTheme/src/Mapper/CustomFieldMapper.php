<?php declare(strict_types=1);

namespace FeldmannTheme\Mapper;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Field;
use Shopware\Elasticsearch\Framework\Indexing\EntityMapper;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class CustomFieldMapper extends EntityMapper
{
    private $decorated;

    public function __construct(EntityMapper $decorated)
    {
        $this->decorated = $decorated;
    }

    public function mapField(EntityDefinition $definition, Field $field, Context $context): ?array
    {
        if ($field instanceof CustomFields) {
            return null;
        }

        if ($field->getPropertyName() === 'customFields') {
            return null;
        }

        if ($field->getPropertyName() === 'slotConfig') {
            return  ['type' => 'nested', 'properties' => [], 'dynamic' => false];
        }

        return $this->decorated->mapField($definition, $field, $context);
    }
}
