<?php declare(strict_types=1);

namespace FeldmannTheme\Migration;

use Doctrine\DBAL\Connection;
use FeldmannTheme\Event\OrderEmailToShopManagerEvent;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1600933345OrderEmailToShopManager extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1600933345;
    }

    public function update(Connection $connection): void
    {
        if (!$this->eventActionExists($connection, OrderEmailToShopManagerEvent::EVENT_NAME)) {
            $this->insertEventAction($connection, OrderEmailToShopManagerEvent::EVENT_NAME);
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    protected function eventActionExists(Connection $connection, string $eventName)
    {
        $sql = <<<SQL
SELECT LOWER(Hex(`id`))
FROM `event_action`
WHERE `event_name` = :eventName
AND `action_name` = :actionName
SQL;

        return $connection->executeQuery($sql, [
            'eventName' => $eventName,
            'actionName' => 'action.mail.send',
        ])->fetchColumn();
    }

    protected function insertEventAction(Connection $connection, string $eventName): void
    {
        $connection->insert(
            'event_action',
            [
                'id' => Uuid::randomBytes(),
                'event_name' => $eventName,
                'action_name' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
                'config' => json_encode(['mail_template_type_id' => $this->getOrderConfirmEmailTypeId($connection)]),
                'created_at' => date(Defaults::STORAGE_DATE_FORMAT),
            ]
        );
    }

    protected function getOrderConfirmEmailTypeId(Connection $connection)
    {
        $sql = <<<SQL
SELECT LOWER(Hex(`id`))
FROM `mail_template_type`
WHERE `technical_name` = :technicalName
SQL;

        return $connection->executeQuery($sql, [
            'technicalName' => 'order_confirmation_mail',
        ])->fetchColumn();
    }
}
