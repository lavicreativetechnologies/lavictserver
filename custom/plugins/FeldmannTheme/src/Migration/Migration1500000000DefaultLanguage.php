<?php declare(strict_types=1);

namespace FeldmannTheme\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1500000000DefaultLanguage extends MigrationStep
{
    private $newLanguage = 'de-DE';

    /** @var Connection */
    protected $connection = null;

    public function getCreationTimestamp(): int
    {
        return 1500000000;
    }

    public function update(Connection $connection): void
    {
        $this->connection = $connection;

        $currentLocaleStmt = $this->connection->prepare(
            'SELECT locale.id, locale.code
             FROM language
             INNER JOIN locale ON translation_code_id = locale.id
             WHERE language.id = ?'
        );

        $currentLocaleStmt->execute([Uuid::fromHexToBytes(Defaults::LANGUAGE_SYSTEM)]);
        $currentLocale = $currentLocaleStmt->fetch(\PDO::FETCH_ASSOC);

        if (!$currentLocale) {
            throw new \RuntimeException('Default language locale not found');
        }

        $currentLocaleId = $currentLocale['id'];
        $newDefaultLocaleId = $this->getLocaleId($this->newLanguage);

        // locales match -> do nothing.
        if ($currentLocaleId === $newDefaultLocaleId) {
            return;
        }

        $newDefaultLanguageId = $this->getLanguageId($this->newLanguage);

        // new language not exists -> do nothing.
        if (!$newDefaultLanguageId) {
            return;
        }

        if ($this->newLanguage === 'de-DE' && $currentLocale['code'] === 'en-GB') {
            $this->swapDefaultLanguageId($newDefaultLanguageId);
        } else {
            // currently only de-DE is supported -> do nothing.
            return;
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function swapDefaultLanguageId(string $newLanguageId): void
    {
        $stmt = $this->connection->prepare(
            'UPDATE language
             SET id = :newId
             WHERE id = :oldId'
        );

        // assign new uuid to old DEFAULT
        $stmt->execute([
            'newId' => Uuid::randomBytes(),
            'oldId' => Uuid::fromHexToBytes(Defaults::LANGUAGE_SYSTEM),
        ]);

        // change id to DEFAULT
        $stmt->execute([
            'newId' => Uuid::fromHexToBytes(Defaults::LANGUAGE_SYSTEM),
            'oldId' => $newLanguageId,
        ]);
    }

    private function getLocaleId(string $iso): string
    {
        $stmt = $this->connection->prepare('SELECT locale.id FROM locale WHERE LOWER(locale.code) = LOWER(?)');
        $stmt->execute([$iso]);
        $id = $stmt->fetchColumn();

        if (!$id) {
            throw new \RuntimeException('Locale with iso-code ' . $iso . ' not found');
        }

        return (string) $id;
    }

    private function getLanguageId(string $iso): ?string
    {
        $stmt = $this->connection->prepare(
            'SELECT language.id
             FROM `language`
             INNER JOIN locale ON locale.id = language.translation_code_id
             WHERE LOWER(locale.code) = LOWER(?)'
        );
        $stmt->execute([$iso]);

        return $stmt->fetchColumn() ?: null;
    }
}
