<?php declare(strict_types=1);

namespace FeldmannTheme\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1596611580NavigationUserVisibility extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1596611580;
    }

    public function update(Connection $connection): void
    {
        $fieldData = [
            'id' => Uuid::randomBytes(),
            'name' => 'custom_category_fields_navigation_user_visibility',
            'type' => 'select',
            'config' => json_encode([
                'label' => [
                    'de-DE' => 'Kategorie anzeigen für',
                ],
                "componentName" => "sw-single-select",
                "customFieldType" => "select",
                'customFieldPosition' => 4,
                'options' => [
                    [
                        'label' => [
                            'de-DE' => 'Gast & Kunden',
                        ],
                        'value' => 'both'
                    ],
                    [
                        'label' => [
                            'de-DE' => 'nur Gast',
                        ],
                        'value' => 'visitor'
                    ],
                    [
                        'label' => [
                            'de-DE' => 'nur Kunden',
                        ],
                        'value' => 'user'
                    ],
                ],
            ]),
            'set_id' => $this->getFieldSetIdByName($connection, 'custom_category_fields'),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $connection->insert('custom_field', $fieldData);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function getFieldSetIdByName(Connection $connection, string $setName): string
    {
        $sql = <<<SQL
SELECT `id` FROM `custom_field_set` WHERE `name` = :setName
SQL;
        /** @var string $id */
        $id = $connection->executeQuery($sql, ['setName' => $setName])->fetchColumn();
        if (!$id) {
            throw new \RuntimeException(sprintf('custom field set for name "%s" not found.', $setName));
        }

        return $id;
    }
}
