<?php declare(strict_types=1);

namespace FeldmannTheme\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1590406272SaleNavigation extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1590406272;
    }

    public function update(Connection $connection): void
    {
        $fieldData = [
            'id' => Uuid::randomBytes(),
            'name' => 'custom_category_fields_navigation_flag',
            'type' => 'text',
            'config' => json_encode([
                'componentName' => 'sw-field',
                'type' => 'text',
                'customFieldType' => 'text',
                'customFieldPosition' => 2,
                'label' => [
                    'en-GB' => 'Category navigation flag',
                ],
                'placeholder' => [
                    'en-GB' => 'is-sale, is-cms-link...',
                ],
                'helpText' => [
                    'en-GB' => 'is-sale is the flag for sale category. is-cms-link is the flag for anchor of cms elements, which should be assigned to a css class like is-cms-{{CategoryName}} in cms sector',
                ],
            ]),
            'set_id' => $this->getFieldSetIdByName($connection, 'custom_category_fields'),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $connection->insert('custom_field', $fieldData);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function getFieldSetIdByName(Connection $connection, string $setName): string
    {
        $sql = <<<SQL
SELECT `id` FROM `custom_field_set` WHERE `name` = :setName
SQL;
        /** @var string $id */
        $id = $connection->executeQuery($sql, ['setName' => $setName])->fetchColumn();
        if (!$id) {
            throw new \RuntimeException(sprintf('custom field set for name "%s" not found.', $setName));
        }

        return $id;
    }
}
