<?php declare(strict_types=1);

namespace FeldmannTheme\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1589192300 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1589192300;
    }

    public function update(Connection $connection): void
    {
        $fieldId = Uuid::randomBytes();
        $fieldSetId = Uuid::randomBytes();

        $fieldData = [
            'id' => $fieldId,
            'name' => 'custom_category_fields_icon_class',
            'type' => 'text',
            'config' => json_encode([
                'componentName' => 'sw-field',
                'type' => 'text',
                'customFieldType' => 'text',
                'customFieldPosition' => 1,
                'label' => [
                    'en-GB' => 'Category icon class',
                    'placeholder' => [
                        'en-GB' => 'fas-truck',
                        'helpText' => [
                            'en-GB' => 'Defines the icon which displayed in front of category',
                        ],
                    ],
                ],
            ]),
            'set_id' => $fieldSetId,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $fieldSetData = [
            'id' => $fieldSetId,
            'name' => 'custom_category_fields',
            'config' => json_encode([
                'label' => [
                    'en-GB' => 'Custom category fields',
                ],
            ]),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $fieldSetRelationData = [
            'id' => Uuid::randomBytes(),
            'set_id' => $fieldSetId,
            'entity_name' => 'category',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $connection->insert('custom_field_set', $fieldSetData);
        $connection->insert('custom_field', $fieldData);
        $connection->insert('custom_field_set_relation', $fieldSetRelationData);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
