<?php declare(strict_types=1);

namespace FeldmannRegistrationExtend\Decorators;

use Shopware\Core\Checkout\Customer\SalesChannel\CustomerResponse;
use Shopware\Core\Checkout\Customer\SalesChannel\RegisterRoute as AccountRegistrationServiceInner;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Validation\DataBag\DataBag;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\Framework\Validation\DataValidationDefinition;
use Shopware\Core\Framework\Validation\DataValidationFactoryInterface;
use Shopware\Core\System\NumberRange\ValueGenerator\NumberRangeValueGeneratorInterface;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\Validation\DataValidator;
use Shopware\Core\System\SystemConfig\SystemConfigService;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class AccountRegistrationService extends AccountRegistrationServiceInner
{
    public const PROOF_FAX = 'fax';

    public const PROOF_FILE = 'file';

    public const PROOF_EXISTING_CUSTOMER = 'existing_customer';

    public const VALID_MIME_TYPE = [
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/bmp',
        'image/tiff',
        'application/pdf',
    ];

    private AccountRegistrationServiceInner $accountRegistrationServiceInner;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        NumberRangeValueGeneratorInterface $numberRangeValueGenerator,
        DataValidator $validator,
        DataValidationFactoryInterface $accountValidationFactory,
        DataValidationFactoryInterface $addressValidationFactory,
        SystemConfigService $systemConfigService,
        EntityRepositoryInterface $customerRepository,
        SalesChannelContextPersister $contextPersister
    )
    {
        parent::__construct(
            $eventDispatcher,
            $numberRangeValueGenerator,
            $validator,
            $accountValidationFactory,
            $addressValidationFactory,
            $systemConfigService,
            $customerRepository,
            $contextPersister
        );
    }

    public function register(
        RequestDataBag $data,
        SalesChannelContext $context,
        bool $validateStorefrontUrl = true,
        ?DataValidationDefinition $additionalValidationDefinitions = null
    ): CustomerResponse {

        $registerData = parent::register($data, $context,$validateStorefrontUrl,$additionalValidationDefinitions);
        $data = $this->addUploadedFile($data);

        $data = $this->addPersonalDataToShippingAddress($data);
        $additionalValidationDefinitions = $this->addValidations($data, $additionalValidationDefinitions);
        return $registerData;
    }

    public function finishDoubleOptInRegistration(RequestDataBag $dataBag, SalesChannelContext $context): string
    {
        return $this->accountRegistrationServiceInner->finishDoubleOptInRegistration($dataBag, $context);
    }

    protected function addValidations(RequestDataBag $data, ?DataValidationDefinition $additionalValidationDefinitions): DataValidationDefinition
    {
        $additionalValidationDefinitions->add('registerProof', new NotBlank(), new EqualTo([
            'value' => $data->get('registerProof'),
        ]));

        if ($data->get('registerProof') === self::PROOF_EXISTING_CUSTOMER) {
            $additionalValidationDefinitions->add('registerProofCustomerNumber', new NotBlank(), new EqualTo([
                'value' => $data->get('registerProofCustomerNumber'),
            ]));
        }

        if ($data->get('registerProof') === self::PROOF_FILE) {
            $additionalValidationDefinitions->add('registerProofFile', new File([
                'mimeTypes' => self::VALID_MIME_TYPE
            ]));
        }

        return $additionalValidationDefinitions;
    }

    protected function addUploadedFile(RequestDataBag $data): RequestDataBag
    {

        if ($data->get('registerProof') === self::PROOF_FILE) {
            $bag = new FileBag($_FILES);
            $file = $bag->get('registerProofFile');

            $data->add(['registerProofFile' => $file]);
        }

        return $data;
    }

    private function addPersonalDataToShippingAddress(RequestDataBag $data): RequestDataBag
    {
        if ($data->get('differentShippingAddress')) {
            $shippingAddressData = $data->get('shippingAddress');
            $shippingAddressData->set('firstName', $data->get('firstName'));
            $shippingAddressData->set('lastName', $data->get('lastName'));
            $shippingAddressData->set('salutationId', $data->get('salutationId'));

            $billingAddressData = $data->get('billingAddress');
            if ($phoneNumber = $billingAddressData->get('phoneNumber')) {
                $shippingAddressData->set('phoneNumber', $phoneNumber);
            }
        }

        return $data;
    }
}
