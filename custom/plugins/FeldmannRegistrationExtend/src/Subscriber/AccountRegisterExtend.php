<?php declare(strict_types=1);

namespace FeldmannRegistrationExtend\Subscriber;

use Shopware\Core\Checkout\Customer\CustomerEvents;
use Shopware\Core\Content\Media\File\FileNameProvider;
use Shopware\Core\Content\Media\File\FileSaver;
use Shopware\Core\Content\Media\File\MediaFile;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\Event\DataMappingEvent;
use Shopware\Core\Framework\Util\Random;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Recovery\Common\HttpClient\Response;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\FileBag;


class AccountRegisterExtend implements EventSubscriberInterface
{

    /**
     * @var EntityRepositoryInterface
     */
    private $mediaRepository;

    private FileSaver $mediaUpdater;
    private FileNameProvider $fileNameProvider;


    public function __construct(
        EntityRepositoryInterface $mediaRepository,
        FileSaver $mediaUpdater,
        FileNameProvider $fileNameProvider
    ) {
        $this->mediaRepository = $mediaRepository;
        $this->mediaUpdater = $mediaUpdater;
        $this->fileNameProvider = $fileNameProvider;
    }
    public static function getSubscribedEvents(): array
    {
        // Return the events to listen to as array like this:  <event to listen to> => <method to execute>
        return [
            CustomerEvents::MAPPING_REGISTER_CUSTOMER => 'extendInformation'
        ];
    }

    public function extendInformation(DataMappingEvent $event): bool
    {

        $inputData = $event->getInput();
        $outputData = $event->getOutput();

        $registerHow = $inputData->get('custom_registration_ext_hear');
        $businessLicense = $inputData->get('registerProof');
        $newsletter = (bool)$inputData->get('subscribeNewsletter', false);


        if ($businessLicense == 'file'){
            $bag = new FileBag($_FILES);
            $file = $bag->get('registerProofFile');
            $proofDetail = $this->addUploadedFile($file);
            $outputData['customFields'] = array('custom_registration_ext_hear' => $registerHow, 'custom_registration_ext_Business_license' => $businessLicense, 'custom_registration_ext_file' => $proofDetail, 'custom_registration_ext_receive_newsletter' => $newsletter);
        }elseif ($businessLicense == 'existing_customer'){
            $proofDetail = $inputData->get('registerProofCustomerNumber');
            $outputData['customFields'] = array('custom_registration_ext_hear' => $registerHow, 'custom_registration_ext_Business_license' => $businessLicense, 'custom_registration_customer_number' => $proofDetail, 'custom_registration_ext_receive_newsletter' => $newsletter);
        }else{
            $outputData['customFields'] = array('custom_registration_ext_hear' => $registerHow, 'custom_registration_ext_Business_license' => $businessLicense, 'custom_registration_ext_receive_newsletter' => $newsletter );
        }

        $event->setOutput($outputData);
        return true;
    }

    private function addUploadedFile($file): String
    {
        $testSupportedExtension = array('doc','pdf');

        $context = Context::createDefaultContext();


        $fileName = $file->getClientOriginalName();
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $mediaId = '';
        if (!in_array($ext,$testSupportedExtension) ) {
            $error = true;
            $message = 'Invalid Extension';
        }
        else {
            $fileName = $fileName . Random::getInteger(100, 1000);

            $mediaId = Uuid::randomHex();
            $media = [
                [
                    'id' => $mediaId,
                    'name' => $fileName,
                    'fileName' => $fileName,
                    'mimeType' => $file->getClientMimeType(),
                    'fileExtension' => $file->guessExtension(),
                ]
            ];


            $mediaId = $this->mediaRepository->create($media, Context::createDefaultContext())->getEvents()->getElements()[1]->getIds()[0];
                if (is_array($mediaId)) {
                    $mediaId = $mediaId['mediaId'];
                }
                try {
                    $this->upload($file, $fileName, $mediaId, $context);
                } catch (\Exception $exception) {
                    $fileName = $fileName . Random::getInteger(100, 1000);
                    $this->upload($file, $fileName, $mediaId, $context);
                }
            }

        return $mediaId;

    }
    public function upload($file, $fileName, $mediaId, $context)
    {

        return $this->mediaUpdater->persistFileToMedia(
            new MediaFile(
                $file->getRealPath(),
                $file->getMimeType(),
                $file->guessExtension(),
                $file->getSize()
            ),
            $this->fileNameProvider->provide(
                $fileName,
                $file->guessExtension(),
                $mediaId,
                $context
            ),
            $mediaId,
            $context
        );
    }


}
