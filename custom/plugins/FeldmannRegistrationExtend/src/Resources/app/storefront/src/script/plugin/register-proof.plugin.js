import Plugin from 'src/plugin-system/plugin.class';
import Iterator from 'src/helper/iterator.helper';

export default class RegisterProofPlugin extends Plugin {

    static options = {
        selectDataAttribute: 'data-register-proof-select',
        optionDataAttribute: 'data-register-proof-option'
    };

    init() {
        this._getTargets();
        this._registerEvents();
        this._initForm();
        this._onChange();
    }

    _getTargets() {
        this._select = this.el.querySelector(this._dataAttributeSelector(this.options.selectDataAttribute));
        this._options = this.el.querySelectorAll(this._dataAttributeSelector(this.options.optionDataAttribute));
        this._form = this.el.closest('form');
    }

    _registerEvents() {
        this._select.addEventListener('change', this._onChange.bind(this));
    }

    _initForm() {
        if (!this._form.hasAttribute('enctype')) {
            this._form.setAttribute('enctype', 'multipart/form-data');
        }
    }

    _onChange() {
        Iterator.iterate(this._options, node => {
            this._toggleOptions(node);
        });
    }

    _toggleOptions(node) {
        if (node.getAttribute(this.options.optionDataAttribute) === this._select.value) {
            this._showElement(node);
        } else {
            this._hideElement(node);
        }
    }

    _showElement(node) {
        if (node.classList.contains('d-none')) {
            node.classList.remove('d-none');
        }

        const inputNode = node.querySelector('.proof-option');
        if (!inputNode.hasAttribute('required')) {
            inputNode.setAttribute('required', 'required');
        }
    }

    _hideElement(node) {
        if (!node.classList.contains('d-none')) {
            node.classList.add('d-none');
        }

        const inputNode = node.querySelector('.proof-option');
        if (inputNode.hasAttribute('required')) {
            inputNode.removeAttribute('required');
        }
    }

    _dataAttributeSelector(attribute) {
        return '[' + attribute + ']';
    }
}
