import RegisterProof from './script/plugin/register-proof.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('RegisterProof', RegisterProof, '[data-register-proof]');
