<?php declare(strict_types=1);

namespace Ies\WishList\Storefront\Controller;

use Ies\WishList\Service\WishListProductService;
use Ies\WishList\Service\WishListService;
use Ies\WishList\Storefront\Page\WishListPageLoader;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Routing\Annotation\LoginRequired;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 *
 * @RouteScope(scopes={"storefront"})
 */
class WishListController extends StorefrontController
{
    private WishListService $wishListService;

    private WishListProductService $wishListProductService;

    private WishListPageLoader $wishListPageLoader;

    private EntityRepositoryInterface $productRepository;

    public function __construct(
        WishListService $wishListService,
        WishListProductService $wishListProductService,
        WishListPageLoader $wishListPageLoader,
        EntityRepositoryInterface $productRepository
    ) {
        $this->wishListService = $wishListService;
        $this->wishListProductService = $wishListProductService;
        $this->wishListPageLoader = $wishListPageLoader;
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/wishlist", name="frontend.wishlist.page", options={"seo"="false"}, methods={"GET"})
     * @LoginRequired()
     */
    public function index(Request $request, SalesChannelContext $context): Response
    {
        $page = $this->wishListPageLoader->load($request, $context);

        return $this->renderStorefront('@Storefront/storefront/page/wish-list/index.html.twig', ['page' => $page]);
    }

    /**
     * @Route("/wishlist/add", name="frontend.wishlist.add", options={"seo"="false"}, methods={"POST"}, defaults={"XmlHttpRequest"=true, "csrf_protected"=false})
     */
    public function add(Request $request, RequestDataBag $data, SalesChannelContext $salesChannelContext): Response
    {
        $productId = $data->get('productId');
        $product = $this->productRepository->search(new Criteria([$productId]), $salesChannelContext->getContext())->first();
        if (!$product) {
            return new JsonResponse(['itemCount' => 0]);
        }

        $wishList = $this->wishListService->getWishList($salesChannelContext);
        if (!$wishList) {
            $id = $this->wishListService->createWishList($salesChannelContext);
            $this->wishListProductService->addProductToWishList(reset($id), $productId, $salesChannelContext);

            return new JsonResponse(['itemCount' => 1]);
        }

        $wishListProducts = $wishList->get('wish_list_product')->filterByProperty('productId', $productId);
        $wishListId = $wishList->getId();
        if (!$wishListProducts->getElements()) {
            $this->wishListProductService->addProductToWishList($wishListId, $productId, $salesChannelContext);
        }

        return new JsonResponse(['itemCount' => $this->wishListProductService->getProductCountsFromWishList($wishListId, $salesChannelContext)]);
    }


    /**
     * @Route("/wishlist/remove", name="frontend.wishlist.remove", options={"seo"="false"}, methods={"POST"}, defaults={"XmlHttpRequest"=true, "csrf_protected"=false})
     */
    public function remove(Request $request, RequestDataBag $data, SalesChannelContext $salesChannelContext): Response
    {
        $wishList = $this->wishListService->getWishList($salesChannelContext);
        if (!$wishList) {
            return $this->createResponse($request, ['itemCount' => 0]);
        }

        $productId = $data->get('productId');
        $wishListId = $wishList->getId();
        if ($this->wishListProductService->hasProductFromWishList($wishListId, $productId, $salesChannelContext)) {
            $this->wishListProductService->removeProductFromWishList($wishListId, $productId, $salesChannelContext);
        }

        if (!$this->wishListProductService->getProductCountsFromWishList($wishListId, $salesChannelContext)) {
            $this->wishListService->deleteWishList($wishListId, $salesChannelContext);

            return $this->createResponse($request, ['itemCount' => 0]);
        }

        $itemCount = $this->wishListProductService->getProductCountsFromWishList($wishListId, $salesChannelContext);
        return $this->createResponse($request, ['itemCount' => $itemCount]);
    }

    private function createResponse(Request $request, array $itemCount): Response
    {
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse($itemCount);
        }

        return $this->createActionResponse($request);
    }
}
