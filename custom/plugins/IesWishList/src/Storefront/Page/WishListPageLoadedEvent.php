<?php declare(strict_types=1);

namespace Ies\WishList\Storefront\Page;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\PageLoadedEvent;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListPageLoadedEvent extends PageLoadedEvent
{
    protected WishListPage $page;

    public function __construct(WishListPage $page, SalesChannelContext $salesChannelContext, Request $request)
    {
        $this->page = $page;
        parent::__construct($salesChannelContext, $request);
    }

    public function getPage(): WishListPage
    {
        return $this->page;
    }
}
