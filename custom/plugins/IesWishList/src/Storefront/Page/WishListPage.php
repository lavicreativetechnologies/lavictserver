<?php declare(strict_types=1);

namespace Ies\WishList\Storefront\Page;

use Ies\WishList\Core\WishList\WishListEntity;
use Shopware\Storefront\Page\Page;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListPage extends Page
{
    private ?WishListEntity $wishList;

    private array $wishListItems;

    public function getWishList(): ?WishListEntity
    {
        return $this->wishList;
    }

    public function setWishList(?WishListEntity $wishList): void
    {
        $this->wishList = $wishList;
    }

    public function getWishListItems(): array
    {
        return $this->wishListItems;
    }

    public function setWishListItems(array $wishListItems): void
    {
        $this->wishListItems = $wishListItems;
    }
}
