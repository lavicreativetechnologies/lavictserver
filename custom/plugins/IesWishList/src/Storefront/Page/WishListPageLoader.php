<?php declare(strict_types=1);

namespace Ies\WishList\Storefront\Page;

use Ies\WishList\Core\WishListProduct\WishListProductEntity;
use Ies\WishList\Service\WishListProductService;
use Ies\WishList\Service\WishListService;
use Shopware\Core\Content\Product\SalesChannel\Detail\AbstractProductDetailRoute;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\GenericPageLoaderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListPageLoader
{
    private WishListService $wishListService;

    private WishListProductService $wishListProductService;

    private GenericPageLoaderInterface $genericLoader;

    private AbstractProductDetailRoute $productDetailRoute;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        WishListService $wishListService,
        WishListProductService $wishListProductService,
        GenericPageLoaderInterface $genericLoader,
        AbstractProductDetailRoute $productDetailRoute,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->wishListService = $wishListService;
        $this->wishListProductService = $wishListProductService;
        $this->genericLoader = $genericLoader;
        $this->productDetailRoute = $productDetailRoute;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function load(Request $request, SalesChannelContext $salesChannelContext): WishListPage
    {
        $page = $this->genericLoader->load($request, $salesChannelContext);
        $page = WishListPage::createFrom($page);
        $wishList = $this->wishListService->getWishList($salesChannelContext);
        $wishListItems = [];
        if ($wishList) {
            $products = $this->wishListProductService->getProductsFromWishList($wishList->getId(), $salesChannelContext);
            $criteria = $this->getProductCriteria();
            /** @var WishListProductEntity $product */
            foreach ($products as $product) {
                $wishListItems[] = $this->productDetailRoute->load(
                    $product->getProductId(),
                    $request,
                    $salesChannelContext,
                    $criteria
                )->getProduct();
            }
        }

        $page->setWishList($wishList);
        $page->setWishListItems($wishListItems);

        $this->eventDispatcher->dispatch(
            new WishListPageLoadedEvent($page, $salesChannelContext, $request)
        );

        return $page;
    }

    private function getProductCriteria()
    {
        $criteria = (new Criteria())
            ->addAssociation('manufacturer.media')
            ->addAssociation('options.group')
            ->addAssociation('properties.group')
            ->addAssociation('mainCategories.category');

        $criteria
            ->getAssociation('media')
            ->addSorting(new FieldSorting('position'));

        return $criteria;
    }
}
