import WishListDetailPlugin from './script/plugin/wishlist-detail.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('WishListWidgetPlugin', WishListDetailPlugin, '[data-wish-list-detail]');

// Necessary for the webpack hot module reloading server
if (module.hot) {
    module.hot.accept();
}
