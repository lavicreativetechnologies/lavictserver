export default class WishListHeaderBadge {

    static options = {
        headerCountBadgeSelector: '.header-actions-btn .wish-list-badge'
    };

    static updateBadgeCount(itemCount) {
        this._countBadge = document.querySelector(this.options.headerCountBadgeSelector);
        this._countBadge.innerHTML = itemCount;
        if (itemCount > 0) {
            this._showElement(this._countBadge);
        } else {
            this._hideElement(this._countBadge);
        }
    }

    static _showElement(node) {
        if (node.classList.contains('d-none')) {
            node.classList.remove('d-none');
        }
    }

    static _hideElement(node) {
        if (!node.classList.contains('d-none')) {
            node.classList.add('d-none');
        }
    }

}
