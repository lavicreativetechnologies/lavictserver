import Plugin from 'src/plugin-system/plugin.class';
import HttpClient from 'src/service/http-client.service';
import ElementLoadingIndicatorUtil from 'src/utility/loading-indicator/element-loading-indicator.util';
import FormSerializeUtil from 'src/utility/form/form-serialize.util';
import WishListHeaderBadge from '../helper/wishlist-header-badge';

export default class WishListDetailPlugin extends Plugin {
    static options = {
        formSelector: '.wish-list-form',
        linkSelector: '.wish-list-link',
        loaderContainerSelector: '.product-detail-buy'
    };

    init() {
        this._initElements();
        this._registerEvents();
    }

    _initElements() {
        this._loaded = false;
        this._client = new HttpClient(window.accessKey, window.contextToken);
        this._formElement = this.el.querySelector(this.options.formSelector);
        this._linkElement = this.el.querySelector(this.options.linkSelector);
        this._loaderContainer = this.el.closest(this.options.loaderContainerSelector);
    }

    _registerEvents() {
        this._linkElement.addEventListener('click', this._onClick.bind(this));
    }

    _onClick() {
        if (this._loaded) {
            return;
        }

        this._submitForm();
    }

    _submitForm() {
        ElementLoadingIndicatorUtil.create(this._loaderContainer);
        this._loaded = true;

        const action = this._formElement.getAttribute('action');
        const data = FormSerializeUtil.serialize(this._formElement);
        this._client.post(action, data, this._onAfterAjaxSubmit.bind(this));
    }

    _onAfterAjaxSubmit(response) {
        this._formElement.setAttribute('action', this._updateFormAction());
        WishListHeaderBadge.updateBadgeCount(JSON.parse(response).itemCount);

        ElementLoadingIndicatorUtil.remove(this._loaderContainer);
        this._loaded = false;
    }

    _updateFormAction() {
        const action = this._formElement.getAttribute('action');
        if (action === window.router['frontend.wishlist.add']) {
            return window.router['frontend.wishlist.remove'];
        }

        return window.router['frontend.wishlist.add'];
    }
}
