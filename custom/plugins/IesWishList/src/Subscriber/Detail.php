<?php declare(strict_types=1);

namespace Ies\WishList\Subscriber;

use Ies\WishList\Service\WishListProductService;
use Ies\WishList\Service\WishListService;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class Detail implements EventSubscriberInterface
{
    private WishListService $wishListService;

    private WishListProductService $wishListProductService;

    public function __construct(WishListService $wishListService, WishListProductService $wishListProductService)
    {
        $this->wishListService = $wishListService;
        $this->wishListProductService = $wishListProductService;
    }

    public static function getSubscribedEvents()
    {
        return [
            ProductPageLoadedEvent::class => 'onProductPageLoaded'
        ];
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event): void
    {
        $salesChannelContext = $event->getSalesChannelContext();
        $wishList = $this->wishListService->getWishList($salesChannelContext);
        if (!$wishList) {
            return;
        }

        $page = $event->getPage();
        $productId = $page->getProduct()->getId();
        $page->assign([
            'productInWishList' => $this->wishListProductService->hasProductFromWishList($wishList->getId(), $productId, $salesChannelContext)
        ]);
    }
}
