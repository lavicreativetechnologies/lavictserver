<?php declare(strict_types=1);

namespace Ies\WishList\Subscriber;

use Ies\WishList\Service\WishListProductService;
use Ies\WishList\Service\WishListService;
use Shopware\Storefront\Pagelet\Header\HeaderPageletLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class Header implements EventSubscriberInterface
{
    private WishListService $wishListService;

    private WishListProductService $wishListProductService;

    public function __construct(WishListService $wishListService, WishListProductService $wishListProductService)
    {
        $this->wishListService = $wishListService;
        $this->wishListProductService = $wishListProductService;
    }

    public static function getSubscribedEvents()
    {
        return [
            HeaderPageletLoadedEvent::class => 'onHeaderLoaded',
        ];
    }

    public function onHeaderLoaded(HeaderPageletLoadedEvent $event)
    {
        $salesChannelContext = $event->getSalesChannelContext();
        $wishList = $this->wishListService->getWishList($salesChannelContext);

        if ($wishList) {
            $count = $this->wishListProductService->getProductCountsFromWishList($wishList->getId(), $salesChannelContext);
            $event->getPagelet()->assign(['wishListItemCount' => $count]);
        }
    }
}
