<?php declare(strict_types=1);

namespace Ies\WishList\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1591696535AddWishListProduct extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1591696535;
    }

    public function update(Connection $connection): void
    {
        $connection->executeQuery('
            CREATE TABLE IF NOT EXISTS `ies_wish_list_product` (
                `wish_list_id` BINARY(16) NOT NULL,
                `product_id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`wish_list_id`, `product_id`),
                CONSTRAINT `fk.ies_wish_list_product.wish_list_id` FOREIGN KEY (`wish_list_id`)
                    REFERENCES `ies_wish_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.ies_wish_list_product.product_id` FOREIGN KEY (`product_id`)
                    REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
