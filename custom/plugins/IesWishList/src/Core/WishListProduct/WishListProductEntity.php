<?php declare(strict_types=1);

namespace Ies\WishList\Core\WishListProduct;

use Ies\WishList\Core\WishList\WishListEntity;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListProductEntity extends Entity
{
    use EntityIdTrait;

    protected string $productId;

    protected ?WishListEntity $wishList;

    protected ?ProductEntity $product;

    public function setWishList(?WishListEntity $wishList): void
    {
        $this->wishList = $wishList;
    }

    public function getWishList(): ?WishListEntity
    {
        return $this->wishList;
    }

    public function getProduct(): ?ProductEntity
    {
        return $this->product;
    }

    public function setProduct(?ProductEntity $product): void
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }
}
