<?php declare(strict_types=1);

namespace Ies\WishList\Core\WishListProduct;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(WishListProductEntity $entity)
 * @method void              set(string $key, WishListProductEntity $entity)
 * @method WishListProductEntity[]    getIterator()
 * @method WishListProductEntity[]    getElements()
 * @method WishListProductEntity|null get(string $key)
 * @method WishListProductEntity|null first()
 * @method WishListProductEntity|null last()
 */
class WishListProductCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return WishListProductEntity::class;
    }
}
