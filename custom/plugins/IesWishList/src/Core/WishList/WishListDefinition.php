<?php declare(strict_types=1);

namespace Ies\WishList\Core\WishList;

use Ies\WishList\Core\WishListProduct\WishListProductDefinition;
use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\SalesChannel\SalesChannelDefinition;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'ies_wish_list';
    }

    public function getCollectionClass(): string
    {
        return WishListCollection::class;
    }

    public function getEntityClass(): string
    {
        return WishListEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey(), new Required()),
            new FkField('sales_channel_id', 'salesChannelId', SalesChannelDefinition::class),
            new FkField('customer_id', 'customerId', CustomerDefinition::class),

            new OneToManyAssociationField('wish_list_product', WishListProductDefinition::class, 'wish_list_id', 'id'),
            new ManyToOneAssociationField('salesChannel', 'sales_channel_id', SalesChannelDefinition::class, 'id', false),
            new ManyToOneAssociationField('customer', 'customer_id', CustomerDefinition::class, 'id'),
        ]);
    }
}
