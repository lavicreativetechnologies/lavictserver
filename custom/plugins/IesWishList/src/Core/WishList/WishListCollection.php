<?php declare(strict_types=1);

namespace Ies\WishList\Core\WishList;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(WishListEntity $entity)
 * @method void              set(string $key, WishListEntity $entity)
 * @method WishListEntity[]    getIterator()
 * @method WishListEntity[]    getElements()
 * @method WishListEntity|null get(string $key)
 * @method WishListEntity|null first()
 * @method WishListEntity|null last()
 */
class WishListCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return WishListEntity::class;
    }
}
