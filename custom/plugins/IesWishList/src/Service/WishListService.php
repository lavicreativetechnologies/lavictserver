<?php declare(strict_types=1);

namespace Ies\WishList\Service;

use Ies\WishList\Core\WishList\WishListEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListService
{
    private EntityRepositoryInterface $wishListRepository;

    public function __construct(EntityRepositoryInterface $wishListRepository)
    {
        $this->wishListRepository = $wishListRepository;
    }

    public function getWishList(SalesChannelContext $salesChannelContext): ?WishListEntity
    {
        $customer = $salesChannelContext->getCustomer();
        if (!$customer) {
            return null;
        }

        $criteria = new Criteria();
        $criteria->addAssociation('wish_list_product');
        $criteria->addFilter(new EqualsFilter('customerId', $customer->getId()));

        return $this->wishListRepository->search($criteria, $salesChannelContext->getContext())->first();
    }

    public function createWishList(SalesChannelContext $salesChannelContext): array
    {
        $customer = $salesChannelContext->getCustomer();
        if (!$customer) {
            return [];
        }

        $wishList = [
            'salesChannelId' => $salesChannelContext->getSalesChannel()->getId()
        ];
        $criteria = new Criteria();
        $customerId = $customer->getId();
        $wishList['customerId'] = $customerId;
        $criteria->addFilter(new EqualsFilter('customerId', $customerId));
        $this->wishListRepository->create([$wishList], $salesChannelContext->getContext());

        return $this->wishListRepository->search($criteria, $salesChannelContext->getContext())->getIds();
    }

    public function deleteWishList(string $wishListId, SalesChannelContext $salesChannelContext): void
    {
        $this->wishListRepository->delete([['id' => $wishListId]], $salesChannelContext->getContext());
    }
}
