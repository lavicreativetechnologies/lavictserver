<?php declare(strict_types=1);

namespace Ies\WishList\Service;

use Ies\WishList\Core\WishListProduct\WishListProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class WishListProductService
{
    private EntityRepositoryInterface $wishListProductRepository;

    public function __construct(EntityRepositoryInterface $wishListProductRepository)
    {
        $this->wishListProductRepository = $wishListProductRepository;
    }

    public function addProductToWishList(string $wishListId, string $productId, SalesChannelContext $salesChannelContext): void
    {
        $wishListProduct = [
            'wishListId' => $wishListId,
            'productId' => $productId
        ];

        $this->wishListProductRepository->create([$wishListProduct], $salesChannelContext->getContext());
    }

    public function removeProductFromWishList(string $wishListId, string $productId, SalesChannelContext $salesChannelContext): void
    {
        $wishListProduct = [
            'wishListId' => $wishListId,
            'productId' => $productId
        ];

        $this->wishListProductRepository->delete([$wishListProduct], $salesChannelContext->getContext());
    }

    public function hasProductFromWishList(string $wishListId, string $productId, SalesChannelContext $salesChannelContext): ?WishListProductEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('wishListId', $wishListId));
        $criteria->addFilter(new EqualsFilter('productId', $productId));

        return $this->wishListProductRepository->search($criteria, $salesChannelContext->getContext())->first();
    }

    public function getProductsFromWishList(string $wishListId, SalesChannelContext $salesChannelContext): ?EntityCollection
    {
        $searchResult = $this->searchProductFromWishList($wishListId, $salesChannelContext);

        return $searchResult->getEntities();
    }

    public function getProductCountsFromWishList(string $wishListId, SalesChannelContext $salesChannelContext): int
    {
        $searchResult = $this->searchProductFromWishList($wishListId, $salesChannelContext);

        return $searchResult->count();
    }

    private function searchProductFromWishList(string $wishListId, SalesChannelContext $salesChannelContext): EntitySearchResult
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('wishListId', $wishListId));

        return $this->wishListProductRepository->search($criteria, $salesChannelContext->getContext());
    }
}
