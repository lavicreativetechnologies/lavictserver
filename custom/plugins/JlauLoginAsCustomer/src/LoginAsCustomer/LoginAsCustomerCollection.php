<?php

namespace Jlau\LoginAsCustomer\LoginAsCustomer;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void                       add(LoginAsCustomerEntity $entity)
 * @method void                       set(string $key, LoginAsCustomerEntity $entity)
 * @method LoginAsCustomerEntity[]    getIterator()
 * @method LoginAsCustomerEntity[]    getElements()
 * @method LoginAsCustomerEntity|null get(string $key)
 * @method LoginAsCustomerEntity|null first()
 * @method LoginAsCustomerEntity|null last()
 */
class LoginAsCustomerCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return LoginAsCustomerEntity::class;
    }
}
