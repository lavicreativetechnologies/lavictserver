<?php declare(strict_types=1);

namespace Jlau\LoginAsCustomer\Exception;

class InvalidDataException extends \Exception
{
}
