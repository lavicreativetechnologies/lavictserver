<?php declare(strict_types=1);

namespace Jlau\LoginAsCustomer;

use Jlau\LoginAsCustomer\Controller\LoginAsCustomer;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Util\Random;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class JlauLoginAsCustomer extends Plugin
{
    public function activate(ActivateContext $activateContext): void
    {
        $this->addPrivileges('login_as_customer', ['jlau.customer.login-as-customer']);

        $configService = $this->container->get(SystemConfigService::class);
        $random = Random::getAlphanumericString(32);
        $configService->set('JlauLoginAsCustomer.config.secret', $random);

        $ivlen = openssl_cipher_iv_length(LoginAsCustomer::CIPHER);
        $iv = Random::getAlphanumericString($ivlen);
        $configService->set('JlauLoginAsCustomer.config.iv', $iv);
    }

    public function deactivate(DeactivateContext $deactivateContext): void
    {
        $this->removePrivileges(['jlau.customer.login-as-customer']);

        $configService = $this->container->get(SystemConfigService::class);
        $configService->delete('JlauLoginAsCustomer.config.secret');
        $configService->delete('JlauLoginAsCustomer.config.iv');
    }
}
