import './component/customer-login-button';
import './module/sw-customer/page/sw-customer-detail';

// Translations
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
