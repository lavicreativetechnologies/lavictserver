import template from './customer-login-button.html.twig';
import './customer-login-button.scss';

const { Component, Mixin } = Shopware;

Component.register('customer-login-button', {
    template,

    mixins: [
        Mixin.getByName('notification')
    ],

    inject: ['syncService'],

    props: {
        customer: {
            type: Object,
            required: true
        }
    },

    methods: {
        onClick() {
            return this.syncService.httpClient.post(
                '/jlau/login-as-customer',
                {
                    'customerId': this.customer.id
                },
                {
                    headers: this.syncService.getBasicHeaders()
                }
            )
            .then((response) => {
                if (!response.data.success) {
                    this.createNotificationError({
                        title: this.$tc('jlau-login-customer.errors.title'),
                        message: this.$tc(response.data.message)
                    });

                    return;
                }

                window.open(response.data.url, "_blank");
            });
        }
    }
});
