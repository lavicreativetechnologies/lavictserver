Shopware.Service('privileges').addPrivilegeMappingEntry({
    category: 'additional_permissions',
    parent: null,
    key: 'customers',
    roles: {
        login_as_customer: {
            privileges: ['jlau.customer.login-as-customer'],
            dependencies: []
        }
    }
});
