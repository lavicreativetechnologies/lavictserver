<?php declare(strict_types=1);

namespace Jlau\LoginAsCustomer\Controller;

use Jlau\LoginAsCustomer\Exception\InvalidDataException;
use Jlau\LoginAsCustomer\LoginAsCustomer\LoginAsCustomerEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SalesChannel\SalesChannelEntity;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LoginAsCustomer extends AbstractController
{
    private const UNAUTHORIZED_MESSAGE = 'Sorry but you are not authorized for this operation';

    public const CIPHER = 'AES-128-CTR';

    /** @var AccountService */
    private $accountService;

    /** @var EntityRepositoryInterface */
    private $customerRepository;

    /** @var EntityRepositoryInterface */
    private $loginAsCustomerRepository;

    /** @var SystemConfigService */
    private $systemConfigService;

    public function __construct(
        AccountService $accountService,
        SystemConfigService $systemConfigService,
        EntityRepositoryInterface $customerRepository,
        EntityRepositoryInterface $loginAsCustomerRepository
    ) {
        $this->accountService = $accountService;
        $this->systemConfigService = $systemConfigService;
        $this->customerRepository = $customerRepository;
        $this->loginAsCustomerRepository = $loginAsCustomerRepository;
    }

    /**
     * 1. Secured API endpoint gets called via AJAX and creates a new entry in jlau_login_as_customer
     * 2. The generated ID is encrypted and passed back as hash value to the admin
     *
     * @RouteScope(scopes={"api"})
     * @Route("/api/v{version}/jlau/login-as-customer", name="api.action.jlau.login-as-customer", methods={"POST"})
     */
    public function loginAsCustomer(RequestDataBag $dataBag, Context $context): JsonResponse
    {
        $customerId = $dataBag->get('customerId');

        try {
            $salesChannel = $this->getCustomersSalesChannel($customerId, $context);
            $domain = $this->getSalesChannelsDomain($salesChannel);
        } catch (InvalidDataException $e) {
            return new JsonResponse([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }

        $id = Uuid::randomHex();
        $secret = $this->systemConfigService->get('JlauLoginAsCustomer.config.secret');
        $iv = $this->systemConfigService->get('JlauLoginAsCustomer.config.iv');
        $encryptedId = openssl_encrypt($id, self::CIPHER, $secret, 0, $iv);

        $router = $this->container->get('router');
        $path = $router->generate(
            'storefront.jlau.login-as-customer',
            [
                'hash' => $encryptedId
            ],
            UrlGeneratorInterface::ABSOLUTE_PATH
        );

        $loginAsCustomer = [
            'id' => $id,
            'customerId' => $customerId,
        ];

        $this->loginAsCustomerRepository->create([$loginAsCustomer], $context);

        return new JsonResponse([
            'success' => true,
            'url' => $domain . $path,
        ]);
    }

    /**
     * 1. Throw error when no hash is passed
     * 2. Decrypt passed hash and search as ID in jlau_login_as_customer table
     * 3. Check if entity is unused by the "used" flag
     * 4. Check if created date of entity is not older than 10 seconds
     * 5. Login customer and set entity as used
     *
     * @RouteScope(scopes={"storefront"})
     * @Route("/jlau/login-as-customer", name="storefront.jlau.login-as-customer", methods={"GET"})
     */
    public function loginAndRedirect(Request $request, SalesChannelContext $context): Response
    {
        $hash = $request->query->get('hash');
        if (!$hash) {
            throw new UnauthorizedHttpException(self::UNAUTHORIZED_MESSAGE);
        }

        $secret = $this->systemConfigService->get('JlauLoginAsCustomer.config.secret');
        $iv = $this->systemConfigService->get('JlauLoginAsCustomer.config.iv');
        $id = openssl_decrypt($hash, self::CIPHER, $secret, 0, $iv);

        $loginAsCustomer = $this->findEntity($id, $context->getContext());
        $this->validate($loginAsCustomer);

        $this->accountService->login($loginAsCustomer->getCustomer()->getEmail(), $context);

        $this->loginAsCustomerRepository->update([
            [
                'id' => $id,
                'used' => true
            ]
        ], $context->getContext());

        $response = $this->redirectToRoute('frontend.home.page');
        // Add compatibility to plugin "Persistenter Warenkorb" from ACRIS E-Commerce GmbH
        $response->headers->clearCookie('acris_persistent_cart');
        return $response;
    }

    /**
     * @throws InvalidDataException
     */
    protected function getCustomersSalesChannel(string $customerId, Context $context): SalesChannelEntity
    {
        $criteria = new Criteria([$customerId]);
        $criteria->addAssociation('salesChannel');
        $criteria->addAssociation('salesChannel.domains');

        $searchResult = $this->customerRepository->search($criteria, $context);
        if (!$searchResult->count()) {
            throw new InvalidDataException('jlau-login-customer.errors.customer-not-found');
        }

        /** @var CustomerEntity $customer */
        $customer = $searchResult->first();
        return $customer->getSalesChannel();
    }

    /**
     * @throws InvalidDataException
     */
    protected function getSalesChannelsDomain(SalesChannelEntity $salesChannel): string
    {
        if (!$salesChannel->getActive()) {
            throw new InvalidDataException('jlau-login-customer.errors.saleschannel-inactive');
        }
        if ($salesChannel->getTypeId() !== Defaults::SALES_CHANNEL_TYPE_STOREFRONT) {
            throw new InvalidDataException('jlau-login-customer.errors.saleschannel-not-type-storefront');
        }

        $domains = $salesChannel->getDomains();
        if (!$domains) {
            throw new InvalidDataException('jlau-login-customer.errors.no-domains-in-saleschannel');
        }

        $domain = $domains->first();
        if (!$domain) {
            throw new InvalidDataException('jlau-login-customer.errors.no-domains-in-saleschannel');
        }

        return rtrim($domain->getUrl(), '/');
    }

    /**
     * @throws UnauthorizedHttpException
     */
    protected function validate(LoginAsCustomerEntity $loginAsCustomer)
    {
        if ($loginAsCustomer->isUsed()) {
            throw new UnauthorizedHttpException(self::UNAUTHORIZED_MESSAGE);
        }

        $currentTimestamp = (new \DateTime())->getTimestamp();
        if ($currentTimestamp - $loginAsCustomer->getCreatedAt()->getTimestamp() > 10) {
            throw new UnauthorizedHttpException(self::UNAUTHORIZED_MESSAGE);
        }
    }

    /**
     * @throws UnauthorizedHttpException
     */
    protected function findEntity(string $id, Context $context): LoginAsCustomerEntity
    {
        $criteria = new Criteria([$id]);
        $criteria->addFilter(
            new EqualsFilter('used', false)
        );
        $criteria->addAssociation('customer');

        /** @var LoginAsCustomerEntity|null $loginAsCustomer */
        $loginAsCustomer = $this->loginAsCustomerRepository->search($criteria, $context)->first();
        if (!$loginAsCustomer) {
            throw new UnauthorizedHttpException(self::UNAUTHORIZED_MESSAGE);
        }

        return $loginAsCustomer;
    }
}
