# LoginAsCustomer

## An integration for [Shopware 6](https://github.com/shopware/platform)

Mit diesem Plugin ist es jetzt möglich sich aus dem Admin-Bereich heraus als Kunde im Shop einzuloggen. 
Dieses Feature war bei Shopware 5 bereits integriert, wurde aber für Shopware 6 nicht umgesetzt.

Nach der Plugininstallation gibt es in der Kunden-Detailansicht einen neuen Button „Als Kunde einloggen“. Ein Klick auf den Button öffnet ein neuen Tab im Browser in dem das Frontend geöffnet wird und man automatisch als der jeweilige Kunde eingeloggt ist.

Es wird immer der Verkaufskanal geöffnet in dem der Kunde zugeordnet ist. Dieser Verkaufskanal muss vom Typ Storefront sein. Headless funktioniert in dem Falle nicht, da es mit keiner spezifischen URL verknüpft ist.
Der Verkaufskanal und der Kunde müssen aktiv sein.
