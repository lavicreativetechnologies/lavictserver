<?php declare(strict_types=1);

namespace Ies\FileSearch\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use Symfony\Component\HttpFoundation\Response;

class DownloadLizenzNotAccepted extends ShopwareHttpException
{
    public function __construct(string $fileName)
    {
        parent::__construct('Download license for file {{ fileName }} not accepted', ['fileName' => $fileName]);
    }

    public function getErrorCode(): string
    {
        return 'CONTENT__IES_FEIL_SEARCH_DOWNLOAD_LICENSE_NOT_ACCEPTED';
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_FORBIDDEN;
    }
}
