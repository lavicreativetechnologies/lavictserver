<?php declare(strict_types=1);

namespace Ies\FileSearch\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use Symfony\Component\HttpFoundation\Response;

class FileNotFoundException extends ShopwareHttpException
{
    public function __construct(string $fileName)
    {
        parent::__construct('Cannot find file {{ fileName }}', ['fileName' => $fileName]);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }

    public function getErrorCode(): string
    {
        return 'CONTENT__IES_FEIL_SEARCH_FILE_NOT_FOUND';
    }
}
