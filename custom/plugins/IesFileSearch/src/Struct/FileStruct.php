<?php declare(strict_types=1);

namespace Ies\FileSearch\Struct;

use Shopware\Core\Framework\Struct\Struct;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class FileStruct extends Struct
{
    public array $files = [];

    public function getFiles(): array
    {
        return $this->files;
    }

    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    public function hasFiles(): bool
    {
        foreach ($this->files as $extension => $files) {
            if (!empty($files)) {
                return true;
            }
        }

        return false;
    }

    public function count(): int
    {
        $count = 0;
        foreach ($this->files as $extension => $files) {
            $count += count($files);
        }
        return $count;
    }
}
