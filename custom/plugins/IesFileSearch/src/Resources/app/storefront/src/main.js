import ConfirmedDownloadPlugin from './script/plugin/confirmed-download.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('ConfirmedDownload', ConfirmedDownloadPlugin, '[data-confirmed-download]');

