import Plugin from 'src/plugin-system/plugin.class';

export default class ConfirmedDownloadPlugin extends Plugin {

    init() {
        const form = this.el;
        const dialog = form.closest('.modal');

        this.el.addEventListener('submit', () => {
            $(dialog).modal('hide');
        })

        dialog.classList.add('confirmed-download-dialog')
    }

}
