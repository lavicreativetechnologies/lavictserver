import template from './sw-cms-el-confirmed-download.html.twig';
import './sw-cms-el-confirmed-download.scss';

const {Component, Mixin} = Shopware;

Component.register('sw-cms-el-confirmed-download', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],
});
