import './preview'
import './component'

Shopware.Service('cmsService').registerCmsElement({
    name: 'confirmed-download',
    label: 'sw-cms.elements.confirmedDownload.label',
    component: 'sw-cms-el-confirmed-download',
    previewComponent: 'sw-cms-el-preview-confirmed-download',
});
