import template from './sw-cms-el-preview-confirmed-download.html.twig';
import './sw-cms-el-preview-confirmed-download.scss';

Shopware.Component.register('sw-cms-el-preview-confirmed-download', {
    template
});
