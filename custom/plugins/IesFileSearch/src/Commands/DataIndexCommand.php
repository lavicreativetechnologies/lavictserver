<?php declare(strict_types=1);

namespace Ies\FileSearch\Commands;

use Ies\FileSearch\Services\IndexInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DataIndexCommand extends Command
{
    protected static $defaultName = 'ies:index:data';

    private IndexInterface $indexer;

    public function __construct(IndexInterface $indexer)
    {
        parent::__construct();

        $this->indexer = $indexer;
    }

    protected function configure()
    {
        $this->setDescription('Builds the data index (datenblaetter, unterlagen, video, zeichnungen)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->indexer->build();

        $io->success('Data successfully indexed');
    }
}
