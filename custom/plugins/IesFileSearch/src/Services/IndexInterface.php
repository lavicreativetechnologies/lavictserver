<?php declare(strict_types=1);

namespace Ies\FileSearch\Services;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
interface IndexInterface
{
    /**
     * Builds the index data
     *
     * @var bool $cleanUp False, the database will not truncated, otherwise it will.
     *
     * @return void
     */
    public function build(bool $cleanUp = true);
}
