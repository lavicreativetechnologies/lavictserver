<?php declare(strict_types=1);

namespace Ies\FileSearch\Services;

use Doctrine\DBAL\Connection;
use Ies\FileSearch\Components\DirectoryIteratorManager;
use Ies\FileSearch\Core\File\FileEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use SplFileInfo;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DataIndexer implements IndexInterface
{
    /** @var DirectoryIteratorManager */
    private DirectoryIteratorManager $iteratorManager;

    /** @var EntityRepositoryInterface */
    private $repository;

    /** @var Connection */
    private Connection $connection;

    protected string $publicPath = '';

    public function __construct(
        DirectoryIteratorManager $iteratorManager,
        EntityRepositoryInterface $repository,
        Connection $connection,
        string $publicPath = ''
    ) {
        $this->iteratorManager = $iteratorManager;
        $this->repository = $repository;
        $this->connection = $connection;
        $this->publicPath = $publicPath;
    }

    public function build(bool $cleanUp = true): void
    {
        if ($cleanUp) {
            $this->cleanIndex();
        }

        $this->processCategoryFiles($this->iteratorManager->getVideoDirectoryIterator());
        $this->processCategoryFiles($this->iteratorManager->getApprovalDirectoryIterator());
        $this->processProductFiles();
    }

    protected function processCategoryFiles(\RecursiveDirectoryIterator $directories): void
    {
        $categoryIds = $this->getCategoryIds();

        $filteredDirectories = $this->iteratorManager->filterByCategoryId($directories, $categoryIds);

        foreach ($filteredDirectories as $directory) {
            $files = $this->iteratorManager->getFilesFromFilter($directory);

            /** @var SplFileInfo $file */
            foreach ($files as $file) {
                $extension = strtolower($file->getExtension());

                if (!in_array($extension, ['jpg', 'mp4', 'pdf'])) {
                    continue;
                }

                $entity = new FileEntity();
                $entity->setFilepath($this->getRelativePath($file->getPathname()));
                $entity->setFilename($file->getFilename());
                $entity->setFileExtension($file->getExtension());

                switch ($extension) {
                    case 'jpg':
                        $entity->setKind('thumb');
                        break;
                    case 'mp4':
                        $entity->setKind('video');
                        break;
                    case 'dwg':
                    case 'dxf':
                    case 'igs':
                    case 'step':
                    case 'x_t':
                    case 'pdf':
                        $entity->setKind('zulassungen');
                        break;
                }

                $this->repository->upsert([
                    $entity->jsonSerialize()
                ], Context::createDefaultContext());
            }
        }
    }

    protected function processProductFiles(): void
    {
        $detailOrdernumbers = $this->getDetailOrdernumbers();

        $chunk = array_chunk($detailOrdernumbers, 20);

        foreach ($chunk as $ordernumbers) {
            $allFiles = $this->iteratorManager->getAllByOrdernumbers($ordernumbers);

            /** @var SplFileInfo $file */
            foreach ($allFiles as $file) {
                $entity = new FileEntity();
                // This if-else is needed for directory "unterlagen". This files do not only contain the ordernumber!
                // It isn`t a good fix, but a quick one
                if (stripos($file->getPathname(), '/unterlagen/') !== false) {
                    preg_match('/([^_]+)/', $file->getBasename(), $matchResult);

                    $ordernumber = $matchResult[0] ?: $file->getBasename('.' . $file->getExtension());
                    $entity->setProductNumber($ordernumber);
                } else {
                    $entity->setProductNumber($file->getBasename('.' . $file->getExtension()));
                }

                $entity->setFilepath($this->getRelativePath($file->getPathname()));
                $entity->setFilename($file->getFilename());
                $entity->setKind(basename($file->getPath()));
                $entity->setFileExtension($file->getExtension());

                $this->repository->upsert([
                    $entity->jsonSerialize()
                ], Context::createDefaultContext());
            }
        }
    }

    protected function cleanIndex(): void
    {
        $this->connection->executeQuery('TRUNCATE ies_files');
    }

    private function getCategoryIds(): array
    {
        $qb = $this->connection->createQueryBuilder();
        $query = $qb->select('id')->from('category');

        return $query->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    private function getDetailOrdernumbers(): array
    {
        $qb = $this->connection->createQueryBuilder();
        $query = $qb->select('product_number')->from('product');

        return $query->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function getRelativePath(string $path): string
    {
        return str_replace($this->publicPath, '', $path);
    }
}
