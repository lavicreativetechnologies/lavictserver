<?php declare(strict_types=1);

namespace Ies\FileSearch\Services;

use DateTime;
use Ies\FileSearch\Components\BinaryFileResponse;
use Ies\FileSearch\Events\CADLizenzAgreementCustomerEmailEvent;
use Ies\FileSearch\Events\CADLizenzAgreementEmailEvent;
use Ies\FileSearch\Events\CADLizenzAgreementSupportEmailEvent;
use Ies\FileSearch\Exception\DownloadLizenzNotAccepted;
use Ies\FileSearch\Exception\FileNotFoundException;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Validation\DataBag\DataBag;
use Shopware\Core\Framework\Validation\Exception\ConstraintViolationException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ArchiveManager
{
    private string $globalArchivePath;

    private SystemConfigService $configService;

    private EventDispatcherInterface $eventDispatcher;

    private LoggerInterface $logger;

    public function __construct(
        string $globalArchivePath,
        SystemConfigService $configService,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger
    ) {
        $this->globalArchivePath = $globalArchivePath;
        $this->configService = $configService;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    public function denyAccessUnlessLicenseInactive(string $fileName, SalesChannelContext $salesChannelContext): void
    {
        if ($this->isLizenzActive($salesChannelContext)) {
            throw new DownloadLizenzNotAccepted($fileName);
        }
    }

    public function isLizenzActive(SalesChannelContext $salesChannelContext): bool
    {
        $confKey = 'FileSearch.config.zipLizenzAgreementInfoPage';
        $lizenzWidgetId = $this->getConfigVar($confKey, $salesChannelContext);

        return !empty($lizenzWidgetId);
    }

    private function getConfigVar(string $varName, SalesChannelContext $salesChannelContext)
    {
        $salesChannelId = $salesChannelContext->getSalesChannel()->getId();

        return $this->configService->get($varName, $salesChannelId) ?? '';
    }

    public function download(string $fileName): BinaryFileResponse
    {
        $cleanFileName = basename($fileName);
        $globalFilePath = $this->globalArchivePath . '/' . $cleanFileName;

        if (!file_exists($globalFilePath)) {
            throw new FileNotFoundException($fileName);
        }

        $response = new BinaryFileResponse($globalFilePath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);

        return $response;
    }

    public function sendNotifications(string $fileName, SalesChannelContext $salesChannelContext): self
    {
        $data = $this->getMailData($fileName, $salesChannelContext);

        $salesChannelId = $salesChannelContext->getSalesChannel()->getId();
        $context = $salesChannelContext->getContext();

        $configVar = 'FileSearch.config.zipLizenzAgreementSupportMail';
        $email = $this->getConfigVar($configVar, $salesChannelContext);
        $recipients = $this->wrapRecipients($email);
        $event = new CADLizenzAgreementSupportEmailEvent($context, $salesChannelId, $recipients, $data);
        $this->dispatch($event);

        $email = $salesChannelContext->getCustomer()->getEmail();
        $recipients = $this->wrapRecipients($email);
        $event = new CADLizenzAgreementCustomerEmailEvent($context, $salesChannelId, $recipients, $data);
        $this->dispatch($event);

        return $this;
    }

    private function getMailData(string $fileName, SalesChannelContext $salesChannelContext): DataBag
    {
        $data = [
            'file' => $fileName,
            'date' => new DateTime(),
            'customer' => $salesChannelContext->getCustomer(),
        ];

        return new DataBag($data);
    }

    private function wrapRecipients(...$recipients): MailRecipientStruct
    {
        return new MailRecipientStruct(array_combine($recipients, $recipients));
    }

    private function dispatch(CADLizenzAgreementEmailEvent $event): void
    {
        try {
            $this->eventDispatcher->dispatch($event);
        } catch (ConstraintViolationException $exception) {
            $this->logger->error($exception->getMessage());
            $this->logger->debug($exception);
        }
    }
}
