<?php declare(strict_types=1);

namespace Ies\FileSearch\Services;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DataLoader
{
    /**
     * @var Connection
     */
    private Connection $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function loadByQuery(string $query): array
    {
        $qb = $this->getFilesQueryBuilder();
        $qb->where($qb->expr()->like('f.filepath', ':query'))
            ->setParameter('query', '%' . $query . '%');

        $data = $qb->execute()->fetchAll();

        return $this->formatOutput($data);
    }

    public function loadDrawingsByOrdernumber(string $productNumber): array
    {
        return $this->loadByProductNumberAndKind($productNumber, 'zeichnungen');
    }

    public function loadDatasheetsByOrdernumber(string $productNumber): array
    {
        return $this->loadByProductNumberAndKind($productNumber, 'datenblaetter');
    }

    public function loadDocumentsByOrdernumber(string $productNumber): array
    {
        return $this->loadByProductNumberAndKind($productNumber, 'unterlagen');
    }

    public function loadApprovalsByArticleId(string $articleId): array
    {
        throw new \RuntimeException('Load kind `zulassungen` by category ident');
    }

    public function loadVideosByCategoryId(string $categoryId): array
    {
        throw new \RuntimeException('Load kind `video` by category ident');
    }

    protected function loadByProductNumberAndKind(string $productNumber, string $kind): array
    {
        $qb = $this->getFilesQueryBuilder();
        $qb->where('f.product_number = :productNumber')
            ->andWhere($qb->expr()->eq('f.kind', ':kind'))
            ->setParameter('productNumber', $productNumber)
            ->setParameter('kind', $kind);

        return $qb->execute()->fetchAll();
    }

    protected function getFilesQueryBuilder(): QueryBuilder
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('f.*')->from('ies_files', 'f');

        return $qb;
    }

    protected function formatOutput(array $loadedData): array
    {
        $outputFiles = [];

        foreach ($loadedData as &$file) {
            $file['name'] = @basename($file['filepath']);

            $extension = strtolower($file['file_extension']);

            $outputFiles[$extension][] = $file;
        }

        return $outputFiles;
    }
}
