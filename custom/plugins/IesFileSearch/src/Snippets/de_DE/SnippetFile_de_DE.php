<?php declare(strict_types=1);

namespace Ies\FileSearch\Snippets\de_DE;

use Shopware\Core\System\Snippet\Files\SnippetFileInterface;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class SnippetFile_de_DE implements SnippetFileInterface
{
    public function getName(): string
    {
        return 'messages.de-DE';
    }

    public function getPath(): string
    {
        return __DIR__ . '/messages.de-DE.json';
    }

    public function getIso(): string
    {
        return 'de-DE';
    }

    public function getAuthor(): string
    {
        return 'isento eCommerce solutions GmbH';
    }

    public function isBase(): bool
    {
        return true;
    }
}
