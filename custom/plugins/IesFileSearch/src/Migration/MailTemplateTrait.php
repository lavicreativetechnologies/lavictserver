<?php declare(strict_types=1);

namespace Ies\FileSearch\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Uuid\Exception\InvalidUuidException;
use Shopware\Core\Framework\Uuid\Uuid;

/**
 * @author    Lei Wang <lei.wang@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
trait MailTemplateTrait
{
    /**
     * @throws DBALException
     */
    protected function autoInsertAllRelations(Connection $connection, array $mailConfig): void
    {
        if (!$this->mailTemplateTypeExists($connection, $mailConfig['type'])) {
            $this->insertMailTemplateType($connection, $mailConfig);
        }

        if (!$this->mailTemplateExists($connection, $mailConfig['type'])) {
            $this->insertMailTemplate($connection, $mailConfig);
        }

        if (!$this->eventActionExists($connection, $mailConfig['event'])) {
            $this->insertEventAction($connection, $mailConfig);
        }
    }

    /**
     * @throws DBALException
     * @throws InvalidUuidException
     */
    protected function insertMailTemplateType(Connection $connection, array $mailTemplateConfig): void
    {
        $mailTemplateTypeId = Uuid::fromHexToBytes($mailTemplateConfig['id']);

        $connection->insert(
            'mail_template_type',
            [
                'id' => $mailTemplateTypeId,
                'technical_name' => $mailTemplateConfig['type'],
                'available_entities' => $mailTemplateConfig['availableEntities'],
                'created_at' => date(Defaults::STORAGE_DATE_FORMAT),
            ]
        );

        $connection->insert(
            'mail_template_type_translation',
            [
                'mail_template_type_id' => $mailTemplateTypeId,
                'name' => $mailTemplateConfig['description'],
                'language_id' => $this->getLanguageIdByLocale($connection, 'en-GB'),
                'created_at' => date(Defaults::STORAGE_DATE_FORMAT),
            ]
        );

        $connection->insert(
            'mail_template_type_translation',
            [
                'mail_template_type_id' => $mailTemplateTypeId,
                'name' => $mailTemplateConfig['descriptionDe'],
                'language_id' => $this->getLanguageIdByLocale($connection, 'de-DE'),
                'created_at' => date(Defaults::STORAGE_DATE_FORMAT),
            ]
        );
    }

    /**
     * @throws DBALException
     * @throws InvalidUuidException
     */
    protected function insertMailTemplate(Connection $connection, array $mailTemplateConfig): void
    {
        $mailTemplateId = Uuid::randomBytes();
        $connection->insert(
            'mail_template',
            [
                'id' => $mailTemplateId,
                'mail_template_type_id' => Uuid::fromHexToBytes($mailTemplateConfig['id']),
                'system_default' => 1,
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            ]
        );

        $connection->insert(
            'mail_template_translation',
            [
                'subject' => $mailTemplateConfig['subject'],
                'description' => $mailTemplateConfig['description'],
                'sender_name' => '{{ salesChannel.name }}',
                'content_html' => $this->getHtmlTemplateEn(),
                'content_plain' => $this->getPlainTemplateEn(),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                'mail_template_id' => $mailTemplateId,
                'language_id' => $this->getLanguageIdByLocale($connection, 'en-GB'),
            ]
        );

        $connection->insert(
            'mail_template_translation',
            [
                'subject' => $mailTemplateConfig['subjectDe'],
                'description' => $mailTemplateConfig['descriptionDe'],
                'sender_name' => '{{ salesChannel.name }}',
                'content_html' => $this->getHtmlTemplateDe(),
                'content_plain' => $this->getPlainTemplateDe(),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                'mail_template_id' => $mailTemplateId,
                'language_id' => $this->getLanguageIdByLocale($connection, 'de-DE'),
            ]
        );
    }

    /**
     * @throws DBALException
     */
    protected function insertEventAction(Connection $connection, array $mailTemplateConfig): void
    {
        $connection->insert(
            'event_action',
            [
                'id' => Uuid::randomBytes(),
                'event_name' => $mailTemplateConfig['event'],
                'action_name' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
                'config' => json_encode(['mail_template_type_id' => $mailTemplateConfig['id']]),
                'created_at' => date(Defaults::STORAGE_DATE_FORMAT),
            ]
        );
    }

    protected function getLanguageIdByLocale(Connection $connection, string $locale)
    {
        $sql = <<<SQL
SELECT `language`.`id`
FROM `language`
INNER JOIN `locale` ON `locale`.`id` = `language`.`locale_id`
WHERE `locale`.`code` = :code
SQL;

        return $connection->executeQuery($sql, [
            'code' => $locale,
        ])->fetchColumn();
    }

    protected function mailTemplateTypeExists(Connection $connection, $technicalName)
    {
        $sql = <<<SQL
SELECT LOWER(Hex(`id`))
FROM `mail_template_type`
WHERE `technical_name` = :technicalName
SQL;

        return $connection->executeQuery($sql, [
            'technicalName' => $technicalName,
        ])->fetchColumn();
    }

    protected function mailTemplateExists(Connection $connection, $mailType)
    {
        $sql = <<<SQL
    SELECT `mail_template`.`id`
    FROM `mail_template` LEFT JOIN `mail_template_type` ON `mail_template`.`mail_template_type_id` = `mail_template_type`.id
    WHERE `mail_template_type`.`technical_name` = :technical_name
SQL;

        return $connection->executeQuery($sql, [
            'technical_name' => $mailType,
        ])->fetchColumn();
    }

    protected function eventActionExists(Connection $connection, $eventName)
    {
        $sql = <<<SQL
SELECT LOWER(Hex(`id`))
FROM `event_action`
WHERE `event_name` = :eventName
AND `action_name` = :actionName
SQL;

        return $connection->executeQuery($sql, [
            'eventName' => $eventName,
            'actionName' => 'action.mail.send',
        ])->fetchColumn();
    }

    protected function loadContent(string $filename): string
    {
        return file_get_contents(__DIR__ . '/mails/' . $filename);
    }

    protected function getHtmlTemplateEn(): string
    {
        return $this->getHtmlTemplateDe();
    }

    protected function getPlainTemplateEn(): string
    {
        return $this->getPlainTemplateDe();
    }

    abstract protected function getHtmlTemplateDe(): string;

    abstract protected function getPlainTemplateDe(): string;
}
