<?php declare(strict_types=1);

namespace Ies\FileSearch\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1587532356 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1587532356;
    }

    public function update(Connection $connection): void
    {
        $connection->executeQuery('
            CREATE TABLE IF NOT EXISTS `ies_files` (
                `id` BINARY(16) NOT NULL,
                `filepath` varchar(300) NOT NULL,
                `filename` varchar(100) NOT NULL DEFAULT "",
                `kind` varchar(30) NOT NULL DEFAULT "",
                `file_extension` varchar(5) NOT NULL DEFAULT "",
                `product_number` varchar(255) DEFAULT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
