<?php declare(strict_types=1);

namespace Ies\FileSearch\Migration;

use Doctrine\DBAL\Connection;
use Ies\FileSearch\Events\CADLizenzAgreementSupportEmailEvent;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1611930636LizenzAgreementSupportMails extends MigrationStep
{
    use MailTemplateTrait;

    public function getCreationTimestamp(): int
    {
        return 1611930636;
    }

    public function update(Connection $connection): void
    {
        $eventName = CADLizenzAgreementSupportEmailEvent::EVENT_NAME;

        $this->autoInsertAllRelations($connection, [
            'id' => '49716b6d0ae349fda3bf1f0041efe596',
            'description' => 'CAD-Download (mail to shop owner)',
            'descriptionDe' => 'CAD-Download (Email an Shop-Betreiber)',
            'subject' => 'CAD-Download Lizenz Agreement accepted',
            'subjectDe' => 'CAD-Download vom Shop - Nutzungsvereinbarungsvertrag akzeptiert',
            'type' => str_replace('.', '_', $eventName),
            'event' => $eventName,
            'availableEntities' => \json_encode(['salesChannel' => 'sales_channel']),
        ]);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    protected function getHtmlTemplateDe(): string
    {
        return $this->loadContent('download_support.html.twig');
    }

    protected function getPlainTemplateDe(): string
    {
        return $this->loadContent('download_support.plain.twig');
    }
}
