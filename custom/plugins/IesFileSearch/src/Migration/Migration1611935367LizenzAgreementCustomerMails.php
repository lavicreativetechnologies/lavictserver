<?php declare(strict_types=1);

namespace Ies\FileSearch\Migration;

use Doctrine\DBAL\Connection;
use Ies\FileSearch\Events\CADLizenzAgreementCustomerEmailEvent;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1611935367LizenzAgreementCustomerMails extends MigrationStep
{
    use MailTemplateTrait;

    public function getCreationTimestamp(): int
    {
        return 1611935367;
    }

    public function update(Connection $connection): void
    {
        $eventName = CADLizenzAgreementCustomerEmailEvent::EVENT_NAME;

        $this->autoInsertAllRelations($connection, [
            'id' => '38e71472ca064ef69b46c52a3872332f',
            'description' => 'CAD-Download (mail to customer)',
            'descriptionDe' => 'CAD-Download (Email an Shop-Nutzer)',
            'subject' => 'CAD-Download Lizenz Agreement',
            'subjectDe' => 'CAD-Download - Nutzungsvereinbarungsvertra',
            'type' => str_replace('.', '_', $eventName),
            'event' => $eventName,
            'availableEntities' => \json_encode(['salesChannel' => 'sales_channel']),
        ]);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    protected function getHtmlTemplateDe(): string
    {
        return $this->loadContent('download_customer.html.twig');
    }

    protected function getPlainTemplateDe(): string
    {
        return $this->loadContent('download_customer.plain.twig');
    }
}
