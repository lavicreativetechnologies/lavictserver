<?php declare(strict_types=1);

namespace Ies\FileSearch\Subscriber;

use Ies\FileSearch\Struct\FileStruct;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ies\FileSearch\Services\DataLoader;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DetailSubscriber implements EventSubscriberInterface
{
    /**
     * @var DataLoader
     */
    private DataLoader $dataLoader;

    /**
     * @param DataLoader $dataLoader
     */
    public function __construct(DataLoader $dataLoader)
    {
        $this->dataLoader = $dataLoader;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductPageLoadedEvent::class => 'onProductPageLoaded',
        ];
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event)
    {
        $page = $event->getPage();

        $product = $page->getProduct();
        $productNumber = $product->getProductNumber();

        $drawings = $this->dataLoader->loadDrawingsByOrdernumber($productNumber);
        $datasheets = $this->dataLoader->loadDatasheetsByOrdernumber($productNumber);
        $documents = $this->dataLoader->loadDocumentsByOrdernumber($productNumber);
        //$approvals = $this->dataLoader->loadApprovalsByArticleId($productId);
        //$videos = $this->dataLoader->loadVideosByCategoryId($categoryId);

        $fileStruct = new FileStruct();
        $fileStruct->setFiles([
            'datenblaetter' => $datasheets,
            'unterlagen' => $documents,
            'zeichnungen' => $drawings,
            //'zulassungen' => $approvals,
            //'videos' => $videos
        ]);

        $product->addExtension('ies_files', $fileStruct);

        $page->setProduct($product);
    }
}
