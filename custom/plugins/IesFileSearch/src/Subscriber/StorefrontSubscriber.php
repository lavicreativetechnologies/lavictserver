<?php declare(strict_types=1);

namespace Ies\FileSearch\Subscriber;

use Ies\RightsManagement\Services\RightsManagementService;
use Shopware\Storefront\Event\StorefrontRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class StorefrontSubscriber implements EventSubscriberInterface
{
    private const PERMISSION_DOWNLOAD_VISIBLE = 'download_category_visible';

    private RightsManagementService $rightsManagementService;

    public function __construct(RightsManagementService $rightsManagementService)
    {
        $this->rightsManagementService = $rightsManagementService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            StorefrontRenderEvent::class => 'onStorefrontRender',
        ];
    }

    public function onStorefrontRender(StorefrontRenderEvent $event): void
    {
        $hasPermissionDownload = $this->rightsManagementService->hasPermission(
            self::PERMISSION_DOWNLOAD_VISIBLE,
            $event->getSalesChannelContext()
        );

        $event->setParameter('hasPermissionDownload', $hasPermissionDownload);
    }
}
