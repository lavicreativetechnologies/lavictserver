<?php declare(strict_types=1);

namespace Ies\FileSearch\Storefront\Controller;

use Ies\FileSearch\Services\ArchiveManager;
use Ies\FileSearch\Services\DataLoader;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Shopware\Storefront\Page\GenericPageLoaderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 *
 * @RouteScope(scopes={"storefront"})
 */
class FilesSearchController extends StorefrontController
{
    private DataLoader $dataLoader;

    private GenericPageLoaderInterface $genericPageLoader;

    private ArchiveManager $archiveManager;

    public function __construct(
        DataLoader $dataLoader,
        GenericPageLoaderInterface $genericPageLoader,
        ArchiveManager $archiveManager
    ) {
        $this->dataLoader = $dataLoader;
        $this->genericPageLoader = $genericPageLoader;
        $this->archiveManager = $archiveManager;
    }

    /**
     * @Route("/feldmann/files", name="frontend.ies_files.search", options={"seo"="false"}, methods={"GET", "POST"})
     */
    public function search(Request $request, SalesChannelContext $context): Response
    {
        $this->denyAccessUnlessLoggedIn();

        $files = [];
        $query = $request->get('query', '');

        if ($query !== '') {
            $files = $this->dataLoader->loadByQuery($query);
        }

        $page = $this->genericPageLoader->load($request, $context);

        return $this->renderStorefront('@Storefront/storefront/page/file-search/index.html.twig', [
            'page' => $page,
            'query' => $query,
            'files' => $files,
            'isArchiveLizenzActive' => $this->archiveManager->isLizenzActive($context),
        ]);
    }

    /**
     * @Route("/feldmann/files/accept-lizenz", name="fronted.ies_files.accept_lizenz", options={"seo"="false"}, methods={"POST"}, defaults={"XmlHttpRequest"=true})
     */
    public function downloadLicensedArchive(Request $request, SalesChannelContext $context): Response
    {
        $this->denyAccessUnlessLoggedIn();

        $fileName = $request->get('file');

        return $this->archiveManager
            ->sendNotifications($fileName, $context)
            ->download($fileName);
    }


    /**
     * @Route("/feldmann/files/archive", name="fronted.ies_files.get_archive", options={"seo"="false"}, methods={"GET"})
     */
    public function downloadArchive(Request $request, SalesChannelContext $context): Response
    {
        $this->denyAccessUnlessLoggedIn();

        $fileName = $request->get('file');

        $this->archiveManager->denyAccessUnlessLicenseInactive($fileName, $context);

        return $this->archiveManager->download($fileName);
    }
}
