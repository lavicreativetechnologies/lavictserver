<?php declare(strict_types=1);

namespace Ies\FileSearch\Events;

class CADLizenzAgreementCustomerEmailEvent extends CADLizenzAgreementEmailEvent
{
    public const EVENT_NAME = 'ies.cad.agreement.mail.customer';

    public function getName(): string
    {
        return self::EVENT_NAME;
    }
}
