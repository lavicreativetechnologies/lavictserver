<?php declare(strict_types=1);

namespace Ies\FileSearch\Events;

class CADLizenzAgreementSupportEmailEvent extends CADLizenzAgreementEmailEvent
{
    public const EVENT_NAME = 'ies.cad.agreement.mail.support';

    public function getName(): string
    {
        return self::EVENT_NAME;
    }
}
