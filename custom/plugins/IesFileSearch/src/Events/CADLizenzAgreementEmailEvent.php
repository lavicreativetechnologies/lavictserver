<?php declare(strict_types=1);

namespace Ies\FileSearch\Events;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\BusinessEventInterface;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\MailActionInterface;
use Shopware\Core\Framework\Validation\DataBag\DataBag;
use Symfony\Contracts\EventDispatcher\Event;

abstract class CADLizenzAgreementEmailEvent extends Event implements BusinessEventInterface, MailActionInterface
{
    public const EVENT_NAME = 'ies.customer.password_email';

    private Context $context;

    private string $salesChannelId;

    private MailRecipientStruct $recipients;

    private array $mailData;

    public function __construct(Context $context, string $salesChannelId, MailRecipientStruct $recipients, DataBag $mailData)
    {
        $this->context = $context;
        $this->salesChannelId = $salesChannelId;
        $this->recipients = $recipients;
        $this->mailData = $mailData->all();
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())->add('data', new ObjectType());
    }

    public function getContext(): Context
    {
        return $this->context;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        return $this->recipients;
    }

    public function getSalesChannelId(): string
    {
        return $this->salesChannelId;
    }

    public function getData(): array
    {
        return $this->mailData;
    }
}
