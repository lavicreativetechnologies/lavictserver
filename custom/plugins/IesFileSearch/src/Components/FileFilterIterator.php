<?php declare(strict_types=1);

namespace Ies\FileSearch\Components;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class FileFilterIterator extends DirectoryFilterIterator
{
    /**
     * @return bool
     */
    public function accept()
    {
        $file = $this->current();

        if ($file->isDir()) {
            return false;
        }

        if (is_array($this->filterKey)) {
            return in_array($file->getBasename('.' . $file->getExtension()), $this->filterKey);
        }

        return $file->getBasename('.' . $file->getExtension()) === $this->filterKey;
    }
}
