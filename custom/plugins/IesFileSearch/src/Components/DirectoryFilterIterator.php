<?php declare(strict_types=1);

namespace Ies\FileSearch\Components;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DirectoryFilterIterator extends \RecursiveFilterIterator
{
    /**
     * @var string|array
     */
    protected $filterKey;

    /**
     * @param \RecursiveIterator $iterator
     * @param string|array       $filterKey
     */
    public function __construct(\RecursiveIterator $iterator, $filterKey)
    {
        $this->filterKey = $filterKey;

        parent::__construct($iterator);
    }

    /**
     * @return bool
     */
    public function accept()
    {
        if (is_array($this->filterKey)) {
            return in_array($this->current()->getFilename(), $this->filterKey);
        }

        return $this->current()->getFilename() === $this->filterKey;
    }
}
