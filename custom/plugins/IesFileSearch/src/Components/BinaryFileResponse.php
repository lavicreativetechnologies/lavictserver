<?php declare(strict_types=1);

namespace Ies\FileSearch\Components;

use Shopware\Storefront\Framework\Csrf\CsrfPlaceholderHandler;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;

/**
 * This class is one hack for shopware download problem.
 * Error: "The content cannot be set on a BinaryFileResponse instance." if used default controller method for downloads.
 *
 * @see ControllerTrait::file()
 * @see CsrfPlaceholderHandler::replaceCsrfToken()
 */
class BinaryFileResponse extends \Symfony\Component\HttpFoundation\BinaryFileResponse
{
    public function getContent()
    {
        return '';
    }

    public function setContent($content)
    {
        return $this;
    }
}
