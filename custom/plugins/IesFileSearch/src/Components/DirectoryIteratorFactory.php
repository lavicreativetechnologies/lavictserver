<?php declare(strict_types=1);

namespace Ies\FileSearch\Components;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DirectoryIteratorFactory
{
    /**
     * @param string $dir
     *
     * @return \RecursiveDirectoryIterator
     */
    public function create($dir)
    {
        return new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
    }

    /**
     * @param \RecursiveIterator $recursiveIterator
     * @param array              $filterKeys
     *
     * @return DirectoryFilterIterator
     */
    public function createDirectoryFilter(\RecursiveIterator $recursiveIterator, $filterKeys = [])
    {
        return new DirectoryFilterIterator($recursiveIterator, $filterKeys);
    }

    /**
     * @param \RecursiveIterator $recursiveIterator
     * @param array              $filterKeys
     *
     * @return DirectoryFilterIterator
     */
    public function createFileFilter(\RecursiveIterator $recursiveIterator, $filterKeys = [])
    {
        return new FileFilterIterator($recursiveIterator, $filterKeys);
    }

    /**
     * @param \RecursiveIterator $recursiveIterator
     * @param array              $filterKeys
     *
     * @return DirectoryFilterIterator
     */
    public function createDocumentFilter(\RecursiveIterator $recursiveIterator, $filterKeys = [])
    {
        return new DocumentFilterInterator($recursiveIterator, $filterKeys);
    }
}
