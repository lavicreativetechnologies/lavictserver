<?php declare(strict_types=1);

namespace Ies\FileSearch\Components;

use RecursiveDirectoryIterator;
use SplFileInfo;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DirectoryIteratorManager
{
    /** @var DirectoryIteratorFactory */
    protected $factory;

    /** @var string */
    protected $videoDir;

    /** @var string */
    protected $datasheetDir;

    /** @var string */
    protected $documentDir;

    /** @var string */
    protected $drawingDir;

    /** @var string */
    protected $approvalDir;

    public function __construct(
        DirectoryIteratorFactory $factory,
        string $videoDir,
        string $datasheetDir,
        string $documentDir,
        string $drawingDir,
        string $approvalDir
    ) {
        $this->factory = $factory;

        $this->videoDir = $videoDir;
        $this->datasheetDir = $datasheetDir;
        $this->documentDir = $documentDir;
        $this->drawingDir = $drawingDir;
        $this->approvalDir = $approvalDir;
    }

    /**
     * @param \RecursiveIterator $recursiveIterator
     * @param array              $categoryIds
     *
     * @return DirectoryFilterIterator
     */
    public function filterByCategoryId(\RecursiveIterator $recursiveIterator, $categoryIds = [])
    {
        return $this->factory->createDirectoryFilter($recursiveIterator, $categoryIds);
    }

    /**
     * @param \RecursiveIterator $recursiveIterator
     * @param array              $ordernumbers
     *
     * @return DirectoryFilterIterator
     */
    public function filterByOrdernumbers(\RecursiveIterator $recursiveIterator, $ordernumbers = [])
    {
        return $this->factory->createDirectoryFilter($recursiveIterator, $ordernumbers);
    }

    /**
     * @param array $ordernumbers
     *
     * @return array
     */
    public function getAllByOrdernumbers($ordernumbers = [])
    {
        $datasheetFilter = $this->factory->createFileFilter($this->getDatasheetDirectoryIterator(), $ordernumbers);
        $documentFilter = $this->factory->createDocumentFilter($this->getDocumentDirectoryIterator(), $ordernumbers);
        $drawingFilter = $this->factory->createFileFilter($this->getDrawingDirectoryIterator(), $ordernumbers);
        $approvalFilter = $this->factory->createFileFilter($this->getApprovalDirectoryIterator(), $ordernumbers);

        return iterator_to_array($datasheetFilter)
            + iterator_to_array($documentFilter)
            + iterator_to_array($drawingFilter)
            + iterator_to_array($approvalFilter);
    }

    /**
     * @param SplFileInfo $dir
     *
     * @return RecursiveDirectoryIterator
     */
    public function getFilesFromFilter(SplFileInfo $dir)
    {
        return $this->factory->create($dir->getPathname());
    }

    /**
     * @return RecursiveDirectoryIterator
     */
    public function getVideoDirectoryIterator()
    {
        return $this->factory->create($this->videoDir);
    }

    /**
     * @return RecursiveDirectoryIterator
     */
    public function getDatasheetDirectoryIterator()
    {
        return $this->factory->create($this->datasheetDir);
    }

    /**
     * @return RecursiveDirectoryIterator
     */
    public function getDocumentDirectoryIterator()
    {
        return $this->factory->create($this->documentDir);
    }

    /**
     * @return RecursiveDirectoryIterator
     */
    public function getDrawingDirectoryIterator()
    {
        return $this->factory->create($this->drawingDir);
    }

    /**
     * @return RecursiveDirectoryIterator
     */
    public function getApprovalDirectoryIterator()
    {
        return $this->factory->create($this->approvalDir);
    }
}
