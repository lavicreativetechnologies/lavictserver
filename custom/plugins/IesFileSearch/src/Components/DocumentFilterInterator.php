<?php declare(strict_types=1);

namespace Ies\FileSearch\Components;

/**
 * @author    Daniel Hormess <daniel.hormess@isento-ecommcerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH
 */
class DocumentFilterInterator extends DirectoryFilterIterator
{
    /**
     * @return bool
     */
    public function accept()
    {
        $file = $this->current();

        if ($file->isDir()) {
            return false;
        }

        return $this->filterKeyExistInFilename($file->getBasename('.' . $file->getExtension()));
    }

    /**
     * @param $filename
     * @return bool
     */
    protected function filterKeyExistInFilename($filename)
    {
        if (!is_array($this->filterKey)) {
            if (stripos($filename, $this->filterKey) !== false) {
                return true;
            }
        }

        foreach ($this->filterKey as $filter) {
            if (stripos($filename, $filter) !== false) {
                return true;
            }
        }

        return false;
    }
}
