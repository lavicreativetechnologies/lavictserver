<?php declare(strict_types=1);

namespace Ies\FileSearch\Core\File;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\Country\CountryDefinition;

class FileDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'ies_files';
    }

    public function getEntityClass(): string
    {
        return FileEntity::class;
    }

    public function getCollectionClass(): string
    {
        return FileCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new StringField('filepath', 'filepath'))->addFlags(new Required()),
            new StringField('filename', 'filename'),
            new StringField('kind', 'kind'),
            new StringField('file_extension', 'fileExtension'),
            new StringField('product_number', 'productNumber')
        ]);
    }
}
