<?php

declare(strict_types=1);

namespace FeldmannSpecialDiscount\Installer;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\CustomField\Aggregate\CustomFieldSet\CustomFieldSetEntity;
use Shopware\Core\System\CustomField\CustomFieldEntity;
use Shopware\Core\System\CustomField\Aggregate\CustomFieldSetRelation\CustomFieldSetRelationEntity;

class CustomFieldInstaller implements InstallerInterface
{
    public const FIELDSET_ID_CUSTOM_CUSTOMER = 'aacbcf9bedfb4827853b75c5fd278d3f';
    public const FIELDSET_ID_CUSTOM_PRODUCT = 'aadbcf9bedfb4827853b75c5fd578d9f';

    public const CUSTOMER_DISCOUNT = 'special_discount_percentage';
    public const PRODUCT_DISCOUNT = 'special_discount_discountable';

    /** @var EntityRepositoryInterface */
    private $customFieldRepository;

    /** @var EntityRepositoryInterface */
    private $customFieldSetRepository;

    /** @var EntityRepositoryInterface */
    private $customFieldSetRelationRepository;

    /** @var array */
    private $customFields;

    /** @var array */
    private $customFieldSets;

    public function __construct(ContainerInterface $container)
    {
        $this->customFieldSetRepository = $container->get('custom_field_set.repository');
        $this->customFieldRepository    = $container->get('custom_field.repository');

        $this->customFieldSetRelationRepository    = $container->get('custom_field_set_relation.repository');

        $this->customFieldSets = [
            [
                'id' => self::FIELDSET_ID_CUSTOM_CUSTOMER,
                'name'   => 'custom_customers_special_discount',
                'config' => [
                    'label' => [
                        'en-GB' => 'Special Discount for customers',
                        'de-DE' => 'Special Discount de customers',
                    ],
                ],
                'relation' => [
                    'entityName' => 'customer',
                ],
            ],

            [
                'id' => self::FIELDSET_ID_CUSTOM_PRODUCT,
                'name'   => 'custom_products_special_discount',
                'config' => [
                    'label' => [
                        'en-GB' => 'Special Discount for products',
                        'de-DE' => 'Special Discount de products',
                    ],
                ],
                'relation' => [
                    'entityName' => 'product',
                ],
            ],
        ];

        $this->customFields = [
            [
                'name'             => self::CUSTOMER_DISCOUNT,
                'type'             => CustomFieldTypes::FLOAT,
                'customFieldSetId' => self::FIELDSET_ID_CUSTOM_CUSTOMER,
            ],

            [
                'name'             => self::PRODUCT_DISCOUNT,
                'type'             => CustomFieldTypes::BOOL,
                'customFieldSetId' => self::FIELDSET_ID_CUSTOM_PRODUCT,
            ],
        ];
    }

    public function install(InstallContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->upsertCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->upsertCustomField($customField, $context->getContext());
        }
    }

    public function update(UpdateContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->upsertCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->upsertCustomField($customField, $context->getContext());
        }
    }

    public function uninstall(UninstallContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->deactivateCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->deactivateCustomField($customField, $context->getContext());
        }
    }

    public function activate(ActivateContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->upsertCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->upsertCustomField($customField, $context->getContext());
        }
    }

    public function deactivate(DeactivateContext $context): void
    {
        foreach ($this->customFieldSets as $customFieldSet) {
            $this->deactivateCustomFieldSet($customFieldSet, $context->getContext());
        }
        foreach ($this->customFields as $customField) {
            $this->deactivateCustomField($customField, $context->getContext());
        }
    }

    private function upsertCustomField(array $customField, Context $context): void
    {
        $Field = $this->getCustomField($customField['name'], $context);
//        $fid = null;

//        foreach ($this->customFieldSets as $customFieldSet) {
//            $FieldSetId = $this->getCustomFieldSet($customFieldSet['name'], $context);
//            array_push($arr,$FieldSetId->getId());
//            if ($FieldSetId->getId() == $customField['customFieldSetId']){
//                $fid = $FieldSetId->getId();
//            }
//        }

        if(isset($Field)){
            $data = [
                'id'               => $Field->getId(),
                'name'             => $customField['name'],
                'type'             => $customField['type'],
                'active'           => true,
                'customFieldSetId' => $customField['customFieldSetId'],
            ];
        }
        else{
            $data = [
                'name'             => $customField['name'],
                'type'             => $customField['type'],
                'active'           => true,
                'customFieldSetId' => $customField['customFieldSetId'],
            ];
        }

        $this->customFieldRepository->upsert([$data], $context);
    }

    private function deactivateCustomField(array $customField, Context $context): void
    {
        $Field = $this->getCustomField($customField['name'], $context);

        if($Field){
            $data = [
                'id'               => $Field->getId(),
                'name'             => $customField['name'],
                'type'             => $customField['type'],
                'active'           => false,
                'customFieldSetId' => $customField['customFieldSetId'],
            ];

            $this->customFieldRepository->upsert([$data], $context);
        }
    }

    private function upsertCustomFieldSet(array $customFieldSet, Context $context): void
    {
        $FieldSet = $this->getCustomFieldSet($customFieldSet['name'], $context);
        if(isset($FieldSet)){
            $set = $this->getCustomFieldSetRelation($FieldSet->getId(),$context);
            $data = [
                'id'        => $FieldSet->getId(),
                'name'      => $customFieldSet['name'],
                'config'    => $customFieldSet['config'],
                'active'    => true,
                'relations' => [
                    [
                        'id'         => $set->getId(),
                        'entityName' => $customFieldSet['relation']['entityName'],
                    ],
                ],
            ];
        }
        else{
            $data = [
                'id'        => $customFieldSet['id'],
                'name'      => $customFieldSet['name'],
                'config'    => $customFieldSet['config'],
                'active'    => true,
                'relations' => [
                    [
                        'entityName' => $customFieldSet['relation']['entityName'],
                    ],
                ],
            ];
        }

        $this->customFieldSetRepository->upsert([$data], $context);
    }

    private function deactivateCustomFieldSet(array $customFieldSet, Context $context): void
    {
        $FieldSet = $this->getCustomFieldSet($customFieldSet['name'], $context);
        if($FieldSet){
            $set = $this->getCustomFieldSetRelation($FieldSet->getId(),$context);
            $data = [
                'id'        => $FieldSet->getId(),
                'name'      => $customFieldSet['name'],
                'config'    => $customFieldSet['config'],
                'active'    => false,
                'relations' => [
                    [
                        'id'         => $set->getId(),
                        'entityName' => $customFieldSet['relation']['entityName'],
                    ],
                ],
            ];
            $this->customFieldSetRepository->upsert([$data], $context);
        }
    }

    /**
     * @param string $fieldSetName
     * @param Context $context
     * @return CustomFieldSetEntity|null
     */
    protected function getCustomFieldSet(string $fieldSetName, Context $context)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $fieldSetName));

        return $this->customFieldSetRepository
            ->search($criteria, $context)->first();
    }

    /**
     * @param string $fieldName
     * @param Context $context
     * @return CustomFieldEntity|null
     */
    protected function getCustomField(string $fieldName, Context $context)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $fieldName));

        return $this->customFieldRepository
            ->search($criteria, $context)->first();
    }

    /**
     * @param string $fieldRelationId
     * @param Context $context
     * @return CustomFieldSetRelationEntity|null
     */
    protected function getCustomFieldSetRelation(string $fieldRelationId, Context $context)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('customFieldSetId', $fieldRelationId));

        return $this->customFieldSetRelationRepository
            ->search($criteria, $context)->first();
    }
}
