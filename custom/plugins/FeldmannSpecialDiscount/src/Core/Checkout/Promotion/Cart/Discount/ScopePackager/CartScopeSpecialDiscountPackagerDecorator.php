<?php declare(strict_types=1);

namespace FeldmannSpecialDiscount\Core\Checkout\Promotion\Cart\Discount\ScopePackager;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\Group\LineItemQuantity;
use Shopware\Core\Checkout\Cart\LineItem\Group\LineItemQuantityCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\Struct\PriceDefinitionInterface;
use Shopware\Core\Checkout\Cart\Rule\LineItemScope;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountLineItem;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountPackage;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountPackageCollection;
use Shopware\Core\Checkout\Promotion\Cart\Discount\DiscountPackager;
use Shopware\Core\Framework\Rule\Rule;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class CartScopeSpecialDiscountPackagerDecorator extends DiscountPackager
{
    /**
     * @var DiscountPackager
     */
    private $originalPackager;

    public function __construct(DiscountPackager $originalPackager)
    {
        $this->originalPackager = $originalPackager;
    }

    /**
     * {@inheritdoc}
     */
    public function getMatchingItems(DiscountLineItem $discount, Cart $cart, SalesChannelContext $context): DiscountPackageCollection
    {
        $originalDiscountPackage = $this->originalPackager->getMatchingItems($discount, $cart, $context);

        $productLineItems = $cart
            ->getLineItems()
            ->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE);

        $items = [];
        /* @var LineItem $lineItem */
        foreach ($productLineItems as $lineItem) {
            if($lineItem->hasPayloadValue('discountable'))
            {
                $items[] = new LineItemQuantity($lineItem->getId(), $lineItem->getQuantity());
            }
        }

        if ($items === []) {
            return new DiscountPackageCollection([]);
        }

        $package = new DiscountPackage(new LineItemQuantityCollection($items));
        return new DiscountPackageCollection([$package]);
    }

    /**
     * {@inheritdoc}
     */
    public function getDecorated(): DiscountPackager
    {
        return $this->originalPackager;
    }

    public function getResultContext(): string
    {
        return $this->originalPackager->getResultContext();
    }
}
