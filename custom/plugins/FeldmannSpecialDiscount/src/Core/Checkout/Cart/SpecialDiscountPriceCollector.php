<?php declare(strict_types=1);

namespace FeldmannSpecialDiscount\Core\Checkout\Cart;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartDataCollectorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\Struct\AbsolutePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\PercentagePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class SpecialDiscountPriceCollector implements CartDataCollectorInterface
{
    /**
     * @var EntityRepositoryInterface
     */
    private $vioCustomerRepository;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public const CUSTOM_KEY = 'special_discount_percentage';
    public const LINE_ITEM_TYPE = 'feldmannDiscount';
    public const DATA_KEY = 'feldmannDiscounts';

    public function __construct(
        EntityRepositoryInterface $vioCustomerRepository,
        SystemConfigService       $systemConfigService
    )
    {
        $this->vioCustomerRepository = $vioCustomerRepository;
        $this->systemConfigService = $systemConfigService;
    }

    public function collect(CartDataCollection $data, Cart $original, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $lineItems = $original->getLineItems()->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE);


        if ($lineItems->count() === 0) {
            return;
        }
        if ($context->getCustomer()) {
            $cid = $context->getCustomer()->getId();
            $customerGroupId = $context->getCustomer()->getGroupId();
            $customerDiscountGroups = $this->systemConfigService->get('FeldmannSpecialDiscount.config.discountToCustomerGroup');

//          check the group of a customer
            if (!in_array($customerGroupId, $customerDiscountGroups)) {
                return;
            }

            $x = 0;

            foreach ($lineItems as $lineItem) {
                if ($lineItem->getPayloadValue('customFields') !== null && isset($lineItem->getPayloadValue('customFields')['special_discount_discountable'])) {
                    if ($lineItem->getPayloadValue('customFields')['special_discount_discountable'] == true) {
//                      get vio special price
                        $criteria = new Criteria();
                        $criteria->addFilter(new EqualsFilter('productId', $lineItem->getReferencedId()))
                            ->addFilter((new EqualsFilter('customerId', $cid)));

                        $matchData = $this->vioCustomerRepository->search($criteria, $context->getContext())->first();

                        if ($matchData == null) {
                            $lineItem->setPayloadValue(
                                'discountable',
                                true
                            );
                            $x++;

                            $key = $this->buildKey($lineItem->getId());
                            $data->set($key, $lineItem);
                        }
                    }
                }
            }
            if ($x > 0){
                $customDiscount = $this->checkCustomerDiscount($context, $data, $original, $behavior);
                $this->buildDiscountLineItems($data, $original, $context, $behavior, $customDiscount);
            }
        }

    }

    private function buildKey(string $id): string
    {
        return 'discountable-' . $id;
    }

    private function checkCustomerDiscount(SalesChannelContext $context, CartDataCollection $data, Cart $original, CartBehavior $behavior): float
    {
        $customFields = $context->getCustomer()->getCustomFields();
        $defaultDiscount = (float)$this->systemConfigService->get('FeldmannSpecialDiscount.config.discountPercentage');

        if ($customFields != null && isset($customFields[self::CUSTOM_KEY]) && $customFields[self::CUSTOM_KEY] > 0) {
            $discount = $customFields[self::CUSTOM_KEY];
            $discount = ($discount < $defaultDiscount) ? $defaultDiscount : $discount;
        } else {
            $discount = $defaultDiscount;
        }

        return $discount;
    }

    private function buildDiscountLineItems(CartDataCollection $data, Cart $original, SalesChannelContext $context, CartBehavior $behavior, float $discount)
    {
        $promotionItem = new LineItem(uuid::randomHex(), self::LINE_ITEM_TYPE,uuid::randomHex());
        $promotionItem->setLabel($discount . '% ' . 'Feldmannrabatt');
        $promotionItem->setDescription('Special Discount');
        $promotionItem->setGood(false);
        $promotionItem->setRemovable(true);
        $priceDefinition = new PercentagePriceDefinition($discount * -1, $context->getContext()->getCurrencyPrecision());
        $promotionItem->setPriceDefinition($priceDefinition);
        $discountLineItems = [];

        $discountLineItems[] = $promotionItem;

        if (count($discountLineItems) > 0) {
            $data->set(self::DATA_KEY, new LineItemCollection($discountLineItems));
        } else {
            $data->remove(self::DATA_KEY);
        }
    }
}
