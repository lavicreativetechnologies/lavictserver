<?php declare(strict_types=1);

namespace FeldmannSpecialDiscount\Core\Checkout\Cart;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartDataCollectorInterface;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\PercentagePriceCalculator;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\Checkout\Cart\Price\Struct\PriceCollection;
class SpecialDiscountPriceProcessor implements CartProcessorInterface
{
    /**
     * @var PercentagePriceCalculator
     */
    private $percentagePriceCalculator;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(
        PercentagePriceCalculator $percentagePriceCalculator,
        SystemConfigService $systemConfigService

        ) {
            $this->percentagePriceCalculator = $percentagePriceCalculator;
            $this->systemConfigService = $systemConfigService;
    }

    public function process(CartDataCollection $data, Cart $original, Cart $toCalculate, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $lineItems = $original->getLineItems()->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE);

        if ($lineItems->count() === 0) {
            return;
        }
        if ($context->getCustomer()){
            $customerGroupId = $context->getCustomer()->getGroupId();
            $customerDiscountGroups = $this->systemConfigService->get('FeldmannSpecialDiscount.config.discountToCustomerGroup');

            if (!in_array($customerGroupId,$customerDiscountGroups)){
                return;
            }

            /** @var LineItemCollection $discountLineItems */
            $discountLineItems = $data->get(SpecialDiscountPriceCollector::DATA_KEY);
            if ($discountLineItems){

                foreach ($discountLineItems as $discountLineItem)
                {
                    $this->calculateDiscountPrice($data,$original,$toCalculate,$discountLineItem, $context);
                    $toCalculate->addLineItems(new LineItemCollection([$discountLineItem]));
                }
            }

        }
    }

    private function calculateDiscountPrice(CartDataCollection $data,Cart $original,Cart $toCalculate,$discountLineItem, SalesChannelContext $context)
    {
        $lineItems = $original->getLineItems()
        ->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE);

        $prices = [];

        foreach ($lineItems as $lineItem)
        {
            if ($data->has('discountable-'.$lineItem->getId())) {
                $prices[] = $lineItem->getPrice();
            }
        }
        $priceCollection = new PriceCollection($prices);

        $priceDefinition = $discountLineItem->getPriceDefinition();

        $price = $this->percentagePriceCalculator->calculate(
            $priceDefinition->getPercentage(),
            $priceCollection,
            $context
        );
        $discountLineItem->setPrice($price);
    }
}
