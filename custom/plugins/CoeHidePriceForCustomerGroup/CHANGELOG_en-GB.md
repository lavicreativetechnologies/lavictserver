# 1.0.0
- Initial release

#1.0.1
- Prices will be hidden for the cross-selling module as well

#1.0.2
- The "Add to cart" buttons (listing and detail page) can now be displayed/hidden based on whether the prices are visible or not
- Added switch to plug-in configuration

#1.1.0
- When prices are hidden, the price-filter will be hidden as well
- New Configuratoin option: "Add to cart"-Text replacement instead of hiding
- New Text snippet for alternative Text: coeHidePrice.cartReplacement
- As of this version. Shopware < 6.3.2 will no longer be supported

#1.1.1
- When the "Add to cart" button text is replaced, you can add a path to redirect to after clicking the button
