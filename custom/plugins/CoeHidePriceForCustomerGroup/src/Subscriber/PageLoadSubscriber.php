<?php

namespace CoeHidePriceForCustomerGroup\Subscriber;

use Shopware\Core\Checkout\Customer\Aggregate\CustomerGroup\CustomerGroupEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Event\StorefrontRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\System\SystemConfig\SystemConfigService;

/**
 * Class PageLoadSubscriber
 * @package CoeHidePriceForCustomerGroup\Subscriber
 * @author Jeffry Block <jeffry.block@codeenterprise.de>
 */
class PageLoadSubscriber implements EventSubscriberInterface
{
    /** @var string */
    public const PARAM_HIDE_PRICE = "coeHidePrice";

    /** @var string */
    public const PARAM_DISABLE_CHECKOUT = "coeHideCheckoutButtons";

    /** @var string */
    public const PARAM_HIDE_ADD_TO_CART = "coeHideAddToCartButtons";

    /** @var bool */
    public const PARAM_SHOW_CART_REPLACEMENT = "coeShowAddToCartTextReplace";

    public const PARAM_SHOW_CART_REDIRECT = "coeShowAddToCartRedirect";

    /**
     * @var SystemConfigService
     */
    private $configService;

    /**
     * PageLoadSubscriber constructor.
     * @param SystemConfigService $configService
     */
    public function __construct(SystemConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
     * @return array
     * @author Jeffry Block <jeffry.block@codeenterprise.de>
     */
    public static function getSubscribedEvents()
    {
        return [
            StorefrontRenderEvent::class => "onRenderStorefront"
        ];
    }

    /**
     * @param StorefrontRenderEvent $event
     * @author Jeffry Block <jeffry.block@codeenterprise.de>
     */
    public function onRenderStorefront(StorefrontRenderEvent $event){
        /** @var SalesChannelContext $context */
        $context = $event->getSalesChannelContext();

        /** @var CustomerGroupEntity $customerGroup */
        $customerGroup = $context->getCurrentCustomerGroup();

        /** @var array $customFields */
        $customFields = $customerGroup->getCustomFields();

        /** @var CustomerEntity $customer */
        $customer = $context->getCustomer();

        if(is_null($customer) || $customer->getGuest()){
            $hidePrice = $this->configService->get(
                "CoeHidePriceForCustomerGroup.config.coeHidePriceForGuests",
                $context->getSalesChannel()->getId()
            );
        }else if(is_null($customFields) || is_null($customFields[PageLoadSubscriber::PARAM_HIDE_PRICE])){
            $hidePrice = false;
        }else{
            $hidePrice = $customFields[PageLoadSubscriber::PARAM_HIDE_PRICE] == "1";
        }

        $hideCheckoutButton = $hidePrice ? $this->configService->get(
            "CoeHidePriceForCustomerGroup.config.coeHideCheckoutButtons",
            $context->getSalesChannel()->getId()) : false;

        $hideAddToCartButtons = $hidePrice ? $this->configService->get(
            "CoeHidePriceForCustomerGroup.config.coeHideAddToCartButtons",
            $context->getSalesChannel()->getId()) : false;

        $showCartTextReplacement = $hidePrice ? $this->configService->get(
            "CoeHidePriceForCustomerGroup.config.coeShowAddToCartTextReplace",
            $context->getSalesChannel()->getId()) : false;

        $showCartRedirect = $hidePrice ? $this->configService->get(
            "CoeHidePriceForCustomerGroup.config.coeShowAddToCartRedirect",
            $context->getSalesChannel()->getId()) : null;

        $event->setParameter(PageLoadSubscriber::PARAM_HIDE_PRICE, $hidePrice);
        $event->setParameter(PageLoadSubscriber::PARAM_HIDE_ADD_TO_CART, $hideAddToCartButtons);
        $event->setParameter(PageLoadSubscriber::PARAM_DISABLE_CHECKOUT, $hideCheckoutButton);
        $event->setParameter(PageLoadSubscriber::PARAM_SHOW_CART_REPLACEMENT, $showCartTextReplacement);
        $event->setParameter(PageLoadSubscriber::PARAM_SHOW_CART_REDIRECT, $showCartRedirect);
    }

}
