import template from './sw-settings-customer-group-detail.html.twig';
import deDE from '../../snippet/de-DE.json';
import enGB from '../../snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);

const { Component} = Shopware;

Component.override('sw-settings-customer-group-detail', {
    template,

    data() {
        return {
            isLoading: false,
            customerGroup: {},
            isSaveSuccessful: false
        };
    },

    computed: {
        coeHidePrice:{
            get(){
                return this.customerGroup.customFields.coeHidePrice == "1" ||  this.customerGroup.customFields.coeHidePrice == true;
            },
            set(value) {
                this.customerGroup.customFields.coeHidePrice = value;
            }
        }
    },

    watch: {
        "customerGroup.customFields":{
            handler() {
                this.initializeCustomFields();
            }
        }
    },

    methods: {
        initializeCustomFields(){
            if (!this.customerGroup.customFields) {
                this.customerGroup.customFields = {
                    coeHidePrice : false
                };
            }
        }
    }

});