<?php declare(strict_types=1);

namespace CoeHidePriceForCustomerGroup;

use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;

/**
 * Class CoeHidePriceForCustomerGroup
 * @package CoeEmailSendCopy
 * @author Jeffry Block <jeffry.block@codeenterprise.de>
 */
class CoeHidePriceForCustomerGroup extends Plugin {

    /**
     * @param InstallContext $context
     * @author Jeffry Block <jeffry.block@codeenterprise.de>
     */
    public function install(InstallContext $context): void
    {
        parent::install($context);
    }

    /**
     * @param UninstallContext $context
     * @author Jeffry Block <jeffry.block@codeenterprise.de>
     */
    public function uninstall(UninstallContext $context): void
    {
        parent::uninstall($context);

        if ($context->keepUserData()) {
            return;
        }
    }
}