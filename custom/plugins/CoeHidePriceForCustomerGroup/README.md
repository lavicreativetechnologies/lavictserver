### Beschreibung

Mithilfe dieses Plug-Ins können Preise in der Storefront für bestimmte Kundengruppen oder auch für nicht angemeldete Kunden ausgeblendet werden.

Es besteht auch die Möglichkeit, die "Checkout"-Buttons auszublenden, wenn für den aktuellen Kunden die Preise nicht angezeigt werden.

Die Kundengruppen Konfiguration findet in der jeweiligen Kundengruppe (Einstellungen -> Shop -> Kundengruppen) statt.
Die Einstellung, ob Preise für eine gewisse Kundengruppe sichtbar ist oder nicht kann dabei für jede definierte Sprache einzeln vorgenommen.
So kann zum Beispiel konfiguriert werden, dass Kunden mit der Kundengruppe "Händler" im deutschen Shop keine Preise angezeigt bekommen, im englischen hingegen schon.

Die Einstellung ob nicht angemeldete Benutzer Preise angezeigt bekommen oder nicht findet in der Plug-In Konfiguration statt.
Hier kann auch definiert werden, ob die "Checkout"-Buttons für nicht angemeldete Benutzer ein- oder ausgeblendet werden sollen.

Dabei ist wichtig zu wissen, dass dieses Plug-In nichts an der Logik vom Shop ändert, sondern lediglich einzelne Bereiche im Storefront ausblendet.


### Installation / Konfiguration
1. Installier das Plug-In wie gewohnt über den Plug-In Manager
2. Aktiviere das Plug-In
3. Öffne die Plug-In Konfiguration (Einstellungen -> System -> Plugins -> Preise pro Benutzergruppe oder für Gäste ausblenden -> Konfiguration)
4. "Preise für Gäste ausblenden" und "Checkout Buttons ausblenden" definieren.
5. Öffne die gewünschte Kundengruppe, für die die Preise ausgbeblendet werden sollen (Einstellungen -> Shop -> Kundengruppen)
6. Setze den Wert dementsprechend und achte dabei darauf, welche Sprache ausgewählt ist.

Zum ändern des alternativen Warenkorb Textes, wenn Preise ausgeblendet sind muss in den Textbausteinen der Wert von "coeHidePrice.cartReplacement" angepasst werden.

### Features
- Preise Sprachunabhängig pro Kundengruppe ausblenden
- Preise Sprachabhängig für nicht angemeldete Benutzer ausblenden
- Checkout-Buttons Sprachabhängig ausblenden, wenn auch Preise ausgeblendet sind
- "Add to cart" Buttons Sprachabhängig ausblenden, wenn auch Preise ausgeblendet sind
- "Add to cart" Button Text alternativ ändern, anstatt ihn auszublenden wenn auch Preise ausgeblendet sind
- Kompatibel mit dem Merkzettel Plug-In

### Tags
Preise ausblenden, Händler, Händlerpreise, Checkout deaktivieren, Preisanzeige

![codeenterprise GmbH](https://www.codeenterprise.de/wp-content/uploads/2018/10/codeenterprise_logo_quer.png "codeenterprise GmbH")