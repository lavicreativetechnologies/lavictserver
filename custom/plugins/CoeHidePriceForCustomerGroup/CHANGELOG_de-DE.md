# 1.0.0
- Initiale Veröffentlichung

#1.0.1
- Preise werden nun auch beim cross-selling Modul ausgeblendet

#1.0.2
- "Zum Warenkorb hinzufügen" Buttons aus dem Listing oder der Detailseite können nun abhängig davon ob der Preis angezeigt wird ein-/ausgeblendet werden
- Funktionalität kann über die Plug-In Einstellungen aktiviert werden

#1.1.0
- Wenn Preise ausgeblendet sind, wird nun auch der Preisfilter ausgeblendet.
- Neue Konfigurations Option: "In den Wartenkorb"-Text ersetzen anstatt ihn auszublenden
- Text-Snippet für alternativen Text hinzugefügt: coeHidePrice.cartReplacement
- Ab dieser Version wird Shopware < 6.3.2 nicht mehr unterstützt

#1.1.1
- Wenn der "Zum Warenkorb hinzufügen" Button Text ersetzt wurde kann ein Pfad für eine Weiterleitung bei Klick des Buttons hinterlegt werden.
