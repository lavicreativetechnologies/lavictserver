<?php declare(strict_types=1);

namespace Ies\Offers;

use Ies\BueroPlus\BueroPlus;
use Shopware\Core\Framework\Plugin;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class Offers extends Plugin
{
    public const MAILTYPE_ACCEPT_OFFER = 'accept_offer';

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $activePlugins = $container->getParameter('kernel.active_plugins');
        if (!isset($activePlugins[BueroPlus::class])) {
            return;
        }

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/Resources/config'));
        $loader->load('buero_plus.xml');
    }
}
