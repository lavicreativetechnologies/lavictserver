<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Offer;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 *
 * @method void             add(OfferEntity $entity)
 * @method void             set(string $key, OfferEntity $entity)
 * @method OfferEntity[]    getIterator()
 * @method OfferEntity[]    getElements()
 * @method OfferEntity|null get(string $key)
 * @method OfferEntity|null first()
 * @method OfferEntity|null last()
 */
class OfferCollection extends EntityCollection
{
    public function getApiAlias(): string
    {
        return 'offer_collection';
    }

    protected function getExpectedClass(): string
    {
        return OfferEntity::class;
    }
}
