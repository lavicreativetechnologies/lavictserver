<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Event;

use Ies\Offers\Offer\OfferDefinition;
use Ies\Offers\Offer\OfferEntity;
use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\EventData\EntityType;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;
use Shopware\Core\Framework\Event\MailActionInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class AcceptOfferRequestEvent extends Event implements MailActionInterface
{
    private SalesChannelContext $salesChannelContext;

    private OfferEntity $offer;

    private ?string $message;

    private MailRecipientStruct $recipientStruct;

    public function __construct(
        SalesChannelContext $salesChannelContext,
        OfferEntity $offer,
        MailRecipientStruct $recipientStruct,
        ?string $message
    ) {
        $this->salesChannelContext = $salesChannelContext;
        $this->offer = $offer;
        $this->message = $message;
        $this->recipientStruct = $recipientStruct;
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('offer', new EntityType(OfferDefinition::class))
            ->add('message', new ScalarValueType(ScalarValueType::TYPE_STRING))
            ->add('customer', new EntityType(CustomerDefinition::class));
    }

    public function getName(): string
    {
        return self::class;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        return $this->recipientStruct;
    }

    public function getSalesChannelId(): ?string
    {
        return $this->salesChannelContext->getSalesChannel()->getId();
    }

    public function getContext(): Context
    {
        return $this->salesChannelContext->getContext();
    }

    public function getOffer(): OfferEntity
    {
        return $this->offer;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getCustomer(): CustomerEntity
    {
        return $this->salesChannelContext->getCustomer();
    }
}
