<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Event;

use Ies\Offers\Page\AccountDocumentPage;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\PageLoadedEvent;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class AccountDocumentPageLoadedEvent extends PageLoadedEvent
{
    protected AccountDocumentPage $page;

    public function __construct(AccountDocumentPage $page, SalesChannelContext $salesChannelContext, Request $request)
    {
        $this->page = $page;
        parent::__construct($salesChannelContext, $request);
    }

    public function getPage(): AccountDocumentPage
    {
        return $this->page;
    }
}
