<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Page;

use Ies\Offers\Event\AccountDocumentPageLoadedEvent;
use Shopware\Core\Checkout\Cart\Exception\CustomerNotLoggedInException;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Framework\Page\StorefrontSearchResult;
use Shopware\Storefront\Page\GenericPageLoaderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class AccountDocumentPageLoader
{
    protected GenericPageLoaderInterface $genericLoader;

    protected EventDispatcherInterface $eventDispatcher;

    protected AccountService $accountService;

    public function __construct(
        GenericPageLoaderInterface $genericLoader,
        EventDispatcherInterface $eventDispatcher,
        AccountService $accountService
    ) {
        $this->genericLoader = $genericLoader;
        $this->eventDispatcher = $eventDispatcher;
        $this->accountService = $accountService;
    }

    public function load(
        Request $request,
        SalesChannelContext $salesChannelContext,
        EntityRepositoryInterface $repository
    ): AccountDocumentPage {
        if (!$salesChannelContext->getCustomer() && $request->get('deepLinkCode', false) === false) {
            throw new CustomerNotLoggedInException();
        }

        $page = $this->genericLoader->load($request, $salesChannelContext);

        $page = AccountDocumentPage::createFrom($page);

        $page->setDocuments(
            StorefrontSearchResult::createFrom(
                $this->getDocuments($request, $salesChannelContext, $repository)
            )
        );

        $page->setDeepLinkCode($request->get('deepLinkCode'));

        if ($request->get('deepLinkCode') && $page->getDocuments()->first() !== null) {
            $this->accountService->login(
                $page->getDocuments()->first()->getCustomer()->getEmail(),
                $salesChannelContext,
                true
            );
        }

        $this->eventDispatcher->dispatch(
            new AccountDocumentPageLoadedEvent($page, $salesChannelContext, $request)
        );

        return $page;
    }

    private function getDocuments(
        Request $request,
        SalesChannelContext $context,
        EntityRepositoryInterface $repository
    ): EntitySearchResult {
        $criteria = $this->createCriteria($request);
        if ($context->getCustomer()) {
            $criteria->addFilter(new EqualsFilter('customer.id', $context->getCustomer()->getId()));
        }

        return $repository->search($criteria, $context->getContext());
    }

    private function createCriteria(Request $request): Criteria
    {
        $limit = (int) $request->query->get('limit', 10);
        $page = (int) $request->query->get('p', 1);

        $criteria = (new Criteria())
            ->addSorting(new FieldSorting('createdAt', FieldSorting::DESCENDING))
            ->addAssociation('documentMediaFile')
            ->addAssociation('customer')
            ->setLimit($limit)
            ->setOffset(($page - 1) * $limit)
            ->setTotalCountMode(Criteria::TOTAL_COUNT_MODE_NEXT_PAGES);

        if ($request->get('deepLinkCode')) {
            $criteria->addFilter(new EqualsFilter('deepLinkCode', $request->get('deepLinkCode')));
        }

        return $criteria;
    }
}
