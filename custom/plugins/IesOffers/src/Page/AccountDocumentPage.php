<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Page;

use Shopware\Storefront\Framework\Page\StorefrontSearchResult;
use Shopware\Storefront\Page\Page;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class AccountDocumentPage extends Page
{
    protected StorefrontSearchResult $documents;

    protected ?string $deepLinkCode;

    public function getDocuments(): StorefrontSearchResult
    {
        return $this->documents;
    }

    public function setDocuments(StorefrontSearchResult $documents): void
    {
        $this->documents = $documents;
    }

    public function getDeepLinkCode(): ?string
    {
        return $this->deepLinkCode;
    }

    public function setDeepLinkCode(?string $deepLinkCode): void
    {
        $this->deepLinkCode = $deepLinkCode;
    }
}
