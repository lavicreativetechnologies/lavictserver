<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Struct;

use Shopware\Core\Framework\Struct\Struct;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class DocumentIdStruct extends Struct
{
    protected string $id;

    protected string $deeplinkCode;

    public function __construct(string $id, string $deeplinkCode)
    {
        $this->id = $id;
        $this->deeplinkCode = $deeplinkCode;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getDeeplinkCode(): string
    {
        return $this->deeplinkCode;
    }

    public function setDeeplinkCode(string $deeplinkCode): void
    {
        $this->deeplinkCode = $deeplinkCode;
    }

    public function getApiAlias(): string
    {
        return 'document_id';
    }
}
