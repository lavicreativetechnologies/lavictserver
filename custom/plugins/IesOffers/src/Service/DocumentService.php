<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Service;

use Ies\Offers\CustomerDocument\CustomerDocumentEntity;
use Ies\Offers\Exception\InvalidDocumentException;
use Ies\Offers\Struct\DocumentIdStruct;
use Shopware\Core\Checkout\Document\Exception\DocumentGenerationException;
use Shopware\Core\Checkout\Document\FileGenerator\FileGeneratorRegistry;
use Shopware\Core\Checkout\Document\GeneratedDocument;
use Shopware\Core\Content\Media\MediaService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Util\Random;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class DocumentService
{
    protected MediaService $mediaService;

    protected FileGeneratorRegistry $fileGeneratorRegistry;

    public function __construct(
        MediaService $mediaService,
        FileGeneratorRegistry $fileGeneratorRegistry
    ) {
        $this->mediaService = $mediaService;
        $this->fileGeneratorRegistry = $fileGeneratorRegistry;
    }

    public function create(
        string $customerId,
        string $number,
        Context $context,
        EntityRepositoryInterface $documentRepository
    ): DocumentIdStruct {
        $id = Uuid::randomHex();
        $deepLinkCode = Random::getAlphanumericString(32);

        $documentRepository->create(
            [
                [
                    'id' => $id,
                    'number' => $number,
                    'customerId' => $customerId,
                    'deepLinkCode' => $deepLinkCode,
                ],
            ],
            $context
        );

        return new DocumentIdStruct($id, $deepLinkCode);
    }

    public function getDocument(CustomerDocumentEntity $documentEntity, Context $context): GeneratedDocument
    {
        $generatedDocument = new GeneratedDocument();
        if (!$this->hasValidFile($documentEntity)) {
            throw new InvalidDocumentException($documentEntity->getId());
        }

        $generatedDocument->setFilename($documentEntity->getDocumentMediaFile()->getFileName() . '.' . $documentEntity->getDocumentMediaFile()->getFileExtension());
        $generatedDocument->setContentType($documentEntity->getDocumentMediaFile()->getMimeType());

        $fileBlob = '';
        $mediaService = $this->mediaService;
        $context->scope(
            Context::SYSTEM_SCOPE,
            static function (Context $context) use ($mediaService, $documentEntity, &$fileBlob): void {
                $fileBlob = $mediaService->loadFile($documentEntity->getDocumentMediaFileId(), $context);
            }
        );
        $generatedDocument->setFileBlob($fileBlob);

        return $generatedDocument;
    }

    public function uploadFile(
        string $documentId,
        Context $context,
        Request $uploadedFileRequest,
        EntityRepositoryInterface $documentRepository
    ): DocumentIdStruct {
        $document = $documentRepository->search(new Criteria([$documentId]), $context)->first();

        if ($document->getDocumentMediaFile() !== null) {
            throw new DocumentGenerationException('Document already exists');
        }

        $mediaFile = $this->mediaService->fetchFile($uploadedFileRequest);

        $fileName = $uploadedFileRequest->query->get('fileName');

        $mediaService = $this->mediaService;
        $mediaId = null;
        $context->scope(Context::SYSTEM_SCOPE, static function (Context $context) use (
            $fileName,
            $mediaService,
            $mediaFile,
            &$mediaId
        ): void {
            $mediaId = $mediaService->saveMediaFile($mediaFile, $fileName, $context, 'document');
        });

        $document->setDocumentMediaFileId($mediaId);
        $documentRepository->update(
            [
                [
                    'id' => $document->getId(),
                    'documentMediaFileId' => $document->getDocumentMediaFileId(),
                ],
            ],
            $context
        );

        return new DocumentIdStruct($documentId, $document->getDeepLinkCode());
    }

    private function hasValidFile(CustomerDocumentEntity $documentEntity): bool
    {
        return $documentEntity->getDocumentMediaFile() !== null
            && $documentEntity->getDocumentMediaFile()->getFileName() !== null;
    }
}
