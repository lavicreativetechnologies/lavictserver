<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Invoice;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 *
 * @method void               add(InvoiceEntity $entity)
 * @method void               set(string $key, InvoiceEntity $entity)
 * @method InvoiceEntity[]    getIterator()
 * @method InvoiceEntity[]    getElements()
 * @method InvoiceEntity|null get(string $key)
 * @method InvoiceEntity|null first()
 * @method InvoiceEntity|null last()
 */
class InvoiceCollection extends EntityCollection
{
    public function getApiAlias(): string
    {
        return 'invoice_collection';
    }

    protected function getExpectedClass(): string
    {
        return InvoiceEntity::class;
    }
}
