<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Controller;

use Ies\Offers\Event\AcceptOfferRequestEvent;
use Ies\Offers\Exception\InvalidDocumentException;
use Ies\Offers\Page\AccountDocumentPageLoader;
use Ies\Offers\Service\DocumentService;
use Shopware\Core\Checkout\Cart\Exception\CustomerNotLoggedInException;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Routing\Annotation\LoginRequired;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @RouteScope(scopes={"storefront"})
 */
class AccountController extends StorefrontController
{
    protected AccountDocumentPageLoader $documentPageLoader;

    protected SystemConfigService $systemConfigService;

    protected EntityRepositoryInterface $invoiceRepository;

    protected EntityRepositoryInterface $offerRepository;

    protected DocumentService $documentService;

    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AccountDocumentPageLoader $documentPageLoader,
        SystemConfigService $systemConfigService,
        EntityRepositoryInterface $offerRepository,
        EntityRepositoryInterface $invoiceRepository,
        DocumentService $documentService,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->documentPageLoader = $documentPageLoader;
        $this->systemConfigService = $systemConfigService;
        $this->documentService = $documentService;
        $this->offerRepository = $offerRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/account/offer", name="frontend.account.offer.page", options={"seo"="false"}, methods={"GET"})
     * @LoginRequired()
     * @throws CustomerNotLoggedInException
     */
    public function offerOverview(Request $request, SalesChannelContext $context): Response
    {
        $page = $this->documentPageLoader->load($request, $context, $this->offerRepository);

        return $this->renderStorefront(
            '@Storefront/storefront/page/account/offer-history/index.html.twig',
            ['page' => $page]
        );
    }

    /**
     * @Route("/account/offer/accept", name="frontend.account.offer.accept", methods={"POST"})
     * @LoginRequired()
     */
    public function acceptOffer(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
        $message = $data->get('message', null);
        $id = $data->get('offerId');

        $offer = $this->offerRepository->search(new Criteria([$id]), $context->getContext())->first();
        $receivers[] = $this->systemConfigService->get(
            'core.basicInformation.email',
            $context->getSalesChannel()->getId()
        );

        foreach ($receivers as $mail) {
            $event = new AcceptOfferRequestEvent($context, $offer, new MailRecipientStruct([$mail => $mail]), $message);
            $this->eventDispatcher->dispatch($event);
        }

        $this->offerRepository->update([
            [
                'id' => $id,
                'accepted' => true
            ]
        ], $context->getContext());

        return $this->createActionResponse($request);
    }

    /**
     * @Route("/account/offer/download/{offerId}/{deepLinkCode}", name="frontend.account.offer.download", methods={"GET"})
     * @LoginRequired()
     */
    public function downloadOffer(Request $request, string $offerId, string $deepLinkCode, Context $context): Response
    {
        return $this->download($request, $offerId, $deepLinkCode, $context, $this->offerRepository);
    }

    /**
     * @Route("/account/invoice", name="frontend.account.invoice.page", options={"seo"="false"}, methods={"GET"})
     * @LoginRequired()
     * @throws CustomerNotLoggedInException
     */
    public function invoiceOverview(Request $request, SalesChannelContext $context): Response
    {
        $page = $this->documentPageLoader->load($request, $context, $this->invoiceRepository);

        return $this->renderStorefront(
            '@Storefront/storefront/page/account/invoice-history/index.html.twig',
            ['page' => $page]
        );
    }

    /**
     * @Route("/account/invoice/download/{invoiceId}/{deepLinkCode}", name="frontend.account.invoice.download", methods={"GET"})
     * @LoginRequired()
     */
    public function downloadInvoice(Request $request, string $invoiceId, string $deepLinkCode, Context $context): Response
    {
        return $this->download($request, $invoiceId, $deepLinkCode, $context, $this->invoiceRepository);
    }

    private function download(
        Request $request,
        string $documentId,
        string $deepLinkCode,
        Context $context,
        EntityRepositoryInterface $documentRepository
    ): Response {
        $download = $request->query->getBoolean('download', true);

        $criteria = new Criteria();
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_AND, [
            new EqualsFilter('id', $documentId),
            new EqualsFilter('deepLinkCode', $deepLinkCode),
        ]));
        $criteria->addAssociation('documentMediaFile');

        $invoice = $documentRepository->search($criteria, $context)->get($documentId);

        if (!$invoice) {
            throw new InvalidDocumentException($documentId);
        }

        $generatedDocument = $this->documentService->getDocument($invoice, $context);

        return $this->createResponse(
            $generatedDocument->getFilename(),
            $generatedDocument->getFileBlob(),
            $download,
            $generatedDocument->getContentType()
        );
    }

    private function createResponse(string $filename, string $content, bool $forceDownload, string $contentType): Response
    {
        $response = new Response($content);

        $disposition = HeaderUtils::makeDisposition(
            $forceDownload ? HeaderUtils::DISPOSITION_ATTACHMENT : HeaderUtils::DISPOSITION_INLINE,
            $filename,
            // only printable ascii
            preg_replace('/[\x00-\x1F\x7F-\xFF]/', '_', $filename)
        );

        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
