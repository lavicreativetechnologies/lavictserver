<?php declare(strict_types=1);

namespace Ies\Offers\Migration;

use Doctrine\DBAL\Connection;
use Ies\Offers\Event\AcceptOfferRequestEvent;
use Ies\Offers\Offers;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1594291910AcceptOfferFormTemplate extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1594291910;
    }

    public function update(Connection $connection): void
    {
        $acceptOfferTemplateId = $this->getAcceptOfferMailTemplateId($connection);
        $acceptOfferEventConfig = $this->getAcceptOfferMailEventConfig($connection);

        $config = json_decode($acceptOfferEventConfig, true);
        $contactTemplateTypeId = Uuid::fromHexToBytes($config['mail_template_type_id']);

        $update = false;
        if (!$acceptOfferTemplateId) {
            $acceptOfferTemplateId = Uuid::randomBytes();
        } else {
            $update = true;
        }

        if (!is_string($acceptOfferTemplateId)) {
            return;
        }

        if ($update === true) {
            $connection->update(
                'mail_template',
                [
                    'updated_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                ],
                ['id' => $acceptOfferTemplateId]
            );

            $connection->delete('mail_template_translation', ['mail_template_id' => $acceptOfferTemplateId]);
        } else {
            $connection->insert(
                'mail_template',
                [
                    'id' => $acceptOfferTemplateId,
                    'mail_template_type_id' => $contactTemplateTypeId,
                    'system_default' => 1,
                    'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                ]
            );
        }

        $connection->insert(
            'mail_template_translation',
            [
                'subject' => 'Auftrag erteilt - {{ offer.number }}',
                'description' => 'Auftrag erteilt',
                'sender_name' => '{{ salesChannel.name }}',
                'content_html' => $this->getHtmlTemplateDe(),
                'content_plain' => $this->getPlainTemplateDe(),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                'mail_template_id' => $acceptOfferTemplateId,
                'language_id' => Uuid::fromHexToBytes(Defaults::LANGUAGE_SYSTEM),
            ]
        );
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function getHtmlTemplateDe(): string
    {
        return '<div style="font-family:arial; font-size:12px;">
            <p>
                Der Kunde {{ customer.firstName }} {{ customer.lastName }} ({{ customer.customerNumber }}) hat das Angebot {{ offer.number }} angenommen.
            </p>
            <p>
                {% if message %}
                    Es wurde folgende Nachricht hinterlassen:
                    <p>{{ message|nl2br }}</p>
                {% else %}
                    Es wurde keine Nachricht hinterlassen.
                {% endif %}
            </p>
        </div>';
    }

    private function getPlainTemplateDe(): string
    {
        return 'Der Kunde {{ customer.firstName }} {{ customer.lastName }} ({{ customer.customerNumber }}) hat das Angebot {{ offer.number }} angenommen.

                {% if message %}Es wurde folgende Nachricht hinterlassen:
                {{ message }}
                {% else %}Es wurde keine Nachricht hinterlassen.{% endif %}';
    }

    private function getAcceptOfferMailEventConfig(Connection $connection): string
    {
        $sql = <<<SQL
SELECT `event_action`.`config`
FROM `event_action`
WHERE `event_action`.`event_name` = :event_name AND `event_action`.`action_name` = :action_name
SQL;

        $contactEventConfig = (string) $connection->executeQuery(
            $sql,
            [
                'event_name' => AcceptOfferRequestEvent::class,
                'action_name' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
            ]
        )->fetchColumn();

        return $contactEventConfig;
    }

    private function getAcceptOfferMailTemplateId(Connection $connection): ?string
    {
        $sql = <<<SQL
    SELECT `mail_template`.`id`
    FROM `mail_template` LEFT JOIN `mail_template_type` ON `mail_template`.`mail_template_type_id` = `mail_template_type`.id
    WHERE `mail_template_type`.`technical_name` = :technical_name
SQL;

        $templateTypeId = $connection->executeQuery(
            $sql,
            [
                'technical_name' => Offers::MAILTYPE_ACCEPT_OFFER,
            ]
        )->fetchColumn();

        if ($templateTypeId) {
            return $templateTypeId;
        }

        return null;
    }
}
