<?php declare(strict_types=1);

namespace Ies\Offers\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1594126704CreateOfferTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1594126704;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
            CREATE TABLE `ies_offer` (
              `id` BINARY(16) NOT NULL,
              `customer_id` BINARY(16) NOT NULL,
              `number` VARCHAR(255) NOT NULL,
              `accepted` TINYINT(1) NOT NULL DEFAULT 0,
              `document_media_file_id` BINARY(16) NULL,
              `deep_link_code` VARCHAR(32) NOT NULL,
              `custom_fields` JSON NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE `uniq.deep_link_code` (`deep_link_code`),
              CONSTRAINT `json.ies_offer.custom_fields` CHECK (JSON_VALID(`custom_fields`)),
              CONSTRAINT `fk.ies_offer.customer_id` FOREIGN KEY (`customer_id`)
                REFERENCES `customer` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
              CONSTRAINT `fk.ies_offer.document_media_file_id` FOREIGN KEY (`document_media_file_id`)
                REFERENCES `media` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
SQL;
        $connection->executeUpdate($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
