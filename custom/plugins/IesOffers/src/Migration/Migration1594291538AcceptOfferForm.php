<?php declare(strict_types=1);

namespace Ies\Offers\Migration;

use Doctrine\DBAL\Connection;
use Ies\Offers\Event\AcceptOfferRequestEvent;
use Ies\Offers\Offers;
use Shopware\Core\Content\ContactForm\Event\ContactFormEvent;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Content\MailTemplate\MailTemplateTypes;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1594291538AcceptOfferForm extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1594291538;
    }

    public function update(Connection $connection): void
    {
        $acceptOfferFormEmailTemplate = [
            'id' => Uuid::randomHex(),
            'name' => 'Accept offer',
            'nameDe' => 'Angebot akzeptieren',
            'availableEntities' => json_encode(['salesChannel' => 'sales_channel']),
        ];

        $mailTemplateTypeId = Uuid::fromHexToBytes($acceptOfferFormEmailTemplate['id']);
        $connection->insert(
            'mail_template_type',
            [
                'id' => $mailTemplateTypeId,
                'technical_name' => Offers::MAILTYPE_ACCEPT_OFFER,
                'available_entities' => $acceptOfferFormEmailTemplate['availableEntities'],
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            ]
        );

        $connection->insert(
            'mail_template_type_translation',
            [
                'mail_template_type_id' => $mailTemplateTypeId,
                'name' => $acceptOfferFormEmailTemplate['name'],
                'language_id' => $this->getLanguageIdByLocale($connection, 'en-GB'),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            ]
        );

        $connection->insert(
            'mail_template_type_translation',
            [
                'mail_template_type_id' => $mailTemplateTypeId,
                'name' => $acceptOfferFormEmailTemplate['nameDe'],
                'language_id' => $this->getLanguageIdByLocale($connection, 'de-DE'),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            ]
        );

        $connection->insert(
            'event_action',
            [
                'id' => Uuid::randomBytes(),
                'event_name' => AcceptOfferRequestEvent::class,
                'action_name' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
                'config' => json_encode(['mail_template_type_id' => $acceptOfferFormEmailTemplate['id']]),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            ]
        );
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function getLanguageIdByLocale(Connection $connection, string $locale): string
    {
        $sql = <<<SQL
SELECT `language`.`id`
FROM `language`
INNER JOIN `locale` ON `locale`.`id` = `language`.`locale_id`
WHERE `locale`.`code` = :code
SQL;

        /** @var string|false $languageId */
        $languageId = $connection->executeQuery($sql, ['code' => $locale])->fetchColumn();
        if (!$languageId) {
            throw new \RuntimeException(sprintf('Language for locale "%s" not found.', $locale));
        }

        return $languageId;
    }
}
