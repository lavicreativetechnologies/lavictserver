<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Import;

use Ies\BueroPlus\Components\Document\DocumentDetailsStruct;
use Ies\BueroPlus\Components\Document\DocumentImportException;
use Ies\BueroPlus\Components\Document\Import\InvoiceImporter;
use Ies\Offers\Service\DocumentService as InvoiceDocumentService;
use Shopware\Core\Checkout\Document\DocumentService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class CustomerInvoiceImport extends InvoiceImporter
{
    protected EntityRepositoryInterface $customerRepository;

    protected InvoiceDocumentService $invoiceDocumentService;

    protected EntityRepositoryInterface $invoiceRepository;

    public function __construct(
        DocumentService $documentService,
        EntityRepositoryInterface $orderRepository,
        EntityRepositoryInterface $customerRepository,
        EntityRepositoryInterface $invoiceRepository,
        InvoiceDocumentService $invoiceDocumentService
    ) {
        parent::__construct($documentService, $orderRepository);
        $this->customerRepository = $customerRepository;
        $this->invoiceDocumentService = $invoiceDocumentService;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * Normal invoices are mapped to a order. When this failes we try to map it to a customer
     */
    public function import(\SplFileInfo $document, DocumentDetailsStruct $documentDetailsStruct, Context $context): void
    {
        try {
            parent::import($document, $documentDetailsStruct, $context);
        } catch (DocumentImportException $e) {
            $customerNumber = $documentDetailsStruct->getCustomerNumber();
            $customerId = $this->findCustomer($customerNumber, $context);

            $invoiceIdStruct = $this->invoiceDocumentService->create(
                $customerId,
                $documentDetailsStruct->getDocumentNr(),
                $context,
                $this->invoiceRepository
            );

            $requestStruct = $this->createFileUploadRequest($document);

            $this->invoiceDocumentService->uploadFile(
                $invoiceIdStruct->getId(),
                $context,
                $requestStruct->getRequest(),
                $this->invoiceRepository
            );
            @fclose($requestStruct->getResource());

            unlink($document->getRealPath());
        }
    }

    protected function findCustomer(string $customerNumber, Context $context)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('customerNumber', $customerNumber));
        $criteria->setLimit(1);

        $result = $this->customerRepository->searchIds($criteria, $context);
        if (!$result->firstId()) {
            throw new DocumentImportException('Customer with number ' . $customerNumber . ' not found');
        }
        return $result->firstId();
    }
}
