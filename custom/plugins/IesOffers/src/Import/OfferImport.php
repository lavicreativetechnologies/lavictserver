<?php declare(strict_types=1);
/**
 * © isento eCommerce solutions GmbH
 */

namespace Ies\Offers\Import;

use Ies\BueroPlus\Components\Document\DocumentDetailsStruct;
use Ies\BueroPlus\Components\Document\DocumentImportException;
use Ies\BueroPlus\Components\Document\Import\AbstractPdfImporter;
use Ies\Offers\Service\DocumentService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

/**
 * @author    Jörg Lautenschlager <joerg.lautenschlager@isento-ecommerce.de>
 * @copyright 2020 isento eCommerce solutions GmbH (http://www.isento-ecommerce.de)
 */
class OfferImport extends AbstractPdfImporter
{
    protected DocumentService $documentService;

    protected EntityRepositoryInterface $offerRepository;

    protected EntityRepositoryInterface $customerRepository;

    public function __construct(
        DocumentService $documentService,
        EntityRepositoryInterface $offerRepository,
        EntityRepositoryInterface $customerRepository
    ) {
        $this->documentService = $documentService;
        $this->offerRepository = $offerRepository;
        $this->customerRepository = $customerRepository;
    }

    public function getDocumentAbbreviation(): string
    {
        return 'AN';
    }

    public function import(\SplFileInfo $document, DocumentDetailsStruct $documentDetailsStruct, Context $context): void
    {
        $offerIdStruct = $this->documentService->create(
            $this->getCustomerId($documentDetailsStruct->getCustomerNumber(), $context),
            $documentDetailsStruct->getDocumentNr(),
            $context,
            $this->offerRepository
        );

        $requestStruct = $this->createFileUploadRequest($document);

        $this->documentService->uploadFile(
            $offerIdStruct->getId(),
            $context,
            $requestStruct->getRequest(),
            $this->offerRepository
        );
        @fclose($requestStruct->getResource());

        unlink($document->getRealPath());
    }

    public function getCustomerId(string $customerNumber, Context $context): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('customerNumber', $customerNumber));
        $customerIds = $this->customerRepository->searchIds($criteria, $context);
        $customerId = $customerIds->firstId();
        if (!$customerId) {
            throw new DocumentImportException('Customer with number ' . $customerNumber . ' cannot be found');
        }

        return $customerIds->firstId();
    }
}
