<?php declare(strict_types=1);

namespace Ies\Offers\Tests\Service;

use Ies\Offers\Offer\OfferEntity;
use Ies\Offers\Service\DocumentService;
use Ies\Offers\Struct\DocumentIdStruct;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Document\GeneratedDocument;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Test\TestCaseBase\IntegrationTestBehaviour;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

class DocumentServiceTest extends TestCase
{
    use IntegrationTestBehaviour;

    private Context $context;

    public function setUp(): void
    {
        parent::setUp();
        $this->context = Context::createDefaultContext();
    }

    public function testCreateOffer()
    {
        $offerIdStruct = $this->createOffer();
        $offerRepository = $this->getContainer()->get('ies_offer.repository');
        $offer = $offerRepository->search(new Criteria([$offerIdStruct->getId()]), $this->context);

        self::assertCount(1, $offer);
    }

    public function testUploadFile()
    {
        $offerIdStruct = $this->createOffer();
        $offerIdStruct = $this->uploadDocument($offerIdStruct);

        $criteria = new Criteria([$offerIdStruct->getId()]);
        $criteria->addAssociation('documentMediaFile');
        $offerRepository = $this->getContainer()->get('ies_offer.repository');
        /** @var EntitySearchResult $searchResult */
        $searchResult = $offerRepository->search($criteria, $this->context);
        /** @var OfferEntity $offer */
        $offer = $searchResult->first();

        self::assertEquals('test', $offer->getDocumentMediaFile()->getFileName());
    }

    public function testGetOfferDocument()
    {
        $offerIdStruct = $this->createOffer();
        $offerIdStruct = $this->uploadDocument($offerIdStruct);

        $criteria = new Criteria([$offerIdStruct->getId()]);
        $criteria->addAssociation('documentMediaFile');
        $offerRepository = $this->getContainer()->get('ies_offer.repository');
        $searchResult = $offerRepository->search($criteria, $this->context);
        /** @var OfferEntity $offer */
        $offer = $searchResult->first();

        $offerService = $this->getContainer()->get(DocumentService::class);

        /** @var GeneratedDocument $generatedDocument */
        $generatedDocument = $offerService->getDocument($offer, $this->context);
        self::assertEquals('test.pdf', $generatedDocument->getFilename());
    }

    private function createOffer(): DocumentIdStruct
    {
        $customerId = $this->createCustomer();
        $offerService = $this->getContainer()->get(DocumentService::class);
        $offerRepository = $this->getContainer()->get('ies_offer.repository');

        /** @var DocumentIdStruct $offerIdStruct */
        return $offerService->create($customerId, 'Angebot1234', $this->context, $offerRepository);
    }

    private function uploadDocument(DocumentIdStruct $offerIdStruct): DocumentIdStruct
    {
        $offerService = $this->getContainer()->get(DocumentService::class);
        $offerRepository = $this->getContainer()->get('ies_offer.repository');
        $path = realpath(__DIR__ . '/../data/test.pdf');
        $resource = @fopen($path, 'rb');
        $stat = fstat($resource);
        $request = new Request(
            [
                'fileName' => 'test',
                'extension' => 'pdf'
            ],
            [],
            [],
            [],
            [],
            ['CONTENT_LENGTH' => $stat['size']],
            $resource
        );

        return $offerService->uploadFile($offerIdStruct->getId(), $this->context, $request, $offerRepository);
    }

    private function createCustomer(): string
    {
        $customerId = Uuid::randomHex();
        $salutationId = $this->getValidSalutationId();
        $paymentMethodId = $this->getValidPaymentMethodId();

        $customer = [
            [
                'id' => $customerId,
                'salesChannelId' => Defaults::SALES_CHANNEL,
                'defaultShippingAddress' => [
                    'id' => $customerId,
                    'firstName' => 'Test',
                    'lastName' => 'Test',
                    'city' => 'Schöppingen',
                    'street' => 'Ebbinghoff 10',
                    'zipcode' => '48624',
                    'salutationId' => $salutationId,
                    'countryId' => $this->getValidCountryId(),
                ],
                'defaultBillingAddressId' => $customerId,
                'defaultPaymentMethodId' => $paymentMethodId,
                'groupId' => Defaults::FALLBACK_CUSTOMER_GROUP,
                'email' => Uuid::randomHex() . '@example.com',
                'password' => 'not',
                'firstName' => 'Test',
                'lastName' => 'Test',
                'salutationId' => $salutationId,
                'customerNumber' => '12345',
            ],
        ];

        $this->getContainer()->get('customer.repository')->create($customer, $this->context);

        return $customerId;
    }
}
