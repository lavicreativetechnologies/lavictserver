<?php declare(strict_types=1);

namespace Ultra\Breadcrumb;

use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\System\CustomField\CustomFieldTypes;

class UltraBreadcrumb extends Plugin
{
	public function install(InstallContext $context): void
	{
		$this->addDefaultConfiguration();
	}

	public function activate(ActivateContext $context): void
	{
		
		$customFieldSetRepository = $this->container->get('custom_field_set.repository');
        $customFieldSetRepository->upsert([$this->getFieldSet()], $context->getContext());
		
	}

	public function deactivate(DeactivateContext $context): void
	{
		
		$customFieldSetRepository = $this->container->get('custom_field_set.repository');
        $customFieldSetRepository->delete([$this->getFieldSet()], $context->getContext());
		
	}

	public function uninstall(UninstallContext $context): void
	{
		parent::uninstall($context);

		if ($context->keepUserData()) {
			return;
		}
	}

    private function addDefaultConfiguration(): void
    {


		//$this->setValue('activeonoff', true);

		
    }

    /**
     * @param string $configName
     * @param null $default
     */
    public function setValue(string $configName, $default = null) : void
    {
        $systemConfigService = $this->container->get(SystemConfigService::class);
        $domain = $this->getName() . '.config.';

        if( $systemConfigService->get($domain . $configName) === null )
        {
            $systemConfigService->set($domain . $configName, $default);
        }
    }
	
	private function getFieldSet()
    {
        return [
            'id' => 'af5a26965d914492b45bf30000000110',
            'name' => 'ultra_breacrumb',
            'config' => [
				"translated" => true,
                'label' => [
                    'de-DE' => 'Breadcrumb Toolbox',
                    'en-GB' => 'Breadcrumb Toolbox'
                ],
            ],
            'customFields' => [
				[
                    'id' => '60e874a1a32a495e88d36e0000000111',
                    'name' => 'ultra_breacrumb_turnon',
                    'type' => CustomFieldTypes::BOOL,
                    'config' => [
						'componentName' => 'sw-field',
						'type' => 'checkbox',
						'customFieldType' => 'checkbox',
                        'label' => [
                            'de-DE' => 'Breadcrumb deaktivieren',
                            'en-GB' => 'Deactivate breadcrumb',
                        ],
                        'customFieldPosition' => 1
                    ],
                ],
            ],
            'relations' => [
                ['id' => '2cc921dca23243c0901976870d9cc8a3', 'entityName' => 'category'],
            ]
        ];
    }
}
