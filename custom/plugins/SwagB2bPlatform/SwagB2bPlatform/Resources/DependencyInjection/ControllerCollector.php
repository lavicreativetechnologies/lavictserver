<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\DependencyInjection;

use RuntimeException;
use SwagB2bPlatform\Routing\RouteLoader;
use SwagB2bPlatform\Routing\RouteValue;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class ControllerCollector implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $controllers = $container->findTaggedServiceIds('b2b.controller');

        $routeMap = [];

        foreach ($controllers as $serviceId => $controller) {
            $service = $container->findDefinition($serviceId);

            if (!isset($controller[0]['route'])) {
                throw new RuntimeException('Invalid container configuration found at ' . $serviceId);
            }

            $routeMap[$controller[0]['route']] = new Definition(RouteValue::class, [
                $service->getClass(),
                $serviceId,
            ]);

            $service->setPublic(true);
            $service->addTag('controller.service_arguments');
        }

        $routeLoaderService = $container->findDefinition(RouteLoader::class);
        $routeLoaderService->replaceArgument(0, $routeMap);
    }
}
