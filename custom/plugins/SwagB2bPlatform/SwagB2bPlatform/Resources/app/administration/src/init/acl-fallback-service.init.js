import AclFallbackService from '../service/acl-fallback.service';

if (!Shopware.Service('acl')) {
    Shopware.Application.addServiceProvider('acl', () => {
        return new AclFallbackService();
    });
}
