import template from './sw-customer-detail-base.html.twig';

Shopware.Component.override('sw-customer-detail-base', {
    template,
    inject: [
        'repositoryFactory',
        'acl'
    ],
    computed: {
        dataRepository() {
            return this.repositoryFactory.create('b2b_customer_data');
        },
        b2bData() {
            if (!this.customer) {
                return null;
            }

            const { b2bCustomerData } = this.customer.extensions;
            if (b2bCustomerData) {
                return b2bCustomerData;
            }

            const data = this.dataRepository.create(Shopware.context);
            data.customer_id = this.customer.id;
            data.isDebtor = false;
            data.isSalesRepresentative = false;
            this.customer.extensions.b2bCustomerData = data;

            return data;
        }
    },
    methods: {
        uncheckRole(value, role) {
            if (!value) {
                return;
            }

            this.b2bData[role] = false;
        }
    }
});
