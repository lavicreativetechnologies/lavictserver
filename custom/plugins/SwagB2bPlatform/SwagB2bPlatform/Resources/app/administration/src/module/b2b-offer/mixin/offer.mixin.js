import { OFFER_STATES } from '../../../service/offer.api.service';
import { DISCOUNT_ID, DISCOUNT_TYPES } from '../constants';

Shopware.Mixin.register('b2b-offer', {
    data() {
        return {
            DISCOUNT_ID,
            DISCOUNT_TYPES
        }
    },
    computed: {
        statusDeclined() {
            if (!this.offer) {
                return false;
            }

            return this.offer.status === OFFER_STATES.DECLINED_BY_ADMIN;
        },
        statusAccepted() {
            if (!this.offer) {
                return false;
            }

            return this.offer.status === OFFER_STATES.SENT_BY_ADMIN;
        },
        statusAcceptedBoth() {
            if (!this.offer) {
                return false;
            }

            return this.offer.status === OFFER_STATES.ACCEPTED_BY_BOTH;
        },
        statusDeclinedUser() {
            if (!this.offer) {
                return false;
            }

            return this.offer.status === OFFER_STATES.DECLINED_BY_USER;
        },
        statusConverted() {
            if (!this.offer) {
                return false;
            }

            return this.offer.status === OFFER_STATES.CONVERTED;
        },
        statusButtonDisabled() {
            return this.statusAccepted || this.statusAcceptedBoth || this.statusConverted || this.statusDeclined || this.statusDeclinedUser;
        }
    },
    methods: {
        getUpdatePayload(comment = '', expirationDate = null) {
            if (!this.offer) {
                return;
            }

            const payload = {
                comment,
                expirationDate,
                orderContextId: this.offer.orderContextId,
                positions: []
            };

            this.positions.forEach((position) => {
                if (position.id === DISCOUNT_ID) {
                    const { newDiscountAbsolute, newDiscountPercentage } = position;
                    payload.discountAbsolute = newDiscountAbsolute;
                    payload.discountPercentage = newDiscountPercentage;
                } else {
                    const { id, name, referenceNumber, quantity, discountAmountNet, updated, created } = position;
                    payload.positions.push({
                        id: created ? null : id,
                        name,
                        referenceNumber,
                        quantity,
                        discountAmountNet,
                        created,
                        updated
                    });
                }
            });

            return payload;
        }
    }
});
