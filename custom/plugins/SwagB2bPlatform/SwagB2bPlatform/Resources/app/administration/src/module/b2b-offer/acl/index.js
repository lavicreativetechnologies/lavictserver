if (Shopware.Service('privileges')) {
    Shopware.Service('privileges').addPrivilegeMappingEntry({
        category: 'permissions',
        parent: 'orders',
        key: 'b2b-offer',
        roles: {
            viewer: {
                privileges: [],
                dependencies: [
                    'customer.viewer'
                ]
            },
            editor: {
                privileges: [],
                dependencies: [
                    'b2b-offer.viewer'
                ]
            },
            deleter: {
                privileges: [],
                dependencies: [
                    'b2b-offer.viewer'
                ]
            }
        }
    });
}
