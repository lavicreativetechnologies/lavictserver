/* istanbul ignore file */

export const OFFER_STATES = {
    EXPIRED: 'offer_status_expired',
    OPEN: 'offer_status_open',
    SENT_BY_USER: 'offer_status_accepted_user',
    SENT_BY_ADMIN: 'offer_status_accepted_admin',
    DECLINED_BY_USER: 'offer_status_declined_user',
    DECLINED_BY_ADMIN: 'offer_status_declined_admin',
    ACCEPTED_BY_BOTH: 'offer_status_accepted_both',
    CONVERTED: 'offer_status_converted'
};

export default class OfferApiService extends Shopware.Classes.ApiService {
    constructor(httpClient, loginService, apiEndpoint = '_action/offer') {
        super(httpClient, loginService, apiEndpoint);
    }

    static get name() {
        return 'offerApiService'
    }

    getOfferList(additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.get(`${this.getApiBasePath()}`, { params, headers })
          .then((response) => {
              const responseData = Shopware.Classes.ApiService.handleResponse(response);

              return responseData || { data: [], meta: { total: 0 } };
          });
    }

    getOfferDetail(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.get(`${this.getApiBasePath(offerId)}`, { params, headers })
          .then((response) => {
              const responseData = Shopware.Classes.ApiService.handleResponse(response);
              return responseData.data || null;
          });
    }

    updateOffer(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath(offerId)}/update`, params, { headers })
          .then((response) => {
              return response.status === 200;
          });
    }

    delete(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath(offerId)}/delete`, params, { headers })
          .then((response) => {
              return response.status === 200;
          });
    }

    bulkDelete(additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath()}/bulk-delete`, params, { headers })
          .then((response) => {
              return response.status === 200;
          });
    }

    declineOffer(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath(offerId)}/decline`, params, { headers })
          .then((response) => {
              const responseData = Shopware.Classes.ApiService.handleResponse(response);

              return responseData.updatedStatus || null;
          });
    }

    acceptOffer(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = additionalParams;
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath(offerId)}/accept`, params, { headers })
          .then((response) => {
              const responseData = Shopware.Classes.ApiService.handleResponse(response);

              return responseData.updatedStatus || null;
          });
    }

    getOfferPreview(offerId, additionalParams = {}, additionalHeaders = {}) {
        const params = Object.assign(additionalParams, { offerId: offerId });
        const headers = this.getBasicHeaders(additionalHeaders);

        return this.httpClient.post(`${this.getApiBasePath()}/preview`, params, { headers })
            .then((response) => {
                const responseData = Shopware.Classes.ApiService.handleResponse(response);

                return responseData.data || null;
            });
    }
}
