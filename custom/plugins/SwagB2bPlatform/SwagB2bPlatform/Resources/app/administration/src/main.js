/* istanbul ignore file */
import './init/acl-fallback-service.init';
import './init/sales-representative-service.init';
import './init/offer-service.init';
import './init/offer-line-item-reference-service.init';
import './init/offer-log-service.init';
import './init/log-item.service';
import './module/b2b-user';
import './module/b2b-salesrep-clients';
import './module/b2b-offer';
import './extension/sw-customer';
import './extension/sw-order';

import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
