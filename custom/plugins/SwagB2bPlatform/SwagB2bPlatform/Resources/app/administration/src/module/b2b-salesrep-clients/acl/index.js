if (Shopware.Service('privileges')) {
    Shopware.Service('privileges').addPrivilegeMappingEntry({
        category: 'permissions',
        parent: 'customers',
        key: 'sales-representative-clients',
        roles: {
            viewer: {
                privileges: [

                ],
                dependencies: [
                    'b2b-user.viewer'
                ]
            },
            editor: {
                privileges: [

                ],
                dependencies: [
                    'b2b-user.editor'
                ]
            }
        }
    });
}
