const { Component, Mixin } = Shopware;
import template from './offer-discount.html.twig';
import './offer-discount.scss';

const NAME = 'b2b-offer-history-OfferDiscount';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\Offer\\Framework\\AuditLogDiscountEntity',
    mixins: [
        Mixin.getByName('b2b-currency')
    ],
    props: {
        item: {
            required: true,
            type: Object
        }
    },
    computed: {
        discountChanged() {
            return this.item.logValue.oldValue !== 0;
        },
        formattedOldDiscountValue() {
            const value = this.item.logValue.oldValue;

            if (this.item.logValue.oldDiscountWasAbsolute) {
                return this.formatCurrency(value);
            }

            return `${value} %`;
        },
        formattedNewDiscountValue() {
            const value = this.item.logValue.newValue;

            if (this.item.logValue.newDiscountIsAbsolute) {
                return this.formatCurrency(value);
            }

            return `${value} %`;
        },

    },
    methods: {
        getContentTitleSnippet() {
            if (this.discountChanged) {
                return this.$tc('b2b-offer.detail.history.titleDiscount');
            }

            return this.$tc('b2b-offer.detail.history.titleDiscountAdded');
        },
    }
});