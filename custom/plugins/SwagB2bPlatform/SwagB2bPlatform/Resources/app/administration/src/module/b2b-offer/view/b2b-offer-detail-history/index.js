import template from './b2b-offer-detail-history.html.twig';
import './b2b-offer-detail-history.scss';

export const COMPONENT_NAME = 'b2b-offer-detail-history';

Shopware.Component.register(COMPONENT_NAME, {
    template,
    props: {
        offer: {
            required: true,
            type: Object
        },
        activity: {
            required: true,
            type: Object
        },
        isLoading: {
            required: false,
            type: Boolean
        }
    },
    data() {
        return {
            message: ''
        }
    },
    watch: {
        activity: function(newActivity, oldActivity) {
            this.$nextTick(function() {
                var historyContainer = this.$refs.historyContainer;
                historyContainer.scrollTop = historyContainer.scrollHeight;
            });
        }
    },
    computed: {
        activityIsEmpty() {
            return Object.keys(this.activity).length === 0;
        },
    },
    methods: {
        handleSubmit() {
            const { isLoading, message } = this;

            if (isLoading || !message.match(/\S/g)) {
                return;
            }

            this.message = '';

            this.$emit('add-comment', message);
        },
        getItemAlignment(item) {
            return item.authorIdentity.isBackend ? 'right' : 'left';
        },
    }
});
