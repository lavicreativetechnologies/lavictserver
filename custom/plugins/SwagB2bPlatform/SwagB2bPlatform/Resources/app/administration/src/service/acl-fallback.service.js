export default class AclFallbackService {
    constructor() {
        this.name = 'acl';
    }

    can() {
        return true;
    }
}
