const { Component } = Shopware;
import template from './status-change.html.twig';
import './status-change.scss';

const NAME = 'b2b-offer-history-status-change';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\Offer\\Framework\\AuditLogValueOfferDiffEntity',
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});
