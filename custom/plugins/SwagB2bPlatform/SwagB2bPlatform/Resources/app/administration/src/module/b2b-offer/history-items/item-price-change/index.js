const { Component, Mixin } = Shopware;
import template from './item-price-change.html.twig';
import './item-price-change.scss';

const NAME = 'b2b-offer-history-item-price-change';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\Offer\\Framework\\AuditLogValueLineItemPriceEntity',
    mixins: [
        Mixin.getByName('b2b-currency')
    ],
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});