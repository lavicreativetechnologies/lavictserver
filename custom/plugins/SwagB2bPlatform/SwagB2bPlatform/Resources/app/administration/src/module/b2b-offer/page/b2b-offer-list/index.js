import template from './b2b-offer-list.html.twig';
import './b2b-offer-list.scss';
import OfferApiService, { OFFER_STATES } from '../../../../service/offer.api.service';

const { Component, Mixin } = Shopware;
export const COMPONENT_NAME = 'b2b-offer-list';

Component.register(COMPONENT_NAME, {
    template,
    inject: {
        offerService: OfferApiService.name,
        acl: 'acl'
    },
    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('b2b-date'),
        Mixin.getByName('b2b-currency')
    ],
    data() {
        return {
            term: '',
            isLoading: true,
            isBulkLoading: false,
            showDeleteModal: false,
            showBulkDeleteModal: false,
            page: 1,
            limit: 10,
            filterStateId: null,
            offers: [],
            selection: []
        };
    },
    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },
    computed: {
        offerColumns() {
            return this.getOfferColumns();
        },
        total() {
            return this.offers.length;
        }
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.getList();
        },
        onRefresh() {
            this.getList();
        },
        onPageChange({ page, limit }) {
            this.page = page;
            this.limit = limit;

            this.onRefresh();
        },
        onChangeFilterState(stateId) {
            this.filterStateId = stateId;
            this.onRefresh();
        },
        handleSelectionChange(newSelection) {
            this.selection = [];

            Object.values(newSelection).forEach((selectedOffer) => {
                this.selection.push(selectedOffer.id);
            });
        },
        onDelete(id) {
            this.showDeleteModal = id;
        },
        onCloseDeleteModal() {
            this.showDeleteModal = false;
        },
        getDebtorLink(item) {
            return {
                name: 'sw.customer.detail',
                params: {
                    id: item.id
                }
            };
        },
        async getList() {
            try {
                const { page, limit } = this;
                this.isLoading = true;

                const { data } = await this.offerService.getOfferList({ page, limit });

                this.offers = data;
            } catch {
                this.createNotificationError({
                    title: this.$tc('b2b-offer.notifications.titleError'),
                    message: this.$tc('b2b-offer.notifications.messageListError')
                });
            } finally {
                this.isLoading = false;
            }
        },
        async onConfirmBulkDelete() {
            try {
                this.isBulkLoading = true;
                await this.offerService.bulkDelete({ offerIds: this.selection });
                this.onRefresh();
            } catch {
                this.createNotificationError({
                    title: this.$tc('b2b-offer.notifications.titleError'),
                    message: this.$tc('b2b-offer.notifications.messageDeleteError')
                });
            } finally {
                this.showBulkDeleteModal = false;
                this.isBulkLoading = false;
            }
        },
        async onConfirmDelete(id) {
            try {
                this.showDeleteModal = false;
                await this.offerService.delete(id.toString());
                this.onRefresh();
            } catch {
                this.createNotificationError({
                    title: this.$tc('b2b-offer.list.titleError'),
                    message: this.$tc('b2b-offer.list.messageDeleteError')
                });
            }
        },
        getStateLabelClass(state) {
            const baseClass = 'b2b-offer-list__state-label';
            let modifier = '';

            switch (state) {
                case OFFER_STATES.EXPIRED:
                case OFFER_STATES.OPEN:
                case OFFER_STATES.SENT_BY_USER:
                    modifier = 'open';
                    break;
                case OFFER_STATES.SENT_BY_ADMIN:
                    modifier = 'sent';
                    break;
                case OFFER_STATES.DECLINED_BY_ADMIN:
                case OFFER_STATES.DECLINED_BY_USER:
                    modifier = 'rejected';
                    break;
                case OFFER_STATES.ACCEPTED_BY_BOTH:
                case OFFER_STATES.CONVERTED:
                    modifier = 'accepted';
                    break;
            }

            return `${baseClass}--${modifier}`;
        },
        getOfferColumns() {
            return [{
                property: 'id',
                label: 'b2b-offer.list.columnId',
                allowResize: true,
                visible: false
            }, {
                property: 'debtor.email',
                label: 'b2b-offer.list.columnDebtor',
                allowResize: true
            }, {
                property: 'debtor.company',
                label: 'b2b-offer.list.columnDebtorCompany',
                allowResize: true
            }, {
                property: 'status',
                label: 'b2b-offer.list.columnState',
                allowResize: true
            }, {
                property: 'listAmountNet',
                label: 'b2b-offer.list.columnListAmountNet',
                allowResize: true,
                align: 'right'
            }, {
                property: 'discountAmountNet',
                label: 'b2b-offer.list.columnListDiscountAmountNet',
                allowResize: true,
                align: 'right'
            }, {
                property: 'discountValueNet',
                label: 'b2b-offer.list.columnTotalDiscount',
                allowResize: true,
                align: 'right'
            }, {
                property: 'createdAt',
                label: 'b2b-offer.list.columnCreated',
                allowResize: true
            }, {
                property: 'changedStatusAt',
                label: 'b2b-offer.list.columnUpdated',
                allowResize: true
            }, {
                property: 'expiredAt',
                label: 'b2b-offer.list.columnExpires',
                allowResize: true
            }, {
                property: 'listPositionCount',
                label: 'b2b-offer.list.columnPositions',
                allowResize: true,
                align: 'right'
            }];
        }
    }
});
