import template from './b2b-offer-detail.html.twig';
import './b2b-offer-detail.scss';
import OfferApiService from '../../../../service/offer.api.service';
import OfferLogApiService from '../../../../service/offer-log.api.service';
import OfferLineItemReferenceApiService from '../../../../service/offer-line-item-reference.api.service';

export const COMPONENT_NAME = 'b2b-offer-detail';

const { Component, Mixin } = Shopware;

Component.register(COMPONENT_NAME, {
    template,
    inject: {
        offerService: OfferApiService.name,
        offerLogService: OfferLogApiService.name,
        offerLineItemReferenceService: OfferLineItemReferenceApiService.name
    },
    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('b2b-offer')
    ],
    props: {
        offerId: {
            required: true,
            type: String
        }
    },
    watch: {
        $route(to, from) {
            this.onRouteChange(to, from);
        }
    },
    data() {
        return {
            offer: {},
            isLoading: false,
            showEditButton: true,
            isLoadingBase: false,
            isLoadingDiscount: false,
            isLoadingActivity: false,
            activity: {},
            positions: [],
        };
    },
    metaInfo() {
        return {
            title: this.$createTitle(this.offerId)
        };
    },
    computed: {
        editMode: {
            get() {
                if (!this.$route.query) {
                    return false;
                }

                const { edit } = this.$route.query;
                return edit === 'true' || edit === true;
            },
            set(editMode) {
                let route = {
                    name: this.$route.name
                };

                if (editMode && !this.statusAcceptedBoth) {
                    route.query = {
                        edit: true
                    }
                }

                this.$router.push(route);
            }
        },
        requestOrderContextId() {
            return this.offer.orderContextId.toString();
        }
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.onRouteChange(this.$route);
        },
        onRouteChange(to, from = to) {
            if (this.statusAcceptedBoth) {
                this.editMode = false;
            }

            if (to.name !== from.name || (this.offer.id && this.editMode)) {
                return;
            }

            this.refresh();
        },
        onSetEditMode(state) {
            this.editMode = state;

            if (!state) {
                this.refresh();
            }
        },
        onItemAdded() {
            for (let i = 0; i < this.positions.length; i++) {
                if (this.positions[i].id === this.DISCOUNT_ID) {
                    this.positions.push(this.positions.splice(i, 1)[0]);
                    break;
                }
            }
        },
        addDiscountPosition() {
            this.positions.push({
                id: this.DISCOUNT_ID,
                name: this.$tc('b2b-offer.detail.discount.labelDiscount'),
                discountAbsolute: this.offer.discountValueNet,
                discountPercentage: this.offer.percentageDiscount,
                newDiscountAbsolute: this.offer.discountValueNet,
                newDiscountPercentage: this.offer.percentageDiscount
            });
        },
        async onUpdateOffer(comment, expirationDate) {
            try {
                this.isLoadingDiscount = true;
                const payload = this.getUpdatePayload(comment, expirationDate);
                await this.offerService.updateOffer(this.offerId, payload);
                this.refresh();
            } catch {
                this.handleOfferUpdateError();
            } finally {
                this.isLoadingDiscount = false;
            }
        },
        async refresh() {
            try {
                this.isLoading = true;
                this.editMode = false;

                await this.getOffer();

                if (!this.offer.id) {
                    return;
                }

                await Promise.all([
                    this.getActivityData(),
                    this.getPositions()
                ]);
            } catch {
                this.handleOfferDetailError();
            } finally {
                this.isLoading = false
            }
        },
        async onDeclineOffer() {
            try {
                this.offer.status = await this.offerService.declineOffer(this.offerId);
                this.getActivityData();
            } catch {
                this.handleOfferUpdateError();
            }
        },
        async onAcceptOffer() {
            try {
                this.offer.status = await this.offerService.acceptOffer(this.offerId);
                this.getActivityData();
            } catch {
                this.handleOfferUpdateError();
            }
        },
        async onAddComment(message) {
            try {
                this.isLoadingActivity = true;
                await this.offerLogService.addComment(this.requestOrderContextId, { comment: message });
                await this.getActivityData();
            } catch {
                this.handleCommentError();
            } finally {
                this.isLoadingActivity = false;
            }
        },
        async getOffer() {
            try {
                this.isLoading = true;
                this.offer = await this.offerService.getOfferDetail(this.offerId);
            } catch {
                this.handleOfferDetailError();
            } finally {
                this.isLoading = false;
            }
        },
        async getActivityData() {
            try {
                this.isLoadingActivity = true;
                this.activity = await this.offerLogService.getOfferActivity(this.requestOrderContextId);
            } catch {
                this.handleOfferDetailError();
            } finally {
                this.isLoadingActivity = false;
            }
        },
        async getPositions() {
            try {
                this.isLoadingDiscount = true;
                this.positions = await this.offerLineItemReferenceService.getAllPositions(this.offerId);
                this.addDiscountPosition();
            } catch {
                this.handleOfferDetailError();
            } finally {
                this.isLoadingDiscount = false;
            }
        },
        handleOfferDetailError() {
            this.createNotificationError({
                title: this.$tc('b2b-offer.notifications.titleError'),
                message: this.$tc('b2b-offer.notifications.messageDetailError')
            });
        },
        handleOfferUpdateError() {
            this.createNotificationError({
                title: this.$tc('b2b-offer.notifications.titleError'),
                message: this.$tc('b2b-offer.notifications.messageOfferUpdatedError')
            });
        },
        handleCommentError() {
            this.createNotificationError({
                title: this.$tc('b2b-offer.notifications.titleError'),
                message: this.$tc('b2b-offer.notifications.messageCommentError')
            });
        }
    }
});
