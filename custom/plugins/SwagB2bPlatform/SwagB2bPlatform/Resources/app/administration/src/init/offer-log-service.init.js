/* istanbul ignore file */
import OfferLogService from '../service/offer-log.api.service';

Shopware.Application.addServiceProvider(OfferLogService.name, (container) => {
    const initContainer = Shopware.Application.getContainer('init');
    return new OfferLogService(initContainer.httpClient, container.loginService);
});