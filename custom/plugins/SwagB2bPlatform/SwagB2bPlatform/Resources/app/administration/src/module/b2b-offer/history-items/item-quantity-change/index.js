const { Component } = Shopware;
import template from './item-quantity-change.html.twig';
import './item-quantity-change.scss';

const NAME = 'b2b-offer-history-item-quantity-change';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\AuditLog\\Framework\\AuditLogValueLineItemQuantityEntity',
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});
