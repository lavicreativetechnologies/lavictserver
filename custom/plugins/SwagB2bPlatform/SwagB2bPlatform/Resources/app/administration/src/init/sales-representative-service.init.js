/* istanbul ignore file */
import SalesRepresentativeService from '../service/sales-representative.api.service';

Shopware.Application.addServiceProvider(SalesRepresentativeService.name, (container) => {
    const initContainer = Shopware.Application.getContainer('init');
    return new SalesRepresentativeService(initContainer.httpClient, container.loginService);
});