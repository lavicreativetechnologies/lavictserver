import template from './b2b-offer-history-item-author.html.twig';
import './b2b-offer-history-item-author.scss';

const COMPONENT_NAME = 'b2b-offer-history-item-author';

const { Component, Mixin } = Shopware;

Component.register(COMPONENT_NAME, {
    template,
    mixins: [
        Mixin.getByName('b2b-date'),
        Mixin.getByName('b2b-currency')
    ],
    props: {
        item: {
            required: true,
            type: Object
        },
        align: {
            required: true,
            type: String,
            validator(value) {
                return ['right', 'left',].includes(value);
            }
        }
    },
    computed: {
        firstName() {
            return this.item.authorIdentity.firstName;
        },
        lastName() {
            return this.item.authorIdentity.lastName;
        }
    }
});