const { Component } = Shopware;
import template from './item-remove.html.twig';
import './item-remove.scss';

const NAME = 'b2b-offer-history-item-remove';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\AuditLog\\Framework\\AuditLogValueLineItemRemoveEntity',
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});