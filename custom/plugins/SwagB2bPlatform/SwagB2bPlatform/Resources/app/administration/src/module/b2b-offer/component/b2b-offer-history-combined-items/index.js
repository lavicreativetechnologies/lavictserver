import template from './b2b-offer-history-combined-items.html.twig';
import './b2b-offer-history-combined-items.scss';

const COMPONENT_NAME = 'b2b-offer-history-combined-items';

const { Component } = Shopware;

Component.register(COMPONENT_NAME, {
    template,
    props: {
        activity: {
            required: true,
            type: Array
        },
    },
    computed: {
        getHistoryItemAlignmentClass() {
            return this.activity[0].authorIdentity.isBackend ? 'right' : 'left';
        }
    }
});