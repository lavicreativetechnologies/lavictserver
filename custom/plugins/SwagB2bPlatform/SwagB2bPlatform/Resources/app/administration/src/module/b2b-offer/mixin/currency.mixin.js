const { Context } = Shopware;

let _systemCurrency = null;

Shopware.Mixin.register('b2b-currency', {
    filter: [
        Shopware.Filter.getByName('currency')
    ],
    inject: ['repositoryFactory'],
    computed: {
        currencyRepository() {
            return this.repositoryFactory.create('currency');
        },
        currencyIsoCode() {
            if (!_systemCurrency) {
                return 'EUR';
            }

            return _systemCurrency.isoCode;
        },
        currencyDecimalPrecision() {
            if (!_systemCurrency) {
                return 2;
            }

            return _systemCurrency.decimalPrecision;
        }
    },
    mounted() {
        this.mountedComponent();
    },
    methods: {
        async mountedComponent() {
            await this.loadSystemCurrency();
        },
        formatCurrency(value = 0) {
            const { currencyIsoCode, currencyDecimalPrecision } = this;

            return Shopware.Filter.getByName('currency')(
                value,
                currencyIsoCode,
                currencyDecimalPrecision
            );
        },
        async loadSystemCurrency() {
            if (_systemCurrency) {
                return _systemCurrency;
            }

            _systemCurrency = Object.assign({}, await this.currencyRepository.get(
                Context.app.systemCurrencyId,
                Context.api
            ));

            return _systemCurrency;
        }
    }
});
