/* istanbul ignore file */
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';
import './acl';
import './mixin/currency.mixin';
import './mixin/date.mixin';
import './mixin/offer.mixin';
import './component/b2b-offer-history-item';
import './component/b2b-offer-history-item-author';
import './component/b2b-offer-discount-grid';
import './component/b2b-offer-history-combined-items';
import './history-items';
import { COMPONENT_NAME as B2bOfferList_Name } from './page/b2b-offer-list';
import { COMPONENT_NAME as B2bOfferDetail_Name } from './page/b2b-offer-detail';
import './view/b2b-offer-detail-base';
import './view/b2b-offer-detail-discount';
import './view/b2b-offer-detail-history';

const MODULE_NAME = 'b2b-offer';

Shopware.Module.register(MODULE_NAME, {
    type: 'plugin',
    name: 'B2B Offer',
    title: 'b2b-offer.general.title',
    description: 'b2b-offer.general.description',
    color: '#A092F0',
    icon: 'default-action-tags',
    favicon: 'icon-module-orders.png',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },
    routes: {
        index: {
            privilege: 'b2b-offer.viewer',
            components: {
                default: B2bOfferList_Name
            },
            path: ''
        },
        detail: {
            privilege: 'b2b-offer.viewer',
            component: B2bOfferDetail_Name,
            path: ':id',
            meta: {
                parentPath: 'b2b.offer.index'
            },
            props: {
                default(route) {
                    return {
                        offerId: route.params.id.toString()
                    };
                }
            }
        }
    },
    navigation: [{
        privilege: 'b2b-offer.viewer',
        path: 'b2b.offer.index',
        label: 'b2b-offer.general.label',
        color: '#A092F0',
        icon: 'default-action-tags',
        parent: 'sw-order'
    }]
});
