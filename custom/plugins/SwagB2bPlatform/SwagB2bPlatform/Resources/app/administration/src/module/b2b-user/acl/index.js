if (Shopware.Service('privileges')) {
    Shopware.Service('privileges').addPrivilegeMappingEntry({
        category: 'permissions',
        parent: 'customers',
        key: 'b2b-user',
        roles: {
            viewer: {
                privileges: [
                    'b2b_customer_data:read'
                ],
                dependencies: [
                    'customer.viewer'
                ]
            },
            editor: {
                privileges: [
                    'b2b_customer_data:update'
                ],
                dependencies: [
                    'b2b_customer_data:create',
                    'b2b-user.viewer',
                    'customer.editor'
                ]
            }
        }
    });
}
