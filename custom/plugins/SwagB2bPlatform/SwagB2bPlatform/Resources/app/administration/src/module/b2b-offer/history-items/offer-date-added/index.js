const { Component, Mixin } = Shopware;
import template from './offer-date-added.html.twig';
import './offer-date-added.scss';

const NAME = 'b2b-offer-history-offer-date-added';

export default Component.register(NAME, {
    template,
    name: NAME,
    logClass: 'Shopware\\B2B\\Offer\\Framework\\AuditLogExpirationDate',
    mixins: [
        Mixin.getByName('b2b-date'),
    ],
    props: {
        item: {
            required: true,
            type: Object
        }
    },
});