import { B2BEvents } from '../enums';
import { AjaxPanelInterface, EventInterface, InitOnLoad } from '../decorator';

declare const $;

@AjaxPanelInterface
@EventInterface
@InitOnLoad
export default class AjaxPanelModalPlugin extends window.PluginBaseClass {
    public static options = {
        CLASS_MODAL_MODIFIER: 'b2b-modal-panel',
        CLASS_LINK_MODAL_SMALL: 'link-modal--small',
        CLASS_MODAL_SMALL: 'modal--small',
        CLASS_NAVIGATION_CONTAINER: 'tab--container',
        SELECTOR_PANEL: '.b2b--ajax-panel',
        SELECTOR_PANEL_CLOSE: '.b2b--close-modal-box',
        SELECTOR_NO_NAVIGATION: '.no--navigation',
        SELECTOR_NAVIGATION_TOGGLE: '.b2b--modal-navigation--toggle',
        SELECTOR_MODAL: '.js--modal',
    };

    private modal: HTMLElement = null;

    public init(): void {
        this.applyEventListeners();
    }

    protected applyEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_BEFORE_LOAD, this.handleModalBeforeLoad.bind(this));
        this._subscribe(B2BEvents.PANEL_BEFORE_LOAD, this.handleSmallModalBeforeLoad.bind(this));
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoad.bind(this));
    }

    protected handleModalBeforeLoad(event: Event, { ajaxData, panel }: AjaxPanelEventData): void {
        if (!this.isModal(panel)) {
            return;
        }

        this._breakEventExecution(event);

        this.modal = this.openModal(panel.outerHTML);
        this.setModalAttributes(ajaxData);
        this._publish(B2BEvents.PANEL_REGISTER, this.modal);
    }

    protected handleSmallModalBeforeLoad(event: Event, { source }: AjaxPanelEventData): void {
        if (this.isModalOrModalLink(source)) {
            this.handleSmallModal(this.isSmallModal(source));
        }
    }

    protected openModal(content): HTMLElement {
        return $.modal.open(content)._$content[0].querySelector(this.options.SELECTOR_PANEL);
    }

    protected closeModal(): void {
        $.modal.close();
    }

    protected handlePanelAfterLoad(event: Event, { panel }: AjaxPanelEventData): void {
        if (this.shouldCloseOnSuccess(panel)) {
            this.closeModal();
        } else {
            this.toggleNavigation(panel);
        }
    }

    protected setModalAttributes(ajaxData: AjaxPanelEventData['ajaxData']): void {
        this.modal.dataset.url = ajaxData.url;
        this.modal.dataset.payload = JSON.stringify(ajaxData.data);
        this.modal.dataset.b2bModalAjaxPanel = 'true';
        this.modal.classList.remove(this.options.CLASS_MODAL_MODIFIER);
    }

    protected isModal(element: HTMLElement): boolean {
        return element.classList.contains(this.options.CLASS_MODAL_MODIFIER);
    }

    protected isModalOrModalLink(target): Boolean {
        let currentTarget = target;
        let isModal = false;

        while (currentTarget) {
            if (currentTarget.classList.contains('js--modal') || currentTarget.classList.contains('link-modal--small')) {
                isModal = true;
                currentTarget = false;
            }
            currentTarget = currentTarget.parentElement;
        }

        return isModal;
    }

    protected isSmallModal(element: HTMLElement): Boolean {
        const { CLASS_LINK_MODAL_SMALL, CLASS_MODAL_SMALL } = this.options;

        return element.classList.contains(CLASS_LINK_MODAL_SMALL) || element.classList.contains(CLASS_MODAL_SMALL);
    }

    protected handleSmallModal(isSmallModal): void {
        const modalContainer = document.querySelector(this.options.SELECTOR_MODAL);

        if (modalContainer) {
            if (isSmallModal) {
                modalContainer.classList.add('is--small');
            } else {
                modalContainer.classList.remove('is--small');
            }
        }
    }

    protected toggleNavigation(panel: HTMLElement): void {
        const { CLASS_NAVIGATION_CONTAINER, SELECTOR_NAVIGATION_TOGGLE } = this.options;

        if (panel.classList.contains(CLASS_NAVIGATION_CONTAINER)) {
            return;
        }

        const toggle = document.querySelector(SELECTOR_NAVIGATION_TOGGLE);

        if (!toggle) {
            return;
        }

        toggle.parentElement.classList.toggle('navigation--hidden', this.shouldHideNavigation(panel));
    }

    protected shouldHideNavigation(panel: HTMLElement): boolean {
        return !!panel.querySelector(this.options.SELECTOR_NO_NAVIGATION);
    }

    protected shouldCloseOnSuccess(panel: HTMLElement): boolean {
        return !!panel.querySelector('[data-modal-close]');
    }
}
