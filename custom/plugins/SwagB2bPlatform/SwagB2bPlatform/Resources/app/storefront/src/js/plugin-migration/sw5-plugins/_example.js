// Import the plugin base object containing defaults and methods
import { b2bAjaxPanel } from '../../jsPluginBaseObjects';

// If necessary, provide overrides as an additional object
// NOTE: You cannot use arrow functions, if you want to stay inside the plugin scope.
const overrides = {
  error(e) {
    console.log(e); // eslint-disable-line no-console
  },
};

// Merge the base object, overrides and sw6 specific parameters.
// If you dont need any overrides, just remove the "overrides"-parameter. (see Object.assign documentation)
export default ({
  ...b2bAjaxPanel,
  ...overrides,
  name: 'b2bAjaxPanel',
  initOnLoad: true,
  selector: 'body',
});
