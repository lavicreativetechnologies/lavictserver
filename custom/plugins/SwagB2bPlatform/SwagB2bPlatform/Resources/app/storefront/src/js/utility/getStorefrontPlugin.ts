/* istanbul ignore file */

import { IPluginBaseClass } from '../interfaces';

function getStorefrontPlugin(pluginName: string): IPluginBaseClass[];
function getStorefrontPlugin(pluginName: string, firstOnly: boolean = true): IPluginBaseClass | IPluginBaseClass[] {
    const pluginMap = window.PluginManager.getPlugin(pluginName);
    const instances: IPluginBaseClass = pluginMap.get('instances');

    if (!instances.length) {
        return null;
    }

    return firstOnly ? instances[0] : instances;
}

export default getStorefrontPlugin;
