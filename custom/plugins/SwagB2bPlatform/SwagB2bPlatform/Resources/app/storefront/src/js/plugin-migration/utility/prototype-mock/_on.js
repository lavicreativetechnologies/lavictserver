export default async function _on(...args) {
  $(args[0]).on(...Array.from(args).slice(1));
  return this;
}
