import * as CSRFFallback from './csrf-polyfills';
import * as JqueryPolyfills from './jquery-polyfills';
import * as JqueryElPolyfills from './jquery-el-polyfills';
import * as StateManagerPolyfills from './stateManager-polyfills';

export default function () {
  return new Promise((resolve) => {
    /* eslint-disable */
    jQuery.extend(jQuery.fn, JqueryElPolyfills);
    jQuery.extend(jQuery, JqueryPolyfills);

    Event.prototype.isDefaultPrevented = function () {
      return this.defaultPrevented;
    };

    (function ($) {
      const context = $({});
      $.subscribe = function () {
        context.on.apply(context, arguments);
      };
      $.unsubscribe = function () {
        context.off.apply(context, arguments);
      };
      $.publish = function () {
        context.trigger.apply(context, arguments);
      };
    }(jQuery));

    window.CSRF = window.CSRF || CSRFFallback;
    window.StateManager = Object.assign(window.StateManager || {}, StateManagerPolyfills);

    resolve(true);
    /* eslint-enable */
  });
}
