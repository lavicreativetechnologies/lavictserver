/* eslint-disable */

export default class StringHelper {
    static ucFirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    static lcFirst(string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }

    static toDashCase(string) {
        return string.replace(/([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase();
    }

    static toLowerCamelCase(string, separator) {
        const upperCamelCase = StringHelper.toUpperCamelCase(string, separator);

        return StringHelper.lcFirst(upperCamelCase);
    }

    static toUpperCamelCase(string, separator) {
        if (!separator) {
            return StringHelper.ucFirst(string.toLowerCase());
        }

        const stringParts = string.split(separator);

        return stringParts.map((string) => StringHelper.ucFirst(string.toLowerCase())).join('');
    }

    static parsePrimitive(value) {
        try {
            if (/^\d+(.|,)\d+$/.test(value)) {
                value = value.replace(',', '.');
            }

            return JSON.parse(value);
        } catch (e) {
            return value.toString();
        }
    }
}

/* eslint-enable */
