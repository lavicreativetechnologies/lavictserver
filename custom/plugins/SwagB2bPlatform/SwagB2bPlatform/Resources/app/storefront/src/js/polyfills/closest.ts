/* istanbul ignore else */
if (window.Element && !Element.prototype.closest) {
    Element.prototype.closest = function(selector: string) {
        const matches = (this.document || this.ownerDocument).querySelectorAll(selector);

        let i = matches.length;
        let el = this;

        do {
            while (--i >= 0 && matches.item(i) !== el) {
                // @ts-ignore
            }
            el = el.parentElement;
        } while ((i < 0) && el.parentElement);

        return el;
    };
}
