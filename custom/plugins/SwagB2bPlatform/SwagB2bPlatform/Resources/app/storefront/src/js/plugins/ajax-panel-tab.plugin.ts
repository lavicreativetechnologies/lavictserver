import { EventInterface, InitOnLoad } from '../decorator';

@EventInterface
@InitOnLoad
export default class AjaxPanelTabPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_NAVIGATION: '.modal--tabs',
        SELECTOR_NAVIGATION_LINK: '.b2b--tab-link',
        SELECTOR_MODAL: '.js--modal',
        CLASS_NAVIGATION_ACTIVE: 'navigation--active',
        CLASS_ACTIVE: 'tab--active',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        const { SELECTOR_NAVIGATION, SELECTOR_NAVIGATION_LINK } = this.options;

        this._addGlobalEventListener(
            'click',
            `${SELECTOR_NAVIGATION} ${SELECTOR_NAVIGATION_LINK}`,
            this.handleClickEvent.bind(this),
        );
    }

    protected handleClickEvent(event: Event): void {
        const { SELECTOR_NAVIGATION, CLASS_ACTIVE } = this.options;
        const eventTarget = <HTMLElement>event.target;
        const lastActive = document.querySelector(`${SELECTOR_NAVIGATION} .${CLASS_ACTIVE}`);

        lastActive.classList.remove(CLASS_ACTIVE);
        eventTarget.classList.add(CLASS_ACTIVE);

        this.hideNavigation();
    }

    protected hideNavigation(): void {
        const { SELECTOR_MODAL, CLASS_NAVIGATION_ACTIVE } = this.options;
        const modal = document.querySelector(SELECTOR_MODAL);

        if (!modal) {
            return;
        }

        modal.classList.remove(CLASS_NAVIGATION_ACTIVE);
    }
}
