import axios, { AxiosResponse } from 'axios';
import { getStorefrontPlugin } from '../utility';
import { B2BEvents } from '../enums';
import { AjaxPanelInterface, EventInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@EventInterface
@InitOnLoad
export default class AjaxPanelFastorderTablePlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_MODULE: '.module-fastorder',
        SELECTOR_INPUT: '.module-fastorder input',
        SELECTOR_INPUT_CONTAINER: '.fast-order-inputs',
        SELECTOR_INPUT_ORDERNUMBER: '.module-fastorder .input-ordernumber',
        SELECTOR_INPUT_QUANTITY: '.module-fastorder .input-quantity',
        SELECTOR_FORM_SUBMIT: '.cart-form, .order-list-form',
        SELECTOR_TABLE: '.table--fastorder',
        SELECTOR_TABLE_LAST_ROW: '.table--fastorder tbody tr:last-of-type',
        SELECTOR_LAST_ROW: 'tbody tr:last-of-type',
        SELECTOR_PRODUCT_DETAIL: '.product-detail-form-container',
        SELECTOR_ORDERLIST_DROPDOWN: '.b2b--orderlist-dropdown',
        SELECTOR_ORDERLIST_FORM: '.order-list-form',
        SELECTOR_ORDERLIST_FORM_SELECT_ACTIVE: '.order-list-form select[name="orderlist"] option:checked',
        SELECTOR_MESSAGE_CONTAINER: '.b2b--message-container',
        SELECTOR_MESSAGE_SUCCESS: '.b2b--message-success',
        SELECTOR_REMOTE_BOX: '[data-id="fast-order-remote-box"]',
        CLASS_TABLE_VIEW: 'table-view',
        CLASS_CART_FORM: 'cart-form',
        CLEAR_MESSAGE_TIMEOUT_MS: 3000,
        CLEAR_MESSAGE_TIMEOUT: 0,
        CLASS_FADE_OPACITY_OUT: 'fadeOpacity--out',
        ANIMATION_DURATION: 1000,
        ROUTE_OFFCANVAS: 'frontend.cart.offcanvas',
        PLUGIN_OFFCANVAS_CART: 'OffCanvasCart',
    };

    private module: HTMLElement;

    public init(): void {
        this.module = document.querySelector(this.options.SELECTOR_MODULE);

        if (!this.module) {
            return;
        }

        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        const {
            SELECTOR_INPUT, SELECTOR_INPUT_ORDERNUMBER, SELECTOR_INPUT_QUANTITY, SELECTOR_FORM_SUBMIT,
        } = this.options;

        this._addGlobalEventListener('input', SELECTOR_INPUT, this.handleInputChange.bind(this));
        this._addGlobalEventListener('change', SELECTOR_INPUT_ORDERNUMBER, this.handleOrderNumberChange.bind(this));
        this._addGlobalEventListener('input', SELECTOR_INPUT_QUANTITY, this.handleQuantityChange.bind(this));
        this._addGlobalEventListener('blur', SELECTOR_INPUT_QUANTITY, this.handleQuantityBlur.bind(this));
        this._addGlobalEventListener('submit', SELECTOR_FORM_SUBMIT, this.handleFormSubmit.bind(this));

        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
        this._subscribe(B2BEvents.FASTORDER_PRODUCT_SUCCESS, this.handleOrderNumberChangeSuccess.bind(this));
        this._subscribe(B2BEvents.UPLOAD_SUCCESS, this.handleUploadSuccess.bind(this));
    }

    protected handlePanelAfterLoadEvent(event: CustomEvent, eventData: AjaxPanelEventData): void {
        const { SELECTOR_TABLE, CLASS_CART_FORM, SELECTOR_INPUT_QUANTITY } = this.options;
        const table: HTMLElement = document.querySelector(SELECTOR_TABLE);
        this.setSubmitState(table);

        const isAddToCartSource = eventData.source.classList.contains(CLASS_CART_FORM);

        if (isAddToCartSource) {
            this.resetOrderListSelect();

            this.resetMessage();
            this.openCartOnSuccess(eventData.panel);
        }

        const quantityInputs:NodeListOf<HTMLInputElement> = document.querySelectorAll(SELECTOR_INPUT_QUANTITY);
        quantityInputs.forEach((input) => {
            input.value = input.value ?? '';
        });
    }

    protected handleInputChange(event: Event): void {
        const { SELECTOR_TABLE, CLASS_TABLE_VIEW } = this.options;
        const eventTarget = <HTMLElement>event.target;
        let table: HTMLElement = eventTarget ? eventTarget.closest(SELECTOR_TABLE) : null;

        if (!table) {
            table = document.querySelector(SELECTOR_TABLE);
        }

        this.setSubmitState(table);

        if (table.classList.contains(CLASS_TABLE_VIEW)) {
            return;
        }

        this.checkProductsForQuantity();
        this.appendNewRow(table);
    }

    protected async handleOrderNumberChange(event: Event): Promise<void> {
        const eventTarget = <HTMLInputElement>event.target;
        const row = eventTarget.closest('tr');
        const headlineColumn = row.querySelector('.col-headline');
        const productUrlElement: HTMLElement = document.querySelector('[data-product-url]');

        if (!productUrlElement) {
            return;
        }

        const response: AxiosResponse = await axios(`${productUrlElement.dataset.productUrl}?orderNumber=${eventTarget.value}`);

        if (response.status >= 400) {
            return;
        }

        this._publish(B2BEvents.FASTORDER_PRODUCT_SUCCESS, {
            data: response.data,
            row,
        });

        headlineColumn.innerHTML = response.data;
    }

    protected handleOrderNumberChangeSuccess(event: Event, { data, row }): void {
        if (data.indexOf('headline-product') === -1) {
            return;
        }

        const quantityInputs = row.querySelectorAll('input');
        const lastQuantityInput = quantityInputs[quantityInputs.length - 1];

        if (parseInt(lastQuantityInput.value, 10) > 0) {
            return;
        }

        lastQuantityInput.focus();
        const productTable: HTMLElement = document.querySelector(this.options.SELECTOR_TABLE);
        this.setSubmitState(productTable);
    }

    protected handleUploadSuccess(): void {
        this.updateRowIndex();
    }

    protected handleQuantityChange(event: Event): void {
        const eventTarget = <HTMLInputElement>event.target;

        if (this.isLastRow(eventTarget)) {
            return;
        }

        document.body.addEventListener('click', () => {
            if (!eventTarget || parseInt(eventTarget.value, 10) > 0) {
                return;
            }

            eventTarget.closest('tr').remove();
        });
    }

    protected handleQuantityBlur(event: Event): void {
        const eventTarget = <HTMLInputElement>event.target;

        if (this.isLastRow(eventTarget) || parseInt(eventTarget.value, 10) > 0) {
            return;
        }

        eventTarget.closest('tr').remove();
    }

    protected handleFormSubmit(event: Event): void {
        const { SELECTOR_REMOTE_BOX, SELECTOR_INPUT_CONTAINER } = this.options;
        const panel: HTMLElement = document.querySelector(SELECTOR_REMOTE_BOX);
        const inputContainer = document.querySelector(SELECTOR_INPUT_CONTAINER);
        const form = <HTMLFormElement>event.target;

        this._breakEventExecution(event);
        this.handleRequest(panel, form, inputContainer);
    }

    protected handleRequest(panel: HTMLElement, form: HTMLFormElement, inputContainer: HTMLElement): void {
        const { SELECTOR_ORDERLIST_FORM_SELECT_ACTIVE } = this.options;
        const selectedOption: HTMLSelectElement = document.querySelector(SELECTOR_ORDERLIST_FORM_SELECT_ACTIVE);
        const clonedForm = <HTMLFormElement>form.cloneNode();

        inputContainer.querySelectorAll('input').forEach((input) => {
            clonedForm.appendChild(input.cloneNode());
        });

        clonedForm.querySelectorAll('input').forEach((input) => {
            this.checkProducts(input, clonedForm);
        });

        if (selectedOption) {
            const orderNumberInput = document.createElement('input');
            orderNumberInput.name = 'orderlist';
            orderNumberInput.value = selectedOption.value;
            clonedForm.appendChild(orderNumberInput);
        }

        const csrfInput: HTMLInputElement = form.querySelector('[name="_csrf_token"]');
        const orderListInput: HTMLInputElement = form.querySelector('[name="orderlist"]');

        if (csrfInput) {
            clonedForm.appendChild(csrfInput);
        }

        if (orderListInput) {
            clonedForm.appendChild(orderListInput);
        }

        if (!this.validateProductList(clonedForm)) {
            return;
        }

        clonedForm.dataset.ajaxPanelTriggerReload = 'fast-order-grid';

        this._publish(B2BEvents.PANEL_AJAX, {
            url: clonedForm.action,
            target: panel,
            source: clonedForm,
        });
    }

    protected checkProducts(input: HTMLInputElement, form: HTMLFormElement): void {
        if (!input.classList.contains('form-control') && !input.classList.contains('input-quantity') && !input.classList.contains('b2b-table-quantity')) {
            return;
        }

        const selectorReference = `input[name="${input.getAttribute('name').replace('quantity', 'referenceNumber')}"]`;

        if (!input.value) {
            const referenceInput = form.querySelector(selectorReference);

            if (referenceInput) {
                referenceInput.remove();
            }

            input.remove();

            return;
        }

        const referenceInput: HTMLInputElement = form.querySelector(selectorReference);

        if (referenceInput.value) {
            return;
        }

        referenceInput.remove();
        input.remove();
    }

    protected validateProductList(form: HTMLFormElement): number {
        const inputs = Array.from(form.querySelectorAll('input'));

        return inputs.reduce((accumulator, currentInput) => accumulator + ((currentInput.name !== 'orderlist' && currentInput.name !== '_csrf_token') ? 1 : 0), 0);
    }

    protected isLastRow(element: HTMLElement): boolean {
        const row = element.closest('tr');
        const lastRow = document.querySelector(this.options.SELECTOR_TABLE_LAST_ROW);

        return row === lastRow;
    }

    protected setSubmitState(table: HTMLElement): void {
        if (!table) {
            return;
        }

        const rows: NodeListOf<HTMLElement> = table.querySelectorAll('tbody tr');
        const btn = document.querySelector('.cart--link');
        const hasValidRows = this.hasValidRows(rows);

        if (btn) {
            btn.toggleAttribute('disabled', !hasValidRows);
        }

        if (document.querySelector(this.options.SELECTOR_PRODUCT_DETAIL)) {
            return;
        }

        const select = this.module.querySelector('.b2b--orderlist-dropdown');

        if (!select) {
            return;
        }

        select.toggleAttribute('disabled', !hasValidRows);
    }

    protected hasValidRows(rows: NodeListOf<HTMLElement>): Boolean {
        let hasValidRows = false;

        rows.forEach((row) => {
            const inputs = Array.from(row.querySelectorAll('input'));
            let validInputs = true;

            inputs.forEach((rowInput) => {
                if (!rowInput.value || parseInt(rowInput.value, 10) <= 0) {
                    validInputs = false;
                }
            });

            if (validInputs) {
                hasValidRows = true;
            }
        });

        return hasValidRows;
    }

    protected checkProductsForQuantity(): boolean {
        let productsCheck = false;

        const inputs: NodeListOf<HTMLInputElement> = document.querySelectorAll(this.options.SELECTOR_INPUT_QUANTITY);

        inputs.forEach((input) => {
            if (parseInt(input.value, 10) > 0) {
                productsCheck = true;
            }
        });

        return productsCheck;
    }

    protected appendNewRow(table: HTMLElement): void {
        const rows = table.querySelectorAll('tr');
        const lastRow = rows[rows.length - 1];
        const inputs = lastRow.querySelectorAll('input');
        const inputProductNumber = inputs[0];
        const inputQuantity = inputs[inputs.length - 1];

        if (!inputProductNumber.value && !inputQuantity.value) {
            return;
        }

        const newRow = <HTMLElement>lastRow.cloneNode(true);
        const newIndex = parseInt(lastRow.dataset.index, 10) + 1;

        const searchResults = newRow.querySelector('.b2b--search-results');

        if (searchResults) {
            searchResults.remove();
        }

        const newRowInputs = newRow.querySelectorAll('input');

        newRowInputs[0].setAttribute('name', `products[${newIndex}][referenceNumber]`);
        newRowInputs[newRowInputs.length - 1].setAttribute('name', `products[${newIndex}][quantity]`);

        const newRowContent = newRow.innerHTML;

        table.insertAdjacentHTML('beforeend', `<tr data-index="${newIndex}">${newRowContent}</tr>`);
    }

    protected openCartOnSuccess(panel: HTMLElement): void {
        const {
            SELECTOR_MESSAGE_CONTAINER, SELECTOR_MESSAGE_SUCCESS, ROUTE_OFFCANVAS, PLUGIN_OFFCANVAS_CART,
        } = this.options;
        const messageContainer = panel.querySelector(SELECTOR_MESSAGE_CONTAINER);

        if (!messageContainer.querySelector(SELECTOR_MESSAGE_SUCCESS)) {
            return;
        }

        const offCanvasCart: any = getStorefrontPlugin(PLUGIN_OFFCANVAS_CART);

        if (!offCanvasCart) {
            return;
        }

        offCanvasCart.openOffCanvas(window.router[ROUTE_OFFCANVAS]);
    }

    protected resetOrderListSelect(): void {
        const select = this.module.querySelector(this.options.SELECTOR_ORDERLIST_DROPDOWN);
        select.setAttribute('selectedIndex', '0');
    }

    protected resetMessage(): void {
        const {
            ANIMATION_DURATION, SELECTOR_MESSAGE_CONTAINER, CLEAR_MESSAGE_TIMEOUT, CLEAR_MESSAGE_TIMEOUT_MS, CLASS_FADE_OPACITY_OUT,
        } = this.options;
        const target: HTMLElement = document.querySelector(SELECTOR_MESSAGE_CONTAINER);
        const animationDelay = CLEAR_MESSAGE_TIMEOUT_MS - ANIMATION_DURATION;
        clearTimeout(CLEAR_MESSAGE_TIMEOUT);

        setTimeout(() => {
            target.style.animationDuration = `${ANIMATION_DURATION}ms`;
            target.classList.add(CLASS_FADE_OPACITY_OUT);
        }, animationDelay);

        this.options.CLEAR_MESSAGE_TIMEOUT = setTimeout(() => {
            if (target && target.parentElement) {
                target.parentElement.removeChild(target);
            }
        }, CLEAR_MESSAGE_TIMEOUT_MS);
    }

    protected updateRowIndex() {
        const table = document.querySelector(this.options.SELECTOR_TABLE);
        const rows: NodeListOf<HTMLElement> = table.querySelectorAll('tbody tr');

        rows.forEach((row, index) => {
            const inputReference = row.querySelector('.input-ordernumber');
            const inputQuantity = row.querySelector('.input-quantity');

            row.dataset.index = index.toString();
            inputReference.setAttribute('name', `products[${index}][referenceNumber]`);
            inputQuantity.setAttribute('name', `products[${index}][quantity]`);
        });
    }
}
