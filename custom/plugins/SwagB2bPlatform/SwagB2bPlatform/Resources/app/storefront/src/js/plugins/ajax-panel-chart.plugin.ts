import ApexCharts, { ApexOptions } from 'apexcharts';
import { parseForm } from '../utility';

export default class AjaxPanelChartPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_CONTAINER: '.b2b-statistics-chart',
        SELECTOR_FORM: '[data-id="statistic-grid"] form',
        DATA_URL: 'chartUrl',
    };

    private container: HTMLDivElement;

    private form: HTMLFormElement;

    private chart: ApexCharts;

    public async init(): Promise<void> {
        this.getElements();

        if (!this.container || !this.form) {
            return;
        }

        this.initChart();
    }

    public update(): void {
        if (!this.chart) {
            return;
        }

        this.getElements();
        this.updateChartData();
    }

    protected getElements(): void {
        const { SELECTOR_CONTAINER, SELECTOR_FORM } = this.options;
        this.container = document.querySelector(SELECTOR_CONTAINER);
        this.form = document.querySelector(SELECTOR_FORM);
    }

    protected async fetchChartData(): Promise<object|null> {
        const chartUrl = this.container.dataset[this.options.DATA_URL] + this.getPayload();
        const response = await fetch(chartUrl, {
            method: this.form.method,
        });

        if (response.status !== 200) {
            return null;
        }

        return response.json();
    }

    protected getPayload(): FormData {
        return <FormData> parseForm(this.form, true);
    }

    protected async initChart(): Promise<void> {
        const chartData = await this.fetchChartData();

        if (!chartData) {
            Promise.resolve();
        }

        const config = <ApexOptions> { ...this.getDefaultOptions(), ...this.processChartData(chartData) };

        this.chart = new ApexCharts(this.container, config);

        return this.chart.render();
    }

    protected async updateChartData(): Promise<void> {
        const chartData = await this.fetchChartData();

        if (!chartData) {
            return Promise.resolve();
        }

        return this.chart.updateOptions(this.processChartData(chartData));
    }

    protected processChartData(responseData): ApexOptions {
        const series = Object.keys(responseData.data).map((key) => ({
            name: key,
            data: responseData.data[key],
        }));

        const xaxis = {
            categories: responseData.labels,
        };

        return {
            series,
            xaxis,
        };
    }

    protected getDefaultOptions(): ApexOptions {
        return {
            chart: {
                height: 350,
                type: 'line',
                fontFamily: 'Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif',
                toolbar: {
                    show: true,
                    tools: {
                        download: false,
                    },
                },
            },
            markers: {
                size: 4,
                strokeWidth: 0,
                hover: {
                    size: 8,
                },
            },
            stroke: {
                width: 2,
            },
            title: {
                margin: 0,
                style: {
                    color: '#52667a',
                    fontSize: '24px',
                },
            },
            tooltip: {
                theme: 'dark',
            },
            xaxis: {
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
                labels: {
                    hideOverlappingLabels: true,
                    style: {
                        colors: '#52667a',
                    },
                },
                tooltip: {
                    enabled: true,
                    offsetY: 10,
                },
            },
            yaxis: {
                labels: {
                    style: {

                    },
                },
            },
            series: [],
        };
    }
}
