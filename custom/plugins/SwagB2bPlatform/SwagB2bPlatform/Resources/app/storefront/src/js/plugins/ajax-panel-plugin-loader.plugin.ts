import { AjaxPanelInterface, InitOnLoad } from '../decorator';
import { IPluginWrapper } from '../interfaces';
import { B2BEvents } from '../enums';

declare const $;

@AjaxPanelInterface
@InitOnLoad
export default class AjaxPanelPluginLoaderPlugin extends window.PluginBaseClass {
    public static options = {
        PLUGIN_TYPES: {
            JQUERY: 'JQUERY',
            PLUGIN_WRAPPER: 'PLUGIN_WRAPPER',
            STOREFRONT: 'STOREFRONT',
        },
    };

    private storefrontPlugins: object;

    public init(): void {
        this.getStorefrontPlugins();
        this.registerEventListeners();
    }

    protected getStorefrontPlugins(): void {
        this.storefrontPlugins = require('./index');
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_BEFORE_LOAD, this.handlePanelBeforeLoadEvent.bind(this));
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelBeforeLoadEvent(event: CustomEvent, eventData: AjaxPanelEventData): void {
        this.eachPluginName(eventData, (pluginType, pluginName, panel) => {
            const { JQUERY, PLUGIN_WRAPPER, STOREFRONT } = this.options.PLUGIN_TYPES;

            switch (pluginType) {
                case JQUERY:
                    this.destroyJQueryPlugin(panel, pluginName);
                    break;
                case PLUGIN_WRAPPER:
                    break;
                case STOREFRONT:
                    break;
                default:
            }
        });
    }

    protected handlePanelAfterLoadEvent(event: CustomEvent, eventData: AjaxPanelEventData): void {
        this.eachPluginName(eventData, (pluginType, pluginName, panel) => {
            const { JQUERY, PLUGIN_WRAPPER, STOREFRONT } = this.options.PLUGIN_TYPES;

            switch (pluginType) {
                case JQUERY:
                    this.initJQueryPlugin(panel, pluginName);
                    break;
                case PLUGIN_WRAPPER:
                    this.initPluginWrapperPlugin(panel, pluginName);
                    break;
                case STOREFRONT:
                    this.initStorefrontPlugin(panel, pluginName);
                    break;
                default:
            }
        });
    }

    protected determinePluginType(panel: HTMLElement, pluginName: string): string {
        const { JQUERY, PLUGIN_WRAPPER, STOREFRONT } = this.options.PLUGIN_TYPES;

        if ($(panel)[pluginName]) {
            return JQUERY;
        } if (this.getPluginWrapper().pluginObjects[pluginName]) {
            return PLUGIN_WRAPPER;
        } if (this.storefrontPlugins[this.formatStorefrontPluginName(pluginName)]) {
            return STOREFRONT;
        }

        console.debug(`Error determining plugin type for: "${pluginName}"!`); // eslint-disable-line no-console

        return null;
    }

    protected initJQueryPlugin(panel: HTMLElement, pluginName: string): void {
        $(panel)[pluginName]();
    }

    protected destroyJQueryPlugin(panel: HTMLElement, pluginName: string): void {
        if (!panel) {
            return;
        }

        const name = `plugin_${pluginName}`;
        const plugin = <any>panel.dataset[name];

        if (!plugin) {
            return;
        }

        plugin.destroy();
        panel.dataset[name] = '';
    }

    protected initPluginWrapperPlugin(panel: HTMLElement, pluginName: string): void {
        const { pluginObjects } = this.getPluginWrapper();
        const plugin = pluginObjects[pluginName];
        const { selector } = plugin;

        const $panel = $(panel);

        plugin.$el = selector ? $(selector) : $panel;
        $panel[pluginName] = plugin.init.bind(pluginObjects[pluginName]);

        $panel[pluginName]();
    }

    protected getPluginWrapper(): IPluginWrapper {
        return window.B2bPluginWrapper;
    }

    protected initStorefrontPlugin(panel: HTMLElement, pluginName: string): void {
        const registryName = this.getRegistryName(pluginName);
        const registeredPlugin = window.PluginManager.getPlugin(registryName);

        const instances = registeredPlugin.get('instances');

        if (instances.length) {
            instances.forEach((instance) => {
                setTimeout(() => {
                    instance.el = panel;
                    instance.update();
                });
            });
        } else {
            window.PluginManager.initializePlugin(registryName, panel);
        }
    }

    protected formatStorefrontPluginName(pluginName: string): string {
        return `${pluginName.charAt(0).toUpperCase() + pluginName.slice(1)}Plugin`;
    }

    protected getRegistryName(pluginName: string): string {
        return `B2B_${this.formatStorefrontPluginName(pluginName)}`;
    }

    protected eachPluginName(eventData: AjaxPanelEventData, callback: Function): void {
        const { panel } = eventData;
        const pluginNames = panel.dataset.plugins;

        if (!pluginNames) {
            return;
        }

        pluginNames.split(',').forEach((pluginName) => {
            const pluginType = this.determinePluginType(panel, pluginName);

            callback(pluginType, pluginName, panel);
        });
    }
}
