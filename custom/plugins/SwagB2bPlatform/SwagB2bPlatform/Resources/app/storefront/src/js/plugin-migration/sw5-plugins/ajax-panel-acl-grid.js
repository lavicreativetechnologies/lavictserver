import { b2bAjaxPanelAclGrid } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-acl-grid.plugin
 */
export default ({
  ...b2bAjaxPanelAclGrid,
  name: 'b2bAjaxPanelAclGrid',
  initOnLoad: true,
  selector: 'body',
});
