import { B2BKeycodes } from '../enums';
import { EventInterface, InitOnLoad } from '../decorator';

@EventInterface
@InitOnLoad
export default class SubmitOnEnterPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_FORM: '[data-submit-enter]',
        SELECTOR_SUBMIT: 'button[type=submit]',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._addGlobalEventListener('keypress', this.options.SELECTOR_FORM, this.handleKeypressEvent.bind(this));
    }

    protected handleKeypressEvent(event: KeyboardEvent, panel: HTMLElement): void {
        if (event.keyCode !== B2BKeycodes.ENTER) {
            return;
        }

        this._breakEventExecution(event);
        const button: HTMLButtonElement = panel.querySelector(this.options.SELECTOR_SUBMIT);

        if (!button) {
            return;
        }

        button.click();
    }
}
