import { b2bContactPasswordActivation } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.6.0 - Will be replaced by src/js/plugins/contact-password-activation.plugin
 */
export default ({
  ...b2bContactPasswordActivation,
  name: 'b2bContactPasswordActivation',
  initOnLoad: false,
});
