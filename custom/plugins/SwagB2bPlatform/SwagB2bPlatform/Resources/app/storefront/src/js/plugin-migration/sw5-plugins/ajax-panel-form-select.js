import { b2bAjaxPanelFormSelect } from '../../jsPluginBaseObjects';
import { subscribe } from '../../utility/index.ts';

const overrides = {
  init() {
    const me = this;

    this.applyDataAttributes();

    subscribe(
      'b2b--ajax-panel_loaded',
      $.proxy(me.registerEvents, me),
    );
  },
};

/**
 * @deprecated tag:v4.6.0 - Will be replaced by src/js/plugins/ajax-panel-form-select.plugin
 */
export default ({
  ...b2bAjaxPanelFormSelect,
  ...overrides,
  name: 'b2bAjaxPanelFormSelect',
  initOnLoad: true,
  selector: 'body',
});
