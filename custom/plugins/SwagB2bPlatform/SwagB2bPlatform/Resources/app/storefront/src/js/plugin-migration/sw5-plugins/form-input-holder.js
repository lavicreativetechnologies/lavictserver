import { b2bFormInputHolder } from '../../jsPluginBaseObjects';

export default ({
  ...b2bFormInputHolder,
  name: 'b2bFormInputHolder',
  initOnLoad: true,
  selector: '*[data-b2b-form-input-holder="true"]',
});
