import { b2bGridComponent } from '../../jsPluginBaseObjects';
import { publish } from '../../utility/index.ts';

const overrides = {
  onDeleteClick(event) {
    const me = this;
    const $target = $(event.currentTarget);
    const $form = $target.closest('form');

    event.preventDefault();

    $target.attr('disabled', 'disabled');

    if ($target.data('confirm')) {
      me.defaults.activeForm = $form;

      $.ajax({
        url: $target.data('confirm-url'),
        type: 'post',
        data: $form.serialize(),
        success(response) {
          $target.prop('disabled', false);

          $.b2bConfirmModal.open(response, {
            confirm() {
              me.onConfirmRemove();
            },
            cancel() {
              $.b2bConfirmModal.close();
            },
          });
        },
        error() {
          $target.prop('disabled', false);
        },
      });
    } else {
      me.removeItemAjax($form);
    }
  },

  removeItemAjax($form) {
    const csrf = $form.data('csrfToken');

    if (csrf) {
      const csrfInput = $form.find('[name="_csrf_token"]');
      if (csrfInput) {
        csrfInput.val(csrf);
      }
    }

    $.ajax({
      url: $form.attr('action'),
      method: $form.attr('method'),
      data: $form.serialize(),
      contentType: 'application/x-www-form-urlencoded',
      success() {
        const $ajaxPanel = $form.closest('.b2b--ajax-panel');
        publish('b2b--ajax-panel_refresh', {
          url: $form.attr('data-reload-url'),
          target: $ajaxPanel[0],
        });
      },
    });
  },
};

export default ({
  ...b2bGridComponent,
  ...overrides,
  name: 'b2bGridComponent',
  initOnLoad: false,
});
