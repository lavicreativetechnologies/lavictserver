import axios from 'axios';
import { EventInterface, InitOnLoad } from '../decorator';
import { B2BKeycodes } from '../enums';

@EventInterface
@InitOnLoad
export default class AjaxPanelProductSearchPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_PRODUCT_SEARCH: '[data-product-search]',
        SELECTOR_RESULTS: '.b2b--search-results',
        SELECTOR_SEARCH_CONTAINER: '.b2b--search-container',
        SELECTOR_QUANTITY: '.b2b--search-quantity',
        CLASS_LOADING_CONTAINER: 'container--element-loader',
        CLASS_LOADING_INDICATOR_ICON: 'icon--loading-indicator',
        CLASS_IS_ACTIVE: 'is--active',
        CLASS_SEARCH_RESULTS: 'b2b--search-results',
        DATA_LOADING: 'loading',
        DELAY_SEARCH_MS: 300,
    };

    private resultsActive: boolean = false;

    private isLoading: boolean = false;

    private throttleTimeout: ReturnType<typeof setTimeout> = null;

    public init(): void {
        this.registerEventListener();
    }

    protected registerEventListener(): void {
        const { SELECTOR_PRODUCT_SEARCH, SELECTOR_RESULTS } = this.options;

        this._addGlobalEventListener('keydown', SELECTOR_PRODUCT_SEARCH, this.handleKeyDown.bind(this));
        this._addGlobalEventListener('keyup', SELECTOR_PRODUCT_SEARCH, this.handleKeyUp.bind(this));
        this._addGlobalEventListener('focus', SELECTOR_PRODUCT_SEARCH, this.handleFocus.bind(this));
        this._addGlobalEventListener('click', `${SELECTOR_RESULTS} li`, this.handleClickSelectElement.bind(this));
        this._addGlobalEventListener('click', 'button[type="submit"]', this.handleSubmit.bind(this));
        this._addGlobalEventListener('click', 'body', this.handleBodyClick.bind(this));
    }

    protected handleKeyDown(event: KeyboardEvent): void {
        const { keyCode } = event;
        if (keyCode === B2BKeycodes.TAB) {
            this.hideResults();
        } else if (keyCode === B2BKeycodes.ENTER) {
            this.handleSelectElement(event);
        }
    }

    protected handleKeyUp(event: KeyboardEvent): void {
        const input = <HTMLInputElement> event.currentTarget;
        const term = input.value;
        const { keyCode } = event;

        this._breakEventExecution(event);

        if (!term.length) {
            this.hideResults();
        }

        if (keyCode === B2BKeycodes.LEFT || keyCode === B2BKeycodes.RIGHT) {
            return;
        } else if (keyCode === B2BKeycodes.DOWN && this.resultsActive) {
            this.focusNextElement(event);

            return;
        } else if (keyCode === B2BKeycodes.UP && this.resultsActive) {
            this.focusPreviousElement(event);

            return;
        }

        this.searchRequest(term, input);
    }

    protected handleFocus(event: Event): void {
        const searchInput = <HTMLInputElement> event.currentTarget;
        const term = searchInput.value;

        this.searchRequest(term, searchInput);
    }

    protected handleSelectElement(event: Event): void {
        const { SELECTOR_SEARCH_CONTAINER, CLASS_IS_ACTIVE } = this.options;
        const currentTarget = <HTMLInputElement> event.currentTarget;
        const searchContainer = currentTarget.closest(SELECTOR_SEARCH_CONTAINER);
        const currentElement = searchContainer.querySelector(`.${CLASS_IS_ACTIVE}`);

        if (!currentElement || !this.resultsActive) {
            return;
        }

        this._breakEventExecution(event);

        currentTarget.value = currentElement.querySelector('span').innerText;

        this._dispatchEvent('change', currentTarget);
        this.updateQuantityInput(searchContainer, currentElement);
        this.hideResults();
    }

    protected handleClickSelectElement(event: Event): void {
        const { SELECTOR_SEARCH_CONTAINER } = this.options;
        const currentTarget = <HTMLInputElement> event.currentTarget;

        if (!currentTarget) {
            return;
        }

        const searchContainer = currentTarget.closest(SELECTOR_SEARCH_CONTAINER);
        const input = searchContainer.querySelector('input');
        input.value = currentTarget.querySelector('span').innerText;
        this._dispatchEvent('change', input);

        this.updateQuantityInput(searchContainer, currentTarget);
        this.hideResults();
    }

    protected handleSubmit(event: Event): void {
        if (!this.resultsActive) {
            return;
        }

        this._breakEventExecution(event);
    }

    protected handleBodyClick(event: Event): void {
        if (!this.resultsActive) {
            return;
        }

        if (!this.clickedSearchContainer(this.getEventPath(event))) {
            this.hideResults();
        }
    }

    protected clickedSearchContainer(eventPath: Element[]): Boolean {
        const { SELECTOR_SEARCH_CONTAINER } = this.options;

        return eventPath.reduce((isSearchContainer, element) => {
            if (element.matches && element.matches(SELECTOR_SEARCH_CONTAINER)) {
                isSearchContainer = true;
            }

            return isSearchContainer;
        }, false);
    }

    protected getEventPath(event: Event): Element[] {
        const path = <Element[]> event.composedPath();

        return this.removeUnrelatedElements(path);
    }

    protected removeUnrelatedElements(path: EventTarget[]): Element[] {
        path.length -= 2;

        return path as Element[];
    }

    protected enableLoading(input: HTMLInputElement): void {
        const {
            SELECTOR_SEARCH_CONTAINER, CLASS_LOADING_CONTAINER, CLASS_LOADING_INDICATOR_ICON, DATA_LOADING,
        } = this.options;

        if (input.dataset[DATA_LOADING]) {
            return;
        }

        const searchContainer = input.closest(SELECTOR_SEARCH_CONTAINER);
        const loadingContainer = searchContainer.querySelector(`.${CLASS_LOADING_CONTAINER}`);

        if (loadingContainer) {
            loadingContainer.parentElement.removeChild(loadingContainer);
        }
        input.insertAdjacentHTML('afterend', `<div class="${CLASS_LOADING_CONTAINER}"><i class="${CLASS_LOADING_INDICATOR_ICON}"></i></div>`);

        input.dataset[DATA_LOADING] = 'true';
        this.isLoading = true;
    }

    protected disableLoading(input: HTMLInputElement): void {
        const {
            SELECTOR_SEARCH_CONTAINER, CLASS_LOADING_CONTAINER, DATA_LOADING,
        } = this.options;

        if (!input.dataset[DATA_LOADING]) {
            return;
        }

        const searchContainer = input.closest(SELECTOR_SEARCH_CONTAINER);
        const loadingContainer = searchContainer.querySelector(`.${CLASS_LOADING_CONTAINER}`);

        searchContainer.removeChild(loadingContainer);

        delete input.dataset[DATA_LOADING];
        this.isLoading = false;
    }

    protected hideResults(): void {
        this.resultsActive = false;
        const searchResults = document.querySelector(this.options.SELECTOR_RESULTS);

        if (searchResults) {
            searchResults.parentElement.removeChild(searchResults);
        }
    }

    protected showResults(input: HTMLInputElement, template: string): void {
        if (this.resultsActive) {
            this.updateResults(input, template);

            return;
        }

        this.resultsActive = true;
        input.insertAdjacentHTML('afterend', `<div class="b2b--search-results">${template}</div>`);
    }

    protected updateResults(input: HTMLInputElement, template: string): void {
        const { SELECTOR_SEARCH_CONTAINER, SELECTOR_RESULTS } = this.options;
        const searchContainer = input.closest(SELECTOR_SEARCH_CONTAINER);
        const searchResults = searchContainer.querySelector(SELECTOR_RESULTS);

        searchResults.innerHTML = template;
    }

    protected focusNextElement(event: Event): void {
        const { SELECTOR_SEARCH_CONTAINER, SELECTOR_RESULTS, CLASS_IS_ACTIVE } = this.options;
        const currentTarget = <HTMLInputElement> event.currentTarget;
        const searchContainer = currentTarget.closest(SELECTOR_SEARCH_CONTAINER);
        const resultsContainer = searchContainer.querySelector(SELECTOR_RESULTS);

        let currentElement = searchContainer.querySelector(`.${CLASS_IS_ACTIVE}`);

        if (!currentElement) {
            resultsContainer.querySelector('li').classList.add(CLASS_IS_ACTIVE);

            return;
        }

        currentElement = resultsContainer.querySelector(`.${CLASS_IS_ACTIVE}`);
        currentElement.classList.remove(CLASS_IS_ACTIVE);

        const nextItem = currentElement.nextElementSibling;

        if (!nextItem) {
            return;
        }

        nextItem.classList.add(CLASS_IS_ACTIVE);
        resultsContainer.scrollTo({ x: 0, y: nextItem.offsetTop });
    }

    protected focusPreviousElement(event: Event): void {
        const { SELECTOR_SEARCH_CONTAINER, SELECTOR_RESULTS, CLASS_IS_ACTIVE } = this.options;
        const currentTarget = <HTMLInputElement> event.currentTarget;
        const searchContainer = currentTarget.closest(SELECTOR_SEARCH_CONTAINER);
        const resultsContainer = searchContainer.querySelector(SELECTOR_RESULTS);

        let currentElement = searchContainer.querySelector(`.${CLASS_IS_ACTIVE}`);

        if (!currentElement) {
            const allItems = searchContainer.querySelectorAll('li');
            allItems[allItems.length - 1].classList.add(CLASS_IS_ACTIVE);

            return;
        }

        currentElement = resultsContainer.querySelector(`.${CLASS_IS_ACTIVE}`);
        currentElement.classList.remove(CLASS_IS_ACTIVE);

        const prevItem = currentElement.previousElementSibling;

        if (!prevItem) {
            return;
        }

        prevItem.classList.add(CLASS_IS_ACTIVE);
        resultsContainer.scrollTo({ x: 0, y: prevItem.offsetTop });
    }

    protected searchRequest(term: string, input: HTMLInputElement): void {
        const searchUrl = input.dataset.productSearch;

        this.resetQuantityInput(input);

        if (!term.length) {
            this.disableLoading(input);
            this.hideResults();
        } else {
            this.enableLoading(input);
            clearTimeout(this.throttleTimeout);

            this.throttleTimeout = setTimeout(() => {
                axios({
                    url: searchUrl,
                    method: 'GET',
                    params: {
                        term,
                    },
                    transformResponse: (response: string) => {
                        this.showResults(input, response);
                    },
                });
            }, this.options.DELAY_SEARCH_MS);
        }
    }

    protected updateQuantityInput(container: HTMLElement, current: HTMLElement): void {
        const { SELECTOR_QUANTITY } = this.options;
        const row = container.closest('tr');

        if (!row) {
            return;
        }

        const quantityInput = row.querySelector(SELECTOR_QUANTITY);

        if (current.hasAttribute('max')) {
            quantityInput.setAttribute('max', current.getAttribute('max'));
        }

        if (current.hasAttribute('min')) {
            quantityInput.setAttribute('min', current.getAttribute('min'));
        }

        if (current.hasAttribute('step')) {
            quantityInput.setAttribute('step', current.getAttribute('step'));
        }
    }

    protected resetQuantityInput(input: HTMLInputElement): void {
        const { SELECTOR_SEARCH_CONTAINER, SELECTOR_QUANTITY } = this.options;
        const searchContainer = input.closest(SELECTOR_SEARCH_CONTAINER);
        const row = searchContainer.closest('tr');

        if (!row) {
            return;
        }

        const quantityInput = row.querySelector(SELECTOR_QUANTITY);

        if (!quantityInput) {
            return;
        }

        quantityInput.setAttribute('min', '1');
        quantityInput.removeAttribute('max');
        quantityInput.removeAttribute('step');
    }
}
