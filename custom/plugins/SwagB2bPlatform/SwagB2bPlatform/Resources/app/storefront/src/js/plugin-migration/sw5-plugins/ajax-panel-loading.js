import { b2bAjaxPanelLoading } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.6.0 - Will be replaced by src/js/plugins/ajax-panel-loading.plugin
 */
export default ({
  ...b2bAjaxPanelLoading,
  name: 'b2bAjaxPanelLoading',
  initOnLoad: true,
  selector: 'body',
});
