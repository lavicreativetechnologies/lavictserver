function removeUnrelatedElements(path: EventTarget[]): Element[] {
    path.length -= 3;

    return path as Element[];
}

function getEventTarget(event: CustomEvent, selector: string): Document|Element {
    if (selector === 'document') {
        return document;
    }

    const path = removeUnrelatedElements(event.composedPath());

    let currentTarget = path.shift();

    while (currentTarget) {
        if (currentTarget.matches(selector)) {
            return currentTarget;
        }

        currentTarget = path.shift();
    }

    return null;
}

function setEventTarget(event: Event, target: Document|Element): Event {
    Object.defineProperty(event, 'target', {
        writable: true,
        value: target,
    });

    Object.defineProperty(event, 'currentTarget', {
        writable: true,
        value: target,
    });

    return event;
}

export default function(eventName: string, selector: string, callback: Function): EventListener {
    const callbackFn = (event) => {
        const target = getEventTarget(event, selector);

        if (!target) {
            return;
        }

        setEventTarget(event, target);

        callback(event);
    };

    document.addEventListener(eventName, callbackFn, false);

    return callbackFn;
}
