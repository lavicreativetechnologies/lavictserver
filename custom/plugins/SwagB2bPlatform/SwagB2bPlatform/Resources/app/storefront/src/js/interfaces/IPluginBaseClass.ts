export interface IPluginBaseClass {
    new (el: HTMLElement, options?: object, pluginName?: string | boolean);
    $emitter: {
        publish: (eventName: string, detail?: any) => void;
        subscribe: (eventName: string, callback: () => void, opts?: object) => void;
        unsubscribe: (eventName: string) => void;
    };
    options: any;
    el: HTMLElement;
}

export interface IPluginBaseClassConstructor {
    new (el: HTMLElement, options?: object, pluginName?: string | boolean): IPluginBaseClass;
}
