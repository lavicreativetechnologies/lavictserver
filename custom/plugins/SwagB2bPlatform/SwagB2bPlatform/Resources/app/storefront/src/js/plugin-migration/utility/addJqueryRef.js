import getPluginWrapper from './getPluginWrapper';

export default function (pluginName) {
  const jQueryRef = {};

  jQueryRef[pluginName] = getPluginWrapper().pluginObjects[pluginName];
  jQuery.extend(jQuery, jQueryRef);
}
