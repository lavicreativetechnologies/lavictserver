/* eslint-disable */

export default class NativeEventEmitter {
    constructor(el = document) {
        this._el = el;
        el.$emitter = this;
        this._listeners = [];
    }

    publish(eventName, detail = {}) {
        const event = new CustomEvent(eventName, {
            detail,
        });

        this.el.dispatchEvent(event);
    }

    subscribe(eventName, callback, opts = {}) {
        const emitter = this;
        const splitEventName = eventName.split('.');
        let cb = opts.scope ? callback.bind(opts.scope) : callback;

        if (opts.once && opts.once === true) {
            const onceCallback = cb;
            cb = function onceListener(event) {
                emitter.unsubscribe(eventName);
                onceCallback(event);
            };
        }

        this.el.addEventListener(splitEventName[0], cb);

        this.listeners.push({
            splitEventName,
            opts,
            cb,
        });

        return true;
    }

    unsubscribe(eventName) {
        const splitEventName = eventName.split('.');
        this.listeners = this.listeners.reduce((accumulator, listener) => {
            const foundEvent = listener.splitEventName.sort()
                .toString() === splitEventName.sort().toString();

            if (foundEvent) {
                this.el.removeEventListener(listener.splitEventName[0], listener.cb);

                return accumulator;
            }

            accumulator.push(listener);

            return accumulator;
        }, []);

        return true;
    }

    reset() {
        this.listeners.forEach((listener) => {
            this.el.removeEventListener(listener.splitEventName[0], listener.cb);
        });

        this.listeners = [];

        return true;
    }

    get el() {
        return this._el;
    }

    set el(value) {
        this._el = value;
    }

    get listeners() {
        return this._listeners;
    }

    set listeners(value) {
        this._listeners = value;
    }
}

/* eslint-enable */
