function subscribe(eventType: string, callback: (event: CustomEvent, eventData?: AjaxPanelEventData | object) => any): void {
    document.addEventListener(eventType, (event: CustomEvent) => {
        if (event.detail && event.detail.panel) {
            Object.defineProperty(event, 'target', {
                value: event.detail.panel,
                writable: false,
            });
        }

        callback(event, event.detail);
    });
}

function publish(event: string | CustomEvent, payload?: AjaxPanelEventData | object): CustomEvent {
    let ajaxPanelEvent = <CustomEvent>event;

    if (typeof event === 'string') {
        ajaxPanelEvent = new CustomEvent(event, { detail: payload });
    }

    document.dispatchEvent(ajaxPanelEvent);

    return ajaxPanelEvent;
}

export {
    subscribe,
    publish,
};
