import { b2bAjaxPanelFormDisable } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-form-disable.plugin
 */
export default ({
  ...b2bAjaxPanelFormDisable,
  name: 'b2bAjaxPanelFormDisable',
  initOnLoad: true,
  selector: 'body',
});
