/* eslint-disable */
// @ts-nocheck

import NativeEventEmitter from './emitter.helper';
import DomAccess from './dom-access.helper';
import StringHelper from './string.helper';
import deepmerge from 'deepmerge';

export default class PluginBase {

    constructor(el, options = {}, pluginName = false) {
        if (!DomAccess.isNode(el)) {
            throw new Error('There is no valid element given.');
        }

        this.el = el;
        this.$emitter = new NativeEventEmitter(this.el);
        this._pluginName = this._getPluginName(pluginName);
        this.options = this._mergeOptions(options);
        this._initialized = false;

        this._registerInstance();
        this._init();
    }

    init() {
        throw new Error(`The "init" method for the plugin "${this._pluginName}" is not defined.`);
    }

    update() {

    }

    _init() {
        if (this._initialized) {
            return;
        }

        this.init();
        this._initialized = true;
    }

    _update() {
        if (!this._initialized) {
            return;
        }

        this.update();
    }

    _mergeOptions(options) {
        const dashedPluginName = StringHelper.toDashCase(this._pluginName);
        const dataAttributeConfig = DomAccess.getDataAttribute(this.el, `data-${dashedPluginName}-config`, false);
        const dataAttributeOptions = DomAccess.getAttribute(this.el, `data-${dashedPluginName}-options`, false);

        const merge = [
            this.constructor.options,
            this.options,
            options,
        ];

        if (dataAttributeConfig) {
            merge.push(window.PluginConfigManager.get(this._pluginName, dataAttributeConfig));
        }
        try {
            if (dataAttributeOptions) {
                merge.push(JSON.parse(dataAttributeOptions));
            }
        } catch (e) {
            console.error(this.el);
            throw new Error(
                `The data attribute "data-${dashedPluginName}-options" could not be parsed to json: ${e.message}`,
            );
        }

        return deepmerge.all(merge.map((config) => config || {}));
    }

    _registerInstance() {
        const elementPluginInstances = PluginManager.getPluginInstancesFromElement(this.el);
        elementPluginInstances.set(this._pluginName, this);

        const plugin = PluginManager.getPlugin(this._pluginName, false);
        plugin.get('instances').push(this);
    }

    _getPluginName(pluginName) {
        if (!pluginName) {
            pluginName = this.constructor.name;
        }

        return pluginName;
    }
}

/* eslint-enable */
