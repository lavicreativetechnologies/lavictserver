/* eslint-disable */

import StringHelper from './string.helper';

export default class DomAccess {
    static isNode(element) {
        if (typeof element !== 'object' || element === null) {
            return false;
        }

        if (element === document || element === window) {
            return true;
        }

        return element instanceof Node;
    }

    static hasAttribute(element, attribute) {
        if (!DomAccess.isNode(element)) {
            throw new Error('The element must be a valid HTML Node!');
        }

        if (typeof element.hasAttribute !== 'function') {
            return false;
        }

        return element.hasAttribute(attribute);
    }

    static getAttribute(element, attribute, strict = true) {
        if (strict && DomAccess.hasAttribute(element, attribute) === false) {
            throw new Error(`The required property "${attribute}" does not exist!`);
        }

        if (typeof element.getAttribute !== 'function') {
            if (strict) {
                throw new Error('This node doesn\'t support the getAttribute function!');
            }

            return undefined;
        }

        return element.getAttribute(attribute);
    }

    static getDataAttribute(element, key, strict = true) {
        const keyWithoutData = key.replace(/^data(|-)/, '');
        const parsedKey = StringHelper.toLowerCamelCase(keyWithoutData, '-');
        if (!DomAccess.isNode(element)) {
            if (strict) {
                throw new Error('The passed node is not a valid HTML Node!');
            }

            return undefined;
        }

        if (typeof element.dataset === 'undefined') {
            if (strict) {
                throw new Error('This node doesn\'t support the dataset attribute!');
            }

            return undefined;
        }

        const attribute = element.dataset[parsedKey];

        if (typeof attribute === 'undefined') {
            if (strict) {
                throw new Error(`The required data attribute "${key}" does not exist on ${element}!`);
            }

            return attribute;
        }

        return StringHelper.parsePrimitive(attribute);
    }

    static querySelector(parentNode, selector, strict = true) {
        if (strict && !DomAccess.isNode(parentNode)) {
            throw new Error('The parent node is not a valid HTML Node!');
        }

        const element = parentNode.querySelector(selector) || false;

        if (strict && element === false) {
            throw new Error(`The required element "${selector}" does not exist in parent node!`);
        }

        return element;
    }

    static querySelectorAll(parentNode, selector, strict = true) {
        if (strict && !DomAccess.isNode(parentNode)) {
            throw new Error('The parent node is not a valid HTML Node!');
        }

        let elements = parentNode.querySelectorAll(selector);
        if (elements.length === 0) {
            elements = false;
        }

        if (strict && elements === false) {
            throw new Error(`At least one item of "${selector}" must exist in parent node!`);
        }

        return elements;
    }
}

/* eslint-enable */
