import { b2bImageZoom } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be removed
 */
export default ({
  ...b2bImageZoom,
  name: 'b2bImageZoom',
  initOnLoad: true,
  selector: '.js--img-zoom--container',
});
