export default function(event: Event): void {
    event.preventDefault();

    Object.defineProperty(event, 'defaultPrevented', {
        value: true,
        writable: false,
    });
}
