import animate from './animate';
import b2bFormInputHolder from './b2bFormInputHolder';
import { fadeIn, fadeOut } from './fade';
import flatpickr from './flatpickr';
import slideDown from './slideDown';
import slideUp from './slideUp';

/**
 * jQuery.stop() polyfill
 * @deprecated
 */
function stop() {
  return this;
}

/**
 * jQuery.finish() polyfill
 * @deprecated Since running CSS3 transitions and animations can't be 'finished'
 *             using JavaScript and JavaScript animations won't be implemented
 */
function finish() {
  return this;
}

export {
  animate,
  b2bFormInputHolder,
  fadeOut,
  fadeIn,
  finish,
  flatpickr,
  slideDown,
  stop,
  slideUp,
};
