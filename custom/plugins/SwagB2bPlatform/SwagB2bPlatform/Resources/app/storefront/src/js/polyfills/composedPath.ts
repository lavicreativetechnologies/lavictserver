(function(_event, _document, _window) {
    if (_event.composedPath) {
        return;
    }

    _event.composedPath = function() {
        if (this.path) {
            return this.path;
        }

        let { target } = this;

        this.path = [];

        while (target.parentNode !== null) {
            this.path.push(target);
            target = target.parentNode;
        }

        this.path.push(_document, _window);

        return this.path;
    };
}(Event.prototype, document, window));
