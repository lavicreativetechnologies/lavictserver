import axios, { AxiosResponse, Method } from 'axios';
import { EventInterface } from '../decorator';
import { parseForm } from '../utility';

@EventInterface
export default class AssignmentGridPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_AJAX_PANEL: '.b2b--ajax-panel',
        SELECTOR_GRID_FORM: 'form.b2b--assignment-form',
        SELECTOR_GRID: '.b2b--assignment-grid',
        SELECTOR_ALLOW_INPUT: '.assign--allow',
        SELECTOR_GRANT_INPUT: '.assign--grantable',
        SELECTOR_ROW: '.panel--tr',
        SELECTOR_TREE_ITEM: '.is--b2b-tree li',
        CLASS_ERROR: 'grid--errors',
        CLASS_ROW_FORM: 'b2b--assignment-row-form',
        EVENT_TREE_TOGGLE: 'tree_toggle_menu',
        DATA_PREVIOUS_STATE: 'previousState',
        DATA_FORM_TARGET: 'target',
    };

    public init(): void {
        this.applyEventListeners();
        this.renderGrid();
    }

    public update(): void {
        this.init();
    }

    protected applyEventListeners(): void {
        this._removeGlobalEventListeners();
        const { SELECTOR_GRID_FORM, EVENT_TREE_TOGGLE } = this.options;

        this._addGlobalEventListener(EVENT_TREE_TOGGLE, 'document', this.renderGrid.bind(this));
        this._addGlobalEventListener('change', `${SELECTOR_GRID_FORM} input`, this.handleChangeEvent.bind(this));
        const forms: NodeListOf<HTMLFormElement> = document.querySelectorAll(SELECTOR_GRID_FORM);

        forms.forEach((form) => {
            form.addEventListener('submit', this.handleFormSubmit.bind(this));
        });
    }

    protected handleChangeEvent(): void {
        this.renderGrid();
    }

    protected renderGrid(): void {
        const { SELECTOR_GRID, SELECTOR_ROW, SELECTOR_TREE_ITEM } = this.options;

        const permissionRows: NodeListOf<HTMLElement> = document.querySelectorAll(`${SELECTOR_GRID} ${SELECTOR_ROW}`);
        this.renderPermissionRows(permissionRows);

        if (permissionRows.length) {
            return;
        }

        const allocationRows: NodeListOf<HTMLElement> = document.querySelectorAll(`${SELECTOR_ROW}, ${SELECTOR_TREE_ITEM}`);
        this.renderAllocationRows(allocationRows);
    }

    protected renderPermissionRows(rows: NodeListOf<HTMLElement>): void {
        rows.forEach((row) => {
            const forms = Array.from(row.querySelectorAll('form'));

            if (!forms.length) {
                return;
            }

            this.updateCheckboxStates(forms);
        });
    }

    protected renderAllocationRows(rows: NodeListOf<HTMLElement>): void {
        rows.forEach((row) => {
            const checkboxAllow: HTMLInputElement = row.querySelector(this.options.SELECTOR_ALLOW_INPUT);

            if (!checkboxAllow) {
                return;
            }

            this.updateDisabledStates([checkboxAllow]);
        });
    }

    protected async handleFormSubmit(event: Event): Promise<void> {
        this._breakEventExecution(event);
        const form = <HTMLFormElement>event.target;

        const response: AxiosResponse = await axios(form.action, {
            method: <Method>form.method,
            data: parseForm(form),
        });

        if (response.status !== 200) {
            return;
        }

        this.processResponse(response, form);
    }

    protected processResponse(response: AxiosResponse, form: HTMLFormElement): void {
        const { data } = response;

        if (!data) {
            return;
        }

        if (data.routes) {
            this.updateRoutes(data.routes);
        } else if (typeof data === 'string') {
            this.updateFormContent(form, data);
        }
    }

    protected updateRoutes(routes: string[]): void {
        routes.forEach((route) => {
            const checkbox: HTMLInputElement = document.querySelector(`#${route}`);

            this.updateCheckbox(checkbox, true);
        });
    }

    protected updateFormContent(form: HTMLFormElement, content: string): void {
        if (content.indexOf('b2b--ajax-panel') === -1) {
            const ajaxPanel = form.closest(this.options.SELECTOR_AJAX_PANEL);

            if (ajaxPanel) {
                ajaxPanel.innerHTML = content;
            }
        }

        this.update();
    }

    protected updateCheckboxStates(forms: HTMLFormElement[]): void {
        const { SELECTOR_ALLOW_INPUT, SELECTOR_GRANT_INPUT } = this.options;
        const formAll: HTMLFormElement = forms.shift();
        const allowAllCheckbox: HTMLInputElement = formAll.querySelector(SELECTOR_ALLOW_INPUT);
        const grantAllCheckbox: HTMLInputElement = formAll.querySelector(SELECTOR_GRANT_INPUT);
        const allowCheckboxes: HTMLInputElement[] = [];
        const grantCheckboxes: HTMLInputElement[] = [];

        forms.forEach((form) => {
            allowCheckboxes.push(form.querySelector(SELECTOR_ALLOW_INPUT));
            grantCheckboxes.push(form.querySelector(SELECTOR_GRANT_INPUT));
        });

        this.updateCheckbox(allowAllCheckbox, this.allChecked(allowCheckboxes));
        this.updateCheckbox(grantAllCheckbox, this.allChecked(grantCheckboxes));

        this.updateDisabledStates([allowAllCheckbox, ...allowCheckboxes]);
    }

    protected allChecked(checkboxes: HTMLInputElement[]): boolean {
        return checkboxes.every((checkbox) => !!checkbox && !!checkbox.checked);
    }

    protected updateDisabledStates(checkboxes: HTMLInputElement[]): void {
        checkboxes.forEach((checkbox) => {
            if (!checkbox) {
                return;
            }

            const grantCheckbox: HTMLInputElement = checkbox.closest('form').querySelector(this.options.SELECTOR_GRANT_INPUT);

            if (!grantCheckbox) {
                return;
            }

            const shouldDisable = !checkbox.checked;

            if (shouldDisable) {
                this.updateCheckbox(grantCheckbox, false);
            }

            grantCheckbox.toggleAttribute('disabled', shouldDisable);
        });
    }

    protected updateCheckbox(checkbox: HTMLInputElement, checked: boolean): void {
        checkbox.checked = checked;
    }
}
