import getPluginWrapper from '../getPluginWrapper';

export default function _destroy() {
  const me = this;
  const name = me.getName();

  me._events.forEach((obj) => {
    if (typeof obj === 'object') {
      obj.el.off(obj.event);
    }
  });
  Object.keys(me.opts).forEach((key) => {
    delete me.opts[key];
  });

  me.$el.removeData(`plugin_${name}`);
  $.publish(`plugin/${name}/onDestroy`, [me]);
  getPluginWrapper().initialized[name] = false;

  return me;
}
