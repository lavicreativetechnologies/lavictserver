import * as Prototype from './prototype-mock';

export default function (name, pluginObj, _options = {}) {
  const { defaults = {}, selector = '', options = {} } = pluginObj;

  const _env = {
    _name: name,
    $el: $(selector),
    opts: { ...defaults, ...options, ..._options },
    eventSuffix: `.${name}`,
    _events: [],
  };

  return { ...Prototype, ..._env, ...pluginObj };
}
