/**
 * This plugin duplicates elements on button click, clears the input elements values and append them to a specific container
 */
export default {

  defaults: {
    containerSelector: '.duplicate-item--container',
    itemSelector: '.duplicate-item--item',
    inputSelector: '.duplicate-item--input',
    buttonSelector: '.duplicate-item--button',
  },

  init() {
    const me = this;

    me._on(me.defaults.buttonSelector, 'click', $.proxy(me.onButtonClick, me));
  },

  onButtonClick() {
    const me = this;

    const $item = $(me.defaults.itemSelector);
    const $clonedItem = $item.clone();

    $item.removeClass(me.defaults.itemSelector.substring(1));
    $item.find(me.defaults.itemSelector.substring(1))
      .removeClass(me.defaults.inputSelector.substring(1));

    $clonedItem.find(me.defaults.inputSelector)
      .val('');

    $(me.defaults.containerSelector).append($clonedItem);
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
