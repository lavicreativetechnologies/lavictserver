import { B2BRegex } from '../enums';

function getUrlParams(): object {
    const regex = new RegExp(B2BRegex.URL_PARAMETERS, 'gi');
    const params = {};

    // @ts-ignore
    window.location.href.replace(regex, (matched, key, value): void => {
        params[key] = value;
    });

    return params;
}

export default function(param: string, defaultValue?: string): string {
    if (window.location.href.includes(param)) {
        return getUrlParams()[param];
    }

    return defaultValue || null;
}
