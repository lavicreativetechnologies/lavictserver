/* global StateManager */

import { b2bModal } from '../../jsPluginBaseObjects';

const _close = b2bModal.close;

const overrides = {
  defaults: {
    ...b2bModal.defaults,
    additionalClass: 'js--modal-animated',
  },

  init(content, options) {
    this.open(content, options);
  },

  initModalBox() {
    const me = this;

    me._$modalBox = $('<div>', {
      class: 'js--modal',
    });

    me._$header = $('<div>', {
      class: 'header',
    }).appendTo(me._$modalBox);

    me._$title = $('<div>', {
      class: 'title',
    }).appendTo(me._$header);

    me._$content = $('<div>', {
      class: 'content',
    }).appendTo(me._$modalBox);

    me._$closeButton = $('<div>', {
      class: 'modal--close',
      text: '✖',
    }).appendTo(me._$modalBox);

    me._$mobileNav = $('<span class="b2b--modal-navigation--toggle"><span></span></span>', {
    }).appendTo(me._$modalBox);

    $('body').append(me._$modalBox);

    $.publish('plugin/swModal/onInit', [me]);
  },
  toggleMobileNavigation() {
    const me = this;
    const activeClass = 'navigation--active';
    me._$modalBox.toggleClass(activeClass);
  },
  registerEvents() {
    const me = this;
    const $window = $(window);

    me._$closeButton.on('click', $.proxy(me.close, me));

    me._$mobileNav.on('click', $.proxy(me.toggleMobileNavigation, me));

    $window.on('modal', $.proxy(me.onKeyDown, me));
    StateManager.on('resize', me.onWindowResize, me);

    StateManager.registerListener({
      state: 'xs',
      enter() {
        me._$modalBox.addClass('is--fullscreen');
      },
      exit() {
        me._$modalBox.removeClass('is--fullscreen');
      },
    });

    $.publish('plugin/swModal/onRegisterEvents', [me]);
  },
  close() {
    _close.bind(this)();
    $.b2bConfirmModal.close();

    setTimeout(() => {
      const ajaxPanelModal = document.querySelector('.js--modal');

      ajaxPanelModal.classList.remove('is--small');
    }, 300);
  },
};

export default ({
  ...b2bModal,
  ...overrides,
  name: 'b2bModal',
  alias: 'modal',
  initOnLoad: false,
});
