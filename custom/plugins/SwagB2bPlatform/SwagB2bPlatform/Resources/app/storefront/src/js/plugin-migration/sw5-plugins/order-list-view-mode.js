import { getUrlParameter, setUrlParameter, mutationHelper } from '../../utility/index.ts';
import { getPluginWrapper } from '../utility';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/order-list-view-mode.plugin
 */
export default ({
  name: 'b2bOrderListViewMode',
  selector: '.js--b2b-listingview-mode',
  initOnLoad: true,
  defaults: {
    selectorMutationTarget: '.cms-element-product-listing',
    modeListing: 'listing',
    modeTable: 'table',
    urlParamKey: 'b2bListingView',
  },
  init() {
    const mutationTarget = document.querySelector(this.defaults.selectorMutationTarget);
    let targetToggle = this.getElement()[0];

    this.addEventListener(targetToggle);

    if (mutationTarget) {
      mutationHelper(mutationTarget, () => {
        targetToggle = document.querySelector(this.selector);

        getPluginWrapper().pluginObjects.ajaxPanel.init();
        this.addEventListener(targetToggle);
      });
    }
  },
  addEventListener(target) {
    if (target) {
      target.addEventListener('click', this.toggleViewMode.bind(this));
    }
  },
  toggleViewMode() {
    const { urlParamKey, modeListing, modeTable } = this.defaults;
    const currentViewMode = getUrlParameter(urlParamKey, modeListing);

    if (currentViewMode === modeTable) {
      setUrlParameter(urlParamKey, modeListing);
    } else if (currentViewMode === modeListing) {
      setUrlParameter(urlParamKey, modeTable);
    }
  },
});
