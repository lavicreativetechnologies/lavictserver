export default {
  defaults: {
    exclusiveFieldClass: 'is--exclusive-selection',
  },

  init() {
    const me = this;

    me.applyDataAttributes();

    me._on(me.$el, 'change', $.proxy(me.exclusiveSelect, me));
  },

  exclusiveSelect(event) {
    const me = this;
    const $target = $(event.target);

    if ($target.hasClass(me.opts.exclusiveFieldClass)) {
      $(`.${me.opts.exclusiveFieldClass}`).prop('checked', false);
      $target.prop('checked', true);
    }
  },

  destroy() {
    this._destroy();
  },
};
