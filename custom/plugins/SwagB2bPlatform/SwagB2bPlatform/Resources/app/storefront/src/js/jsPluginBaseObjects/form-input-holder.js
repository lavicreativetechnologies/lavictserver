/**
 * Sync input field with another field which will be addressed by element class
 *
 * USAGE:
 *
 * <input type="text" name="original" data-b2b-form-input-holder="true" data-targetElement="targetClass">
 *
 * <input type="hidden" name="target" class="targetClass">
 *
 */
export default {
  init() {
    this.registerGlobalListeners();
  },

  registerGlobalListeners() {
    const me = this;

    me._on(me.$el, 'change', $.proxy(me.onChange, me));

    me.$el.change();
  },

  onChange(event) {
    const sourceValue = event.target.value;
    const $targetElement = $(`.${$(event.target).attr('data-targetElement')}`);

    if (!$targetElement.length) {
      return;
    }

    $targetElement.val(sourceValue);
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
