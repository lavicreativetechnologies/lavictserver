export default class ContactPasswordActivationPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_PASSWORD_CONTAINER: '.b2b--password',
        SELECTOR_CHECKBOX_ACTIVATION: '.b2b--password-activation',
    };

    public init(): void {
        this.el.addEventListener('change', this.handleChange.bind(this));
    }

    public update(): void {
        this.init();
    }

    protected handleChange(event: InputEvent): void {
        const { SELECTOR_CHECKBOX_ACTIVATION, SELECTOR_PASSWORD_CONTAINER } = this.options;

        const eventTarget = <HTMLInputElement> event.target;

        if (!eventTarget.matches(SELECTOR_CHECKBOX_ACTIVATION)) {
            return;
        }

        const passwordContainer = <Element[]> this.el.querySelectorAll(SELECTOR_PASSWORD_CONTAINER);

        this.toggleContainerVisibility(passwordContainer, eventTarget.checked);
    }

    protected toggleContainerVisibility(container: Element[], isHidden: boolean): void {
        container.forEach((element) => {
            element.toggleAttribute('hidden', isHidden);
        });
    }
}
