import { B2BEvents } from '../enums';
import { AjaxPanelInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@InitOnLoad
export default class AjaxPanelFormSelectPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_NAVIGATION: 'select.is--ajax-panel-navigation',
        CLASS_AJAX_PANEL_LINK: 'ajax-panel-link',
        DATA_TARGET: 'target',
        DATA_URL: 'href',
    };

    public init(): void {
        this.registerEventListener();
    }

    protected registerEventListener(): void {
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoad.bind(this));
    }

    protected handlePanelAfterLoad(event: Event, eventData: AjaxPanelEventData): void {
        const select = eventData.panel.querySelector(this.options.SELECTOR_NAVIGATION);

        if (!select) {
            return;
        }

        select.addEventListener('change', this.handleChange.bind(this));
        this.triggerChange(select);
    }

    protected triggerChange(select: HTMLSelectElement): void {
        select.dispatchEvent(new Event('change'));
    }

    protected handleChange(event: Event): void {
        const select = <HTMLSelectElement> event.target;
        const option: HTMLOptionElement = select.querySelector(`option[value="${select.value}"]`);

        if (!option || !option.dataset[this.options.DATA_URL]) {
            return;
        }

        const link = this.createCustomLink(option);
        select.insertAdjacentElement('afterend', link);
        link.click();
    }

    protected createCustomLink(option: HTMLOptionElement): HTMLSpanElement {
        const { CLASS_AJAX_PANEL_LINK, DATA_TARGET, DATA_URL } = this.options;
        const link = document.createElement('span');

        link.style.display = 'none';
        link.classList.add(CLASS_AJAX_PANEL_LINK);
        link.dataset[DATA_TARGET] = option.dataset[DATA_TARGET];
        link.dataset[DATA_URL] = option.dataset[DATA_URL];

        return link;
    }
}
