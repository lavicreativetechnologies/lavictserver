enum B2BEvents {
    PANEL_BEFORE_LOAD = 'b2b--ajax-panel_loading',
    PANEL_AFTER_LOAD = 'b2b--ajax-panel_loaded',
    PANEL_REFRESH = 'b2b--ajax-panel_refresh',
    PANEL_RELOAD = 'b2b--ajax-panel_reload',
    PANEL_AJAX = 'b2b--do-ajax-call',
    PANEL_REGISTER = 'b2b--ajax-panel_register',
    UPLOAD_SUCCESS = 'b2b/upload/success',
    UPLOAD_FAILURE = 'b2b--upload_failure',
    FASTORDER_PRODUCT_SUCCESS = 'b2b--fastorder_success',
    ORDERNUMBER_SAVE = 'b2b/ordernumber/onSaveOrderNumber'
}

export default B2BEvents;
