import Chart from 'chart.js';
import { b2bAjaxPanelChart } from '../../jsPluginBaseObjects';

window.Chart = window.Chart || Chart;

const overrides = {
  loadChartConfig() {
    const me = this;
    const config = {
      type: 'line',
      data: {},
      options: {
        responsive: true,
        title: {
          display: false,
          text: '',
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true,
        },
        legend: {
          onClick(event) {
            event.stopPropagation();
          },
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
            },
          }],
        },
      },
    };

    me.defaults.chartConfig = config;
  },
};

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-chart.plugin
 */
export default ({
  ...b2bAjaxPanelChart,
  ...overrides,
  name: 'b2bAjaxPanelChart',
  initOnLoad: false,
  selector: '#b2b-canvas',
});
