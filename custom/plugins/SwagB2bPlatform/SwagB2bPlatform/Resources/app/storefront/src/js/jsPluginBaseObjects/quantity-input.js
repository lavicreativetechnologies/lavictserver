export default {
  defaults: {
    quantityInputSelector: '*.b2b--quantity-input',
    btnMinusSelector: '.btn--minus',
    btnPlusSelector: '.btn--plus',
  },
  init() {
    const me = this;
    const _quantityInputs = $(me.defaults.quantityInputSelector);
    if (_quantityInputs && _quantityInputs.length > 0) {
      for (let i = 0; i < _quantityInputs.length; i++) {
        this.initQuantityInput(_quantityInputs[i]);
      }
    }
  },
  initQuantityInput(_input) {
    const _me = this;
    const _step = parseInt(_input.dataset.step) || 1;
    let _min = parseInt(_input.dataset.min) || 1;
    let _max = parseInt(_input.dataset.max) || -1;
    const _container = _input.parentElement;
    let _btnIntervalId = [];

    const buttonHandler = function (addition, isClick = false) {
      setTimeout(() => {
        if ((isClick && _btnIntervalId.length === 0) || (!isClick && _btnIntervalId.length === 1)) {
          let newValue = parseInt(_input.value) + addition;
          if (newValue > _max) {
            newValue = _max;
          } else if (newValue < _min) {
            newValue = _min;
          }
          _input.value = newValue;
        }
      }, 50);
    };

    const buttonMouseDown = function (addition) {
      if (_btnIntervalId.length === 0) {
        _btnIntervalId.push(setInterval(() => {
          buttonHandler(addition);
        }, 100));
      }
    };

    const buttonMouseUp = function (e) {
      const btnClassList = `${e.target.classList}`;
      const silent = !(
        btnClassList.indexOf(_me.defaults.btnMinusSelector.replace('.', '')) > -1
                || btnClassList.indexOf(_me.defaults.btnPlusSelector.replace('.', '')) > -1
      );

      if (_btnIntervalId.length > 0) {
        for (let i = 0; i < _btnIntervalId.length; i++) {
          clearInterval(_btnIntervalId[i]);
        }
        _btnIntervalId = [];
        if (!!_input.form && !_input.form.classList.contains('buy-widget') && !silent) {
          setTimeout(() => {
            _input.form.submit();
          }, 75);
        }
      }
    };
    const initQuantityButtons = function () {
      const buttonMinus = _container.querySelector(_me.defaults.btnMinusSelector);
      const buttonPlus = _container.querySelector(_me.defaults.btnPlusSelector);

      if (buttonMinus) {
        buttonMinus.addEventListener('click', () => {
          buttonHandler(_step * -1, true);
        });
        buttonMinus.addEventListener('mousedown', () => {
          buttonMouseDown(_step * -1);
        });
        buttonMinus.addEventListener('mouseup', buttonMouseUp);
      }

      if (buttonPlus) {
        buttonPlus.addEventListener('click', () => {
          buttonHandler(_step, true);
        });
        buttonPlus.addEventListener('mousedown', () => {
          buttonMouseDown(_step);
        });
        buttonPlus.addEventListener('mouseup', buttonMouseUp);
      }

      if (buttonMinus || buttonPlus) {
        window.addEventListener('mouseup', buttonMouseUp);
      }
    };

    const initMinMax = function () {
      let _newMin = _min;
      let _newMax = _max;
      let _newValue = _input.value;

      if (_min !== _step) {
        if (_min < _step) {
          _newMin = _step;
        } else if (_min > _step) {
          _newMin = Math.ceil(_min / _step);
        }

        _min = _newMin;
        _input.dataset.min = _newMin;
      }

      if (_max > 0 && _max !== _step) {
        if (_max < _step) {
          _newMax = _step;
        } else if (_max > _step) {
          _newMax = Math.ceil(_max / _step) * _step;
        }

        _max = _newMax;
        _input.dataset.max = _newMax;
      }

      if (_newValue < _min) {
        _newValue = _min;
      } else if (_newValue > _max) {
        _newValue = _max;
      } else if (_newValue % _step) {
        _newValue = Math.ceil(_newValue / _step) * _step;
      }

      _input.value = _newValue;
    };

    if (_max > _min || _step > 1) {
      initQuantityButtons();
      if (_max > _min) {
        initMinMax();
      }
      // _me._on(_input, 'change', this.inputHandler)
      _input.addEventListener('change', this.inputHandler);
    }
  },
  inputHandler(e) {
    e.preventDefault();
    const input = e.target;
    if (input) {
      const value = parseInt(input.value);
      const lastValue = parseInt(input.dataset.lastValue);
      const min = parseInt(input.dataset.min);
      const max = parseInt(input.dataset.max);
      const step = parseInt(input.dataset.step);

      const calcPurchaseStep = function (value, step) {
        if (value > 0) {
          return Math.ceil(value / step) * step;
        }
        if (value < 0) {
          return Math.floor(value / step) * step;
        }

        return step;
      };

      let newValue = calcPurchaseStep(value, step);

      if (max > min && (value > max || value < min)) {
        if (value > max && (lastValue === max || lastValue === value)) {
          newValue = max;
        } else if (value < min && (lastValue === min || lastValue === value)) {
          newValue = min;
        } else {
          newValue = value > max ? max : min;
        }
      }

      input.value = newValue;
      input.dataset.lastValue = newValue;
    }
  },
  destroy() {
    const me = this;
    me._destroy();
  },
};
