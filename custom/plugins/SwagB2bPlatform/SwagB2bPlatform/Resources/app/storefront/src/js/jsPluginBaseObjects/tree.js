export default {
  defaults: {
    moduleSelector: '.is--b2b-tree',
    status: {
      closed: 'is--closed',
      loading: 'is--loading',
      opened: 'is--opened',
      selected: 'is--selected',
    }
  },

  init() {
    this.applyDataAttributes();
    this.registerGlobalListeners();
  },

  registerGlobalListeners() {
    const me = this;
    const tree = me.defaults.moduleSelector;

    me._on(tree, 'click', 'a', $.proxy(me.onExpand, me));

    me._on(tree, 'click', 'li', $.proxy(me.onSelect, me));

    me._on(tree, 'change', 'input', $.proxy(me.onChange, me));
  },

  onExpand(event) {
    const me = this;

    event.preventDefault();
    event.stopPropagation();

    const $anchor = $(event.target).closest('a');
    const $tree = $anchor.closest(me.defaults.moduleSelector);
    const $listItem = $anchor.closest('li');

    if ($listItem.hasClass(me.defaults.status.loading)) {
      return;
    }

    if ($listItem.hasClass(me.defaults.status.opened)) {
      me.changeObjectClass($listItem, me.defaults.status.opened, me.defaults.status.closed);

      $listItem.find('ul').remove();

      return;
    }

    me.changeObjectClass($listItem, me.defaults.status.closed, me.defaults.status.loading);

    const $selectedInput = $tree.find('input:checked');

    $.ajax({
      url: $anchor[0].href,
      success(response) {
        me.changeObjectClass($listItem, me.defaults.status.loading, me.defaults.status.opened);

        $listItem.append(response);
        $selectedInput.prop('checked', true);
        me.doLayout($tree);

        $anchor.trigger('tree_toggle_menu', $anchor);
      },
    });
  },

  changeObjectClass($object, toRemove, toAdd) {
    $object.removeClass(toRemove).addClass(toAdd);
  },

  onSelect(event) {
    const $target = $(event.target);

    if ($target.parents('a').length) {
      return;
    }

    if ($target.is('input')) {
      return;
    }

    const $input = $target.closest('input');

    if ($input.length) {
      $input.click();
    }
  },

  findRootTree($element) {
    const me = this;

    while (true) {
      let $tree = $element.parent().closest(me.defaults.moduleSelector);
      if (!$tree.length) {
        return $element;
      }
      $element = $tree;
    }
  },

  onChange(event) {
    const me = this;

    const $input = $(event.target).closest('input');
    const $tree = me.findRootTree($input);
    const value = $input.val();
    const inputId = $tree.data('treeConnectedInputId');

    $tree
      .find('.is--selected')
      .removeClass(me.defaults.status.selected);

    if ($input.prop('checked')) {
      $input
        .closest('li')
        .addClass(me.defaults.status.selected);
    }

    $(`#${inputId}`).val(value);
  },

  doLayout($tree) {
    $tree.find('.b2b-tree-node-inner').each(function () {
      const $this = $(this);
      const multiplier = $this.parents('ul.is--b2b-tree').length - 1;

      $this.css('marginLeft', multiplier * 24);
    });
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
