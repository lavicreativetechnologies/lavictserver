import {
  addGlobalEventListener,
  breakEventExecution,
  getStorefrontPlugin,
  publish,
  subscribe,
} from '../../utility/index.ts';

const CART_FORM_CLASS = 'cart-form';

const b2bAjaxListingView = {
  defaults: {
    moduleSelector: '.module-listingview',
    tableSelector: '.table--listingview',
    listingviewInputContainerSelector: '.listing-view-inputs',
    submitFormSelector: '.cart-form, .order-list-form',
    orderListDropdownSelector: '.b2b--orderlist-dropdown',
    orderListForm: '.order-list-form',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
    selectorMessageContainer: '.b2b--message-container',
    clearMessageTimeoutMs: 3000,
    classFadeOpacityOut: 'fadeOpacity--out',
    animationDuration: 1000,
  },

  init() {
    $.publish('b2b/listingview/onInit', [this]);

    this.applyDataAttributes();
    this.registerGlobalListeners();
  },

  registerGlobalListeners() {
    const me = this;

    $.publish('b2b/listingview/registerGlobalListeners', [me]);

    const module = me.defaults.moduleSelector;

    me._on(module, 'change', 'input', $.proxy(me.onTableInputChange, me));

    addGlobalEventListener('submit', me.defaults.submitFormSelector, me.onFormSubmit.bind(this));

    subscribe(me.defaults.panelAfterLoadEvent, me.onAfterLoadEvent.bind(this));
  },

  onAfterLoadEvent(event, eventData) {
    const me = this;

    this.setSubmitState($(this.defaults.tableSelector));

    $.publish('b2b/listingview/onAfterLoadEvent', [me]);

    const isAddToCartSource = eventData.source.classList.contains(CART_FORM_CLASS);

    if (!eventData.ajaxData.url.includes('b2bfastorderremote/addProductsToCart')) {
      return;
    }

    this.resetOrderListSelect();

    this.resetMessage();

    if (isAddToCartSource) {
      this.openCartOnSuccess(eventData.panel);
    }
  },

  resetOrderListSelect() {
    const select = this.$el.find(this.defaults.orderListDropdownSelector);
    select.prop('selectedIndex', 0).val();
  },

  openCartOnSuccess(containerPanel) {
    const $panel = $(containerPanel);
    const $messageContainer = $panel.find('.b2b--message-container');

    if (!$messageContainer.find('.b2b--message-success').length) {
      return;
    }

    const offCanvasCart = getStorefrontPlugin('OffCanvasCart');

    if (!offCanvasCart) {
      return;
    }

    offCanvasCart.openOffCanvas(window.router['frontend.cart.offcanvas']);
  },

  onTableInputChange(event) {
    const me = this;
    let $productTable = $(event.target).closest(me.defaults.tableSelector);

    if (!$productTable.length) {
      $productTable = $(this.defaults.tableSelector);
    }

    $.publish('b2b/listingview/onTableInputChange', [me, $productTable]);

    this.setSubmitState($productTable);
  },

  setSubmitState(table) {
    if (!table.length) {
      return;
    }

    const rows = Array.from(table[0].querySelectorAll('tbody tr'));
    let hasValidRows = false;

    rows.forEach((row) => {
      const inputs = Array.from(row.querySelectorAll('input'));
      let validInputs = true;

      inputs.forEach((rowInput) => {
        if (!rowInput.value || rowInput.value === '' || parseInt(rowInput.value, 10) <= 0) {
          validInputs = false;
        }
      });

      if (validInputs) {
        hasValidRows = true;
      }
    });

    const btn = this.$el.find('.cart--link');
    btn.prop('disabled', !hasValidRows);

    if (!document.querySelector('.product-detail-form-container')) {
      const select = this.$el.find('.b2b--orderlist-dropdown');
      select.prop('disabled', !hasValidRows);
    }
  },

  onFormSubmit(event) {
    const me = this;
    const $form = $(event.target);

    breakEventExecution(event);

    $.publish('b2b/listingview/onFormSubmit', [me, $form]);

    me.handleRequest(
      $('[data-id="listing-view-remote-box"]'),
      $form,
      $(me.defaults.listingviewInputContainerSelector),
    );
  },

  handleRequest($panel, $form, $formCloneInputs) {
    const me = this;
    const $clonedForm = $form.clone();

    $.publish('b2b/listingview/handleRequest', [me, $clonedForm]);

    $formCloneInputs.find('input').each(function() {
      $(this).clone().appendTo($clonedForm);
    });

    $clonedForm.find('input').each(function() {
      me.checkProducts($(this), $clonedForm);
    });

    const $selectedOption = $(me.defaults.orderListForm).find('select[name="orderlist"] option:selected');

    if ($selectedOption.length) {
      $clonedForm.append(`<input type="hidden" name="orderlist" value="${$selectedOption.val()}">`);
    }

    $clonedForm.data('ajax-panel-trigger-reload', 'listing-view-grid');

    publish('b2b--do-ajax-call', { url: $clonedForm.attr('action'), target: $panel[0], source: $clonedForm[0] });
  },

  checkProducts($element, $form) {
    $.publish('b2b/fastorder/checkProducts', [$element, $form]);

    if (($element.hasClass('input-quantity') || $element.hasClass('b2b-table-quantity')) && !$element.val()) {
      $form
        .find(`input[name="${$element.attr('name').replace('quantity', 'referenceNumber')}"]`)
        .remove();

      $element.remove();

      return;
    }

    if ($element.is('.input-quantity, .b2b-table-quantity')) {
      const $reference = $form.find(`input[name="${$element.attr('name').replace('quantity', 'referenceNumber')}"]`);

      if (!$reference.val()) {
        $reference.remove();
        $element.remove();
      }
    }
  },

  resetMessage() {
    const {
      animationDuration, selectorMessageContainer, clearMessageTimeoutMs, classFadeOpacityOut,
    } = this.defaults;
    clearTimeout(this.clearMessageTimeout);
    const target = document.querySelector(selectorMessageContainer);
    const animationDelay = clearMessageTimeoutMs - animationDuration;

    setTimeout(() => {
      target.style.animationDuration = `${animationDuration}ms`;
      target.classList.add(classFadeOpacityOut);
    }, animationDelay);

    this.clearMessageTimeout = setTimeout(() => {
      if (target && target.parentElement) {
        target.parentElement.removeChild(target);
      }
    }, clearMessageTimeoutMs);
  },
};

export default ({
  ...b2bAjaxListingView,
  name: 'b2bAjaxListingView',
  initOnLoad: true,
  selector: '.module-listingview',
});
