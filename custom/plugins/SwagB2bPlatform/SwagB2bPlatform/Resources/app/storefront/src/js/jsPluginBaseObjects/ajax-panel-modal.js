/**
 * Handles modal box views:
 *
 * If an ajax panel has the additional class `b2b-modal-panel` the content will not get wrapped inside a modal box.
 *
 * <div class="b2b--ajax-panel b2b-modal-panel" data-id="role-detail">
 *
 * WARNING: this is done through html cloning, do not depend on its event handlers and properties
 */
export default {
  defaults: {
    panelSelector: '.b2b--ajax-panel',

    panelCloseClassSelector: '.b2b--close-modal-box',

    modalModifierClass: 'b2b-modal-panel',

    normalWidth: 1000,

    normalHeight: 711,
  },

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const { ajaxData } = eventData;

        if (!$panel.hasClass(me.defaults.modalModifierClass)) {
          return;
        }

        const height = me.defaults.normalHeight;
        const width = me.defaults.normalWidth;

        event.preventDefault();

        $.overlay.close();

        const $newPanel = $.modal.open($panel[0].outerHTML, {
          width,
          height,
          sizing: 'fixed',
        })._$content.find(me.defaults.panelSelector);

        $('.modal--close').addClass('b2b--modal-close');

        $newPanel
          .data('payload', ajaxData.data)
          .data('url', ajaxData.url)
          .removeClass(me.defaults.modalModifierClass)
          .data('b2bModalAjaxPanel', true);

        $newPanel.trigger('b2b--ajax-panel_register');
      },
    );

    me._on(
      document,
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const hasCloseTrigger = Boolean($panel.find(me.defaults.panelCloseClassSelector).length);

        if (!hasCloseTrigger) {
          return;
        }

        $.modal.close();
      },
    );
  },

  destroy() {
    const me = this;
    me._destroy();

    $.overlay.close();
  },
};
