import { IPluginBaseClass } from './IPluginBaseClass';
import IPluginWrapper from './IPluginWrapper';

declare global {
    interface Window {
        PluginManager: {
            register: (pluginName: string, pluginClass: any, selector?: string | NodeList | HTMLElement) => void;
            deregister: (pluginName: string) => void;
            getPlugin: (pluginName: string) => Map<any, any>;
            getPluginList: () => Map<any, any>[];
            initializePlugin: (pluginName: string, selector?: string | Document | HTMLElement, options?: object) => void;
            initializePlugins: () => void;
        },
        B2bPluginWrapper: IPluginWrapper,
        PluginBaseClass: IPluginBaseClass,
        Chart: any,
        StateManager: {
            addPlugin: (selector: string, pluginName: string) => void;
        },
        router: object;
    }
}

export {};
