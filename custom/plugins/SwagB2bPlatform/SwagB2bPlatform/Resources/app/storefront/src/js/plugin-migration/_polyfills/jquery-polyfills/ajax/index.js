import prepareRequest from './prepareRequest';
import handleResponse from './handleResponse';

function getContentType(data, contentType) {
  let _contentType = contentType;

  if (data instanceof FormData) {
    _contentType = 'multipart/form-data';
  }

  return _contentType;
}

function getHeaders(contentType) {
  const { accessKey = '', contextToken = '' } = window;

  const headers = {
    'sw-access-key': accessKey,
    'sw-context-token': contextToken,
  };

  if (contentType && contentType !== 'multipart/form-data') {
    Object.assign(headers, { 'Content-Type': contentType });
  }

  return headers;
}

function performRequest(url, config) {
  return window.fetch(url, config);
}

export default async function (config) {
  const {
    url, data, type, contentType = 'application/x-www-form-urlencoded', method = 'GET',
  } = config;

  const _method = (type || method).toUpperCase();
  const _contentType = getContentType(data, contentType);

  const headers = getHeaders(_contentType);

  const { _url, _data } = prepareRequest(url, _method, _contentType, data);

  const requestConfig = {
    method: _method,
    headers,
  };

  if (_method !== 'GET') {
    requestConfig.body = _data;
  }

  const response = await performRequest(_url, requestConfig);

  await handleResponse(response, config);
}
