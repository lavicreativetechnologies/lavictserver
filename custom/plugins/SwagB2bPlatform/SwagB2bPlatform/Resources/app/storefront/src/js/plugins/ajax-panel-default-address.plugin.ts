import { AjaxPanelInterface } from '../decorator';

@AjaxPanelInterface
export default class AjaxPanelDefaultAddressPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_EXCLUSIVE_FIELD: '.is--exclusive-selection',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this.el.addEventListener('change', this.handleExclusiveSelect.bind(this));
    }

    protected handleExclusiveSelect(event: Event): void {
        const eventTarget = <HTMLInputElement>event.target;

        if (!eventTarget.matches(this.options.SELECTOR_EXCLUSIVE_FIELD)) {
            return;
        }

        this.checkTarget(eventTarget);
    }

    protected checkTarget(target): void {
        const exclusiveFields: NodeListOf<HTMLInputElement> = document.querySelectorAll(this.options.SELECTOR_EXCLUSIVE_FIELD);
        exclusiveFields.forEach((field) => {
            field.checked = false;
        });

        target.checked = true;
    }
}
