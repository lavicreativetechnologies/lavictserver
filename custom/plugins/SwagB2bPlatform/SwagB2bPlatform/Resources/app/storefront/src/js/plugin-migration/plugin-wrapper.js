import {
  addJqueryRef, PluginInstance, runPlugin,
} from './utility';
import applyPolyfills from './_polyfills/applyPolyfills';

export default class PluginWrapper {
  constructor(initPlugins = true) {
    this.initPlugins = initPlugins;

    return this.init();
  }

  async init() {
    window.B2bPluginWrapper = this;

    this.pluginObjects = {};
    this.initialized = {};
    this.aliasMap = {};
    this.events = {};
    this.actions = {
      pushEvent: this.pushEvent.bind(this),
    };

    await applyPolyfills();
    await this.getPluginList();

    if (!this.initPlugins) {
      return;
    }

    this.initializePlugins();
  }

  initializePlugins() {
    const { pluginObjects } = this;
    Object.keys(pluginObjects).forEach((pluginName) => {
      const { name, plugin, $el } = pluginObjects[pluginName];

      if (plugin.initOnLoad && $el && $el.length > 0) {
        runPlugin(name, plugin);
      }
    });
  }

  async getPluginList() {
    const PluginMigrations = require('./sw5-plugins/_index'); // eslint-disable-line
    const pluginNames = Object.keys(PluginMigrations);
    for (let i = 0; i < pluginNames.length; i++) {
      const pluginName = pluginNames[i];
      const { alias } = PluginMigrations[pluginName];

      if (alias !== pluginName) {
        this.aliasMap[alias] = pluginName;
      }

      this.pluginObjects[pluginName] = new PluginInstance(pluginName, PluginMigrations[pluginName]);
      this.initialized[pluginName] = false;

      addJqueryRef(pluginName);
    }
  }

  pushEvent(data) {
    const { event, target, args = {} } = data;

    if (!event || !target) {
      return;
    }

    if (!this.events[event]) {
      this.events[event] = [];
    }
    this.events[event].push({
      target,
      args,
    });
  }
}
