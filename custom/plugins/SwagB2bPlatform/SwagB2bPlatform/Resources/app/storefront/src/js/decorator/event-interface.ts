import { addGlobalEventListener, breakEventExecution } from '../utility';

export default function EventInterface<T extends { new(...args: any[]): {} }>(constructor: T) {
    constructor.prototype._globalEventListeners = [];

    return class extends constructor {
        private _globalEventListeners: { eventType: string, callback: EventListener }[];

        protected _addGlobalEventListener(eventType: string, selector: string, callback: (event: CustomEvent) => any): void {
            const callbackFn: EventListener = addGlobalEventListener(eventType, selector, callback);
            this._saveEvent.call(this, eventType, callbackFn);
        }

        protected _dispatchEvent(eventType: string, target: HTMLElement, defaultPrevented: Boolean = false): void {
            const event = this._createEvent(eventType);

            if (defaultPrevented) {
                this._breakEventExecution(event);
            }

            target.dispatchEvent(event);
        }

        protected _breakEventExecution(event: Event | CustomEvent): void {
            breakEventExecution(event);
        }

        protected _removeGlobalEventListeners(): void {
            this._globalEventListeners.forEach(({ eventType, callback }) => {
                document.removeEventListener(eventType, callback);
            });
        }

        protected _saveEvent(eventType: string, callback: EventListener): void {
            this._globalEventListeners.push({
                eventType,
                callback,
            });
        }

        protected _createEvent(eventType: string): Event {
            if (typeof Event === 'function') {
                return new Event(eventType, { bubbles: true, cancelable: true });
            }

            const event = document.createEvent('Event');
            event.initEvent(eventType, true, true);

            return event;
        }
    };
}
