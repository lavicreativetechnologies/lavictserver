/**
 * Loads occurred per ajax, replaces it in the DOM and intercepts all it's submit and link click events.
 *
 * USAGE:
 *
 * Basic container needs to look like this:
 *
 * <div class="b2b--ajax-panel" data-url="http://foo/bar"></div>
 *
 * If DOM element should be a trigger, but can neither be <a>, nor <form>, you can add a special class
 *
 * <button class="ajax-panel-link" data-href="http://foo/bar">Click</button>
 *
 * If a link should use a different target:
 *
 * Container:  <div ... data-id="foreign"></div>
 * Link: <a [...] data-target="foreign">Open in another component</a>
 *
 * If you want a link to be ignored, add the class '.ignore--b2b-ajax-panel'.
 *
 * EVENTS:
 *
 * Triggered:
 *    'b2b--ajax-panel_loading': triggered on the panel before loading new data
 *    'b2b--ajax-panel_loaded': triggered on the panel after loading and replacing the data
 *
 * Handled:
 *      'b2b--ajax-panel_refresh': triggered on the panel reissues the last request on the panel
 *      'b2b--do-ajax-call': trigger a ajax request with included refresh
 *
 * global: CSRF
 */
export default {
  defaults: {
    panelSelector: '.b2b--ajax-panel',

    panelLinkDataKey: 'href',
    panelUrlDataKey: 'url',
    panelPayloadDataKey: 'payload',
    panelHistoryDataKey: 'lastPanelRequest',

    panelLoadingClass: 'content--loading',
    panelIconLoadingIndicatorClass: 'icon--loading-indicator',

    ignoreSelector: '.ignore--b2b-ajax-panel',

    panelLinkSelector: '.ajax-panel-link',

    panelBeforeLoadEvent: 'b2b--ajax-panel_loading',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',

    panelRefreshEvent: 'b2b--ajax-panel_refresh',
    panelRegisterEvent: 'b2b--ajax-panel_register',

    performAjaxCallEvent: 'b2b--do-ajax-call',
  },

  init() {
    const me = this;

    this.applyDataAttributes();
    this.registerGlobalListeners();
    this.registerShopwarePlugins();

    $(me.defaults.panelSelector).each((index, panel) => {
      me.register(panel);
    });
  },

  register(panel) {
    const $panel = $(panel);
    const url = $panel.data(this.defaults.panelUrlDataKey);

    if (url && url.length) {
      this.load(url, panel, $panel);
    }
  },

  error(target) {
    $(target).html('<div style="margin:20px"><h1 style="margin-bottom:5px">An error occurred</h1><p>Please enable error logging for more information about this error.</p></div>');
  },

  load(url, target, source) {
    const me = this;
    const $target = $(target);
    let serializedData = {};
    let method = 'GET';

    if (source && source.length) {
      if (source.is('form')) {
        if (source.attr('method')) {
          method = source.attr('method').toUpperCase() === 'POST' ? 'POST' : 'GET';
        }
        serializedData = source.serializeArray();
      } else if (source.data(me.defaults.panelPayloadDataKey)) {
        serializedData = source.data(me.defaults.panelPayloadDataKey);
      }
    }

    const ajaxData = {
      type: method,
      url,
      data: serializedData,
    };

    me.doAjaxCall(ajaxData, $target, source);
  },

  doAjaxCall(ajaxData, $target, $source) {
    const me = this;

    $target.append($('<div>', {
      class: me.defaults.panelLoadingClass,
      html: $('<i>', {
        class: me.defaults.panelIconLoadingIndicatorClass,
      }),
    }));

    if (me.notify($target, me.defaults.panelBeforeLoadEvent, { panel: $target, source: $source, ajaxData })) {
      return;
    }

    if (ajaxData.type === 'GET') {
      $target.data(me.defaults.panelHistoryDataKey, ajaxData);
    }

    $.ajax($.extend({}, ajaxData, {
      success(response, status, xhr) {
        /** global: window */
        if (xhr.getResponseHeader('B2b-no-login')) {
          window.location.reload();

          return;
        }

        // close modal after success
        if ($source.data('close-success') && !$(response).find('.modal--errors').length) {
          $.modal.close();
        } else {
          $target.html(response);
        }

        me.refreshShopwarePlugins($target);

        $target.trigger(me.defaults.panelAfterLoadEvent, {
          panel: $target,
          source: $source,
          ajaxData,
        });
      },
      error() {
        me.error($target);
      },
      always() {
        $target.removeClass(me.defaults.panelLoadingClass);
      },
    }));
  },

  breakEventExecution(event) {
    event.preventDefault();
  },

  registerGlobalListeners() {
    const me = this;

    me._on(document, 'click', `${me.defaults.panelSelector} a, ${me.defaults.panelLinkSelector}`, function (event) {
      if (event.isDefaultPrevented()) {
        return;
      }

      const $eventTarget = $(event.target);
      if (!$eventTarget.is(this) && $eventTarget.closest('form').length) {
        return;
      }

      const $anchor = $(this);
      if ($anchor.is(me.defaults.ignoreSelector)) {
        return;
      }

      me.breakEventExecution(event);

      let url;
      const $panel = $anchor.closest(me.defaults.panelSelector);
      const targetPanel = $anchor.data('target');
      const $targetPanel = $(`${me.defaults.panelSelector}[data-id="${targetPanel}"]`);

      if ($anchor[0].hasAttribute(me.defaults.panelLinkDataKey)) {
        url = $anchor.attr(me.defaults.panelLinkDataKey);
      } else {
        url = $anchor.data(me.defaults.panelLinkDataKey);
      }

      if ($targetPanel.length) {
        me.load(url, $targetPanel, $anchor);
      } else {
        me.load(url, $panel, $anchor);
      }
    });

    me._on(document, 'submit', `${me.defaults.panelSelector} form, form${me.defaults.panelLinkSelector}`, function (event) {
      if (event.isDefaultPrevented()) {
        return;
      }

      const $form = $(this);

      if ($form.is(me.defaults.ignoreSelector)) {
        return;
      }

      me.breakEventExecution(event);

      const $panel = $form.closest(me.defaults.panelSelector);
      const url = $form.attr('action');
      const targetPanel = $form.data('target');
      const $targetPanel = $(`${me.defaults.panelSelector}[data-id="${targetPanel}"]`);

      if ($targetPanel.length) {
        me.load(url, $targetPanel, $form);
      } else {
        me.load(url, $panel, $form);
      }
    });

    me._on(document, me.defaults.panelAfterLoadEvent, me.defaults.panelSelector, function (event) {
      if (event.isDefaultPrevented()) {
        return;
      }

      const $panel = $(this);

      me.breakEventExecution(event);

      $panel.find(me.defaults.panelSelector).each(function () {
        const panel = this;

        me.register(panel);
      });
    });

    me._on(document, me.defaults.performAjaxCallEvent, (event, url, $target, source) => {
      me.load(url, $target, source);
    });

    me._on(document, me.defaults.panelRefreshEvent, me.defaults.panelSelector, function (event) {
      if (event.isDefaultPrevented()) {
        return;
      }

      const $panel = $(this);
      const ajaxData = $panel.data(me.defaults.panelHistoryDataKey);

      if (!ajaxData) {
        return;
      }

      me.breakEventExecution(event);
      me.doAjaxCall(ajaxData, $panel, $panel);
    });

    me._on(document, me.defaults.panelRegisterEvent, me.defaults.panelSelector, function (event) {
      if (event.isDefaultPrevented()) {
        return;
      }

      const $panel = $(this);

      me.register($panel);
    });

    me._on(document, 'click', '*[data-form-id]', function (event) {
      const me = this;
      const formId = $(me).data('form-id');
      const $form = $(`#${formId}`);

      if (!$form.length) {
        return;
      }

      $form.submit();
    });
  },

  notify($panel, eventName, data) {
    const beforeEvent = $.Event(eventName);
    $panel.trigger(beforeEvent, data);

    return beforeEvent.isDefaultPrevented();
  },

  registerShopwarePlugins() {
    StateManager.addPlugin('select:not([data-no-fancy-select="true"])', 'swSelectboxReplacement');

    StateManager.addPlugin('.datepicker', 'swDatePicker');

    StateManager.addPlugin('.csv--configuration .configuration--header', 'swCollapsePanel', {
      contentSiblingSelector: '.csv--configuration .configuration--content',
    });
  },

  refreshShopwarePlugins($target) {
    $.modal.onWindowResize();

    StateManager.updatePlugin('select:not([data-no-fancy-select="true"])', 'swSelectboxReplacement');

    if ($target.find('.datepicker').length) {
      StateManager.updatePlugin('.datepicker', 'swDatePicker');
    }

    StateManager.updatePlugin('.csv--configuration .configuration--header', 'swCollapsePanel');

    CSRF.updateForms();
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
