import { B2BEvents } from '../enums';
import { AjaxPanelInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@InitOnLoad
export default class AccordionPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_ACCORDION: '.b2b-accordion',
        SELECTOR_TITLE: '.b2b-accordion__title',
        CLASS_ACTIVE: 'b2b-accordion--open',
    };

    public init(): void {
        this.registerEventListeners();
        this.handlePanelAfterLoadEvent(null);
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelAfterLoadEvent(event: CustomEvent): void {
        const accordions = (event ? <HTMLElement>event.target : document).querySelectorAll(this.options.SELECTOR_ACCORDION);

        accordions.forEach((accordion) => {
            this.initAccordion(accordion);
        });
    }

    protected initAccordion(target: HTMLElement): void {
        const head = target.querySelector(this.options.SELECTOR_TITLE);

        head.addEventListener('click', this.handleClick.bind(this, target));
    }

    protected handleClick(target: HTMLElement): void {
        const { CLASS_ACTIVE } = this.options;
        const isOpen = target.classList.contains(CLASS_ACTIVE);

        target.classList.toggle(CLASS_ACTIVE, !isOpen);

        if (!isOpen) {
            this.scrollIntoView(target);
        }
    }

    protected scrollIntoView(target: HTMLElement): void {
        target.scrollIntoView({
            block: 'start',
            behavior: 'smooth',
        });
    }
}
