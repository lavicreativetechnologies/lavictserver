import {
  getStorefrontPlugin, subscribe, breakEventExecution,
} from '../../utility/index.ts';
import { b2bOrderList } from '../../jsPluginBaseObjects';

const overrides = {
  defaults: {
    orderListRemoteAddButtonSelector: '.orderlist--add',
    orderListGridAddToCartSelector: '.orderlist--add-to-cart',
    orderListDetailQuantitySelector: '.product-detail-quantity-select',
    alertHideDelay: 2000,
    snippets: {
      createPlaceholder: null,
      createError: null,
    },
    inputContainerCls: 'b2b-orderlist-input-container',
    notificationContainerCls: 'b2b-orderlist-notification-container',
    orderlistDropdownSelector: '.b2b--orderlist-dropdown',
    orderListValidationSelector: '.b2b--order-list-validation',
    keyMap: {
      esc: 27,
    },
    inProgress: false,
    selectorMessageContainer: '.b2b--message-container',
    clearMessageTimeoutMs: 3000,
    classFadeOpacityOut: 'fadeOpacity--out',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
    animationDuration: 1000,
  },
  clearMessageTimeout: null,

  init() {
    const me = this;

    if (!me.$el.length) {
      me.$el = $('body');
    }

    const $orderlistDropdown = me.$el.find(me.opts.orderlistDropdownSelector);

    me.applyDataAttributes();

    me._on(me.$el.find(me.defaults.orderListValidationSelector), 'click', $.proxy(me.validateOrderList, me));

    if (!$orderlistDropdown.length) {
      return;
    }

    me.opts.snippets.createPlaceholder = $orderlistDropdown.data('new-placeholder');
    me.opts.snippets.createError = $orderlistDropdown.data('new-error');

    me._on(me.opts.orderlistDropdownSelector, 'change', $.proxy(me.changeOrderList, me));

    me._on(me.opts.orderListDetailQuantitySelector, 'change', $.proxy(me.handleDetailQuantity));

    me.$el.on(me.getEventName('click'), '.orderlist-create-abort', $.proxy(me.onOrderListCreateAbort, me));

    me.$el.on(me.getEventName('click'), '.orderlist-create-save', $.proxy(me.onOrderListCreateSubmit, me));

    me.$el.on(me.getEventName('keyup'), '.form--orderlist-add input', $.proxy(me.onOrderListCreateKeyUp, me));

    me.$el.on(me.getEventName('focus'), '.form--orderlist-add input', $.proxy(me.onOrderListCreateFocusIn, me));

    me.$el.on(me.getEventName('focusout'), '.form--orderlist-add input', $.proxy(me.onOrderListCreateFocusOut, me));

    subscribe(me.defaults.panelAfterLoadEvent, $.proxy(me.onAfterLoadEvent, me));

    me.removeAlertsDelayed();
  },

  onAfterLoadEvent(event, eventData) {
    if (!eventData.ajaxData) {
      return;
    }

    if (eventData.ajaxData.data.length > 1) {
      this.resetMessage();
    }
  },

  validateOrderList(event) {
    const me = this;
    const $target = $(event.currentTarget);
    const $form = $target.closest('form');
    const submit = $form.find('button').get(0);

    if (submit && !submit.disabled) {
      $.ajax({
        url: $target.data('check-url'),
        type: 'post',
        data: $form.serialize(),
        success(response) {
          if (response) {
            const $formInner = $(response).find('.orderlist--add-to-cart');
            $.b2bConfirmModal.open(response, {
              confirm() {
                $.ajax({
                  url: $target.data('action-url'),
                  type: 'post',
                  data: $formInner.serialize(),
                  success() {
                    me.handleOrderListCreateOrder($formInner);

                    $.b2bConfirmModal.close();
                  },
                });
              },
              cancel() {
                $.b2bConfirmModal.close();
              },
            });
          } else {
            me.handleOrderListCreateOrder($form);
          }
        },
        error() {
          $.ajax({
            url: $target.data('action-url'),
            type: 'post',
            data: $form.serialize(),
            success() {
              $.b2bConfirmModal.close();
            },
          });
        },
      });
    }
  },

  handleOrderListCreateOrder() {
    const offCanvasCart = getStorefrontPlugin('OffCanvasCart');
    if (offCanvasCart) {
      offCanvasCart.openOffCanvas(window.router['frontend.cart.offcanvas']);
    }
  },

  enableOrderListCreate($select) {
    $select.closest('.select-field').hide().siblings('.create-field').show();

    $('.form--orderlist-add').find('input').focus();
  },

  disableOrderListCreate($select) {
    $select.val($select.find('option:first').val());
    $select.closest('.select-field').show().siblings('.create-field').hide();
  },

  onOrderListCreateSubmit(event) {
    const me = this;
    const $submitButton = $(event.currentTarget);
    const $form = $submitButton.closest('.b2b--orderlist-add');
    const $parentForm = $form.closest('form');
    const $select = $parentForm.find(me.opts.orderlistDropdownSelector);
    let orderList = null;

    breakEventExecution(event);

    if (me.opts.inProgress) {
      return;
    }

    me.opts.inProgress = true;

    const csrf = $form.data('csrf');
    const name = $form.find('input[name=name]').val();

    const formData = new FormData();
    formData.append('name', name);
    formData.append('_csrf_token', csrf);

    $.ajax({
      url: $form.data('action-create'),
      method: 'POST',
      data: formData,
      success(jsonResponse) {
        orderList = JSON.parse(jsonResponse);

        if (!orderList.orderListId) {
          me.showOrderListCreateError();

          return;
        }

        $select.remove();
        $parentForm.append($('<input>', {
          type: 'hidden',
          name: 'orderlist',
          value: orderList.orderListId,
        }));

        $parentForm[0].dispatchEvent(new Event('submit', { bubbles: true }));
      },
      error() {
        me.showOrderListCreateError();
      },
      always() {
        me.opts.inProgress = false;
      },
    });
  },

  resetMessage() {
    const {
      animationDuration, selectorMessageContainer, clearMessageTimeoutMs, classFadeOpacityOut,
    } = this.defaults;
    const target = document.querySelector(selectorMessageContainer);
    const animationDelay = clearMessageTimeoutMs - animationDuration;

    clearTimeout(this.clearMessageTimeout);

    target.style.animationDuration = `${animationDuration}ms`;

    setTimeout(() => {
      target.classList.add(classFadeOpacityOut);
    }, animationDelay);

    this.clearMessageTimeout = setTimeout(() => {
      if (target && target.parentElement) {
        target.parentElement.removeChild(target);
      }
    }, clearMessageTimeoutMs);
  },

  changeOrderList(event) {
    const me = this;
    const select = event.currentTarget;

    if (select.value === '_new_') {
      me.enableOrderListCreate($(select));
    } else {
      select.closest('form').dispatchEvent(new Event('submit', { bubbles: true }));
    }
  },
};

export default ({
  ...b2bOrderList,
  ...overrides,
  name: 'b2bOrderList',
  initOnLoad: true,
  selector: '.module--orderlist',
});
