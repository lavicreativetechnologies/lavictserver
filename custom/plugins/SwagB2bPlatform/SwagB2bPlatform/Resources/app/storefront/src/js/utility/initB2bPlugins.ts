/* istanbul ignore file */

export default function(): Promise<void> {
    return new Promise((resolve) => {
        const { register, initializePlugin } = window.PluginManager;
        const plugins = require('../plugins');

        const pluginNames = Object.keys(plugins);

        pluginNames.forEach((pluginName) => {
            const registryPluginName = `B2B_${pluginName}`;
            const plugin = plugins[pluginName];

            register(registryPluginName, plugins[pluginName]);

            if (!plugin.initOnLoad) {
                return;
            }

            initializePlugin(registryPluginName, document);
        });

        resolve();
    });
}
