import { publish, subscribe } from '../utility';

export default function AjaxPanelInterface<T extends { new(...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
        protected _subscribe(eventType: string, callback: (event: CustomEvent, eventData?: AjaxPanelEventData | object) => any): void {
            subscribe(eventType, callback);
        }

        protected _publish(eventType: string | CustomEvent, payload: AjaxPanelEventData | object): CustomEvent {
            return publish(eventType, payload);
        }
    };
}
