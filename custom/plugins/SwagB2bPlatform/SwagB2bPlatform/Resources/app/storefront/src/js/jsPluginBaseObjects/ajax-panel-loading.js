/**
 * This plugin adds loading indicators to panels which are flagged by the 'is--loading' class
 */
export default {
  defaults: {

    loadingIndicatorParentCls: 'content--loading',

    loadingIndicatorCls: 'icon--loading-indicator',

    disableCls: 'has--no-indicator',
  },

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const $panel = $(eventData.panel);

        if ($panel.hasClass(me.defaults.disableCls)) {
          return;
        }

        $panel.html(`<div class="${me.defaults.loadingIndicatorParentCls}"><i class="${me.defaults.loadingIndicatorCls}"></i></div>`);
      },
    );
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
