import { b2bAjaxPanelDefaultAddress } from '../../jsPluginBaseObjects';


/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-default-address.plugin
 */
export default ({
  ...b2bAjaxPanelDefaultAddress,
  name: 'b2bAjaxPanelDefaultAddress',
  initOnLoad: false,
});
