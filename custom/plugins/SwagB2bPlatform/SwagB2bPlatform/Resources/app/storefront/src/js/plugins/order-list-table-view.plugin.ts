import { B2BEvents, B2BKeycodes } from '../enums';
import { AjaxPanelInterface, EventInterface } from '../decorator';

@AjaxPanelInterface
@EventInterface
export default class OrderListTableViewPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_TABLE_VIEW: '.b2b--orderlist-tableview',
        SELECTOR_SUBMIT_BUTTON: '.cart-form .btn',
        SELECTOR_SELECT: '.b2b--orderlist-dropdown',
        SELECTOR_QUANTITY_INPUT: '.fast-order-inputs .form-control',
    };

    private tableView: HTMLElement;

    public init(): void {
        this.tableView = document.querySelector(this.options.SELECTOR_TABLE_VIEW);

        if (!this.tableView) {
            return;
        }

        this.initTableView();
        this.registerEventListeners();
    }

    protected initTableView(): void {
        this.tableView.addEventListener('keypress', this.handleKeyboardEnterEvent.bind(this));
        const quantityInputs = this.tableView.querySelectorAll(this.options.SELECTOR_QUANTITY_INPUT);

        quantityInputs.forEach((input) => {
            input.addEventListener('input', this.handleQuantityChange.bind(this));
        });
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.clearAfterSubmit.bind(this));
    }

    protected handleKeyboardEnterEvent(event: KeyboardEvent): void {
        if (event.keyCode !== B2BKeycodes.ENTER) {
            return;
        }

        this._breakEventExecution(event);
        const button: HTMLButtonElement = this.tableView.querySelector(this.options.SELECTOR_SUBMIT_BUTTON);

        if (!button || !this.hasValidRows()) {
            return;
        }

        button.click();
    }

    protected handleQuantityChange(): void {
        this.setSubmitState();
    }

    protected hasValidRows(): Boolean {
        const inputs: NodeListOf<HTMLInputElement> = this.tableView.querySelectorAll(this.options.SELECTOR_QUANTITY_INPUT);
        let hasQuantity = false;

        inputs.forEach(({ value }) => {
            const intValue = parseInt(value, 10);
            if (intValue > 0) {
                hasQuantity = true;
            }
        });

        return hasQuantity;
    }

    protected clearAfterSubmit(): void {
        const inputs: NodeListOf<HTMLInputElement> = this.tableView.querySelectorAll(this.options.SELECTOR_QUANTITY_INPUT);

        inputs.forEach((input) => {
            input.value = '';
        });
    }

    protected setSubmitState(): void {
        const { SELECTOR_SUBMIT_BUTTON, SELECTOR_SELECT } = this.options;
        const btn = this.tableView.querySelector(SELECTOR_SUBMIT_BUTTON);
        const select = this.tableView.querySelector(SELECTOR_SELECT);

        const disableInputs = !this.hasValidRows();

        btn.toggleAttribute('disabled', disableInputs);
        select.toggleAttribute('disabled', disableInputs);
    }
}
