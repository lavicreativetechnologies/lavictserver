import PluginBaseClass from '../compatibility/PluginBase.class.js';
import { IPluginBaseClass } from '../interfaces';

export default function() {
    window.PluginBaseClass = (window.PluginBaseClass ?? <IPluginBaseClass> PluginBaseClass);
}
