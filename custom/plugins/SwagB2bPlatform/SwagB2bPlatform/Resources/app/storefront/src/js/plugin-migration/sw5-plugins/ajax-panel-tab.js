import { b2bAjaxPanelTab } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-tab.plugin
 */
export default ({
  ...b2bAjaxPanelTab,
  name: 'b2bAjaxPanelTab',
  initOnLoad: true,
  selector: 'body',
});
