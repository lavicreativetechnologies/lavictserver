import { b2bAutoSubmit } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/auto-submit.plugin
 */
export default ({
  ...b2bAutoSubmit,
  name: 'b2bAutoSubmit',
  initOnLoad: true,
  selector: 'body',
});
