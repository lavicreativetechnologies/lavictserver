import { b2bConfirmBox } from '../../jsPluginBaseObjects';

function hasOpenModal() {
  return !!document.querySelector('.js--modal-animated');
}

const overrides = {
  init(content, options) {
    this.open(content, options);
  },

  open(content, handler) {
    const me = this;
    const opts = me.defaults;

    if (hasOpenModal()) {
      const divModalBackground = document.querySelector('.modal--background-visible');
      const divModalBackgroundClone = divModalBackground.cloneNode(false);

      document.querySelector('.b2b--modal').appendChild(divModalBackgroundClone);
      divModalBackgroundClone.classList.add('modal--background', 'modal--background-visible', 'b2b-confirm-modal--background');
      divModalBackgroundClone.addEventListener('click', () => {
        me.close();
      });
    }

    me._$handler = handler;

    let $modalBox = me._$modalBox;
    if (!$modalBox) {
      me._$previousOverlay = $.overlay.overlay;

      $.overlay.open($.extend({}, {
        closeOnClick: opts.closeOnOverlay,
        onClose: $.proxy(me.onOverlayClose, me),
        overlayCls: 'js--overlay b2bConfirmOverlay',
      }));

      me.initModalBox();
      me.registerEvents();

      $modalBox = me._$modalBox;
    }

    me._$content.html(content);

    $modalBox.find(me.defaults.confirmModalSelector).click($.proxy(me.onButtonConfirm, me));
    $modalBox.find(me.defaults.cancelModalSelector).click($.proxy(me.onButtonCancel, me));

    $('html').addClass('no--scroll');

    setTimeout(() => {
      $modalBox.addClass('js--modal-animated');
    });

    return me;
  },

  close() {
    const me = this;
    const $modalBox = me._$modalBox;

    const modalBackground = document.querySelector('.b2b-confirm-modal--background');
    if (modalBackground) {
      modalBackground.remove();
    }

    if ($modalBox !== null) {
      $modalBox.removeClass('js--modal-animated');

      setTimeout(() => {
        $modalBox.remove();

        me._$content = null;
        me._$modalBox = null;
        me._$handler = null;
      }, 500);
    }

    if (!hasOpenModal()) {
      $.overlay.close();
      document.body.parentElement.classList.remove('no--scroll');
    }

    $.overlay.overlay = me._$previousOverlay;
    me._$previousOverlay = null;
  },
};

export default ({
  ...b2bConfirmBox,
  ...overrides,
  name: 'b2bConfirmBox',
  alias: 'b2bConfirmModal',
  initOnLoad: false,
});
