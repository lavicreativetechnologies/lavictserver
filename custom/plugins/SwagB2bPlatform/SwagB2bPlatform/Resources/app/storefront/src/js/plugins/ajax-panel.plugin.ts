import axios, { AxiosResponse } from 'axios';
import { B2BEvents } from '../enums';
import { parseForm } from '../utility';
import { AjaxPanelInterface, EventInterface, InitOnLoad } from '../decorator';

declare const $;

@AjaxPanelInterface
@EventInterface
@InitOnLoad
export default class AjaxPanelPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_PANEL: '.b2b--ajax-panel',
        SELECTOR_PANEL_LINK: '.ajax-panel-link',
        SELECTOR_MODAL: '.b2b--modal',
        CLASS_IGNORE: 'ignore--b2b-ajax-panel',
        CLASS_IGNORE_HISTORY: 'ajax-panel--ignore-history',
        DATA_URL: 'url',
        DATA_LINK: 'href',
        DATA_PAYLOAD: 'payload',
        DATA_TARGET: 'target',
        DATA_HISTORY: 'lastPanelRequest',
        DATA_MODAL_CLOSE: 'closeSuccess',
        DATA_FORM_ID: 'formId',
        HEADER_NO_LOGIN: 'B2b-no-login',
    };

    public init(): void {
        setTimeout(() => {
            this.registerGlobalListeners();

            document.querySelectorAll(this.options.SELECTOR_PANEL).forEach((panel) => {
                this.register(panel);
            });
        });
    }

    protected register(panel: HTMLElement): void {
        const url = panel.dataset[this.options.DATA_URL];

        if (!url) {
            return;
        }

        this.load(url, panel, panel);
    }

    protected registerGlobalListeners(): void {
        const {
            PANEL_REGISTER, PANEL_AJAX, PANEL_REFRESH, PANEL_AFTER_LOAD,
        } = B2BEvents;
        const { SELECTOR_PANEL, SELECTOR_PANEL_LINK } = this.options;

        this._addGlobalEventListener('click', `${SELECTOR_PANEL} a, ${SELECTOR_PANEL_LINK}`, this.handleLinkEvent.bind(this));
        this._addGlobalEventListener('submit', `${SELECTOR_PANEL} form, form${SELECTOR_PANEL_LINK}`, this.handleSubmitEvent.bind(this));
        this._addGlobalEventListener('click', '[data-form-id]', this.handleFormLinkEvent.bind(this));

        this._subscribe(PANEL_REGISTER, this.handleRegisterEvent.bind(this));
        this._subscribe(PANEL_AJAX, this.handleAjaxEvent.bind(this));
        this._subscribe(PANEL_REFRESH, this.handleRefreshEvent.bind(this));
        this._subscribe(PANEL_AFTER_LOAD, this.handleAfterLoadEvent.bind(this));
    }

    protected handleLinkEvent(event: Event): void {
        if (event.defaultPrevented) {
            return;
        }

        const {
            CLASS_IGNORE, DATA_LINK, DATA_TARGET, SELECTOR_MODAL, SELECTOR_PANEL,
        } = this.options;

        const eventTarget = <HTMLElement>event.target;

        if (eventTarget.classList.contains(CLASS_IGNORE)) {
            return;
        }

        if (!eventTarget.closest(SELECTOR_PANEL) && eventTarget.closest('form')) {
            return;
        }

        this._breakEventExecution(event);

        const modal = eventTarget.closest(SELECTOR_MODAL);
        const panel = eventTarget.closest(SELECTOR_PANEL);
        const url = eventTarget.dataset[DATA_LINK];

        const targetPanelId = eventTarget.dataset[DATA_TARGET];
        const targetPanel: HTMLElement = (modal || document).querySelector(`${SELECTOR_PANEL}[data-id="${targetPanelId}"]`);

        this.load(url, targetPanel || panel, eventTarget);
    }

    protected handleSubmitEvent(event: Event): void {
        if (event.defaultPrevented) {
            return;
        }

        const { CLASS_IGNORE, DATA_TARGET, SELECTOR_PANEL } = this.options;
        const form = <HTMLFormElement>event.target;

        if (form.classList.contains(CLASS_IGNORE)) {
            return;
        }

        this._breakEventExecution(event);

        const panel = form.closest(SELECTOR_PANEL);
        const url = form.action;

        const targetPanelId = form.dataset[DATA_TARGET];
        const targetPanel: HTMLElement = document.querySelector(`${SELECTOR_PANEL}[data-id="${targetPanelId}"]`);

        this.load(url, targetPanel || panel, form);
    }

    protected handleFormLinkEvent(event: Event): void {
        if (event.defaultPrevented) {
            return;
        }

        this._breakEventExecution(event);

        const { DATA_FORM_ID, SELECTOR_MODAL } = this.options;
        const eventTarget = <HTMLElement>event.target;
        const modal = eventTarget.closest(SELECTOR_MODAL);
        const formId = eventTarget.dataset[DATA_FORM_ID];
        const form = (modal || document).querySelector(`#${formId}`);

        if (!form) {
            return;
        }

        this._dispatchEvent('submit', form);
    }

    protected handleRegisterEvent(event: CustomEvent, panel: HTMLElement): void {
        if (event.defaultPrevented) {
            return;
        }

        this.register(panel);
    }

    protected handleAjaxEvent(event: CustomEvent, eventData: { url: string, target: HTMLElement, source: HTMLElement, ajaxData?: any }): void {
        const {
            url, target, source, ajaxData,
        } = eventData;

        this.load(url, target, source, ajaxData);
    }

    protected handleRefreshEvent(event: CustomEvent, eventData: { target: HTMLElement }): void {
        if (event.defaultPrevented) {
            return;
        }

        const { target } = eventData;

        if (!target) {
            return;
        }

        const rawAjaxData = target.dataset[this.options.DATA_HISTORY];

        if (!rawAjaxData) {
            return;
        }

        const ajaxData = JSON.parse(rawAjaxData);

        this._breakEventExecution(event);
        this.load(ajaxData.url, target, target, ajaxData);
    }

    protected handleAfterLoadEvent(event: CustomEvent, eventData: AjaxPanelEventData): void {
        if (event.defaultPrevented) {
            return;
        }

        this._breakEventExecution(event);

        eventData.panel.querySelectorAll(this.options.SELECTOR_PANEL).forEach((childPanel: HTMLElement) => {
            this.register(childPanel);
        });
    }

    protected async load(url: string, target: HTMLElement, source: HTMLElement, customPayload?: any): Promise<void> {
        const payload = customPayload || this.getPayload(source, url);
        const eventPayload = this.createEventPayload(target, source, payload);
        const beforeLoadEvent = this.beforeLoadEvent(eventPayload);

        if (beforeLoadEvent.defaultPrevented) {
            return;
        }

        try {
            this.saveHistory(target, payload);
            const response = await this.request(payload.url, payload);
            this.processResponse(target, source, response);
            this.afterLoadEvent(eventPayload);
        } catch (error) {
            this.handleError(target, error);
        }
    }

    protected request(url: string, payload: any): Promise<AxiosResponse> {
        return axios({
            url,
            ...payload,
        });
    }

    protected saveHistory(target: HTMLElement, data: any): void {
        const { CLASS_IGNORE_HISTORY, DATA_HISTORY } = this.options;

        if (data.method === 'GET' && !target.classList.contains(CLASS_IGNORE_HISTORY)) {
            target.dataset[DATA_HISTORY] = JSON.stringify(data);
        }
    }

    protected processResponse(target: HTMLElement, source: HTMLElement, response: AxiosResponse): void {
        const { DATA_MODAL_CLOSE, HEADER_NO_LOGIN } = this.options;

        if (response.headers[HEADER_NO_LOGIN]) {
            this.reloadPage();
        } else if (source.dataset[DATA_MODAL_CLOSE] && !(response.data.indexOf('.modal--errors') === -1)) {
            this.closeModal();
        } else if (response) {
            this.insertContent(target, response.data);
        }
    }

    protected handleError(target: HTMLElement, error: unknown): void {
        console.debug(error); // eslint-disable-line no-console
        target.innerHTML = '<div style="margin:20px"><h1 style="margin-bottom:5px">An error occurred</h1><p>Please enable error logging for more information about this error.</p></div>';
    }

    protected beforeLoadEvent(eventPayload: AjaxPanelEventData): CustomEvent {
        return this._publish(B2BEvents.PANEL_BEFORE_LOAD, eventPayload);
    }

    protected afterLoadEvent(eventPayload: AjaxPanelEventData): void {
        this._publish(B2BEvents.PANEL_AFTER_LOAD, eventPayload);
    }

    protected createEventPayload(target, source, data): AjaxPanelEventData {
        return {
            panel: target,
            source,
            ajaxData: data,
        };
    }

    protected insertContent(target: HTMLElement, content: string): void {
        target.innerHTML = content;
    }

    protected closeModal(): void {
        $.modal.close();
    }

    protected getPayload(panel: HTMLElement, url: string): any {
        const { DATA_PAYLOAD } = this.options;
        const payload = {
            method: 'GET',
            data: {},
            url,
        };

        if (panel.tagName.toLowerCase() === 'form') {
            const form = <HTMLFormElement>panel;
            payload.method = form.method.toUpperCase() || 'GET';
            payload.data = parseForm(form);
            payload.url += parseForm(form, true);
        } else if (panel.dataset[DATA_PAYLOAD]) {
            payload.data = JSON.parse(panel.dataset[DATA_PAYLOAD]);
        }

        return payload;
    }

    protected reloadPage(): any {
        window.location.reload();
    }
}
