/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/autofocus.plugin
 */
const autofocus = {
  name: 'b2bAutoFocus',
  selector: 'body',
  initOnLoad: true,
  defaults: {
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
    autoFocusSelector: '[autofocus]',
  },
  init() {
    this._on(
      document,
      this.defaults.panelAfterLoadEvent,
      this.handlePanelAfterLoadEvent.bind(this),
    );
  },
  handlePanelAfterLoadEvent({ target }) {
    const autofocusInput = target.querySelector(this.defaults.autoFocusSelector);

    if (autofocusInput) {
      autofocusInput.focus();
    }
  },
};

export default autofocus;
