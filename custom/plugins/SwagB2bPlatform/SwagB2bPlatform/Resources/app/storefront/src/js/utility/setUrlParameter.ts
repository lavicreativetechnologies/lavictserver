import { B2BRegex } from '../enums';
import getUrlParameter from './getUrlParameter';

function replaceParameter(searchStr: string, parameter: string, regex: RegExp) {
    return searchStr.replace(regex, `$1${parameter}`);
}

function appendParameter(searchStr: string, parameter: string) {
    return `${searchStr}${searchStr.length ? '&' : '?'}${parameter}`;
}

export default function(key: string, value: string): void {
    const newParameter = `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
    const regex = new RegExp(B2BRegex.URL_PARAMETER.replace('%identifier%', key));
    let searchStr = window.location.search;

    searchStr = replaceParameter(searchStr, newParameter, regex);

    const replaced = !!getUrlParameter(key);

    if (!replaced) {
        searchStr = appendParameter(searchStr, newParameter);
    }

    window.location.search = searchStr;
}
