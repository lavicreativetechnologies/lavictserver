import { b2bAjaxPanelPluginLoader } from '../../jsPluginBaseObjects';
import { getPluginWrapper } from '../utility';

const overrides = {
  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        me.eachPluginName(eventData, (pluginName, $panel) => {
          me.destroyPlugin($panel, pluginName);
        });
      },
    );

    me._on(
      document,
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        me.eachPluginName(eventData, (pluginName, $panel) => {
          const { pluginObjects } = getPluginWrapper();

          if ($panel[pluginName]) {
            $panel[pluginName]();
          } else if (pluginObjects[pluginName]) {
            const plugin = pluginObjects[pluginName];
            plugin.$el = $panel;
            $panel[pluginName] = plugin.init // eslint-disable-line no-param-reassign
              .bind(pluginObjects[pluginName]);

            $panel[pluginName]();
          }
        });
      },
    );
  },
};

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-plugin-loader.plugin
 */
export default ({
  ...b2bAjaxPanelPluginLoader,
  ...overrides,
  name: 'b2bAjaxPanelPluginLoader',
  initOnLoad: true,
  selector: 'body',
});
