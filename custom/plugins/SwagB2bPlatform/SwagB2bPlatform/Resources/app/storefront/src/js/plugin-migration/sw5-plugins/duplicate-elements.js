import { b2bDuplicateElements } from '../../jsPluginBaseObjects';

const overrides = {

  onButtonClick() {
    const me = this;

    const $container = me.defaults.containerSelector;
    const $item = $(`${$container} div${me.defaults.itemSelector}:last-child`);
    const $clonedItem = $item.clone();

    $clonedItem.find('input').val('');
    $clonedItem.find('button').remove();

    $clonedItem.append('<div class="input-group-append"><button type="button" class="btn btn-outline-danger" onclick="this.parentNode.parentNode.remove()">✕</button></div>');

    $($container).append($clonedItem);
  },
};

export default ({
  ...b2bDuplicateElements,
  ...overrides,
  name: 'b2bDuplicateElements',
  initOnLoad: false,
});
