/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/submit-on-enter.plugin
 */
const submitOnEnter = {
  name: 'b2bFormSubmitOnEnter',
  selector: 'body',
  initOnLoad: true,
  defaults: {
    target: '[data-submit-enter]',
    event: 'keypress',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
  },
  init() {
    this._on(
      document,
      this.defaults.panelAfterLoadEvent,
      this.handlePanelAfterLoadEvent.bind(this),
    );
  },
  handlePanelAfterLoadEvent({ target }) {
    const form = target.querySelector(this.defaults.target);

    if (!form) {
      return;
    }

    const keypressHandler = (_event) => {
      if (_event.keyCode === 13) {
        _event.preventDefault();
        $(target).find('button[type=submit]').click();
      }
    };

    form.addEventListener(this.defaults.event, keypressHandler);
  },
};

export default submitOnEnter;
