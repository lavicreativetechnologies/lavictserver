import { AjaxPanelInterface, EventInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@EventInterface
@InitOnLoad
export default class AjaxPanelEditInlinePlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_EDIT_DISABLED: '.no--edit',
        SELECTOR_ROW: '[data-class="row"]',
        SELECTOR_QUANTITY_SELECT: '[data-display="edit-mode"]',
        SELECTOR_QUANTITY_VIEW: '[data-display="view-mode"]',
        SELECTOR_EDIT_INLINE: '[data-mode="edit"]',
        SELECTOR_SPACER: '[data-display="spacer-mode"]',
        CLASS_HIDDEN: 'is--hidden',
        ATTRIBUTE_HIDDEN: 'hidden',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._addGlobalEventListener('click', this.options.SELECTOR_EDIT_INLINE, this.handleClickEvent.bind(this));
    }

    protected handleClickEvent(event: Event): void {
        const actualClickElement = <HTMLElement> event.target;

        if (actualClickElement.matches(this.options.SELECTOR_EDIT_DISABLED)) {
            return;
        }

        this.processElements(event);
    }

    protected processElements(event: Event) {
        const {
            SELECTOR_ROW, SELECTOR_QUANTITY_SELECT, SELECTOR_QUANTITY_VIEW, ATTRIBUTE_HIDDEN,
        } = this.options;

        const button = <HTMLButtonElement> event.currentTarget;
        const row = button.closest(SELECTOR_ROW);
        const quantitySelect = row.querySelector(SELECTOR_QUANTITY_SELECT);
        const quantityView = row.querySelector(SELECTOR_QUANTITY_VIEW);
        const commentInput = row.nextElementSibling;

        button.setAttribute('disabled', 'disabled');

        quantitySelect.removeAttribute(ATTRIBUTE_HIDDEN);
        commentInput.removeAttribute(ATTRIBUTE_HIDDEN);
        quantityView.setAttribute(ATTRIBUTE_HIDDEN, '');

        const spacer = commentInput.nextElementSibling;
        spacer.parentElement.removeChild(spacer);

        const quantityInput = quantitySelect.querySelector('input');
        const submitBtn: HTMLButtonElement = commentInput.querySelector('button[type=submit]');
        quantityInput.focus();

        quantityInput.addEventListener('keypress', (_event: KeyboardEvent) => this.handleEnter(_event, submitBtn));
    }

    protected handleEnter(event: KeyboardEvent, submitTarget: HTMLButtonElement): void {
        if (event.keyCode !== 13) {
            return;
        }

        this._breakEventExecution(event);
        submitTarget.click();
    }
}
