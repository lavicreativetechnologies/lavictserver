/**
 * Disables all form elements on 'b2b--ajax-panel_loading' loading. Just add 'has--b2b-form' to the container
 */
export default {
  defaults: {
    triggerSelector: '.has--b2b-form',
  },

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const $panel = $(eventData.panel);

        if (!$panel.is(me.defaults.triggerSelector)) {
          return;
        }

        $panel.find('input, select, button, form').prop('disabled', true);
      },
    );
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
