import { B2BEvents } from '../enums';
import { AjaxPanelInterface } from '../decorator';

@AjaxPanelInterface
export default class AjaxPanelFormDisablePlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_FORM_ELEMENTS: 'input, select, button, form',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_BEFORE_LOAD, this.handlePanelBeforeLoadEvent.bind(this));
    }

    protected handlePanelBeforeLoadEvent(event: CustomEvent): void {
        const eventTarget = <HTMLElement>event.target;
        const formElements: NodeListOf<HTMLElement> = eventTarget.querySelectorAll(this.options.SELECTOR_FORM_ELEMENTS);

        this.disableElements(formElements);
    }

    protected disableElements(elements: NodeListOf<HTMLElement>): void {
        elements.forEach((element) => {
            element.setAttribute('disabled', 'disabled');
        });
    }
}
