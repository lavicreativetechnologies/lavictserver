import './polyfills';
import { initB2BPlugins, setupPluginBaseClass, waitForStorefront } from './utility';
import PluginWrapper from './plugin-migration/plugin-wrapper';

(async function initializeB2b(): Promise<void> {
    await waitForStorefront();
    setupPluginBaseClass();
    await initB2BPlugins();
    await new PluginWrapper();
}());
