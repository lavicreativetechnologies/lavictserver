import { b2bCollapsePanel } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be removed
 */
export default ({
  ...b2bCollapsePanel,
  name: 'b2bCollapsePanel',
  alias: 'swCollapsePanel',
  initOnLoad: false,
});
