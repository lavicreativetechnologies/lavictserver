/* istanbul ignore file */

const READY_STATE_COMPLETE = 'complete';
const READY_STATE_EVENT = 'readystatechange';

function isReady(): boolean {
    return document.readyState === READY_STATE_COMPLETE;
}

export default function(): Promise<void> {
    return new Promise((resolve): void => {
        if (isReady()) {
            resolve();
        }

        document.addEventListener(READY_STATE_EVENT, () => {
            if (isReady()) {
                resolve();
            }
        });
    });
}
