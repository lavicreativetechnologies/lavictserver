/**
 * A valid ajax panel trigger element (a, form) can issue a reload on a different panel.
 *
 * <form [...] data-ajax-panel-trigger-reload="another-panel-id">
 *
 * Will after submit trigger a reload of the other panel. If the panel id equals `_WINDOW_` the whole page is reloaded.
 */
export default {

  defaults: {
    panelSelector: '.b2b--ajax-panel',

    sourceTriggerDataKey: 'ajaxPanelTriggerReload',

    targetDataKey: 'reloadNext',

    windowReloadKey: '_WINDOW_',
  },

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const $source = $(eventData.source);
        const reloadTrigger = $source.data(me.defaults.sourceTriggerDataKey);

        if (!reloadTrigger) {
          return;
        }

        $panel.data(me.defaults.targetDataKey, reloadTrigger);
      },
    );

    me._on(
      document,
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const reloadTrigger = $panel.data(me.defaults.targetDataKey);

        if (!reloadTrigger) {
          return;
        }

        if (reloadTrigger.indexOf(me.defaults.windowReloadKey) !== -1) {
          window.location.reload();

          return;
        }

        $panel.removeData(me.defaults.targetDataKey);
        const panelIds = reloadTrigger.split(',');

        for (let i = 0; i < panelIds.length; i++) {
          $(`${me.defaults.panelSelector}[data-id=${panelIds[i]}]`).trigger('b2b--ajax-panel_refresh');
        }
      },
    );
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
