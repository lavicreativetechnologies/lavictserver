import { B2BEvents } from '../enums';
import { AjaxPanelInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@InitOnLoad
export default class AjaxPanelAclFormPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_TRIGGER: '.has--b2b-form',
        SELECTOR_DISABLE_ELEMENTS: 'form.is--b2b-acl-forbidden input, form.is--b2b-acl-forbidden select, form.is--b2b-acl-forbidden button',
        SELECTOR_FORBIDDEN_BUTTON: 'form.is--b2b-acl-forbidden button[type="submit"]',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelAfterLoadEvent(event: CustomEvent, eventData: AjaxPanelEventData): void {
        const { panel } = eventData;
        const { SELECTOR_DISABLE_ELEMENTS, SELECTOR_FORBIDDEN_BUTTON } = this.options;

        const inputElements: NodeListOf<HTMLElement> = panel.querySelectorAll(SELECTOR_DISABLE_ELEMENTS);
        const submitButtons: NodeListOf<HTMLElement> = panel.querySelectorAll(SELECTOR_FORBIDDEN_BUTTON);

        this.disableElements(inputElements);
        this.removeElements(submitButtons);
    }

    protected disableElements(elements: NodeListOf<HTMLElement>): void {
        elements.forEach((element) => {
            element.setAttribute('disabled', 'disabled');
        });
    }

    protected removeElements(elements: NodeListOf<HTMLElement>): void {
        elements.forEach((element) => {
            element.remove();
        });
    }
}
