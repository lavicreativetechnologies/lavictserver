import { b2bAjaxPanelOrderNumber } from '../../jsPluginBaseObjects';
import { publish } from '../../utility/index.ts';

const overrides = {
  onSaveOrderNumber(event) {
    event.preventDefault();
    event.stopPropagation();

    const me = this;
    const $target = $(event.target);
    const $row = $target.closest('tr');
    const $saveUrl = $row.attr('data-save-url');
    const $id = $row.attr('data-id');
    const $orderNumberInput = $row.find(me.defaults.inputSelector);
    const $customOrderNumber = $row.find(me.defaults.inputCustomOrderNumberSelector);

    $.publish('b2b/ordernumber/onSaveOrderNumber', [me, $target, $row, $saveUrl, $id, $orderNumberInput, $customOrderNumber]);

    if (!$saveUrl) {
      return;
    }

    const $orderNumberGrid = $('.b2b--ajax-panel[data-id="order-number-grid"]');

    publish('b2b--do-ajax-call', {
      source: $orderNumberGrid[0],
      target: $orderNumberGrid[0],
      url: $saveUrl,
      ajaxData: {
        url: $saveUrl,
        method: 'POST',
        data: this.getPayload($row),
        transformResponse: (data) => {
          $.publish('b2b/ordernumber/onSaveOrderNumber/success', [me, data, $target, $row, $saveUrl, $id, $orderNumberInput, $customOrderNumber]);

          return data;
        },
      },
    });
  },

  getPayload($row) {
    const me = this;

    const $id = $row.attr('data-id');
    const $orderNumberInput = $row.find(me.defaults.inputSelector);
    const $customOrderNumber = $row.find(me.defaults.inputCustomOrderNumberSelector);

    const $filterField = $('input[name="filters[all][field-name]"]').val();
    const $filterType = $('input[name="filters[all][type]"]').val();
    const $filterValue = $('input[name="filters[all][value]"]').val();
    const $sortBy = $('select[name="sort-by"] option[selected="selected"]').val();
    const $page = $('select[name="page"] option[selected="selected"]').val();
    const $csrf = $row.find('input[name="custom-csrf-input"]').val();

    return {
      id: $id,
      orderNumber: $($orderNumberInput).val(),
      customOrderNumber: $($customOrderNumber).val(),
      'filters[all][field-name]': $filterField,
      'filters[all][type]': $filterType,
      'filters[all][value]': $filterValue,
      'sort-by': $sortBy,
      page: $page,
      _csrf_token: $csrf,
    };
  },
};

export default ({
  ...b2bAjaxPanelOrderNumber,
  ...overrides,
  name: 'b2bAjaxPanelOrderNumber',
  initOnLoad: true,
  selector: '.module-ordernumber',
});
