/**
 * Disables row actions in grids
 */
export default {
  defaults: {},

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(document, 'b2b--ajax-panel_loading', $.proxy(me.disableForm, me));
  },

  disableForm(event, eventData) {
    const $source = $(eventData.source);

    if (!$source.is('.ajax-panel-link.is--b2b-acl-forbidden')) {
      return;
    }

    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
