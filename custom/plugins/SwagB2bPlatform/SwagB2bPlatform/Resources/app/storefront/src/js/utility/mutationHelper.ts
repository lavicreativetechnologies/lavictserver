export default function(target: HTMLElement, callback: () => void, execOnInit: boolean = false): void {
    const observer = new MutationObserver((mutations: MutationRecord[]) => {
        mutations.forEach(callback);
    });

    const config = {
        childList: true,
    };

    observer.observe(target, config);

    if (execOnInit) {
        callback();
    }
}
