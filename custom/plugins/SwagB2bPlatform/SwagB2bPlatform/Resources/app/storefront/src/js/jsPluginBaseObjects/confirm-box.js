/**
 * global: StateManager
 *
 * Additional modal to add a confirm box
 *
 * The open function has a parameter which can be null or a handler with the function cancel and confirm.
 * If the handler is not null, elements with the classes
 *      "b2b--cancel-action" and
 *      "b2b--confirm-action"
 * in the confirm box trigger the handlers functions by a click event.
 */

export default {
  _$modalBox: null,

  _$content: null,

  _$handler: null,

  _$previousOverlay: null,

  defaults: {
    cancelModalSelector: '.b2b--cancel-action',

    confirmModalSelector: '.b2b--confirm-action',
  },

  onButtonCancel() {
    const me = this;

    if (!me._$handler) {
      return;
    }

    me._$handler.cancel();
  },

  onButtonConfirm() {
    const me = this;

    if (!me._$handler) {
      return;
    }

    me._$handler.confirm();
  },

  open(content, handler) {
    const me = this;
    let $modalBox = me._$modalBox;
    const opts = me.defaults;

    me._$handler = handler;

    if (!$modalBox) {
      me._$previousOverlay = $.overlay.overlay;

      $.overlay.open($.extend({}, {
        closeOnClick: opts.closeOnOverlay,
        onClose: $.proxy(me.onOverlayClose, me),
        overlayCls: 'js--overlay b2bConfirmOverlay',
      }));

      me.initModalBox();
      me.registerEvents();

      $modalBox = me._$modalBox;
    }

    $modalBox.toggleClass('sizing--auto', false);
    $modalBox.toggleClass('sizing--fixed', false);
    $modalBox.toggleClass('sizing--content', true);
    $modalBox.toggleClass('no--header', true);

    $modalBox.css('width', 600);
    $modalBox.css('height', 'auto');
    $modalBox.css('display', 'block');

    me._$content.html(content);

    $modalBox.find(me.defaults.confirmModalSelector).click($.proxy(me.onButtonConfirm, me));
    $modalBox.find(me.defaults.cancelModalSelector).click($.proxy(me.onButtonCancel, me));

    me.setTransition({ opacity: 1 }, 500, 'linear');

    $('html').addClass('no--scroll');

    return me;
  },

  close() {
    const me = this;
    const $modalBox = me._$modalBox;

    $.overlay.close();
    $('html').removeClass('no--scroll');

    if ($modalBox !== null) {
      me.setTransition({
        opacity: 0,
      }, 500, 'linear', () => {
        $modalBox.css('display', 'none');
        $modalBox.remove();
        me._$content = null;
        me._$modalBox = null;
        me._$handler = null;
      });
    }

    $.overlay.overlay = me._$previousOverlay;
    me._$previousOverlay = null;
  },

  setTransition(css, duration, animation, callback) {
    const me = this;
    const $modalBox = me._$modalBox;

    if (!$.support.transition) {
      $modalBox.stop(true).animate(css, duration, animation, callback);

      return;
    }

    $modalBox.stop(true).transition(css, duration, animation, callback);
  },

  initModalBox() {
    const me = this;

    me._$modalBox = $('<div>', {
      class: 'js--modal modal--confirm b2bConfirmModal',
    });

    me._$content = $('<div>', {
      class: 'content',
    }).appendTo(me._$modalBox);

    $('body').append(me._$modalBox);
  },

  registerEvents() {
    const me = this;
    const $window = $(window);

    $window.on('keydown.modal', $.proxy(me.onKeyDown, me));

    StateManager.registerListener({
      state: 'xs',
      enter() {
        me._$modalBox.addClass('is--fullscreen');
      },
      exit() {
        me._$modalBox.removeClass('is--fullscreen');
      },
    });
  },

  onKeyDown(event) {
    const me = this;
    const keyCode = event.which;
    const keys = [27];
    const len = keys.length;
    let i = 0;

    for (; i < len; i++) {
      if (keys[i] === keyCode) {
        me.close();
      }
    }
  },

  center() {
    const me = this;
    const $modalBox = me._$modalBox;

    $modalBox.css('top', ($(window).height() - $modalBox.height()) / 2);
  },

  onOverlayClose() {
    const me = this;
    me.close();
  },
};
