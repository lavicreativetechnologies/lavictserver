/**
 * @deprecated tag:v4.5.0 - Will be removed
 */
export default ({
  name: 'b2bMobileMenuScroller',
  selector: 'body',
  initOnLoad: true,
  defaults: {
    selectorScrollTarget: '.navigation--menu',
    selectorArrows: '.b2b--menu-scroller',
    selectorArrowLeft: '.b2b--menu-scroller--left',
    selectorArrowRight: '.b2b--menu-scroller--right',
    clickScrollDistance: 120,
    touchScrollDistance: 10,
    touchIntervalDelay: 20,
  },
  touchInterval: -1,
  scrollTarget: null,
  arrows: null,
  arrowLeft: null,
  arrowRight: null,
  init() {
    this.getElements();

    if (!this.scrollTarget || this.arrows.length !== 2 || !this.arrowLeft || !this.arrowRight) {
      return;
    }

    this.applyEventListener();
    this.scrollIntoView();
  },
  getElements() {
    this.scrollTarget = document.querySelector(this.defaults.selectorScrollTarget);
    this.arrows = document.querySelectorAll(this.defaults.selectorArrows);
    this.arrowLeft = document.querySelector(this.defaults.selectorArrowLeft);
    this.arrowRight = document.querySelector(this.defaults.selectorArrowRight);
  },
  applyEventListener() {
    this.arrowLeft.addEventListener('click', () => {
      this.clickHandler('left');
    });
    this.arrowRight.addEventListener('click', () => {
      this.clickHandler('right');
    });
    this.arrowLeft.addEventListener('touchstart', () => {
      this.startHandler('left');
    });
    this.arrowRight.addEventListener('touchstart', () => {
      this.startHandler('right');
    });
    this.arrowLeft.addEventListener('mousedown', () => {
      this.startHandler('left');
    });
    this.arrowRight.addEventListener('mousedown', () => {
      this.startHandler('right');
    });
    window.addEventListener('touchend', this.endHandler);
    window.addEventListener('touchcancel', this.endHandler);
    window.addEventListener('mouseup', this.endHandler);
    window.addEventListener('resize', this.scrollIntoView);
  },
  clickHandler(direction, behavior = 'smooth') {
    this.scrollTarget.scrollBy({
      top: 0,
      left: (this.defaults.clickScrollDistance * (direction === 'left' ? -1 : 1)),
      behavior,
    });
  },
  startHandler(direction) {
    this.endHandler();
    this.touchInterval = setInterval(() => {
      this.scrollTarget.scrollBy({
        top: 0,
        left: (this.defaults.touchScrollDistance * (direction === 'left' ? -1 : 1)),
      });
    }, this.defaults.touchIntervalDelay);
  },
  endHandler() {
    clearInterval(this.touchInterval);
  },
  scrollIntoView() {
    if (!this.scrollTarget) {
      return;
    }

    const activeElement = this.getActiveElement();

    if (activeElement && this.isHorizontallyInViewport(activeElement) > 0) {
      this.clickHandler('right', 'auto');
    }
  },
  getActiveElement() {
    return this.scrollTarget.querySelector('.is--active');
  },
  isHorizontallyInViewport(element) {
    const bounding = element.getBoundingClientRect();

    return (bounding.right - ((window.innerWidth || document.documentElement.clientWidth) - 40));
  },
});
