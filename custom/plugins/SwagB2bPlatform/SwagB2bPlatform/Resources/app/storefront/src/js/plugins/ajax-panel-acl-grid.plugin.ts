import { B2BEvents } from '../enums';
import { AjaxPanelInterface, EventInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@EventInterface
@InitOnLoad
export default class AjaxPanelAclGridPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_FORBIDDEN: '.ajax-panel-link.is--b2b-acl-forbidden',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelAfterLoadEvent(event: CustomEvent, eventData: AjaxPanelEventData): void {
        const { source } = eventData;

        if (!source.matches(this.options.SELECTOR_FORBIDDEN)) {
            return;
        }

        this.breakEventExecution(event);
    }

    protected breakEventExecution(event): void {
        this._breakEventExecution(event);
    }
}
