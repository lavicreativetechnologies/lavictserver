import { b2bAjaxPanelTriggerReload } from '../../jsPluginBaseObjects';
import { publish, subscribe } from '../../utility/index.ts';

const overrides = {
  init() {
    const me = this;

    this.applyDataAttributes();

    subscribe(
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const $source = $(eventData.source);
        const reloadTrigger = $source.data(me.defaults.sourceTriggerDataKey);

        if (!reloadTrigger) {
          return;
        }

        $panel.data(me.defaults.targetDataKey, reloadTrigger);
      },
    );

    subscribe(
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const reloadTrigger = $panel.data(me.defaults.targetDataKey);

        if (!reloadTrigger) {
          return;
        }

        if (reloadTrigger.indexOf(me.defaults.windowReloadKey) !== -1) {
          window.location.reload();

          return;
        }

        $panel.removeData(me.defaults.targetDataKey);
        const panelIds = reloadTrigger.split(',');

        for (let i = 0; i < panelIds.length; i++) {
          const $target = $(`${me.defaults.panelSelector}[data-id="${panelIds[i]}"]`);

          publish('b2b--ajax-panel_refresh', {
            target: $target[0],
          });
        }
      },
    );
  },
};

/**
 * @deprecated tag:v4.6.0 - Will be replaced by src/js/plugins/ajax-panel-trigger-reload.plugin
 */
export default ({
  ...b2bAjaxPanelTriggerReload,
  ...overrides,
  name: 'b2bAjaxPanelTriggerReload',
  initOnLoad: true,
  selector: 'body',
});
