import { B2BEvents } from '../enums';
import { AjaxPanelInterface } from '../decorator';

@AjaxPanelInterface
export default class AutofocusPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_AUTOFOCUS: '[autofocus]',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoadEvent.bind(this));
    }

    protected handlePanelAfterLoadEvent(event: CustomEvent): void {
        const eventTarget = <HTMLElement>event.target;
        const autofocusInput = eventTarget.querySelector(this.options.SELECTOR_AUTOFOCUS);

        if (!autofocusInput) {
            return;
        }

        autofocusInput.focus();
    }
}
