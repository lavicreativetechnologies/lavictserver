import 'whatwg-fetch';
import 'promise-polyfill/src/polyfill';
import 'classlist-polyfill';
import URLSearchParams from '@ungap/url-search-params';

import './closest';
import './toggleAttribute';
import './composedPath';

window.URLSearchParams = window.URLSearchParams || URLSearchParams;
