/* eslint-disable */
function _slideUp(target, speed) {
  /* eslint-disable-next-line */
  return new Promise(async resolve => {
    const height = target.offsetHeight;

    target.style.visibility = 'visible';
    target.style.height = `${height}px`;
    target.style.transition = `height ${speed}ms ease, visibility ${(speed/6)}ms ease`;

    await new Promise((_resolve) => {
      setTimeout(_resolve);
    });

    target.style.height = 0;
    target.style.visibility = 'hidden';

    await new Promise((_resolve) => {
      setTimeout(_resolve, speed);
    });

    resolve();
  });
}

export default async function (speed = 400, callback) {
  const target = this.length > 0 ? this[0] : false;

  if (target) {
    await _slideUp(target, speed);
  }

  if (callback) {
    callback();
  }

  return this;
}
/* eslint-enable */
