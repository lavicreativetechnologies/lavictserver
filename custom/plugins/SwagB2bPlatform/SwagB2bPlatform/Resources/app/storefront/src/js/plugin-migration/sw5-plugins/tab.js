import { b2bTab } from '../../jsPluginBaseObjects';
import { publish } from '../../utility/index.ts';

const overrides = {
  disableTabs(event, deletedRoleElement) {
    const me = this;
    const tabMenu = $(me.defaults.tabMenuSelector);
    const ajaxPanel = tabMenu.find('.tab--container.b2b--ajax-panel');
    const roleId = tabMenu.find('.b2b--tree-selection-aware').first().attr('value');

    if (roleId !== deletedRoleElement.id) {
      return;
    }

    tabMenu.find('.tab--link').each(function disableTab() {
      $(this).attr('disabled', 'disabled').removeClass('is--active');
    });

    publish(
      'b2b--do-ajax-call',
      {
        url: tabMenu.data('default-tab-url'),
        panel: ajaxPanel[0],
        target: ajaxPanel[0],
      },
    );
  },
};

export default ({
  ...b2bTab,
  ...overrides,
  name: 'b2bTab',
  initOnLoad: true,
  selector: 'body',
});
