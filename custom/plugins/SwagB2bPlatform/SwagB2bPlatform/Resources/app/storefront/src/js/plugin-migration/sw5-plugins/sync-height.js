import { b2bSyncHeight } from '../../jsPluginBaseObjects';
import { subscribe } from '../../utility/index.ts';

const overrides = {
  addListeners() {
    const me = this;

    subscribe('b2b--ajax-panel_loaded', $.proxy(me.syncHeight, me));
  },
};

export default ({
  ...b2bSyncHeight,
  ...overrides,
  name: 'b2bSyncHeight',
  initOnLoad: true,
  selector: 'body',
});
