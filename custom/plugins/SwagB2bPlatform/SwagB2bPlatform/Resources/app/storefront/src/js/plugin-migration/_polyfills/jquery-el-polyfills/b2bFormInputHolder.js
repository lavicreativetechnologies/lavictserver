import { getPluginWrapper } from '../../utility';

export default function () {
  const { formInputHolder } = getPluginWrapper().pluginObjects;
  const { selector } = formInputHolder;

  const targets = document.querySelectorAll(selector);

  targets.forEach((target) => {
    this.$el = $(target);
    formInputHolder.init();
  });

  return this;
}
