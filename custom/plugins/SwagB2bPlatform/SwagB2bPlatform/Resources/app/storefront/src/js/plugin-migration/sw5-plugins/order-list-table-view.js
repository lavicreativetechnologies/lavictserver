import { mutationHelper } from '../../utility/index.ts';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/order-list-table-view.plugin
 */
export default ({
  name: 'b2bOrderListTableView',
  selector: '.b2b--orderlist-tableview',
  initOnLoad: true,
  defaults: {
    selectorMutationTarget: '.cms-element-product-listing',
    selectorEnterSubmit: '[data-submit-enter]',
    selectorSubmitTarget: '.cart-form',
    selectorSubmitBtn: '.cart-form .btn',
    selectorSelect: '.b2b--orderlist-dropdown',
    selectorQuantityInput: '.b2b--orderlist-tableview .fast-order-inputs .form-control',
    selectorMessageContainer: '.b2b--message-container',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
    panelBeforeLoadEvent: 'b2b--ajax-panel_loading',
    keyEnter: 13,
  },
  el: null,
  loading: false,
  init() {
    const mutationTarget = document.querySelector(this.defaults.selectorMutationTarget);

    if (!mutationTarget) {
      return;
    }

    mutationHelper(mutationTarget, this.initTableView.bind(this), true);

    this._on(
      document,
      this.defaults.panelAfterLoadEvent,
      this.handlePanelReload.bind(this),
    );
  },
  initTableView() {
    this.el = document.querySelector(this.defaults.selectorEnterSubmit);

    if (!this.el) {
      return;
    }

    const targetEnterSubmit = document.querySelector(this.defaults.selectorEnterSubmit);

    if (targetEnterSubmit) {
      targetEnterSubmit.addEventListener('keypress', this.handleKeyboardEvent.bind(this));
    }

    const quantityInputs = this.el.querySelectorAll(this.defaults.selectorQuantityInput);

    quantityInputs.forEach((input) => {
      input.addEventListener('input', this.handleQuantityChange.bind(this));
    });
  },
  handleKeyboardEvent(event) {
    const { keyCode } = event;

    if (keyCode !== this.defaults.keyEnter) {
      return;
    }

    event.preventDefault();
    const btn = document.querySelector(this.defaults.selectorSubmitBtn);

    if (btn && this.hasValidRows()) {
      btn.click();
    }
  },
  handleQuantityChange() {
    this.setSubmitState();
  },
  setSubmitState() {
    const btn = document.querySelector(this.defaults.selectorSubmitBtn);
    const select = document.querySelector(this.defaults.selectorSelect);
    const state = this.hasValidRows();

    if (state) {
      btn.removeAttribute('disabled');
      select.removeAttribute('disabled');
    } else {
      btn.setAttribute('disabled', 'disabled');
      select.setAttribute('disabled', 'disabled');
    }
  },
  hasValidRows() {
    let hasQuantity = false;

    if (!this.el) {
      return hasQuantity;
    }

    const inputs = this.el.querySelectorAll(this.defaults.selectorQuantityInput);

    inputs.forEach(({ value }) => {
      const _value = parseInt(value, 10);
      if (_value > 0) {
        hasQuantity = true;
      }
    });

    return hasQuantity;
  },
  clearAfterSubmit() {
    const inputs = document.querySelectorAll(this.defaults.selectorQuantityInput);

    inputs.forEach((input) => {
      input.value = ''; // eslint-disable-line no-param-reassign
    });
  },
  handlePanelReload(event, { source }) { // eslint-disable-line no-unused-vars
    if (source[0] && source[0].classList.contains('cart-form')) {
      this.clearAfterSubmit();
    }

    this.setSubmitState();
  },
});
