import { B2BEvents } from '../enums';
import { AjaxPanelInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@InitOnLoad
export default class AjaxPanelLoadingPlugin extends window.PluginBaseClass {
    public static options = {
        CLASS_LOADING_SPINNER: 'content--loading',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_BEFORE_LOAD, this.handlePanelBeforeLoad.bind(this));
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoad.bind(this));
    }

    protected handlePanelBeforeLoad(event: Event, eventData: AjaxPanelEventData): void {
        this.insertLoadingSpinner(eventData.panel);
    }

    protected handlePanelAfterLoad(event: Event, eventData: AjaxPanelEventData): void {
        this.removeLoadingSpinner(eventData.panel);
    }

    protected insertLoadingSpinner(element: HTMLElement): void {
        element.insertAdjacentHTML(
            'beforeend',
            `<div class="${this.options.CLASS_LOADING_SPINNER}"><span></span><span></span><span></span><span></span></div>`,
        );
    }

    protected removeLoadingSpinner(element: HTMLElement): void {
        const lastChild = element.lastElementChild;

        if (lastChild.classList.value === this.options.CLASS_LOADING_SPINNER) {
            lastChild.remove();
        }
    }
}
