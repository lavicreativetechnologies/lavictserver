export default function getEventName(event) {
  if (!event || (typeof event !== 'string' && !event.type)) {
    return '';
  }

  const suffix = this.eventSuffix;
  let eventType = event;

  if (typeof event === 'object') {
    eventType = event.type;
  }

  const parts = eventType.split(' ');
  const len = parts.length;

  for (let i = 0; i < len; i++) {
    parts[i] += suffix;
  }

  return parts.join(' ');
}
