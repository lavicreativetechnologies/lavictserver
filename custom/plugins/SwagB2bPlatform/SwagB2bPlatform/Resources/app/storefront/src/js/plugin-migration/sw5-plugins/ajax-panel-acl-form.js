import { b2bAjaxPanelAclForm } from '../../jsPluginBaseObjects';

/**
 * @deprecated tag:v4.5.0 - Will be replaced by src/js/plugins/ajax-panel-acl-form.plugin
 */
export default ({
  ...b2bAjaxPanelAclForm,
  name: 'b2bAjaxPanelAclForm',
  initOnLoad: true,
  selector: 'body',
});
