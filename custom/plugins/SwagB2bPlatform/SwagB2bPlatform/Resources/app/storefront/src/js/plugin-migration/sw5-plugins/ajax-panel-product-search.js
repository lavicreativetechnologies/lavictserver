import { b2bAjaxPanelProductSearch } from '../../jsPluginBaseObjects';

const overrides = {
  onSelectElement(event) {
    const me = this;
    const $currentTarget = $(event.currentTarget);
    const $searchContainer = $currentTarget.closest(me.defaults.searchContainer);
    const $currentElement = $searchContainer.find('.is--active');

    if (!$currentElement.length || !me.defaults.resultsActive) {
      return;
    }

    event.preventDefault();

    const orderNumber = $currentElement.find('span').html();
    $currentTarget.val(orderNumber);
    $currentTarget[0].dispatchEvent(new Event('change', { bubbles: true }));

    me.setQuantityInput($searchContainer, $currentElement);

    me.hideResults();
  },

  onClickSelectElement(event) {
    const me = this;
    const $currentTarget = $(event.currentTarget);
    const $searchContainer = $currentTarget.closest(me.defaults.searchContainer);
    const $input = $searchContainer.find('input');

    if (!$currentTarget.length) {
      return;
    }

    const orderNumber = $currentTarget.find('span').html();
    $input.val(orderNumber);
    $input[0].dispatchEvent(new Event('change', { bubbles: true }));

    me.setQuantityInput($searchContainer, $currentTarget);

    me.hideResults();
  },
};

export default ({
  ...b2bAjaxPanelProductSearch,
  ...overrides,
  name: 'b2bAjaxPanelProductSearch',
  initOnLoad: true,
  selector: 'body',
});
