export default {
  defaults: {
    rowSelector: '[data-class="row"]',
  },

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'click',
      '*[data-mode="edit"]:not(.is--b2b-acl-forbidden)',
      (event) => {
        $('*[data-b2b-form-input-holder="true"]').b2bFormInputHolder();

        const $button = $(event.currentTarget);
        const $row = $button.closest(me.defaults.rowSelector);
        const $quantitySelect = $row.find('[data-display="edit-mode"]');
        const $quantityView = $row.find('[data-display="view-mode"]');
        const $commentInput = $row.next('tr');
        const $actualClickElement = $(event.target);

        if ($actualClickElement.hasClass('no--edit')) {
          return;
        }

        $button.attr('disabled', 'disabled');

        $quantitySelect.removeClass('is--hidden');
        $quantityView.addClass('is--hidden');
        $commentInput.removeClass('is--hidden');

        const $spacer = $commentInput.next('[data-display="spacer-mode"]');
        $spacer.remove();
      },
    );
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
