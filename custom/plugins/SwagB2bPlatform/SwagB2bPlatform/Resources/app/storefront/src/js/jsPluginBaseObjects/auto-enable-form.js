/**
 * Add `is--auto-enable-form` class to a form element to enable the submit button when every item has a value
 *
 * On change triggers a b2b_auto_enable_form event
 */
export default {
  defaults: {
    formSelector: '.is--auto-enable-form',
  },

  init() {
    const me = this;

    this.applyDataAttributes();

    me._on(
      document,
      'change',
      `${me.defaults.formSelector} input,${me.defaults.formSelector} select`,
      me.handleChangeEvent,
    );
  },

  handleChangeEvent(event) {
    const $form = $(this).closest('form');
    let enable = true;
    const $submit = $form.find('button:submit');

    if (event.isDefaultPrevented()) {
      return;
    }

    $form.find('input, select').each(function () {
      const $formItem = $(this);

      if (!$formItem.val()) {
        enable = false;
      }
    });

    $submit.attr('disabled', !enable);
    $form.trigger('b2b_auto_enable_form');
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
