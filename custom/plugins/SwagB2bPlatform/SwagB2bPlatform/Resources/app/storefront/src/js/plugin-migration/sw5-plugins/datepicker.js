import { b2bDatepicker } from '../../jsPluginBaseObjects';
import { subscribe } from '../../utility/index.ts';

const _init = b2bDatepicker.init;

const overrides = {
  defaults: {
    ...b2bDatepicker.defaults,
    rangeStartInput: 'from',
    rangeEndInput: 'to',
    panelAfterLoadEvent: 'b2b--ajax-panel_loaded',
    inputSelector: '.datepicker',
  },
  init() {
    subscribe(this.defaults.panelAfterLoadEvent, $.proxy(this.onAfterLoadEvent, this));
  },
  onAfterLoadEvent() {
    const me = this;
    me.$el = $(this.defaults.inputSelector);

    if (!me.$el.length) {
      return;
    }

    _init.bind(this)();
    const config = $.extend({}, me.opts);

    if (me.opts.mode === 'multiple' && me.opts.multiDateSeparator !== null) {
      me.$el.val(me.convertMultiSeparatorToFlatpickr(me.$el.val()));
    }

    if (me.opts.enabledDates !== null) {
      if (typeof me.opts.enabledDates === 'string') {
        me.opts.enabledDates = me.opts.enabledDates.split(',');
      }

      config.enable = me.opts.enabledDates;
    }

    config.onReady = $.proxy(me.onPickerReady, me);
    config.onChange = $.proxy(me.onPickerChange, me);
    config.onOpen = $.proxy(me.onPickerOpen, me);
    config.onClose = $.proxy(me.onPickerClose, me);
    config.defaultDate = me.$el.data.defaultDate;

    for (let i = 0; i < me.$el.length; i++) {
      const el = $(me.$el[i]);

      me.flatpickr = el.flatpickr(config);

      if (me.opts.mode === 'multiple' && me.opts.multiDateSeparator !== null) {
        el.val(me.convertMultiSeparator(el.val()));
      }

      if (me.opts.mode === 'range') {
        me.setDatePickerValFromInputs();
        me.setStartInputVal();
        me.setEndInputVal();
      }
    }

    $.publish('plugin/swDatePicker/onInitFlatpickr', [me, config]);
  },
};

export default ({
  ...b2bDatepicker,
  ...overrides,
  name: 'b2bDatepicker',
  selector: 'body',
  initOnLoad: true,
});
