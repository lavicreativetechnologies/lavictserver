import getPluginWrapper from './getPluginWrapper';

let pluginWrapper = null;

function getInitializedPlugins() {
  const { initialized } = pluginWrapper;
  const initializedList = [];

  Object.keys(initialized).forEach((name) => {
    if (initialized[name]) {
      initializedList.push(name);
    }
  });

  return initializedList;
}

function reInitPlugins(list) {
  list.forEach((pluginName) => {
    const plugin = pluginWrapper.pluginObjects[pluginName];

    plugin._overrideEl(plugin.selector);

    plugin._update();
  });
}

export default function () {
  pluginWrapper = getPluginWrapper();
  const initializedPlugins = getInitializedPlugins();

  reInitPlugins(initializedPlugins);
}
