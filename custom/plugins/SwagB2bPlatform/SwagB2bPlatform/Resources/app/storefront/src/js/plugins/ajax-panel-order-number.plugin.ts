import axios, { AxiosRequestConfig } from 'axios';
import { AjaxPanelInterface, EventInterface, InitOnLoad } from '../decorator';
import { B2BEvents } from '../enums';

interface OrderNumberSavePayload {
    id: string;
    orderNumber: string;
    customOrderNumber: string;
    'filters[all][field-name]': string;
    'filters[all][type]': string;
    'filters[all][value]': string;
    'sort-by': string;
    page: string;
    _csrf_token: string;
}

@AjaxPanelInterface
@EventInterface
@InitOnLoad
export default class AjaxPanelOrderNumberPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_MODULE: '.module-ordernumber',
        SELECTOR_TABLE: '.table--ordernumber',
        SELECTOR_PANEL: '.b2b--ajax-panel[data-id="order-number-grid"]',
        SELECTOR_ROW: '[data-class="row"]',
        SELECTOR_INPUT_ORDER_NUMBER: '.input-ordernumber',
        SELECTOR_INPUT_CUSTOM_ORDER_NUMBER: '.input-customordernumber',
        SELECTOR_BTN_SAVE: '.btn--edit',
        SELECTOR_CSRF: 'input[name="custom-csrf-input"]',
    };

    public init(): void {
        this.registerEventListener();
    }

    protected registerEventListener(): void {
        const {
            SELECTOR_MODULE, SELECTOR_BTN_SAVE, SELECTOR_TABLE, SELECTOR_INPUT_ORDER_NUMBER,
        } = this.options;

        this._addGlobalEventListener('change', `${SELECTOR_TABLE} input`, this.handleTableInput.bind(this));
        this._addGlobalEventListener('change', `${SELECTOR_TABLE} ${SELECTOR_INPUT_ORDER_NUMBER}`, this.handleInputOrderNumber.bind(this));
        this._addGlobalEventListener('keydown', `${SELECTOR_TABLE} input`, this.handleEnter.bind(this));
        this._addGlobalEventListener('click', `${SELECTOR_MODULE} ${SELECTOR_BTN_SAVE}`, this.handleButtonEdit.bind(this));
    }

    protected handleTableInput(event: InputEvent): void {
        const { SELECTOR_TABLE, SELECTOR_INPUT_ORDER_NUMBER, SELECTOR_INPUT_CUSTOM_ORDER_NUMBER } = this.options;

        const eventTarget = <HTMLInputElement> event.target;
        const row = eventTarget.closest('tr');
        const tableOrderNumber = eventTarget.closest(SELECTOR_TABLE);

        if (!row.dataset.saveUrl) {
            return;
        }

        const rows = tableOrderNumber.querySelectorAll('tr');
        const lastRow = rows[rows.length - 1];
        const inputOrderNumber = lastRow.querySelector(SELECTOR_INPUT_ORDER_NUMBER);
        const inputCustomOrderNumber = lastRow.querySelector(SELECTOR_INPUT_CUSTOM_ORDER_NUMBER);

        if (!inputOrderNumber.value && !inputCustomOrderNumber.value) {
            return;
        }

        tableOrderNumber.appendChild(this.cloneLastRow(lastRow));
    }

    protected cloneLastRow(row: HTMLElement): HTMLElement {
        const { SELECTOR_INPUT_ORDER_NUMBER, SELECTOR_INPUT_CUSTOM_ORDER_NUMBER, SELECTOR_BTN_SAVE } = this.options;
        const newRow = <HTMLElement> row.cloneNode(true);
        newRow.querySelector('.col-headline').innerHTML = '';
        newRow.querySelector(SELECTOR_INPUT_ORDER_NUMBER).value = '';
        newRow.querySelector(SELECTOR_INPUT_CUSTOM_ORDER_NUMBER).value = '';
        const searchResults = newRow.querySelector('.b2b--search-results');

        const saveButton = row.querySelector(SELECTOR_BTN_SAVE);
        saveButton.removeAttribute('hidden');

        if (searchResults) {
            searchResults.parentElement.removeChild(searchResults);
        }

        return newRow;
    }

    protected handleInputOrderNumber(event: InputEvent): void {
        const eventTarget = <HTMLInputElement> event.target;
        const row = eventTarget.closest('tr');
        const colHeadline = row.querySelector('.col-headline');
        const elProductUrl: HTMLElement = document.querySelector('[data-product-url]');

        if (!elProductUrl) {
            return;
        }

        const config: AxiosRequestConfig = {
            url: elProductUrl.dataset.productUrl,
            method: 'GET',
            params: {
                orderNumber: eventTarget.value,
            },
            transformResponse: (data) => {
                colHeadline.innerHTML = data;
            },
        };

        axios(config);
    }

    protected handleButtonEdit(event: Event): void {
        this._breakEventExecution(event);
        const eventTarget = <HTMLElement> event.target;

        const panel = document.querySelector(this.options.SELECTOR_PANEL);
        const row = eventTarget.closest('tr');

        const config: AxiosRequestConfig = {
            url: row.dataset.saveUrl,
            method: 'POST',
            data: this.getSavePayload(row),
        };

        this._publish(B2BEvents.ORDERNUMBER_SAVE);

        this._publish(B2BEvents.PANEL_AJAX, {
            url: config.url,
            target: panel,
            source: panel,
            ajaxData: config,
        });
    }

    protected getSavePayload(tableRow: HTMLElement): OrderNumberSavePayload {
        const { SELECTOR_INPUT_ORDER_NUMBER, SELECTOR_INPUT_CUSTOM_ORDER_NUMBER, SELECTOR_CSRF } = this.options;

        const filterField: HTMLInputElement = document.querySelector('input[name="filters[all][field-name]"]');
        const filterType: HTMLInputElement = document.querySelector('input[name="filters[all][type]"]');
        const filterValue: HTMLInputElement = document.querySelector('input[name="filters[all][value]"]');
        const filterSort: HTMLSelectElement = document.querySelector('select[name="sort-by"] option[selected="selected"]');
        const filterPage: HTMLSelectElement = document.querySelector('select[name="page"] option[selected="selected"]');

        return {
            id: tableRow.dataset.id,
            orderNumber: tableRow.querySelector(SELECTOR_INPUT_ORDER_NUMBER).value,
            customOrderNumber: tableRow.querySelector(SELECTOR_INPUT_CUSTOM_ORDER_NUMBER).value,
            _csrf_token: tableRow.querySelector(SELECTOR_CSRF).value,
            'filters[all][field-name]': filterField.value,
            'filters[all][type]': filterType.value,
            'filters[all][value]': filterValue.value,
            'sort-by': filterSort.value,
            page: filterPage.value,
        };
    }

    protected handleEnter(event: KeyboardEvent): void {
        if (event.key !== 'Enter') {
            return;
        }

        const eventTarget = <HTMLElement> event.target;
        const row = eventTarget.closest('tr');
        const inputOrderNumber = row.querySelector(this.options.SELECTOR_INPUT_ORDER_NUMBER);
        const inputCustomOrderNumber = row.querySelector(this.options.SELECTOR_INPUT_CUSTOM_ORDER_NUMBER);

        if (!inputOrderNumber.value || !inputCustomOrderNumber.value) {
            return;
        }

        const btn = row.querySelector(this.options.SELECTOR_BTN_SAVE);
        this._dispatchEvent('click', btn);
    }
}
