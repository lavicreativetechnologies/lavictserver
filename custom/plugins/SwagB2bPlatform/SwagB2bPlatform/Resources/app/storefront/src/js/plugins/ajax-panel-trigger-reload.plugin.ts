import { B2BEvents } from '../enums';
import { AjaxPanelInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@InitOnLoad
export default class AjaxPanelTriggerReloadPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_PANEL: '.b2b--ajax-panel',
        DATA_RELOAD_TARGET: 'ajaxPanelTriggerReload',
        RELOAD_TARGET_WINDOW: '_WINDOW_',
    };

    public init(): void {
        this.applyEventListeners();
    }

    protected applyEventListeners(): void {
        this._subscribe(B2BEvents.PANEL_BEFORE_LOAD, this.handlePanelBeforeLoad.bind(this));
        this._subscribe(B2BEvents.PANEL_AFTER_LOAD, this.handlePanelAfterLoad.bind(this));
    }

    protected handlePanelBeforeLoad(event: Event, { source, panel }: AjaxPanelEventData): void {
        const { DATA_RELOAD_TARGET } = this.options;
        const reloadTrigger = source.dataset[DATA_RELOAD_TARGET];

        if (!reloadTrigger) {
            return;
        }

        panel.dataset[DATA_RELOAD_TARGET] = reloadTrigger;
    }

    protected handlePanelAfterLoad(event: Event, { panel }: AjaxPanelEventData): void {
        const reloadTrigger = panel.dataset[this.options.DATA_RELOAD_TARGET];

        if (!reloadTrigger) {
            return;
        }

        this.reloadWindow(reloadTrigger);
        this.reloadPanels(reloadTrigger);
    }

    protected reloadWindow(reloadTrigger: string): void {
        if (reloadTrigger.indexOf(this.options.RELOAD_TARGET_WINDOW) > -1) {
            window.location.reload();
        }
    }

    protected reloadPanels(reloadTrigger): void {
        reloadTrigger.split(',').forEach(this.triggerPanelReload.bind(this));
    }

    protected triggerPanelReload(panelId: string): void {
        const target = document.querySelector(`${this.options.SELECTOR_PANEL}[data-id="${panelId}"]`);

        if (!target) {
            return;
        }

        this._publish(B2BEvents.PANEL_REFRESH, {
            target,
        });
    }
}
