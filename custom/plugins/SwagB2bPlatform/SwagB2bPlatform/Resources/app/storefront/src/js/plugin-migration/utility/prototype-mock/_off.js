/* eslint-disable prefer-rest-params */
export default function _off() {
  const me = this;
  const events = me._events;
  const pluginEvent = me.getEventName(arguments[0]);
  const element = arguments[1];
  const eventIds = [];
  const $element = $(element);
  const filteredEvents = $.grep(events, (obj, id) => {
    eventIds.push(id);

    return (typeof obj !== 'undefined' && pluginEvent === obj.event && $element[0] === obj.el[0]);
  });

  filteredEvents.forEach(({ event }) => {
    $element.off(event);
  });

  eventIds.forEach((id) => {
    delete events[id];
  });

  $.publish(`plugin/${me._name}/onRemoveEvent`, [$element, arguments[0]]);

  return me;
}
/* eslint-enable prefer-rest-params */
