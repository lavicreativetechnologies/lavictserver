import { EventInterface, InitOnLoad } from '../decorator';

@EventInterface
@InitOnLoad
export default class AutoSubmitPlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_AUTO_SUBMIT: '.is--auto-submit',
        SELECTOR_AUTO_SUBMIT_NON_INPUT: '.is--auto-submit:not(input):not(select):not(button):not(.select-field)',
        DATA_LINKED_FORM: 'linkedForm',
        DATA_IGNORE_VALUES: ['_new_'],
        DATA_NATIVE_SUBMIT: 'autoSubmitNative',
    };

    public init(): void {
        this.registerEventListeners();
    }

    protected registerEventListeners(): void {
        const { SELECTOR_AUTO_SUBMIT, SELECTOR_AUTO_SUBMIT_NON_INPUT } = this.options;

        this._addGlobalEventListener('change', SELECTOR_AUTO_SUBMIT, this.handleTriggerEvent.bind(this));
        this._addGlobalEventListener('click', SELECTOR_AUTO_SUBMIT_NON_INPUT, this.handleTriggerEvent.bind(this));
    }

    protected handleTriggerEvent(event: Event): void {
        const eventTarget = <HTMLSelectElement>event.target;

        if (event.defaultPrevented || this.isIgnoredValue(eventTarget.value)) {
            return;
        }

        this.submitForm(this.getForm(eventTarget), event);
    }

    protected submitForm(form: HTMLFormElement, event: Event): void {
        const isNativeSubmit = form.dataset[this.options.DATA_NATIVE_SUBMIT];

        if (isNativeSubmit) {
            form.submit();
        } else {
            this._breakEventExecution(event);
            this._dispatchEvent('submit', form);
        }
    }

    protected getForm(target: HTMLElement): HTMLFormElement {
        const linkedFormClass = target.dataset[this.options.DATA_LINKED_FORM];
        let form;

        if (linkedFormClass) {
            form = document.querySelector(`form.${linkedFormClass}`);
        } else {
            form = target.closest('form');
        }

        return form;
    }

    protected isIgnoredValue(value: string | number): Boolean {
        return this.options.DATA_IGNORE_VALUES.includes(value);
    }
}
