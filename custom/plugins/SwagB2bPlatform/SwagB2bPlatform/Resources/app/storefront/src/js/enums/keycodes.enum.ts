enum B2BKeycodes {
    ENTER = 13,
    TAB = 9,
    LEFT = 37,
    UP = 38,
    RIGHT = 39,
    DOWN = 40
}

export default B2BKeycodes;
