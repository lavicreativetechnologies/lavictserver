function InitOnLoad<T extends { new(...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
        public static initOnLoad = true;
    };
}

export default InitOnLoad;
