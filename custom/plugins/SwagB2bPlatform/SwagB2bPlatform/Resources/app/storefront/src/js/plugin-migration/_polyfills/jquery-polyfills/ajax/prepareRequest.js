function buildQueryString(data) {
  const result = [];
  const handler = (key, value) => {
    result[result.length] = `${encodeURIComponent(key)}=${encodeURIComponent(value || '')}`;
  };

  if (data.constructor === Array) {
    data.forEach(({ name, value }) => {
      handler(name, value);
    });
  } else if (data.constructor === Object) {
    Object.keys(data).forEach((key) => {
      handler(key, data[key]);
    });
  } else if (data.constructor === String) {
    return data;
  }

  if (result.length > 0) {
    return result.join('&');
  }

  return '';
}

export default function (url, method, contentType, data = {}) {
  let _url = url;
  let _data = data;

  if (method === 'GET' && data && _url.indexOf('?') === -1) {
    const queryString = `${buildQueryString(data)}`;

    _url = `${_url}?${queryString}`;
  } else if (contentType === 'application/json') {
    _data = JSON.stringify(_data);
  } else if (contentType === 'multipart/form-data') {
    _data = data;
  } else {
    _data = buildQueryString(_data);
  }

  return {
    _url,
    _data,
  };
}
