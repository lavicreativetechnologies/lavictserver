import { getUrlParameter, setUrlParameter, mutationHelper } from '../utility';
import { getPluginWrapper } from '../plugin-migration/utility';
import { AjaxPanelInterface, InitOnLoad } from '../decorator';

@AjaxPanelInterface
@InitOnLoad
export default class OrderListViewModePlugin extends window.PluginBaseClass {
    public static options = {
        SELECTOR_TOGGLE: '.js--b2b-listingview-mode',
        SELECTOR_MUTATION_TARGET: '.cms-element-product-listing',
        URL_PARAM_KEY: 'b2bListingView',
        VIEW_MODES: {
            LISTING: 'listing',
            TABLE: 'table',
        },
    };

    private toggle: HTMLElement;

    private mutationTarget: HTMLElement;

    public init(): void {
        this.getElements();

        if (!this.toggle || !this.mutationTarget) {
            return;
        }

        mutationHelper(this.mutationTarget, this.mutationCallback.bind(this));
        this.addEventListener();
    }

    protected getElements(): void {
        const { SELECTOR_TOGGLE, SELECTOR_MUTATION_TARGET } = this.options;

        this.toggle = document.querySelector(SELECTOR_TOGGLE);
        this.mutationTarget = document.querySelector(SELECTOR_MUTATION_TARGET);
    }

    protected mutationCallback(): void {
        getPluginWrapper().pluginObjects.ajaxPanel.init();
        this.addEventListener();
    }

    protected addEventListener(): void {
        this.toggle.addEventListener('click', this.toggleViewMode.bind(this));
    }

    protected toggleViewMode(): void {
        setUrlParameter(this.options.URL_PARAM_KEY, this.getNewViewMode());
    }

    protected getNewViewMode(): string {
        const { VIEW_MODES, URL_PARAM_KEY } = this.options;
        const { LISTING, TABLE } = VIEW_MODES;
        const currentViewMode = getUrlParameter(URL_PARAM_KEY, LISTING);

        return currentViewMode === TABLE ? LISTING : TABLE;
    }
}
