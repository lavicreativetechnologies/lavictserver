import _destroy from './_destroy';
import _off from './_off';
import _on from './_on';
import _overrideEl from './_overrideEl';
import _update from './_update';
import { applyDataAttributes } from './applyDataAttributes';
import destroy from './destroy';
import getElement from './getElement';
import getEventName from './getEventName';
import getName from './getName';
import getOption from './getOption';
import getOptions from './getOptions';
import setOption from './setOption';

export {
  _destroy,
  _off,
  _on,
  _overrideEl,
  _update,
  applyDataAttributes,
  destroy,
  getElement,
  getEventName,
  getName,
  getOption,
  getOptions,
  setOption,
};
