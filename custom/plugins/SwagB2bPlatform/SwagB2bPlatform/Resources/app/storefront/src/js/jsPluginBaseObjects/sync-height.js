/**
 * This plugin syncs the height of two or more containers and puts a min-height on them
 *
 * Enable by adding the class `b2b--sync-height` and set a group through data-sync-group="_NAME_"
 */
export default {
  defaults: {
    elementSelector: '.b2b--sync-height',
  },

  init() {
    this.applyDataAttributes();
    this.syncHeight();
    this.addListeners();
  },

  addListeners() {
    const me = this;

    me._on(document, 'b2b--ajax-panel_loaded', me.defaults.elementSelector, $.proxy(me.syncHeight, me));
  },

  syncHeight() {
    const me = this;

    const handledGroups = {};

    $(me.defaults.elementSelector).each(function () {
      const $el = $(this);
      const groupName = $el.data('syncGroup');

      if (handledGroups[groupName]) {
        return;
      }

      handledGroups[groupName] = true;

      const $group = $(`${me.defaults.elementSelector}[data-sync-group="${groupName}"]`);

      $group.each(function () {
        $(this).css('minHeight', 0);
      });

      let maxHeight = 0;

      $group.each(function () {
        const currentHeight = $(this).innerHeight();

        if (currentHeight > maxHeight) {
          maxHeight = currentHeight;
        }
      });

      $group.each(function () {
        $(this).css('minHeight', `${maxHeight}px`);
      });
    });
  },

  destroy() {
    const me = this;
    me._destroy();
  },
};
