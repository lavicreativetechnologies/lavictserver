import { b2bAjaxPanelModal } from '../../jsPluginBaseObjects';
import { breakEventExecution, publish, subscribe } from '../../utility/index.ts';

const toggleSelector = '.b2b--modal-navigation--toggle';

function handleSmallModal(isSmallModal) {
  const modalContainer = document.querySelector('.js--modal');

  if (modalContainer) {
    if (isSmallModal) {
      modalContainer.classList.add('is--small');
    } else {
      modalContainer.classList.remove('is--small');
    }
  }
}

function isModalOrModalLink(target) {
  let currentTarget = target;
  let _isModal = false;

  while (currentTarget) {
    if (currentTarget.classList.contains('js--modal') || currentTarget.classList.contains('link-modal--small')) {
      _isModal = true;
      currentTarget = false;
    }
    currentTarget = currentTarget.parentElement;
  }

  return _isModal;
}

const overrides = {
  defaults: {
    ...b2bAjaxPanelModal.defaults,
    normalWidth: 1200,
  },
  init() {
    const me = this;

    this.applyDataAttributes();

    subscribe(
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const { ajaxData } = eventData;

        if (!$panel.hasClass(me.defaults.modalModifierClass)) {
          return;
        }

        const height = me.defaults.normalHeight;
        const width = me.defaults.normalWidth;

        breakEventExecution(event);

        $.overlay.close();

        const $newPanel = $.modal.open($panel[0].outerHTML, {
          width,
          height,
          sizing: 'fixed',
        })._$content.find(me.defaults.panelSelector);

        $('.modal--close').addClass('b2b--modal-close');

        const newPanel = $newPanel[0];
        newPanel.dataset.url = ajaxData.url;
        newPanel.dataset.payload = JSON.stringify(ajaxData.data);
        newPanel.dataset.b2bModalAjaxPanel = true;
        newPanel.classList.remove(me.defaults.modalModifierClass);

        publish('b2b--ajax-panel_register', newPanel);
      },
    );

    subscribe(
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        const $panel = $(eventData.panel);
        const hasCloseTrigger = Boolean($panel.find(me.defaults.panelCloseClassSelector).length);

        if (!hasCloseTrigger) {
          return;
        }

        $.modal.close();
      },
    );

    subscribe(
      'b2b--ajax-panel_loading',
      (event, eventData) => {
        const { source } = eventData;

        if (isModalOrModalLink(source)) {
          const isSmallModal = source.classList.contains('link-modal--small') || source.classList.contains('modal--small');

          handleSmallModal(isSmallModal);
        }
      },
    );

    subscribe(
      'b2b--ajax-panel_loaded',
      (event, eventData) => {
        const { panel } = eventData;
        this.toggleNavigation(panel);
        this.shouldCloseOnSuccess(panel);
      },
    );
  },

  shouldCloseOnSuccess(panel) {
    const shouldClose = $(panel).find('[data-modal-close]').length;

    if (!shouldClose) {
      return;
    }

    $.modal.close();
  },

  toggleNavigation(panel) {
    const $panel = $(panel);
    if (!$panel.hasClass('tab--container')) {
      const hideToggle = !!$panel[0].querySelector('.no--navigation');
      const toggle = $(toggleSelector);
      toggle.parent().toggleClass('navigation--hidden', hideToggle);
    }
  },
};

/**
 * @deprecated tag:v4.6.0 - Will be replaced by src/js/plugins/ajax-panel-modal.plugin
 */
export default ({
  ...b2bAjaxPanelModal,
  ...overrides,
  name: 'b2bAjaxPanelModal',
  initOnLoad: true,
  selector: 'body',
});
