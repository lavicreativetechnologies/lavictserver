export default function (selector) {
  const lastEl = this.$el;

  this.selector = selector;
  this.$el = $(selector);
  if (lastEl !== this.$el && this.init && this.$el.length > 0) {
    this.init();
  }
}
