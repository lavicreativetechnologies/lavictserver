module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
  ],
  extends: [
    'airbnb-typescript/base'
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    'curly': [2, 'all'],
    'brace-style': [1, '1tbs' , {
      'allowSingleLine': false
    }],
    'func-names': 0,
    'no-underscore-dangle': 0,
    'class-methods-use-this': 0,
    'import/prefer-default-export': 0,
    'no-plusplus': 0,
    'newline-before-return': 1,
    'import/no-unresolved': 0,
    'no-param-reassign': 0,
    'max-len': 0,
    'import/extensions': 0,
    'no-new': 0,
    'no-undef': 0,
    'global-require': 0,
    'no-else-return': 0,
    'quotes': ['warn', 'single'],
    'indent': ['warn', 4, {
      'SwitchCase': 1,
    }],
    '@typescript-eslint/indent': ['warn', 4, {
      'SwitchCase': 1,
    }],
    'space-before-function-paren': ["error", {
      "anonymous": "never",
      "named": "never",
      "asyncArrow": "always"
    }],
    '@typescript-eslint/space-before-function-paren': ['error', 'never'],
    '@typescript-eslint/no-unnecessary-type-assertion': 1
  }
};
