const path = require('path');
const fs = require('fs');

const loaderPath = path.join(__dirname, '../../../../views/storefront/bundles/BundleLoader.html.twig');
const devLoader = path.join(__dirname, 'load-dev.html.twig');

const setHMR = isDev => {
  if (isDev) {
    fs.createReadStream(devLoader).pipe(fs.createWriteStream(loaderPath));
  } else if (fs.existsSync(loaderPath)) {
    fs.unlinkSync(loaderPath);
  }
};

module.exports = setHMR;