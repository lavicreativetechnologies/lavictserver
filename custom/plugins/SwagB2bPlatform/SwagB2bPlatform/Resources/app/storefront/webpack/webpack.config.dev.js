const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const WebpackBar = require('webpackbar');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base');
const setHMR = require('./helper/setHMR');

setHMR(true);

module.exports = merge(baseConfig, {
    mode: 'development',
    devServer: {
        host: '0.0.0.0',
        port: 10999,
        quiet: true
    },
    devtool: 'inline-source-map',
    plugins: [
        new FriendlyErrorsWebpackPlugin(),
        new WebpackBar({
          name: 'B2B Storefront',
        }),
        new BundleAnalyzerPlugin()
    ]
});
