const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base');
const setHMR = require('./helper/setHMR');

setHMR(false);

module.exports = merge(baseConfig, {
  mode: 'production',
  devtool: false
});
