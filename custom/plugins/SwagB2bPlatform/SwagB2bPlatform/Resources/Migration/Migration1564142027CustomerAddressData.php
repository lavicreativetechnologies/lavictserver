<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1564142027CustomerAddressData implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1564142027;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE `b2b_customer_address_data` (
                `address_id` BINARY(16) NOT NULL, 
                `b2b_type` TEXT NULL,
                `updated_at` DATETIME(3),
                `created_at` DATETIME(3) NOT NULL,
                PRIMARY KEY (`address_id`),
                CONSTRAINT fk_b2b_data_customer_address
		            FOREIGN KEY (address_id) REFERENCES customer_address (id) ON DELETE CASCADE 
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
