<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\B2B\Offer\BridgePlatform\OfferStatusNotifyMailEvent;
use Shopware\Core\Content\MailTemplate\MailTemplateActions;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\CustomField\CustomFieldEntity;
use SwagB2bPlatform\SetUp;
use Symfony\Component\DependencyInjection\Container;
use function array_merge;
use function file_get_contents;

class Migration1610529821ChangeOfferStatusChangeNotificationMail implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1610529821;
    }

    public function updateDatabase(Connection $connection): void
    {
        //nth
    }

    public function updateThroughServices(Container $container): void
    {
        $this->removeUnusedCustomField($container);
        $this->addEventAction($container);
    }

    protected function addEventAction(Container $container): void
    {
        $templateTypeId = $this->getMailTemplateTypeId($container);

        if (!$templateTypeId) {
            return;
        }

        $templateId = $this->getMailTemplateId($container, $templateTypeId);

        if (!$templateId) {
            return;
        }

        $this->setSenderName($container, $templateId);

        $repository = $container->get('event_action.repository');
        $context = Context::createDefaultContext();

        $repository->create([
            [
                'eventName' => OfferStatusNotifyMailEvent::EVENT_NAME,
                'actionName' => MailTemplateActions::MAIL_TEMPLATE_MAIL_SEND_ACTION,
                'config' => [
                    'mail_template_type_id' => $templateTypeId,
                    'mail_template_id' => $templateId,
                ],
            ],
        ], $context);
    }

    protected function removeUnusedCustomField(Container $container): void
    {
        $customFieldSetRepository = $container->get('custom_field_set.repository');

        $context = Context::createDefaultContext();

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', 'swag_b2b_plugin_user'));

        $entitySearchResult = $customFieldSetRepository->search($criteria, $context);

        /** @var CustomFieldEntity $result */
        $result = $entitySearchResult->first();

        if (!$result) {
            return;
        }

        $customFieldSetRepository->delete([[
            'id' => $result->getId(),
        ]], $context);
    }

    protected function getMailTemplateId(Container $container, string $mailTemplateTypeId): ?string
    {
        $mailTemplateRepository = $container->get('mail_template.repository');

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('mailTemplateTypeId', $mailTemplateTypeId));

        $result = $mailTemplateRepository->searchIds($criteria, Context::createDefaultContext());

        return $result->firstId();
    }

    protected function getMailTemplateTypeId(Container $container): ?string
    {
        $mailTemplateTypeRepository = $container->get('mail_template_type.repository');

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('technicalName', 'b2bOfferStatusChanged_general'));

        $result = $mailTemplateTypeRepository->searchIds($criteria, Context::createDefaultContext());

        return $result->firstId();
    }

    protected function setSenderName(Container $container, string $mailTemplateId): void
    {
        $fixturePath = __DIR__ . '/FileFixtures/Offer/b2bOfferStatusChangedNotificationMail/';

        $translations = [
            SetUp::DEFAULT_LANGUAGE_CODE => [
                'senderName' => '{{ salesChannel.name }}',
                'subject' => 'The status of your offer has changed',
                'contentPlain' => file_get_contents($fixturePath . 'plain_en.twig'),
                'contentHtml' => file_get_contents($fixturePath . 'html_en.twig'),
            ],
            'de-DE' => [
                'senderName' => '{{ salesChannel.name }}',
                'subject' => 'Der Status deines Angebotes hat sich geändert',
                'contentPlain' => file_get_contents($fixturePath . 'plain_de.twig'),
                'contentHtml' => file_get_contents($fixturePath . 'html_de.twig'),
            ],
        ];

        $mailTemplateRepository = $container->get('mail_template.repository');

        $mailTemplateRepository->update(
            [
                array_merge([
                    'id' => $mailTemplateId,
                    'translations' => $translations,
                ], $translations[SetUp::DEFAULT_LANGUAGE_CODE]),
            ], Context::createDefaultContext()
        );
    }
}
