<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1500008528AddCurrencyFactorToBudgetTransaction implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1500008528;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_budget_transaction`
              ADD COLUMN `currency_factor` DOUBLE NOT NULL DEFAULT 1;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
