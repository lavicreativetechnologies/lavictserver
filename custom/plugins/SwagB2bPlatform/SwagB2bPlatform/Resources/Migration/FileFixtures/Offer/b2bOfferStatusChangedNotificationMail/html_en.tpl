Hello {$addressee.name},<br/>
<br/>
the status of {if $context.isToEnquirer}your offer{else}the offer by {$offer.enquirer}{/if} at {config name=shopName} has changed.<br>
It is now "{s name="{$offer.status}"}{$offer.status}{/s}".<br>
{if !$offer.finished}Please log into your {if $context.isToAdmin}admin-{/if}account and check the offer under {if $context.isToAdmin}"Customers" - "Offers"{else}"My account" - "Dashboard" - "Offers"{/if}{/if}
