<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1499168548SalesRepresentativeToContact implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1499168548;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE `b2b_sales_representative_clients` (
              `sales_representative_id` BINARY(16) NOT NULL,
              `client_id` int(11) NOT NULL,
              
              PRIMARY KEY (`sales_representative_id`, `client_id`),

              CONSTRAINT `FK_customer` FOREIGN KEY (`sales_representative_id`)
                REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `FK_client_id` FOREIGN KEY (`client_id`)
                REFERENCES `b2b_store_front_auth` (`id`) ON DELETE CASCADE ON UPDATE CASCADE      
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
