<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Symfony\Component\DependencyInjection\Container;

class Migration1493479531InstallOrderContextAndAddOrderStates implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1493479531;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->query('
            CREATE TABLE `b2b_order_context` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `list_id` INT(11) NOT NULL,

                `ordernumber` VARCHAR(255) NULL DEFAULT NULL,

                `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `cleared_at` DATETIME NULL DEFAULT NULL,
                `declined_at` DATETIME NULL DEFAULT NULL,

                `shipping_address_id` BINARY(16) NOT NULL,
                `billing_address_id` BINARY(16) NOT NULL,

                `payment_id` BINARY(16) NULL DEFAULT NULL,
                `shipping_id` BINARY(16) NOT NULL,

                `status_id` BINARY(16) NOT NULL,

                `comment` TEXT NOT NULL,
                `device_type` VARCHAR(255) NOT NULL,

                `order_reference` VARCHAR(255) NULL DEFAULT NULL,
                `requested_delivery_date` VARCHAR(255) NULL DEFAULT NULL,
                `auth_id` INT(11) NULL DEFAULT NULL,

                `currency_factor` DOUBLE NOT NULL,

                PRIMARY KEY (`id`),
                INDEX `FK_b2b_line_item_list_s_order_b2b_line_item_list` (`list_id`),

                CONSTRAINT `b2b_order_context_auth_user_id_FK` FOREIGN KEY (`auth_id`)
                  REFERENCES `b2b_store_front_auth` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
                CONSTRAINT `FK_b2b_order_s_order_b2b_line_item_list_id` FOREIGN KEY (`list_id`)
                  REFERENCES `b2b_line_item_list` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
                CONSTRAINT `b2b_order_context_status_id_FK` FOREIGN KEY (`status_id`)
                  REFERENCES `state_machine_state` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');

        $stateMaschineOrderStateId = $connection->fetchColumn('SELECT id from state_machine where technical_name = :technicalName', ['technicalName' => 'order.state']);

        $orderClearanceDeniedStateId = Uuid::randomBytes();
        $orderClearanceOpenStateId = Uuid::randomBytes();

        $connection->executeUpdate(
            'INSERT INTO state_machine_state (id, technical_name, state_machine_id, created_at, updated_at)
                                   VALUES (:orderClearanceDeniedId, :orderClearanceDenied, :stateMaschineId, NOW(), NOW()),
                                          (:orderClearanceOpenId, :orderClearanceOpen, :stateMaschineId, NOW(), NOW());',
            [
                'stateMaschineId' => $stateMaschineOrderStateId,
                'orderClearanceDeniedId' => $orderClearanceDeniedStateId,
                'orderClearanceDenied' => 'orderclearance_denied',
                'orderClearanceOpenId' => $orderClearanceOpenStateId,
                'orderClearanceOpen' => 'orderclearance_open',
            ]
        );
    }

    public function updateThroughServices(Container $container): void
    {
        $customFieldSetRepository = $container->get('custom_field_set.repository');
        $customFieldSetRepository->create([
            [
                'name' => 'swag_b2b_plugin_order',
                'customFields' => [
                    ['name' => 'b2b_auth_id', 'type' => CustomFieldTypes::INT],
                    ['name' => 'b2b_order_reference', 'type' => CustomFieldTypes::TEXT],
                    ['name' => 'b2b_requested_delivery_date', 'type' => CustomFieldTypes::TEXT],
                    ['name' => 'b2b_clearance_comment', 'type' => CustomFieldTypes::TEXT],
                ],
                'relations' => [
                    ['entityName' => 'order'],
                ],
            ],
        ], Context::createDefaultContext());
    }
}
