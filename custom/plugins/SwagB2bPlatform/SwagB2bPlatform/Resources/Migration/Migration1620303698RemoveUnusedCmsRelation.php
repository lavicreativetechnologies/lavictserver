<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;
use function mb_strpos;

class Migration1620303698RemoveUnusedCmsRelation implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1620303698;
    }

    public function updateDatabase(Connection $connection): void
    {
        $authDdl = $connection->fetchColumn('SHOW CREATE TABLE `b2b_store_front_auth`', [], 1);

        if ($this->alreadyExecuted($authDdl)) {
            return;
        }

        if ($this->isPriorTo64($authDdl)) {
            $connection->exec('
                ALTER TABLE `b2b_store_front_auth`
                    DROP FOREIGN KEY `FK_b2b_store_front_auth_cms_page_id`,
                    DROP INDEX `FK_b2b_store_front_auth_cms_page_id`,
                    DROP COLUMN `cms_page_id`
            ');
        } else {
            $connection->exec('
                ALTER TABLE `b2b_store_front_auth`
                    DROP FOREIGN KEY `FK_b2b_store_front_auth_cms_page_id`,
                    DROP INDEX `FK_b2b_store_front_auth_cms_page_id`,
                    DROP COLUMN `cms_page_id`,
                    DROP COLUMN `cms_page_version_id`
            ');
        }
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }

    protected function alreadyExecuted(string $authDdl): bool
    {
        return mb_strpos($authDdl, 'cms_page_id') === false;
    }

    protected function isPriorTo64(string $authDdl): bool
    {
        return mb_strpos($authDdl, 'cms_page_version_id') === false;
    }
}
