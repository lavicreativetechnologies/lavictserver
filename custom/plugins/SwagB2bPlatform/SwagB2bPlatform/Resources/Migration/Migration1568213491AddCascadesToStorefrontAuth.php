<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1568213491AddCascadesToStorefrontAuth implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1568213491;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_store_front_auth
            DROP FOREIGN KEY FK_b2b_store_front_auth_s_media_id;
        ');
        $connection->exec('
            ALTER TABLE b2b_store_front_auth
            ADD CONSTRAINT FK_b2b_store_front_auth_media_id FOREIGN KEY (media_id) REFERENCES media (id)
                ON DELETE SET NULL ON UPDATE CASCADE;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
