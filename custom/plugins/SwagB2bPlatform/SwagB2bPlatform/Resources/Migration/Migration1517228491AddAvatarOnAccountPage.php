<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\DependencyInjection\Container;

class Migration1517228491AddAvatarOnAccountPage implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1517228491;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
ALTER TABLE b2b_store_front_auth
  ADD media_id BINARY(16) NULL;
CREATE INDEX b2b_store_front_auth_media_id_idx
  ON b2b_store_front_auth (media_id);
ALTER TABLE b2b_store_front_auth
  ADD CONSTRAINT FK_b2b_store_front_auth_s_media_id
FOREIGN KEY (media_id) REFERENCES media (id);

ALTER TABLE b2b_store_front_auth drop COLUMN avatar;');

        $connection->executeUpdate('INSERT INTO media_folder (id, name, created_at) VALUES (:id, \'B2b\', NOW());', ['id' => Uuid::randomBytes()]);
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
