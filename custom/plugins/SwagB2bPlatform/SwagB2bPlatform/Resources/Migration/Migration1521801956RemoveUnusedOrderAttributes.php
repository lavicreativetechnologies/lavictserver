<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Symfony\Component\DependencyInjection\Container;
use function array_map;

class Migration1521801956RemoveUnusedOrderAttributes implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1521801956;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
        $context = Context::createDefaultContext();
        $customFieldRepository = $container->get('custom_field.repository');
        $ids = $customFieldRepository->searchIds(
            (new Criteria())->addFilter(
                new EqualsAnyFilter(
                    'name',
                    ['b2b_requested_delivery_date', 'b2b_order_reference', 'b2b_clearance_comment']
                )
            ),
            $context
        );

        $ids = array_map(function ($id) { return ['id' => $id]; }, $ids->getIds());

        $customFieldRepository->delete($ids, $context);
    }
}
