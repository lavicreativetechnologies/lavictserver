<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1625121257DeleteSalesChannelContextsWithIntId implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1625121257;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec("
            DELETE FROM sales_channel_api_context WHERE payload LIKE '%b2b_front_auth_identity_id%';
        ");
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
