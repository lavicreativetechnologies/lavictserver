<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1498027169InstallInStock implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1498027169;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE `b2b_in_stocks` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `auth_id` int(11) NOT NULL,
              `in_stock` double NOT NULL,
              `product_id` BINARY(16) NOT NULL,
            
              PRIMARY KEY (`id`),
            
              UNIQUE INDEX `b2b_debtor_from_to_product_idx` (`auth_id`, `product_id`),
              INDEX `b2b_in_stocks_product_idx` (`product_id`),
              INDEX `b2b_in_stocks_auth_idx` (`auth_id`),
            
              CONSTRAINT `FK_in_stocks_auth_id` FOREIGN KEY (`auth_id`)
                REFERENCES `b2b_store_front_auth` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `FK_in_stocks_product_id` FOREIGN KEY (`product_id`)
                REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
