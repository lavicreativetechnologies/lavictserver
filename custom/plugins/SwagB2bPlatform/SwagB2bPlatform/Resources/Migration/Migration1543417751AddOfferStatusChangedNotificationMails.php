<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use SwagB2bPlatform\SetUp;
use Symfony\Component\DependencyInjection\Container;
use function array_merge;
use function file_get_contents;

class Migration1543417751AddOfferStatusChangedNotificationMails implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1543417751;
    }

    public function updateDatabase(Connection $connection): void
    {
        // nth
    }

    public function updateThroughServices(Container $container): void
    {
        $this->addMailTemplate($container);

        $this->addMailCheckbox($container);
    }

    protected function addMailCheckbox(Container $container): void
    {
        $customFieldSetRepository = $container->get('custom_field_set.repository');
        $customFieldSetRepository->create(
            [
                [
                    'name' => 'swag_b2b_plugin_user',
                    'customFields' => [
                        [
                            'name' => 'send_offer_status_changed_notification_mail',
                            'type' => CustomFieldTypes::BOOL,
                            'config' => [
                                'translated' => true,
                                'label' => [
                                    SetUp::DEFAULT_LANGUAGE_CODE => 'Receive offer status notification',
                                    'de-DE' => 'Erhalte Nachrichten bei Angebotsstatusänderungen',
                                ],
                                'supportText' => [
                                    SetUp::DEFAULT_LANGUAGE_CODE => 'Activate or deactivate notification mail',
                                    'de-DE' => 'Aktiviere oder deaktiviere Benachrichtigungsmail',
                                ],
                                'helpText' => [
                                    SetUp::DEFAULT_LANGUAGE_CODE => 'The system sends out an email to every admin, who checked this box, when the status of an offer changed and an admin is required to proceed.',
                                    'de-DE' => 'Das System schickt an jeden Administrator, der dieses Feld anwählt eine E-Mail, jedesmal, wenn sich der Status eines Angbots ändert und es einen Administrator zum fortfahren benötigt.',
                                ],
                            ],
                        ],
                    ],
                    'relations' => [
                        ['entityName' => 'user'],
                    ],
                ],
            ],
            Context::createDefaultContext()
        );
    }

    protected function addMailTemplate(Container $container): void
    {
        $mailName = 'b2bOfferStatusChanged_general';
        $fixturePath = __DIR__ . '/FileFixtures/Offer/b2bOfferStatusChangedNotificationMail/';

        $translations = [
            SetUp::DEFAULT_LANGUAGE_CODE => [
                'subject' => 'The status of {if $context.isToEnquirer}your offer{else}the offer by {$offer.enquirer}{/if} at {config name=shopName} has changed',
                'contentPlain' => file_get_contents($fixturePath . 'plain_en.tpl'),
                'contentHtml' => file_get_contents($fixturePath . 'html_en.tpl'),
            ],
            'de-DE' => [
                'subject' => 'Der Status {if $context.isToEnquirer}deines Angebotes{else}des Angebots von{$offer.enquirer}{/if} bei {config name=shopName} hat sich ge&auml;ndert.',
                'contentPlain' => file_get_contents($fixturePath . 'plain_de.tpl'),
                'contentHtml' => file_get_contents($fixturePath . 'html_de.tpl'),
            ],
        ];

        $mailTemplateRepository = $container->get('mail_template.repository');
        $context = Context::createDefaultContext();

        $mailTemplateId = Uuid::randomHex();

        $mailTemplateData = array_merge([
            'id' => $mailTemplateId,
            'systemDefault' => false,
            'mailTemplateType' => [
                'technicalName' => $mailName,
                'name' => $mailName,
            ],
            'translations' => $translations,
        ], $translations[SetUp::DEFAULT_LANGUAGE_CODE]);

        $mailTemplateRepository->create([$mailTemplateData], $context);
    }
}
