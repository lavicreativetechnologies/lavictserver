<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1592466014MoveCurrencyFactorFromListToLineItemReferences implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1592466014;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_line_item_reference`
            ADD COLUMN `currency_factor` DOUBLE DEFAULT 1.0;
        ');

        $connection->exec('
            UPDATE `b2b_line_item_reference`
            INNER JOIN `b2b_line_item_list`
            ON `b2b_line_item_reference`.`list_id` = `b2b_line_item_list`.`id`
            SET `b2b_line_item_reference`.`currency_factor` = `b2b_line_item_list`.`currency_factor`;
        ');

        $connection->exec('
            ALTER TABLE `b2b_line_item_list`
            DROP COLUMN `currency_factor`;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
