<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1559657920CustomerData implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1559657920;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE `b2b_customer_data` (
                `customer_id` BINARY(16) NOT NULL, 
                `is_debtor` TINYINT(1) NOT NULL,
                `is_sales_representative` TINYINT(1) NOT NULL,
                `updated_at` DATETIME(3),
                `created_at` DATETIME(3) NOT NULL,
                PRIMARY KEY (`customer_id`),
                CONSTRAINT fk_b2b_data_customer
		            FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE CASCADE
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
