<?php declare(strict_types=1);

namespace SwagB2bPlatform\Resources\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1502351255EmotionContactBase implements MigrationStepInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCreationTimeStamp(): int
    {
        return 1502351255;
    }

    /**
     * {@inheritdoc}
     */
    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_store_front_auth`
                ADD COLUMN `cms_page_id` BINARY(16) DEFAULT NULL,
                ADD CONSTRAINT FK_b2b_store_front_auth_cms_page_id
                FOREIGN KEY (`cms_page_id`) REFERENCES `cms_page`(`id`)
                ON DELETE SET NULL
                ON UPDATE NO ACTION;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function updateThroughServices(Container $container): void
    {
    }
}
