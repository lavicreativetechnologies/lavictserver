<?php declare(strict_types=1);

namespace SwagB2bPlatform\Subscriber;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LoginHeaderSender implements EventSubscriberInterface
{
    const HEADER_NAME = 'b2b-no-login';

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'addLoginHeader',
        ];
    }

    public function addLoginHeader(ResponseEvent $event): void
    {
        $response = $event->getResponse();

        if (!$this->authenticationService->isB2b()) {
            $response->headers->set(static::HEADER_NAME, true);
        }
    }
}
