<?php declare(strict_types=1);

namespace SwagB2bPlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Acl\Framework\DependencyInjection\AclFrameworkConfiguration;
use Shopware\B2B\AclRoute\Framework\AclRoutingUpdateService;
use Shopware\B2B\Common\Migration\MigrationCollectionLoader;
use Shopware\B2B\Common\Migration\MigrationRuntime;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SetUp
{
    public const DEFAULT_LANGUAGE_CODE = 'en-GB';

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setUp(array $migrationPaths): void
    {
        $this->migrate($migrationPaths);
        $this->updateAclRouteConfig();
    }

    /**
     * @param string[] $migrationPaths
     * @internal
     */
    protected function migrate(array $migrationPaths): void
    {
        $collector = new MigrationCollectionLoader();
        foreach ($migrationPaths as $migrationPath) {
            $collector->addDirectory($migrationPath, 'SwagB2bPlatform\Resources\Migration');
        }
        $migrations = $collector->getMigrationCollection();

        $migrationRuntime = new MigrationRuntime(
            'b2b_migration',
            $this->container->get(Connection::class),
            $this->container
        );

        $migrationRuntime->migrate($migrations);
    }

    /**
     * @internal
     */
    protected function updateAclRouteConfig(): void
    {
        $aclRoutingUpdateService = new AclRoutingUpdateService($this->container->get(Connection::class));

        $aclRoutingUpdateService->addConfig(AclFrameworkConfiguration::getAclConfigData());
    }
}
