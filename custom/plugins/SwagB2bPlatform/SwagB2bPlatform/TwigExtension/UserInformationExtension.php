<?php declare(strict_types=1);

namespace SwagB2bPlatform\TwigExtension;

use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractNamedUser;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Twig\Extension\RuntimeExtensionInterface;

class UserInformationExtension implements RuntimeExtensionInterface
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function getUserName(): ?array
    {
        if (!$this->authenticationService->isAuthenticated()) {
            return null;
        }

        $user = $this->authenticationService->getIdentity()->getEntity();

        if (!$user instanceof AbstractNamedUser) {
            return null;
        }

        return [
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
        ];
    }
}
