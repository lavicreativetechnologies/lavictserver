<?php declare(strict_types=1);

namespace SwagB2bPlatform\TwigExtension;

use InvalidArgumentException;
use Shopware\B2B\AclRoute\Framework\AclRouteService;
use Twig\Extension\RuntimeExtensionInterface;
use function array_map;
use function explode;
use function implode;
use function is_array;
use function trim;

class RuntimeExtension implements RuntimeExtensionInterface
{
    const ACL_CLASS_NAME = 'is--b2b-acl';
    const ACL_CLASS_FORBIDDEN = 'is--b2b-acl-forbidden';
    const ACL_CLASS_ALLOWED = 'is--b2b-acl-allowed';
    const ACL_CLASS_CONTROLLER_PREFIX = 'is--b2b-acl-controller-';
    const ACL_CLASS_ACTION_PREFIX = 'is--b2b-acl-action-';

    /**
     * @var AclRouteService
     */
    private $routeService;

    public function __construct(AclRouteService $routeService)
    {
        $this->routeService = $routeService;
    }

    public function aclCheck(string $controller, ?string $action = null, ?string $actions = null): bool
    {
        if ($actions) {
            $action = explode(',', trim($actions));
        }

        if (!$action) {
            throw new InvalidArgumentException('Missing required parameter "action"');
        }

        if (is_array($action)) {
            $isAllowed = $this->isAllowed($controller, $action);
        } else {
            $isAllowed = $this->isAllowed($controller, [$action]);
        }

        return $isAllowed;
    }

    public function acl(string $controller, ?string $action = null, ?string $actions = null): string
    {
        if (isset($actions)) {
            $action = explode(',', trim($actions));
        }

        if (!isset($action)) {
            throw new InvalidArgumentException('Missing required parameter "action"');
        }

        if (is_array($action)) {
            $isAllowed = $this->isAllowed($controller, $action);
            $aclAction = implode(' ', array_map(function (string $value) {
                return self::ACL_CLASS_ACTION_PREFIX . $value;
            }, $action));
        } else {
            $isAllowed = $this->isAllowed($controller, [$action]);
            $aclAction = self::ACL_CLASS_ACTION_PREFIX . $action;
        }

        $aclCssClass = self::ACL_CLASS_FORBIDDEN;

        if ($isAllowed) {
            $aclCssClass = self::ACL_CLASS_ALLOWED;
        }

        return implode(' ', [
            self::ACL_CLASS_NAME,
            self::ACL_CLASS_CONTROLLER_PREFIX . $controller,
            $aclAction,
            $aclCssClass,
        ]);
    }

    /**
     * @internal
     */
    protected function isAllowed(string $controller, array $actions): bool
    {
        foreach ($actions as $action) {
            if (!$this->routeService->isRouteAllowed($controller, $action)) {
                return false;
            }
        }

        return true;
    }
}
