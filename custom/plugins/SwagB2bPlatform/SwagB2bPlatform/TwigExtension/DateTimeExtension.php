<?php declare(strict_types=1);

namespace SwagB2bPlatform\TwigExtension;

use DateTime;
use Twig\Extension\RuntimeExtensionInterface;
use function date_format;

class DateTimeExtension implements RuntimeExtensionInterface
{
    const DEFAULT_DATE_TIME_FORMAT = 'd M Y - H:i';
    const FALLBACK_CHARACTER = '-';

    public function formatDateTime(DateTime $dateTime = null, ?string $format = null): string
    {
        if ($dateTime === null) {
            return self::FALLBACK_CHARACTER;
        }

        if ($format === null) {
            $format = self::DEFAULT_DATE_TIME_FORMAT;
        }

        return date_format($dateTime, $format);
    }
}
