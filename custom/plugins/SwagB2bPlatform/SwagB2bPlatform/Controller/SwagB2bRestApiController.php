<?php declare(strict_types=1);

namespace SwagB2bPlatform\Controller;

use Shopware\B2B\Common\MvcExtension\Request as MvcRequest;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\RestApi\RestRoutingService;
use Shopware\B2B\Debtor\BridgePlatform\DebtorApiAuthenticator;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginService;
use Shopware\Core\Checkout\Cart\Exception\CustomerNotLoggedInException;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class SwagB2bRestApiController extends AbstractController
{
    /**
     * @var RestRoutingService
     */
    private $routingService;

    /**
     * @var DebtorApiAuthenticator
     */
    private $apiAuthenticator;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    /**
     * @var LoginService
     */
    private $loginService;

    public function __construct(
        RestRoutingService $routingService,
        DebtorApiAuthenticator $apiAuthenticator,
        AuthStorageAdapterInterface $authStorageAdapter,
        CredentialsBuilderInterface $credentialsBuilder,
        LoginService $loginService
    ) {
        $this->routingService = $routingService;
        $this->apiAuthenticator = $apiAuthenticator;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->credentialsBuilder = $credentialsBuilder;
        $this->loginService = $loginService;
    }

    /**
     * @Route("/api/b2b", name="api.b2b")
     * @Route("/api/b2b/{wildcard}", name="api.b2b.wildcard", requirements={"wildcard": ".*"})
     */
    public function restApi(MvcRequest $request, Request $original): JsonResponse
    {
        $pathInfo = $original->getPathInfo();

        $this->apiAuthenticator
            ->authenticateFromPathInfo($pathInfo);

        return $this->handleApiRequest($pathInfo, $original, $request);
    }

    /**
     * @RouteScope(scopes={"store-api"})
     * @Route("/store-api/v{version}/b2b", name="store-api.b2b")
     * @Route("/store-api/v{version}/b2b/{wildcard}", name="store-api.b2b.wildcard", requirements={"wildcard": ".*"})
     * @Route("/store-api/b2b", name="store-api.64.b2b")
     * @Route("/store-api/b2b/{wildcard}", name="store-api.64.b2b.wildcard", requirements={"wildcard": ".*"})
     */
    public function storeApi(MvcRequest $request, Request $original, SalesChannelContext $context): JsonResponse
    {
        if (!$context->getCustomer() instanceof CustomerEntity) {
            throw new CustomerNotLoggedInException();
        }

        $credentials = $this->credentialsBuilder->createCredentialsByEmail($context->getCustomer()->getEmail());
        try {
            $identity = $this->loginService->getIdentityByCredentials($credentials);
            $this->authStorageAdapter->setIdentity($identity);
        } catch (NotFoundException $e) {
            return new JsonResponse(['success' => false]);
        }

        return $this->handleApiRequest($this->buildPathInfoForStoreApi($original, $identity), $original, $request);
    }

    /**
     * @internal
     */
    protected function handleApiRequest(string $pathInfo, Request $original, MvcRequest $request): JsonResponse
    {
        $data = $this->routingService
            ->getDispatchable($original->getMethod(), $pathInfo)
            ->dispatch($request);

        return new JsonResponse($data);
    }

    /**
     * @internal
     */
    protected function buildPathInfoForStoreApi(Request $original, Identity $identity): string
    {
        $wildcard = $original->get('wildcard', '');
        if (empty($wildcard)) {
            return '/api/b2b';
        }

        return '/api/b2b/debtor/' . $identity->getEntity()->email . '/' . $wildcard;
    }
}
