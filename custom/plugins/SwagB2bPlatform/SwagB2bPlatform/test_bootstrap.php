<?php declare(strict_types=1);

use Shopware\B2B\Common\Testing\KernelStorage;
use SwagB2bPlatform\Testing\TestFactory;

\define('TEST_PROJECT_DIR', __DIR__ . '/../../shopware-platform');
$loader = require TEST_PROJECT_DIR . '/vendor/autoload.php';

KernelStorage::init(new TestFactory($loader));
if (!KernelStorage::has()) {
    KernelStorage::store(KernelStorage::$kernelFactory->bootKernel());
}

return $loader;
