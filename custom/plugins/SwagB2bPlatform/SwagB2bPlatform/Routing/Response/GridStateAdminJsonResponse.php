<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class GridStateAdminJsonResponse extends JsonResponse
{
    public function __construct(array $gridState, int $status = 200, array $headers = [], bool $json = false)
    {
        $data = [
            'total' => $gridState['total'],
            'data' => $gridState['data'],
            'aggregations' => [],
        ];

        parent::__construct($data, $status, $headers, $json);
    }
}
