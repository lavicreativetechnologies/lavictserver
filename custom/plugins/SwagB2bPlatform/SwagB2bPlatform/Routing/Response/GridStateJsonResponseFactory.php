<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing\Response;

use Shopware\Core\Framework\Api\Response\ResponseFactoryRegistry;
use Shopware\Core\Framework\Api\Response\Type\Api\JsonType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class GridStateJsonResponseFactory
{
    /**
     * @var ResponseFactoryRegistry
     */
    private $responseFactoryRegistry;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(
        ResponseFactoryRegistry $responseFactoryRegistry,
        RequestStack $requestStack
    ) {
        $this->responseFactoryRegistry = $responseFactoryRegistry;
        $this->requestStack = $requestStack;
    }

    public function createResponse(array $gridState): JsonResponse
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($this->responseFactoryRegistry->getType($request) instanceof JsonType) {
            return new GridStateAdminJsonResponse($gridState);
        }

        return new GridStateAdminVndApiJsonResponse($request, $gridState);
    }
}
