<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing;

class RouteValue
{
    /**
     * @var string
     */
    public $className;

    /**
     * @var string
     */
    public $serviceId;

    public function __construct(string $className, string $serviceId)
    {
        $this->className = $className;
        $this->serviceId = $serviceId;
    }
}
