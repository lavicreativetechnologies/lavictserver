<?php declare(strict_types=1);

namespace SwagB2bPlatform\Routing;

use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Content\Seo\SeoUrlPlaceholderHandlerInterface;
use Shopware\Core\Framework\Adapter\Twig\TemplateFinderInterface;
use Shopware\Storefront\Framework\Routing\RequestTransformer;
use Shopware\Storefront\Page\GenericPageLoader;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;
use function is_array;

class ViewAssigner implements EventSubscriberInterface
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var GenericPageLoader
     */
    private $pageLoader;

    /**
     * @var SeoUrlPlaceholderHandlerInterface
     */
    private $seoUrlPlaceholderHandler;

    /**
     * @var TemplateFinderInterface
     */
    private $templateFinder;

    public function __construct(
        Environment $twig,
        ContextProvider $contextProvider,
        GenericPageLoader $pageLoader,
        SeoUrlPlaceholderHandlerInterface $seoUrlPlaceholderHandler,
        TemplateFinderInterface $templateFinder
    ) {
        $this->twig = $twig;
        $this->contextProvider = $contextProvider;
        $this->pageLoader = $pageLoader;
        $this->seoUrlPlaceholderHandler = $seoUrlPlaceholderHandler;
        $this->templateFinder = $templateFinder;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => 'wrapResponse',
        ];
    }

    public function wrapResponse(ViewEvent $controllerEvent): void
    {
        $request = $controllerEvent->getRequest();
        $requestAttributes = $request->attributes;

        if (!$requestAttributes->has(RouteLoader::ROUTE_IS_B2B)) {
            return;
        }

        $controllerResult = $controllerEvent->getControllerResult();

        if (!$this->isValidB2bControllerResult($controllerResult)) {
            return;
        }

        $controllerResult['controllerName'] = $requestAttributes->get(RouteLoader::ROUTE_CONTROLLER_ROUTE_NAME);
        $controllerResult['controllerAction'] = $requestAttributes->get(RouteLoader::ROUTE_CONTROLLER_ACTION);

        $controllerResult['page'] = $this->pageLoader->load(
            $request,
            $this->contextProvider->getSalesChannelContext()
        );

        $templateName = $requestAttributes->get(RouteLoader::ROUTE_TEMPLATE);
        $viewScript = $this->templateFinder->find('@SwagB2bPlatform/storefront/frontend/' . $templateName . '.html.twig');

        $host = $requestAttributes->get(RequestTransformer::SALES_CHANNEL_ABSOLUTE_BASE_URL)
            . $requestAttributes->get(RequestTransformer::SALES_CHANNEL_BASE_URL);

        $response = new Response($this->twig->render($viewScript, $controllerResult));
        $response->setContent(
            $this->seoUrlPlaceholderHandler->replace(
                $response->getContent(),
                $host,
                $this->contextProvider->getSalesChannelContext()
            )
        );

        $controllerEvent->setResponse($response);
    }

    /**
     * @internal
     */
    protected function isValidB2bControllerResult($controllerResult): bool
    {
        if ($controllerResult === null) {
            return true;
        }

        if (is_array($controllerResult)) {
            return true;
        }

        return false;
    }
}
