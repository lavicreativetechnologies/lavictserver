<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Framework\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Contact\Framework\DependencyInjection\ContactFrameworkConfiguration;
use Shopware\B2B\Role\Framework\DependencyInjection\RoleFrameworkConfiguration;
use Shopware\B2B\RoleContact\Bridge\DependencyInjection\RoleContactBridgeConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RoleContactFrameworkConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/framework-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        $config = [
            new ContactFrameworkConfiguration(),
            new RoleFrameworkConfiguration(),
        ];

        if (ShopwareVersion::isClassic()) {
            $config[] = new RoleContactBridgeConfiguration();
        }

        return $config;
    }
}
