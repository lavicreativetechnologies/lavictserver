<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Framework;

use Shopware\B2B\Common\IdValue;

/**
 * Assigns roles to contacts M:N
 */
class RoleContactService
{
    /**
     * @var RoleContactRepository
     */
    private $roleContactRepository;

    public function __construct(
        RoleContactRepository $roleContactRepository
    ) {
        $this->roleContactRepository = $roleContactRepository;
    }

    public function getActiveRolesByContactId(IdValue $contactId): array
    {
        return $this->roleContactRepository
            ->getActiveRoleIdsByContactId($contactId);
    }
}
