<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Api;

use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\RoleContact\Framework\RoleContactAssignmentService;
use Shopware\B2B\RoleContact\Framework\RoleContactService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function count;

class RoleContactController
{
    /**
     * @var RoleContactService
     */
    private $roleContactService;

    /**
     * @var RoleContactAssignmentService
     */
    private $roleContactAssignmentService;

    /**
     * @var AuthenticationIdentityLoaderInterface
     */
    private $contactRepository;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        RoleContactService $roleContactService,
        RoleContactAssignmentService $roleContactAssignmentService,
        AuthenticationIdentityLoaderInterface $contactRepository,
        LoginContextService $loginContextService,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->roleContactService = $roleContactService;
        $this->roleContactAssignmentService = $roleContactAssignmentService;
        $this->contactRepository = $contactRepository;
        $this->loginContextService = $loginContextService;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(string $email): array
    {
        $contactIdentity = $this->contactRepository
            ->fetchIdentityByEmail($email, $this->loginContextService)
            ->getOwnershipContext();

        $roles = $this->roleContactService
            ->getActiveRolesByContactId($contactIdentity->identityId);

        $totalCount = count($roles);

        return ['success' => true, 'roles' => $roles, 'totalCount' => $totalCount];
    }

    public function createAction(string $email, Request $request): array
    {
        $contactOwnership = $this->contactRepository
            ->fetchIdentityByEmail($email, $this->loginContextService)
            ->getOwnershipContext();
        $debtorOwnershipContext = $this->getDebtorOwnershipContext();

        $this->roleContactAssignmentService
            ->assign($debtorOwnershipContext, $request->getIdValue('roleId'), $contactOwnership->identityId);

        return ['success' => true];
    }

    public function removeAction(string $email, Request $request): array
    {
        $contactIdentity = $this->contactRepository
            ->fetchIdentityByEmail($email, $this->loginContextService)
            ->getOwnershipContext();
        $debtorOwnershipContext = $this->getDebtorOwnershipContext();

        $this->roleContactAssignmentService
            ->removeAssignment($debtorOwnershipContext, $request->getIdValue('roleId'), $contactIdentity->identityId);

        return ['success' => true];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
