<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContact\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContactRoleVisibilityApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'getAllAllowed',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/grant',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'getAllGrant',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/role/{roleId}',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'getAllowed',
                ['contactEmail', 'roleId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/role/{roleId}',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'allow',
                ['contactEmail', 'roleId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/role/{roleId}/grant',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'allowGrant',
                ['contactEmail', 'roleId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/allow',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'multipleAllow',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/deny',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'multipleDeny',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contact_role/{contactEmail}/role/{roleId}',
                'b2b_role_contact.api_contact_role_visibility_controller',
                'deny',
                ['contactEmail', 'roleId'],
            ],
        ];
    }
}
