<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\IdentityChainIdentityLoader;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserLoginServiceInterface;
use Shopware\Core\Checkout\Customer\Exception\BadCredentialsException;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class UserLoginService implements UserLoginServiceInterface
{
    /**
     * @var IdentityChainIdentityLoader
     */
    private $chainIdentityLoader;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var StoreFrontAuthenticationRepository
     */
    private $repository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var UserDataService
     */
    private $userDataService;

    public function __construct(
        IdentityChainIdentityLoader $chainIdentityLoader,
        LoginContextService $loginContextService,
        StoreFrontAuthenticationRepository $repository,
        UserRepository $userRepository,
        AuthenticationService $authenticationService,
        AccountService $accountService,
        ContextProvider $contextProvider,
        UserDataService $userDataService
    ) {
        $this->chainIdentityLoader = $chainIdentityLoader;
        $this->loginContextService = $loginContextService;
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
        $this->accountService = $accountService;
        $this->contextProvider = $contextProvider;
        $this->userDataService = $userDataService;
    }

    public function loginByMail(string $email): void
    {
        $currentUserCredentials = $this->authenticationService->getIdentity()
            ->getLoginCredentials();

        $identity = $this->chainIdentityLoader
            ->fetchIdentityByEmail($email, $this->loginContextService);

        $this->userRepository
            ->syncUser(
                $this->userDataService->transformIdentityToUserData($identity)
            );

        $loginCredentials = $identity->getLoginCredentials();

        if (!ShopwareVersion::isShopware6_2_X()) {
            $this->accountService->logout($this->contextProvider->getSalesChannelContext());
        }

        try {
            $this->accountService->login($loginCredentials->email, $this->contextProvider->getSalesChannelContext());

            return;
        } catch (BadCredentialsException | UnauthorizedHttpException $badCredentialsException) {
            $errorMessage = $badCredentialsException->getMessage();
        }

        try {
            $this->accountService->login($currentUserCredentials->email, $this->contextProvider->getSalesChannelContext());
        } catch (BadCredentialsException | UnauthorizedHttpException $badCredentialsException) {
            // nth
        }

        $field = 'Email';

        $violation = new ConstraintViolation(
            $errorMessage,
            $errorMessage,
            [],
            '',
            $field,
            $field,
            null,
            null
        );

        $violationList = new ConstraintViolationList([$violation]);

        throw new ValidationException($identity->getEntity(), $violationList, 'Validation violations detected, can not proceed:', 400);
    }

    public function loginByAuthId(IdValue $authId): void
    {
        $authentication = $this->repository
            ->fetchAuthenticationById($authId);

        $identity = $this->chainIdentityLoader
            ->fetchIdentityByAuthentication($authentication, $this->loginContextService);

        $this->loginByMail($identity->getLoginCredentials()->email);
    }
}
