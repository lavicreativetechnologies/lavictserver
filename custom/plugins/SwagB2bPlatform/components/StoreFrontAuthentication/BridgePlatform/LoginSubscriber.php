<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginService;
use Shopware\Core\Checkout\Customer\Event\CustomerBeforeLoginEvent;
use Shopware\Core\Checkout\Customer\Event\CustomerLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LoginSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoginService
     */
    private $loginService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        LoginService $loginService,
        UserRepository $userRepository,
        CredentialsBuilderInterface $credentialsBuilder,
        RequestStack $requestStack,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->loginService = $loginService;
        $this->userRepository = $userRepository;
        $this->credentialsBuilder = $credentialsBuilder;
        $this->requestStack = $requestStack;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CustomerBeforeLoginEvent::class => ['syncUserData', 10],
            CustomerLoginEvent::class => 'storeIdentity',
        ];
    }

    public function syncUserData(CustomerBeforeLoginEvent $event): void
    {
        if ($this->isGuestCustomerRegistration()) {
            return;
        }

        $credentials = $this->credentialsBuilder
            ->createCredentialsByEmail($event->getEmail());

        try {
            $userData = $this->loginService
                ->getUserDataBeforeLogin($credentials);
        } catch (NotFoundException $e) {
            return;
        }

        $this->userRepository->syncUser($userData);
    }

    public function storeIdentity(CustomerLoginEvent $event): void
    {
        $customer = $event->getCustomer();
        if ($customer->getGuest()) {
            return;
        }

        $credentials = $this->credentialsBuilder->createCredentialsByEmail($customer->getEmail());

        try {
            $identity = $this->loginService->getIdentityByCredentials($credentials);
        } catch (NotFoundException $e) {
            return;
        }

        $this->checkBillingAddress($identity);
        $this->checkShippingAddress($identity);

        $this->authStorageAdapter->setIdentity($identity);
    }

    /**
     * @protected
     */
    protected function checkBillingAddress(Identity $identity): void
    {
        $billingAddressId = $identity->getMainBillingAddress()->id;
        $this->userRepository->checkAddress(
            $billingAddressId,
            AddressEntity::TYPE_BILLING,
            $identity->getOwnershipContext()
        );
    }

    /**
     * @protected
     */
    protected function checkShippingAddress(Identity $identity): void
    {
        $shippingAddressId = $identity->getMainShippingAddress()->id;
        $this->userRepository->checkAddress(
            $shippingAddressId,
            AddressEntity::TYPE_SHIPPING,
            $identity->getOwnershipContext()
        );
    }

    /**
     * @internal
     */
    protected function isGuestCustomerRegistration(): bool
    {
        $request = $this->requestStack->getMasterRequest();

        return $request && (bool) $request->get('guest', false);
    }
}
