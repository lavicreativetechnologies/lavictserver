<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\Core\Framework\Struct\Struct;

class B2bIdentityIdStruct extends Struct
{
    /**
     * @var IdValue
     */
    private $identityId;

    public function __construct(IdValue $identityId)
    {
        $this->identityId = $identityId;
    }

    public function getIdentityId(): IdValue
    {
        return $this->identityId;
    }
}
