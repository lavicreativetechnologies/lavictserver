<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\UserPostalSettings;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class UserPostalSettingsMailData
{
    /**
     * @var UserPostalSettings|null
     */
    private $postalSettings;

    public function __construct(?UserPostalSettings $postalSettings)
    {
        $this->postalSettings = $postalSettings;
    }

    public static function getPostalSettingsMailDataType(): EventDataType
    {
        $settings = new ObjectType();
        $settings->add('salutation', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $settings->add('title', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $settings->add('firstName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $settings->add('lastName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $settings->add('language', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $settings->add('email', new ScalarValueType(ScalarValueType::TYPE_STRING));

        return $settings;
    }

    public function getSalutation(): string
    {
        return $this->postalSettings ? $this->postalSettings->salutation : '';
    }

    public function getTitle(): ?string
    {
        return  $this->postalSettings ? $this->postalSettings->title : '';
    }

    public function getFirstName(): string
    {
        return  $this->postalSettings ? $this->postalSettings->firstName : '';
    }

    public function getLastName(): string
    {
        return  $this->postalSettings ? $this->postalSettings->lastName : '';
    }

    public function getLanguage(): string
    {
        return  $this->postalSettings ? (string) $this->postalSettings->language->getValue() : '';
    }

    public function getEmail(): string
    {
        return  $this->postalSettings ? $this->postalSettings->email : '';
    }
}
