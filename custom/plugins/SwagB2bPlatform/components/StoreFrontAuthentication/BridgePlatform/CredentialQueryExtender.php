<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialQueryExtenderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\WrongCredentialEntityException;
use function is_a;

class CredentialQueryExtender implements CredentialQueryExtenderInterface
{
    /**
     * @param CredentialsEntity $credentialsEntity
     */
    public function extendQuery(QueryBuilder $query, AbstractCredentialsEntity $credentialsEntity): void
    {
        if (!is_a($credentialsEntity, CredentialsEntity::class)) {
            throw new WrongCredentialEntityException('The provided entity is not a ' . CredentialsEntity::class);
        }

        $query->andWhere('email = :email')
            ->setParameter('email', $credentialsEntity->email);

        if ($credentialsEntity->customerScope) {
            $query->andWhere('sales_channel_id = :salesChannelId')
                ->setParameter('salesChannelId', $credentialsEntity->salesChannelId->getStorageValue());
        }
    }
}
