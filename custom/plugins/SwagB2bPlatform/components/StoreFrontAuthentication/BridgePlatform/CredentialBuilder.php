<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserRepositoryInterface;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class CredentialBuilder implements CredentialsBuilderInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ContextProvider $contextProvider,
        SystemConfigService $systemConfigService
    ) {
        $this->userRepository = $userRepository;
        $this->contextProvider = $contextProvider;
        $this->systemConfigService = $systemConfigService;
    }

    public function createCredentials(array $parameters): AbstractCredentialsEntity
    {
        $entity = new CredentialsEntity();

        $entity->email = $parameters['email'];
        $entity->salesChannelId = IdValue::create($this->contextProvider->getSalesChannelContext()->getSalesChannel()->getId());
        $entity->customerScope = $this->systemConfigService->get('core.systemWideLoginRegistration.isCustomerBoundToSalesChannel');

        return $entity;
    }

    public function createCredentialsByEmail(string $email = null): AbstractCredentialsEntity
    {
        $entity = new CredentialsEntity();

        $entity->email = $email;
        $entity->salesChannelId = IdValue::create($this->contextProvider->getSalesChannelContext()->getSalesChannel()->getId());
        $entity->customerScope = $this->systemConfigService->get('core.systemWideLoginRegistration.isCustomerBoundToSalesChannel');

        return $entity;
    }

    public function createCredentialsByUserId(IdValue $userId): AbstractCredentialsEntity
    {
        $user = $this->userRepository->fetchOneById($userId);
        $entity = new CredentialsEntity();

        $entity->email = $user['email'];
        $entity->salesChannelId = IdValue::create($this->contextProvider->getSalesChannelContext()->getSalesChannel()->getId());
        $entity->customerScope = $this->systemConfigService->get('core.systemWideLoginRegistration.isCustomerBoundToSalesChannel');

        return $entity;
    }
}
