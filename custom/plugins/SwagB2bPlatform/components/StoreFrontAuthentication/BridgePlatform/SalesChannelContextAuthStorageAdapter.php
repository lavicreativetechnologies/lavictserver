<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\Framework\StorageInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\NoIdentitySetException;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;
use Shopware\Core\Checkout\Customer\Event\CustomerLogoutEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SalesChannelContextAuthStorageAdapter implements AuthStorageAdapterInterface, EventSubscriberInterface
{
    public const IDENTITY_ID_KEY = 'b2b_front_auth_identity_id';

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var StoreFrontAuthenticationRepository
     */
    private $storefrontAuthRepository;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var AuthenticationIdentityLoaderInterface
     */
    private $chainIdentityLoader;

    /**
     * @var Identity
     */
    private $identity;

    /**
     * @var bool
     */
    private $isLoggedIn;

    /**
     * @var StorageInterface
     */
    private $storage;

    public function __construct(
        ContextProvider $contextProvider,
        StoreFrontAuthenticationRepository $storefrontAuthRepository,
        LoginContextService $loginContextService,
        AuthenticationIdentityLoaderInterface $chainIdentityLoader,
        StorageInterface $storage
    ) {
        $this->contextProvider = $contextProvider;
        $this->storefrontAuthRepository = $storefrontAuthRepository;
        $this->loginContextService = $loginContextService;
        $this->chainIdentityLoader = $chainIdentityLoader;
        $this->storage = $storage;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CustomerLogoutEvent::class => 'unsetIdentity',
        ];
    }

    public function unsetIdentity(): void
    {
        // $this->identity = null;
        // $this->storage->remove(self::IDENTITY_ID_KEY);
    }

    public function setIdentity(Identity $identity): void
    {
        $this->identity = $identity;
        $this->storage->set(self::IDENTITY_ID_KEY, new B2bIdentityIdStruct($identity->getAuthId()));
    }

    public function getIdentity(): Identity
    {
        if ($this->identity) {
            return $this->identity;
        }

        $identityIdStruct = $this->storage->get(self::IDENTITY_ID_KEY);

        if (!$identityIdStruct) {
            throw new NoIdentitySetException('Sales channel context does not have a stored identity');
        }

        $authEntity = $this->storefrontAuthRepository->fetchAuthenticationById($identityIdStruct->getIdentityId());

        return $this->identity = $this->chainIdentityLoader->fetchIdentityByAuthentication($authEntity, $this->loginContextService);
    }

    public function isAuthenticated(): bool
    {
        if ($this->isLoggedIn) {
            return $this->isLoggedIn;
        }

        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        return $this->isLoggedIn = (bool) $salesChannelContext->getCustomer();
    }
}
