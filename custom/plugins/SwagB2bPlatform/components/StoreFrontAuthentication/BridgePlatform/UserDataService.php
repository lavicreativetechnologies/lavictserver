<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Contact\Framework\ContactIdentity;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserDataServiceInterface;

class UserDataService implements UserDataServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        UserRepository $userRepository,
        ContextProvider $contextProvider
    ) {
        $this->userRepository = $userRepository;
        $this->contextProvider = $contextProvider;
    }

    public function transformIdentityToUserData(Identity $identity): array
    {
        $loginCredentials = $identity->getLoginCredentials();
        $orderCredentials = $identity->getOrderCredentials();
        $loginContext = $this->createLoginContextByIdentity($identity);
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        return [
            'password' => $loginCredentials->password,
            'active' => $loginCredentials->active ? 1 : 0,
            'first_name' => $identity->getEntity()->firstName,
            'last_name' => $identity->getEntity()->lastName,
            'salutation_id' => $this->getSalutationIdByIdentity($identity)->getStorageValue(),
            'title' => $identity->getEntity()->title,
            'customer_number' => $orderCredentials->customerNumber,
            'default_billing_address_id' => $identity->getMainBillingAddress()->id->getStorageValue(),
            'default_shipping_address_id' => $identity->getMainShippingAddress()->id->getStorageValue(),
            'sales_channel_id' => $loginContext->salesChannelId->getStorageValue(),
            'customer_group_id' => $loginContext->customerGroupId->getStorageValue(),
            'default_payment_method_id' => $loginContext->paymentId->getStorageValue(),
            'email' => $identity->getEntity()->email,
            'language_id' => IdValue::create($salesChannelContext->getSalesChannel()->getLanguageId())->getStorageValue(),
        ];
    }

    /**
     * @internal
     */
    protected function createLoginContextByIdentity(Identity $identity): CustomerLoginContext
    {
        $loginContext = $this->userRepository
            ->fetchOwnerLoginDataByIdentity($identity);

        return new CustomerLoginContext(
            IdValue::create($loginContext['sales_channel_id']),
            IdValue::create($loginContext['customer_group_id']),
            IdValue::create($loginContext['default_payment_method_id']),
            $identity->getAvatar()
        );
    }

    /**
     * @internal
     */
    protected function getSalutationIdByIdentity(Identity $identity): IdValue
    {
        if ($identity instanceof ContactIdentity) {
            $salutationKey = $identity->getEntity()->salutation;

            return $this->userRepository->fetchSalutationIdByKey($salutationKey);
        }

        return $this->userRepository
            ->fetchSalutationIdByEmail($identity->getEntity()->email);
    }
}
