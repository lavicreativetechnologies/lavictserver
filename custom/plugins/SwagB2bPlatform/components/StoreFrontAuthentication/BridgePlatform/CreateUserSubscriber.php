<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginService;
use Shopware\Core\Checkout\Customer\Event\CustomerLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreateUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoginService
     */
    private $loginService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    public function __construct(
        LoginService $loginService,
        UserRepository $userRepository,
        CredentialsBuilderInterface $credentialsBuilder
    ) {
        $this->loginService = $loginService;
        $this->userRepository = $userRepository;
        $this->credentialsBuilder = $credentialsBuilder;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CustomerLoginEvent::class => 'syncUserData',
        ];
    }

    public function syncUserData(CustomerLoginEvent $args): void
    {
        $customer = $args->getCustomer();

        if (!$customer) {
            return;
        }

        $credentials = $this->credentialsBuilder->createCredentialsByEmail($customer->getEmail());

        try {
            $userData = $this->loginService
                ->getUserDataBeforeLogin($credentials);
        } catch (NotFoundException $e) {
            return;
        }

        $this->userRepository->syncUser($userData);
    }
}
