<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AvatarRepositoryInterface;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

class AvatarRepository implements AvatarRepositoryInterface
{
    const TABLE_NAME = 'b2b_store_front_auth';
    const TABLE_ALIAS = 'auth';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityRepositoryInterface
     */
    private $mediaRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        Connection $connection,
        EntityRepositoryInterface $mediaRepository,
        ContextProvider $contextProvider
    ) {
        $this->connection = $connection;
        $this->mediaRepository = $mediaRepository;
        $this->contextProvider = $contextProvider;
    }

    public function syncAvatarImage(IdValue $authId, IdValue $mediaId): void
    {
        $this->connection->createQueryBuilder()
            ->update(self::TABLE_NAME, self::TABLE_ALIAS)
            ->set('media_id', ':mediaId')
            ->where('id = :id')
            ->setParameters([
                'id' => $authId->getStorageValue(),
                'mediaId' => $mediaId->getStorageValue(),
            ])
            ->execute();
    }

    public function fetchAvatarByAuthId(IdValue $authId): string
    {
        $mediaId = (string) $this->connection->createQueryBuilder()
            ->select('media.id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'media', 'media', 'media.id = media_id')
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $authId->getStorageValue())
            ->execute()
            ->fetchColumn();

        if (!$mediaId) {
            return '';
        }

        $mediaId = IdValue::create($mediaId)->getValue();
        $criteria = new Criteria([$mediaId]);

        /** @var MediaEntity|null $currentMedia */
        $currentMedia = $this->mediaRepository
            ->search($criteria, $this->contextProvider->getContext())
            ->get($mediaId);

        if (!$currentMedia) {
            return '';
        }

        return $currentMedia->getUrl();
    }
}
