<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Shopware\Core\Framework\Routing\RequestTransformerInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestTransformerDecorator implements RequestTransformerInterface
{
    /**
     * @var Request|null
     */
    private $initialRequest;

    /**
     * @var RequestTransformerInterface
     */
    private $decorated;

    public function __construct(
        RequestTransformerInterface $decorated
    ) {
        $this->decorated = $decorated;
    }

    public function transform(Request $request): Request
    {
        $this->initialRequest = $this->decorated->transform($request);

        return $this->initialRequest;
    }

    public function extractInheritableAttributes(Request $sourceRequest): array
    {
        return $this->decorated->extractInheritableAttributes($sourceRequest);
    }

    public function getInitialRequest(): ?Request
    {
        return $this->initialRequest;
    }
}
