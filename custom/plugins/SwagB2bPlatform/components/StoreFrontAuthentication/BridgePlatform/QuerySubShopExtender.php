<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\BridgePlatform;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\QuerySubShopExtenderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;
use Shopware\Core\Checkout\Customer\CustomerDefinition;
use function sprintf;

class QuerySubShopExtender implements QuerySubShopExtenderInterface
{
    private const DEBTOR_AUTH_TABLE_ALIAS = 'debtorAuth';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    /**
     * @throws Exception
     */
    public function extendQuery(
        QueryBuilder $query,
        AbstractCredentialsEntity $credentialsEntity,
        string $authAlias
    ): void {
        if (!ShopwareVersion::isShopware6_3_4() || !$credentialsEntity->customerScope) {
            return;
        }

        $this->bindSalesChannelToQuery($query, $credentialsEntity->salesChannelId);
    }

    /**
     * @internal
     */
    protected function bindSalesChannelToQuery(QueryBuilder $query, IdValue $salesChannelId): void
    {
        $query
            ->innerJoin(
                StoreFrontAuthenticationRepository::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_NAME,
                self::DEBTOR_AUTH_TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.context_owner_id = ' . self::DEBTOR_AUTH_TABLE_ALIAS . '.id'
            )
            ->innerJoin(
                self::DEBTOR_AUTH_TABLE_ALIAS,
                CustomerDefinition::ENTITY_NAME,
                CustomerDefinition::ENTITY_NAME,
                $this->getBoundSalesChannelJoinCondition()
            )
            ->andWhere(CustomerDefinition::ENTITY_NAME . '.bound_sales_channel_id = :boundSalesChannelId OR ' . $query->expr()->isNull(CustomerDefinition::ENTITY_NAME . '.bound_sales_channel_id'))
            ->setParameter('boundSalesChannelId', $salesChannelId->getStorageValue());
    }

    /**
     * @internal
     */
    protected function getBoundSalesChannelJoinCondition(): string
    {
        return sprintf(
            'UNHEX(%s.provider_context) = %s.id',
            self::DEBTOR_AUTH_TABLE_ALIAS,
            CustomerDefinition::ENTITY_NAME
        );
    }
}
