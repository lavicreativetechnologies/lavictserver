<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

class OwnershipContext
{
    /**
     * @var string
     */
    public $shopOwnerEmail;

    /**
     * @var IdValue
     */
    public $shopOwnerUserId;

    /**
     * @var IdValue
     */
    public $identityId;

    /**
     * @var string
     */
    public $identityClassName;

    /**
     * @var IdValue
     */
    public $authId;

    /**
     * @var IdValue
     */
    public $contextOwnerId;

    public function __construct(
        IdValue $authId,
        IdValue $contextOwnerId,
        string $shopOwnerEmail,
        IdValue $shopOwnerUserId,
        IdValue $identityId,
        string $identityClassName
    ) {
        $this->authId = $authId;
        $this->contextOwnerId = $contextOwnerId;
        $this->shopOwnerEmail = $shopOwnerEmail;
        $this->shopOwnerUserId = $shopOwnerUserId;
        $this->identityId = $identityId;
        $this->identityClassName = $identityClassName;
    }
}
