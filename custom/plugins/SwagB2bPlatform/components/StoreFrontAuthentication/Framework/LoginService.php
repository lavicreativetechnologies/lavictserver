<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\Repository\NotFoundException;

class LoginService
{
    /**
     * @var IdentityChainIdentityLoader
     */
    private $identityChainRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var UserDataServiceInterface
     */
    private $userDataService;

    public function __construct(
        IdentityChainIdentityLoader $identityChainRepository,
        AuthStorageAdapterInterface $authStorageAdapter,
        LoginContextService $loginContextService,
        UserDataServiceInterface $userDataService
    ) {
        $this->identityChainRepository = $identityChainRepository;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->loginContextService = $loginContextService;
        $this->userDataService = $userDataService;
    }

    public function getUserDataBeforeLogin(AbstractCredentialsEntity $credentials): array
    {
        $identity = $this->identityChainRepository
            ->fetchIdentityByCredentials($credentials, $this->loginContextService);

        return $this->userDataService->transformIdentityToUserData($identity);
    }

    public function setIdentityFor(AbstractCredentialsEntity $credentialsEntity): void
    {
        try {
            $identity = $this->getIdentityByCredentials($credentialsEntity);
        } catch (NotFoundException $e) {
            return;
        }

        $this->authStorageAdapter
            ->setIdentity($identity);
    }

    public function getIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity): Identity
    {
        return $this->identityChainRepository
            ->fetchIdentityByCredentials($credentialsEntity, $this->loginContextService);
    }
}
