<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use function array_keys;

class OwnerToContextOwnerTransferCollector implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $service = $container->findDefinition('b2b_front_auth.transfer_owner_data_to_context_owner_service');
        $tags = $container->findTaggedServiceIds('b2b_front_auth.transfer_owner_data_to_context_owner');
        $serviceIds = array_keys($tags);

        $repositories = [];
        foreach ($serviceIds as $serviceId) {
            $repositories[] = new Reference($serviceId);
        }

        $service->replaceArgument(0, $repositories);
    }
}
