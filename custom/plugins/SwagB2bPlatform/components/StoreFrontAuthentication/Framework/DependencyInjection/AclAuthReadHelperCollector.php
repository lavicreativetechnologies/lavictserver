<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use function array_keys;

class AclAuthReadHelperCollector implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container): void
    {
        $loader = $container->findDefinition('b2b_front_auth.acl_auth_read_helper_loader');
        $tags = $container->findTaggedServiceIds('b2b_front_auth.acl_auth_read_helper');
        $serviceIds = array_keys($tags);

        $readHelpers = [];
        foreach ($serviceIds as $serviceId) {
            $readHelpers[] = new Reference($serviceId);
        }

        $loader->replaceArgument(0, $readHelpers);
    }
}
