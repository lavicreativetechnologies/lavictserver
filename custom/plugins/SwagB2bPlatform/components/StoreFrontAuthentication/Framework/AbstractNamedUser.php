<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

abstract class AbstractNamedUser
{
    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
