<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

interface UserDataServiceInterface
{
    public function transformIdentityToUserData(Identity $identity): array;
}
