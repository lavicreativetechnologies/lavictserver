<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\IdValue;

interface CredentialsBuilderInterface
{
    public function createCredentials(array $parameters): AbstractCredentialsEntity;

    public function createCredentialsByEmail(string $email): AbstractCredentialsEntity;

    public function createCredentialsByUserId(IdValue $userId): AbstractCredentialsEntity;
}
