<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

abstract class AbstractCredentialsEntity
{
    /**
     * @var string
     */
    public $email;
}
