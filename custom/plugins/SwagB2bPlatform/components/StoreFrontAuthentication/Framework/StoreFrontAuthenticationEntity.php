<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;

class StoreFrontAuthenticationEntity implements Entity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var IdValue
     */
    public $contextOwnerId;

    /**
     * @var string
     */
    public $providerKey;

    /**
     * @var IdValue
     */
    public $providerContext;

    /**
     * @var IdValue
     */
    public $mediaId;

    public function fromDatabaseArray(array $data): Entity
    {
        $this->id = IdValue::create($data['id']);
        $this->contextOwnerId = IdValue::create($data['context_owner_id']);
        $this->providerKey = $data['provider_key'];
        $this->providerContext = IdValue::create($data['provider_context']);
        $this->mediaId = IdValue::create($data['media_id']);

        return $this;
    }
}
