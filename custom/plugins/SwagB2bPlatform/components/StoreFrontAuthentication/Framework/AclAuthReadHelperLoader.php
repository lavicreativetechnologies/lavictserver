<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Doctrine\DBAL\Query\QueryBuilder;

class AclAuthReadHelperLoader implements AclAuthReadHelperInterface
{
    /**
     * @var AclAuthReadHelperInterface[]
     */
    private $aclAuthHelpers;

    public function __construct(array $aclAuthHelpers)
    {
        $this->aclAuthHelpers = $aclAuthHelpers;
    }

    public function applyAclVisibility(OwnershipContext $ownershipContext, QueryBuilder $queryBuilder, string $alias = 'aclQuery'): void
    {
        foreach ($this->aclAuthHelpers as $authHelper) {
            $authHelper->applyAclVisibility($ownershipContext, $queryBuilder, $alias);
        }
    }
}
