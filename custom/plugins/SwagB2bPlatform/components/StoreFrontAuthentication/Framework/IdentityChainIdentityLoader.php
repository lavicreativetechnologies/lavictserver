<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\Repository\NotFoundException;
use function sprintf;

class IdentityChainIdentityLoader implements AuthenticationIdentityLoaderInterface
{
    /**
     * @var AuthenticationIdentityLoaderInterface[]
     */
    private $authenticationRepositories;

    /**
     * @param AuthenticationIdentityLoaderInterface[] $authenticationRepositories
     */
    public function __construct(array $authenticationRepositories)
    {
        $this->setRepositories($authenticationRepositories);
    }

    /**
     * @param AuthenticationIdentityLoaderInterface[] $authenticationRepositories
     */
    public function setRepositories(array $authenticationRepositories): void
    {
        $this->authenticationRepositories = [];

        foreach ($authenticationRepositories as $repository) {
            $this->addRepository($repository);
        }
    }

    public function addRepository(AuthenticationIdentityLoaderInterface $repository): void
    {
        $this->authenticationRepositories[] = $repository;
    }

    public function fetchIdentityByEmail(string $email, LoginContextService $contextService, bool $isApi = false): Identity
    {
        foreach ($this->authenticationRepositories as $repository) {
            try {
                return $repository->fetchIdentityByEmail($email, $contextService);
            } catch (NotFoundException $e) {
                continue;
            }
        }

        throw new NotFoundException(sprintf('No identity found with email %s', $email));
    }

    public function fetchIdentityByAuthentication(StoreFrontAuthenticationEntity $authentication, LoginContextService $contextService, bool $isApi = false): Identity
    {
        foreach ($this->authenticationRepositories as $repository) {
            try {
                return $repository->fetchIdentityByAuthentication($authentication, $contextService);
            } catch (NotFoundException $e) {
                continue;
            }
        }

        throw new NotFoundException(sprintf('No identity found from %s and id %s', $authentication->providerKey, $authentication->providerContext->getValue()));
    }

    public function fetchIdentityByCredentials(AbstractCredentialsEntity $credentialsEntity, LoginContextService $contextService, bool $isApi = false): Identity
    {
        foreach ($this->authenticationRepositories as $repository) {
            try {
                return $repository->fetchIdentityByCredentials($credentialsEntity, $contextService);
            } catch (NotFoundException $e) {
                continue;
            }
        }

        throw new NotFoundException('No identity found with the credentials');
    }
}
