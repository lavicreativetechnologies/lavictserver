<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Doctrine\DBAL\Query\QueryBuilder;

interface CredentialQueryExtenderInterface
{
    public function extendQuery(QueryBuilder $query, AbstractCredentialsEntity $credentialsEntity);
}
