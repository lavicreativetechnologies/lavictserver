<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;

class StoreFrontAuthenticationRepository
{
    public const TABLE_NAME = 'b2b_store_front_auth';

    public const TABLE_ALIAS = 'auth';

    const CONTEXT_OWNER_ID_PLACEHOLDER = '__contextOwnerId__';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function fetchIdByProviderData(string $providerKey, IdValue $providerContext): IdValue
    {
        return IdValue::create($this->connection
            ->fetchColumn('SELECT id FROM ' . self::TABLE_NAME . ' WHERE provider_key = :providerKey AND provider_context = :providerContext', [
                'providerKey' => $providerKey,
                'providerContext' => $providerContext->getValue(),  // set the provider context as string cause it can contain both Uuid and IntId
        ]));
    }

    public function fetchAuthenticationById(IdValue $authId): StoreFrontAuthenticationEntity
    {
        $data = $this->connection->fetchAssoc('SELECT * FROM ' . self::TABLE_NAME . ' WHERE id = :id', ['id' => $authId->getStorageValue()]);

        if (!$data) {
            throw new NotFoundException('Could not find a authentication with id "' . $authId->getValue() . '"');
        }

        $entity = new StoreFrontAuthenticationEntity();
        $entity->fromDatabaseArray($data);

        return $entity;
    }

    public function createAuthContextEntry(string $providerKey, IdValue $providerContext, IdValue $contextOwnerId = null): IdValue
    {
        $insertData = [
            'provider_key' => $providerKey,
            'provider_context' => $providerContext->getValue(),
            'context_owner_id' => 0,
        ];

        if ($contextOwnerId) {
            $insertData['context_owner_id'] = $contextOwnerId->getStorageValue();
        }

        $this->connection->insert(
            self::TABLE_NAME,
            $insertData
        );

        $authId = IdValue::create((int) $this->connection->lastInsertId());

        if (!$contextOwnerId) {
            $this->connection->update(
                self::TABLE_NAME,
                ['context_owner_id' => $authId->getStorageValue()],
                ['id' => $authId->getStorageValue()]
            );
        }

        return $authId;
    }

    public function removeContextOwner(OwnershipContext $ownershipContext): void
    {
        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('context_owner_id = :contextOwner')
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue())
            ->execute();
    }

    public function removeUser(IdValue $id): void
    {
        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('id = :id')
            ->setParameter('id', $id->getStorageValue())
            ->execute();
    }
}
