<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;

interface Identity
{
    public function getAuthId(): IdValue;

    public function getContextAuthId(): IdValue;

    public function getId(): IdValue;

    public function getTableName(): string;

    public function getOwnershipContext(): OwnershipContext;

    public function getLoginCredentials(): UserLoginCredentials;

    public function getPostalSettings(): UserPostalSettings;

    public function getOrderCredentials(): UserOrderCredentials;

    public function getMainShippingAddress(): UserAddress;

    public function getMainBillingAddress(): UserAddress;

    /**
     * @internal returning an Entity is like returning Mixed, please work with the Identity
     */
    public function getEntity(): Entity;

    public function getOwnershipEntity(): Entity;

    public function isSuperAdmin(): bool;

    public function isApiUser(): bool;

    public function getAvatar(): string;

    public function setAvatar(string $avatar);
}
