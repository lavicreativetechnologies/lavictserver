<?php declare(strict_types=1);

namespace Shopware\B2B\StoreFrontAuthentication\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\ShopwareCore\DependencyInjection\ShopwareCoreConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Shop\Bridge\DependencyInjection\ShopBridgeConfiguration;
use Shopware\B2B\StoreFrontAuthentication\BridgePlatform\DependencyInjection\StoreFrontAuthenticationBridgeConfiguration as PlatformStoreFrontAuthenticationBridgeConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class StoreFrontAuthenticationBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformStoreFrontAuthenticationBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            ShopBridgeConfiguration::create(),
            new ShopwareCoreConfiguration(),
        ];
    }
}
