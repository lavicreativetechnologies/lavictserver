<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

interface CartDestination
{
    public function getName(): string;
}
