<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemCheckoutSource;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\OrderClearance\BridgePlatform\OrderClearanceAcceptedMailEvent;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceRepository;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrderClearanceFinishSubscriber extends CartFinishSubscriber
{
    /**
     * @var OrderClearanceRepository
     */
    private $orderClearanceRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var TotalPriceCalculationService
     */
    private $totalPriceCalculationService;

    public function __construct(
        OrderClearanceRepository $orderClearanceRepository,
        AuthenticationService $authenticationService,
        EventDispatcherInterface $eventDispatcher,
        ContextProvider $contextProvider,
        LineItemListService $lineItemListService,
        TotalPriceCalculationService $totalPriceCalculationService
    ) {
        parent::__construct($eventDispatcher);

        $this->orderClearanceRepository = $orderClearanceRepository;
        $this->authenticationService = $authenticationService;
        $this->contextProvider = $contextProvider;
        $this->lineItemListService = $lineItemListService;
        $this->totalPriceCalculationService = $totalPriceCalculationService;
    }

    protected function onCartFinish(Cart $cart, OrderEntity $orderEntity, SalesChannelContext $salesChannelContext): ?OrderContext
    {
        if (!$this->authenticationService->isB2b()) {
            return null;
        }

        $cartDestination = $this->getCartState()->getDestination();
        if (!$cartDestination instanceof OrderClearanceFinishDestination) {
            return null;
        }

        $orderNumber = $orderEntity->getOrderNumber();
        $orderClearance = $cartDestination->getOrderClearance();
        $identity = $this->authenticationService->getIdentity();

        if (!$orderClearance) {
            return null;
        }

        $this->updateFromCart($orderClearance, $cart, $identity->getOwnershipContext(), $salesChannelContext);
        $this->finishOrderClearance($orderClearance, $orderNumber, $identity->getOwnershipContext());
        $this->dispatchOrderClearanceAccepted($orderClearance);

        return $orderClearance;
    }

    /**
     * @internal
     */
    protected function updateFromCart(OrderClearanceEntity $orderClearance, Cart $cart, OwnershipContext $ownershipContext, SalesChannelContext $salesChannelContext): void
    {
        $this->lineItemListService
            ->updateListThroughCheckoutSource(
                $orderClearance->list,
                new LineItemCheckoutSource($cart),
                $ownershipContext
            );

        $orderClearance->billingAddressId = IdValue::create($salesChannelContext->getCustomer()->getActiveBillingAddress()->getId());
        $orderClearance->shippingAddressId = IdValue::create($salesChannelContext->getCustomer()->getActiveShippingAddress()->getId());
        $orderClearance->shippingId = IdValue::create($salesChannelContext->getShippingMethod()->getId());
        $orderClearance->paymentId = IdValue::create($salesChannelContext->getPaymentMethod()->getId());
        $orderClearance->shippingAmount = $this->totalPriceCalculationService->getTotalAmount($cart->getShippingCosts(), $salesChannelContext);
        $orderClearance->shippingAmountNet = $this->totalPriceCalculationService->getTotalAmountNet($cart->getShippingCosts(), $salesChannelContext);
    }

    /**
     * @internal
     */
    protected function finishOrderClearance(OrderClearanceEntity $orderClearance, string $originalOrderNumber, OwnershipContext $context): void
    {
        /* @todo missing sync order to line item list */
        $orderClearance->orderNumber = $originalOrderNumber;

        $this->orderClearanceRepository
            ->acceptOrderClearance($orderClearance, $context);
    }

    /**
     * @internal
     */
    protected function dispatchOrderClearanceAccepted(OrderClearanceEntity $clearanceEntity): void
    {
        $this->eventDispatcher->dispatch(
            new OrderClearanceAcceptedMailEvent(
                $clearanceEntity,
                $clearanceEntity->orderNumber,
                $this->contextProvider->getSalesChannelContext()
            )
        );
    }
}
