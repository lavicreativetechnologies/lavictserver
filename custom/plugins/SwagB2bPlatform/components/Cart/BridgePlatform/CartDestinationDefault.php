<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

class CartDestinationDefault implements CartDestination
{
    public function getName(): string
    {
        return 'default';
    }
}
