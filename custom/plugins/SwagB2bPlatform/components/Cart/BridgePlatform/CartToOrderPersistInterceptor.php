<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\Error\Error;
use Shopware\Core\Checkout\Cart\Exception\InvalidCartException;
use Shopware\Core\Checkout\Cart\Order\OrderPersisterInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CartToOrderPersistInterceptor implements OrderPersisterInterface
{
    /**
     * @var OrderPersisterInterface
     */
    private $decoratedOrderPersister;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(
        OrderPersisterInterface $decoratedOrderPersister,
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher
    ) {
        $this->decoratedOrderPersister = $decoratedOrderPersister;
        $this->dispatcher = $dispatcher;
        $this->container = $container;
    }

    public function persist(Cart $cart, SalesChannelContext $context): string
    {
        try {
            return $this->decoratedOrderPersister->persist($cart, $context);
        } catch (InvalidCartException $e) {
        }

        if (!$this->getAuthenticationService()->isB2b()) {
            throw $e;
        }

        if (!$this->hasB2bErrors($cart)) {
            throw $e;
        }

        $this->dispatcher
            ->dispatch(new CartInterceptEvent($cart, $context));

        throw $e;
    }

    /**
     * @internal
     */
    protected function hasB2bErrors(Cart $cart): bool
    {
        /** @var Error $error */
        foreach ($cart->getErrors() as $error) {
            if ($error instanceof CartAccessError && $error->blockOrder()) {
                return true;
            }
        }

        return false;
    }

    protected function getAuthenticationService(): AuthenticationService
    {
        return $this->container
            ->get('b2b_front_auth.authentication_service');
    }
}
