<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\Cart\Framework\ErrorMessage;
use Shopware\B2B\Cart\Framework\InformationMessage;
use Shopware\Core\Checkout\Cart\Error\Error;
use function array_merge;
use function get_object_vars;
use function is_object;

class CartAccessError extends Error
{
    const CART_BLOCKER_ID = 'b2b-cart-clearance-block';
    const CART_CLOCKER_MESSAGE = 'cart-clearance-block';
    const SPECIAL_CASE_TYPE_TIME_RESTRICTION = 'timeRestriction';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $messageKey;

    /**
     * @var int
     */
    private $level;

    /**
     * @var bool
     */
    private $blockOrder;

    /**
     * @var array
     */
    private $parameters;

    public function __construct(string $id, string $messageKey, int $level, bool $blockOrder, array $parameters = [])
    {
        $this->id = $id;
        $this->messageKey = $messageKey;
        $this->level = $level;
        $this->blockOrder = $blockOrder;
        $this->parameters = $parameters;

        parent::__construct($messageKey);
    }

    public static function fromErrorMessage(ErrorMessage $error, int $index): CartAccessError
    {
        return new self(
            self::toIdKey($index, 'error', $error->sender),
            $error->error,
            self::LEVEL_ERROR,
            true,
            $error->params
        );
    }

    public static function fromInformationMessage(InformationMessage $info, int $index): CartAccessError
    {
        return new self(
            self::toIdKey($index, 'notice', $info->sender),
            $info->type,
            self::LEVEL_NOTICE,
            false,
            $info->params
        );
    }

    public static function createCartBlockedForClearanceMessage(): CartAccessError
    {
        return new self(
            self::CART_BLOCKER_ID,
            self::CART_CLOCKER_MESSAGE,
            self::LEVEL_WARNING,
            true,
            []
        );
    }

    private static function toIdKey(int $index, string $level, string $sender): string
    {
        return $index . '-' . $level . '-' . $sender;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getMessageKey(): string
    {
        return 'b2b.' . $this->messageKey;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function blockOrder(): bool
    {
        return $this->blockOrder;
    }

    public function getParameters(): array
    {
        return $this->reformatParameters($this->parameters);
    }

    /**
     * @internal
     */
    protected function reformatParameters(array $parameters): array
    {
        $reformatedParameters = [[]];

        foreach ($parameters as $key => $parameter) {
            if (is_object($parameter)) {
                $reformatedParameters[] = $this->reformatParameters(get_object_vars($parameter));
                continue;
            }

            if ($key === self::SPECIAL_CASE_TYPE_TIME_RESTRICTION) {
                $parameter = 'b2b.' . $parameter;
            }

            $reformatedParameters[] = ['%' . $key . '%' => $parameter];
        }

        return array_merge(...$reformatedParameters);
    }
}
