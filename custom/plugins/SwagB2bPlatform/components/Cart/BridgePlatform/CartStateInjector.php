<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @todo remove with shopware 6.1 dropped support
 */
class CartStateInjector implements CartProcessorInterface
{
    public function process(CartDataCollection $data, Cart $original, Cart $toCalculate, SalesChannelContext $context, CartBehavior $behavior): void
    {
        if ($original->hasExtension(CartState::NAME)) {
            $cartState = $original->getExtension(CartState::NAME);
        } else {
            $cartState = new CartState();
        }

        $original->addExtension(CartState::NAME, $cartState);
        $toCalculate->addExtension(CartState::NAME, $cartState);
    }
}
