<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\Core\Checkout\Cart\Error\Error;
use Shopware\Core\Framework\Struct\Collection;

/**
 * @method void       set(string $key, Error $entity)
 * @method Error[]    getIterator()
 * @method Error[]    getElements()
 * @method Error|null get(string $key)
 * @method Error|null first()
 * @method Error|null last()
 */
class CartMessageCollection extends Collection
{
    /**
     * @param Error $error
     */
    public function add($error): void
    {
        $this->set($error->getId(), $error);
    }

    public function getNotices(): array
    {
        return $this->fmap(function (Error $error): ?Error {
            return $error->getLevel() === Error::LEVEL_NOTICE ? $error : null;
        });
    }

    public function getErrors(): array
    {
        return $this->fmap(function (Error $error): ?Error {
            return $error->getLevel() === Error::LEVEL_ERROR ? $error : null;
        });
    }

    protected function getExpectedClass(): ?string
    {
        return Error::class;
    }
}
