<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class TotalPriceCalculationService
{
    public function getTotalAmount(CalculatedPrice $price, SalesChannelContext $salesChannelContext): float
    {
        if ($salesChannelContext->getCurrentCustomerGroup()->getDisplayGross()) {
            return $price->getTotalPrice();
        }

        return $price->getTotalPrice() + $price->getCalculatedTaxes()->getAmount();
    }

    public function getTotalAmountNet(CalculatedPrice $price, SalesChannelContext $salesChannelContext): float
    {
        if ($salesChannelContext->getCurrentCustomerGroup()->getDisplayGross()) {
            return $price->getTotalPrice() - $price->getCalculatedTaxes()->getAmount();
        }

        return $price->getTotalPrice();
    }
}
