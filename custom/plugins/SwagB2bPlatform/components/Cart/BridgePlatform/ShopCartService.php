<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartPersisterInterface;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class ShopCartService
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartPersisterInterface
     */
    private $cartPersister;

    public function __construct(
        CartService $cartService,
        CartPersisterInterface $cartPersister
    ) {
        $this->cartService = $cartService;
        $this->cartPersister = $cartPersister;
    }

    public function clear(SalesChannelContext $salesChannelContext): Cart
    {
        $token = $salesChannelContext->getToken();

        $this->cartPersister
            ->delete($token, $salesChannelContext);

        $cart = $this->cartService
            ->createNew($token);

        $cart->addExtension(CartState::NAME, new CartState());

        return $cart;
    }
}
