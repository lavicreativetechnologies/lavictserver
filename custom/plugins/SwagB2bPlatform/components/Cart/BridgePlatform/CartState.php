<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Framework\Struct\Struct;
use function array_keys;
use function array_pop;
use function end;
use function get_class;
use function get_object_vars;

class CartState extends Struct
{
    public const NAME = 'b2b-cart-state';

    /**
     * @var CartDestination[]
     */
    private $destinations = [];

    /**
     * @var CartMessageCollection|null
     */
    private $messageCollection;

    public function __construct()
    {
        $this->destinations[] = new CartDestinationDefault();
    }

    public static function extract(Cart $fromCart): self
    {
        return $fromCart->getExtension(self::NAME);
    }

    public function rollBackDestination(): void
    {
        array_pop($this->destinations);
    }

    public function setDestination(CartDestination $state): void
    {
        if (get_class($this->getDestination()) === get_class($state)) {
            return;
        }

        $this->destinations[] = $state;
    }

    public function getDestination(): CartDestination
    {
        return end($this->destinations);
    }

    public function hasDestination(string $destinationClass): bool
    {
        foreach ($this->destinations as $destination) {
            if ($destination instanceof $destinationClass) {
                return true;
            }
        }

        return false;
    }

    public function removeDestination(string $destinationClass): void
    {
        foreach ($this->destinations as $index => $destination) {
            if ($destination instanceof $destinationClass) {
                unset($this->destinations[$index]);

                return;
            }
        }
    }

    public function getMessages(): CartMessageCollection
    {
        if ($this->messageCollection === null) {
            $this->messageCollection = new CartMessageCollection();
        }

        return $this->messageCollection;
    }

    public function __sleep()
    {
        $properties = get_object_vars($this);
        unset($properties['messageCollection']);

        return array_keys($properties);
    }
}
