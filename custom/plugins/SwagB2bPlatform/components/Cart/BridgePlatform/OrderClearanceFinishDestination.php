<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;

class OrderClearanceFinishDestination implements CartDestination
{
    private const NAME = 'cleared-order';

    /**
     * @var OrderClearanceEntity
     */
    protected $orderClearance;

    public function __construct(OrderClearanceEntity $orderClearance)
    {
        $this->orderClearance = $orderClearance;
    }

    public function getOrderClearance(): OrderClearanceEntity
    {
        return $this->orderClearance;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
