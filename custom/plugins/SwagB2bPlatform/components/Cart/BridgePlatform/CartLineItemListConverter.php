<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\BridgePlatform;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\LineItemList\BridgePlatform\UnitPriceCalculationService;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use function array_map;
use function sprintf;

class CartLineItemListConverter
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var UnitPriceCalculationService
     */
    private $unitPriceCalculationService;

    public function __construct(
        ContextProvider $contextProvider,
        SalesChannelRepositoryInterface $productRepository,
        CartService $cartService,
        UnitPriceCalculationService $unitPriceCalculationService
    ) {
        $this->contextProvider = $contextProvider;
        $this->productRepository = $productRepository;
        $this->cartService = $cartService;
        $this->unitPriceCalculationService = $unitPriceCalculationService;
    }

    public function cartToLineItemList(Cart $cart, OwnershipContext $ownershipContext): LineItemList
    {
        $context = $this->contextProvider->getSalesChannelContext();

        $list = new LineItemList();
        $list->contextOwnerId = $ownershipContext->contextOwnerId;

        $lineItems = $cart->getLineItems();
        $products = $this->getProductsByReferenceIds($lineItems->getReferenceIds(), $context);

        $references = [];
        foreach ($lineItems->getIterator() as $lineItem) {
            if (!$lineItem->isGood()) {
                continue;
            }

            $product = $products[$lineItem->getReferencedId()] ?? null;

            if (!$product) {
                throw new NotFoundException(sprintf('Product not found with the given reference id: "%s"', $lineItem->getReferencedId()));
            }

            $reference = new LineItemReference();
            $reference->referenceNumber = $product->getProductNumber();
            $reference->name = $product->getName();
            $reference->quantity = $lineItem->getQuantity();

            $this->setReferenceAmounts($reference, $lineItem, $context);

            $reference->mode = LineItemType::create($lineItem->getType());
            $reference->maxPurchase = $product->getMaxPurchase();
            $reference->minPurchase = $product->getMinPurchase();
            $reference->inStock = $product->getStock();
            $reference->isLastStock = $product->getIsCloseout();
            $reference->unavailableBecauseOfMinPurchase = $reference->minPurchase && $reference->quantity < $reference->minPurchase;
            $reference->unavailableBecauseOfMaxPurchase = $reference->maxPurchase && $reference->quantity > $reference->maxPurchase;
            $reference->unavailableBecauseOfStock = $reference->inStock && $reference->quantity > $reference->inStock;

            $list->amount += $reference->amount * $reference->quantity;
            $list->amountNet += $reference->amountNet * $reference->quantity;

            $references[] = $reference;
        }

        $list->references = $references;

        return $list;
    }

    /**
     * @internal
     */
    protected function setReferenceAmounts(LineItemReference $reference, LineItem $lineItem, SalesChannelContext $context): void
    {
        $price = $lineItem->getPrice();
        if (!$price instanceof CalculatedPrice) {
            $reference->amount = 0;
            $reference->amountNet = 0;

            return;
        }

        $reference->amount = $this->unitPriceCalculationService->getUnitAmount($price, $context);
        $reference->amountNet = $this->unitPriceCalculationService->getUnitAmountNet($price, $context);
    }

    public function lineItemListToCart(LineItemList $list): Cart
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        $referenceNumbers = array_map(function (LineItemReference $reference) {
            return $reference->referenceNumber;
        }, $list->references);

        $products = $this->getProductsByReferenceNumbers($referenceNumbers, $salesChannelContext);

        $lineItemCollection = new LineItemCollection();
        foreach ($list->references as $reference) {
            $product = $products[$reference->referenceNumber] ?? null;

            if (!$product) {
                continue;
            }

            if ($reference->mode->getValue() === null) {
                $reference->mode = LineItemType::create(LineItem::PRODUCT_LINE_ITEM_TYPE);
            }

            $lineItem = new LineItem(
                Uuid::randomHex(),
                $reference->mode->getValue(),
                $product->getId(),
                $reference->quantity
            );

            $lineItem->setStackable(true);
            $lineItem->setRemovable(true);

            $lineItemCollection->add($lineItem);
        }

        $cart = new Cart(Uuid::randomHex(), $salesChannelContext->getToken());
        $cart->setLineItems($lineItemCollection);

        return $this->cartService->recalculate($cart, $salesChannelContext);
    }

    /**
     * @internal
     * @return ProductEntity[]
     */
    protected function getProductsByReferenceIds(array $ids, SalesChannelContext $context): array
    {
        if (empty($ids)) {
            return [];
        }

        $criteria = new Criteria($ids);
        $criteria->addAssociation('prices');

        return $this->productRepository->search($criteria, $context)->getElements();
    }

    /**
     * @internal
     * @return ProductEntity[]
     */
    protected function getProductsByReferenceNumbers(array $referenceNumbers, SalesChannelContext $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('productNumber', $referenceNumbers));
        $criteria->addAssociation('prices');

        $products = $this->productRepository->search($criteria, $context)->getElements();

        $formattedProducts = [];

        /** @var ProductEntity $product */
        foreach ($products as $product) {
            $formattedProducts[$product->getProductNumber()] = $product;
        }

        return $formattedProducts;
    }
}
