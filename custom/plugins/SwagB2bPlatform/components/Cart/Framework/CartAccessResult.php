<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Framework;

use function count;

class CartAccessResult
{
    /**
     * @var array
     */
    public $information = [];

    /**
     * @var int
     */
    public $errorCount = 0;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var bool
     */
    private $clearable = true;

    public function addError(string $sender, string $error, array $params = []): void
    {
        $this->errors[] = new ErrorMessage($sender, $error, $params);
        $this->errorCount++;
    }

    public function addInformation(string $sender, string $type, array $params): void
    {
        $this->information[] = new InformationMessage($sender, $type, $params);
    }

    /**
     * @param CartAccessResult[] $results
     */
    public function addErrors(self ...$results): void
    {
        foreach ($results as $result) {
            foreach ($result->getErrors() as $error) {
                $this->errors[] = $error;
            }
        }

        $this->errorCount = count($this->errors);
    }

    public function setClearable(bool $set = true): void
    {
        $this->clearable = $set;
    }

    public function isClearable(): bool
    {
        return $this->clearable;
    }

    /**
     * @return ErrorMessage[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return (bool) $this->errorCount;
    }
}
