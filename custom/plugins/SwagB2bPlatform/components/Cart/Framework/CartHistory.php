<?php declare(strict_types=1);

namespace Shopware\B2B\Cart\Framework;

use function property_exists;

class CartHistory
{
    /**
     * @var int
     */
    public $orderAmount;

    /**
     * @var int
     */
    public $orderQuantity;

    /**
     * @var int
     */
    public $orderItemQuantity;

    /**
     * @var string
     */
    public $timeRestriction;

    public function fromDatabaseArray(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }
    }
}
