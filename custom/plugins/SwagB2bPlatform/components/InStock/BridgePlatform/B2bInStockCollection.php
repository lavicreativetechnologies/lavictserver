<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void                  add(B2bInStockEntity $entity)
 * @method void                  set(string $key, B2bInStockEntity $entity)
 * @method B2bInStockEntity[]    getIterator()
 * @method B2bInStockEntity[]    getElements()
 * @method B2bInStockEntity|null get(string $key)
 * @method B2bInStockEntity|null first()
 * @method B2bInStockEntity|null last()
 */
class B2bInStockCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return B2bInStockEntity::class;
    }

    public function getBestMatchByAuthId(IdValue $authId): int
    {
        $bestMatch = null;

        /** @var B2bInStockEntity $entity */
        foreach ($this as $entity) {
            if ($bestMatch === null || $authId->getValue() === $entity->getAuthId()) {
                $bestMatch = $entity->getInStock();
            }
        }

        return $bestMatch;
    }
}
