<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use Doctrine\DBAL\Connection;
use ONGR\ElasticsearchDSL\Query\Compound\BoolQuery;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Elasticsearch\Framework\AbstractElasticsearchDefinition;
use Shopware\Elasticsearch\Framework\FullText;
use Shopware\Elasticsearch\Framework\Indexing\EntityMapper;

class ElasticsearchProductDefinition extends AbstractElasticsearchDefinition
{
    private const EXTENSION_NAME = SalesChannelProductExtension::B2B_IN_STOCK_EXTENSION_NAME;
    private const IN_STOCK_FIELD = 'inStock';
    private const AUTH_ID_FIELD = 'authId';

    /**
     * @var AbstractElasticsearchDefinition
     */
    private $decorated;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(
        AbstractElasticsearchDefinition $decorated,
        EntityMapper $mapper,
        Connection $connection
    ) {
        parent::__construct($mapper);
        $this->decorated = $decorated;
        $this->connection = $connection;
    }

    public function getEntityDefinition(): EntityDefinition
    {
        return $this->decorated->getEntityDefinition();
    }

    public function extendCriteria(Criteria $criteria): void
    {
        $this->decorated->extendCriteria($criteria);
        $criteria->addAssociation(self::EXTENSION_NAME);
    }

    public function getMapping(Context $context): array
    {
        $mapping = $this->decorated->getMapping($context);
        $definition = $this->decorated->getEntityDefinition();

        $mapping['properties'][self::EXTENSION_NAME] = $this->mapper->mapField(
            $definition,
            $definition->getField(self::EXTENSION_NAME),
            $context
        );

        return $mapping;
    }

    public function buildTermQuery(Context $context, Criteria $criteria): BoolQuery
    {
        return $this->decorated->buildTermQuery($context, $criteria);
    }

    public function extendEntities(EntityCollection $collection): EntityCollection
    {
        return $this->decorated->extendEntities($collection);
    }

    public function extendDocuments(array $documents, Context $context): array
    {
        return $this->decorated->extendDocuments($documents, $context);
    }

    public function fetch(array $ids, Context $context): array
    {
        $documents = $this->decorated->fetch($ids, $context);

        $query = <<<SQL
SELECT
    LOWER(HEX(product_id)) as id,
    auth_id,
    in_stock
FROM b2b_in_stocks
WHERE
    product_id IN (:ids)
    AND product_version_id = :liveVersion
SQL;

        $associationData = $this->connection->fetchAllAssociativeIndexed(
            $query,
            [
                'ids' => $ids,
                'liveVersion' => Uuid::fromHexToBytes(Defaults::LIVE_VERSION),
            ],
            [
                'ids' => Connection::PARAM_STR_ARRAY,
            ]
        );

        foreach ($associationData as $productId => $inStockData) {
            $documents[$productId][self::EXTENSION_NAME][] = [
                self::AUTH_ID_FIELD => $inStockData['auth_id'],
                self::IN_STOCK_FIELD => $inStockData['in_stock'],
            ];
        }

        return $documents;
    }

    public function hasNewIndexerPattern(): bool
    {
        return $this->decorated->hasNewIndexerPattern();
    }

    public function buildFullText(Entity $entity): FullText
    {
        return $this->decorated->buildFullText($entity);
    }
}
