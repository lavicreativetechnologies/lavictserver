<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\BridgePlatform;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;

class B2bInStockEntity extends Entity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $authId;

    /**
     * @var int
     */
    protected $inStock;

    /**
     * @var string
     */
    protected $productId;

    /**
     * @var ProductEntity
     */
    protected $product;

    public function getId(): int
    {
        return $this->id;
    }

    public function getAuthId(): int
    {
        return $this->authId;
    }

    public function getInStock(): int
    {
        return $this->inStock;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getProduct(): ProductEntity
    {
        return $this->product;
    }

    public function setProduct(ProductEntity $product): void
    {
        $this->product = $product;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setAuthId(int $authId): void
    {
        $this->authId = $authId;
    }

    public function setInStock(int $inStock): void
    {
        $this->inStock = $inStock;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }
}
