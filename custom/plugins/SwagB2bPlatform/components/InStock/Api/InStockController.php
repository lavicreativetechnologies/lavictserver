<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Api;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\InStock\Framework\InStockCrudService;
use Shopware\B2B\InStock\Framework\InStockRepository;
use Shopware\B2B\InStock\Framework\InStockSearchStruct;

class InStockController
{
    /**
     * @var InStockRepository
     */
    private $inStockRepository;

    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var InStockCrudService
     */
    private $inStockCrudService;

    public function __construct(
        InStockRepository $inStockRepository,
        GridHelper $requestHelper,
        InStockCrudService $inStockCrudService
    ) {
        $this->inStockRepository = $inStockRepository;
        $this->requestHelper = $requestHelper;
        $this->inStockCrudService = $inStockCrudService;
    }

    public function getListAction(string $authId, Request $request): array
    {
        $authId = IdValue::create($authId);
        $search = new InStockSearchStruct();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $inStocks = $this->inStockRepository->fetchInStocksByAuthId($authId, $search);

        $totalCount = $this->inStockRepository->fetchTotalCount($authId, $search);

        return ['success' => true, 'inStocks' => $inStocks, 'totalCount' => $totalCount];
    }

    public function getAction(string $inStockId): array
    {
        $inStockId = IdValue::create($inStockId);
        $inStock = $this->inStockRepository
            ->fetchOneById($inStockId);

        return ['success' => true, 'inStock' => $inStock];
    }

    public function createAction(Request $request): array
    {
        $newRecord = $this->inStockCrudService
            ->createNewRecordRequest($request->getPost());

        $inStock = $this->inStockCrudService
            ->create($newRecord);

        return ['success' => true, 'inStock' => $inStock];
    }

    public function updateAction(string $inStockId, Request $request): array
    {
        $inStockId = IdValue::create($inStockId);
        $data = $request->getPost();
        $data['id'] = $inStockId;

        $existingRecord = $this->inStockCrudService
            ->createExistingRecordRequest($data);

        $inStock = $this->inStockCrudService
            ->update($existingRecord);

        return ['success' => true, 'inStock' => $inStock];
    }

    public function removeAction(string $inStockId): array
    {
        $inStockId = IdValue::create($inStockId);
        $inStock = $this->inStockCrudService
            ->remove($inStockId);

        return ['success' => true, 'inStock' => $inStock];
    }
}
