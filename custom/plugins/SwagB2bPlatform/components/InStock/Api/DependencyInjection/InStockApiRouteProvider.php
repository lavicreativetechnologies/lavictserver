<?php declare(strict_types=1);

namespace Shopware\B2B\InStock\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class InStockApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/in_stock/auth_id/{authId}',
                'b2b_in_stock.api_controller',
                'getList',
                ['authId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/in_stock',
                'b2b_in_stock.api_controller',
                'create',
                [],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/in_stock/{inStockId}',
                'b2b_in_stock.api_controller',
                'get',
                ['inStockId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/in_stock/{inStockId}',
                'b2b_in_stock.api_controller',
                'update',
                ['inStockId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/in_stock/{inStockId}',
                'b2b_in_stock.api_controller',
                'remove',
                ['inStockId'],
            ],
        ];
    }
}
