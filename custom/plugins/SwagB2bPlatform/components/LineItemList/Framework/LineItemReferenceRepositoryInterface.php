<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyContext;

interface LineItemReferenceRepositoryInterface extends GridRepository
{
    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     * @return LineItemReference[]
     */
    public function fetchList(IdValue $listId, LineItemReferenceSearchStruct $searchStruct /**, CurrencyContext $currencyContext = null **/): array;

    public function fetchTotalCount(IdValue $listId, LineItemReferenceSearchStruct $searchStruct): int;

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     * @throws NotFoundException
     */
    public function fetchReferenceById(IdValue $id /**, CurrencyContext $currencyContext **/): LineItemReference;

    public function flipSorting(LineItemReference $lineItemReferenceOne, LineItemReference $lineItemReferenceTwo);

    public function hasReference(string $referenceNumber, IdValue $listId): bool;

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     */
    public function getReferenceByReferenceNumberAndListId(string $referenceNumber, IdValue $listId /**, CurrencyContext $currencyContext **/): LineItemReference;

    /**
     * @throws CanNotInsertExistingRecordException
     */
    public function addReference(IdValue $listId, LineItemReference $lineItemReference): LineItemReference;

    public function addVoucherCodeToReferenceById(IdValue $referenceId, string $voucherCode);

    public function updateReference(IdValue $listId, LineItemReference $lineItemReference): void;

    public function removeReference(IdValue $id);

    public function syncReferences(IdValue $listId, array $references);

    public function removeReferenceByListId(IdValue $id);

    public function getMainTableAlias(): string;

    public function getFullTextSearchFields(): array;

    public function getAdditionalSearchResourceAndFields(): array;

    public function updatePrices(LineItemReference $lineItemReference);

    public function getOwnershipByReferenceId(IdValue $referenceId): IdValue;
}
