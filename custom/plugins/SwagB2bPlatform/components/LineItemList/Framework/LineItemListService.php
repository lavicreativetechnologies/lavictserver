<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use InvalidArgumentException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;
use function count;

class LineItemListService
{
    /**
     * @var LineItemListRepository
     */
    private $listRepository;

    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $referenceRepository;

    /**
     * @var LineItemShopWriterServiceInterface
     */
    private $bridgeService;

    /**
     * @var LineItemCheckoutProviderInterface
     */
    private $checkoutProvider;

    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    /**
     * @var LineItemReferenceValidationService
     */
    private $lineItemReferenceValidationService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(
        LineItemListRepository $listRepository,
        LineItemReferenceRepositoryInterface $referenceRepository,
        LineItemShopWriterServiceInterface $bridgeService,
        LineItemCheckoutProviderInterface $checkoutProvider,
        ProductProviderInterface $productProvider,
        LineItemReferenceValidationService $lineItemReferenceValidationService,
        CurrencyService $currencyService
    ) {
        $this->listRepository = $listRepository;
        $this->referenceRepository = $referenceRepository;
        $this->bridgeService = $bridgeService;
        $this->checkoutProvider = $checkoutProvider;
        $this->productProvider = $productProvider;
        $this->lineItemReferenceValidationService = $lineItemReferenceValidationService;
        $this->currencyService = $currencyService;
    }

    public function produceCart(
        IdValue $listId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext,
        bool $clearBasket = false
    ): void {
        $list = $this->listRepository->fetchOneListById($listId, $currencyContext, $ownershipContext);

        $this->bridgeService->triggerCart($list, $clearBasket);
    }

    public function createListThroughListObject(
        LineItemList $list,
        OwnershipContext $ownershipContext
    ): LineItemList {
        $list = $this->listRepository
            ->addList($list, $ownershipContext);

        $this->referenceRepository->syncReferences($list->id, $list->references);

        return $list;
    }

    protected function updateList(
        LineItemList $list,
        OwnershipContext $ownershipContext
    ): LineItemList {
        $this->listRepository
            ->updateListPrices($list, $ownershipContext);

        $this->referenceRepository->syncReferences($list->id, $list->references);

        return $list;
    }

    /**
     * @param LineItemReference[] $references
     */
    public function addReferencesToList(IdValue $listId, array $references): void
    {
        foreach ($references as $reference) {
            $this->referenceRepository
                ->addReference($listId, $reference);
        }
    }

    public function addVoucherCodeToReference(LineItemReference $reference): void
    {
        $this->referenceRepository
            ->addVoucherCodeToReferenceById($reference->id, $reference->voucherCode);
    }

    public function createListThroughCheckoutSource(
        OwnershipContext $ownershipContext,
        LineItemListSource $lineItemListSources
    ): LineItemList {
        $list = $this->checkoutProvider->createList($lineItemListSources);

        $list->contextOwnerId = $ownershipContext->contextOwnerId;

        $this->createListThroughListObject($list, $ownershipContext);

        return $list;
    }

    public function updateListThroughCheckoutSource(
        LineItemList $list,
        LineItemListSource $lineItemListSources,
        OwnershipContext $ownershipContext
    ): LineItemList {
        $list = $this->checkoutProvider->updateList($list, $lineItemListSources);

        $this->updateList($list, $ownershipContext);

        return $list;
    }

    public function createListThroughCartId(string $cartId, OwnershipContext $ownershipContext): LineItemList
    {
        $list = $this->checkoutProvider->createListFromCartId($cartId);
        $list->contextOwnerId = $ownershipContext->contextOwnerId;

        return $list;
    }

    public function updateListReferences(
        LineItemList $list,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext,
        bool $replace = false
    ): LineItemList {
        if (!$replace) {
            $oldList = $this->listRepository->fetchOneListById($list->id, $currencyContext, $ownershipContext);

            $references = [];
            foreach ($list->references as $reference) {
                try {
                    $oldReference = $oldList->getReferenceByNumber($reference->referenceNumber);
                } catch (InvalidArgumentException $e) {
                    $references[] = $reference;
                    continue;
                }

                $reference->id = $oldReference->id;
                $reference->quantity += $oldReference->quantity;
                if (!$reference->comment) {
                    $reference->comment = $oldReference->comment;
                }

                $validator = $this->lineItemReferenceValidationService->createUpdateValidation($reference);

                $violations = $validator->getViolations();

                if (count($violations)) {
                    throw new ValidationException(
                        $reference,
                        $violations,
                        'Validation violations detected, can not proceed:',
                        400
                    );
                }

                $this->referenceRepository->updateReference($list->id, $reference);
                $oldReference->setData($reference->jsonSerialize());
            }

            $list->references = array_merge($oldList->references, $references);
        } else {
            $this->referenceRepository->removeReferenceByListId($list->id);
            $references = $list->references;
        }

        $this->addReferencesToList($list->id, $references);

        $this->updateListPrices($list, $ownershipContext);

        return $list;
    }

    public function updateListPrices(LineItemList $lineItemList, OwnershipContext $ownershipContext): void
    {
        $this->productProvider
            ->updateList($lineItemList);

        foreach ($lineItemList->references as $reference) {
            if (!$reference->mode instanceof ProductLineItemType) {
                continue;
            }

            $this->referenceRepository
                ->updatePrices($reference);
        }

        $this->listRepository
            ->updateListPrices($lineItemList, $ownershipContext);
    }

    public function updateListPricesById(
        IdValue $listId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): LineItemList {
        $lineItemList = $this->listRepository
            ->fetchOneListById($listId, $currencyContext, $ownershipContext);

        $this->updateListPrices($lineItemList, $ownershipContext);

        return $lineItemList;
    }
}
