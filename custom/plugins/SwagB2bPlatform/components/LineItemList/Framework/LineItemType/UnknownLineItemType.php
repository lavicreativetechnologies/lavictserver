<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework\LineItemType;

class UnknownLineItemType extends LineItemType
{
}
