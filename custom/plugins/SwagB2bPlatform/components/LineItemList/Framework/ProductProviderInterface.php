<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

interface ProductProviderInterface
{
    /**
     * @throws InvalidProductFromShopException
     */
    public function updateList(LineItemList $list);

    public static function convertPriceToLocale(string $price, string $locale = 'en_EN'): float;

    public function updateReference(LineItemReference $lineItemReference);

    public function isProduct(string $productNumber): bool;

    public function setMaxMinAndSteps(LineItemReference $lineItemReference);

    public function setMaxMinAndStepsForItems(array $lineItemReferences);
}
