<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

interface LineItemCheckoutProviderInterface
{
    public function createList(LineItemListSource $source): LineItemList;

    public function createListFromCartId(string $cartId): LineItemList;

    public function updateList(LineItemList $list, LineItemListSource $lineItemListSources): LineItemList;
}
