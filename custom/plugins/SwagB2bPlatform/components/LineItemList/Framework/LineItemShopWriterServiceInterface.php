<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

interface LineItemShopWriterServiceInterface
{
    public function triggerCart(LineItemList $list, bool $clearBasket): void;
}
