<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\SearchStruct;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\ProductName\Framework\ProductNameService;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use function array_map;
use function func_get_args;

class LineItemReferenceService
{
    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $lineItemReferenceRepository;

    /**
     * @var ProductNameService
     */
    private $productNameService;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(
        LineItemReferenceRepositoryInterface $lineItemReferenceRepository,
        ProductNameService $productNameService,
        ProductServiceInterface $productService,
        CurrencyService $currencyService
    ) {
        $this->lineItemReferenceRepository = $lineItemReferenceRepository;
        $this->productNameService = $productNameService;
        $this->productService = $productService;
        $this->currencyService = $currencyService;
    }

    /**
     * @param $searchStruct
     * @return LineItemReference[]
     */
    public function fetchLineItemsReferencesWithProductNames(
        IdValue $listId,
        ?SearchStruct $searchStruct
        /**, CurrencyContext $currencyContext **/
    ): array {
        $currencyContext = func_get_args()[2] ?? null;

        if (!$currencyContext) {
            $currencyContext = $this->currencyService->createCurrencyContext();
        }

        $lineItemReferences = $this->lineItemReferenceRepository
            ->fetchList($listId, $searchStruct, $currencyContext);

        $this->productNameService->translateProductNames($lineItemReferences);

        return $lineItemReferences;
    }

    public function fetchLineItemListProductNames(LineItemList $lineItemList): LineItemList
    {
        $this->productNameService->translateProductNames($lineItemList->references);

        return $lineItemList;
    }

    public function getReferencesWithStock(array $references): array
    {
        $orderNumbers = array_map(
            function (LineItemReference $reference) {
                return $reference->referenceNumber;
            },
            $references
        );

        $stocks = $this->productService->fetchStocksByOrderNumbers($orderNumbers);

        foreach ($references as $reference) {
            if (!isset($stocks[$reference->referenceNumber])) {
                continue;
            }

            $reference->inStock = $stocks[$reference->referenceNumber]['inStock'];
            $reference->isLastStock = $stocks[$reference->referenceNumber]['isLastStock'];
        }

        return $references;
    }
}
