<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\Framework;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyCalculator;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\Currency\Framework\CurrencyService;
use function array_merge;
use function func_get_args;
use function sprintf;

class LineItemReferenceRepository implements LineItemReferenceRepositoryInterface
{
    const TABLE_NAME = 'b2b_line_item_reference';
    const TABLE_ALIAS = 'lineItem';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    /**
     * @var CurrencyCalculator
     */
    private $currencyCalculator;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        ProductProviderInterface $productProvider,
        CurrencyCalculator $currencyCalculator,
        CurrencyService $currencyService
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->productProvider = $productProvider;
        $this->currencyCalculator = $currencyCalculator;
        $this->currencyService = $currencyService;
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     * @return LineItemReference[]
     */
    public function fetchList(IdValue $listId, LineItemReferenceSearchStruct $searchStruct /**, CurrencyContext $currencyContext = null */): array
    {
        $currencyContext = func_get_args()[2] ?? null;

        $queryBuilder = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('listId', $listId->getStorageValue())
            ->orderBy('coalesce(sort, id)');

        $this->dbalHelper->applyFilters($searchStruct, $queryBuilder);

        $rawItemReferences = $queryBuilder
            ->execute()
            ->fetchAll();

        if (!$currencyContext) {
            $currencyContext = $this->currencyService->createCurrencyContext();
        }

        $lineItemReferences = [];
        foreach ($rawItemReferences as $rawItemReference) {
            $lineItemReference = (new LineItemReference())
                ->fromDatabaseArray($rawItemReference);
            $lineItemReferences[] = $lineItemReference;
        }

        $this->productProvider->setMaxMinAndStepsForItems($lineItemReferences);
        $this->currencyCalculator->recalculateAmounts(
            $lineItemReferences,
            $currencyContext
        );

        return $lineItemReferences;
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     */
    public function fetchTotalCount(IdValue $listId, LineItemReferenceSearchStruct $searchStruct /**, OwnershipContext $OwnershipContext */): int
    {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('listId', $listId->getStorageValue());

        $this->dbalHelper->applyFilters($searchStruct, $queryBuilder);

        return (int) $queryBuilder
            ->execute()
            ->fetchColumn();
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     * @throws NotFoundException
     */
    public function fetchReferenceById(IdValue $id /* CurrencyContext $currencyContext = null */): LineItemReference
    {
        $currencyContext = func_get_args()[1] ?? null;

        $rawReferenceData = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue())
            ->execute()
            ->fetch();

        if (!$rawReferenceData) {
            throw new NotFoundException(sprintf('reference with id %s not found', $id));
        }

        if (!$currencyContext) {
            $currencyContext = $this->currencyService->createCurrencyContext();
        }

        $lineItemReference = new LineItemReference();
        $lineItemReference->fromDatabaseArray($rawReferenceData);

        $this->productProvider->setMaxMinAndStepsForItems([$lineItemReference]);
        $this->currencyCalculator->recalculateAmount(
            $lineItemReference,
            $currencyContext
        );

        return $lineItemReference;
    }

    public function flipSorting(LineItemReference $lineItemReferenceOne, LineItemReference $lineItemReferenceTwo): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            [
                'sort' => $lineItemReferenceTwo->sort ?? $lineItemReferenceTwo->id->getStorageValue(),
            ],
            [
                'id' => $lineItemReferenceOne->id->getStorageValue(),
            ]
        );

        $this->connection->update(
            self::TABLE_NAME,
            [
                'sort' => $lineItemReferenceOne->sort ?? $lineItemReferenceOne->id->getStorageValue(),
            ],
            [
                'id' => $lineItemReferenceTwo->id->getStorageValue(),
            ]
        );
    }

    public function hasReference(string $referenceNumber, IdValue $listId): bool
    {
        return (bool) $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.reference_number = :referenceNumber')
            ->andWhere(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('referenceNumber', $referenceNumber)
            ->setParameter('listId', $listId->getStorageValue())
            ->execute()
            ->fetchColumn();
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     */
    public function getReferenceByReferenceNumberAndListId(string $referenceNumber, IdValue $listId /**, CurrencyContext $currencyContext = null */): LineItemReference
    {
        $currencyContext = func_get_args()[2] ?? null;

        $rawReferenceData = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.reference_number = :referenceNumber')
            ->andWhere(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('referenceNumber', $referenceNumber)
            ->setParameter('listId', $listId->getStorageValue())
            ->execute()
            ->fetch();

        if (!$rawReferenceData) {
            throw new NotFoundException(sprintf('Reference not found in list with id %s', $listId));
        }

        if (!$currencyContext) {
            $currencyContext = $this->currencyService->createCurrencyContext();
        }

        $lineItemReference = new LineItemReference();
        $lineItemReference->fromDatabaseArray($rawReferenceData);

        $this->productProvider->setMaxMinAndStepsForItems([$lineItemReference]);
        $this->currencyCalculator->recalculateAmount(
            $lineItemReference,
            $currencyContext
        );

        return $lineItemReference;
    }

    /**
     * @throws CanNotInsertExistingRecordException
     */
    public function addReference(
        IdValue $listId,
        LineItemReference $lineItemReference
    ): LineItemReference {
        if (!$lineItemReference->isNew()) {
            throw new CanNotInsertExistingRecordException('The list item provided already exists');
        }

        $this->connection->insert(
            self::TABLE_NAME,
            array_merge(
                $lineItemReference->toDatabaseArray(),
                ['list_id' => $listId->getStorageValue()]
            )
        );

        $lineItemReference->id = IdValue::create($this->connection
            ->lastInsertId());

        return $lineItemReference;
    }

    public function addVoucherCodeToReferenceById(IdValue $referenceId, string $voucherCode): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            [
                'voucher_code' => $voucherCode,
            ],
            [
                'id' => $referenceId->getStorageValue(),
            ]
        );
    }

    public function updateReference(IdValue $listId, LineItemReference $lineItemReference): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            $lineItemReference->toDatabaseArray(),
            [
                'id' => $lineItemReference->id->getStorageValue(),
                'list_id' => $listId->getStorageValue(),
            ]
        );
    }

    public function removeReference(IdValue $id): void
    {
        $this->connection->delete(
            self::TABLE_NAME,
            ['id' => $id->getStorageValue()]
        );
    }

    public function syncReferences(IdValue $listId, array $references): void
    {
        $ids = [];

        foreach ($references as $reference) {
            if ($reference->isNew()) {
                $this->addReference($listId, $reference);
            } else {
                $this->updateReference($listId, $reference);
            }

            $ids[] = $reference->id->getStorageValue();
        }

        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('id NOT IN (:ids) AND list_id = :listId')
            ->setParameter('ids', $ids, Connection::PARAM_INT_ARRAY)
            ->setParameter('listId', $listId->getStorageValue())
            ->execute();
    }

    public function removeReferenceByListId(IdValue $id): void
    {
        $this->connection->delete(
            self::TABLE_NAME,
            ['list_id' => $id->getStorageValue()]
        );
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [
            'reference_number',
            'comment',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }

    public function updatePrices(LineItemReference $lineItemReference): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            [
                'amount_net' => $lineItemReference->amountNet,
                'amount' => $lineItemReference->amount,
                'currency_factor' => $lineItemReference->currencyFactor,
            ],
            ['id' => $lineItemReference->id->getStorageValue()]
        );
    }

    public function getOwnershipByReferenceId(IdValue $referenceId): IdValue
    {
        return IdValue::create(
            $this->connection->createQueryBuilder()
                ->select(LineItemListRepository::TABLE_ALIAS . '.context_owner_id')
                ->from(self::TABLE_NAME, self::TABLE_ALIAS)
                ->innerJoin(
                    self::TABLE_ALIAS,
                    LineItemListRepository::TABLE_NAME,
                    LineItemListRepository::TABLE_ALIAS,
                    self::TABLE_ALIAS . '.list_id = ' . LineItemListRepository::TABLE_ALIAS . '.id'
                )
                ->where(self::TABLE_ALIAS . '.id = :referenceId')
                ->setParameter('referenceId', $referenceId)
                ->execute()
                ->fetchColumn()
        );
    }
}
