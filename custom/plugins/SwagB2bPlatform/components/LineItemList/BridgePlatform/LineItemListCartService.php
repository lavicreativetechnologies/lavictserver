<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Doctrine\DBAL\Connection;
use InvalidArgumentException;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Framework\Uuid\Uuid;
use function sprintf;

class LineItemListCartService
{
    public const cartNamePrefix = 'b2bLineItemListCart';

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(CartService $cartService, Connection $connection, ContextProvider $contextProvider)
    {
        $this->cartService = $cartService;
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
    }

    public function getCartByList(LineItemList $list): Cart
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();
        $name = $this->generateNameByList($list);

        $fetchedToken = $token = $this->connection->createQueryBuilder()
            ->select('token')
            ->from('cart')
            ->where('name = :name')
            ->setParameter('name', $name)->execute()
            ->fetchColumn();

        if (!$token) {
            $token = Uuid::randomHex();
        }

        $cart = $this->cartService->getCart($token, $salesChannelContext);

        if (!$fetchedToken) {
            $cart->setName($name);
            $cart->markModified();
        }

        return $cart;
    }

    /**
     * @internal
     */
    protected function generateNameByList(LineItemList $list): string
    {
        if ($list->id instanceof NullIdValue) {
            throw new InvalidArgumentException('list id not set');
        }

        return sprintf('%s::%s', self::cartNamePrefix, $list->id->getValue());
    }
}
