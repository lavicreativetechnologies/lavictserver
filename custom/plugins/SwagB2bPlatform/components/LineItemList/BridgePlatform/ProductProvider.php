<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use InvalidArgumentException;
use NumberFormatter;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Shopware\B2B\LineItemList\Framework\UnsupportedQuantityException;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\ReferencePriceDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Price\ProductPriceDefinitionBuilderInterface;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use function array_keys;
use function array_pop;
use function count;
use function current;
use function numfmt_create;
use function numfmt_parse;

class ProductProvider implements ProductProviderInterface
{
    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var AuthenticationService
     */
    private $storefrontAuthenticationService;

    /**
     * @var ProductPriceDefinitionBuilderInterface|null
     */
    private $productPriceDefinitionBuilder;

    /**
     * @var QuantityPriceCalculator
     */
    private $quantityPriceCalculator;

    /**
     * @var UnitPriceCalculationService
     */
    private $unitPriceCalculationService;

    public function __construct(
        SalesChannelRepositoryInterface $productRepository,
        ContextProvider $contextProvider,
        AuthenticationService $storefrontAuthenticationService,
        QuantityPriceCalculator $quantityPriceCalculator,
        ?ProductPriceDefinitionBuilderInterface $productPriceDefinitionBuilder,
        UnitPriceCalculationService $unitPriceCalculationService
    ) {
        $this->productRepository = $productRepository;
        $this->contextProvider = $contextProvider;
        $this->storefrontAuthenticationService = $storefrontAuthenticationService;
        $this->productPriceDefinitionBuilder = $productPriceDefinitionBuilder;
        $this->quantityPriceCalculator = $quantityPriceCalculator;
        $this->unitPriceCalculationService = $unitPriceCalculationService;
    }

    public function updateList(LineItemList $list): void
    {
        $productNumbers = [];
        foreach ($list->references as $reference) {
            $productNumbers[$reference->referenceNumber] = $reference;
        }

        $products = $this->getProductList(array_keys($productNumbers));

        foreach ($productNumbers as $productNumber => $reference) {
            if (!$reference->mode instanceof ProductLineItemType) {
                continue;
            }

            $this->updateReferenceFromProduct($products, $productNumber, $reference);
        }

        $list->recalculateListAmounts();
    }

    public static function convertPriceToLocale(string $price, string $locale = 'en_EN'): float
    {
        $formatter = numfmt_create($locale, NumberFormatter::DEFAULT_STYLE);

        $formattedPrice = numfmt_parse($formatter, $price);

        if (!$formattedPrice) {
            throw new InvalidArgumentException('Wrong price format given');
        }

        return (float) $formattedPrice;
    }

    public function updateReference(LineItemReference $reference): void
    {
        $products = $this->getProductList([$reference->referenceNumber]);
        $product = array_pop($products);

        if (!$product) {
            return;
        }

        $reference->referenceNumber = $product->getProductNumber();
        $this->determinePriceForReference($reference, $product);
    }

    public function isProduct(string $productNumber): bool
    {
        $products = $this->getProductList([$productNumber]);

        return count($products) > 0;
    }

    public function setMaxMinAndSteps(LineItemReference $lineItemReference): void
    {
        $purchaseStep = 1;
        $minPurchase = 1;

        $product = current($this->getProductList([$lineItemReference->referenceNumber]));

        if ($product) {
            if ($max = $product->getMaxPurchase()) {
                $lineItemReference->maxPurchase = $max;
            }

            if ($step = $product->getPurchaseSteps()) {
                $purchaseStep = $step;
            }

            if ($min = $product->getMinPurchase()) {
                $minPurchase = $min;
            }
        }

        $lineItemReference->minPurchase = $minPurchase;
        $lineItemReference->purchaseStep = $purchaseStep;
    }

    public function setMaxMinAndStepsForItems(array $lineItemReferences): void
    {
        $numbers = [];
        foreach ($lineItemReferences as $lineItemReference) {
            $numbers[$lineItemReference->referenceNumber] = $lineItemReference;
        }

        $products = $this->getProductList(array_keys($numbers));

        foreach ($products as $product) {
            $productNumber = $product->getProductNumber();
            if ($stock = $product->getStock()) {
                $numbers[$productNumber]->inStock = $stock;
                $numbers[$productNumber]->isLastStock = $product->getIsCloseout();

                if ($max = $product->getMaxPurchase()) {
                    $numbers[$productNumber]->maxPurchase = $max;
                }

                if ($step = $product->getPurchaseSteps()) {
                    $numbers[$productNumber]->purchaseStep = $step;
                }

                if ($min = $product->getMinPurchase()) {
                    $numbers[$productNumber]->minPurchase = $min;
                }
            }
        }
    }

    /**
     * @return ProductEntity[]
     */
    protected function getProductList(array $productNumbers): array
    {
        $criteria = new Criteria();
        $criteria->addAssociation('prices');
        $criteria->addAssociation(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME);
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_OR, [
            new EqualsAnyFilter('productNumber', $productNumbers),
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsAnyFilter('product.b2bOrderNumber.customOrderNumber', $productNumbers),
                new EqualsFilter(
                    'product.b2bOrderNumber.contextOwnerId',
                    $this->storefrontAuthenticationService->getIdentity()->getContextAuthId()
                ),
            ]),
        ]));

        return $this->productRepository->search(
            $criteria,
            $this->contextProvider->getSalesChannelContext()
        )->getElements();
    }

    /**
     * @param ProductEntity[] $products
     */
    protected function updateReferenceFromProduct(array $products, string $productNumber, LineItemReference $reference): void
    {
        $product = null;

        foreach ($products as $currentProduct) {
            if (
                $currentProduct->getProductNumber() === $productNumber
            ) {
                $product = $currentProduct;
                break;
            }
        }

        if (!$product) {
            return;
        }

        $reference->referenceNumber = $product->getProductNumber();
        $this->determinePriceForReference($reference, $product);
    }

    protected function determinePriceForReference(LineItemReference $reference, SalesChannelProductEntity $product): void
    {
        if (!$reference->quantity) {
            throw new UnsupportedQuantityException('The quantity is not supported');
        }

        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        if (ShopwareVersion::isShopware6_4_X()) {
            $quantityPriceDefinition = $this->getPriceDefinition($product, $reference->quantity);
        } else {
            $productPriceDefinition = $this->productPriceDefinitionBuilder->build(
                $product,
                $salesChannelContext,
                $reference->quantity
            );

            $quantityPriceDefinition = $productPriceDefinition->getQuantityPrice();
        }

        $calculatedPrice = $this->quantityPriceCalculator->calculate($quantityPriceDefinition, $salesChannelContext);

        $reference->amount = $this->unitPriceCalculationService->getUnitAmount($calculatedPrice, $salesChannelContext);
        $reference->amountNet = $this->unitPriceCalculationService->getUnitAmountNet($calculatedPrice, $salesChannelContext);
    }

    private function getPriceDefinition(SalesChannelProductEntity $product, int $quantity): QuantityPriceDefinition
    {
        if ($product->getCalculatedPrices()->count() === 0) {
            return $this->buildPriceDefinition($product->getCalculatedPrice(), $quantity);
        }

        // keep loop reference to $price variable to get last quantity price in case of "null"
        $price = $product->getCalculatedPrice();
        foreach ($product->getCalculatedPrices() as $price) {
            if ($quantity <= $price->getQuantity()) {
                break;
            }
        }

        return $this->buildPriceDefinition($price, $quantity);
    }

    private function buildPriceDefinition(CalculatedPrice $price, int $quantity): QuantityPriceDefinition
    {
        $definition = new QuantityPriceDefinition($price->getUnitPrice(), $price->getTaxRules(), $quantity);
        if ($price->getListPrice() !== null) {
            $definition->setListPrice($price->getListPrice()->getPrice());
        }

        if ($price->getReferencePrice() !== null) {
            $definition->setReferencePriceDefinition(
                new ReferencePriceDefinition(
                    $price->getReferencePrice()->getPurchaseUnit(),
                    $price->getReferencePrice()->getReferenceUnit(),
                    $price->getReferencePrice()->getUnitName()
                )
            );
        }

        return $definition;
    }
}
