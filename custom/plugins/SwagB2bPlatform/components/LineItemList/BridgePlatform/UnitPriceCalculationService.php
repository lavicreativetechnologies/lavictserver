<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class UnitPriceCalculationService
{
    public function getUnitAmount(CalculatedPrice $price, SalesChannelContext $salesChannelContext): float
    {
        if ($salesChannelContext->getCurrentCustomerGroup()->getDisplayGross()) {
            return $price->getUnitPrice();
        }

        return $price->getUnitPrice() + ($price->getCalculatedTaxes()->getAmount() / $price->getQuantity());
    }

    public function getUnitAmountNet(CalculatedPrice $price, SalesChannelContext $salesChannelContext): float
    {
        if ($salesChannelContext->getCurrentCustomerGroup()->getDisplayGross()) {
            return $price->getUnitPrice() - ($price->getCalculatedTaxes()->getAmount() / $price->getQuantity());
        }

        return $price->getUnitPrice();
    }
}
