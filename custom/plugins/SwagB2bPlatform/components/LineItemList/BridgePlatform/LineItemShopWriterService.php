<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\ShopCartService;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemShopWriterServiceInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use function array_map;

class LineItemShopWriterService implements LineItemShopWriterServiceInterface
{
    public const ORDER_LINE_ITEM_COMMENT = 'b2b_order_line_item_comment';

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ShopCartService
     */
    private $shopCartService;

    public function __construct(
        ContextProvider $contextProvider,
        CartService $cartService,
        SalesChannelRepositoryInterface $productRepository,
        ShopCartService $shopCartService
    ) {
        $this->contextProvider = $contextProvider;
        $this->cartService = $cartService;
        $this->productRepository = $productRepository;
        $this->shopCartService = $shopCartService;
    }

    public function triggerCart(LineItemList $list, bool $clearBasket): void
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        if ($clearBasket) {
            $this->shopCartService->clear($salesChannelContext);
        }

        $cart = $this->cartService
            ->getCart($salesChannelContext->getToken(), $salesChannelContext);

        $lineItems = $this->convertToLineItems($list);

        foreach ($lineItems as $lineItem) {
            $this->cartService->add($cart, $lineItem, $salesChannelContext);
        }
    }

    /**
     * @internal
     * @return LineItem[]
     */
    protected function convertToLineItems(LineItemList $list): array
    {
        $products = $this->fetchProducts($list);
        $lineItems = [];

        foreach ($list->references as $reference) {
            $id = $this->reduceToId($products, $reference);

            if ($id === null) {
                continue;
            }

            $lineItem = new LineItem(
                $id,
                $reference->mode->getValue(),
                $id,
                $reference->quantity
            );

            $lineItem->setRemovable(true);
            $lineItem->setStackable(true);
            $lineItem->addExtension(
                self::ORDER_LINE_ITEM_COMMENT,
                new OrderLineItemCommentStruct($reference->comment)
            );

            $lineItems[] = $lineItem;
        }

        return $lineItems;
    }

    /**
     * @internal
     */
    protected function reduceToId(EntitySearchResult $result, LineItemReference $reference): ?string
    {
        foreach ($result->getElements() as $product) {
            $b2bOrderNumberEntity = $product->getExtension(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME)
                ->first();

            if (($b2bOrderNumberEntity && $b2bOrderNumberEntity->getCustomOrderNumber() === $reference->referenceNumber)
                || $product->getProductNumber() === $reference->referenceNumber
            ) {
                return $product->getId();
            }
        }

        return null;
    }

    /**
     * @internal
     */
    protected function fetchProducts(LineItemList $list): EntitySearchResult
    {
        $productNumbers = array_map(function (LineItemReference $reference) {
            return $reference->referenceNumber;
        }, $list->references);

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('productNumber', $productNumbers));

        return $this->productRepository
            ->search($criteria, $this->contextProvider->getSalesChannelContext());
    }
}
