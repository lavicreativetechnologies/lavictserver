<?php declare(strict_types=1);

namespace Shopware\B2B\LineItemList\BridgePlatform;

use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\Core\Framework\Event\EventData\ArrayType;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;
use function array_map;

class LineItemListMailData
{
    /**
     * @var LineItemList
     */
    private $list;

    /**
     * @var LineItemReferenceMailData[]|null
     */
    private $references;

    public function __construct(LineItemList $list)
    {
        $this->list = $list;
    }

    public static function getLineItemListDataType(): EventDataType
    {
        $list = new ObjectType();
        $list->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $list->add('contextOwnerId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $list->add('amount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $list->add('amountNet', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $list->add('references', new ArrayType(LineItemReferenceMailData::getReferenceDataType()));

        return $list;
    }

    public function getId(): string
    {
        return (string) $this->list->id->getValue();
    }

    public function getContextOwnerId(): string
    {
        return (string) $this->list->contextOwnerId->getValue();
    }

    public function getReferences(): array
    {
        if (!$this->references) {
            $this->references = array_map(static function (LineItemReference $reference) {
                return new LineItemReferenceMailData($reference);
            }, $this->list->references);
        }

        return $this->references;
    }

    public function getAmount(): float
    {
        return $this->list->amount;
    }

    public function getAmountNet(): float
    {
        return $this->list->amountNet;
    }
}
