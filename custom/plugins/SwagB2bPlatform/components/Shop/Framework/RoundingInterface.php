<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

interface RoundingInterface
{
    public function round(float $netPrice, float $tax = 0, int $quantity = 1): float;
}
