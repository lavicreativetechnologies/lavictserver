<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

use Shopware\B2B\LineItemList\Framework\LineItemReference;

interface ProductServiceInterface
{
    public function fetchProductNameByOrderNumber(string $orderNumber): string;

    public function fetchProductNamesByOrderNumbers(array $orderNumbers): array;

    public function fetchOrderNumberByReferenceNumber(string $referenceNumber): string;

    public function fetchProductOrderNumbersByReferenceNumbers(array $referenceNumbers): array;

    /**
     * @return string[] $productNumber => $productName
     */
    public function searchProductsByNameOrOrderNumber(string $term, int $limit): array;

    public function fetchStocksByOrderNumbers(array $orderNumbers): array;

    public function isNormalProduct(LineItemReference $reference): bool;
}
