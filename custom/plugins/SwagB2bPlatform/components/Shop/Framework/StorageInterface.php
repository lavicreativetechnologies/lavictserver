<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

interface StorageInterface
{
    /**
     * @param $value
     */
    public function set(string $key, $value);

    public function get(string $key);

    public function remove(string $key);
}
