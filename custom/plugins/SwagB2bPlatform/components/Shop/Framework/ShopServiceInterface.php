<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

use Shopware\B2B\Common\IdValue;

interface ShopServiceInterface
{
    public function getRootCategoryId(): IdValue;

    public function getCurrentCurrencyFactor(): float;

    public function getCurrentCurrencySymbol(): string;
}
