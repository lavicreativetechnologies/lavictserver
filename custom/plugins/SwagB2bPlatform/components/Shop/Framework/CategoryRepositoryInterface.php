<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Framework;

use Shopware\B2B\Common\IdValue;

interface CategoryRepositoryInterface
{
    /**
     * @return CategoryNode[]
     */
    public function fetchChildren(IdValue $parentId): array;

    public function hasProduct(IdValue $categoryId, string $productNumber): bool;

    public function fetchNodeById(IdValue $categoryId): CategoryNode;
}
