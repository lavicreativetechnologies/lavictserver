<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\Bridge\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use function str_replace;

class ModelValidOperatorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasParameter('shopware.model.validOperators')) {
            return;
        }

        $validOperators = $container->getParameter('shopware.model.validOperators');
        $validOperators[] = str_replace(' ', '', 'IS NULL OR attribute.b2bIsDebtor = ');
        $validOperators[] = str_replace(' ', '', 'IS NULL OR attribute.b2bIsSalesRepresentative = ');

        $container->setParameter('shopware.model.validOperators', $validOperators);
    }
}
