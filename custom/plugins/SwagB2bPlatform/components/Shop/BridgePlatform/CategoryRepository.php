<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Shop\Framework\CategoryNode;
use Shopware\B2B\Shop\Framework\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{
    use TranslatedFieldQueryExtenderTrait;

    const TABLE_NAME = 'category';

    const TABLE_ALIAS = 'category';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        Connection $connection,
        ContextProvider $contextProvider
    ) {
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
    }

    public function fetchChildren(IdValue $parentId): array
    {
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->select(self::TABLE_ALIAS . '.*')
            ->addSelect('(SELECT COUNT(*) FROM ' . self::TABLE_NAME . ' innerCategory WHERE innerCategory.parent_id = ' . self::TABLE_ALIAS . '.id LIMIT 1) AS has_children')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.parent_id = :parentId')
            ->orderBy('level')
            ->setParameter('parentId', $parentId->getStorageValue());

        $this->addTranslatedFieldSelect(
            $queryBuilder,
            'name',
            'name',
            'category_translation',
            self::TABLE_NAME,
            self::TABLE_ALIAS,
            'id',
            $this->contextProvider->getSalesChannelContext(),
            true
        );

        $rawResult = $queryBuilder->execute()
            ->fetchAll();

        $nodes = [];
        foreach ($rawResult as $row) {
            $nodes[] = new CategoryNode(IdValue::create($row['id']), $row['name'], (bool) $row['has_children']);
        }

        return $nodes;
    }

    public function hasProduct(IdValue $categoryId, string $productNumber): bool
    {
        return (bool) $this->connection->fetchColumn(
            'SELECT COUNT(*)
            FROM product
            WHERE JSON_CONTAINS(category_tree, JSON_ARRAY(:categoryId))
                AND product.product_number = :productNumber',
            [
                'categoryId' => $categoryId->getValue(),
                'productNumber' => $productNumber,
            ]
        );
    }

    public function fetchNodeById(IdValue $categoryId): CategoryNode
    {
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->select(self::TABLE_ALIAS . '.*')
            ->addSelect('(SELECT COUNT(*) FROM ' . self::TABLE_NAME . ' innerCategory WHERE innerCategory.parent_id = ' . self::TABLE_ALIAS . '.id LIMIT 1) AS has_children')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :categoryId')
            ->orderBy('level')
            ->setParameter('categoryId', $categoryId->getStorageValue());

        $this->addTranslatedFieldSelect(
            $queryBuilder,
            'name',
            'name',
            'category_translation',
            self::TABLE_NAME,
            self::TABLE_ALIAS,
            'id',
            $this->contextProvider->getSalesChannelContext(),
            true
        );

        $rawResult = $queryBuilder->execute()
            ->fetch();

        return new CategoryNode(IdValue::create($rawResult['id']), $rawResult['name'], (bool) $rawResult['has_children']);
    }
}
