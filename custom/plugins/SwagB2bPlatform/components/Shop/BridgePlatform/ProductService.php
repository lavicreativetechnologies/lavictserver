<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\OrderNumber\BridgePlatform\B2bOrderNumberEntity;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use function array_map;
use function implode;
use function sprintf;

class ProductService implements ProductServiceInterface
{
    /**
     * @var SalesChannelRepositoryInterface
     */
    private $salesChannelProductRepository;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        SalesChannelRepositoryInterface $salesChannelProductRepository,
        ContextProvider $contextProvider
    ) {
        $this->salesChannelProductRepository = $salesChannelProductRepository;
        $this->contextProvider = $contextProvider;
    }

    public function fetchProductNameByOrderNumber(string $productOrderNumber): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('productNumber', $productOrderNumber));
        $criteria->addAssociation('options');

        $product = $this->salesChannelProductRepository->search($criteria, $this->contextProvider->getSalesChannelContext())->first();

        /** @var ProductEntity $product */
        if (!$product) {
            throw new NotFoundException("Product with reference number {$productOrderNumber} not found.");
        }

        return $this->createProductName($product);
    }

    public function fetchOrderNumberByReferenceNumber(string $referenceNumber): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('product.productNumber', $referenceNumber));

        $product = $this->salesChannelProductRepository->search($criteria, $this->contextProvider->getSalesChannelContext())->first();

        /** @var ProductEntity $product */
        if (!$product) {
            throw new NotFoundException("Product with reference number {$referenceNumber} not found.");
        }

        return $product->getProductNumber();
    }

    public function fetchProductOrderNumbersByReferenceNumbers(array $referenceNumbers): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('product.productNumber', $referenceNumbers));

        $products = $this->salesChannelProductRepository->search($criteria, $this->contextProvider->getSalesChannelContext())->getElements();

        return array_map(static function (ProductEntity $product) {
            return $product->getProductNumber();
        }, $products);
    }

    public function fetchProductNamesByOrderNumbers(array $productOrderNumbers): array
    {
        $criteria = new Criteria();
        $criteria->addAssociation('options');
        $criteria->addFilter(new EqualsAnyFilter('product.productNumber', $productOrderNumbers));

        $products = $this->salesChannelProductRepository->search($criteria, $this->contextProvider->getSalesChannelContext());

        $productNames = [];

        /** @var ProductEntity $product */
        foreach ($products as $product) {
            $productNames[$product->getProductNumber()] = $this->createProductName($product);
        }

        return $productNames;
    }

    public function searchProductsByNameOrOrderNumber(string $term, int $limit): array
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        $criteria = new Criteria();
        $criteria->setLimit($limit);
        $criteria->addAssociation('options');
        $criteria->addFilter(
            new MultiFilter(MultiFilter::CONNECTION_OR, [
                new ContainsFilter('product.productNumber', $term),
                new ContainsFilter('product.name', $term),
            ])
        );

        $searchResult = $this->salesChannelProductRepository->search($criteria, $salesChannelContext);

        $products = [];
        /** @var ProductEntity $product */
        foreach ($searchResult->getIterator() as $product) {
            /** @var B2bOrderNumberEntity $customOrderNumber */
            $customOrderNumber = null;

            if ($product->hasExtension(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME)) {
                $customOrderNumber = $product
                    ->getExtension(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME)
                    ->first();
            }
            $orderNumber = $customOrderNumber ? $customOrderNumber->getCustomOrderNumber() : $product->getProductNumber();

            $products[$orderNumber] = ['name' => $this->createProductName($product)];

            $products[$orderNumber]['min'] = $product->getMinPurchase() ?? 1;
            $products[$orderNumber]['step'] = $product->getPurchaseSteps() ?? 1;

            if ($max = $product->getMaxPurchase()) {
                $products[$orderNumber]['max'] = $max;
            }
        }

        return $products;
    }

    // TODO: refactor this function
    public function fetchStocksByOrderNumbers(array $productOrderNumbers): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('product.productNumber', $productOrderNumbers));

        $products = $this->salesChannelProductRepository->search($criteria, $this->contextProvider->getSalesChannelContext());

        if (!$products->getTotal()) {
            return [];
        }

        $stockArray = [];
        /** @var ProductEntity $product */
        foreach ($products->getIterator() as $product) {
            $stockArray[$product->getProductNumber()] = [
                'inStock' => $product->getStock(),
                'isLastStock' => $product->getIsCloseout(),
            ];
        }

        return $stockArray;
    }

    public function isNormalProduct(LineItemReference $reference): bool
    {
        return $reference->mode instanceof ProductLineItemType;
    }

    private function createProductName(ProductEntity $product): string
    {
        $options = $product->getOptions();

        if (!$options || !$options->count()) {
            return (string) $product->getTranslation('name');
        }

        $optionNames = [];
        /** @var PropertyGroupOptionEntity $option */
        foreach ($options as $option) {
            $optionNames[] = $option->getTranslation('name');
        }

        return sprintf(
            '%s (%s)',
            $product->getTranslation('name'),
            implode(', ', $optionNames)
        );
    }
}
