<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Shop\Framework\StorageInterface;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use function serialize;
use function unserialize;

class Storage implements StorageInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SalesChannelContextPersister
     */
    private $salesChannelContextPersister;

    public function __construct(ContextProvider $contextProvider, SalesChannelContextPersister $salesChannelContextPersister)
    {
        $this->contextProvider = $contextProvider;
        $this->salesChannelContextPersister = $salesChannelContextPersister;
    }

    public function set(string $key, $value): void
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();
        $parameters = [
            $salesChannelContext->getToken(),
            [$key => serialize($value)],
            $salesChannelContext->getSalesChannel()->getId(),
        ];

        $customer = $salesChannelContext->getCustomer();
        if ($customer) {
            $parameters[] = $customer->getId();
        }

        $this->salesChannelContextPersister->save(...$parameters);
    }

    public function get(string $key)
    {
        $customerId = null;

        $salesChannelContext = $this->contextProvider->getSalesChannelContext();
        $salesChannelId = $salesChannelContext->getSalesChannel()->getId();
        $customer = $salesChannelContext->getCustomer();

        if ($customer) {
            $customerId = $customer->getId();
        }

        $salesChannelContextPayload = $this->salesChannelContextPersister->load(
            $salesChannelContext->getToken(),
            $salesChannelId,
            $customerId
        );

        return isset($salesChannelContextPayload[$key]) ? unserialize($salesChannelContextPayload[$key]) : null;
    }

    public function remove(string $key): void
    {
        $this->set($key, null);
    }
}
