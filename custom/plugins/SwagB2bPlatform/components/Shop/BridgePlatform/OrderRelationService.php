<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Shop\Framework\OrderRelationServiceInterface;

class OrderRelationService implements OrderRelationServiceInterface
{
    use TranslatedFieldQueryExtenderTrait;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        Connection $connection,
        ContextProvider $contextProvider
    ) {
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
    }

    public function getShippingNameForId(IdValue $shippingId): string
    {
        if ($shippingId instanceof NullIdValue) {
            throw new NotFoundException('Unable to find shipping method');
        }

        $query = $this->connection->createQueryBuilder()
            ->select('shipping_display_name')
            ->from('shipping_method', 'shippingMethod')
            ->where('shippingMethod.id = :id')
            ->setParameter('id', $shippingId->getStorageValue());

        $this->addTranslatedFieldSelect(
            $query,
            'name',
            'shipping_display_name',
            'shipping_method_translation',
            'shipping_method',
            'shippingMethod',
            'id',
            $this->contextProvider->getSalesChannelContext()
        );

        $shippingName = $query->execute()->fetchColumn();

        if (!$shippingName) {
            throw new NotFoundException('Unable to find shipping method with id ' . $shippingId->getValue());
        }

        return $shippingName;
    }

    public function getPaymentNameForId(IdValue $paymentId): string
    {
        if ($paymentId instanceof NullIdValue) {
            throw new NotFoundException('Unable to find payment method');
        }

        $query = $this->connection->createQueryBuilder()
            ->select('payment_display_name')
            ->from('payment_method', 'paymentMethod')
            ->where('paymentMethod.id = :id')
            ->setParameter('id', $paymentId->getStorageValue());

        $this->addTranslatedFieldSelect(
            $query,
            'name',
            'payment_display_name',
            'payment_method_translation',
            'payment_method',
            'paymentMethod',
            'id',
            $this->contextProvider->getSalesChannelContext()
        );

        $paymentName = $query->execute()->fetchColumn();

        if (!$paymentName) {
            throw new NotFoundException('Unable to find payment method with id ' . $paymentId->getValue());
        }

        return $paymentName;
    }
}
