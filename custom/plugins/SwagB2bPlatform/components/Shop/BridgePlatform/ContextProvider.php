<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Routing\Event\SalesChannelContextResolvedEvent;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceInterface;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceParameters;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ContextProvider implements EventSubscriberInterface
{
    /**
     * @var SalesChannelContext|null
     */
    private $salesChannelContext;

    /**
     * @var Context|null
     */
    private $context;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var SalesChannelContextServiceInterface
     */
    private $salesChannelContextService;

    public function __construct(
        RequestStack $requestStack,
        SalesChannelContextServiceInterface $salesChannelContextService
    ) {
        $this->requestStack = $requestStack;
        $this->salesChannelContextService = $salesChannelContextService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SalesChannelContextResolvedEvent::class => 'onSalesChannelContextResolved',
        ];
    }

    public function getSalesChannelContext(): SalesChannelContext
    {
        if ($this->salesChannelContext) {
            return $this->salesChannelContext;
        }

        $this->salesChannelContext = $this->loadSalesChannelContextFromRequest();

        if (!$this->salesChannelContext) {
            $this->salesChannelContext = $this->loadDefaultSalesChannelContext();
        }

        return $this->salesChannelContext;
    }

    public function setSalesChannelContext(SalesChannelContext $salesChannelContext): void
    {
        $this->salesChannelContext = $salesChannelContext;
    }

    public function hasSalesChannelContext(): bool
    {
        return $this->salesChannelContext instanceof SalesChannelContext;
    }

    public function getContext(): Context
    {
        if ($this->context) {
            return $this->context;
        }

        $this->context = $this->loadContextFromRequest();

        if ($this->context) {
            return $this->context;
        }

        return $this->getSalesChannelContext()->getContext();
    }

    public function isAdminApiSource(): bool
    {
        return $this->getContext()->getSource() instanceof AdminApiSource;
    }

    public function setContext(Context $context): void
    {
        $this->context = $context;
    }

    /**
     * @internal
     */
    protected function loadSalesChannelContextFromRequest(): ?SalesChannelContext
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        if (!$masterRequest) {
            return null;
        }

        return $masterRequest
            ->attributes
            ->get(PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT);
    }

    /**
     * @internal
     */
    protected function loadContextFromRequest(): ?Context
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        if (!$masterRequest) {
            return null;
        }

        return $masterRequest
            ->attributes
            ->get(PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT);
    }

    public function onSalesChannelContextResolved(SalesChannelContextResolvedEvent $event): void
    {
        $this->salesChannelContext = $event->getSalesChannelContext();
    }

    /**
     * @internal
     */
    protected function loadDefaultSalesChannelContext(): SalesChannelContext
    {
        if (ShopwareVersion::isShopware6_4_X()) {
            return $this
                ->salesChannelContextService
                ->get(new SalesChannelContextServiceParameters(
                    Defaults::SALES_CHANNEL,
                    Uuid::randomHex(),
                    Defaults::LANGUAGE_SYSTEM
                ));
        }

        return $this
            ->salesChannelContextService
            ->get(Defaults::SALES_CHANNEL, Uuid::randomHex(), Defaults::LANGUAGE_SYSTEM);
    }
}
