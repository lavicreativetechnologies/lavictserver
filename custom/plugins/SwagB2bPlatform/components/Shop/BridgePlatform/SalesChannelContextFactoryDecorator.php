<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Exception;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\StoreFrontAuthentication\BridgePlatform\B2bIdentityIdStruct;
use Shopware\B2B\StoreFrontAuthentication\BridgePlatform\SalesChannelContextAuthStorageAdapter;
use Shopware\Core\Checkout\Cart\Delivery\Struct\ShippingLocation;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\Tax\TaxCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SalesChannelContextFactoryDecorator extends SalesChannelContextFactory
{
    /**
     * @var SalesChannelContextFactory
     */
    private $decorated;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(SalesChannelContextFactory $decorated, ContainerInterface $container)
    {
        $this->decorated = $decorated;
        $this->container = $container;
    }

    public function create(string $token, string $salesChannelId, array $options = []): SalesChannelContext
    {
        $context = $this->decorated->create($token, $salesChannelId, $options);

        if (isset($options[SalesChannelContextAuthStorageAdapter::IDENTITY_ID_KEY])) {
            $identityId = IdValue::create($options[SalesChannelContextAuthStorageAdapter::IDENTITY_ID_KEY]);
            $this->addB2bIdentityIdExtension($context, $identityId);
        }

        if (!$this->isSalesChannelContextProviderInitialized()) {
            return $context;
        }

        $salesChannelContextProvider = $this->getSalesChannelContextProvider();

        if (!$salesChannelContextProvider->hasSalesChannelContext()) {
            $salesChannelContextProvider->setSalesChannelContext($context);
        }

        return $context;
    }

    /**
     * @internal
     */
    protected function getSalesChannelContextProvider(): ContextProvider
    {
        return $this->container->get('b2b_shop.context_provider');
    }

    /**
     * @internal
     */
    protected function isSalesChannelContextProviderInitialized(): bool
    {
        return $this->container->initialized('b2b_shop.context_provider');
    }

    /**
     * @internal
     */
    protected function addB2bIdentityIdExtension(SalesChannelContext $context, IdValue $identityId): void
    {
        $context->addExtension(
            SalesChannelContextAuthStorageAdapter::IDENTITY_ID_KEY,
            new B2bIdentityIdStruct($identityId)
        );
    }

    public function getTaxRules(Context $context, ?CustomerEntity $customer, ShippingLocation $shippingLocation): TaxCollection
    {
        if (ShopwareVersion::isShopware6_4_X()) {
            throw new Exception('Method is private in 6.4');
        }

        return $this->decorated->getTaxRules($context, $customer, $shippingLocation);
    }
}
