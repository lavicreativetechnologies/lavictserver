<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use function array_search;
use function array_splice;
use function implode;
use function sprintf;
use function str_replace;
use function uniqid;

trait TranslatedFieldQueryExtenderTrait
{
    /**
     * @internal
     */
    protected function addTranslatedFieldSelect(
        QueryBuilder $query,
        string $columnName,
        string $columnAlias,
        string $translationTableName,
        string $joinTableName,
        string $joinTableAlias,
        string $joinField,
        SalesChannelContext $salesChannelContext,
        bool $hasVersion = false
    ): void {
        $columns = [];
        $mainTableName = str_replace('_translation', '', $translationTableName);
        $primaryIdColumn = $mainTableName . '_id';
        $versionIdColumn = $mainTableName . '_version_id';
        $translationQuery = $query->getConnection()->createQueryBuilder();
        $translationQuery->from($joinTableName, $joinTableAlias);

        $context = $salesChannelContext->getContext();
        $languageIdChain = $this->createLanguageIdChain($salesChannelContext);

        foreach ($languageIdChain as $languageId) {
            $language = IdValue::create($languageId);
            $alias = uniqid($translationTableName, false);
            $languageParameter = 'languageId' . $alias;
            $joinCondition = sprintf(
                '%s.%s = %s.%s AND %s.language_id = :%s',
                $joinTableAlias,
                $joinField,
                $alias,
                $primaryIdColumn,
                $alias,
                $languageParameter
            );

            if ($hasVersion) {
                $joinCondition .= sprintf(' AND %s.%s = :versionId', $alias, $versionIdColumn);
                $query->setParameter('versionId', IdValue::create($context->getVersionId())->getStorageValue());
            }

            $translationQuery->leftJoin(
                $joinTableAlias,
                $translationTableName,
                $alias,
                $joinCondition
            );
            $query->setParameter($languageParameter, $language->getStorageValue());

            $columns[] = $alias . '.' . $columnName;
        }

        $translationQuery->addSelect(sprintf('DISTINCT COALESCE(%s) as %s', implode(',', $columns), $columnAlias));
        $translationQuery->addSelect($joinTableAlias . '.' . $joinField . ' as ' . $primaryIdColumn);

        $joinAlias = $columnAlias . '_translation';
        $query->addSelect($joinAlias . '.' . $columnAlias);

        $query->leftJoin($joinTableAlias, '(' . $translationQuery->getSQL() . ')', $joinAlias, sprintf(
            '%s.%s = %s.%s',
            $joinTableAlias,
            $joinField,
            $joinAlias,
            $primaryIdColumn
        ));
    }

    /**
     * @protected
     */
    protected function createLanguageIdChain(SalesChannelContext $salesChannelContext): array
    {
        $languageIdChain = $salesChannelContext
            ->getContext()
            ->getLanguageIdChain();

        $language = $salesChannelContext
            ->getSalesChannel()
            ->getLanguage();

        if (!$language || !$language->getParentId()) {
            return $languageIdChain;
        }

        return $this->prioritizeParentIdInLanguageChain($language->getParentId(), $languageIdChain);
    }

    /**
     * @protected
     */
    protected function prioritizeParentIdInLanguageChain(string $parentLanguageId, array $languageChain): array
    {
        if (($index = array_search($parentLanguageId, $languageChain, true)) !== false) {
            unset($languageChain[$index]);
        }

        array_splice($languageChain, 1, 0, $parentLanguageId);

        return $languageChain;
    }
}
