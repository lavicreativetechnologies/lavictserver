<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use Shopware\B2B\Shop\Framework\RoundingInterface;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Checkout\Cart\Price\PriceRounding;

class RoundingService implements RoundingInterface
{
    private const DECIMAL_PRECISION = 2;

    /**
     * @var PriceRounding|null
     */
    private $priceRounding;

    /**
     * @var CashRounding|null
     */
    private $cashRounding;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        ?PriceRounding $priceRounding,
        ?CashRounding $cashRounding,
        ContextProvider $contextProvider
    ) {
        $this->priceRounding = $priceRounding;
        $this->cashRounding = $cashRounding;
        $this->contextProvider = $contextProvider;
    }

    public function round(float $netPrice, float $tax = 0, int $quantity = null): float
    {
        if (!$quantity) {
            $quantity = 1;
        }

        $grossPrice = ($netPrice / 100 * (100 + $tax)) * $quantity;

        if ($this->priceRounding) {
            return $this->getPriceRoundingPrice($grossPrice);
        }

        return $this->getCashRoundingPrice($grossPrice);
    }

    private function getPriceRoundingPrice(float $grossPrice): float
    {
        return $this->priceRounding->round(
            $grossPrice,
            self::DECIMAL_PRECISION
        );
    }

    private function getCashRoundingPrice(float $grossPrice): float
    {
        $context = $this->contextProvider->getSalesChannelContext();

        return $this->cashRounding->cashRound($grossPrice, $context->getItemRounding());
    }
}
