<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use function class_exists;

if (class_exists('\\Shopware\\Core\\Framework\\DataAbstractionLayer\\EntityExtension')) {
    abstract class EntityExtension extends \Shopware\Core\Framework\DataAbstractionLayer\EntityExtension
    {
    }
} else {
    // TODO: Remove with 6.1 support drop
    abstract class EntityExtension implements \Shopware\Core\Framework\DataAbstractionLayer\EntityExtensionInterface
    {
    }
}
