<?php declare(strict_types=1);

namespace Shopware\B2B\Shop\BridgePlatform;

use DateTimeInterface;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class DateTimeDataType
{
    /**
     * @var DateTimeInterface
     */
    private $dateTime;

    public function __construct(DateTimeInterface $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    public static function getDateTimeData(): EventDataType
    {
        $dateType = new ObjectType();

        $dateType->add('year', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $dateType->add('month', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $dateType->add('monthName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $dateType->add('Day', new ScalarValueType(ScalarValueType::TYPE_STRING));

        return $dateType;
    }

    public function __toString(): string
    {
        return $this->dateTime->format('YYYY-MM-DD');
    }

    public function getYear(): string
    {
        return $this->dateTime->format('YYYY');
    }

    public function getMonth(): string
    {
        return $this->dateTime->format('MM');
    }

    public function getMonthName(): string
    {
        return $this->dateTime->format('MMM');
    }

    public function getDay(): string
    {
        return $this->dateTime->format('DD');
    }
}
