<?php declare(strict_types=1);

namespace Shopware\B2B\Account\Frontend;

use InvalidArgumentException;
use Shopware\B2B\Account\BridgePlatform\InvalidPasswordException;
use Shopware\B2B\Account\BridgePlatform\TooShortPasswordException;
use Shopware\B2B\Account\Framework\AccountImageServiceInterface;
use Shopware\B2B\Account\Framework\AccountRepositoryInterface;
use Shopware\B2B\Account\Framework\AccountServiceInterface;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Shop\Framework\MessageFormatterInterface;
use Shopware\B2B\Shop\Framework\StorageInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use function array_key_exists;

class AccountController
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var AccountServiceInterface
     */
    private $accountService;

    /**
     * @var StorageInterface
     */
    private $sessionStorage;

    /**
     * @var AccountImageServiceInterface
     */
    private $accountImageService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var AccountRepositoryInterface
     */
    private $accountRepository;

    /**
     * @var MessageFormatterInterface
     */
    private $messageFormatter;

    public function __construct(
        AuthenticationService $authenticationService,
        AccountServiceInterface $accountService,
        StorageInterface $sessionStorage,
        AccountImageServiceInterface $accountImageService,
        AuthStorageAdapterInterface $authStorageAdapter,
        AccountRepositoryInterface $accountRepository,
        MessageFormatterInterface $messageFormatter
    ) {
        $this->authenticationService = $authenticationService;
        $this->accountService = $accountService;
        $this->sessionStorage = $sessionStorage;
        $this->accountImageService = $accountImageService;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->accountRepository = $accountRepository;
        $this->messageFormatter = $messageFormatter;
    }

    public function indexAction(): array
    {
        $identity = $this->authenticationService
            ->getIdentity();

        $this->messageFormatter->formatSessionMessage('ChangeMessage');

        $message = $this->sessionStorage->get('ChangeMessage');

        $this->sessionStorage->remove('ChangeMessage');

        return [
            'identity' => $identity->getEntity(),
            'avatar' => $identity->getAvatar(),
            'changeMessage' => $message,
            'newsletter' => $this->accountRepository->hasNewsletter($identity),
        ];
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function savePasswordAction(Request $request): void
    {
        $request->checkPost();

        try {
            $currentPassword = $request->requireParam('currentPassword');
            $password = $request->requireParam('password');
            $passwordConfirm = $request->requireParam('passwordConfirmation');
        } catch (InvalidArgumentException $e) {
            $this->sessionStorage->set('ChangeMessage', ['error', 'RequiredFieldsEmpty']);
            throw new B2bControllerRedirectException('index', 'b2baccount');
        }

        if ($password !== $passwordConfirm) {
            $this->sessionStorage->set('ChangeMessage', ['error', 'NoPasswordConfirm']);
            throw new B2bControllerRedirectException('index', 'b2baccount');
        }

        try {
            $this->accountService->savePassword($currentPassword, $password);
        } catch (TooShortPasswordException $e) {
            $minPasswordLength = $e->getMinPasswordLength();
            $this->sessionStorage->set('ChangeMessage', ['error', $e->getMessage(), ['%int%' => $minPasswordLength]]);

            throw new B2bControllerRedirectException('index', 'b2baccount');
        } catch (InvalidPasswordException $e) {
            $this->sessionStorage->set('ChangeMessage', ['error', $e->getMessage()]);

            throw new B2bControllerRedirectException('index', 'b2baccount');
        }

        $this->sessionStorage->set('ChangeMessage', ['success', 'PasswordChange']);
        throw new B2bControllerRedirectException('index', 'b2baccount');
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function processUploadAction(Request $request): void
    {
        $request->checkPost();

        $imageProfile = $request->requireFileParam('uploadedFile');
        $identity = $this->authenticationService->getIdentity();
        $imgData = $this->accountImageService->uploadImage($identity->getAuthId(), $imageProfile);

        if ($imgData['success']) {
            $identity->setAvatar($imgData['path']);
            $this->authStorageAdapter->setIdentity($identity);

            $this->sessionStorage->set('ChangeMessage', ['success', 'ProfilePictureChanged']);
        } else {
            $snippetKey = array_key_exists('wrongFileExtension', $imgData) ? 'PictureFileExtensionNotValid' : 'ProfilePictureNotChanged';
            $this->sessionStorage->set('ChangeMessage', ['error', $snippetKey]);
        }

        throw new B2bControllerRedirectException('index', 'b2baccount');
    }

    public function saveNewsletterAction(Request $request): void
    {
        $identity = $this->authenticationService->getIdentity();
        $newsletter = (bool) $request->getParam('newsletter', false);

        $this->accountService->saveNewsletter($newsletter, $identity);

        $this->sessionStorage->set('ChangeMessage', ['success', 'AccountNewsletterSuccess']);
        throw new B2bControllerRedirectException('index', 'b2baccount');
    }
}
