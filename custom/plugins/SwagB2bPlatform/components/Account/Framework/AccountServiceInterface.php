<?php declare(strict_types=1);

namespace Shopware\B2B\Account\Framework;

use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

interface AccountServiceInterface
{
    /**
     * Set a new password for the current authenticated identity
     */
    public function savePassword(string $currentPassword, string $newPassword);

    public function saveNewsletter(bool $subscribeNewsletter, Identity $identity);
}
