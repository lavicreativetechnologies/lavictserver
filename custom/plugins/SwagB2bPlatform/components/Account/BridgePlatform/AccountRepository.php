<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Account\Framework\AccountRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class AccountRepository implements AccountRepositoryInterface
{
    const TABLE_NAME = 'customer';
    const TABLE_ALIAS = 'customer';

    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    public function hasNewsletter(Identity $identity): bool
    {
        return (bool) $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.newsletter')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'newsletter_recipient', 'recipient', self::TABLE_ALIAS . '.email = recipient.email')
            ->where(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $identity->getEntity()->email)
            ->execute()
            ->fetchColumn();
    }
}
