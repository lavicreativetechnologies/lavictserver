<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use InvalidArgumentException;
use Shopware\B2B\Common\B2BException;

class InvalidPasswordException extends InvalidArgumentException implements B2BException
{
}
