<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Content\Newsletter\Exception\NewsletterRecipientNotFoundException;
use Shopware\Core\Content\Newsletter\NewsletterSubscriptionService as CoreNewsletterSubscriptionService;
use Shopware\Core\Content\Newsletter\NewsletterSubscriptionServiceInterface;
use Shopware\Core\Content\Newsletter\SalesChannel\NewsletterSubscribeRoute;
use Shopware\Core\Content\Newsletter\SalesChannel\NewsletterUnsubscribeRoute;
use Shopware\Core\Framework\Validation\DataBag\DataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class NewsletterSubscriptionService
{
    /**
     * @var NewsletterSubscribeRoute|null
     */
    private $subscribeRoute;

    /**
     * @var NewsletterUnsubscribeRoute|null
     */
    private $unsubscribeRoute;

    /**
     * @var NewsletterSubscriptionServiceInterface|null
     */
    private $newsletterSubscriptionService;

    /**
     * @var NewsletterRepository
     */
    private $newsletterRepository;

    public function __construct(
        ?NewsletterSubscribeRoute $subscribeRoute,
        ?NewsletterUnsubscribeRoute $unsubscribeRoute,
        ?NewsletterSubscriptionServiceInterface $newsletterSubscriptionService,
        NewsletterRepository $newsletterRepository
    ) {
        $this->subscribeRoute = $subscribeRoute;
        $this->unsubscribeRoute = $unsubscribeRoute;
        $this->newsletterSubscriptionService = $newsletterSubscriptionService;
        $this->newsletterRepository = $newsletterRepository;
    }

    public function subscribeNewsletter(Identity $identity, SalesChannelContext $salesChannelContext): void
    {
        $customerDataBag = $this->createCustomerDataBag(true, $identity);

        if (!ShopwareVersion::isShopware6_2_X()) {
            $this->newsletterSubscriptionService->subscribe(
                $customerDataBag,
                $salesChannelContext
            );
        } else {
            $this->subscribeRoute->subscribe($customerDataBag->toRequestDataBag(), $salesChannelContext, false);
        }

        $this->newsletterRepository->setNewsletterFlag($identity, true);
    }

    public function unsubscribeNewsletter(Identity $identity, SalesChannelContext $salesChannelContext): void
    {
        $customerDataBag = $this->createCustomerDataBag(false, $identity);

        try {
            if (!ShopwareVersion::isShopware6_2_X()) {
                $this->newsletterSubscriptionService->unsubscribe(
                    $customerDataBag,
                    $salesChannelContext
                );
            } else {
                $this->unsubscribeRoute->unsubscribe($customerDataBag->toRequestDataBag(), $salesChannelContext);
            }

            $this->newsletterRepository->clearNewsletterRecipient($identity);
            $this->newsletterRepository->setNewsletterFlag($identity, false);
        } catch (NewsletterRecipientNotFoundException $notFoundException) {
            // email has no active subscription
        }
    }

    /**
     * @internal
     */
    protected function createCustomerDataBag(bool $subscription, Identity $identity): DataBag
    {
        $postalSettings = $identity->getPostalSettings();

        if (!ShopwareVersion::isShopware6_2_X()) {
            $direct = CoreNewsletterSubscriptionService::STATUS_DIRECT;
        } else {
            $direct = NewsletterSubscribeRoute::STATUS_DIRECT;
        }

        $option = $subscription ? $direct : 'unsubscribe';

        $dataBag = new DataBag();
        $dataBag->set('email', $postalSettings->email);
        $dataBag->set('title', $postalSettings->title);
        $dataBag->set('firstName', $postalSettings->firstName);
        $dataBag->set('lastName', $postalSettings->lastName);
        $dataBag->set('option', $option);

        return $dataBag;
    }
}
