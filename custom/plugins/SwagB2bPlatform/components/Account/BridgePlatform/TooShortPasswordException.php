<?php declare(strict_types=1);

namespace Shopware\B2B\Account\BridgePlatform;

use Throwable;

class TooShortPasswordException extends InvalidPasswordException
{
    /**
     * @var int
     */
    private $minPasswordLength;

    public const MESSAGE_KEY = 'ThePasswordMustBeAtLeastIntCharactersLong';

    public function __construct(
        int $minPasswordLength = 8,
        int $code = 0,
        ?Throwable $previous = null
    ) {
        $this->minPasswordLength = $minPasswordLength;
        parent::__construct(self::MESSAGE_KEY, $code, $previous);
    }

    public function getMinPasswordLength(): int
    {
        return $this->minPasswordLength;
    }
}
