<?php declare(strict_types=1);

namespace Shopware\B2B\Currency\Framework;

class CurrencyCalculator
{
    /**
     * @param CurrencyAware[] $currencyAwares
     */
    public function recalculateAmounts(array $currencyAwares, CurrencyContext $currencyContext): void
    {
        foreach ($currencyAwares as $currencyAware) {
            $this->recalculateAmount($currencyAware, $currencyContext);
        }
    }

    public function recalculateAmount(CurrencyAware $currencyAware, CurrencyContext $currencyContext): CurrencyAware
    {
        foreach ($currencyAware->getAmountPropertyNames() as $propertyName) {
            $currencyAware->{$propertyName} = ($currencyAware->{$propertyName} / $currencyAware->getCurrencyFactor()) * $currencyContext->currentCurrencyFactor;
        }

        return $currencyAware;
    }

    public function getSqlCalculationPart(
        string $amountPropertyName,
        string $factorPropertyName,
        CurrencyContext $currencyContext,
        string $tableAlias = null
    ): string {
        if ($tableAlias) {
            return "(($tableAlias.$amountPropertyName/$tableAlias.$factorPropertyName) * {$currencyContext->currentCurrencyFactor})";
        }

        return "(($amountPropertyName/$factorPropertyName) * {$currencyContext->currentCurrencyFactor})";
    }
}
