<?php declare(strict_types=1);

namespace Shopware\B2B\Common\MvcExtension;

use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\IdValue;
use Symfony\Component\HttpFoundation\File\File;

interface Request
{
    /**
     * @param $key
     * @param mixed|null $default
     */
    public function getParam($key, $default = null);

    public function getIdValue($key, $default = null): IdValue;

    /**
     * @param $key
     */
    public function requireFileParam($key): File;

    /**
     * @param $key
     */
    public function requireParam($key);

    public function requireIdValue($key): IdValue;

    public function isPost(): bool;

    public function getPost(): array;

    public function getFiles(): array;

    public function getPathInfo(): string;

    public function hasParam(string $key): bool;

    /**
     * @throws B2bControllerRedirectException
     */
    public function checkPost(string $action = 'index', array $params = [], string $controller = null);
}
