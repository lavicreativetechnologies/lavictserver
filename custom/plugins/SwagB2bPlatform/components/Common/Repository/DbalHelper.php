<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use RuntimeException;
use Shopware\B2B\Common\Filter\QueryExtender;
use Throwable;
use function is_int;

class DbalHelper
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var QueryExtender
     */
    private $queryExtender;

    public function __construct(Connection $connection, QueryExtender $queryExtender)
    {
        $this->connection = $connection;
        $this->queryExtender = $queryExtender;
    }

    /**
     * @throws RuntimeException
     * @return mixed the callbacks result
     */
    public function transact(callable $inner)
    {
        $this->connection->beginTransaction();
        try {
            $result = $inner();
        } catch (Throwable $t) {
            $this->connection->rollBack();
            throw new RuntimeException('Transaction failed', 0, $t);
        }

        $this->connection->commit();

        return $result;
    }

    public function applySearchStruct(SearchStruct $searchStruct, QueryBuilder $query): void
    {
        if ($searchStruct->orderBy) {
            $query->orderBy($searchStruct->orderBy, $searchStruct->orderDirection);
        }

        if (is_int($searchStruct->offset) && $searchStruct->offset > 0) {
            $query->setFirstResult($searchStruct->offset);
        }

        if (is_int($searchStruct->limit) && $searchStruct->limit > 0) {
            $query->setMaxResults($searchStruct->limit);
        }

        $this->applyFilters($searchStruct, $query);
    }

    public function applyFilters(SearchStruct $searchStruct, QueryBuilder $query): void
    {
        $this->queryExtender->extendQueryBuilder($searchStruct->filters, $query);
    }
}
