<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

use Shopware\Core\Framework\Uuid\Uuid;

class UuidIdValue extends IdValue
{
    /**
     * @var string
     */
    private $hexValue;

    public function __construct(string $hexValue)
    {
        $this->hexValue = $hexValue;
    }

    public function getStorageValue(): string
    {
        return Uuid::fromHexToBytes($this->hexValue);
    }

    public function getValue(): string
    {
        return $this->hexValue;
    }

    public function jsonSerialize()
    {
        return $this->getValue();
    }

    public function equals(IdValue $value): bool
    {
        return $value instanceof UuidIdValue && $value->hexValue === $this->hexValue;
    }
}
