<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

class IntIdValue extends IdValue
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getStorageValue(): int
    {
        return $this->value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function equals(IdValue $value): bool
    {
        return $value instanceof IntIdValue && $value->value === $this->value;
    }

    public function jsonSerialize()
    {
        return $this->getValue();
    }

    public function __toString()
    {
        return (string) $this->getValue();
    }
}
