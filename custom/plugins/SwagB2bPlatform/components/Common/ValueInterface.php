<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

interface ValueInterface
{
    public function getValue();
}
