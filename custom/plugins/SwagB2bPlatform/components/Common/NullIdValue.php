<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

class NullIdValue extends IdValue
{
    public function __construct()
    {
    }

    public function getStorageValue()
    {
        return null;
    }

    public function getValue()
    {
        return null;
    }

    public function equals(IdValue $value): bool
    {
        return $value instanceof self;
    }

    public function jsonSerialize()
    {
        return null;
    }

    public function __toString()
    {
        return 'null';
    }
}
