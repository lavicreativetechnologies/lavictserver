<?php declare(strict_types=1);

namespace Shopware\B2B\Common\RestApi;

use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Routing\Router;
use function ksort;

class RootController
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return string[]
     */
    public function indexAction(Request $request): array
    {
        $return = [];
        $routes = $this->router->getRoutes();

        $apiBaseUrl = $request->getPathInfo();

        foreach ($routes as $route) {
            $url = $apiBaseUrl . $route->getQuery();

            $return[$url][] = [
                'method' => $route->getMethod(),
                'url' => $url,
                'params' => $route->getParamOrder(),
            ];
        }
        ksort($return);

        return $return;
    }
}
