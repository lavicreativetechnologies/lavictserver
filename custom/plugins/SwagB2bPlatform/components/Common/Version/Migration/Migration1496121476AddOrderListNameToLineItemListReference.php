<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1496121476AddOrderListNameToLineItemListReference implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1496121476;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_line_item_reference`
                ADD COLUMN `order_list_name` VARCHAR(255) NULL DEFAULT NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
