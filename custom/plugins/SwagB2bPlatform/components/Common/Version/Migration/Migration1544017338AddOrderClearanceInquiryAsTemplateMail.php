<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Models\Mail\Mail;
use Shopware_Components_Translation;
use Symfony\Component\DependencyInjection\Container;
use function array_merge;
use function file_get_contents;

class Migration1544017338AddOrderClearanceInquiryAsTemplateMail implements MigrationStepInterface
{
    const LANGUAGES = [
        'german' => 'de_DE',
        'english' => 'en_GB',
    ];

    public function getCreationTimeStamp(): int
    {
        return 1544017338;
    }

    public function updateDatabase(Connection $connection): void
    {
        // nth
    }

    public function updateThroughServices(Container $container): void
    {
        $modelManager = $container->get('models');

        $mailRepo = $modelManager->getRepository('Shopware\Models\Mail\Mail');

        if ($mailRepo->findOneBy(['name' => 'b2bOrderClearanceInquiry'])) {
            return;
        }

        $query = $container->get('dbal_connection')->createQueryBuilder();

        $fixturePath = __DIR__ . '/../../../OrderClearance/Fixtures/Inquiry/';

        $mail = new Mail();

        $emailContent = [
            'name' => 'b2bOrderClearanceInquiry',
            'fromMail' => '{config name=mail}',
            'fromName' => '{config name=shopName}',
            'subject' => 'A contact has requested an order clearance',
            'content' => file_get_contents($fixturePath . 'plain_en.tpl'),
            'contentHtml' => file_get_contents($fixturePath . 'html_en.tpl'),
            'isHtml' => true,
            'mailType' => $mail::MAILTYPE_SYSTEM,
        ];

        $translations = [
            'subject' => 'Ein Kontakt hat eine Bestellfreigabe beantragt',
            'content' => file_get_contents($fixturePath . 'plain_de.tpl'),
            'contentHtml' => file_get_contents($fixturePath . 'html_de.tpl'),
        ];

        $defaultShopLanguage = $query->select('locales.locale')
            ->from('s_core_locales', 'locales')
            ->leftJoin('locales', 's_core_shops', 'shops', 'shops.locale_id = locales.id')
            ->where('shops.default = 1')
            ->execute()
            ->fetch(PDO::FETCH_COLUMN);

        $foreignLanguage = self::LANGUAGES['german'];

        if ($defaultShopLanguage === self::LANGUAGES['german']) {
            $germanMailContent = [
                'subject' => 'Ein Kontakt hat eine Bestellfreigabe beantragt',
                'content' => file_get_contents($fixturePath . 'plain_de.tpl'),
                'contentHtml' => file_get_contents($fixturePath . 'html_de.tpl'),
            ];

            $translations = [
                'subject' => 'A contact has requested an order clearance',
                'content' => file_get_contents($fixturePath . 'plain_en.tpl'),
                'contentHtml' => file_get_contents($fixturePath . 'html_en.tpl'),
            ];

            $foreignLanguage = self::LANGUAGES['english'];

            $emailContent = array_merge($emailContent, $germanMailContent);
        }

        $mail->fromArray($emailContent);

        $modelManager->persist($mail);
        $modelManager->flush();

        $mail = $mailRepo->findOneBy(['name' => 'b2bOrderClearanceInquiry']);

        $foreignShopIds = $query->select('coreShops.id')
            ->from('s_core_shops', 'coreShops')
            ->leftJoin('coreShops', 's_core_locales', 'coreLocales', 'coreShops.locale_id = coreLocales.id')
            ->where('coreLocales.locale = :locale')
            ->setParameter('locale', $foreignLanguage)
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);

        /** @var Shopware_Components_Translation $translation */
        $translation = $container->get('translation');

        foreach ($foreignShopIds as $shopId) {
            $translation->write($shopId, 'config_mails', $mail->getId(), $translations);
        }
    }
}
