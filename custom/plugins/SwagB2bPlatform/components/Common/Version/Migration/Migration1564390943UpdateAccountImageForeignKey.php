<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1564390943UpdateAccountImageForeignKey implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1564390943;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_store_front_auth`
            DROP FOREIGN KEY `FK_b2b_store_front_auth_s_media_id`;
            
            ALTER TABLE `b2b_store_front_auth`
            ADD CONSTRAINT `FK_b2b_store_front_auth_s_media_id`
            FOREIGN KEY (media_id) REFERENCES s_media (id) ON DELETE SET NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
