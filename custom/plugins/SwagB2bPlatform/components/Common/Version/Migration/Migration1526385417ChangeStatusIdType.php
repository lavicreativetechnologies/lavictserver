<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1526385417ChangeStatusIdType implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1526385417;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_order_context` MODIFY `status_id` int(11) NOT NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
