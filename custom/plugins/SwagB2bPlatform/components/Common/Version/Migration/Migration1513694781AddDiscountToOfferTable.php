<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1513694781AddDiscountToOfferTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1513694781;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_offer`
            ADD COLUMN `discount_value_net` DOUBLE DEFAULT NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
    }
}
