<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Bundle\AttributeBundle\Service\TypeMapping;
use Shopware\Models\Mail\Mail;
use Shopware_Components_Translation;
use Symfony\Component\DependencyInjection\Container;
use function array_keys;
use function array_map;
use function array_merge;
use function file_get_contents;

class Migration1561096915FixStatusChangeNotifications implements MigrationStepInterface
{
    const OLD_MIGRATION = __DIR__ . '/Migration1543417751AddOfferStatusChangedNotificationMails.php';

    const FIXTURE_PATH = __DIR__ . '/../../../Offer/Fixtures/b2bOfferStatusChangedNotificationMail/';

    const MAIL_NAME = 'b2bOfferStatusChanged_general';

    protected $default_locale;

    protected $translations_old;

    protected $translations;

    public function getCreationTimeStamp(): int
    {
        return 1561096915;
    }

    public function updateDatabase(Connection $connection): void
    {
        //nth
    }

    public function updateThroughServices(Container $container): void
    {
        $this->default_locale = $container->get('models')
            ->getRepository('Shopware\Models\Shop\Shop')
            ->getDefault()->getLocale()->getLocale();

        $this->translations_old = [
            'en_GB' => [
                'content' => file_get_contents(self::FIXTURE_PATH . 'old/plain_en.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'old/html_en.tpl'),
            ],
            'de_DE' => [
                'content' => file_get_contents(self::FIXTURE_PATH . 'old/plain_de.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'old/html_de.tpl'),
            ],
        ];

        $this->translations = [
            'en_GB' => [
                'content' => file_get_contents(self::FIXTURE_PATH . 'plain_en.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'html_en.tpl'),
            ],
            'de_DE' => [
                'content' => file_get_contents(self::FIXTURE_PATH . 'plain_de.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'html_de.tpl'),
            ],
        ];

        if (!$this->default_locale) {
            $this->default_locale = 'en_GB';
        }

        $this->fixCheckboxSettings($container);
        $this->checkMailSettings($container);
    }

    /**
     * @internal
     */
    protected function fixCheckboxSettings(Container $container): void
    {
        $attributeService = $container->get('shopware_attribute.crud_service');

        $attributeData = $attributeService->get('s_core_auth_attributes', 'send_offer_status_changed_notification_mail');

        if (!$attributeData) {
            $translations = [
                'en_GB' => [
                    'label' => 'Receive offer status notification',
                    'supportText' => 'Activate or deactivate notification mail',
                    'helpText' => 'The system sends out an email to every admin, who checked this box, when the status of an offer changed and an admin is required to proceed.',
                ],
                'de_DE' => [
                    'label' => 'Erhalte Nachrichten bei Angebotsstatusänderungen',
                    'supportText' => 'Aktiviere oder deaktiviere Benachrichtigungsmail',
                    'helpText' => 'Das System schickt an jeden Administrator, der dieses Feld anwählt eine E-Mail, jedesmal, wenn sich der Status eines Angbots ändert und es einen Administrator zum Fortfahren benötigt.',
                ],
            ];

            $attributeData = array_merge([
                'translatable' => false,
                'displayInBackend' => true,
            ], $translations[$this->default_locale]);

            $attributeService->update(
                's_core_auth_attributes',
                'send_offer_status_changed_notification_mail',
                TypeMapping::TYPE_BOOLEAN,
                $attributeData
            );
        }
    }

    /**
     * @internal
     */
    protected function checkMailSettings(Container $container): void
    {
        $modelManager = $container->get('models');
        $mailRepo = $modelManager->getRepository('Shopware\Models\Mail\Mail');

        if ($mail = $mailRepo->findOneBy(['name' => self::MAIL_NAME])) {
            $this->fixMailSettings($container, $mail);
        } else {
            $this->addMailTemplate($container);
        }
    }

    /**
     * @internal
     */
    protected function fixMailSettings(Container $container, Mail $mail): void
    {
        foreach ($this->translations_old as $locale => $translation_old) {
            if ($mail->getContent() === $translation_old['content']) {
                $mail->setContent($this->translations[$locale]['content']);
            }

            if ($mail->getContentHtml() === $translation_old['contentHtml']) {
                $mail->setContentHtml($this->translations[$locale]['contentHtml']);
            }
        }

        if (empty($mail->getContent())) {
            $mail->setContent($this->translations[$this->default_locale]['content']);
        }

        if (empty($mail->getContentHtml())) {
            $mail->setContentHtml($this->translations[$this->default_locale]['contentHtml']);
        }

        $container->get('models')->flush($mail);

        $this->fixMailTranslations($mail, $container);
    }

    /**
     * @internal
     */
    protected function addMailTemplate(Container $container): void
    {
        $mail = new Mail();

        $translations = [
            'en_GB' => [
                'subject' => 'The status of {if $context.isToEnquirer}your offer{else}the offer by {$offer.enquirer}{/if} at {config name=shopName} has changed',
                'content' => file_get_contents(self::FIXTURE_PATH . 'plain_en.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'html_en.tpl'),
            ],
            'de_DE' => [
                'subject' => 'Der Status {if $context.isToEnquirer}deines Angebotes{else}des Angebots von{$offer.enquirer}{/if} bei {config name=shopName} hat sich ge&auml;ndert.',
                'content' => file_get_contents(self::FIXTURE_PATH . 'plain_de.tpl'),
                'contentHtml' => file_get_contents(self::FIXTURE_PATH . 'html_de.tpl'),
            ],
        ];

        $emailContent = array_merge([
            'name' => self::MAIL_NAME,
            'fromMail' => '{config name=mail}',
            'fromName' => '{config name=shopName}',
            'isHtml' => true,
            'mailType' => Mail::MAILTYPE_SYSTEM,
        ], $translations[$this->default_locale]);

        unset($translations[$this->default_locale]);

        $mail->fromArray($emailContent);

        $modelManager = $container->get('models');
        $modelManager->persist($mail);
        $modelManager->flush();

        $mailId = $modelManager->getRepository('Shopware\Models\Mail\Mail')
            ->findOneBy(['name' => self::MAIL_NAME])->getId();
        $this->addMailTranslations($container, $translations, $mailId);
    }

    /**
     * @internal
     */
    protected function addMailTranslations(Container $container, array $translations, int $mailId): void
    {
        foreach (array_keys($translations) as $foreignLanguage) {
            $foreignShopIds = $container->get('dbal_connection')
                ->createQueryBuilder()
                ->select('coreShops.id')
                ->from('s_core_shops', 'coreShops')
                ->leftJoin('coreShops', 's_core_locales', 'coreLocales', 'coreShops.locale_id = coreLocales.id')
                ->where('coreLocales.locale = :locale')
                ->setParameter('locale', $foreignLanguage)
                ->execute()
                ->fetchAll(PDO::FETCH_COLUMN);

            /** @var Shopware_Components_Translation $translation */
            $translation = $container->get('translation');

            foreach ($foreignShopIds as $shopId) {
                $translation->write($shopId, 'config_mails', $mailId, $translations[$foreignLanguage]);
            }
        }
    }

    /**
     * @internal
     */
    protected function fixMailTranslations(Mail $mail, Container $container): void
    {
        $translationLanguages = $this->getAlreadyDefinedTranslationLanguages($mail, $container);

        if (empty($translationLanguages)) {
            $translations = $this->translations;
            unset($translations[$this->default_locale]);

            $this->addMailTranslations($container, $translations, $mail->getId());

            return;
        }

        /** @var Shopware_Components_Translation $translationsComp */
        $translationsComp = $container->get('translation');

        $translations = array_map(function ($lang) use ($mail, $translationsComp) {
            return $translationsComp->read($lang, 'config_mails', $mail->getId());
        }, $translationLanguages);

        foreach ($translations as $translation) {
            $this->checkAndFixSingleTranslation(
                $translation,
                $translation['objectlanguage'],
                $mail->getId(),
                $translationsComp
            );
        }
    }

    /**
     * @internal
     */
    protected function getAlreadyDefinedTranslationLanguages(Mail $mail, Container $container)
    {
        return $container->get('dbal_connection')->createQueryBuilder()
            ->select('objectlanguage')
            ->from('s_core_translations')
            ->where('objecttype = :objectType')
            ->andWhere('objectkey = :objectKey')
            ->setParameters([
                'objectType' => 'config_mails',
                'objectKey' => $mail->getId(),
            ])
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * @internal
     */
    protected function checkAndFixSingleTranslation(
        array $translation,
        string $lang,
        int $mailId,
        Shopware_Components_Translation $translationsComp
    ): void {
        foreach ($this->translations_old as $locale => $translation_old) {
            if (empty($translation['content']) || $translation_old['content'] === $translation['content']) {
                $translation['content'] = $this->translations[$locale]['content'];
                $changedFlag = true;
            }

            if (empty($translation['contentHtml']) || $translation_old['contentHtml'] === $translation['contentHtml']) {
                $translation['contentHtml'] = $this->translations[$locale]['contentHtml'];
                $changedFlag = true;
            }
        }

        if (isset($changedFlag)) {
            $translationsComp->write($lang, 'config_mails', $mailId, $translation, true);
        }
    }
}
