<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1579514614ChangeDebtorRepositoryFromBridgeToFramework implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1579514614;
    }

    public function updateDatabase(Connection $connection)
    {
        // deleted code to prevent error with duplicate debtor entries with different provider_keys in b2b_store_front_auth
        // handled in \Shopware\B2B\Common\Version\Migration\Migration1585899364FixDebtorIssueAfterUpdate301AndWorkingWithIt::updateDatabase
    }

    public function updateThroughServices(Container $container)
    {
        // nth
    }
}
