<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1582203053RemoveAcceptedOrderClearancesTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1582203053;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_order_context
                ADD COLUMN `cleared_by` INT(11) NULL DEFAULT NULL AFTER cleared_at,
                ADD CONSTRAINT `b2b_order_context_cleared_by_FK` FOREIGN KEY (`cleared_by`)
                     REFERENCES `b2b_store_front_auth` (`id`) ON UPDATE NO ACTION ON DELETE SET NULL;
        ');

        $connection->exec('
            UPDATE `b2b_order_context` orderContext
            INNER JOIN `b2b_accepted_order_clearances` clearance ON BINARY orderContext.ordernumber = BINARY clearance.order_number
            SET `cleared_by` = clearance.responsible_auth_id;
        ');

        $connection->exec('DROP TABLE b2b_accepted_order_clearances;');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
