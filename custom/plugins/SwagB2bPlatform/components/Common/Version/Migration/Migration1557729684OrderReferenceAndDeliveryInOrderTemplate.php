<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Shopware\Models\Mail\Mail;
use Symfony\Component\DependencyInjection\Container;
use function explode;
use function file_get_contents;
use function implode;

class Migration1557729684OrderReferenceAndDeliveryInOrderTemplate implements MigrationStepInterface
{
    const FIXTURE_PATH = __DIR__ . '/../../../Order/Fixtures/TemplateMailAddition/';

    public function getCreationTimeStamp(): int
    {
        return 1557729684;
    }

    public function updateDatabase(Connection $connection): void
    {
        // nth
    }

    public function updateThroughServices(Container $container): void
    {
        $queryBuilder = $container->get('dbal_connection')
            ->createQueryBuilder();

        $defaultShopLanguage = $this
            ->fetchDefaultShopLanguage($queryBuilder);

        $modelManager = $container->get('models');
        $mailRepository = $modelManager->getRepository('Shopware\Models\Mail\Mail');

        $mail = $mailRepository->findOneBy(['name' => 'sORDER']);

        $delimiter = '{/foreach}';
        $delimiterHtml = '</table>';

        $content = $this->createMailContent($mail, $delimiter, $defaultShopLanguage);
        $contentHtml = $this->createMailContent($mail, $delimiterHtml, $defaultShopLanguage, true);

        $mail->setContent($content);
        $mail->setContentHtml($contentHtml);

        $modelManager->persist($mail);
        $modelManager->flush();
    }

    protected function createMailContent(Mail $mail, string $delimiter, string $defaultShopLanguage, bool $isHtml = false): string
    {
        if ($isHtml) {
            $type = 'html';
            $content = $mail->getContentHtml();
        } else {
            $type = 'plain';
            $content = $mail->getContent();
        }

        $explodedContent = explode($delimiter, $content, 2);

        $country = explode('_', $defaultShopLanguage)[0];

        $contentAddition = file_get_contents(self::FIXTURE_PATH . 'sORDER_' . $type . '_' . $country . '.tpl');

        $explodedContent[0] .= $delimiter . $contentAddition;

        return implode($explodedContent);
    }

    protected function fetchDefaultShopLanguage(QueryBuilder $queryBuilder): string
    {
        return $queryBuilder->select('locales.locale')
            ->from('s_core_locales', 'locales')
            ->leftJoin('locales', 's_core_shops', 'shops', 'shops.locale_id = locales.id')
            ->where('shops.default = 1')
            ->execute()
            ->fetch(PDO::FETCH_COLUMN);
    }
}
