<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1579857126ChangeOrderContextStateToTranslatableString implements MigrationStepInterface
{
    private const ADD_COLUMN_QUERY = '
            ALTER TABLE `b2b_order_context`
                ADD COLUMN `state` VARCHAR(255) NULL DEFAULT NULL AFTER `status_id`;
        ';

    private const MIGRATE_DATA_QUERY = '
            UPDATE `b2b_order_context` orderContext
            INNER JOIN `s_core_states` state ON state.id = orderContext.status_id
            SET `state` = state.name;
        ';

    private const SOLIDIFY_SCHEMA_QUERY = '
            ALTER TABLE `b2b_order_context`
               DROP COLUMN `status_id`,
               MODIFY `state` VARCHAR(255) NOT NULL;
        ';

    public function getCreationTimeStamp(): int
    {
        return 1579857126;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec(self::ADD_COLUMN_QUERY);
        $connection->exec(self::MIGRATE_DATA_QUERY);
        $connection->exec(self::SOLIDIFY_SCHEMA_QUERY);
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
