<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1568194156EntityIdType implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1568194156;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE b2b_default_order_list modify entity_id VARCHAR(255) null;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
