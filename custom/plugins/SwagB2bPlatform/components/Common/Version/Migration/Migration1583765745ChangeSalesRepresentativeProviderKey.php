<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1583765745ChangeSalesRepresentativeProviderKey implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1583765745;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            UPDATE b2b_store_front_auth SET provider_key = \'Shopware\\\B2B\\\SalesRepresentative\\\Framework\\\SalesRepresentativeRepositoryInterface\'
            WHERE provider_key =
            \'Shopware\\\B2B\\\SalesRepresentative\\\Framework\\\SalesRepresentativeRepository\'
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
