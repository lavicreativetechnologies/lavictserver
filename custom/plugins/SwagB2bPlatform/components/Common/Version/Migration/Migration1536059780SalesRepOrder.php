<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1536059780SalesRepOrder implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1536059780;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_order_context`
            ADD COLUMN sales_representative_auth_id INT(11) DEFAULT NULL,
            ADD CONSTRAINT FK_b2b_order_context_b2b_store_front_auth FOREIGN KEY (`sales_representative_auth_id`)
              REFERENCES `b2b_store_front_auth`(`id`) ON DELETE SET NULL
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        //nth
    }
}
