<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1559826113ChangeProviderContextToVarchar implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1559826113;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            ALTER TABLE `b2b_store_front_auth`
                CHANGE COLUMN `provider_context` `provider_context` VARCHAR(255) NOT NULL;
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
