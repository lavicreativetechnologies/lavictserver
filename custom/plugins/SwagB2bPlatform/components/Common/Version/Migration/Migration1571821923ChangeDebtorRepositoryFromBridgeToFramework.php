<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1571821923ChangeDebtorRepositoryFromBridgeToFramework implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1571821923;
    }

    public function updateDatabase(Connection $connection)
    {
        $connection->exec('
            UPDATE b2b_store_front_auth SET provider_key = \'Shopware\\\B2B\\\Debtor\\\Bridge\\\DebtorRepository\'
            WHERE provider_key =
            \'Shopware\\\B2B\\\Debtor\\\Framework\\\DebtorRepositoryInterface\'
        ');
    }

    public function updateThroughServices(Container $container)
    {
        // nth
    }
}
