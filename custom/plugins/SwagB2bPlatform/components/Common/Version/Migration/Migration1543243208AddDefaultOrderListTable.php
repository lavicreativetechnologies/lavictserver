<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1543243208AddDefaultOrderListTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1543243208;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE b2b_default_order_list (
              order_list_id INTEGER,
              entity_id INTEGER
            );
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
