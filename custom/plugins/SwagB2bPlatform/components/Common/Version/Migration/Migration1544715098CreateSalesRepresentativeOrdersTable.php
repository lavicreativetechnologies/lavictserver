<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1544715098CreateSalesRepresentativeOrdersTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1544715098;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE `b2b_sales_representative_orders`(
                `id` INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                `order_number` VARCHAR(255),
                `client_auth_id` INT,
                `sales_representative_auth_id` INT
            )
        ');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
