<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1597304567MergeWrongSalesRepresentativeAuthEntries implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1597304567;
    }

    public function updateDatabase(Connection $connection)
    {
        $wrongSalesRepAuthData = $connection->fetchAll('
            SELECT id, provider_context
            FROM b2b_store_front_auth
            WHERE provider_key = \'Shopware\\\B2B\\\StoreFrontAuthentication\\\Framework\\\SalesRepresentativeRepositoryInterface\'
        ');

        if (!$wrongSalesRepAuthData) {
            return;
        }

        foreach ($wrongSalesRepAuthData as $wrongAuthData) {
            $this->setRightAuthId($connection, $wrongAuthData);
        }
    }

    /**
     * @internal
     */
    protected function setRightAuthId(Connection $connection, array $wrongAuthData)
    {
        $rightAuthId = $connection->fetchColumn('
                SELECT id
                FROM b2b_store_front_auth
                WHERE provider_key = \'Shopware\\\B2B\\\SalesRepresentative\\\Bridge\\\SalesRepresentativeRepository\'
                  AND provider_context = :providerContext
            ', [':providerContext' => $wrongAuthData['provider_context']]);

        if (!$rightAuthId) {
            $connection->exec('
                UPDATE b2b_store_front_auth
                SET provider_key = \'Shopware\\\B2B\\\SalesRepresentative\\\Bridge\\\SalesRepresentativeRepository\'
                WHERE id = ' . $wrongAuthData['id']
            );

            return;
        }

        $connection->exec('
            DELETE FROM b2b_store_front_auth WHERE id = ' . $wrongAuthData['id']
        );
    }

    public function updateThroughServices(Container $container)
    {
        // nth
    }
}
