<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Version\Migration;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\Migration\MigrationStepInterface;
use Symfony\Component\DependencyInjection\Container;

class Migration1584633430RemovePricesTable implements MigrationStepInterface
{
    public function getCreationTimeStamp(): int
    {
        return 1584633430;
    }

    public function updateDatabase(Connection $connection): void
    {
        $connection->exec('DROP TABLE b2b_prices;');
    }

    public function updateThroughServices(Container $container): void
    {
        // nth
    }
}
