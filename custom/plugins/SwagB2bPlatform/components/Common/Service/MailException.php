<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Service;

use RuntimeException;
use Shopware\B2B\Common\B2BException;

class MailException extends RuntimeException implements B2BException
{
}
