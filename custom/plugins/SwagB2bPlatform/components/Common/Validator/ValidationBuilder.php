<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Validator;

use LogicException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Validator\Constraints\ContainsNotOnlyWhitespace;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function in_array;
use function mb_strpos;
use function mb_strtoupper;
use function preg_match;

class ValidationBuilder
{
    public const CAUSE_IS_UNIQUE = 'isUnique';
    public const CAUSE_STARTS_WITH = 'startsWith';
    public const CAUSE_IS_GREATER_THAN = 'isGreaterThan';
    public const CAUSE_IS_LESS_THAN = 'isLessThan';
    public const CAUSE_IS_LESS_THAN_OR_EQUAL = 'isLessThanOrEqual';
    public const CAUSE_IS_NO_MY_SQL_KEYWORD = 'isNoMySqlKeyword';
    public const CAUSE_IS_A_LANGUAGE_CODE = 'isALanguageCode';
    public const CAUSE_STARTS_WITH_ALPHABETIC_CHARACTER = 'startsWithAlphabeticCharacter';
    public const CAUSE_CONTAINS = 'contains';
    public const CAUSE_IS_ID_VALUE = 'isIdValue';
    public const MAX_STRING_LENGTH_30 = 30;
    public const MAX_STRING_LENGTH_50 = 50;
    public const MAX_STRING_LENGTH_60 = 60;
    public const MAX_STRING_LENGTH_70 = 70;
    public const MAX_STRING_LENGTH_100 = 100;
    public const MAX_STRING_LENGTH_255 = 255;

    /**
     * @var array
     */
    private $assertions = [];

    /**
     * @var array|null
     */
    private $currentAssertion;

    /**
     * @return $this
     */
    public function validateThat(string $propertyName, $propertyValue): self
    {
        $this->finishAssert();

        $this->currentAssertion = [
            'name' => $propertyName,
            'value' => $propertyValue,
            'constraints' => [],
         ];

        return $this;
    }

    /**
     * @return $this
     */
    public function isNotBlank(): self
    {
        $this->addConstraint(new NotBlank());

        return $this;
    }

    /**
     * @return $this
     */
    public function containsNotOnlyWhitespace(): self
    {
        $this->addConstraint(new ContainsNotOnlyWhitespace());

        return $this;
    }

    /**
     * @return $this
     */
    public function isBool(): self
    {
        $this->addConstraint(new Type('bool'));

        return $this;
    }

    /**
     * @return $this
     */
    public function isString(): self
    {
        $this->addConstraint(new Type('string'));
        $this->addConstraint(new ContainsNotOnlyWhitespace());

        return $this;
    }

    /**
     * @return $this
     */
    public function isNumeric(): self
    {
        $this->addConstraint(new Type('numeric'));

        return $this;
    }

    /**
     * @return $this
     */
    public function isIdValueOrNullIdValue(): self
    {
        $this->addConstraint(new Type(IdValue::class));

        return $this;
    }

    /**
     * @return $this
     */
    public function isIdValue(): self
    {
        $this->addConstraint(new Type(IdValue::class));
        $this->withCallback(
            function ($value): bool {
                return !$value instanceof NullIdValue;
            },
            'This should not be a NullIdValue',
            self::CAUSE_IS_ID_VALUE
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isNullIdValue(): self
    {
        $this->addConstraint(new Type(NullIdValue::class));

        return $this;
    }

    /**
     * @return $this
     */
    public function isInt(): self
    {
        $this->addConstraint(new Type('int'));

        return $this;
    }

    /**
     * @return $this
     */
    public function contains(callable $isValid): self
    {
        $this->withCallback(
            function () use ($isValid): bool {
                return $isValid();
            },
            'This value "%value%" is not contained.',
            self::CAUSE_CONTAINS
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isEmail(): self
    {
        $this->addConstraint(new Email());

        return $this;
    }

    /**
     * @return $this
     */
    public function isBlank(): self
    {
        $this->addConstraint(new Blank());

        return $this;
    }

    /**
     * @return $this
     */
    public function isInArray(array $values): self
    {
        $this->addConstraint(new Choice($values));

        return $this;
    }

    /**
     * @return $this
     */
    public function isUnique(callable $isValid): self
    {
        $this->withCallback(
            function () use ($isValid): bool {
                return $isValid();
            },
            'This value is already used.',
            self::CAUSE_IS_UNIQUE
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isStringShorterThan(int $length): self
    {
        $this->addConstraint(new Length([
            'max' => $length,
            'maxMessage' => 'This value has exceeded the maximum length',
        ]));

        return $this;
    }

    /**
     * @return $this
     */
    public function startsWith(string $string): self
    {
        $this->withCallback(
            function ($value) use ($string): bool {
                return 0 === mb_strpos($value, $string);
            },
            'This value must start with "%string%".',
            self::CAUSE_STARTS_WITH,
            [
                '%string%' => $string,
            ]
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isGreaterThan(int $int, bool $strict = false): self
    {
        $this->withCallback(
            function ($value) use ($int): bool {
                return $value > $int;
            },
            'The value %value% must be greater than %int%.',
            self::CAUSE_IS_GREATER_THAN,
            [
                '%int%' => $int,
            ],
            $strict
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isGreaterEqualThan(int $int): self
    {
        $this->withCallback(
            function ($value) use ($int): bool {
                return $value >= $int;
            },
            'The value %value% must be greater equal than %int%.',
            self::CAUSE_IS_GREATER_THAN,
            [
                '%int%' => $int,
            ],
            true
        );

        return $this;
    }

    public function isLessThan(int $int, bool $strict = false): self
    {
        $this->withCallback(
            function ($value) use ($int): bool {
                return $value < $int;
            },
            'The value %value% must be less than %int%.',
            self::CAUSE_IS_LESS_THAN,
            [
                '%int%' => $int,
            ],
            $strict
        );

        return $this;
    }

    public function isLessOrEqualThan(int $int): self
    {
        $this->withCallback(
            function ($value) use ($int): bool {
                return $value <= $int;
            },
            'The value %value% must be less than or equal to %int%.',
            self::CAUSE_IS_LESS_THAN_OR_EQUAL,
            [
                '%int%' => $int,
            ],
            true
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isNoMySqlKeyword(): self
    {
        $this->withCallback(
            function ($value): bool {
                return !in_array(mb_strtoupper($value), MySQL55Keywords::KEYWORDS, true);
            },
            'This value must not be a MySQL reserved word.',
            self::CAUSE_IS_NO_MY_SQL_KEYWORD
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function isALanguageCode(): self
    {
        $this->withCallback(
            function ($value): bool {
                if (ShopwareVersion::isClassic()) {
                    $locale = Intl::getLocaleBundle()->getLocaleName($value);
                } else {
                    $locale = Languages::getName($value);
                }

                return (bool) $locale;
            },
            'This language code is not valid.',
            self::CAUSE_IS_A_LANGUAGE_CODE
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function startsWithAlphabeticCharacter(): self
    {
        $this->withCallback(
            function ($value): bool {
                $found = preg_match('/^([a-zA-Z])/', $value);

                return 1 === $found;
            },
            'The value %value% must start with a alphabetic character.',
            self::CAUSE_STARTS_WITH_ALPHABETIC_CHARACTER
        );

        return $this;
    }

    public function getValidator(ValidatorInterface $validator): Validator
    {
        $this->finishAssert();

        $ret = new Validator($validator);

        /*
         * @var mixed
         * @var Constraint $constraint
         */
        foreach ($this->assertions as $assertion) {
            $fieldName = $assertion['name'];
            $value = $assertion['value'];
            $constraints = $assertion['constraints'];

            $ret->addConstraint(
                $fieldName,
                $value,
                $constraints
            );
        }

        $this->assertions = [];

        return $ret;
    }

    /**
     * @internal
     */
    protected function addConstraint(Constraint $constraint): void
    {
        if (!$this->currentAssertion) {
            throw new LogicException('You must set a property before adding constraints');
        }

        $this->currentAssertion['constraints'][] = $constraint;
    }

    /**
     * @internal
     */
    protected function finishAssert(): void
    {
        if (!$this->currentAssertion) {
            return;
        }

        $this->assertions[] = $this->currentAssertion;
        $this->currentAssertion = null;
    }

    /**
     * @return $this
     */
    public function withCallback(callable $isValid, string $message, string $cause, array $parameters = [], bool $strict = false): self
    {
        $fieldName = $this->currentAssertion['name'];

        $this->addConstraint(new Callback(
            function ($value, ExecutionContextInterface $context) use ($isValid, $fieldName, $message, $cause, $parameters, $strict): void {
                if (!$strict && !$value) {
                    return;
                }

                if ($isValid($value)) {
                    return;
                }

                $constraintViolationBuilder = $context->buildViolation($message);
                $constraintViolationBuilder->setParameter('%value%', (string) $value);
                $constraintViolationBuilder->setCause($cause);

                foreach ($parameters as $key => $parameter) {
                    $constraintViolationBuilder->setParameter($key, (string) $parameter);
                }

                $constraintViolationBuilder->atPath($fieldName)
                    ->addViolation();
            }
        ));

        return $this;
    }
}
