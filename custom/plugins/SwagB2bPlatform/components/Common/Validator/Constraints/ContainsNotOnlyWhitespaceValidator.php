<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;
use function mb_strlen;
use function trim;

class ContainsNotOnlyWhitespaceValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value contains not only whitespace.
     *
     * @param mixed $value The value that should be validated
     */
    public function validate($value, Constraint $constraint, ExecutionContextInterface $context = null): void
    {
        if ($context) {
            $this->context = $context;
        }

        if (!$constraint instanceof ContainsNotOnlyWhitespace) {
            throw new UnexpectedTypeException($constraint, ContainsNotOnlyWhitespace::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            return;
        }

        if (mb_strlen(trim($value)) === 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
