<?php declare(strict_types=1);

namespace Shopware\B2B\Common\File;

use function fclose;
use function fopen;
use function fputcsv;

class CsvWriter
{
    public function write(array $data, string $fileName = 'php://output'): void
    {
        $file = fopen($fileName, 'w');

        foreach ($data as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }
}
