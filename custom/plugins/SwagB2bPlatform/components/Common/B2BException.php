<?php declare(strict_types=1);

namespace Shopware\B2B\Common;

use Throwable;

interface B2BException extends Throwable
{
}
