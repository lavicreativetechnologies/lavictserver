<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Routing;

interface RouteProvider
{
    public function getRoutes(): array;
}
