<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Routing;

use Exception;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\Components\Api\Exception\NotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function array_merge;
use function method_exists;
use function urldecode;

class RouteDispatchable implements Dispatchable
{
    /**
     * @var Route
     */
    private $route;

    /**
     * @var array
     */
    private $paramValues;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        ContainerInterface $container,
        Route $route,
        array $paramValues = []
    ) {
        $this->route = $route;
        $this->paramValues = $paramValues;
        $this->container = $container;
    }

    /**
     * @throws Exception
     */
    public function dispatch(
        Request $request
    ) {
        $callable = $this->createCallable();
        $params = $this->createParams([$request]);

        return call_user_func_array($callable, $params);
    }

    /**
     * @internal
     * @throws NotFoundException
     * @return array
     */
    protected function createCallable(): callable
    {
        $controller = $this->container->get($this->route->getController());

        $action = $this->route->getAction() . 'Action';

        if (!method_exists($controller, $action)) {
            throw new NotFoundException();
        }

        return [$controller, $action];
    }

    /**
     * @internal
     * @return array
     */
    protected function createParams(array $defaultParams = [])
    {
        $params = [];

        foreach ($this->route->getParamOrder() as $paramName) {
            if (!isset($this->paramValues[$paramName])) {
                $params[] = null;
                continue;
            }

            $params[] = urldecode($this->paramValues[$paramName]);
        }

        return array_merge($params, $defaultParams);
    }
}
