<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Controller;

use Exception;

class B2bControllerRedirectToCheckoutConfirm extends B2bControllerRedirectException implements B2bControllerRouteNameProvider
{
    public function __construct(array $params = [], $code = 0, Exception $previous = null)
    {
        parent::__construct('confirm', 'checkout', $params, $code, $previous);
    }

    public function getRouteName(): string
    {
        return 'frontend.checkout.confirm.page';
    }
}
