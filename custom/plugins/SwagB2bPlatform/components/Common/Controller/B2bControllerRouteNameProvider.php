<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Controller;

interface B2bControllerRouteNameProvider extends B2bControllerRoutingException
{
    public function getRouteName(): string;
}
