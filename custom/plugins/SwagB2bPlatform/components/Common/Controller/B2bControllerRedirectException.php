<?php declare(strict_types=1);

namespace Shopware\B2B\Common\Controller;

use BadMethodCallException;
use Exception;
use function print_r;
use function sprintf;

class B2bControllerRedirectException extends BadMethodCallException implements B2bControllerRoutingException
{
    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $controller;

    /**
     * @var array
     */
    private $params;

    /**
     * @param int $code
     */
    public function __construct(
        string $action,
        string $controller = null,
        array $params = [],
        $code = 0,
        Exception $previous = null
    ) {
        parent::__construct(
            sprintf('Requesting redirect to /%s/%s with %s', $controller, $action, print_r($params, true)),
            $code,
            $previous
        );
        $this->action = $action;
        $this->controller = $controller;
        $this->params = $params;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}
