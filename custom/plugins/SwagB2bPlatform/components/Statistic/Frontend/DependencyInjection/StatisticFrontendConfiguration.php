<?php declare(strict_types=1);

namespace Shopware\B2B\Statistic\Frontend\DependencyInjection;

use Shopware\B2B\Common\Controller\DependencyInjection\ControllerConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Statistic\Framework\DependencyInjection\StatisticFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class StatisticFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new StatisticFrameworkConfiguration(),
            new ControllerConfiguration(),
        ];
    }
}
