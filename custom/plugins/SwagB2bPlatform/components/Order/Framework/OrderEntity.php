<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class OrderEntity extends OrderContext
{
    /**
     * @var LineItemList
     */
    public $list;

    /**
     * @var Identity|null
     */
    public $clearedBy;
}
