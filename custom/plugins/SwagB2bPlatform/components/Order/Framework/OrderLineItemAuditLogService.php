<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogValueBasicEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemAddEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemCommentEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemQuantityEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueLineItemRemoveEntity;
use Shopware\B2B\AuditLog\Framework\RefusingToInsertDuplicatedLogEntryException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\ProductName\Framework\ProductNameService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class OrderLineItemAuditLogService
{
    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderAuditLogService
     */
    private $orderClearanceAuditLogService;

    /**
     * @var ProductNameService
     */
    private $productNameService;

    public function __construct(
        OrderContextRepository $orderContextRepository,
        AuthenticationService $authenticationService,
        OrderAuditLogService $orderClearanceAuditLogService,
        ProductNameService $productNameService
    ) {
        $this->orderContextRepository = $orderContextRepository;
        $this->authenticationService = $authenticationService;
        $this->orderClearanceAuditLogService = $orderClearanceAuditLogService;
        $this->productNameService = $productNameService;
    }

    public function createLineItemComment(IdValue $listId, LineItemReference $item, string $comment): void
    {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemCommentEntity();
        $auditLogValue->oldValue = $item->comment;
        $auditLogValue->newValue = $comment;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue);
    }

    public function createDeleteLineItem(IdValue $listId, LineItemReference $item): void
    {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemRemoveEntity();
        $auditLogValue->oldValue = $item->id;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue);
    }

    public function createAddLineItem(IdValue $listId, LineItemReference $item): void
    {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemAddEntity();
        $auditLogValue->newValue = (int) $item->quantity;
        $auditLogValue->oldValue = $item->comment;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue);
    }

    public function createLineItemChange(
        IdValue $listId,
        LineItemReference $item,
        int $newQuantity
    ): void {
        $productName = $this->getProductName($item);

        $auditLogValue = new AuditLogValueLineItemQuantityEntity();
        $auditLogValue->oldValue = (int) $item->quantity;
        $auditLogValue->newValue = (int) $newQuantity;
        $auditLogValue->orderNumber = $item->referenceNumber;
        $auditLogValue->productName = $productName;

        $this->createAuditLogEntry($listId, $auditLogValue);
    }

    /**
     * @internal
     */
    protected function getProductName(LineItemReference $item): string
    {
        $this->productNameService->translateProductNames([$item]);

        return (string) $item->name;
    }

    /**
     * @internal
     * @param $auditLogValue
     */
    protected function createAuditLogEntry(IdValue $listId, AuditLogValueBasicEntity $auditLogValue): void
    {
        $references = $this->orderContextRepository
            ->fetchAuditLogReferencesByListId($listId);

        $identity = $this->authenticationService
            ->getIdentity();

        try {
            $this->orderClearanceAuditLogService->addLogFromLineItemAuditLogService(
                $auditLogValue,
                $references,
                $identity
            );
        } catch (RefusingToInsertDuplicatedLogEntryException $e) {
            // ignore duplicate log entries
        }
    }
}
