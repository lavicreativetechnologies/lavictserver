<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\IdValue;

interface ShopOrderRepositoryInterface
{
    public function setOrderIdentity(string $orderNumber, IdValue $authId);

    public function setOrderToShopOwnerUser(string $orderNumber, IdValue $debtorId);

    public function fetchOneOrderContextByShopOrderId(IdValue $shopOrderId): OrderContext;

    /**
     * @throws ShopOrderStatusOutOfSyncException
     */
    public function testStatusInSync(IdValue $orderContextId, string $technicalStatusName);

    public function fetchOrderById(IdValue $orderId): array;

    public function setOrderCommentByOrderContextId(IdValue $orderContextId, string $comment);
}
