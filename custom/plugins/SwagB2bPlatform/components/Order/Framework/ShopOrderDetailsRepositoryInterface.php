<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface ShopOrderDetailsRepositoryInterface
{
    public function createListByOrderId(IdValue $orderId, OwnershipContext $ownershipContext): LineItemList;
}
