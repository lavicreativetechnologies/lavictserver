<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\IdValue;

/**
 * @todo split into Bridge and BridgePlatform
 */
class OrderCheckoutSource implements OrderSource
{
    /**
     * @var IdValue
     */
    public $billingAddressId;

    /**
     * @var IdValue
     */
    public $shippingAddressId;

    /**
     * @var IdValue
     */
    public $dispatchId;

    /**
     * @var string
     */
    public $sComment;

    /**
     * @var string
     */
    public $deviceType;

    /**
     * @var float
     */
    public $shippingAmount = 0.0;

    /**
     * @var float
     */
    public $shippingAmountNet = 0.0;

    /**
     * @var IdValue|null
     */
    public $paymentId;

    public function __construct(
        IdValue $billingAddressId,
        IdValue $shippingAddressId,
        IdValue $dispatchId,
        string $sComment,
        string $deviceType,
        float $shippingAmount = 0.0,
        float $shippingAmountNet = 0.0,
        IdValue $paymentId = null
    ) {
        if ($paymentId === null) {
            $paymentId = IdValue::null();
        }

        $this->billingAddressId = $billingAddressId;
        $this->shippingAddressId = $shippingAddressId;
        $this->dispatchId = $dispatchId;
        $this->sComment = $sComment;
        $this->deviceType = $deviceType;
        $this->shippingAmount = $shippingAmount;
        $this->shippingAmountNet = $shippingAmountNet;
        $this->paymentId = $paymentId;
    }
}
