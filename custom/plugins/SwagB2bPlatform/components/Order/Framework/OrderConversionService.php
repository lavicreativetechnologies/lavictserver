<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Address\Framework\AddressService;
use Shopware\B2B\Address\Framework\ShippingAddressRepositoryInterface;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Contact\Framework\ContactIdentityCreator;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OrderConversionService
{
    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var ShopOrderRepositoryInterface
     */
    private $shopOrderRepository;

    /**
     * @var ShopOrderDetailsRepositoryInterface
     */
    private $shopOrderDetailsRepository;

    /**
     * @var ContactIdentityCreator
     */
    private $identityCreator;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var ShippingAddressRepositoryInterface
     */
    private $shippingAddressRepository;

    /**
     * @var AddressService
     */
    private $addressService;

    public function __construct(
        OrderContextRepository $orderContextRepository,
        ShopOrderRepositoryInterface $shopOrderRepository,
        ShopOrderDetailsRepositoryInterface $shopOrderDetailsRepository,
        ContactIdentityCreator $identityCreator,
        AddressRepositoryInterface $addressRepository,
        ShippingAddressRepositoryInterface $shippingAddressRepository,
        AddressService $addressService
    ) {
        $this->orderContextRepository = $orderContextRepository;
        $this->shopOrderRepository = $shopOrderRepository;
        $this->shopOrderDetailsRepository = $shopOrderDetailsRepository;
        $this->identityCreator = $identityCreator;
        $this->addressRepository = $addressRepository;
        $this->shippingAddressRepository = $shippingAddressRepository;
        $this->addressService = $addressService;
    }

    public function convertOrderToB2bOrderContext(IdValue $shopOrderId, OwnershipContext $ownershipContext): OrderContext
    {
        $order = $this->shopOrderRepository->fetchOrderById($shopOrderId);

        $userId = IdValue::create($order['userID']);

        $identity = $this->identityCreator->getOrCreateIdentityByUserId($userId, $ownershipContext);

        $order['auth_id'] = $identity->getAuthId();
        $order['shipping_address_id'] = $identity->getMainShippingAddress()->id;
        $order['billing_address_id'] = $identity->getMainBillingAddress()->id;
        $order['shipping_id'] = $this->shippingAddressRepository->fetchShippingIdByUserId($userId);

        $orderContext = new OrderContext();
        $orderContext->convertFromShopOrderArray($order);

        $lineItemList = $this->shopOrderDetailsRepository->createListByOrderId($shopOrderId, $ownershipContext);

        $orderContext->listId = $lineItemList->id;

        $this->addressRepository->overwriteShopUserIdWithShopOwnerId($userId, $ownershipContext);

        $this->addressService->setAddressAsB2bType(
            $identity->getMainShippingAddress()->id,
            $identity->getMainBillingAddress()->id,
            $ownershipContext
        );

        $this->orderContextRepository
            ->addOrderContext($orderContext);

        return $orderContext;
    }
}
