<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use DomainException;
use Shopware\B2B\Common\B2BException;

class ShopOrderStatusOutOfSyncException extends DomainException implements B2BException
{
}
