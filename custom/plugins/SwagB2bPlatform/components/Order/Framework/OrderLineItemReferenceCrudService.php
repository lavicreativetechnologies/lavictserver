<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Common\Validator\Validator;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceValidationService;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function func_get_args;

class OrderLineItemReferenceCrudService extends AbstractCrudService
{
    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $lineItemReferenceRepository;

    /**
     * @var OrderLineItemAuditLogService
     */
    private $lineItemAuditLogService;

    /**
     * @var LineItemReferenceValidationService
     */
    private $validationService;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    public function __construct(
        LineItemListService $lineItemListService,
        LineItemReferenceValidationService $validationService,
        LineItemReferenceRepositoryInterface $lineItemReferenceRepository,
        OrderLineItemAuditLogService $lineItemAuditLogService,
        ProductProviderInterface $productProvider
    ) {
        $this->lineItemListService = $lineItemListService;
        $this->lineItemReferenceRepository = $lineItemReferenceRepository;
        $this->lineItemAuditLogService = $lineItemAuditLogService;
        $this->validationService = $validationService;
        $this->productProvider = $productProvider;
    }

    public function createCreateCrudRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest($data, [
            'referenceNumber',
            'quantity',
            'comment',
        ]);
    }

    public function createUpdateCrudRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest($data, [
            'id',
            'referenceNumber',
            'quantity',
            'comment',
        ]);
    }

    public function deleteLineItem(
        IdValue $listId,
        IdValue $lineItemId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): void {
        $item = $this->lineItemReferenceRepository
            ->fetchReferenceById($lineItemId, $currencyContext);

        $this->lineItemAuditLogService
            ->createDeleteLineItem($listId, $item);

        $this->lineItemReferenceRepository
            ->removeReference($lineItemId);

        $this->lineItemListService
            ->updateListPricesById($listId, $currencyContext, $ownershipContext);
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     */
    public function flipLineItemSorting(IdValue $lineItemIdOne, IdValue $lineItemIdTwo /**, CurrencyContext $currencyContext = null */): void
    {
        $currencyContext = func_get_args()[2] ?? null;

        $lineItemOne = $this->lineItemReferenceRepository
            ->fetchReferenceById($lineItemIdOne, $currencyContext);

        $lineItemTwo = $this->lineItemReferenceRepository
            ->fetchReferenceById($lineItemIdTwo, $currencyContext);

        $this->lineItemReferenceRepository
            ->flipSorting($lineItemOne, $lineItemTwo);
    }

    public function deleteLineItemFromOrder(
        IdValue $listId,
        IdValue $lineItemId,
        OrderContext $orderContext,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): void {
        if (!$orderContext->isEditable()) {
            throw new UnsupportedOrderStatusException('You should not be able to delete an item of an open Order');
        }

        $this->deleteLineItem($listId, $lineItemId, $currencyContext, $ownershipContext);
    }

    public function updateLineItem(
        IdValue $listId,
        CrudServiceRequest $request,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): LineItemReference {
        $currentItem = $this->lineItemReferenceRepository
            ->fetchReferenceById($request->requireIdValue('id'), $currencyContext);

        $updatedItem = clone $currentItem;
        $updatedItem->setData($request->getFilteredData());

        $validations = $this->validationService
            ->createUpdateValidation($updatedItem);

        $this->testValidation($updatedItem, $validations);

        $this->productProvider->updateReference($updatedItem);

        $this->lineItemReferenceRepository
            ->updateReference($listId, $updatedItem);

        $list = $this->lineItemListService
            ->updateListPricesById($listId, $currencyContext, $ownershipContext);

        $this->lineItemAuditLogService
            ->createLineItemChange($listId, $currentItem, $updatedItem->quantity);

        $this->lineItemAuditLogService
            ->createLineItemComment($listId, $currentItem, $updatedItem->comment);

        return $list->getReferenceById($updatedItem->id);
    }

    public function addLineItem(
        IdValue $listId,
        CrudServiceRequest $request,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): LineItemReference {
        $lineItemReference = new LineItemReference();
        $lineItemReference->setData($request->getFilteredData());
        $lineItemReference->mode = LineItemType::create(0);

        try {
            $validations = $this->trySilentUpdate($listId, $lineItemReference, $currencyContext);
        } catch (NotFoundException $exception) {
            $validations = $this->validationService
                ->createInsertValidation($lineItemReference, $listId);
        }
        $this->testValidation($lineItemReference, $validations);

        $this->productProvider->updateReference($lineItemReference);

        $list = new LineItemList();
        $list->id = $listId;
        $list->references = [$lineItemReference];

        $this->lineItemListService->updateListReferences($list, $currencyContext, $ownershipContext);

        $this->lineItemAuditLogService
            ->createAddLineItem($listId, $lineItemReference);

        return $list->getReferenceById($lineItemReference->id);
    }

    /**
     * @internal
     */
    protected function trySilentUpdate(IdValue $listId, LineItemReference $lineItemReference, CurrencyContext $currencyContext): Validator
    {
        $existingReference = $this->lineItemReferenceRepository
            ->getReferenceByReferenceNumberAndListId($lineItemReference->referenceNumber, $listId, $currencyContext);

        $lineItemReference->quantity += $existingReference->quantity;

        $validations = $this->validationService
            ->createInsertValidation($lineItemReference, $listId);

        $lineItemReference->quantity -= $existingReference->quantity;

        return $validations;
    }
}
