<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Api\DependencyInjection;

use Shopware\B2B\AuditLog\Framework\DependencyInjection\AuditLogFrameworkConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Currency\Framework\DependencyInjection\CurrencyFrameworkConfiguration;
use Shopware\B2B\Debtor\Framework\DependencyInjection\DebtorFrameworkConfiguration;
use Shopware\B2B\Order\Framework\DependencyInjection\OrderFrameworkConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderApiConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/api-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new DebtorFrameworkConfiguration(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
            new CurrencyFrameworkConfiguration(),
            new OrderFrameworkConfiguration(),
            new AuditLogFrameworkConfiguration(),
        ];
    }
}
