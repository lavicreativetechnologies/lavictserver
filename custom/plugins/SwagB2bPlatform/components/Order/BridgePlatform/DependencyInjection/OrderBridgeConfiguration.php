<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform\DependencyInjection;

use Shopware\B2B\Cart\Bridge\DependencyInjection\CartBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Repository\DependencyInjection\RepositoryConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getDependingConfigurations(): array
    {
        return [
            new RepositoryConfiguration(),
            CartBridgeConfiguration::create(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
        ];
    }
}
