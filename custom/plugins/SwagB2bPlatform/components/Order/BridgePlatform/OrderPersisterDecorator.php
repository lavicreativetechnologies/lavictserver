<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\Exception\CustomerNotLoggedInException;
use Shopware\Core\Checkout\Cart\Exception\InvalidCartException;
use Shopware\Core\Checkout\Cart\Order\OrderConversionContext;
use Shopware\Core\Checkout\Cart\Order\OrderConverter;
use Shopware\Core\Checkout\Cart\Order\OrderPersisterInterface;
use Shopware\Core\Checkout\Order\Exception\EmptyCartException;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use function array_merge;

class OrderPersisterDecorator implements OrderPersisterInterface
{
    private const CUSTOM_FIELDS_KEY = 'customFields';

    /**
     * @var OrderPersisterInterface
     */
    private $decorated;

    /**
     * @var EntityRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderConverter
     */
    private $orderConverter;

    public function __construct(
        OrderPersisterInterface $decorated,
        EntityRepositoryInterface $orderRepository,
        OrderConverter $converter
    ) {
        $this->decorated = $decorated;
        $this->orderRepository = $orderRepository;
        $this->orderConverter = $converter;
    }

    public function persist(Cart $cart, SalesChannelContext $context): string
    {
        /** @var AdditionalDataExtension $additionalData */
        $additionalData = $cart->getExtension(AdditionalDataExtension::NAME);

        if (!$additionalData) {
            return $this->decorated->persist($cart, $context);
        }

        $orderReference = $additionalData->getOrderReferenceNumber();
        $deliveryDate = $additionalData->getRequestedDeliveryDate();

        if (!$orderReference && !$deliveryDate) {
            return $this->decorated->persist($cart, $context);
        }

        $this->validateCartBeforePersist($cart, $context);

        $orderData = $this->orderConverter
            ->convertToOrder($cart, $context, new OrderConversionContext());

        $orderData[self::CUSTOM_FIELDS_KEY] = $this
            ->createCustomFields($orderData, (string) $orderReference, (string) $deliveryDate);

        $context->getContext()->scope(Context::SYSTEM_SCOPE, function (Context $context) use ($orderData): void {
            $this->orderRepository->create([$orderData], $context);
        });

        return $orderData['id'];
    }

    /**
     * @internal
     */
    protected function createCustomFields(array $orderData, string $orderReference, string $deliveryDate): array
    {
        $customFields = [
            OrderServiceDecorator::ORDER_REFERENCE_KEY => $orderReference,
            OrderServiceDecorator::REQUESTED_DELIVERY_DATE_KEY => $deliveryDate,
        ];

        if (isset($orderData[self::CUSTOM_FIELDS_KEY])) {
            $customFields = array_merge($customFields, $orderData[self::CUSTOM_FIELDS_KEY]);
        }

        return $customFields;
    }

    /**
     * @internal
     */
    protected function validateCartBeforePersist(Cart $cart, SalesChannelContext $context): void
    {
        if ($cart->getErrors()->blockOrder()) {
            throw new InvalidCartException($cart->getErrors());
        }

        if (!$context->getCustomer()) {
            throw new CustomerNotLoggedInException();
        }

        if ($cart->getLineItems()->count() <= 0) {
            throw new EmptyCartException();
        }
    }
}
