<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Checkout\Cart\Exception\OrderNotFoundException;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Routing\Exception\MissingRequestParameterException;
use Shopware\Core\Framework\Uuid\Exception\InvalidUuidException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\Checkout\Finish\CheckoutFinishPage;
use Shopware\Storefront\Page\Checkout\Finish\CheckoutFinishPageLoadedEvent;
use Shopware\Storefront\Page\Checkout\Finish\CheckoutFinishPageLoader;
use Shopware\Storefront\Page\GenericPageLoader;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class CheckoutFinishPageLoaderDecorator extends CheckoutFinishPageLoader
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var EntityRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var GenericPageLoader
     */
    private $genericLoader;

    /**
     * @var CheckoutFinishPageLoader
     */
    private $decoratedService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        CheckoutFinishPageLoader $decorated,
        EventDispatcherInterface $eventDispatcher,
        EntityRepositoryInterface $orderRepository,
        GenericPageLoader $genericLoader,
        AuthenticationService $authenticationService
    ) {
        $this->decoratedService = $decorated;
        $this->eventDispatcher = $eventDispatcher;
        $this->orderRepository = $orderRepository;
        $this->genericLoader = $genericLoader;
        $this->authenticationService = $authenticationService;
    }

    public function load(Request $request, SalesChannelContext $salesChannelContext): CheckoutFinishPage
    {
        if (!$this->authenticationService->isB2b() || $this->authenticationService->getIdentity()->isSuperAdmin()) {
            return $this->decoratedService->load($request, $salesChannelContext);
        }

        $page = $this->genericLoader->load($request, $salesChannelContext);

        $page = CheckoutFinishPage::createFrom($page);

        $orderId = IdValue::create($request->get('orderId'));

        $page->setOrder($this->getOrderOfDebtor($orderId, $this->authenticationService->getIdentity(), $salesChannelContext));

        $this->eventDispatcher->dispatch(
            new CheckoutFinishPageLoadedEvent($page, $salesChannelContext, $request)
        );

        return $page;
    }

    /**
     * @internal
     */
    protected function getOrderOfDebtor(IdValue $orderId, Identity $identity, SalesChannelContext $salesChannelContext): OrderEntity
    {
        if ($orderId instanceof NullIdValue) {
            throw new MissingRequestParameterException('orderId', '/orderId');
        }

        $criteria = (new Criteria([$orderId->getValue()]))
            ->addFilter(new EqualsFilter('order.orderCustomer.customerId', $identity->getOwnershipContext()->shopOwnerUserId->getValue()))
            ->addAssociation('lineItems.cover')
            ->addAssociation('transactions')
            ->addAssociation('deliveries');

        try {
            $searchResult = $this->orderRepository->search($criteria, $salesChannelContext->getContext());
        } catch (InvalidUuidException $e) {
            throw new OrderNotFoundException($orderId->getValue());
        }

        /** @var OrderEntity|null $order */
        $order = $searchResult->get($orderId->getValue());

        if (!$order) {
            throw new OrderNotFoundException($orderId->getValue());
        }

        return $order;
    }
}
