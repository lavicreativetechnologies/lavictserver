<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Order\Framework\OrderContext;
use Symfony\Contracts\EventDispatcher\Event;

class OrderContextStateChangedEvent extends Event
{
    /**
     * @var string
     */
    private $oldStatus;

    /**
     * @var string
     */
    private $newStatus;

    /**
     * @var OrderContext
     */
    private $orderContext;

    public function __construct(string $oldStatus, string $newStatus, OrderContext $orderContext)
    {
        $this->oldStatus = $oldStatus;
        $this->newStatus = $newStatus;
        $this->orderContext = $orderContext;
    }

    public function getOldStatus(): string
    {
        return $this->oldStatus;
    }

    public function getNewStatus(): string
    {
        return $this->newStatus;
    }

    public function getOrderContext(): OrderContext
    {
        return $this->orderContext;
    }
}
