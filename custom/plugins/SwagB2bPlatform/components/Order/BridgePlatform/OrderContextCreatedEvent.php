<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Order\Framework\OrderContext;
use Symfony\Contracts\EventDispatcher\Event;

class OrderContextCreatedEvent extends Event
{
    /**
     * @var OrderContext
     */
    private $orderContext;

    public function __construct(OrderContext $orderContext)
    {
        $this->orderContext = $orderContext;
    }

    public function getOrderContext(): OrderContext
    {
        return $this->orderContext;
    }
}
