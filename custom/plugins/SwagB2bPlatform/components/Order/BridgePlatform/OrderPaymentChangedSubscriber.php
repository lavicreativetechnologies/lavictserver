<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Storefront\Page\Checkout\Finish\CheckoutFinishPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderPaymentChangedSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        AuthenticationService $authenticationService,
        OrderContextRepository $orderContextRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->orderContextRepository = $orderContextRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CheckoutFinishPageLoadedEvent::class => 'onFinishPageLoaded',
        ];
    }

    public function onFinishPageLoaded(CheckoutFinishPageLoadedEvent $event): void
    {
        if (!$this->authenticationService->isB2b()) {
            return;
        }

        $page = $event->getPage();

        if (!$page->isChangedPayment()) {
            return;
        }

        $order = $page->getOrder();

        $newPaymentId = IdValue::create($order->getTransactions()->last()->getPaymentMethodId());

        $this->orderContextRepository->updatePaymentMethodByOrderNumber($order->getOrderNumber(), $newPaymentId);
    }
}
