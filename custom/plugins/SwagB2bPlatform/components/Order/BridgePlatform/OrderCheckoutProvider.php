<?php declare(strict_types=1);

namespace Shopware\B2B\Order\BridgePlatform;

use InvalidArgumentException;
use Shopware\B2B\Order\Framework\OrderCheckoutProviderInterface;
use Shopware\B2B\Order\Framework\OrderCheckoutSource;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderSource;
use Shopware\Core\Checkout\Order\OrderStates;

class OrderCheckoutProvider implements OrderCheckoutProviderInterface
{
    public function createOrder(OrderSource $source): OrderContext
    {
        $source = $this->testSource($source);

        $order = new OrderContext();
        $order->billingAddressId = $source->billingAddressId;
        $order->shippingAddressId = $source->shippingAddressId;
        $order->shippingId = $source->dispatchId;
        $order->shippingAmount = $source->shippingAmount;
        $order->shippingAmountNet = $source->shippingAmountNet;
        $order->comment = $source->sComment;
        $order->deviceType = $source->deviceType;
        $order->status = OrderStates::STATE_OPEN;
        $order->paymentId = $source->paymentId;

        return $order;
    }

    public function updateOrder(OrderSource $source, OrderContext $order): OrderContext
    {
        $source = $this->testSource($source);

        $order->billingAddressId = $source->billingAddressId;
        $order->shippingAddressId = $source->shippingAddressId;
        $order->shippingId = $source->dispatchId;
        $order->shippingAmount = $source->shippingAmount;
        $order->shippingAmountNet = $source->shippingAmountNet;
        $order->comment = $source->sComment;
        $order->deviceType = $source->deviceType;

        return $order;
    }

    /**
     * @throws InvalidArgumentException
     * @internal
     */
    protected function testSource(OrderSource $source): OrderCheckoutSource
    {
        if (!$source instanceof OrderCheckoutSource) {
            throw new InvalidArgumentException('Invalid source class provided');
        }

        return $source;
    }
}
