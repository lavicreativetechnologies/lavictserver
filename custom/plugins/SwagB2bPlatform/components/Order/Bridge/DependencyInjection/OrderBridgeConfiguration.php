<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Bridge\DependencyInjection;

use Shopware\B2B\Cart\Bridge\DependencyInjection\CartBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Repository\DependencyInjection\RepositoryConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Order\BridgePlatform\DependencyInjection\OrderBridgeConfiguration as PlatformOrderBridgeConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformOrderBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new RepositoryConfiguration(),
            CartBridgeConfiguration::create(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
        ];
    }
}
