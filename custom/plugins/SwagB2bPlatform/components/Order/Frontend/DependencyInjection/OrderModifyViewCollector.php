<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Frontend\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use function array_keys;

class OrderModifyViewCollector implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $loader = $container->findDefinition('b2b_order.modify_view_loader');
        $tags = $container->findTaggedServiceIds('b2b_order.view_modifier');
        $serviceIds = array_keys($tags);

        $extenders = [];
        foreach ($serviceIds as $serviceId) {
            $extenders[] = new Reference($serviceId);
        }

        $loader->replaceArgument(0, $extenders);
    }
}
