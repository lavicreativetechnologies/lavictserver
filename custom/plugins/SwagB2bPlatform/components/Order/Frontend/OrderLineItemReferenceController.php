<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Frontend;

use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceService;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderContextService;
use Shopware\B2B\Order\Framework\OrderLineItemReferenceCrudService;
use Shopware\B2B\Order\Framework\OrderRepository;
use Shopware\B2B\OrderList\Framework\OrderListRelationRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;
use function count;

class OrderLineItemReferenceController
{
    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $repository;

    /**
     * @var GridHelper
     */
    private $grid;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var OrderLineItemReferenceCrudService
     */
    private $lineItemReferenceCrudService;

    /**
     * @var OrderContextService
     */
    private $orderContextService;

    /**
     * @var OrderListRelationRepositoryInterface
     */
    private $orderListRelationRepository;

    /**
     * @var LineItemReferenceService
     */
    private $lineItemReferenceService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderModifyViewLoader
     */
    private $orderModifyViewLoader;

    public function __construct(
        LineItemReferenceRepositoryInterface $repository,
        GridHelper $grid,
        OrderRepository $orderRepository,
        AddressRepositoryInterface $addressRepository,
        OrderLineItemReferenceCrudService $lineItemReferenceCrudService,
        OrderContextService $orderContextService,
        OrderListRelationRepositoryInterface $orderListRelationRepository,
        LineItemReferenceService $lineItemReferenceService,
        CurrencyService $currencyService,
        AuthenticationService $authenticationService,
        OrderModifyViewLoader $orderModifyViewLoader
    ) {
        $this->repository = $repository;
        $this->grid = $grid;
        $this->orderRepository = $orderRepository;
        $this->addressRepository = $addressRepository;
        $this->lineItemReferenceCrudService = $lineItemReferenceCrudService;
        $this->orderContextService = $orderContextService;
        $this->orderListRelationRepository = $orderListRelationRepository;
        $this->lineItemReferenceService = $lineItemReferenceService;
        $this->currencyService = $currencyService;
        $this->authenticationService = $authenticationService;
        $this->orderModifyViewLoader = $orderModifyViewLoader;
    }

    public function masterDataAction(Request $request): array
    {
        $orderContextId = $request->requireIdValue('orderContextId');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authenticationService->getIdentity();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $identity->getOwnershipContext());

        $billingAddress = $this->addressRepository
            ->fetchOneByIdWithoutVisibility($order->billingAddressId, $identity, AddressEntity::TYPE_BILLING);

        $shippingAddress = $this->addressRepository
            ->fetchOneByIdWithoutVisibility($order->shippingAddressId, $identity, AddressEntity::TYPE_SHIPPING);

        return array_merge(
            $this->orderModifyViewLoader->modifyMasterDataView($order),
            $this->getHeaderData($order->list, $order),
            [
                'orderContext' => $order,
                'comment' => $order->comment,
                'requestedDeliveryDate' => $order->requestedDeliveryDate,
                'billingAddress' => $billingAddress,
                'shippingAddress' => $shippingAddress,
            ]
        );
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function updateMasterDataAction(Request $request): void
    {
        $request->checkPost();

        $orderContextId = $request->requireIdValue('orderContextId');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        if ($request->hasParam('comment')) {
            $this->orderContextService
                ->saveComment((string) $request->getParam('comment'), $order);
        }

        if ($request->hasParam('orderReference')) {
            $this->orderContextService
                ->saveOrderReference((string) $request->getParam('orderReference'), $order);
        }

        if ($request->hasParam('requestedDeliveryDate')) {
            $this->orderContextService
                ->saveRequestedDeliveryDate((string) $request->getParam('requestedDeliveryDate'), $order);
        }

        throw new B2bControllerForwardException('masterData', null, ['orderContextId' => $orderContextId->getValue()]);
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function updateOrderReferenceAction(Request $request): void
    {
        $request->checkPost();

        $orderContextId = $request->requireIdValue('orderContextId');
        $orderReference = (string) $request->requireParam('orderReference');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        $this->orderContextService
            ->saveOrderReference($orderReference, $order);

        throw new B2bControllerForwardException('masterData', null, ['orderContextId' => $orderContextId->getValue()]);
    }

    public function listAction(Request $request): array
    {
        $orderContextId = $request->requireIdValue('orderContextId');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        $searchStruct = new LineItemReferenceSearchStruct();

        $this->grid->extractSearchDataInStoreFront($request, $searchStruct);

        $searchStruct->offset = 0;
        $searchStruct->limit = PHP_INT_MAX;

        $items = $this->lineItemReferenceService
            ->fetchLineItemsReferencesWithProductNames(
                $order->listId,
                $searchStruct,
                $currencyContext
            );

        $items = $this->addOrderListToPositionsFromListIdAndNumber($order->listId, $items, $ownershipContext);

        $items = $this->lineItemReferenceService->getReferencesWithStock($items);

        $totalCount = $this->repository
            ->fetchTotalCount($order->listId, $searchStruct);

        $currentPage = $this->grid
            ->getCurrentPage($request);

        $maxPage = $this->grid
            ->getMaxPage($totalCount);

        $gridState = $this->grid
            ->getGridState($request, $searchStruct, $items, $currentPage, $maxPage);

        $validationResponse = $this->grid
            ->getValidationResponse('lineItemReference');

        return array_merge(
            $this->getHeaderData($order->list, $order),
            [
                'itemGrid' => $gridState,
                'listId' => $order->listId->getValue(),
                'orderContext' => $order,
            ],
            $validationResponse
        );
    }

    /**
     * @internal
     * @param LineItemReference[] $references
     * @return LineItemReference[]
     */
    protected function addOrderListToPositionsFromListIdAndNumber(
        IdValue $listId,
        array $references,
        OwnershipContext $ownershipContext
    ): array {
        foreach ($references as $reference) {
            try {
                $reference->orderList = $this->orderListRelationRepository
                    ->fetchOrderListNameForPositionNumber($listId, $reference->referenceNumber, $ownershipContext);
            } catch (NotFoundException $e) {
                // nth
            }
        }

        return $references;
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function saveCommentAction(Request $request): void
    {
        $request->checkPost();

        $comment = (string) $request->requireParam('comment');
        $orderContextId = $request->requireIdValue('orderContextId');
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        $this->orderContextService
            ->saveComment($comment, $order);

        throw new B2bControllerForwardException('masterData', null, ['orderContextId' => $orderContextId->getValue()]);
    }

    public function updateLineItemAction(Request $request): void
    {
        $request->checkPost();

        $orderContextId = $request->requireIdValue('orderContextId');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        $crudRequest = $this->lineItemReferenceCrudService
            ->createUpdateCrudRequest($request->getPost());

        try {
            $this->lineItemReferenceCrudService
                ->updateLineItem($order->listId, $crudRequest, $currencyContext, $ownershipContext);
        } catch (ValidationException $e) {
            $this->grid->pushValidationException($e);
        }

        throw new B2bControllerForwardException('list', null, ['orderContextId' => $orderContextId->getValue()]);
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function removeLineItemAction(Request $request): void
    {
        $request->checkPost();

        $lineItemId = $request->requireIdValue('lineItemId');
        $orderContextId = $request->requireIdValue('orderContextId');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $order = $this->orderRepository
            ->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        $this->lineItemReferenceCrudService
            ->deleteLineItemFromOrder($order->listId, $lineItemId, $order, $currencyContext, $ownershipContext);

        throw new B2bControllerForwardException('list', null, ['orderContextId' => $orderContextId->getValue()]);
    }

    public function newAction(Request $request): array
    {
        $validationResponse = $this->grid
            ->getValidationResponse('lineItemReference');

        $orderContextId = $request->requireIdValue('orderContextId');

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();
        $orderContext = $this->orderRepository->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        return array_merge(
            ['orderContextId' => $request->requireParam('orderContextId'), 'isEditable' => $orderContext->isEditable()],
            $validationResponse
        );
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function createAction(Request $request): void
    {
        $request->checkPost();

        $orderContextId = $request->requireIdValue('orderContextId');

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderContext = $this->orderRepository->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        $crudRequest = $this->lineItemReferenceCrudService
            ->createCreateCrudRequest($request->getPost());

        try {
            $this->lineItemReferenceCrudService
                ->addLineItem($orderContext->listId, $crudRequest, $currencyContext, $ownershipContext);
        } catch (ValidationException $e) {
            $this->grid->pushValidationException($e);
            throw new B2bControllerForwardException(
                'new',
                null,
                ['orderContextId' => $orderContextId->getValue()]
            );
        }

        throw new B2bControllerForwardException(
            'list',
            null,
            ['orderContextId' => $orderContextId->getValue()]
        );
    }

    /**
     * @internal
     */
    protected function getHeaderData(LineItemList $list, OrderContext $orderContext): array
    {
        return [
            'itemCount' => count($list->references),
            'amountNet' => $list->amountNet,
            'amount' => $list->amount,
            'createdAt' => $orderContext->createdAt,
            'orderNumber' => $orderContext->orderNumber,
            'state' => $orderContext->status,
        ];
    }
}
