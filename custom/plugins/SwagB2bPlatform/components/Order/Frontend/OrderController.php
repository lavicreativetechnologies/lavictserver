<?php declare(strict_types=1);

namespace Shopware\B2B\Order\Frontend;

use Shopware\B2B\AuditLog\Framework\AuditLogRepository;
use Shopware\B2B\AuditLog\Framework\AuditLogSearchStruct;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderOwnerService;
use Shopware\B2B\Order\Framework\OrderRepository;
use Shopware\B2B\Order\Framework\OrderSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class OrderController
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var GridHelper
     */
    private $orderGridHelper;

    /**
     * @var AuditLogRepository
     */
    private $auditLogRepository;

    /**
     * @var GridHelper
     */
    private $auditLogGridHelper;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OrderOwnerService
     */
    private $orderOwnerService;

    public function __construct(
        AuthenticationService $authenticationService,
        OrderRepository $orderRepository,
        GridHelper $orderGridHelper,
        AuditLogRepository $auditLogRepository,
        GridHelper $auditLogGridHelper,
        CurrencyService $currencyService,
        OrderOwnerService $orderOwnerService
    ) {
        $this->authenticationService = $authenticationService;
        $this->orderRepository = $orderRepository;
        $this->orderGridHelper = $orderGridHelper;
        $this->auditLogRepository = $auditLogRepository;
        $this->auditLogGridHelper = $auditLogGridHelper;
        $this->currencyService = $currencyService;
        $this->orderOwnerService = $orderOwnerService;
    }

    public function indexAction(): void
    {
        //nth
    }

    public function gridAction(Request $request): array
    {
        $orderCredentials = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $searchStruct = new OrderSearchStruct();

        $this->orderGridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        $order = $this->orderRepository
            ->fetchLists($orderCredentials, $searchStruct, $currencyContext);

        $order = $this->orderOwnerService->createOrderOwners($order);

        $totalCount = $this->orderRepository
            ->fetchTotalCount($orderCredentials, $searchStruct);

        $maxPage = $this->orderGridHelper
            ->getMaxPage($totalCount);

        $currentPage = (int) $request->getParam('page', 1);

        $orderGridState = $this->orderGridHelper
            ->getGridState($request, $searchStruct, $order, $maxPage, $currentPage);

        return [
            'orderGrid' => $orderGridState,
            'message' => $request->getParam('message'),
        ];
    }

    public function detailAction(Request $request): array
    {
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderContextId = $request->requireIdValue('orderContextId');

        $order = $this->orderRepository->fetchOrderById($orderContextId, $currencyContext, $ownershipContext);

        return [
            'list' => $order->list,
            'orderContext' => $order,
        ];
    }

    public function logAction(Request $request): array
    {
        $orderContextId = $request->requireIdValue('orderContextId');

        $searchStruct = new AuditLogSearchStruct();

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $this->orderGridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        $logItems = $this->auditLogRepository
            ->fetchList(OrderContextRepository::TABLE_NAME, $orderContextId, $searchStruct, $currencyContext);

        $totalCount = $this->auditLogRepository
            ->fetchTotalCount(OrderContextRepository::TABLE_NAME, $orderContextId, $searchStruct);

        $maxPage = $this->orderGridHelper
            ->getMaxPage($totalCount);

        $currentPage = (int) $request->getParam('page', 1);

        $orderGridState = $this->auditLogGridHelper
            ->getGridState($request, $searchStruct, $logItems, $maxPage, $currentPage);

        return [
            'gridState' => $orderGridState,
            'orderContextId' => $orderContextId->getValue(),
        ];
    }
}
