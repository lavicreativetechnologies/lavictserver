<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroupContact\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContingentGroupContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact/{email}/contingentgroup',
                'b2b_contingent_group_contact.api_contingent_group_contact_controller',
                'getList',
                ['email'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contact/{email}/contingentgroup',
                'b2b_contingent_group_contact.api_contingent_group_contact_controller',
                'create',
                ['email'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contact/{email}/contingentgroup',
                'b2b_contingent_group_contact.api_contingent_group_contact_controller',
                'remove',
                ['email'],
            ],
        ];
    }
}
