<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Frontend;

use Shopware\B2B\Acl\Framework\AclAccessExtensionService;
use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\AclRoute\Framework\AclRouteAssignmentService;
use Shopware\B2B\AclRoute\Framework\AclRouteRepository;
use Shopware\B2B\AclRoute\Framework\AclRouteService;
use Shopware\B2B\AclRoute\Frontend\AssignmentController;
use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class ContactRouteController extends AssignmentController
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        AclRouteRepository $aclRouteRepository,
        AclRepository $aclRouteAclRepository,
        AclAccessExtensionService $aclAccessExtensionService,
        AclRouteService $aclRouteService,
        ContactRepository $contactRepository,
        AclRouteAssignmentService $aclRouteAssignmentService,
        array $routeMapping
    ) {
        parent::__construct(
            $authenticationService,
            $aclRouteRepository,
            $aclRouteAclRepository,
            $aclAccessExtensionService,
            $aclRouteService,
            $aclRouteAssignmentService,
            $routeMapping
        );
        $this->contactRepository = $contactRepository;
    }

    protected function getContextEntity(IdValue $id): Entity
    {
        return $this->contactRepository
            ->fetchOneById($id, $this->getOwnershipContext());
    }

    protected function getContextParameterName(): string
    {
        return 'contactId';
    }
}
