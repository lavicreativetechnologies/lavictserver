<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Frontend;

use Shopware\B2B\Account\Framework\AccountImageServiceInterface;
use Shopware\B2B\Acl\Framework\AclGrantContextProviderChain;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\Controller\EmptyForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Company\Frontend\CompanyFilterResolver;
use Shopware\B2B\Contact\Framework\ContactCrudService;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Contact\Framework\ContactSearchStruct;
use Shopware\B2B\Shop\Framework\MessageFormatterInterface;
use Shopware\B2B\Shop\Framework\StorageInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AvatarRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_key_exists;
use function array_merge;
use function random_int;
use function sha1;

class ContactController
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var GridHelper
     */
    private $contactGridHelper;

    /**
     * @var ContactCrudService
     */
    private $contactCrudService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var CompanyFilterResolver
     */
    private $companyFilterResolver;

    /**
     * @var AclGrantContextProviderChain
     */
    private $grantContextProviderChain;

    /**
     * @var AccountImageServiceInterface
     */
    private $accountImageService;

    /**
     * @var StorageInterface
     */
    private $sessionStorage;

    /**
     * @var AvatarRepositoryInterface
     */
    private $avatarRepository;

    /**
     * @var MessageFormatterInterface
     */
    private $messageFormatter;

    public function __construct(
        AuthenticationService $authenticationService,
        ContactRepository $contactRepository,
        ContactCrudService $contactCrudService,
        GridHelper $contactGridHelper,
        CompanyFilterResolver $companyFilterResolver,
        AclGrantContextProviderChain $grantContextProviderChain,
        AccountImageServiceInterface $accountImageService,
        AvatarRepositoryInterface $avatarRepository,
        StorageInterface $sessionStorage,
        MessageFormatterInterface $messageFormatter
    ) {
        $this->authenticationService = $authenticationService;
        $this->contactRepository = $contactRepository;
        $this->contactCrudService = $contactCrudService;
        $this->contactGridHelper = $contactGridHelper;
        $this->companyFilterResolver = $companyFilterResolver;
        $this->grantContextProviderChain = $grantContextProviderChain;
        $this->sessionStorage = $sessionStorage;
        $this->accountImageService = $accountImageService;
        $this->avatarRepository = $avatarRepository;
        $this->messageFormatter = $messageFormatter;
    }

    public function indexAction(Request $request): array
    {
        $ownershipContext = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        $searchStruct = $this->createSearchStruct($request, $ownershipContext);

        $contacts = $this->contactRepository
            ->fetchList($ownershipContext, $searchStruct);

        $totalCount = $this->contactRepository
            ->fetchTotalCount($ownershipContext, $searchStruct);

        $maxPage = $this->contactGridHelper
            ->getMaxPage($totalCount);

        $currentPage = (int) $request->getParam('page', 1);

        $contactGridState = $this->contactGridHelper
            ->getGridState($request, $searchStruct, $contacts, $maxPage, $currentPage);

        return array_merge(
            [
                'gridState' => $contactGridState,
                'grantContext' => $searchStruct->aclGrantContext->getIdentifier(),
                'authId' => $ownershipContext->authId,
            ],
            $this->companyFilterResolver->getViewFilterVariables($searchStruct)
        );
    }

    public function detailAction(Request $request): array
    {
        $id = $request->requireIdValue('id');
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        return ['contact' => $this->contactRepository->fetchOneById($id, $ownershipContext)];
    }

    public function editAction(Request $request): array
    {
        $id = $request->requireIdValue('id');
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $validationResponse = $this->contactGridHelper->getValidationResponse('contact');

        $this->messageFormatter->formatSessionMessage('ChangeMessage');

        $message = $this->sessionStorage->get('ChangeMessage');

        $this->sessionStorage->remove('ChangeMessage');

        $contact = $this->contactRepository->fetchOneById($id, $ownershipContext);
        $contact->avatar = $this->avatarRepository->fetchAvatarByAuthId($contact->authId);

        return array_merge([
            'contact' => $contact,
            'changeMessage' => $message,
        ], $validationResponse);
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function updateAction(Request $request): void
    {
        $request->checkPost();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $post = $request->getPost();
        $post['id'] = IdValue::create($post['id']);
        $contact = $this->contactRepository->fetchOneById($post['id'], $ownershipContext);

        $post['encoder'] = $contact->encoder;
        $post['contextOwnerId'] = $contact->contextOwnerId;
        $post['password'] = $contact->password;

        $post = $this->extendRequestDataWithPasswordActivation($post, $request);
        $serviceRequest = $this->contactCrudService
            ->createExistingRecordRequest($post);

        try {
            $contact = $this->contactCrudService->update($serviceRequest, $ownershipContext);
        } catch (ValidationException $e) {
            $this->contactGridHelper->pushValidationException($e);
        }

        throw new B2bControllerForwardException('edit', null, ['id' => $contact->id]);
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function removeAction(Request $request): void
    {
        $request->checkPost();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $id = $request->requireIdValue('id');

        try {
            $this->contactCrudService->remove($id, $ownershipContext);
        } catch (ValidationException $e) {
            // nth
        }

        throw new EmptyForwardException();
    }

    public function newAction(Request $request): array
    {
        $validationResponse = $this->contactGridHelper->getValidationResponse('contact');

        $viewData = [
            'isNew' => true,
            'grantContext' => $request->requireParam('grantContext'),
        ];

        $viewData['createMultiple'] = (bool) $request->getParam('createMultiple');

        return array_merge($viewData, $validationResponse);
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function createAction(Request $request): void
    {
        $request->checkPost();

        $post = $request->getPost();

        $identity = $this->authenticationService->getIdentity();
        $post = $this->extendRequestDataFromIdentity($post, $identity);

        $post = $this->extendRequestDataWithPasswordActivation($post, $request);

        $serviceRequest = $this->contactCrudService->createNewRecordRequest($post);

        $grantContext = $this->grantContextProviderChain
            ->fetchOneByIdentifier($request->requireParam('grantContext'), $identity->getOwnershipContext());

        $viewData = [];
        $viewData['grantContext'] = $request->requireParam('grantContext');
        $viewData['createMultiple'] = array_key_exists('createAdditionalContact', $post);

        try {
            $contact = $this->contactCrudService->create($serviceRequest, $identity, $grantContext);

            if (isset($post['createAdditionalContact'])) {
                throw new B2bControllerForwardException('new', null, $viewData);
            }
        } catch (ValidationException $e) {
            $this->contactGridHelper->pushValidationException($e);
            throw new B2bControllerForwardException('new', null, $viewData);
        }

        throw new B2bControllerForwardException(
            'detail',
            null,
            ['id' => $contact->id->getValue()]
        );
    }

    public function processUploadAction(Request $request): void
    {
        $request->checkPost();

        $post = $request->getPost();
        $imageProfile = $request->requireFileParam('uploadedFile');

        $ownershipContext = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        $contact = $this->contactRepository
            ->fetchOneById(IdValue::create($post['id']), $ownershipContext);

        $imgData = $this->accountImageService
            ->uploadImage($contact->authId, $imageProfile);

        if ($imgData['success']) {
            $this->sessionStorage->set('ChangeMessage', ['success', 'ProfilePictureChanged']);
        } else {
            $this->sessionStorage->set('ChangeMessage', ['error', 'ProfilePictureNotChanged']);
        }

        throw new B2bControllerRedirectException('edit', null, ['id' => $contact->id->getValue()]);
    }

    /**
     * @internal
     */
    protected function extendRequestDataFromIdentity(array $post, Identity $identity): array
    {
        $post['contextOwnerId'] = $identity->getOwnershipContext()->contextOwnerId;
        $post['language'] = $identity->getPostalSettings()->language;

        return $post;
    }

    protected function extendRequestDataWithPasswordActivation(array $post, Request $request): array
    {
        if ($request->getParam('passwordActivation')) {
            $post['passwordNew'] = $post['passwordRepeat'] = sha1((string) random_int(0, PHP_INT_MAX));
        }

        return $post;
    }

    /**
     * @internal
     */
    protected function createSearchStruct(Request $request, OwnershipContext $ownershipContext): ContactSearchStruct
    {
        $searchStruct = new ContactSearchStruct();

        $this->companyFilterResolver
            ->extractGrantContextFromRequest($request, $searchStruct, $ownershipContext);

        $this->contactGridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        return $searchStruct;
    }
}
