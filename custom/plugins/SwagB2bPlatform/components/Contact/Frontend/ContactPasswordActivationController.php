<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Frontend;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Contact\Framework\ContactCrudService;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationEntity;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function array_merge;

class ContactPasswordActivationController
{
    /**
     * @var ContactPasswordActivationServiceInterface
     */
    private $activationService;

    /**
     * @var ContactCrudService
     */
    private $contactCrudService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    public function __construct(
        ContactPasswordActivationServiceInterface $activationService,
        ContactCrudService $contactCrudService,
        AuthenticationService $authenticationService,
        GridHelper $gridHelper
    ) {
        $this->activationService = $activationService;
        $this->contactCrudService = $contactCrudService;
        $this->authenticationService = $authenticationService;
        $this->gridHelper = $gridHelper;
    }

    public function indexAction(Request $request): array
    {
        $hash = $request->requireParam('hash');

        /** @var ContactPasswordActivationEntity $activation */
        $activation = $this->activationService->getValidActivationByHash($hash);

        if ($activation && $request->isPost()) {
            $ownershipContext = $this->authenticationService
                ->getIdentityByAuthId($activation->contact->authId)
                ->getOwnershipContext();

            $requestArray = $activation->contact->toArray();
            $requestArray['passwordNew'] = $request->getParam('passwordNew');
            $requestArray['passwordRepeat'] = $request->getParam('passwordRepeat');

            $crudRequest = $this->contactCrudService
                ->createExistingRecordRequest($requestArray);

            try {
                $this->contactCrudService->update($crudRequest, $ownershipContext);
                $this->activationService->removeActivation($activation);
            } catch (ValidationException $e) {
                $this->gridHelper->pushValidationException($e);
            }
        }

        return array_merge(
            [
                'activation' => $activation,
                'hash' => $hash,
            ],
            $this->getValidationResponse($activation)
        );
    }

    protected function getValidationResponse($activation): array
    {
        if (!$activation) {
            return ['errors' => [['snippetKey' => 'PasswordActivationHashIsInvalid']]];
        }

        if ($this->gridHelper->hasValidationException()) {
            return $this->gridHelper->getValidationResponse('contact');
        }

        return [];
    }
}
