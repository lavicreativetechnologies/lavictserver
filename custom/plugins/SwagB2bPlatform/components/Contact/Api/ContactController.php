<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Api;

use InvalidArgumentException;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Acl\Framework\AclGrantContextProviderChain;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Contact\Framework\ContactCrudService;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Contact\Framework\ContactSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function random_int;
use function sha1;

class ContactController
{
    /**
     * @var ContactCrudService
     */
    private $contactCrudService;

    /**
     * @var ContactRepository
     */
    private $contactSearchRepository;

    /**
     * @var GridHelper
     */
    private $contactGridHelper;

    /**
     * @var AclGrantContextProviderChain
     */
    private $grantContextProviderChain;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        ContactRepository $contactSearchRepository,
        ContactCrudService $contactCrudService,
        GridHelper $contactGridHelper,
        AclGrantContextProviderChain $grantContextProviderChain,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->contactCrudService = $contactCrudService;
        $this->contactSearchRepository = $contactSearchRepository;
        $this->contactGridHelper = $contactGridHelper;
        $this->grantContextProviderChain = $grantContextProviderChain;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function getListAction(Request $request): array
    {
        $search = new ContactSearchStruct();

        $ownershipContext = $this->getDebtorOwnershipContext();

        $this->contactGridHelper
            ->extractSearchDataInRestApi($request, $search);

        $contacts = $this->contactSearchRepository
            ->fetchList($ownershipContext, $search);

        $totalCount = $this->contactSearchRepository
            ->fetchTotalCount($ownershipContext, $search);

        return ['success' => true, 'contacts' => $contacts, 'totalCount' => $totalCount];
    }

    public function getAction(string $contactId): array
    {
        $contactId = IdValue::create($contactId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $contact = $this->contactSearchRepository
            ->fetchOneById($contactId, $ownershipContext);

        return ['success' => true, 'contact' => $contact];
    }

    public function createAction(Request $request): array
    {
        $debtorIdentity = $this->authStorageAdapter->getIdentity();

        $post = $request->getPost();
        $post['passwordNew'] = $request->getParam('password');
        $post['passwordRepeat'] = $request->getParam('password');

        $aclGrantContext = $this->extractGrantContext($request, $debtorIdentity->getOwnershipContext());

        $post['contextOwnerId'] = $debtorIdentity->getOwnershipContext()->contextOwnerId;

        $post = $this->extendRequestDataWithPasswordActivation($post, $request);

        $serviceRequest = $this->contactCrudService
            ->createNewRecordRequest($post);

        $contact = $this->contactCrudService
            ->create($serviceRequest, $debtorIdentity, $aclGrantContext);

        return ['success' => true, 'contact' => $contact];
    }

    public function updateAction(string $contactId, Request $request): array
    {
        $contactId = IdValue::create($contactId);
        $post = $request->getPost();
        $post['id'] = $contactId;

        $post = $this->extendRequestDataWithPasswordActivation($post, $request);

        if ($request->getParam('password')) {
            $post['passwordNew'] = $request->getParam('password');
            $post['passwordRepeat'] = $request->getParam('password');
        }

        $debtorIdentity = $this->authStorageAdapter->getIdentity();
        $post['contextOwnerId'] = $debtorIdentity->getOwnershipContext()->contextOwnerId;

        $serviceRequest = $this->contactCrudService
            ->createExistingRecordRequest($post);

        $contact = $this->contactCrudService
            ->update($serviceRequest, $debtorIdentity->getOwnershipContext());

        return ['success' => true, 'contact' => $contact];
    }

    public function removeAction(string $contactId): array
    {
        $contactId = IdValue::create($contactId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $contact = $this->contactCrudService->remove($contactId, $ownershipContext);

        return ['success' => true, 'contact' => $contact];
    }

    /**
     * @internal
     */
    protected function extendRequestDataWithPasswordActivation(array $post, Request $request): array
    {
        if ($request->getParam('passwordActivation')) {
            $post['passwordNew'] = $post['passwordRepeat'] = sha1((string) random_int(0, PHP_INT_MAX));
        }

        return $post;
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function extractGrantContext(Request $request, OwnershipContext $ownershipContext): AclGrantContext
    {
        $grantContextIdentifier = $request->requireParam('grantContextIdentifier');

        return $this->grantContextProviderChain->fetchOneByIdentifier($grantContextIdentifier, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
