<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact',
                'b2b_debtor.api_contact_controller',
                'getList',
                [],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contact',
                'b2b_debtor.api_contact_controller',
                'create',
                [],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contact/{contactIdentifier}',
                'b2b_debtor.api_contact_controller',
                'update',
                ['contactIdentifier'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contact/{contactIdentifier}',
                'b2b_debtor.api_contact_controller',
                'remove',
                ['contactIdentifier'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contact/{contactIdentifier}',
                'b2b_debtor.api_contact_controller',
                'get',
                ['contactIdentifier'],
            ],
        ];
    }
}
