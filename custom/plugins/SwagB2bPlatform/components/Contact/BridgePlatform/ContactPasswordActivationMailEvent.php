<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\BridgePlatform;

use Shopware\B2B\Contact\Framework\ContactPasswordActivationEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\BusinessEventInterface;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;
use Shopware\Core\Framework\Event\MailActionInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\EventDispatcher\Event;

class ContactPasswordActivationMailEvent extends Event implements BusinessEventInterface, MailActionInterface
{
    public const EVENT_NAME = 'b2b.contact.password.activation.mail';

    /**
     * @var SalesChannelContext
     */
    private $salesChannelContext;

    /**
     * @var ContactPasswordActivationEntityMailData
     */
    private $passwordActivation;

    /**
     * @var string
     */
    private $url;

    public function __construct(
        SalesChannelContext $salesChannelContext,
        ContactPasswordActivationEntity $passwordActivationEntity,
        string $url
    ) {
        $this->salesChannelContext = $salesChannelContext;
        $this->passwordActivation = new ContactPasswordActivationEntityMailData($passwordActivationEntity);
        $this->url = $url;
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('passwordActivation', ContactPasswordActivationEntityMailData::getPasswordActivationDataType())
            ->add('url', new ScalarValueType(ScalarValueType::TYPE_STRING));
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        $contact = $this->passwordActivation->getContact();

        return new MailRecipientStruct([
            $contact->getEmail() => $contact->getFirstName() . ' ' . $contact->getLastName(),
        ]);
    }

    public function getSalesChannelId(): ?string
    {
        return $this->salesChannelContext->getSalesChannel()->getId();
    }

    public function getContext(): Context
    {
        return $this->salesChannelContext->getContext();
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getPasswordActivation(): ContactPasswordActivationEntityMailData
    {
        return $this->passwordActivation;
    }
}
