<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\BridgePlatform;

use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactPasswordProviderInterface;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\FieldSerializer\PasswordFieldSerializer;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\WriteCommandQueue;
use Shopware\Core\Framework\DataAbstractionLayer\Write\DataStack\KeyValuePair;
use Shopware\Core\Framework\DataAbstractionLayer\Write\EntityExistence;
use Shopware\Core\Framework\DataAbstractionLayer\Write\WriteContext;
use Shopware\Core\Framework\DataAbstractionLayer\Write\WriteParameterBag;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use function iterator_to_array;

class ContactPasswordProvider implements ContactPasswordProviderInterface
{
    public const ENCODER_NAME = 'platform';

    /**
     * @var AccountService
     */
    private $customerRepository;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /**
     * @var PasswordFieldSerializer
     */
    private $passwordFieldSerializer;

    public function __construct(
        EntityRepositoryInterface $customerRepository,
        SystemConfigService $systemConfigService,
        PasswordFieldSerializer $passwordFieldSerializer
    ) {
        $this->customerRepository = $customerRepository;
        $this->systemConfigService = $systemConfigService;
        $this->passwordFieldSerializer = $passwordFieldSerializer;
    }

    public function setPassword(ContactEntity $contact, $newPassword): void
    {
        $this->setEncoder($contact);

        $mappingResult = iterator_to_array($this->passwordFieldSerializer->encode(
            $this->customerRepository->getDefinition()->getFields()->get('password'),
            $this->createFakeEntityExistence(),
            new KeyValuePair('password', $newPassword, true),
            $this->createFakeWriteParameterBag()
        ));

        $contact->password = $mappingResult['password'];
    }

    public function setEncoder(ContactEntity $contact): void
    {
        $contact->encoder = self::ENCODER_NAME;
    }

    public function getMinPasswordLength(): int
    {
        return (int) $this->systemConfigService
            ->get('core.loginRegistration.passwordMinLength');
    }

    public function resetFailedLoginsAndLockedUntil(ContactEntity $contact): void
    {
        // not a platform feature
    }

    /**
     * @internal
     */
    protected function createFakeEntityExistence(): EntityExistence
    {
        return new EntityExistence(null, [], true, false, false, []);
    }

    /**
     * @internal
     */
    protected function createFakeWriteParameterBag(): WriteParameterBag
    {
        return new WriteParameterBag(
            $this->customerRepository->getDefinition(),
            WriteContext::createFromContext(Context::createDefaultContext()),
            '',
            new WriteCommandQueue()
        );
    }
}
