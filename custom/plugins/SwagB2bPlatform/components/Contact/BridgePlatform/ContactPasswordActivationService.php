<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\BridgePlatform;

use DateInterval;
use DateTime;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationEntity;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationRepository;
use Shopware\B2B\Contact\Framework\ContactPasswordActivationServiceInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ContactPasswordActivationService implements ContactPasswordActivationServiceInterface
{
    /**
     * @var ContactPasswordActivationRepository
     */
    private $contactPasswordActivationRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        ContactPasswordActivationRepository $contactPasswordActivationRepository,
        EventDispatcherInterface $eventDispatcher,
        RouterInterface $router,
        ContextProvider $contextProvider
    ) {
        $this->contactPasswordActivationRepository = $contactPasswordActivationRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->router = $router;
        $this->contextProvider = $contextProvider;
    }

    public function sendPasswordActivationEmail(ContactEntity $contact): void
    {
        $passwordActivationEntity = $this->createPasswordActivationEntity($contact);
        $url = $this->router->generate(
            'frontend.b2b.b2bcontactpasswordactivation.index',
            ['hash' => $passwordActivationEntity->hash],
            RouterInterface::ABSOLUTE_URL
        );

        $this->eventDispatcher->dispatch(
            new ContactPasswordActivationMailEvent(
                $this->contextProvider->getSalesChannelContext(),
                $passwordActivationEntity,
                $url
            )
        );
    }

    public function getValidActivationByHash(string $hash)
    {
        try {
            $activation = $this->contactPasswordActivationRepository->fetchOneByHash($hash);
        } catch (NotFoundException $e) {
            return null;
        }

        if (!$activation->isValid()) {
            return null;
        }

        return $activation;
    }

    public function removeActivation(ContactPasswordActivationEntity $activation): ContactPasswordActivationEntity
    {
        return $this->contactPasswordActivationRepository->removeActivation($activation);
    }

    public function cleanActivations(): void
    {
        $this->contactPasswordActivationRepository->cleanActivations();
    }

    protected function createPasswordActivationEntity(ContactEntity $contact): ContactPasswordActivationEntity
    {
        $activationEntity = new ContactPasswordActivationEntity();
        $activationEntity->hash = Uuid::randomHex();
        $activationEntity->validUntil = new DateTime();
        $activationEntity->validUntil->add(new DateInterval('PT' . self::EXPIRATION_TIME));
        $activationEntity->contactId = $contact->id;
        $activationEntity->contact = $contact;

        return $this->contactPasswordActivationRepository
            ->addContactActivation($activationEntity);
    }
}
