<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\NotFoundException;
use function sprintf;

class ContactPasswordActivationRepository
{
    const TABLE_NAME = 'b2b_contact_password_activation';

    const TABLE_ALIAS = 'password_activation';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(Connection $connection, ContactRepository $contactRepository)
    {
        $this->connection = $connection;
        $this->contactRepository = $contactRepository;
    }

    public function addContactActivation(ContactPasswordActivationEntity $activation): ContactPasswordActivationEntity
    {
        if (!$activation->isNew()) {
            throw new CanNotInsertExistingRecordException('The contact activation provided already exists');
        }

        $this->connection->insert(
            self::TABLE_NAME,
            $activation->toDatabaseArray()
        );

        $activation->id = IdValue::create($this->connection->lastInsertId());

        return $activation;
    }

    public function fetchOneByHash(string $hash): ContactPasswordActivationEntity
    {
        $statement = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.hash = :hash')
            ->setParameter('hash', $hash)
            ->execute();

        $activationData = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$activationData) {
            throw new NotFoundException(sprintf('Activation not found for %s', $hash));
        }

        $activationEntity = new ContactPasswordActivationEntity();
        $activationEntity = $activationEntity->fromDatabaseArray($activationData);
        $activationEntity->contact = $this->contactRepository->insecureFetchOneById($activationEntity->contactId);

        return $activationEntity;
    }

    public function removeActivation(ContactPasswordActivationEntity $activation): ContactPasswordActivationEntity
    {
        if ($activation->isNew()) {
            throw new CanNotRemoveExistingRecordException('The activation provided does not exist');
        }

        $this->connection->delete(
            self::TABLE_NAME,
            ['hash' => $activation->hash]
        );

        $activation->id = IdValue::null();

        return $activation;
    }

    public function cleanActivations(): void
    {
        $this->connection->transactional(function (): void {
            $statement = $this->connection->createQueryBuilder()
                ->delete('b2b_contact_password_activation')
                ->where('valid_until < NOW()');
            $statement->execute();
        });
    }
}
