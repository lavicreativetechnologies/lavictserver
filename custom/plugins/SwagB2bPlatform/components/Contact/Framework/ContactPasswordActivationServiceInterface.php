<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Common\Service\MailException;

interface ContactPasswordActivationServiceInterface
{
    public const EXPIRATION_TIME = '24H';

    /**
     * @throws MailException
     */
    public function sendPasswordActivationEmail(ContactEntity $contact);

    /**
     * @return void|ContactPasswordActivationEntity
     */
    public function getValidActivationByHash(string $hash);

    public function removeActivation(
        ContactPasswordActivationEntity $activationEntity
    ): ContactPasswordActivationEntity;

    public function cleanActivations();
}
