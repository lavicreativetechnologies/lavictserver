<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

interface ContactPasswordProviderInterface
{
    /**
     * @param $newPassword
     */
    public function setPassword(ContactEntity $contact, $newPassword);

    public function setEncoder(ContactEntity $contact);

    public function getMinPasswordLength(): int;

    public function resetFailedLoginsAndLockedUntil(ContactEntity $contact);
}
