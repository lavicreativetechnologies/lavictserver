<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\EntityCreatorInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class ContactIdentityCreator
{
    /**
     * @var AuthenticationIdentityLoaderInterface
     */
    private $authenticationIdentityLoader;

    /**
     * @var EntityCreatorInterface
     */
    private $entityCreator;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    public function __construct(
        AuthenticationIdentityLoaderInterface $authenticationIdentityLoader,
        EntityCreatorInterface $entityCreator,
        LoginContextService $loginContextService,
        CredentialsBuilderInterface $credentialsBuilder
    ) {
        $this->authenticationIdentityLoader = $authenticationIdentityLoader;
        $this->entityCreator = $entityCreator;
        $this->loginContextService = $loginContextService;
        $this->credentialsBuilder = $credentialsBuilder;
    }

    public function getOrCreateIdentityByUserId(IdValue $id, OwnershipContext $ownershipContext): Identity
    {
        try {
            return $this->getIdentityByUserId($id);
        } catch (NotFoundException $e) {
            $this->entityCreator->createEntityByUserId($id, $ownershipContext);

            return $this->getIdentityByUserId($id);
        }
    }

    /**
     * @internal
     */
    protected function getIdentityByUserId(IdValue $userId): Identity
    {
        $credentials = $this->credentialsBuilder->createCredentialsByUserId($userId);

        return $this->authenticationIdentityLoader->fetchIdentityByCredentials($credentials, $this->loginContextService);
    }
}
