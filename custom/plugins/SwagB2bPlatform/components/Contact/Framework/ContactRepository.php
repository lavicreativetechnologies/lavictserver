<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use InvalidArgumentException;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Company\Framework\CompanyFilterHelper;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\QuerySubShopExtenderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;
use function array_merge;
use function sprintf;

class ContactRepository implements GridRepository
{
    const TABLE_NAME = 'b2b_debtor_contact';
    const TABLE_ALIAS = 'contact';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DebtorRepositoryInterface
     */
    private $debtorRepository;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var StoreFrontAuthenticationRepository
     */
    private $authenticationRepository;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    /**
     * @var CompanyFilterHelper
     */
    private $companyFilterHelper;

    /**
     * @var ContactCompanyAssignmentFilter
     */
    private $contactCompanyAssignmentFilter;

    /**
     * @var ContactCompanyInheritanceFilter
     */
    private $contactCompanyInheritanceFilter;

    /**
     * @var QuerySubShopExtenderInterface
     */
    private $querySubShopExtender;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        DebtorRepositoryInterface $debtorRepository,
        StoreFrontAuthenticationRepository $authenticationRepository,
        AclReadHelper $aclReadHelper,
        ContactCompanyAssignmentFilter $contactCompanyAssignmentFilter,
        CompanyFilterHelper $companyFilterHelper,
        ContactCompanyInheritanceFilter $contactCompanyInheritanceFilter,
        QuerySubShopExtenderInterface $querySubShopExtender
    ) {
        $this->connection = $connection;
        $this->debtorRepository = $debtorRepository;
        $this->dbalHelper = $dbalHelper;
        $this->authenticationRepository = $authenticationRepository;
        $this->aclReadHelper = $aclReadHelper;
        $this->companyFilterHelper = $companyFilterHelper;
        $this->contactCompanyAssignmentFilter = $contactCompanyAssignmentFilter;
        $this->contactCompanyInheritanceFilter = $contactCompanyInheritanceFilter;
        $this->querySubShopExtender = $querySubShopExtender;
    }

    public function fetchOneByEmail(string $email, OwnershipContext $ownershipContext): ContactEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $email);

        $this->filterByContextOwner($ownershipContext, $query);

        $contactData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        return $this->createContactByContactData($contactData, $email);
    }

    public function insecureFetchOneByEmail(string $email): ContactEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $email);

        $contactData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        return $this->createContactByContactData($contactData, $email);
    }

    public function fetchOneByCredentials(AbstractCredentialsEntity $credentialsEntity): ContactEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $credentialsEntity->email)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_store_front_auth', 'auth', self::TABLE_ALIAS . '.auth_id = auth.id');

        $this->querySubShopExtender->extendQuery($query, $credentialsEntity, 'auth');

        $contactData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$contactData) {
            throw new NotFoundException(sprintf('Contact not found for %s', $credentialsEntity->email));
        }

        return $this->createContactByContactData($contactData, $credentialsEntity->email);
    }

    public function hasContactForEmail(string $email): bool
    {
        $statement = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $email)
            ->execute();

        return (bool) $statement->fetchColumn();
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function fetchOneById(IdValue $id, OwnershipContext $ownershipContext): ContactEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue());

        $this->filterByContextOwner($ownershipContext, $query);

        $contactData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        return $this->createContactByContactData($contactData, (string) $id->getValue());
    }

    public function fetchOneByAuthId(IdValue $id, OwnershipContext $ownershipContext): ContactEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.auth_id = :id')
            ->setParameter('id', $id);

        $this->filterByContextOwner($ownershipContext, $query);

        $contactData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        return $this->createContactByContactData($contactData, (string) $id->getValue());
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function insecureFetchOneById(IdValue $id): ContactEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue());

        $contactData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        return $this->createContactByContactData($contactData, (string) $id->getValue());
    }

    /**
     * @internal
     * @param $contactData
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    protected function createContactByContactData($contactData, string $identifier): ContactEntity
    {
        if (!$contactData) {
            throw new NotFoundException(sprintf('Contact not found for %s', $identifier));
        }

        $contact = new ContactEntity();
        $contact->fromDatabaseArray($contactData);

        $authentication = $this->authenticationRepository
            ->fetchAuthenticationById($contact->contextOwnerId);

        $contact->debtor = $this->debtorRepository->fetchOneById($authentication->providerContext);

        return $contact;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException
     */
    public function addContact(ContactEntity $contact, OwnershipContext $ownershipContext): ContactEntity
    {
        if (!$contact->isNew()) {
            throw new CanNotInsertExistingRecordException('The contact provided already exists');
        }

        $this->connection->insert(
            self::TABLE_NAME,
            array_merge(
                $contact->toDatabaseArray(),
                ['context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue()]
            )
        );

        $contact->id = (int) $this->connection->lastInsertId();

        return $contact;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException
     */
    public function updateContact(ContactEntity $contact, OwnershipContext $ownershipContext): ContactEntity
    {
        if ($contact->isNew()) {
            throw new CanNotUpdateExistingRecordException('The contact provided does not exist');
        }

        $this->connection->update(
            self::TABLE_NAME,
            $contact->toDatabaseArray(),
            [
                'id' => $contact->id,
                'context_owner_id' => $ownershipContext->contextOwnerId,
            ]
        );

        return $contact;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException
     */
    public function removeContact(ContactEntity $contact, OwnershipContext $ownershipContext): ContactEntity
    {
        if ($contact->isNew()) {
            throw new CanNotRemoveExistingRecordException('The contact provided does not exist');
        }

        $this->connection->delete(
            self::TABLE_NAME,
            [
                'id' => $contact->id->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );

        $contact->id = IdValue::null();

        return $contact;
    }

    public function updateDefaultAddresses(ContactEntity $contact, OwnershipContext $ownershipContext): void
    {
        $addresses = [];

        if (!$contact->defaultBillingAddressId instanceof NullIdValue) {
            $addresses['default_billing_address_id'] = $contact->defaultBillingAddressId->getStorageValue();
        }

        if (!$contact->defaultShippingAddressId instanceof NullIdValue) {
            $addresses['default_shipping_address_id'] = $contact->defaultShippingAddressId->getStorageValue();
        }

        if (!$addresses) {
            throw new InvalidArgumentException('no addresses given');
        }

        $this->connection->update(
            self::TABLE_NAME,
            $addresses,
            [
                'id' => $contact->id->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );
    }

    /**
     * @return ContactEntity[]
     */
    public function fetchList(OwnershipContext $ownershipContext, ContactSearchStruct $searchStruct): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS);

        $this->filterByContextOwner($ownershipContext, $query);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);
        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $this->companyFilterHelper
            ->applyFilter(
                $this->aclReadHelper,
                $this->contactCompanyAssignmentFilter,
                $searchStruct,
                $query,
                $this->contactCompanyInheritanceFilter
            );

        $statement = $query->execute();
        $contactsData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $contacts = [];
        foreach ($contactsData as $contactData) {
            $contacts[] = (new ContactEntity())->fromDatabaseArray($contactData);
        }

        return $contacts;
    }

    /**
     * @return ContactEntity[]
     */
    public function fetchFullList(OwnershipContext $ownershipContext): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->orderBy(self::TABLE_ALIAS . '.lastname', 'ASC');

        $this->filterByContextOwner($ownershipContext, $query);

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $statement = $query->execute();
        $contactsData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $contacts = [];
        foreach ($contactsData as $contactData) {
            $contacts[] = (new ContactEntity())->fromDatabaseArray($contactData);
        }

        return $contacts;
    }

    /**
     * @return string[]
     */
    public function fetchEmailAddresses(OwnershipContext $ownershipContext): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.email')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS);

        $this->filterByContextOwner($ownershipContext, $query);

        $statement = $query->execute();

        return $statement->fetchAll(PDO::FETCH_COLUMN);
    }

    public function fetchTotalCount(OwnershipContext $context, ContactSearchStruct $contactSearchStruct): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS);

        $this->filterByContextOwner($context, $query);

        $this->aclReadHelper->applyAclVisibility($context, $query);

        $this->companyFilterHelper
            ->applyFilter(
                $this->aclReadHelper,
                $this->contactCompanyAssignmentFilter,
                $contactSearchStruct,
                $query,
                $this->contactCompanyInheritanceFilter
            );

        $this->dbalHelper->applyFilters($contactSearchStruct, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    /**
     * @internal
     */
    protected function filterByContextOwner(OwnershipContext $context, QueryBuilder $query): void
    {
        $query->andWhere(self::TABLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $context->contextOwnerId);
    }

    public function setAuthId(IdValue $contactId, IdValue $authId, OwnershipContext $ownershipContext): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            ['auth_id' => $authId->getStorageValue()],
            [
                'id' => $contactId->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );
    }

    /**
     * @return string query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'email',
            'title',
            'salutation',
            'firstname',
            'lastname',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }
}
