<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractNamedUser;
use function get_object_vars;
use function property_exists;

class ContactEntity extends AbstractNamedUser implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $encoder;

    /**
     * @var string
     */
    public $email;

    /**
     * @var bool
     */
    public $active;

    /**
     * @var IdValue
     */
    public $language;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $salutation;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $department;

    /**
     * @var IdValue
     */
    public $contextOwnerId;

    /**
     * @var DebtorEntity|null
     */
    public $debtor;

    /**
     * Read Only
     *
     * @var IdValue
     */
    public $authId;

    /**
     * read only
     * @var IdValue
     */
    public $defaultBillingAddressId;

    /**
     * read only
     * @var IdValue
     */
    public $defaultShippingAddressId;

    /**
     * @var string
     */
    public $avatar;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->contextOwnerId = IdValue::null();
        $this->language = IdValue::null();
        $this->authId = IdValue::null();
        $this->defaultBillingAddressId = IdValue::null();
        $this->defaultShippingAddressId = IdValue::null();
    }

    /**
     * {@inheritdoc}
     */
    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'password' => $this->password,
            'encoder' => $this->encoder,
            'email' => $this->email,
            'active' => $this->active ? 1 : 0,
            'language' => $this->language->getStorageValue(),
            'title' => $this->title,
            'salutation' => $this->salutation,
            'firstname' => $this->firstName,
            'lastname' => $this->lastName,
            'department' => $this->department,
            'context_owner_id' => $this->contextOwnerId->getStorageValue(),
            'default_billing_address_id' => $this->defaultBillingAddressId->getStorageValue(),
            'default_shipping_address_id' => $this->defaultShippingAddressId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->password = $data['password'];
        $this->encoder = $data['encoder'];
        $this->email = $data['email'];
        $this->active = (bool) $data['active'];
        $this->language = IdValue::create($data['language']);
        $this->title = (string) $data['title'];
        $this->salutation = (string) $data['salutation'];
        $this->firstName = $data['firstname'];
        $this->lastName = $data['lastname'];
        $this->department = $data['department'];
        $this->contextOwnerId = IdValue::create($data['context_owner_id']);
        $this->authId = IdValue::create($data['auth_id']);

        if (isset($data['default_billing_address_id'])) {
            $this->defaultBillingAddressId = IdValue::create($data['default_billing_address_id']);
        }

        if (isset($data['default_shipping_address_id'])) {
            $this->defaultShippingAddressId = IdValue::create($data['default_shipping_address_id']);
        }

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->active = (bool) $this->active;

        $this->id = IdValue::create($this->id);
        $this->contextOwnerId = IdValue::create($this->contextOwnerId);
        $this->language = IdValue::create($this->language);
        $this->defaultBillingAddressId = IdValue::create($this->defaultBillingAddressId);
        $this->defaultShippingAddressId = IdValue::create($this->defaultShippingAddressId);
    }

    public function toArray(): array
    {
        $contactArray = get_object_vars($this);

        unset($contactArray['debtor']);
        foreach ($contactArray as $key => $value) {
            if ($value instanceof IdValue) {
                $contactArray[$key] = $value->getValue();
            }
        }

        return $contactArray;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
