<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserAddress;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserLoginCredentials;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserOrderCredentials;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserPostalSettings;

class ContactIdentity implements Identity
{
    /**
     * @var IdValue
     */
    private $id;

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var ContactEntity
     */
    private $contact;

    /**
     * @var IdValue
     */
    private $authId;

    /**
     * @var DebtorIdentity
     */
    private $debtorIdentity;

    /**
     * @var string
     */
    private $avatar;

    public function __construct(
        IdValue $authId,
        IdValue $id,
        string $tableName,
        ContactEntity $contact,
        DebtorIdentity $debtorIdentity,
        string $avatar = ''
    ) {
        $this->authId = $authId;
        $this->id = $id;
        $this->tableName = $tableName;
        $this->contact = $contact;
        $this->debtorIdentity = $debtorIdentity;
        $this->avatar = $avatar;
    }

    public function getAuthId(): IdValue
    {
        return $this->authId;
    }

    public function getContextAuthId(): IdValue
    {
        return $this->debtorIdentity->getAuthId();
    }

    public function getId(): IdValue
    {
        return $this->id;
    }

    public function getTableName(): string
    {
        return $this->tableName;
    }

    public function getEntity(): Entity
    {
        return $this->contact;
    }

    public function getOwnershipEntity(): Entity
    {
        return $this->getDebtorEntity();
    }

    public function getLoginCredentials(): UserLoginCredentials
    {
        $contactEntity = $this->getContactEntity();

        return new UserLoginCredentials(
            $contactEntity->email,
            $contactEntity->password,
            $contactEntity->encoder,
            $contactEntity->active
        );
    }

    public function getPostalSettings(): UserPostalSettings
    {
        $contactEntity = $this->getContactEntity();

        return new UserPostalSettings(
            $contactEntity->salutation,
            $contactEntity->title,
            $contactEntity->firstName,
            $contactEntity->lastName,
            $this->getDebtorEntity()->language,
            $contactEntity->email
        );
    }

    public function getOrderCredentials(): UserOrderCredentials
    {
        $debtorEntity = $this->getDebtorEntity();
        $contactEntity = $this->getContactEntity();

        return new UserOrderCredentials(
            $debtorEntity->customernumber,
            $debtorEntity->id,
            $contactEntity->email
        );
    }

    public function getMainShippingAddress(): UserAddress
    {
        if (!$this->getContactEntity()->defaultShippingAddressId instanceof NullIdValue) {
            return new UserAddress($this->getContactEntity()->defaultShippingAddressId);
        }

        return new UserAddress($this->getDebtorEntity()->default_shipping_address_id);
    }

    public function getMainBillingAddress(): UserAddress
    {
        if (!$this->getContactEntity()->defaultBillingAddressId instanceof NullIdValue) {
            return new UserAddress($this->getContactEntity()->defaultBillingAddressId);
        }

        return new UserAddress($this->getDebtorEntity()->default_billing_address_id);
    }

    private function getDebtorEntity(): DebtorEntity
    {
        return $this->debtorIdentity->getEntity();
    }

    private function getContactEntity(): ContactEntity
    {
        return $this->getEntity();
    }

    public function getOwnershipContext(): OwnershipContext
    {
        return new OwnershipContext(
            $this->authId,
            $this->debtorIdentity->getAuthId(),
            $this->getDebtorEntity()->email,
            $this->debtorIdentity->getId(),
            $this->getId(),
            __CLASS__
        );
    }

    public function isSuperAdmin(): bool
    {
        return false;
    }

    public function isApiUser(): bool
    {
        return false;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): void
    {
        $this->avatar = $avatar;
    }
}
