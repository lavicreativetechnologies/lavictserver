<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Shopware\B2B\StoreFrontAuthentication\Framework\CredentialsBuilderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserRepositoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function mb_strlen;

class ContactValidationService
{
    const CAUSE_INVALID_ADDRESS = 'InvalidAddress';
    const CAUSE_MAIL_ERROR = 'MailSentError';
    const CAUSE_PASSWORD_LENGTH = 'PasswordLengthError';
    const CAUSE_DELETE_YOURSELF = 'DeleteYourselfError';

    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var CredentialsBuilderInterface
     */
    private $credentialsBuilder;

    /**
     * @var ContactPasswordProviderInterface
     */
    private $passwordProvider;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator,
        ContactRepository $contactRepository,
        UserRepositoryInterface $userRepository,
        CredentialsBuilderInterface $credentialsBuilder,
        AddressRepositoryInterface $addressRepository,
        ContactPasswordProviderInterface $passwordProvider
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
        $this->contactRepository = $contactRepository;
        $this->userRepository = $userRepository;
        $this->credentialsBuilder = $credentialsBuilder;
        $this->addressRepository = $addressRepository;
        $this->passwordProvider = $passwordProvider;
    }

    public function createInsertValidation(ContactEntity $contact, OwnershipContext $ownershipContext = null): Validator
    {
        $validation = $this->createCrudValidation($contact)
            ->validateThat('id', $contact->id)
                ->isNullIdValue()

            ->validateThat('email', $contact->email)
                ->isUnique(function () use ($contact) {
                    $credentials = $this->credentialsBuilder->createCredentialsByEmail($contact->email);

                    try {
                        $this->contactRepository->fetchOneByCredentials($credentials);
                    } catch (NotFoundException $e) {
                        return $this->userRepository->isMailAvailable($credentials);
                    }

                    return false;
                })
            ->isNotBlank()
            ->isEmail();

        if (!$ownershipContext) {
            return $validation->getValidator($this->validator);
        }

        $validation
            ->validateThat('defaultShippingAddressId', $contact->defaultShippingAddressId)
                ->isIdValueOrNullIdValue()
                ->withCallback(
                    function ($adressId) use ($ownershipContext): bool {
                        if ($adressId instanceof NullIdValue) {
                            return true;
                        }

                        return $this->validateAddress($adressId, $ownershipContext);
                    },
                    'The default shipping address id is invalid.',
                    self::CAUSE_INVALID_ADDRESS
                )
            ->validateThat('defaultBillingAddressId', $contact->defaultBillingAddressId)
                ->isIdValueOrNullIdValue()
                ->withCallback(
                    function ($adressId) use ($ownershipContext): bool {
                        if ($adressId instanceof NullIdValue) {
                            return true;
                        }

                        return $this->validateAddress($adressId, $ownershipContext);
                    },
                    'The default billing address id is invalid.',
                    self::CAUSE_INVALID_ADDRESS
                );

        return $validation->getValidator($this->validator);
    }

    public function createUpdateValidation(ContactEntity $contact, OwnershipContext $ownershipContext): Validator
    {
        $validation = $this->createCrudValidation($contact)
            ->validateThat('id', $contact->id)
                ->isIdValue()

            ->validateThat('email', $contact->email)
                ->isNotBlank()
                ->isEmail()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_70)
                ->isUnique(
                    function () use ($contact, $ownershipContext) {
                        try {
                            $mailContact = $this->contactRepository->fetchOneById($contact->id, $ownershipContext);
                        } catch (NotFoundException $e) {
                            return false;
                        }

                        if ($mailContact->email === $contact->email) {
                            return true;
                        }

                        $credentials = $this->credentialsBuilder->createCredentialsByEmail($contact->email);

                        try {
                            $this->contactRepository->fetchOneByCredentials($credentials);

                            return false;
                        } catch (NotFoundException $e) {
                            // nth
                        }

                        return $this->userRepository->isMailAvailable($credentials);
                    }
                )
            ->validateThat('defaultShippingAddressId', $contact->defaultShippingAddressId)
                ->isIdValueOrNullIdValue()
                ->withCallback(
                    function ($adressId) use ($ownershipContext): bool {
                        if ($adressId instanceof NullIdValue) {
                            return true;
                        }

                        return $this->validateAddress($adressId, $ownershipContext);
                    },
                    'The default shipping address id is invalid.',
                    self::CAUSE_INVALID_ADDRESS
                )
            ->validateThat('defaultBillingAddressId', $contact->defaultBillingAddressId)
                ->isIdValueOrNullIdValue()
                ->withCallback(
                    function ($adressId) use ($ownershipContext): bool {
                        if ($adressId instanceof NullIdValue) {
                            return true;
                        }

                        return $this->validateAddress($adressId, $ownershipContext);
                    },
                    'The default billing address id is invalid.',
                    self::CAUSE_INVALID_ADDRESS
                );

        return $validation->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function createCrudValidation(ContactEntity $contact): ValidationBuilder
    {
        return $this->validationBuilder

            ->validateThat('password', $contact->password)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255)
                ->withCallback(
                    function ($password) {
                        return mb_strlen($password) >= $this->passwordProvider->getMinPasswordLength();
                    },
                    'The password must be at least %int% characters long.',
                    self::CAUSE_PASSWORD_LENGTH,
                    [
                        '%int%' => $this->passwordProvider->getMinPasswordLength(),
                    ]
                )

            ->validateThat('encoder', $contact->encoder)
                ->isNotBlank()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255)
                ->isString()

            ->validateThat('active', $contact->active)
                ->isBool()

            ->validateThat('language', $contact->language)
                ->isIdValueOrNullIdValue()

            ->validateThat('title', $contact->title)
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_100)

            ->validateThat('salutation', $contact->salutation)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_30)

            ->validateThat('firstName', $contact->firstName)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_70)

            ->validateThat('lastName', $contact->lastName)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_70)

            ->validateThat('department', $contact->department)
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255)

            ->validateThat('contextOwnerId', $contact->contextOwnerId)
                ->isNotBlank();
    }

    public function createDeleteValidation(ContactEntity $contact, OwnershipContext $ownershipContext): Validator
    {
        return $this->validationBuilder

            ->validateThat('id', $contact->authId->getValue())
                ->withCallback(
                    function ($authId) use ($ownershipContext) {
                        return !($authId === $ownershipContext->authId->getValue());
                    },
                    'You cannot delete yourself',
                    self::CAUSE_DELETE_YOURSELF
                )

            ->getValidator($this->validator);
    }

    public function createMailNotSent(ContactEntity $contact): Validator
    {
        return $this->validationBuilder->validateThat('email', $contact->email)
            ->withCallback(
                function () {
                    return false;
                },
                'The account activation mail could not been sent to %value%',
                self::CAUSE_MAIL_ERROR
            )
            ->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function validateAddress(IdValue $addressId, OwnershipContext $ownershipContext): bool
    {
        try {
            $addressEntity = $this->addressRepository->insecureFetchOneById($addressId);
        } catch (NotFoundException $e) {
            return false;
        }

        return $addressEntity->user_id->equals($ownershipContext->shopOwnerUserId);
    }
}
