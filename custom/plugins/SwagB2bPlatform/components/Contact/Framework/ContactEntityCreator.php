<?php declare(strict_types=1);

namespace Shopware\B2B\Contact\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\EntityCreatorInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserRepositoryInterface;

class ContactEntityCreator implements EntityCreatorInterface
{
    /**
     * @var ContactCrudService
     */
    private $contactCrudService;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        ContactCrudService $contactCrudService,
        UserRepositoryInterface $userRepository
    ) {
        $this->contactCrudService = $contactCrudService;
        $this->userRepository = $userRepository;
    }

    public function createEntityByUserId(IdValue $id, OwnershipContext $ownershipContext): Entity
    {
        $user = $this->userRepository->fetchOneById($id);
        $user['contextOwnerId'] = $ownershipContext->authId;
        $data = $this->contactCrudService->validateShopUserDataForRequest($user);

        $serviceRequest = $this->contactCrudService->createNewRecordRequest($data);

        return $this->contactCrudService->createByApi($serviceRequest, $ownershipContext);
    }
}
