<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Api;

use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\Role\Framework\RoleRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class AssignmentRoleController extends ApiAssignmentController
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    public function __construct(
        RoleRepository $roleRepository,
        AclRepository $aclRepository,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        parent::__construct($aclRepository, $authStorageAdapter);
        $this->roleRepository = $roleRepository;
    }

    public function getAllGrantAction(string $roleId): array
    {
        $roleId = IdValue::create($roleId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->getAllGrant($roleEntity);
    }

    public function getAllAllowedAction(string $roleId): array
    {
        $roleId = IdValue::create($roleId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->getAllAllowed($roleEntity);
    }

    public function getAllowedAction(string $roleId, string $subjectId): array
    {
        $roleId = IdValue::create($roleId);
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->getAllowed($roleEntity, $subjectId);
    }

    public function allowAction(string $roleId, string $subjectId): array
    {
        $roleId = IdValue::create($roleId);
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->allow($roleEntity, $subjectId, false);
    }

    public function allowGrantAction(string $roleId, string $subjectId): array
    {
        $roleId = IdValue::create($roleId);
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->allow($roleEntity, $subjectId, true);
    }

    public function multipleAllowAction(string $roleId, Request $request): array
    {
        $roleId = IdValue::create($roleId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->multipleAllow($roleEntity, $request->getPost());
    }

    public function multipleDenyAction(string $roleId, Request $request): array
    {
        $roleId = IdValue::create($roleId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->multipleDeny($roleEntity, $request->getPost());
    }

    public function denyAction(string $roleId, string $subjectId): array
    {
        $roleId = IdValue::create($roleId);
        $subjectId = IdValue::create($subjectId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        /** @var RoleEntity $roleEntity */
        $roleEntity = $this->getRoleEntityById($roleId, $ownershipContext);

        return $this->deny($roleEntity, $subjectId);
    }

    /**
     * @internal
     */
    protected function getRoleEntityById(IdValue $roleId, OwnershipContext $ownershipContext): CrudEntity
    {
        return $this->roleRepository
            ->fetchOneById($roleId, $ownershipContext);
    }
}
