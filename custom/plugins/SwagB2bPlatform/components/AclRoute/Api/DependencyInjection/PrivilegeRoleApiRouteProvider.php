<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class PrivilegeRoleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}',
                'b2b_assignment.api_privilege_role_controller',
                'getAllAllowed',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/grant',
                'b2b_assignment.api_privilege_role_controller',
                'getAllGrant',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/privilege/{privilegeId}',
                'b2b_assignment.api_privilege_role_controller',
                'getAllowed',
                ['roleId', 'privilegeId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/privilege/{privilegeId}',
                'b2b_assignment.api_privilege_role_controller',
                'allow',
                ['roleId', 'privilegeId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/privilege/{privilegeId}/grant',
                'b2b_assignment.api_privilege_role_controller',
                'allowGrant',
                ['roleId', 'privilegeId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/allow',
                'b2b_assignment.api_privilege_role_controller',
                'multipleAllow',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/deny',
                'b2b_assignment.api_privilege_role_controller',
                'multipleDeny',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/privilege_role/{roleId}/privilege/{privilegeId}',
                'b2b_assignment.api_privilege_role_controller',
                'deny',
                ['roleId', 'privilegeId'],
            ],
        ];
    }
}
