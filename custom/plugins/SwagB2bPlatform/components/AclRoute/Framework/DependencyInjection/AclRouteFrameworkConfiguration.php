<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Framework\DependencyInjection;

use Shopware\B2B\Acl\Framework\DependencyInjection\AclFrameworkConfiguration;
use Shopware\B2B\AclRoute\Bridge\DependencyInjection\AclRouteBridgeConfiguration;
use Shopware\B2B\Common\Controller\DependencyInjection\ControllerConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Repository\DependencyInjection\RepositoryConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AclRouteFrameworkConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/framework-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new AclFrameworkConfiguration(),
            AclRouteBridgeConfiguration::create(),
            new RepositoryConfiguration(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
            new ControllerConfiguration(),
        ];
    }
}
