<?php declare(strict_types=1);

namespace Shopware\B2B\AclRoute\Framework;

use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Acl\Framework\AclRepositoryFactory;
use Shopware\B2B\Acl\Framework\AclUnsupportedContextException;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function mb_strtolower;

/**
 * Main routing service contains inspection methods for routing
 */
class AclRouteService
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var AclRouteRepository
     */
    private $aclRouteRepository;

    /**
     * @var AclRepository
     */
    private $aclRepository;

    /**
     * @var array
     */
    private $allowedControllerActionCache = [];

    /**
     * @throws AclUnsupportedContextException
     */
    public function __construct(
        AuthenticationService $authenticationService,
        AclRouteRepository $aclRouteRepository,
        AclRepositoryFactory $aclRegistry
    ) {
        $this->authenticationService = $authenticationService;
        $this->aclRouteRepository = $aclRouteRepository;
        $this->aclRepository = $aclRegistry
            ->createRepository(AclRouteRepository::PRIVILEGE_TABLE_NAME);
    }

    public function isRouteAllowed(string $controllerName, string $actionName): bool
    {
        if (!$this->authenticationService->isB2b()) {
            return true;
        }

        $id = $this->authenticationService->getIdentity()->getId()->getValue();
        $cacheKey = $id . '_' . $controllerName . '_' . $actionName;

        if (isset($this->allowedControllerActionCache[$cacheKey])) {
            return (bool) $this->allowedControllerActionCache[$cacheKey];
        }

        try {
            $privilegeId = $this->aclRouteRepository
                ->fetchPrivilegeIdByControllerAndActionName(
                    mb_strtolower($controllerName),
                    mb_strtolower($actionName)
                );
        } catch (NotFoundException $e) {
            return true;
        }

        $ownershipContext = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        try {
            $isAllowed = $this->aclRepository
                ->isAllowed($ownershipContext, $privilegeId);
        } catch (AclUnsupportedContextException $e) {
            $isAllowed = true;
        }

        return $this->allowedControllerActionCache[$cacheKey] = $isAllowed;
    }

    /**
     * @return string[]
     */
    public function getPrivilegeTypes(): array
    {
        return [
            'list',
            'detail',
            'update',
            'create',
            'delete',
            'assign',
        ];
    }
}
