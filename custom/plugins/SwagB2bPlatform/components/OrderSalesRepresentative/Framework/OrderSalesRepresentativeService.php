<?php declare(strict_types=1);

namespace Shopware\B2B\OrderSalesRepresentative\Framework;

use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeIdentity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeIdentityInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class OrderSalesRepresentativeService
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        OrderContextRepository $orderContextRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->orderContextRepository = $orderContextRepository;
    }

    public function addSalesRepresentativeToOrder(OrderContext $orderContext): void
    {
        if (!$this->authenticationService->is(SalesRepresentativeIdentityInterface::class)
            || $this->authenticationService->is(SalesRepresentativeIdentity::class)) {
            return;
        }

        $salesRepId = $this->authenticationService->getIdentity()->getSalesRepresentativeId();

        $this->orderContextRepository->setSalesRepresentativeById(
            $orderContext->id,
            $salesRepId
        );
    }
}
