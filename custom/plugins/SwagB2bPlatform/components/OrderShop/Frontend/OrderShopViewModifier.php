<?php declare(strict_types=1);

namespace Shopware\B2B\OrderShop\Frontend;

use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Order\Framework\OrderEntity;
use Shopware\B2B\Order\Frontend\OrderModifyViewInterface;
use Shopware\B2B\Shop\Framework\OrderRelationServiceInterface;

class OrderShopViewModifier implements OrderModifyViewInterface
{
    /**
     * @var OrderRelationServiceInterface
     */
    private $orderRelationService;

    public function __construct(OrderRelationServiceInterface $orderRelationService)
    {
        $this->orderRelationService = $orderRelationService;
    }

    public function modifyMasterDataView(OrderEntity $order): array
    {
        $viewData = [];

        try {
            if ($order->paymentId) {
                $viewData['paymentName'] = $this->orderRelationService->getPaymentNameForId($order->paymentId);
            }
        } catch (NotFoundException $e) {
            //nth
        }

        try {
            $viewData['shippingName'] = $this->orderRelationService->getShippingNameForId($order->shippingId);
        } catch (NotFoundException $e) {
            //nth
        }

        return $viewData;
    }
}
