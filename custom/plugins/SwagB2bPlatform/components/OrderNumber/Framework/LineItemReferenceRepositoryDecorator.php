<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\NotAuthenticatedException;
use function array_key_exists;
use function array_keys;
use function array_map;
use function array_pop;
use function array_values;
use function func_get_args;

class LineItemReferenceRepositoryDecorator implements LineItemReferenceRepositoryInterface
{
    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $decorated;

    public function __construct(
        LineItemReferenceRepositoryInterface $decorated,
        OrderNumberRepositoryInterface $orderNumberRepository,
        AuthenticationService $authenticationService
    ) {
        $this->orderNumberRepository = $orderNumberRepository;
        $this->authenticationService = $authenticationService;
        $this->decorated = $decorated;
    }

    public function addReference(
        IdValue $listId,
        LineItemReference $lineItemReference
    ): LineItemReference {
        try {
            $identity = $this->authenticationService->getIdentity();
            $ownershipContext = $identity->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            return $this->decorated->addReference($listId, $lineItemReference);
        }

        if ($identity->isApiUser()) {
            return $this->decorated->addReference($listId, $lineItemReference);
        }

        $lineItemReference->referenceNumber = $this->orderNumberRepository->fetchOriginalOrderNumber(
            $lineItemReference->referenceNumber,
            $ownershipContext
        );

        return $this->decorated->addReference($listId, $lineItemReference);
    }

    public function updateReference(
        IdValue $listId,
        LineItemReference $lineItemReference
    ): void {
        try {
            $identity = $this->authenticationService->getIdentity();
            $ownershipContext = $identity->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            $this->decorated->updateReference($listId, $lineItemReference);

            return;
        }

        if ($identity->isApiUser()) {
            $this->decorated->updateReference($listId, $lineItemReference);

            return;
        }

        $this->addCustomOrderNumber(
            $lineItemReference,
            $this->orderNumberRepository->fetchCustomOrderNumber(
                $lineItemReference->referenceNumber,
                $ownershipContext
            )
        );

        $this->decorated->updateReference($listId, $lineItemReference);
    }

    public function hasReference(string $referenceNumber, IdValue $listId): bool
    {
        try {
            $identity = $this->authenticationService->getIdentity();
            $ownershipContext = $identity->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            return $this->decorated->hasReference($referenceNumber, $listId);
        }

        if ($identity->isApiUser()) {
            return $this->decorated->hasReference($referenceNumber, $listId);
        }

        $originalNumber = $this->orderNumberRepository->fetchOriginalOrderNumber(
            $referenceNumber,
            $ownershipContext
        );

        return $this->decorated->hasReference($originalNumber, $listId);
    }

    public function getReferenceByReferenceNumberAndListId(string $referenceNumber, IdValue $listId /**, CurrencyContext $currencyContext **/): LineItemReference
    {
        $currencyContext = func_get_args()[2] ?? null;

        try {
            $identity = $this->authenticationService->getIdentity();
            $ownershipContext = $identity->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            return $this->decorated->getReferenceByReferenceNumberAndListId($referenceNumber, $listId, $currencyContext);
        }

        if ($identity->isApiUser()) {
            return $this->decorated->getReferenceByReferenceNumberAndListId($referenceNumber, $listId, $currencyContext);
        }

        $numbers = $this->orderNumberRepository->fetchNumbers(
            [$referenceNumber],
            $ownershipContext
        );

        $originalNumbers = array_keys($numbers);
        $originalNumber = (string) array_pop($originalNumbers);
        $customNumbers = array_values($numbers);
        $customNumber = (string) array_pop($customNumbers);

        $lineItemReference = $this->decorated->getReferenceByReferenceNumberAndListId($originalNumber, $listId, $currencyContext);

        $this->addCustomOrderNumber($lineItemReference, $customNumber);

        return $lineItemReference;
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     * @throws NotFoundException
     */
    public function fetchReferenceById(IdValue $id /**, CurrencyContext $currencyContext **/): LineItemReference
    {
        $currencyContext = func_get_args()[1] ?? null;

        try {
            $identity = $this->authenticationService->getIdentity();
            $ownershipContext = $identity->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            return $this->decorated->fetchReferenceById($id, $currencyContext);
        }

        if ($identity->isApiUser()) {
            return $this->decorated->fetchReferenceById($id, $currencyContext);
        }

        $reference = $this->decorated->fetchReferenceById($id, $currencyContext);

        $this->addCustomOrderNumber(
            $reference,
            $this->orderNumberRepository->fetchCustomOrderNumber(
                $reference->referenceNumber,
                $ownershipContext
            )
        );

        return $reference;
    }

    /**
     * @deprecated tag:v4.5.0 CurrencyContext will be added
     * @return LineItemReference[]
     */
    public function fetchList(IdValue $listId, LineItemReferenceSearchStruct $searchStruct /**, CurrencyContext $currencyContext**/): array
    {
        $currencyContext = func_get_args()[2] ?? null;

        try {
            $identity = $this->authenticationService->getIdentity();
            $ownershipContext = $identity->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            return $this->decorated->fetchList($listId, $searchStruct, $currencyContext);
        }

        if ($identity->isApiUser()) {
            return $this->decorated->fetchList($listId, $searchStruct, $currencyContext);
        }

        $referenceList = $this->decorated->fetchList($listId, $searchStruct, $currencyContext);

        $referenceNumbers = array_map(function (LineItemReference $reference) {
            return $reference->referenceNumber;
        }, $referenceList);

        $numbers = $this->orderNumberRepository->fetchNumbers(
            $referenceNumbers,
            $ownershipContext
        );

        foreach ($referenceList as $reference) {
            if (!array_key_exists($reference->referenceNumber, $numbers)) {
                continue;
            }

            $this->addCustomOrderNumber($reference, $numbers[$reference->referenceNumber]);
        }

        return $referenceList;
    }

    /**
     * @internal
     */
    protected function addCustomOrderNumber(LineItemReference $lineItemReference, string $newOrderNumber): void
    {
        $lineItemReference->customReferenceNumber = $newOrderNumber;
    }

    public function fetchTotalCount(IdValue $listId, LineItemReferenceSearchStruct $searchStruct): int
    {
        return $this->decorated->fetchTotalCount($listId, $searchStruct);
    }

    public function flipSorting(LineItemReference $lineItemReferenceOne, LineItemReference $lineItemReferenceTwo)
    {
        return $this->decorated->flipSorting($lineItemReferenceOne, $lineItemReferenceTwo);
    }

    public function addVoucherCodeToReferenceById(IdValue $referenceId, string $voucherCode)
    {
        return $this->decorated->addVoucherCodeToReferenceById($referenceId, $voucherCode);
    }

    public function removeReference(IdValue $id)
    {
        return $this->decorated->removeReference($id);
    }

    public function syncReferences(IdValue $listId, array $references)
    {
        return $this->decorated->syncReferences($listId, $references);
    }

    public function removeReferenceByListId(IdValue $id)
    {
        return $this->decorated->removeReferenceByListId($id);
    }

    public function getMainTableAlias(): string
    {
        return $this->decorated->getMainTableAlias();
    }

    public function getFullTextSearchFields(): array
    {
        return $this->decorated->getFullTextSearchFields();
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return $this->decorated->getAdditionalSearchResourceAndFields();
    }

    public function updatePrices(LineItemReference $lineItemReference)
    {
        return $this->decorated->updatePrices($lineItemReference);
    }

    public function getOwnershipByReferenceId(IdValue $referenceId): IdValue
    {
        return $this->decorated->getOwnershipByReferenceId($referenceId);
    }
}
