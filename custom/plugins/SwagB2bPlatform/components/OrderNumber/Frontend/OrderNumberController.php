<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Frontend;

use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Repository\SearchStruct;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\OrderNumber\Framework\OrderNumberContext;
use Shopware\B2B\OrderNumber\Framework\OrderNumberCrudService;
use Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface;
use Shopware\B2B\OrderNumber\Framework\OrderNumberService;
use Shopware\B2B\OrderNumber\Framework\UnsupportedFileException;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use function array_merge;
use function trim;

class OrderNumberController
{
    const COUNTING_FROM = 1;

    /**
     * @var ProductServiceInterface
     */
    private $productService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var OrderNumberService
     */
    private $orderNumberService;

    /**
     * @var OrderNumberCrudService
     */
    private $orderNumberCrudService;

    public function __construct(
        ProductServiceInterface $productService,
        AuthenticationService $authenticationService,
        OrderNumberRepositoryInterface $orderNumberRepository,
        GridHelper $gridHelper,
        OrderNumberService $orderNumberCsvService,
        OrderNumberCrudService $orderNumberCrudService
    ) {
        $this->productService = $productService;
        $this->authenticationService = $authenticationService;
        $this->orderNumberRepository = $orderNumberRepository;
        $this->gridHelper = $gridHelper;
        $this->orderNumberService = $orderNumberCsvService;
        $this->orderNumberCrudService = $orderNumberCrudService;
    }

    public function indexAction(): void
    {
        // nth
    }

    public function gridAction(Request $request): array
    {
        $ownerShip = $this->authenticationService->getIdentity()->getOwnershipContext();

        $searchStruct = new SearchStruct();
        $currentPage = (int) $request->getParam('page', 1);

        $this->gridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        $orderNumbers = $this->orderNumberRepository
            ->fetchList($searchStruct, $ownerShip);

        $orderNumbers = $this->orderNumberService->fetchOrderNumberProductNames($orderNumbers);

        $totalCount = $this->orderNumberRepository
            ->fetchTotalCount($searchStruct, $ownerShip);

        $maxPage = $this->gridHelper
            ->getMaxPage($totalCount);

        $gridState = $this->gridHelper
            ->getGridState($request, $searchStruct, $orderNumbers, $maxPage, $currentPage);

        $validationResponse = $this->gridHelper->getValidationResponse('orderNumber');

        $errors = $request->getParam('errors');

        if ($errors) {
            $validationResponse = array_merge($validationResponse, ['errors' => $errors]);
        }

        return array_merge(
            [
                'gridState' => $gridState,
                'message' => $request->getParam('message'),
            ],
            $validationResponse
        );
    }

    public function createAction(Request $request): array
    {
        $request->checkPost();

        $post = $request->getPost();

        $serviceRequest = $this->orderNumberCrudService
            ->createNewRecordRequest($post);

        $identity = $this->authenticationService
            ->getIdentity();

        $message = null;
        try {
            $orderNumber = $this->orderNumberCrudService
                ->create($serviceRequest, $identity->getOwnershipContext());

            $message = [
                'snippetKey' => 'TheCustomOrderNumberValueWasCreated',
                'messageTemplate' => 'The custom ordernumber %value% was created.',
                'parameters' => [
                    '%value%' => $orderNumber->customOrderNumber,
                ],
            ];
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
        }

        throw new B2bControllerForwardException('grid', null, ['message' => $message]);
    }

    public function updateAction(Request $request): array
    {
        $request->checkPost();

        $post = $request->getPost();
        $serviceRequest = $this->orderNumberCrudService
            ->createExistingRecordRequest($post);

        $ownershipContext = $this->authenticationService->getIdentity()
            ->getOwnershipContext();

        $message = null;
        try {
            $orderNumber = $this->orderNumberCrudService
                ->update($serviceRequest, $ownershipContext);

            $message = [
                'snippetKey' => 'TheCustomOrderNumberValueWasUpdated',
                'messageTemplate' => 'The custom ordernumber %value% was updated.',
                'parameters' => [
                    '%value%' => $orderNumber->customOrderNumber,
                ],
            ];
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
        }

        throw new B2bControllerForwardException('grid', null, ['message' => $message]);
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function removeAction(Request $request): void
    {
        $request->checkPost();
        $id = $request->getIdValue('id');

        $ownershipContext = $this->authenticationService->getIdentity()
            ->getOwnershipContext();

        try {
            $this->orderNumberCrudService->remove($id, $ownershipContext);
        } catch (NotFoundException $e) {
            // nth
        }

        throw new B2bControllerForwardException('grid');
    }

    public function getProductNameAction(Request $request): array
    {
        $orderNumber = $request->getParam('orderNumber', '');

        try {
            $productName = $this->productService->fetchProductNameByOrderNumber(trim($orderNumber));
        } catch (NotFoundException $e) {
            $productName = false;
        }

        return ['productName' => $productName];
    }

    public function exportCsvAction(): array
    {
        $ownership = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        $csv = $this->orderNumberService
            ->getCsvExportData($ownership);

        $this->setExportResponseHeader('ordernumber-export.csv');

        return ['csvData' => $csv];
    }

    public function exportXlsAction(): array
    {
        $ownership = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        $xlsData = $this->orderNumberService
            ->getXlsExportData($ownership);

        $this->setExportResponseHeader('ordernumber-export.xls');

        return ['xlsData' => $xlsData];
    }

    public function uploadAction(): void
    {
        // nth
    }

    public function processUploadAction(Request $request): void
    {
        $orderNumberContext = $this->createOrderNumberContextFromRequest($request);
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $uploadedFile = $request->requireFileParam('uploadedFile');
        $errors = null;
        $message = null;

        try {
            $this->orderNumberService->processOrderNumberFile($uploadedFile, $orderNumberContext, $ownershipContext);
            $message = [
                'snippetKey' => 'TheCustomOrderNumbersWereUpdated',
                'messageTemplate' => 'The custom ordernumbers were updated.',
            ];
        } catch (UnsupportedFileException $e) {
            $errors = $this->createErrorArray($e);
        } catch (ValidationException $exception) {
            $this->gridHelper->pushValidationException($exception);
        }

        throw new B2bControllerForwardException('grid', 'b2bordernumber', [
            'errors' => $errors,
            'message' => $message,
        ]);
    }

    /**
     * @internal
     */
    protected function createOrderNumberContextFromRequest(Request $request): OrderNumberContext
    {
        $context = new OrderNumberContext();
        if ($request->hasParam('orderNumberColumn')) {
            $context->orderNumberColumn = (int) $request
                ->getParam('orderNumberColumn');
        }

        if ($request->hasParam('customOrderNumberColumn')) {
            $context->customOrderNumberColumn = (int) $request
                ->getParam('customOrderNumberColumn');
        }

        if ($request->hasParam('csvDelimiter')) {
            $context->csvDelimiter = $request
                ->getParam('csvDelimiter');
        }

        if ($request->hasParam('csvEnclosure') && ($enclosure = $request->getParam('csvEnclosure')) !== '') {
            $context->csvEnclosure = $enclosure;
        }

        if ($request->hasParam('headline')) {
            $context->headline = $request
                    ->getParam('headline') === 'true';
        }

        $context->orderNumberColumn -= static::COUNTING_FROM;
        $context->customOrderNumberColumn -= static::COUNTING_FROM;

        return $context;
    }

    /**
     * @internal
     */
    protected function createErrorArray(UnsupportedFileException $e): array
    {
        return [
            [
                'snippetKey' => 'FileExtensionInvalid',
                'messageTemplate' => 'The file extension %value% was invalid.',
                'parameters' => [
                    '%value%' => $e->getFileExtension(),
                ],
            ],
        ];
    }

    /**
     * @internal
     */
    protected function setExportResponseHeader(string $exportFileName): void
    {
        $response = new Response();

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $exportFileName
        );

        $response->headers->set('Content-Disposition', $disposition);

        $response->sendHeaders();
    }
}
