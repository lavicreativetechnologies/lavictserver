<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface;
use Shopware\B2B\Shop\Framework\ProductServiceInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\NotAuthenticatedException;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use function array_keys;
use function in_array;

class B2bProductServiceDecorator implements ProductServiceInterface
{
    /**
     * @var ProductServiceInterface
     */
    private $decorated;

    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var EntityRepositoryInterface
     */
    private $productRepository;

    public function __construct(
        ProductServiceInterface $decorated,
        OrderNumberRepositoryInterface $orderNumberRepository,
        AuthenticationService $authenticationService,
        EntityRepositoryInterface $productRepository
    ) {
        $this->decorated = $decorated;
        $this->orderNumberRepository = $orderNumberRepository;
        $this->authenticationService = $authenticationService;
        $this->productRepository = $productRepository;
    }

    public function fetchProductNameByOrderNumber(string $orderNumber): string
    {
        return $this->decorated->fetchProductNameByOrderNumber($orderNumber);
    }

    public function fetchProductNamesByOrderNumbers(array $orderNumbers): array
    {
        try {
            $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();
        } catch (NotAuthenticatedException $e) {
            // For admin users
            return $this->fetchProductNamesByOrderNumbersForAdmin($orderNumbers);
        }

        $originalNamesWithNumbers = $this->decorated->fetchProductNamesByOrderNumbers($orderNumbers);
        $numbers = $this->orderNumberRepository->fetchNumbers(array_keys($originalNamesWithNumbers), $ownershipContext);

        return $this->createProductNamesWithGivenOrderNumbers($orderNumbers, $originalNamesWithNumbers, $numbers);
    }

    public function fetchOrderNumberByReferenceNumber(string $referenceNumber): string
    {
        return $this->decorated->fetchOrderNumberByReferenceNumber($referenceNumber);
    }

    /**
     * @return string[] $productNumber => $productName
     */
    public function searchProductsByNameOrOrderNumber(string $term, int $limit): array
    {
        return $this->decorated->searchProductsByNameOrOrderNumber($term, $limit);
    }

    public function fetchStocksByOrderNumbers(array $orderNumbers): array
    {
        return $this->decorated->fetchStocksByOrderNumbers($orderNumbers);
    }

    public function isNormalProduct(LineItemReference $reference): bool
    {
        return $this->decorated->isNormalProduct($reference);
    }

    public function fetchProductOrderNumbersByReferenceNumbers(array $referenceNumbers): array
    {
        return $this->decorated->fetchProductOrderNumbersByReferenceNumbers($referenceNumbers);
    }

    /**
     * @internal
     */
    protected function createProductNamesWithGivenOrderNumbers(array $searchNumbers, array $originalNamesWithNumbers, array $customNumbers): array
    {
        $productNames = [];
        foreach ($originalNamesWithNumbers as $originalNumber => $name) {
            if (in_array($customNumbers[$originalNumber], $searchNumbers, true)) {
                $productNames[$customNumbers[$originalNumber]] = $name;

                continue;
            }

            $productNames[$originalNumber] = $name;
        }

        return $productNames;
    }

    /**
     * @internal
     */
    protected function fetchProductNamesByOrderNumbersForAdmin(array $orderNumbers): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('product.productNumber', $orderNumbers));

        $products = $this->productRepository->search($criteria, Context::createDefaultContext());

        $productNames = [];

        /** @var ProductEntity $product */
        foreach ($products as $product) {
            $productNames[$product->getProductNumber()] = $product->getTranslation('name');
        }

        return $productNames;
    }
}
