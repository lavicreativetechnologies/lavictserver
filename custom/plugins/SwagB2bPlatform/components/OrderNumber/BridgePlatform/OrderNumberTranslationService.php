<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\B2B\OrderNumber\Framework\OrderNumberTranslationServiceInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrderNumberTranslationService implements OrderNumberTranslationServiceInterface
{
    const PRODUCT_LABEL_SNIPPET = 'b2b.ProductLabel';

    const PRODUCT_ORDER_NUMBER_LABEL_SNIPPET = 'b2b.ProductOrderNumberLabel';

    const CUSTOM_PRODUCT_ORDER_NUMBER_LABEL_SNIPPET = 'b2b.CustomProductOrderNumberLabel';

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getProductTranslation(): string
    {
        return $this->translator->trans(self::PRODUCT_LABEL_SNIPPET);
    }

    public function getProductOrderNumberTranslation(): string
    {
        return $this->translator->trans(self::PRODUCT_ORDER_NUMBER_LABEL_SNIPPET);
    }

    public function getCustomProductOrderNumberTranslation(): string
    {
        return $this->translator->trans(self::CUSTOM_PRODUCT_ORDER_NUMBER_LABEL_SNIPPET);
    }
}
