<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Shop\BridgePlatform\SalesChannelProductExtension;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SearchKeyword\ProductSearchBuilderInterface;
use Shopware\Core\Content\Product\SearchKeyword\ProductSearchTermInterpreterInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Query\ScoreQuery;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Term\SearchPattern;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Term\SearchTerm;
use Shopware\Core\Framework\Routing\Exception\MissingRequestParameterException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Elasticsearch\Framework\ElasticsearchHelper;
use Symfony\Component\HttpFoundation\Request;
use function array_shift;
use function array_values;
use function implode;
use function is_array;
use function trim;

class ProductSearchBuilderDecorator implements ProductSearchBuilderInterface
{
    private const PRODUCT_ORDER_NUMBER_CUSTOM_ORDER_NUMBER_FIELD = 'product.b2bOrderNumber.customOrderNumber';
    private const PRODUCT_ORDER_NUMBER_CONTEXT_OWNER_ID_FIELD = 'product.b2bOrderNumber.contextOwnerId';
    private const PRODUCT_SEARCH_KEYWORDS_RANKING_FIELD = 'product.searchKeywords.ranking';
    private const PRODUCT_SEARCH_KEYWORDS_KEYWORD_FIELD = 'product.searchKeywords.keyword';
    private const PRODUCT_SEARCH_KEYWORDS_LANGUAGE_ID_FIELD = 'product.searchKeywords.languageId';
    private const QUERY_SEARCH_PARAMETER = 'search';

    /**
     * @var ProductSearchBuilderInterface
     */
    private $decorated;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var ProductSearchTermInterpreterInterface
     */
    private $interpreter;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ElasticsearchHelper
     */
    private $elasticsearchHelper;

    /**
     * @var ProductDefinition
     */
    private $productDefinition;

    public function __construct(
        ProductSearchBuilderInterface $decorated,
        AuthenticationService $authenticationService,
        ProductSearchTermInterpreterInterface $interpreter,
        Connection $connection,
        ElasticsearchHelper $elasticsearchHelper,
        ProductDefinition $productDefinition
    ) {
        $this->decorated = $decorated;
        $this->authenticationService = $authenticationService;
        $this->interpreter = $interpreter;
        $this->connection = $connection;
        $this->elasticsearchHelper = $elasticsearchHelper;
        $this->productDefinition = $productDefinition;
    }

    public function build(Request $request, Criteria $criteria, SalesChannelContext $context): void
    {
        if (!$this->authenticationService->isB2b()) {
            $this->decorated->build($request, $criteria, $context);

            return;
        }

        if ($this->elasticsearchHelper->allowSearch($this->productDefinition, $context->getContext())) {
            $this->decorated->build($request, $criteria, $context);

            return;
        }

        $pattern = $this->createEnhancedSearchPattern($request, $context);

        $criteria->addAssociation(SalesChannelProductExtension::B2B_ORDER_NUMBER_EXTENSION_NAME);

        $this->updateCriteriaWithScoreQueriesBySearchPattern($criteria, $pattern);
        $this->updateCriteriaWithFiltersBySearchPattern($criteria, $pattern);

        $criteria->addFilter(new EqualsFilter(
            self::PRODUCT_SEARCH_KEYWORDS_LANGUAGE_ID_FIELD,
            $context->getContext()->getLanguageId()
        ));
    }

    /**
     * @internal
     */
    protected function getSearchTermByRequest(Request $request): string
    {
        $search = $request->query->get(self::QUERY_SEARCH_PARAMETER);
        $term = is_array($search) ? implode(' ', $search) : (string) $search;
        $term = trim($term);

        if (empty($term)) {
            throw new MissingRequestParameterException(self::QUERY_SEARCH_PARAMETER);
        }

        return $term;
    }

    /**
     * @internal
     */
    protected function updateCriteriaWithScoreQueriesBySearchPattern(Criteria $criteria, SearchPattern $pattern): void
    {
        foreach ($pattern->getTerms() as $searchTerm) {
            $criteria->addQuery(
                new ScoreQuery(
                    new EqualsFilter(self::PRODUCT_SEARCH_KEYWORDS_KEYWORD_FIELD, $searchTerm->getTerm()),
                    $searchTerm->getScore(),
                    self::PRODUCT_SEARCH_KEYWORDS_RANKING_FIELD
                ),
                new ScoreQuery(
                    new EqualsFilter(self::PRODUCT_ORDER_NUMBER_CUSTOM_ORDER_NUMBER_FIELD, $searchTerm->getTerm()),
                    $searchTerm->getScore()
                )
            );
        }

        $criteria->addQuery(
            new ScoreQuery(
                new ContainsFilter(self::PRODUCT_SEARCH_KEYWORDS_KEYWORD_FIELD, $pattern->getOriginal()->getTerm()),
                $pattern->getOriginal()->getScore(),
                self::PRODUCT_SEARCH_KEYWORDS_RANKING_FIELD
            ),
            new ScoreQuery(
                new ContainsFilter(self::PRODUCT_ORDER_NUMBER_CUSTOM_ORDER_NUMBER_FIELD, $pattern->getOriginal()->getTerm()),
                $pattern->getOriginal()->getScore()
            )
        );
    }

    /**
     * @internal
     */
    protected function updateCriteriaWithFiltersBySearchPattern(Criteria $criteria, SearchPattern $pattern): void
    {
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_OR, [
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsAnyFilter(
                    self::PRODUCT_ORDER_NUMBER_CUSTOM_ORDER_NUMBER_FIELD,
                    array_values($pattern->getAllTerms())
                ),
                new EqualsFilter(
                    self::PRODUCT_ORDER_NUMBER_CONTEXT_OWNER_ID_FIELD,
                    $this->authenticationService->getIdentity()->getContextAuthId()->getStorageValue()
                ),
            ]),
            new EqualsAnyFilter(self::PRODUCT_SEARCH_KEYWORDS_KEYWORD_FIELD, array_values($pattern->getAllTerms())),
        ]));
    }

    /**
     * @internal
     */
    protected function createEnhancedSearchPattern(Request $request, SalesChannelContext $context): SearchPattern
    {
        $pattern = $this->interpreter->interpret(
            $this->getSearchTermByRequest($request),
            $context->getContext()
        );

        $customOrderNumbers = $this->connection->fetchAll(
            'SELECT custom_ordernumber
             FROM b2b_order_number
             WHERE context_owner_id = :contextOwnerId AND custom_ordernumber LIKE :term',
            [
                'term' => '%' . $pattern->getOriginal()->getTerm() . '%',
                'contextOwnerId' => $this->authenticationService->getIdentity()->getContextAuthId()->getStorageValue(),
            ]
        );

        if (!$customOrderNumbers) {
            return $pattern;
        }

        foreach ($customOrderNumbers as $customOrderNumber) {
            $pattern->addTerm(
                new SearchTerm(array_shift($customOrderNumber))
            );
        }

        return $pattern;
    }
}
