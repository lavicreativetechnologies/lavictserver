<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\OrderNumber\Framework\OrderNumberCrudService;
use Shopware\B2B\OrderNumber\Framework\OrderNumberEntity;
use Shopware\B2B\OrderNumber\Framework\OrderNumberFileEntity;
use Shopware\B2B\OrderNumber\Framework\OrderNumberRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Content\Product\Events\ProductIndexerEvent;
use Shopware\Elasticsearch\Framework\ElasticsearchHelper;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use function array_map;
use function array_merge;

class ElasticsearchOrderNumberCrudService extends OrderNumberCrudService
{
    /**
     * @var OrderNumberCrudService
     */
    private $decorated;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var OrderNumberRepositoryInterface
     */
    private $orderNumberRepository;

    /**
     * @var ElasticsearchHelper
     */
    private $elasticsearchHelper;

    public function __construct(
        OrderNumberCrudService $decorated,
        ContextProvider $contextProvider,
        EventDispatcherInterface $eventDispatcher,
        OrderNumberRepositoryInterface $orderNumberRepository,
        ElasticsearchHelper $elasticsearchHelper
    ) {
        $this->decorated = $decorated;
        $this->contextProvider = $contextProvider;
        $this->eventDispatcher = $eventDispatcher;
        $this->orderNumberRepository = $orderNumberRepository;
        $this->elasticsearchHelper = $elasticsearchHelper;
    }

    public function update(CrudServiceRequest $request, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        if (!$this->elasticsearchHelper->allowIndexing()) {
            return $this->decorated->update($request, $ownershipContext);
        }

        $originalOrderNumber = $this->getPreviousOrderNumber($request, $ownershipContext);

        $orderNumber = $this->decorated->update($request, $ownershipContext);

        $this->index($originalOrderNumber->productId->getValue(), $orderNumber->productId->getValue());

        return $orderNumber;
    }

    public function remove(IdValue $id, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $orderNumber = $this->decorated->remove($id, $ownershipContext);

        $this->index($orderNumber->productId->getValue());

        return $orderNumber;
    }

    /**
     * @param OrderNumberFileEntity[] $orderNumbers
     */
    public function replace(array $orderNumbers, OwnershipContext $ownershipContext): void
    {
        if (!$this->elasticsearchHelper->allowIndexing()) {
            $this->decorated->replace($orderNumbers, $ownershipContext);

            return;
        }

        $originalOrderNumbers = $this->orderNumberRepository->fetchAllProductsForExport($ownershipContext);
        $this->decorated->replace($orderNumbers, $ownershipContext);

        $ids = array_merge(
            $this->extractProductIds($originalOrderNumbers),
            $this->extractProductIds($orderNumbers)
        );

        $this->index(...$ids);
    }

    /**
     * @internal
     */
    protected function extractProductIds(array $oderNumberEntites): array
    {
        return array_map(function (OrderNumberEntity $orderNumber) {
            return $orderNumber->productId->getValue();
        }, $oderNumberEntites);
    }

    public function create(CrudServiceRequest $request, OwnershipContext $ownershipContext): OrderNumberEntity
    {
        $orderNumber = $this->decorated->create($request, $ownershipContext);

        $this->index($orderNumber->productId->getValue());

        return $orderNumber;
    }

    /**
     * @internal
     */
    protected function index(string ...$productIds): void
    {
        if (!$this->elasticsearchHelper->allowIndexing()) {
            return;
        }

        $event = new ProductIndexerEvent($productIds, [], [], $this->contextProvider->getContext());

        $this->eventDispatcher->dispatch($event);
    }

    /**
     * @internal
     */
    protected function getPreviousOrderNumber(
        CrudServiceRequest $request,
        OwnershipContext $ownershipContext
    ): OrderNumberEntity {
        $data = $request->getFilteredData();
        $data['contextOwnerId'] = $ownershipContext->contextOwnerId;

        $orderNumber = new OrderNumberEntity();
        $orderNumber->setData($data);

        return $this->orderNumberRepository->fetchOneById($orderNumber->id, $ownershipContext);
    }
}
