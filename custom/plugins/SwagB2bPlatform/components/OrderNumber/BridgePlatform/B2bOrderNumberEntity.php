<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\BridgePlatform;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;

class B2bOrderNumberEntity extends Entity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $customOrderNumber;

    /**
     * @var string
     */
    protected $productId;

    /**
     * @var int
     */
    protected $contextOwnerId;

    /**
     * @var ProductEntity
     */
    protected $product;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCustomOrderNumber(): string
    {
        return $this->customOrderNumber;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getContextOwnerId(): int
    {
        return $this->contextOwnerId;
    }

    public function getProduct(): ProductEntity
    {
        return $this->product;
    }

    public function setProduct(ProductEntity $product): void
    {
        $this->product = $product;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setCustomOrderNumber(string $customOrderNumber): void
    {
        $this->customOrderNumber = $customOrderNumber;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    public function setContextOwnerId(int $contextOwnerId): void
    {
        $this->contextOwnerId = $contextOwnerId;
    }
}
