<?php declare(strict_types=1);

namespace Shopware\B2B\OrderNumber\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class OrderNumberApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorMail}/orderNumber',
                'b2b_order_number.api_order_number_controller',
                'getList',
                ['debtorMail'],
            ],

            [
                'POST',
                '/debtor/{debtorMail}/orderNumber/{orderNumber}/custom/{customOrderNumber}',
                'b2b_order_number.api_order_number_controller',
                'create',
                ['debtorMail', 'orderNumber', 'customOrderNumber'],
            ],

            [
                'PUT',
                '/debtor/{debtorMail}/orderNumber/{orderNumber}/custom/{customOrderNumber}',
                'b2b_order_number.api_order_number_controller',
                'update',
                ['debtorMail', 'orderNumber', 'customOrderNumber'],
            ],

            [
                'DELETE',
                '/debtor/{debtorMail}/custom/{customOrderNumber}',
                'b2b_order_number.api_order_number_controller',
                'remove',
                ['debtorMail', 'customOrderNumber'],
            ],

            [
                'GET',
                '/debtor/{debtorMail}/orderNumber/{orderNumber}',
                'b2b_order_number.api_order_number_controller',
                'get',
                ['debtorMail', 'orderNumber'],
            ],
        ];
    }
}
