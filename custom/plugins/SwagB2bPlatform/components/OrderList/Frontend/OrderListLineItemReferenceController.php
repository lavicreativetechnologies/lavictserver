<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Frontend;

use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceService;
use Shopware\B2B\Order\Framework\OrderLineItemReferenceCrudService;
use Shopware\B2B\OrderList\Framework\OrderListRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function array_merge;
use function count;

class OrderListLineItemReferenceController
{
    /**
     * @var LineItemReferenceService
     */
    private $lineItemReferenceService;

    /**
     * @var OrderLineItemReferenceCrudService
     */
    private $lineItemReferenceCrudService;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $lineItemReferenceRepository;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(
        OrderListRepository $orderListRepository,
        LineItemListRepository $lineItemListRepository,
        LineItemReferenceRepositoryInterface $lineItemReferenceRepository,
        GridHelper $gridHelper,
        OrderLineItemReferenceCrudService $lineItemReferenceCrudService,
        LineItemReferenceService $lineItemReferenceService,
        CurrencyService $currencyService,
        AuthenticationService $authenticationService
    ) {
        $this->orderListRepository = $orderListRepository;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->lineItemReferenceRepository = $lineItemReferenceRepository;
        $this->gridHelper = $gridHelper;
        $this->lineItemReferenceCrudService = $lineItemReferenceCrudService;
        $this->lineItemReferenceService = $lineItemReferenceService;
        $this->currencyService = $currencyService;
        $this->authenticationService = $authenticationService;
    }

    public function indexAction(Request $request): array
    {
        $orderListId = $request->requireIdValue('orderlist');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $listId = $orderList->listId;

        $lineItemList = $this->lineItemListRepository
            ->fetchOneListById($listId, $currencyContext, $ownershipContext);

        $searchStruct = new LineItemReferenceSearchStruct();

        $this->gridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        $searchStruct->offset = 0;
        $searchStruct->limit = PHP_INT_MAX;

        $lineItemList = $this->lineItemReferenceService
            ->fetchLineItemListProductNames($lineItemList);

        $totalCount = $this->lineItemReferenceRepository
            ->fetchTotalCount($listId, $searchStruct);

        $currentPage = $this->gridHelper
            ->getCurrentPage($request);

        $maxPage = $this->gridHelper
            ->getMaxPage($totalCount);

        $gridState = $this->gridHelper
            ->getGridState($request, $searchStruct, $lineItemList->references, $currentPage, $maxPage);

        $validationResponse = $this->gridHelper
            ->getValidationResponse('lineItemReference');

        return array_merge(
            [
                'gridState' => $gridState,
                'listId' => $listId->getValue(),
                'orderList' => $orderList,
            ],
            $this->getHeaderData($lineItemList),
            $validationResponse
        );
    }

    public function newAction(Request $request): array
    {
        $orderListId = $request->requireIdValue('orderlist');
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $validationResponse = $this->gridHelper
            ->getValidationResponse('lineItemReference');

        return array_merge(
            ['orderList' => $orderList],
            $validationResponse
        );
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function createAction(Request $request): void
    {
        try {
            $this->createLineItemReferenceFromRequest($request);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
            throw new B2bControllerForwardException('new', null, ['orderlist' => $request->requireParam('orderlist')]);
        }

        throw new B2bControllerForwardException('index', null, ['orderlist' => $request->requireParam('orderlist')]);
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function updateAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderListId = $request->requireIdValue('orderlist');
        $listId = $request->requireIdValue('listId');

        $crudRequest = $this->lineItemReferenceCrudService
            ->createUpdateCrudRequest($request->getPost());

        try {
            $this->lineItemReferenceCrudService
                ->updateLineItem($listId, $crudRequest, $currencyContext, $ownershipContext);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
        }

        throw new B2bControllerForwardException('index', null, ['orderlist' => $orderListId->getValue()]);
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function removeAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderListId = $request->getIdValue('orderlist');
        $lineItemId = $request->getIdValue('lineItemId');

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $this->lineItemReferenceCrudService
            ->deleteLineItem($orderList->listId, $lineItemId, $currencyContext, $ownershipContext);

        throw new B2bControllerForwardException('index', null, ['id' => $orderList->id->getValue()]);
    }

    public function sortAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderListId = $request->requireIdValue('orderlist');
        $itemIdOne = $request->requireIdValue('itemIdOne');
        $itemIdTwo = $request->requireIdValue('itemIdTwo');

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $this->lineItemReferenceCrudService
                ->flipLineItemSorting($itemIdOne, $itemIdTwo, $currencyContext);

        throw new B2bControllerForwardException('index', null, ['id' => $orderList->id->getValue()]);
    }

    /**
     * @internal
     */
    protected function createLineItemReferenceFromRequest(Request $request): LineItemReference
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $orderListId = $request->requireIdValue('orderlist');

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $this->orderListRepository->setDefaultOrderList($orderList, $currencyContext, $ownershipContext);

        $crudRequest = $this->lineItemReferenceCrudService
            ->createCreateCrudRequest($request->getPost());

        return $this->lineItemReferenceCrudService
            ->addLineItem($orderList->listId, $crudRequest, $currencyContext, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function getHeaderData(LineItemList $list): array
    {
        return [
            'itemCount' => count($list->references),
            'amountNet' => $list->amountNet,
            'amount' => $list->amount,
        ];
    }
}
