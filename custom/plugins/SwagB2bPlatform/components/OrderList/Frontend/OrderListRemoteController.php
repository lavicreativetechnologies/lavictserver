<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Frontend;

use Shopware\B2B\Acl\Framework\AclOperationNotPermittedException;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\OrderList\Framework\OrderListRepository;
use Shopware\B2B\OrderList\Framework\OrderListService;
use Shopware\B2B\OrderList\Framework\RemoteBoxService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function array_merge;
use function count;

class OrderListRemoteController extends RemoteBoxController
{
    /**
     * @var string
     */
    private $action;

    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OrderListService
     */
    private $orderListService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var RemoteBoxService
     */
    private $remoteBoxService;

    public function __construct(
        OrderListRepository $orderListRepository,
        CurrencyService $currencyService,
        RemoteBoxService $remoteBoxService,
        OrderListService $orderListService,
        AuthenticationService $authenticationService
    ) {
        parent::__construct(
            $remoteBoxService,
            $authenticationService,
            $currencyService,
            $orderListRepository
        );
        $this->orderListRepository = $orderListRepository;
        $this->currencyService = $currencyService;
        $this->orderListService = $orderListService;
        $this->authenticationService = $authenticationService;
        $this->remoteBoxService = $remoteBoxService;
    }

    public function remoteListAction(Request $request): array
    {
        $referenceNumber = $request->requireParam('referenceNumber');

        $orderLists = $this->getOrderListsFromRequest($request);

        $defaultOrderList = null;
        try {
            $defaultOrderList = $this->orderListRepository
                ->fetchDefaultOrderList($this->currencyService->createCurrencyContext(), $this->getOwnershipContext());
        } catch (NotFoundException $e) {
            // nth
        }

        return [
            'referenceNumber' => $referenceNumber,
            'b2b_quantity' => (int) $request->getParam('b2b_quantity'),
            'orderLists' => $orderLists,
            'orderListId' => $request->getParam('orderListId'),
            'message' => $request->getParam('message'),
            'validationExceptions' => $request->getParam('validationExceptions'),
            'type' => $request->getParam('type'),
            'defaultOrderList' => $defaultOrderList,
        ];
    }

    public function remoteListCartAction(Request $request): array
    {
        $cartId = $request->requireParam('cartId');

        $orderLists = $this->getOrderListsFromRequest($request);

        return [
            'cartId' => $cartId,
            'orderLists' => $orderLists,
            'orderListId' => $request->getParam('orderListId'),
            'message' => $request->getParam('message'),
            'validationExceptions' => $request->getParam('validationExceptions'),
            'type' => $request->getParam('type'),
        ];
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function processAddProductsToOrderListAction(Request $request): void
    {
        $this->action = 'remoteList';

        $responseData = [
            'referenceNumber' => $request->getParam('referenceNumber'),
            'orderListId' => $request->requireIdValue('orderlist'),
        ];

        $lineItemList = $this
            ->createLineItemListFromRequest($request, $responseData);

        if ($this->getListingActionName() === 'remoteList' && count($lineItemList->references) > 0) {
            $responseData = array_merge(
                $responseData,
                ['b2b_quantity' => $lineItemList->references[0]->quantity]
            );
        }

        try {
            $this->orderListService->addListThroughLineItemList(
                $request->requireIdValue('orderlist'),
                $lineItemList,
                $this->currencyService->createCurrencyContext(),
                $this->getOwnershipContext()
            );
        } catch (NotFoundException | AclOperationNotPermittedException $e) {
            throw new B2bControllerForwardException(
                $this->getListingActionName(),
                $this->getControllerName(),
                array_merge($responseData, ['message' => ['key' => 'NoOrderList', 'type' => 'error']])
            );
        } catch (ValidationException $e) {
            $this->remoteBoxService->addError($e);
            throw new B2bControllerForwardException(
                $this->getListingActionName(),
                $this->getControllerName(),
                array_merge($responseData, $this->getMessages(false))
            );
        }

        $responseData = array_merge($responseData, $this->getMessages());

        throw new B2bControllerForwardException(
            $this->getListingActionName(),
            $this->getControllerName(),
            $responseData
        );
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function addListThroughCartAction(Request $request): void
    {
        $this->action = 'remoteListCart';

        $ownershipContext = $this->getOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $responseData = [
            'cartId' => $request->requireParam('cartId'),
            'orderListId' => $request->requireIdValue('orderlist'),
        ];

        try {
            $orderList = $this->orderListRepository
                ->fetchOneById($responseData['orderListId'], $currencyContext, $ownershipContext);
        } catch (NotFoundException $e) {
            throw new B2bControllerForwardException(
                $this->getListingActionName(),
                $this->getControllerName(),
                array_merge($responseData, ['message' => ['key' => 'NoOrderList', 'type' => 'error']])
            );
        }

        try {
            $this->orderListService->addListThroughCart(
                $orderList,
                $responseData['cartId'],
                $this->authenticationService->getIdentity()->getOwnershipContext(),
                $currencyContext
            );
        } catch (ValidationException $e) {
            $this->remoteBoxService->addError($e);
            throw new B2bControllerForwardException(
                $this->getListingActionName(),
                $this->getControllerName(),
                array_merge($responseData, $this->getMessages(false))
            );
        }

        throw new B2bControllerForwardException(
            $this->getListingActionName(),
            $this->getControllerName(),
            array_merge($responseData, ['message' => ['key' => 'Success', 'type' => 'success']])
        );
    }

    protected function getControllerName(): string
    {
        return 'b2borderlistremote';
    }

    protected function getListingActionName(): string
    {
        return $this->action;
    }
}
