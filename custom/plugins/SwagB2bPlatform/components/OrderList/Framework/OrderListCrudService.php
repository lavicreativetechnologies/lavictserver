<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Acl\Framework\AclUnsupportedContextException;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\Shop\Framework\StorageInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OrderListCrudService extends AbstractCrudService
{
    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    /**
     * @var OrderListValidationService
     */
    private $validationService;

    /**
     * @var AclRepository
     */
    private $aclRepository;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var OrderListRelationRepositoryInterface
     */
    private $orderListRelationRepository;

    /**
     * @var StorageInterface
     */
    private $session;

    public function __construct(
        OrderListRepository $orderListRepository,
        OrderListValidationService $validationService,
        AclRepository $aclRepository,
        LineItemListRepository $lineItemListRepository,
        LineItemListService $lineItemListService,
        OrderListRelationRepositoryInterface $orderListRelationRepository,
        StorageInterface $session
    ) {
        $this->orderListRepository = $orderListRepository;
        $this->validationService = $validationService;
        $this->aclRepository = $aclRepository;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->lineItemListService = $lineItemListService;
        $this->orderListRelationRepository = $orderListRelationRepository;
        $this->session = $session;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'name',
                'budgetId',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'name',
                'budgetId',
            ]
        );
    }

    public function create(
        CrudServiceRequest $request,
        OwnershipContext $ownershipContext
    ): OrderListEntity {
        $data = $request->getFilteredData();
        $data['contextOwnerId'] = $ownershipContext->contextOwnerId;

        $lineItemList = new LineItemList();
        $lineItemList->contextOwnerId = $ownershipContext->contextOwnerId;

        $lineItemList = $this->lineItemListRepository
            ->addList($lineItemList, $ownershipContext);

        $data['listId'] = $lineItemList->id;

        $orderList = new OrderListEntity();
        $orderList->setData($data);

        $validation = $this->validationService
            ->createInsertValidation($orderList);

        $this->testValidation($orderList, $validation);

        $this->lineItemListService
            ->updateListPrices($lineItemList, $ownershipContext);

        $orderList = $this->orderListRepository
            ->addOrderList($orderList, $ownershipContext);

        $orderList->lineItemList = $lineItemList;

        try {
            $this->aclRepository->allow(
                $ownershipContext,
                $orderList->id
            );
        } catch (AclUnsupportedContextException $e) {
            return $orderList;
        }

        return $orderList;
    }

    public function produceCart(IdValue $orderListId, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): void
    {
        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $this->session->set('b2bBudgetId', $orderList->budgetId);

        $this->lineItemListService
            ->produceCart($orderList->listId, $currencyContext, $ownershipContext);

        $list = $this->lineItemListRepository
            ->fetchOneListById($orderList->listId, $currencyContext, $ownershipContext);

        $this->orderListRelationRepository->addOrderListToCartAttribute($list, $orderList->name);
    }

    public function duplicate(
        IdValue $orderListId,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext
    ): OrderListEntity {
        $orderList = $this->orderListRepository->fetchOneById($orderListId, $currencyContext, $ownershipContext);
        $orderList->contextOwnerId = $ownershipContext->contextOwnerId;
        $orderList->id = IdValue::null();

        $validation = $this->validationService
            ->createInsertValidation($orderList);

        $this->testValidation($orderList, $validation);

        $list = $this->createNewLineItemCopy($orderList->listId, $ownershipContext, $currencyContext);

        $this->addDuplicatedOrderList($orderList, $list, $ownershipContext);

        try {
            $this->aclRepository->allow(
                $ownershipContext,
                $orderList->id
            );
        } catch (AclUnsupportedContextException $e) {
            return $orderList;
        }

        return $orderList;
    }

    /**
     * @param LineItemReference[] $references
     */
    protected function resetReferenceItemsIds(array $references): array
    {
        foreach ($references as $reference) {
            $reference->id = IdValue::null();
        }

        return $references;
    }

    /**
     * @internal
     */
    protected function createNewLineItemCopy(
        IdValue $id,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext
    ): LineItemList {
        $list = $this->lineItemListRepository->fetchOneListById($id, $currencyContext, $ownershipContext);
        $list->contextOwnerId = $ownershipContext->contextOwnerId;
        $list->id = IdValue::null();

        $this->resetReferenceItemsIds($list->references);

        return $this->lineItemListService->createListThroughListObject($list, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function addDuplicatedOrderList(
        OrderListEntity $orderList,
        LineItemList $list,
        OwnershipContext $ownershipContext
    ): OrderListEntity {
        $orderList->listId = $list->id;

        return $this->orderListRepository->addOrderList($orderList, $ownershipContext);
    }

    public function update(
        CrudServiceRequest $request,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OrderListEntity {
        $data = $request->getFilteredData();

        $orderList = $this->orderListRepository
            ->fetchOneById($request->requireIdValue('id'), $currencyContext, $ownershipContext);

        $orderList->setData($data);

        $this->session->set('b2bBudgetId', $orderList->budgetId);

        $validation = $this->validationService
            ->createUpdateValidation($orderList);

        $this->testValidation($orderList, $validation);

        return $this->orderListRepository
            ->updateOrderList($orderList, $ownershipContext);
    }

    public function remove(CrudServiceRequest $request, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): OrderListEntity
    {
        $orderListId = $request->requireIdValue('id');
        $orderList = $this->orderListRepository->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $this->orderListRepository
            ->removeOrderList($orderList, $ownershipContext);

        return $orderList;
    }
}
