<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;
use function sprintf;

class OrderListRepository implements GridRepository
{
    const TABLE_NAME = 'b2b_order_list';
    const TABLE_ALIAS = 'orderList';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        LineItemListRepository $lineItemListRepository,
        AclReadHelper $aclReadHelper
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->aclReadHelper = $aclReadHelper;
    }

    public function fetchList(
        OrderListSearchStruct $searchStruct,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext
    ): array {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId);

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper
            ->applySearchStruct($searchStruct, $query);

        $rawOrderListData = $query->execute()->fetchAll();

        $orderLists = [];
        foreach ($rawOrderListData as $orderListData) {
            $orderList = new OrderListEntity();
            $orderList->fromDatabaseArray($orderListData);
            $orderList->lineItemList = $this->lineItemListRepository
                ->fetchOneListById($orderList->listId, $currencyContext, $ownershipContext);

            $orderLists[] = $orderList;
        }

        return $orderLists;
    }

    public function fetchTotalCount(OrderListSearchStruct $searchStruct, OwnershipContext $ownershipContext): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(' . self::TABLE_ALIAS . '.id)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId);

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $this->dbalHelper
            ->applyFilters($searchStruct, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    /**
     * @throws NotFoundException
     */
    public function fetchOneById(IdValue $id, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): OrderListEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->andWhere(self::TABLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameter('id', $id->getStorageValue())
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue());

        $orderListData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$orderListData) {
            throw new NotFoundException(sprintf('Order list not found for %s', $id));
        }

        $orderList = new OrderListEntity();
        $orderList->fromDatabaseArray($orderListData);
        $orderList->lineItemList = $this->lineItemListRepository
            ->fetchOneListById($orderList->listId, $currencyContext, $ownershipContext);

        return $orderList;
    }

    /**
     * @throws CanNotInsertExistingRecordException
     */
    public function addOrderList(OrderListEntity $orderList, OwnershipContext $ownershipContext): OrderListEntity
    {
        if (!$orderList->isNew()) {
            throw new CanNotInsertExistingRecordException('The order list provided already exists');
        }

        $this->connection
            ->insert(
                self::TABLE_NAME,
                array_merge(
                    $orderList->toDatabaseArray(),
                    ['context_owner_id' => $ownershipContext->contextOwnerId]
                )
            );

        $orderList->id = IdValue::create($this->connection->lastInsertId());

        return $orderList;
    }

    /**
     * @throws CanNotUpdateExistingRecordException
     */
    public function updateOrderList(OrderListEntity $orderList, OwnershipContext $ownershipContext): OrderListEntity
    {
        if ($orderList->isNew()) {
            throw new CanNotUpdateExistingRecordException('Order list is not yet created.');
        }

        $this->connection
            ->update(
                self::TABLE_NAME,
                $orderList->toDatabaseArray(),
                [
                    'id' => $orderList->id->getStorageValue(),
                    'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
                ]
            );

        return $orderList;
    }

    /**
     * @throws CanNotRemoveExistingRecordException
     * @return OrderListEntity
     */
    public function removeOrderList(OrderListEntity $orderList, OwnershipContext $ownershipContext)
    {
        if ($orderList->isNew()) {
            throw new CanNotRemoveExistingRecordException('The order list provided does not exist');
        }

        $numDeleted = $this->connection->delete(
            self::TABLE_NAME,
            [
                'id' => $orderList->id->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );

        if (!$numDeleted) {
            throw new NotFoundException('The order list provided does not exist');
        }

        $this->lineItemListRepository->removeLineItemListById($orderList->listId, $ownershipContext);

        $orderList->id = IdValue::null();
        $orderList->listId = IdValue::null();

        return $orderList;
    }

    /**
     * @return string query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'name',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [
            self::TABLE_ALIAS . '.list_id IN (SELECT %1$s.list_id FROM b2b_line_item_reference %1$s WHERE %1$s.list_id = orderList.list_id AND %2$s)' => ['reference_number'],
        ];
    }

    public function fetchDefaultOrderList(CurrencyContext $currencyContext, OwnershipContext $ownershipContext): OrderListEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->join(self::TABLE_ALIAS, 'b2b_default_order_list', '`default`', 'default.order_list_id = orderList.id')
            ->where('default.identity_id = :identityId')
            ->andWhere(self::TABLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameters([
                'identityId' => $ownershipContext->identityId->getValue(),
                'contextOwnerId' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]);

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $orderListData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$orderListData) {
            throw new NotFoundException('Default order list not found');
        }

        $orderList = new OrderListEntity();
        $orderList->fromDatabaseArray($orderListData);
        $orderList->lineItemList = $this->lineItemListRepository
            ->fetchOneListById($orderList->listId, $currencyContext, $ownershipContext);

        return $orderList;
    }

    protected function deleteDefaultOrderListById(IdValue $orderListId): void
    {
        $this->connection->delete(
            'b2b_default_order_list',
            ['order_list_id' => $orderListId->getStorageValue()]
        );
    }

    public function setDefaultOrderList(OrderListEntity $orderList, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): void
    {
        $oldDefault = null;
        try {
            $oldDefault = $this->fetchDefaultOrderList($currencyContext, $ownershipContext);
        } catch (NotFoundException $e) {
            // nth
        }

        if ($oldDefault) {
            if ($oldDefault->id->equals($orderList->id)) {
                return;
            }

            $this->deleteDefaultOrderListById($oldDefault->id);
        }

        // identity_id could contain int and uuid, so the hex value is persisted
        $this->connection->createQueryBuilder()
            ->insert('b2b_default_order_list')
            ->values(
                [
                    'order_list_id' => ':orderListId',
                    'identity_id' => ':identityId',
                ]
            )
            ->setParameter('identityId', $ownershipContext->identityId->getValue())
            ->setParameter('orderListId', $orderList->id->getStorageValue())
            ->execute();
    }
}
