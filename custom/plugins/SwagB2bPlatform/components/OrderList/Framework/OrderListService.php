<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Framework;

use Shopware\B2B\Acl\Framework\AclAccessWriter;
use Shopware\B2B\Common\File\CsvWriter;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceService;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceValidationService;
use Shopware\B2B\LineItemList\Framework\LineItemType\LineItemType;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function count;
use function file_get_contents;
use function preg_replace;
use function tempnam;
use function unlink;

class OrderListService
{
    const INVALID_FILENAME_EXPORT_FIX_REGEX = '/[^[:alnum:]\_\-\s]/';

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var OrderListRepository
     */
    private $orderListRepository;

    /**
     * @var LineItemReferenceValidationService
     */
    private $lineItemReferenceValidationService;

    /**
     * @var CsvWriter
     */
    private $csvWriter;

    /**
     * @var LineItemReferenceService
     */
    private $lineItemReferenceService;

    /**
     * @var AclAccessWriter
     */
    private $aclAccessWriter;

    public function __construct(
        LineItemListService $lineItemListService,
        OrderListRepository $orderListRepository,
        LineItemReferenceValidationService $lineItemReferenceValidationService,
        CsvWriter $csvWriter,
        LineItemReferenceService $lineItemReferenceService,
        AclAccessWriter $aclAccessWriter
    ) {
        $this->lineItemListService = $lineItemListService;
        $this->orderListRepository = $orderListRepository;
        $this->lineItemReferenceValidationService = $lineItemReferenceValidationService;
        $this->csvWriter = $csvWriter;
        $this->lineItemReferenceService = $lineItemReferenceService;
        $this->aclAccessWriter = $aclAccessWriter;
    }

    public function addListThroughCart(
        OrderListEntity $orderList,
        string $cartId,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext
    ): OrderListEntity {
        $list = $this->lineItemListService->createListThroughCartId($cartId, $ownershipContext);

        $list->id = $orderList->listId;

        $this->lineItemListService->updateListReferences($list, $currencyContext, $ownershipContext);

        $this->lineItemListService
            ->updateListPrices($list, $ownershipContext);

        return $orderList;
    }

    public function addListThroughLineItemList(
        IdValue $orderListId,
        LineItemList $list,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OrderListEntity {
        $this->aclAccessWriter
            ->testUpdateAllowed($ownershipContext, $orderListId);

        $orderList = $this->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $this->orderListRepository
            ->setDefaultOrderList($orderList, $currencyContext, $ownershipContext);

        $list->id = $orderList->listId;

        $this->lineItemListService
            ->updateListReferences($list, $currencyContext, $ownershipContext);

        $this->lineItemListService
            ->updateListPrices($list, $ownershipContext);

        return $orderList;
    }

    public function createReferenceFromProductRequest(array $product): LineItemReference
    {
        $reference = new LineItemReference();

        $reference->referenceNumber = $product['referenceNumber'];
        $reference->quantity = (int) $product['quantity'];
        $reference->mode = LineItemType::create(0);
        if (ShopwareVersion::isPlatform()) {
            $reference->mode = LineItemType::create('product');
        }

        $validator = $this->lineItemReferenceValidationService
            ->createReferenceValidation($reference);

        $violations = $validator->getViolations();

        if (count($violations)) {
            throw new ValidationException($reference, $violations, 'Validation violations detected, can not proceed:', 400);
        }

        return $reference;
    }

    public function getCsvExportData(IdValue $orderListId, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): array
    {
        $orderList = $this
            ->orderListRepository
            ->fetchOneById($orderListId, $currencyContext, $ownershipContext);

        $exportData = $this->fetchExportData($orderList);

        $name = tempnam('/tmp', 'csv');

        $this->csvWriter->write($exportData, $name);
        $csv = file_get_contents($name);

        unlink($name);

        $orderListName = $orderList->name;

        $orderListName = preg_replace(self::INVALID_FILENAME_EXPORT_FIX_REGEX, '_', $orderListName);

        return [
            'name' => $orderListName,
            'csv' => $csv,
        ];
    }

    protected function fetchExportData(OrderListEntity $orderList): array
    {
        $data = [];
        foreach ($orderList->lineItemList->references as $lineItemReference) {
            $data[] = [
                $lineItemReference->referenceNumber,
                $lineItemReference->quantity,
            ];
        }

        return $data;
    }

    public function getUnavailableReferences(OrderListEntity $orderList): array
    {
        $orderList->lineItemList = $this->lineItemReferenceService
            ->fetchLineItemListProductNames($orderList->lineItemList);

        $references = $this->lineItemReferenceService
            ->getReferencesWithStock($orderList->lineItemList->references);

        $unavailable = [];
        /** @var LineItemReference $reference */
        foreach ($references as $reference) {
            if ($reference->quantity <= $reference->inStock | !$reference->isLastStock) {
                if ($reference->minPurchase && $reference->quantity < $reference->minPurchase) {
                    $reference->unavailableBecauseOfMinPurchase = true;
                    $unavailable[] = $reference;
                } elseif ($reference->maxPurchase && $reference->quantity > $reference->maxPurchase) {
                    $reference->unavailableBecauseOfMaxPurchase = true;
                    $unavailable[] = $reference;
                }
            } elseif ($reference->isLastStock && $reference->quantity > $reference->inStock) {
                $reference->unavailableBecauseOfStock = true;
                $unavailable[] = $reference;
            }
        }

        return $unavailable;
    }
}
