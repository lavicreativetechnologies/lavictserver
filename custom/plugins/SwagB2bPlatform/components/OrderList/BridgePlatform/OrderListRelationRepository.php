<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\BridgePlatform;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemListCartService;
use Shopware\B2B\LineItemList\BridgePlatform\OrderListExtension;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\OrderList\Framework\OrderListRelationRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use function array_unique;
use function count;
use function implode;

class OrderListRelationRepository implements OrderListRelationRepositoryInterface
{
    const TABLE_NAME = 'b2b_line_item_reference';
    const TABLE_ALIAS = 'lineItemReference';
    const TABLE_NAME_LIST = 'b2b_line_item_list';
    const TABLE_ALIAS_LIST = 'lineItemList';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LineItemListCartService
     */
    private $lineItemListCartService;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var CartService
     */
    private $cartService;

    public function __construct(
        Connection $connection,
        LineItemListCartService $lineItemListCartService,
        ContextProvider $contextProvider,
        CartService $cartService
    ) {
        $this->connection = $connection;
        $this->lineItemListCartService = $lineItemListCartService;
        $this->contextProvider = $contextProvider;
        $this->cartService = $cartService;
    }

    /**
     * @throws NotFoundException
     */
    public function fetchOrderListNameForListId(IdValue $listId, OwnershipContext $ownershipContext): string
    {
        $queryBuilder = $this->createBaseQueryBuilder($ownershipContext);

        $orderList = $queryBuilder
            ->select(self::TABLE_ALIAS . '.order_list_name')
            ->andWhere(self::TABLE_ALIAS . '.list_id = :listId')
            ->andWhere(self::TABLE_ALIAS . '.order_list_name IS NOT NULL')
            ->setParameter('listId', $listId->getStorageValue())
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);

        if (count($orderList) === 0) {
            throw new NotFoundException('Order has no b2b order list');
        }

        return implode(', ', array_unique($orderList));
    }

    /**
     * @throws NotFoundException
     */
    public function fetchOrderListNameForPositionNumber(
        IdValue $listId,
        string $productNumber,
        OwnershipContext $ownershipContext
    ): string {
        $queryBuilder = $this->createBaseQueryBuilder($ownershipContext);

        $orderList = $queryBuilder
            ->select(self::TABLE_ALIAS . '.order_list_name')
            ->andWhere(self::TABLE_ALIAS . '.list_id = :listId')
            ->andWhere(self::TABLE_ALIAS . '.reference_number = :productNumber')
            ->andWhere(self::TABLE_ALIAS . '.order_list_name IS NOT NULL')
            ->andWhere(self::TABLE_ALIAS . '.mode = 0')
            ->setParameter('listId', $listId->getStorageValue())
            ->setParameter('productNumber', $productNumber)
            ->execute()
            ->fetchColumn();

        if (!$orderList) {
            throw new NotFoundException('Position has no b2b order list');
        }

        return (string) $orderList;
    }

    public function addOrderListToCartAttribute(LineItemList $list, string $orderListName): void
    {
        $cart = $this->lineItemListCartService->getCartByList($list);

        // TODO: Find unified solution for adding data to cart
        $data = new OrderListExtension();
        $data->orderListName = $orderListName;
        $data->orderListId = $list->id;

        $cart->addExtension('b2b', $data);

        $this->cartService->recalculate($cart, $this->contextProvider->getSalesChannelContext());
    }

    /**
     * @internal
     */
    protected function createBaseQueryBuilder(OwnershipContext $ownershipContext): QueryBuilder
    {
        return $this->connection->createQueryBuilder()
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, self::TABLE_NAME_LIST, self::TABLE_ALIAS_LIST, self::TABLE_ALIAS . '.list_id = ' . self::TABLE_ALIAS_LIST . '.id')
            ->where(self::TABLE_ALIAS_LIST . '.context_owner_id = :ownerId')
            ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());
    }
}
