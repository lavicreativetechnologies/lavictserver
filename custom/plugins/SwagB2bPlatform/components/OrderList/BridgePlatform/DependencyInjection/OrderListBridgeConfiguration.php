<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\BridgePlatform\DependencyInjection;

use Shopware\B2B\Cart\Bridge\DependencyInjection\CartBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\LineItemList\Framework\DependencyInjection\LineItemListFrameworkConfiguration;
use Shopware\B2B\Order\Framework\DependencyInjection\OrderFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderListBridgeConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new OrderFrameworkConfiguration(),
            CartBridgeConfiguration::create(),
            new LineItemListFrameworkConfiguration(),
        ];
    }
}
