<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class OrderListApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list',
                'b2b_order_list.api_order_list_controller',
                'getList',
                [],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_controller',
                'get',
                ['orderListId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/order_list',
                'b2b_order_list.api_order_list_controller',
                'create',
                [],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_controller',
                'update',
                ['orderListId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_controller',
                'remove',
                ['orderListId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}/items',
                'b2b_order_list.api_order_list_controller',
                'addItems',
                ['orderListId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}/items',
                'b2b_order_list.api_order_list_controller',
                'removeItems',
                ['orderListId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}/items',
                'b2b_order_list.api_order_list_controller',
                'updateItems',
                ['orderListId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}/items',
                'b2b_order_list.api_order_list_controller',
                'getItems',
                ['orderListId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list/{orderListId}/copy',
                'b2b_order_list.api_order_list_controller',
                'duplicate',
                ['orderListId'],
            ],
        ];
    }
}
