<?php declare(strict_types=1);

namespace Shopware\B2B\OrderList\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class OrderListContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}',
                'b2b_order_list.api_order_list_contact_controller',
                'getAllAllowed',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/grant',
                'b2b_order_list.api_order_list_contact_controller',
                'getAllGrant',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_contact_controller',
                'getAllowed',
                ['contactEmail', 'orderListId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_contact_controller',
                'allow',
                ['contactEmail', 'orderListId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/order_list/{orderListId}/grant',
                'b2b_order_list.api_order_list_contact_controller',
                'allowGrant',
                ['contactEmail', 'orderListId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/allow',
                'b2b_order_list.api_order_list_contact_controller',
                'multipleAllow',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/deny',
                'b2b_order_list.api_order_list_contact_controller',
                'multipleDeny',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/order_list_contact/{contactEmail}/order_list/{orderListId}',
                'b2b_order_list.api_order_list_contact_controller',
                'deny',
                ['contactEmail', 'orderListId'],
            ],
        ];
    }
}
