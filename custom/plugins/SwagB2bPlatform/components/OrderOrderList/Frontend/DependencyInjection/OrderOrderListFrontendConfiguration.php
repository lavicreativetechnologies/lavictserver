<?php declare(strict_types=1);

namespace Shopware\B2B\OrderOrderList\Frontend\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Order\Frontend\DependencyInjection\OrderFrontendConfiguration;
use Shopware\B2B\OrderOrderList\Framework\DependencyInjection\OrderOrderListFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderOrderListFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new OrderOrderListFrameworkConfiguration(),
            new OrderFrontendConfiguration(),
        ];
    }
}
