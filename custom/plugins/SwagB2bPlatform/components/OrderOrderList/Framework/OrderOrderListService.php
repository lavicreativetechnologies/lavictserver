<?php declare(strict_types=1);

namespace Shopware\B2B\OrderOrderList\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\LineItemList\Framework\LineItemType\ProductLineItemType;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\OrderList\Framework\OrderListCrudService;
use Shopware\B2B\OrderList\Framework\OrderListEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OrderOrderListService
{
    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var OrderListCrudService
     */
    private $orderListCrudService;

    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    public function __construct(
        LineItemListRepository $lineItemListRepository,
        LineItemListService $lineItemListService,
        OrderListCrudService $orderListCrudService
    ) {
        $this->lineItemListRepository = $lineItemListRepository;
        $this->orderListCrudService = $orderListCrudService;
        $this->lineItemListService = $lineItemListService;
    }

    public function createOrderListFromOrderContext(
        OrderContext $orderContext,
        OwnershipContext $ownershipContext,
        CurrencyContext $currencyContext
    ): OrderListEntity {
        $newRecordRequest = $this->orderListCrudService
            ->createNewRecordRequest(['name' => $orderContext->orderNumber ?? '-']);

        $orderedList = $this->lineItemListRepository
            ->fetchOneListById($orderContext->listId, $currencyContext, $ownershipContext);

        $this->resetListStorageIds($orderedList);
        $this->filterProducts($orderedList);

        $orderListEntity = $this->orderListCrudService
            ->create($newRecordRequest, $ownershipContext);

        $orderListEntity->lineItemList->references = $orderedList->references;

        $this->lineItemListService
            ->updateListReferences($orderListEntity->lineItemList, $currencyContext, $ownershipContext);

        return $orderListEntity;
    }

    /**
     * @internal
     * @param $lineItemList
     */
    protected function resetListStorageIds(LineItemList $lineItemList): void
    {
        $lineItemList->id = IdValue::null();
        foreach ($lineItemList->references as $reference) {
            $reference->id = IdValue::null();
        }
    }

    /**
     * @internal
     * @param $lineItemList
     */
    protected function filterProducts(LineItemList $lineItemList): void
    {
        $productReferences = [];

        foreach ($lineItemList->references as $reference) {
            if (!$reference->mode instanceof ProductLineItemType) {
                continue;
            }
            $productReferences[] = $reference;
        }

        $lineItemList->references = $productReferences;
    }
}
