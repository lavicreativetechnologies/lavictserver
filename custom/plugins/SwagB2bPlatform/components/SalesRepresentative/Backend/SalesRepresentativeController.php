<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Backend;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeSearchStruct;
use function json_decode;

class SalesRepresentativeController
{
    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var SalesRepresentativeClientRepository
     */
    private $clientRepository;

    public function __construct(
        GridHelper $gridHelper,
        SalesRepresentativeClientRepository $clientRepository
    ) {
        $this->gridHelper = $gridHelper;
        $this->clientRepository = $clientRepository;
    }

    public function assignClientAction(Request $request): array
    {
        $clientIds = json_decode($request->requireParam('clientIds'));

        if (!$clientIds) {
            return ['success' => false];
        }

        $salesRepresentativeId = $request->requireIdValue('salesRepresentativeId');
        $clientIds = IdValue::createMultiple($clientIds);
        $assign = (bool) $request->getParam('assign');

        if ($assign) {
            $this->clientRepository->addClientsToSalesRepresentative($clientIds, $salesRepresentativeId);
        } else {
            $this->clientRepository->removeClientsFromSalesRepresentative($clientIds, $salesRepresentativeId);
        }

        return ['success' => true];
    }

    public function listAction(Request $request): array
    {
        return $this->modelAction($request);
    }

    public function modelAction(Request $request): array
    {
        $salesRepresentativeId = $request->requireIdValue('salesRepresentativeId');
        $selectedOnly = (bool) $request->getParam('selectedOnly');

        $searchStruct = new SalesRepresentativeSearchStruct();

        $this->gridHelper->extractSearchDataInBackend($request, $searchStruct);

        if ($selectedOnly) {
            $clients = $this->clientRepository->fetchClientList($searchStruct, $salesRepresentativeId);
            $totalCount = $this->clientRepository->fetchTotalCount($searchStruct, $salesRepresentativeId);
        } else {
            $clients = $this->clientRepository->fetchUnselectedClientList($searchStruct, $salesRepresentativeId);
            $totalCount = $this->clientRepository->fetchUnselectedTotalCount($searchStruct, $salesRepresentativeId);
        }

        return ['data' => $clients, 'count' => $totalCount];
    }
}
