<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Contact\BridgePlatform\ContactAuthenticationIdentityLoader;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\SalesRepresentative\Framework\ClientIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;

class ClientContactAuthenticationIdentityLoader extends ContactAuthenticationIdentityLoader implements ClientIdentityLoaderInterface
{
    private const ALIAS = 'contact';

    public function __construct(ContactRepository $contactRepository, DebtorRepositoryInterface $debtorRepository)
    {
        parent::__construct($contactRepository, $debtorRepository);
    }

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function addClientData(QueryBuilder $query): void
    {
        $query->leftJoin(
            StoreFrontAuthenticationRepository::TABLE_ALIAS,
            '(
                SELECT
                    contact.id,
                    contact.firstname,
                    contact.lastname,
                    contact.salutation,
                    contact.email,
                    contact.active,
                    address.phone_number as phone,
                    address.company as company,
                    "contact" as `type`,
                    customer.email as `shopOwnerEmail`,
                    customer.customer_number as `customerNumber`
                FROM b2b_debtor_contact as contact
                INNER JOIN b2b_store_front_auth auth
                    ON contact.context_owner_id = auth.id
                INNER JOIN customer
                    ON auth.provider_context = hex(customer.id)
                INNER JOIN customer_address address
                    ON customer.default_billing_address_id = address.id
            )',
            self::ALIAS,
            StoreFrontAuthenticationRepository::TABLE_ALIAS . '.provider_context = contact.id AND '
            . StoreFrontAuthenticationRepository::TABLE_ALIAS . '.provider_key = :contactProviderKey'
        )
            ->setParameter('contactProviderKey', ContactRepository::class);
    }
}
