<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity;

class SalesRepresentativeEntityFactory
{
    private const PASSWORD_DEFAULT_ENCODER = 'bcrypt';

    public function create(array $salesRepCustomerData): SalesRepresentativeEntity
    {
        $salesRep = new SalesRepresentativeEntity();
        $salesRep->id = IdValue::create($salesRepCustomerData['id']);
        $salesRep->password = $salesRepCustomerData['password'] ?? $salesRepCustomerData['legacy_password'];
        $salesRep->encoder = $salesRepCustomerData['legacy_encoder'] ?? self::PASSWORD_DEFAULT_ENCODER;
        $salesRep->email = $salesRepCustomerData['email'];
        $salesRep->active = (bool) $salesRepCustomerData['active'];
        $salesRep->paymentID = IdValue::create($salesRepCustomerData['default_payment_method_id']);
        $salesRep->firstlogin = $salesRepCustomerData['first_login'];
        $salesRep->lastlogin = $salesRepCustomerData['last_login'];
        $salesRep->newsletter = (int) $salesRepCustomerData['newsletter'];
        $salesRep->customergroup = IdValue::create($salesRepCustomerData['customer_group_id']);
        $salesRep->language = IdValue::create($salesRepCustomerData['language_id']);
        $salesRep->default_billing_address_id = IdValue::create($salesRepCustomerData['default_billing_address_id']);
        $salesRep->default_shipping_address_id = IdValue::create($salesRepCustomerData['default_shipping_address_id']);
        $salesRep->title = $salesRepCustomerData['title'];
        $salesRep->salutation = $salesRepCustomerData['salutation_display_name'];
        $salesRep->firstName = $salesRepCustomerData['first_name'];
        $salesRep->lastName = $salesRepCustomerData['last_name'];
        $salesRep->birthday = $salesRepCustomerData['birthday'];
        $salesRep->customernumber = $salesRepCustomerData['customer_number'];

        return $salesRep;
    }
}
