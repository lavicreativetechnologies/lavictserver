<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\BridgePlatform;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeEntity;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\BridgePlatform\TranslatedFieldQueryExtenderTrait;
use function sprintf;

class SalesRepresentativeRepository implements GridRepository, SalesRepresentativeRepositoryInterface
{
    use TranslatedFieldQueryExtenderTrait;

    const TABLE_NAME = 'customer';

    const TABLE_ALIAS = 'customer';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SalesRepresentativeEntityFactory
     */
    private $salesRepresentativeEntityFactory;

    public function __construct(
        Connection $connection,
        ContextProvider $contextProvider,
        SalesRepresentativeEntityFactory $salesRepresentativeEntityFactory
    ) {
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
        $this->salesRepresentativeEntityFactory = $salesRepresentativeEntityFactory;
    }

    public function fetchOneById(IdValue $id): SalesRepresentativeEntity
    {
        $salesRepCustomerData = $this->createBaseQuery()
            ->andWhere(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue())
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$salesRepCustomerData) {
            throw new NotFoundException(sprintf('Sales representative not found for %s', $id->getValue()));
        }

        return $this->salesRepresentativeEntityFactory->create($salesRepCustomerData);
    }

    public function fetchOneByEmail(string $email): SalesRepresentativeEntity
    {
        $salesRepCustomerData = $this->createBaseQuery()
            ->andWhere(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $email)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$salesRepCustomerData) {
            throw new NotFoundException(sprintf('Sales representative not found for %s', $email));
        }

        return $this->salesRepresentativeEntityFactory->create($salesRepCustomerData);
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [
            '%2$s' => [
                'contact' => [
                    'phone',
                    'email',
                    'firstname',
                    'lastname',
                    'shopOwnerEmail',
                    'type',
                    'customerNumber',
                    'company',
                ],
                'debtor' => [
                    'phone',
                    'email',
                    'firstname',
                    'lastname',
                    'shopOwnerEmail',
                    'type',
                    'customerNumber',
                    'company',
                ],
            ],
        ];
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * @internal
     */
    protected function createBaseQuery(): QueryBuilder
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_customer_data', 'b2bCustomerData', self::TABLE_ALIAS . '.id = b2bCustomerData.customer_id')
            ->innerJoin(self::TABLE_ALIAS, 'salutation', 'salutation', self::TABLE_ALIAS . '.salutation_id = salutation.id')
            ->where('b2bCustomerData.is_sales_representative = 1');

        $this->addTranslatedFieldSelect(
            $query,
            'display_name',
            'salutation_display_name',
            'salutation_translation',
            'salutation',
            'salutation',
            'id',
            $this->contextProvider->getSalesChannelContext()
        );

        return $query;
    }
}
