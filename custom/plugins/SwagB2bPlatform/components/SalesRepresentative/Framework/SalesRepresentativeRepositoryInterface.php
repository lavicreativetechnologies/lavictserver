<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Common\IdValue;

interface SalesRepresentativeRepositoryInterface
{
    public function fetchOneById(IdValue $id): SalesRepresentativeEntity;

    public function fetchOneByEmail(string $email): SalesRepresentativeEntity;

    public function getTableName(): string;
}
