<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\Framework\ContactIdentity;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserLoginServiceInterface;
use function property_exists;

class SalesRepresentativeService
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var UserLoginServiceInterface
     */
    private $loginService;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var SalesRepresentativeClientRepository
     */
    protected $clientRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        AuthStorageAdapterInterface $authStorageAdapter,
        UserLoginServiceInterface $loginService,
        LoginContextService $loginContextService,
        SalesRepresentativeClientRepository $clientRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->loginService = $loginService;
        $this->loginContextService = $loginContextService;
        $this->clientRepository = $clientRepository;
    }

    public function setClientIdentity(Identity $clientIdentity, SalesRepresentativeIdentity $salesRepresentativeIdentity): void
    {
        if ($clientIdentity instanceof DebtorIdentity) {
            $modifiedIdentity = $this->getDebtorSalesRepresentativeIdentity($salesRepresentativeIdentity, $clientIdentity);
        } elseif ($clientIdentity instanceof ContactIdentity) {
            $modifiedIdentity = $this->getContactSalesRepresentativeIdentity($salesRepresentativeIdentity, $clientIdentity);
        } else {
            throw new NotFoundException('Client identity not found.');
        }

        $this->authStorageAdapter->setIdentity($modifiedIdentity);
    }

    /**
     * @internal
     */
    protected function getDebtorSalesRepresentativeIdentity(
        SalesRepresentativeIdentity $salesRepresentativeIdentity,
        Identity $clientIdentity
    ): SalesRepresentativeDebtorIdentity {
        /** @var SalesRepresentativeEntity $entity */
        $entity = $salesRepresentativeIdentity->getEntity();

        /** @var DebtorEntity $clientEntity */
        $clientEntity = $clientIdentity->getEntity();

        $mediaId = null;
        if (property_exists($entity, 'mediaId')) {
            $mediaId = $entity->mediaId;
        }

        return new SalesRepresentativeDebtorIdentity(
            $salesRepresentativeIdentity->getAuthId(),
            $clientIdentity->getAuthId(),
            $clientIdentity->getId(),
            $clientIdentity->getTableName(),
            $clientEntity,
            $this->loginContextService->getAvatar($clientIdentity->getAuthId()),
            $mediaId,
            $clientIdentity->isApiUser()
        );
    }

    /**
     * @internal
     */
    protected function getContactSalesRepresentativeIdentity(
        SalesRepresentativeIdentity $salesRepresentativeIdentity,
        Identity $clientIdentity
    ): SalesRepresentativeContactIdentity {
        /** @var SalesRepresentativeEntity $entity */
        $entity = $salesRepresentativeIdentity->getEntity();

        /** @var ContactEntity $clientEntity */
        $clientEntity = $clientIdentity->getEntity();

        /** @var DebtorIdentity $debtorIdentity */
        $debtorIdentity = $this->authenticationService
            ->getIdentityByAuthId($clientIdentity->getOwnershipContext()->contextOwnerId);

        $mediaId = null;
        if (property_exists($entity, 'mediaId')) {
            $mediaId = $entity->mediaId;
        }

        return new SalesRepresentativeContactIdentity(
            $salesRepresentativeIdentity->getAuthId(),
            $clientIdentity->getAuthId(),
            $clientIdentity->getId(),
            $clientIdentity->getTableName(),
            $clientEntity,
            $debtorIdentity,
            $this->loginContextService->getAvatar($clientIdentity->getAuthId()),
            $mediaId
        );
    }

    /**
     * SalesRepresentativeIdentityInterface Param to prevent login without right identity
     */
    public function loginByAuthId(SalesRepresentativeIdentityInterface $identity, IdValue $id): void
    {
        $this->loginService->loginByAuthId($id);
    }

    public function setEntitiesToClients(array $clients): array
    {
        foreach ($clients as $client) {
            $client->entity = $this->authenticationService->getIdentityByAuthId($client->authId)->getEntity();
        }

        return $clients;
    }
}
