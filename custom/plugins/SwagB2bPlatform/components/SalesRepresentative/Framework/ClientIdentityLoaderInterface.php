<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;

interface ClientIdentityLoaderInterface extends AuthenticationIdentityLoaderInterface
{
    public function addClientData(QueryBuilder $query);

    public function getAlias(): string;
}
