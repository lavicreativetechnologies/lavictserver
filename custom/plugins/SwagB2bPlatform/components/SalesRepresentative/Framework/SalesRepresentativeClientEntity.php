<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function end;
use function explode;
use function get_class;
use function lcfirst;
use function str_replace;

class SalesRepresentativeClientEntity
{
    /**
     * @var IdValue
     */
    public $authId;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var bool
     */
    public $active;

    /**
     * @var string
     */
    public $company;

    /**
     * @var IdValue
     */
    public $salesRepresentativeId;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $shopOwnerEmail;

    /**
     * @var string
     */
    public $customerNumber;

    public function __construct(Identity $identity, AddressEntity $address, IdValue $salesRepresentativeId)
    {
        $postal = $identity->getPostalSettings();

        $this->authId = $identity->getAuthId();
        $this->firstName = $postal->firstName;
        $this->lastName = $postal->lastName;
        $this->email = $postal->email;
        $this->active = $identity->getLoginCredentials()->active;
        $this->phone = $address->phone;
        $this->company = $address->company;
        $this->salesRepresentativeId = $salesRepresentativeId;
        $this->type = $this->getTypeFromIdentity($identity);
        $this->shopOwnerEmail = $identity->getOwnershipContext()->shopOwnerEmail;
        $this->customerNumber = $identity->getOrderCredentials()->customerNumber;
    }

    /**
     * @internal
     */
    protected function getTypeFromIdentity(Identity $identity): string
    {
        $class = explode('\\', get_class($identity));
        $className = end($class);

        return lcfirst(str_replace('Identity', '', $className));
    }
}
