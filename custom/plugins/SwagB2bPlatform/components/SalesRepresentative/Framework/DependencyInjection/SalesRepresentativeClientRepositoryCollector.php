<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use function array_keys;

class SalesRepresentativeClientRepositoryCollector implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $identityChain = $container->findDefinition('b2b_sales_representative.client_identity_loader');
        $tags = $container->findTaggedServiceIds('b2b_sales_representative.client_authentication_loader');
        $serviceIds = array_keys($tags);

        $repositories = [];
        foreach ($serviceIds as $serviceId) {
            $repositories[] = new Reference($serviceId);
        }

        $identityChain->replaceArgument(0, $repositories);
    }
}
