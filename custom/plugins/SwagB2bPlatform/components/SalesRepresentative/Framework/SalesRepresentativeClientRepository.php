<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationRepository;
use function array_keys;
use function array_map;
use function array_pop;
use function sprintf;

class SalesRepresentativeClientRepository implements GridRepository
{
    const TABLE_NAME = 'b2b_sales_representative_clients';

    const TABLE_ALIAS = 'clients';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var ClientIdentityLoaderInterface
     */
    private $clientIdentityLoader;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    /**
     * @var StoreFrontAuthenticationRepository
     */
    private $authRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    public function __construct(
        Connection $connection,
        ClientIdentityLoaderInterface $clientIdentityLoader,
        DbalHelper $dbalHelper,
        LoginContextService $loginContextService,
        StoreFrontAuthenticationRepository $authRepository,
        AddressRepositoryInterface $addressRepository
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->clientIdentityLoader = $clientIdentityLoader;
        $this->loginContextService = $loginContextService;
        $this->authRepository = $authRepository;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param IdValue[] $clientIds
     * @return SalesRepresentativeClientEntity[]
     */
    public function fetchClientsByAuthIds(array $clientIds, IdValue $salesRepresentativeId): array
    {
        $clients = [];
        foreach ($clientIds as $authId) {
            try {
                $auth = $this->authRepository->fetchAuthenticationById($authId);

                $identity = $this->clientIdentityLoader
                    ->fetchIdentityByAuthentication($auth, $this->loginContextService);

                $client = new SalesRepresentativeClientEntity(
                    $identity,
                    $this->addressRepository->fetchOneById($identity->getMainBillingAddress()->id, $identity),
                    $salesRepresentativeId
                );
                $clients[] = $client;
            } catch (NotFoundException $e) {
                continue;
            }
        }

        return $clients;
    }

    /**
     * @return SalesRepresentativeClientEntity[]
     */
    public function fetchCombinedClientList(
        SalesRepresentativeSearchStruct $searchStruct,
        IdValue $salesRepresentativeId
    ): array {
        $query = $this->createCombinedBaseQuery($salesRepresentativeId);

        $this->clientIdentityLoader->addClientData($query);

        $this->reformatOrderBy($searchStruct, $query);
        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $query->select([
            StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id',
            self::TABLE_ALIAS . '.client_id IS NOT NULL AS ' . ClientIdentityChainLoader::CLIENT_COLUMN_PREFIX . 'assigned',
        ]);

        $combinedQueryResult = $query->execute()->fetchAll(PDO::FETCH_KEY_PAIR);
        $clientIds = IdValue::createMultiple(array_keys($combinedQueryResult));
        $clients = $this->fetchClientsByAuthIds($clientIds, $salesRepresentativeId);

        foreach ($clients as $client) {
            $client->assigned = ($combinedQueryResult[$client->authId->getValue()] ?? null) === '1';
        }

        return $clients;
    }

    public function fetchCombinedClientListTotalCount(IdValue $salesRepresentativeId, SalesRepresentativeSearchStruct $searchStruct): int
    {
        $query = $this->createCombinedBaseQuery($salesRepresentativeId);
        $this->clientIdentityLoader->addClientData($query);

        if ($searchStruct) {
            $this->dbalHelper->applyFilters($searchStruct, $query);
        }

        $query->select('COUNT(' . StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id)');

        return (int) $query->execute()->fetchColumn();
    }

    /**
     * @return SalesRepresentativeClientEntity[]
     */
    public function fetchClientList(
        SalesRepresentativeSearchStruct $searchStruct,
        IdValue $salesRepresentativeId
    ): array {
        $query = $this->createBaseQuery($salesRepresentativeId);

        $this->reformatOrderBy($searchStruct, $query);

        $this->clientIdentityLoader->addClientData($query);

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $clientIds = $query->execute()->fetchAll(PDO::FETCH_COLUMN);
        $clientIds = IdValue::createMultiple($clientIds);

        return $this->fetchClientsByAuthIds($clientIds, $salesRepresentativeId);
    }

    /**
     * @return SalesRepresentativeClientEntity[]
     */
    public function fetchUnselectedClientList(
        SalesRepresentativeSearchStruct $searchStruct,
        IdValue $salesRepresentativeId
    ): array {
        $query = $this->createUnselectedBaseQuery($salesRepresentativeId);

        $this->reformatOrderBy($searchStruct, $query);

        $this->clientIdentityLoader->addClientData($query);

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $this->filterUnselectedQuery($query);

        $clientIds = $query->execute()->fetchAll(PDO::FETCH_COLUMN);
        $clientIds = IdValue::createMultiple($clientIds);

        return $this->fetchClientsByAuthIds($clientIds, $salesRepresentativeId);
    }

    /**
     * @internal
     */
    protected function reformatOrderBy(SalesRepresentativeSearchStruct $searchStruct, QueryBuilder $query): void
    {
        if (!$searchStruct->orderBy) {
            $query->addOrderBy(StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id', 'DESC');

            return;
        }

        $searchStruct->orderBy = ClientIdentityChainLoader::CLIENT_COLUMN_PREFIX . $searchStruct->orderBy;
    }

    public function fetchTotalCount(
        SalesRepresentativeSearchStruct $searchStruct,
        IdValue $salesRepresentativeId
    ): int {
        $query = $this->createBaseQuery($salesRepresentativeId)
            ->select('COUNT(*)');

        $this->clientIdentityLoader->addClientData($query);

        $this->dbalHelper->applyFilters($searchStruct, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    public function fetchUnselectedTotalCount(
        SalesRepresentativeSearchStruct $searchStruct,
        IdValue $salesRepresentativeId
    ): int {
        $query = $this->createUnselectedBaseQuery($salesRepresentativeId)
            ->select('COUNT(*)');

        $this->clientIdentityLoader->addClientData($query);

        $this->dbalHelper->applyFilters($searchStruct, $query);

        $this->filterUnselectedQuery($query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    /**
     * @internal
     */
    protected function createCombinedBaseQuery(
        IdValue $salesRepresentativeId
    ): QueryBuilder {
        return $this->connection->createQueryBuilder()
            ->select([
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id',
                self::TABLE_ALIAS . '.client_id IS NOT NULL AS ' . ClientIdentityChainLoader::CLIENT_COLUMN_PREFIX . 'assigned',
            ])
            ->from(StoreFrontAuthenticationRepository::TABLE_NAME, StoreFrontAuthenticationRepository::TABLE_ALIAS)
            ->leftJoin(
                StoreFrontAuthenticationRepository::TABLE_ALIAS,
                self::TABLE_NAME,
                self::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id = ' . self::TABLE_ALIAS . '.client_id'
                . ' AND ' . self::TABLE_ALIAS . '.sales_representative_id = :salesRepId'
            )
            ->where(StoreFrontAuthenticationRepository::TABLE_ALIAS . '.provider_key != :providerKey')
            ->setParameter('providerKey', SalesRepresentativeRepositoryInterface::class)
            ->setParameter('salesRepId', $salesRepresentativeId->getStorageValue());
    }

    /**
     * @internal
     */
    protected function createBaseQuery(IdValue $salesRepresentativeId): QueryBuilder
    {
        return $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.client_id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(
                self::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_NAME,
                StoreFrontAuthenticationRepository::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id = ' . self::TABLE_ALIAS . '.client_id'
            )
            ->where(self::TABLE_ALIAS . '.sales_representative_id = :id')
            ->setParameter('id', $salesRepresentativeId->getStorageValue());
    }

    /**
     * @internal
     */
    protected function createUnselectedBaseQuery(
        IdValue $salesRepresentativeId
    ): QueryBuilder {
        return $this->connection->createQueryBuilder()
            ->select(StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id')
            ->from(StoreFrontAuthenticationRepository::TABLE_NAME, StoreFrontAuthenticationRepository::TABLE_ALIAS)
            ->where(StoreFrontAuthenticationRepository::TABLE_ALIAS . '.provider_key != :providerKey')
            ->setParameter('providerKey', SalesRepresentativeRepositoryInterface::class)
            ->setParameter('salesRepId', $salesRepresentativeId->getStorageValue());
    }

    /**
     * @internal
     */
    protected function filterUnselectedQuery(QueryBuilder $query): void
    {
        $query->leftJoin(
            StoreFrontAuthenticationRepository::TABLE_ALIAS,
            self::TABLE_NAME,
            self::TABLE_ALIAS,
            StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id = ' . self::TABLE_ALIAS . '.client_id'
            . ' AND ' . self::TABLE_ALIAS . '.sales_representative_id = :salesRepId'
        )
            ->andWhere(self::TABLE_ALIAS . '.sales_representative_id IS NULL');
    }

    public function fetchOneById(SalesRepresentativeEntity $salesRepresentativeEntity, IdValue $clientId): SalesRepresentativeClientEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.client_id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(
                self::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_NAME,
                StoreFrontAuthenticationRepository::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id = ' . self::TABLE_ALIAS . '.client_id'
            )
            ->where(self::TABLE_ALIAS . '.sales_representative_id = :id')
            ->andWhere(self::TABLE_ALIAS . '.client_id = :clientId')
            ->setParameter('id', $salesRepresentativeEntity->id->getStorageValue())
            ->setParameter('clientId', $clientId->getStorageValue());

        $this->clientIdentityLoader->addClientData($query);

        $clientIds = $query->execute()->fetchAll(PDO::FETCH_COLUMN);
        $clientIds = IdValue::createMultiple($clientIds);

        $clients = $this->fetchClientsByAuthIds($clientIds, $salesRepresentativeEntity->id);

        if (!$clients) {
            throw new NotFoundException(sprintf('Client with the id %s not found', $clientId));
        }

        return array_pop($clients);
    }

    public function isSalesRepresentativeClient(SalesRepresentativeEntity $salesRepresentativeEntity, IdValue $clientId): bool
    {
        $clientId = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.client_id')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(
                self::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_NAME,
                StoreFrontAuthenticationRepository::TABLE_ALIAS,
                StoreFrontAuthenticationRepository::TABLE_ALIAS . '.id = ' . self::TABLE_ALIAS . '.client_id'
            )
            ->where(self::TABLE_ALIAS . '.sales_representative_id = :id')
            ->andWhere(self::TABLE_ALIAS . '.client_id = :clientId')
            ->setParameter('id', $salesRepresentativeEntity->id->getStorageValue())
            ->setParameter('clientId', $clientId->getStorageValue())
            ->execute()
            ->fetchColumn();

        return (bool) $clientId;
    }

    public function deleteClientsBySalesRepresentativeId(IdValue $salesRepresentativeId): void
    {
        $this->connection->delete(self::TABLE_NAME, ['sales_representative_id' => $salesRepresentativeId->getStorageValue()]);
    }

    /**
     * @param IdValue[] $clientIds
     */
    public function addClientsToSalesRepresentative(array $clientIds, IdValue $salesRepresentativeId): void
    {
        $this->connection->beginTransaction();

        foreach ($clientIds as $clientId) {
            $this->connection->insert(self::TABLE_NAME, [
                'sales_representative_id' => $salesRepresentativeId->getStorageValue(),
                'client_id' => $clientId->getStorageValue(),
            ]);
        }

        $this->connection->commit();
    }

    /**
     * @param IdValue[] $clientIds
     */
    public function removeClientsFromSalesRepresentative(array $clientIds, IdValue $salesRepresentativeId): void
    {
        $clientIds = array_map(function (IdValue $value) { return $value->getStorageValue(); }, $clientIds);

        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('client_id IN (:clientIds)')
            ->andWhere('sales_representative_id = :salesRepresentativeId')
            ->setParameter('clientIds', $clientIds, $this->connection::PARAM_INT_ARRAY)
            ->setParameter('salesRepresentativeId', $salesRepresentativeId->getStorageValue())
            ->execute();
    }

    public function deleteSalesRepresentativeClient(
        SalesRepresentativeEntity $salesRepresentative,
        SalesRepresentativeClientEntity $clientEntity
    ): void {
        $this->connection->delete(
            self::TABLE_NAME,
            [
                'sales_representative_id' => $salesRepresentative->id->getStorageValue(),
                'client_id' => $clientEntity->authId->getStorageValue(),
            ]
        );
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        if (ShopwareVersion::isClassic()) {
            return [
                '%2$s' => [
                    'contact' => [
                        'phone',
                        'email',
                        'firstname',
                        'lastname',
                        'shopOwnerEmail',
                        'type',
                        'company',
                    ],
                    'debtor' => [
                        'phone',
                        'email',
                        'firstname',
                        'lastname',
                        'shopOwnerEmail',
                        'type',
                        'company',
                    ],
                ],
            ];
        }

        return [
            '%2$s' => [
                'contact' => [
                    'phone',
                    'email',
                    'firstname',
                    'lastname',
                    'shopOwnerEmail',
                    'type',
                    'customerNumber',
                    'company',
                ],
                'debtor' => [
                    'phone',
                    'email',
                    'firstname',
                    'lastname',
                    'shopOwnerEmail',
                    'type',
                    'customerNumber',
                    'company',
                ],
            ],
        ];
    }
}
