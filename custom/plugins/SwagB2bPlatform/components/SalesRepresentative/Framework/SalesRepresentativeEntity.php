<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Framework;

use Shopware\B2B\Debtor\Framework\DebtorEntity;

class SalesRepresentativeEntity extends DebtorEntity
{
}
