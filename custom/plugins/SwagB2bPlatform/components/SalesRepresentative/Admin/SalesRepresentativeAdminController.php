<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Admin;

use Doctrine\DBAL\Driver\PDOException;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeClientRepository;
use Shopware\B2B\SalesRepresentative\Framework\SalesRepresentativeSearchStruct;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use SwagB2bPlatform\Routing\Response\GridStateJsonResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class SalesRepresentativeAdminController
{
    /**
     * @var SalesRepresentativeClientRepository
     */
    private $clientRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var GridStateJsonResponseFactory
     */
    private $gridStateJsonResponseFactory;

    public function __construct(
        SalesRepresentativeClientRepository $clientRepository,
        GridHelper $gridHelper,
        GridStateJsonResponseFactory $gridStateJsonResponseFactory
    ) {
        $this->clientRepository = $clientRepository;
        $this->gridHelper = $gridHelper;
        $this->gridStateJsonResponseFactory = $gridStateJsonResponseFactory;
    }

    /**
     * @Route("/api/v{version}/_action/sales_representative/{salesRepresentativeId}/list-clients", name="api.action.b2bsalesrepresentative.list-clients.version", methods={"POST"})
     * @Route("/api/_action/sales_representative/{salesRepresentativeId}/list-clients", name="api.action.b2bsalesrepresentative.list-clients", methods={"POST"})
     */
    public function listAction(Request $request, Context $context): JsonResponse
    {
        $salesRepresentativeId = $request->requireIdValue('salesRepresentativeId');
        $searchStruct = new SalesRepresentativeSearchStruct();
        $this->gridHelper->extractSearchDataInAdmin($request, $searchStruct);

        $clients = $this->clientRepository->fetchCombinedClientList($searchStruct, $salesRepresentativeId);
        $clientsTotal = $this->clientRepository->fetchCombinedClientListTotalCount($salesRepresentativeId, $searchStruct);

        return $this->gridStateJsonResponseFactory->createResponse(
            $this->gridHelper->getGridState(
                $request,
                $searchStruct,
                $clients,
                $this->gridHelper->getMaxPage($clientsTotal),
                $this->gridHelper->getCurrentPage($request),
                $clientsTotal
            )
        );
    }

    /**
     * @Route("/api/v{version}/_action/sales_representative/{salesRepresentativeId}/assign-client", name="api.action.b2bsalesrepresentative.assign-client.version", methods={"POST"})
     * @Route("/api/_action/sales_representative/{salesRepresentativeId}/assign-client", name="api.action.b2bsalesrepresentative.assign-client", methods={"POST"})
     */
    public function assignAction(Request $request, Context $context): JsonResponse
    {
        $salesRepresentativeId = $request->requireIdValue('salesRepresentativeId');
        $clientId = $request->requireIdValue('clientAuthId');

        try {
            if ($request->getParam('assigned', false)) {
                $this->clientRepository->addClientsToSalesRepresentative([$clientId], $salesRepresentativeId);
            } else {
                $this->clientRepository->removeClientsFromSalesRepresentative([$clientId], $salesRepresentativeId);
            }
        } catch (PDOException $exception) {
            return new JsonResponse(['success' => false]);
        }

        return new JsonResponse(['success' => true]);
    }
}
