<?php declare(strict_types=1);

namespace Shopware\B2B\SalesRepresentative\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class SalesRepresentativeApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/sales_representative/{salesRepresentativeId}/client',
                'b2b_sales_representative.api_controller',
                'getList',
                ['salesRepresentativeId'],
            ],
            [
                'GET',
                '/sales_representative/{salesRepresentativeId}/client/{clientId}',
                'b2b_sales_representative.api_controller',
                'get',
                ['salesRepresentativeId', 'clientId'],
            ],
            [
                'DELETE',
                '/sales_representative/{salesRepresentativeId}/client/{clientId}',
                'b2b_sales_representative.api_controller',
                'deleteClient',
                ['salesRepresentativeId', 'clientId'],
            ],
            [
                'DELETE',
                '/sales_representative/{salesRepresentativeId}/client',
                'b2b_sales_representative.api_controller',
                'deleteClients',
                ['salesRepresentativeId'],
            ],
            [
                'POST',
                '/sales_representative/{salesRepresentativeId}/client/{clientId}',
                'b2b_sales_representative.api_controller',
                'assignClient',
                ['salesRepresentativeId', 'clientId'],
            ],
            [
                'POST',
                '/sales_representative/{salesRepresentativeId}/client',
                'b2b_sales_representative.api_controller',
                'assignClients',
                ['salesRepresentativeId'],
            ],
            [
                'PUT',
                '/sales_representative/{salesRepresentativeId}/client',
                'b2b_sales_representative.api_controller',
                'reAssignClients',
                ['salesRepresentativeId'],
            ],
        ];
    }
}
