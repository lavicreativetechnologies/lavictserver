<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Bridge\DependencyInjection;

use Shopware\B2B\AuditLog\BridgePlatform\DependencyInjection\AuditLogBridgeConfiguration as PlatformAuditLogBridgeConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AuditLogBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformAuditLogBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [];
    }
}
