<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

class AuditLogValueOrderCommentEntity extends AuditLogValueDiffEntity
{
    public function getTemplateName(): string
    {
        return 'OrderClearanceComment';
    }
}
