<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class AuditLogService
{
    /**
     * @var IdValue
     */
    private $auditLogWriteReference;

    /**
     * @var AuditLogAuthorService
     */
    private $auditLogAuthorService;

    /**
     * @var AuditLogRepository
     */
    private $auditLogRepository;

    /**
     * @var AuditLogIndexRepository
     */
    private $auditLogIndexRepository;

    public function __construct(
        AuditLogAuthorService $auditLogAuthorService,
        AuditLogRepository $auditLogRepository,
        AuditLogIndexRepository $auditLogIndexRepository
    ) {
        $this->auditLogAuthorService = $auditLogAuthorService;
        $this->auditLogRepository = $auditLogRepository;
        $this->auditLogIndexRepository = $auditLogIndexRepository;
    }

    /**
     * @param AuditLogIndexEntity[] $auditLogIndex
     * @throws \Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException
     */
    public function createAuditLog(
        AuditLogEntity $auditLogEntity,
        Identity $identity,
        array $auditLogIndex
    ): AuditLogEntity {
        $authorEntity = $this->auditLogAuthorService
            ->createAuthorEntityFromIdentity($identity);

        $auditLogEntity->authorHash = $authorEntity->hash;

        if ($this->auditLogWriteReference) {
            $auditLogEntity->writeWithId = $this->auditLogWriteReference;
        }

        $auditLogEntity = $this->auditLogRepository
            ->createAuditLog($auditLogEntity, $auditLogIndex);

        $this->createAuditLogIndex($auditLogEntity->id, $auditLogIndex);

        if (!$this->auditLogWriteReference) {
            $this->auditLogWriteReference = $auditLogEntity->id;
        }

        return $auditLogEntity;
    }

    public function createBackendAuditLog(
        AuditLogEntity $auditLogEntity,
        array $auditLogIndex
    ): AuditLogEntity {
        $authorEntity = $this->auditLogAuthorService->createBackendAuthorEntity();

        $auditLogEntity->authorHash = $authorEntity->hash;

        if ($this->auditLogWriteReference) {
            $auditLogEntity->writeWithId = $this->auditLogWriteReference;
        }

        $auditLogEntity = $this->auditLogRepository
            ->createAuditLog($auditLogEntity, $auditLogIndex);

        $this->createAuditLogIndex($auditLogEntity->id, $auditLogIndex);

        if (!$this->auditLogWriteReference) {
            $this->auditLogWriteReference = $auditLogEntity->id;
        }

        return $auditLogEntity;
    }

    /**
     * @internal
     */
    protected function createAuditLogIndex(IdValue $auditLogId, array $auditLogIndexes): void
    {
        foreach ($auditLogIndexes as $index) {
            if (!$index instanceof AuditLogIndexEntity) {
                throw new IsNoAuditLogIndexEntityException('Provided entity is not instance of AuditLogIndexEntity');
            }

            $index->auditLogId = $auditLogId;
            $this->auditLogIndexRepository->createAuditLogIndex($index);
        }
    }

    public function resetAuditLogWriteReference(): void
    {
        $this->auditLogWriteReference = null;
    }
}
