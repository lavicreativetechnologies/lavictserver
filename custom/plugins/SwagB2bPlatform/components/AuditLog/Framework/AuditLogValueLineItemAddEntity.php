<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\Framework;

use Shopware\B2B\ProductName\Framework\ProductNameAware;

class AuditLogValueLineItemAddEntity extends AuditLogValueDiffEntity implements ProductNameAware
{
    /**
     * @var string
     */
    public $orderNumber;

    /**
     * @var string
     */
    public $productName;

    public function getTemplateName(): string
    {
        return 'OrderClearanceLineItemAdd';
    }

    public function isChanged(): bool
    {
        return true;
    }

    /**
     * @param string $name
     */
    public function setProductName(string $name = null): void
    {
        $this->productName = $name;
    }

    public function getProductOrderNumber(): string
    {
        return $this->orderNumber;
    }
}
