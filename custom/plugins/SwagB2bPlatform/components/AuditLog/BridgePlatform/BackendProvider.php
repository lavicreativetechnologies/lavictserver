<?php declare(strict_types=1);

namespace Shopware\B2B\AuditLog\BridgePlatform;

use Shopware\B2B\AuditLog\Framework\AuditLogAuthorEntity;
use Shopware\B2B\AuditLog\Framework\BackendProviderInterface;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\WrongContextException;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\User\UserEntity;

class BackendProvider implements BackendProviderInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var EntityRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        ContextProvider $contextProvider,
        EntityRepositoryInterface $userRepository
    ) {
        $this->contextProvider = $contextProvider;
        $this->userRepository = $userRepository;
    }

    public function getBackendUser(): AuditLogAuthorEntity
    {
        if (!$this->contextProvider->isAdminApiSource()) {
            throw new WrongContextException(
                'Your context source has to be an instance of ' . AdminApiSource::class
            );
        }

        $context = $this->contextProvider->getContext();

        $user = $this->getUserByContext($context);

        $authorEntity = new AuditLogAuthorEntity();
        $authorEntity->firstName = $user->getFirstName();
        $authorEntity->lastName = $user->getLastName();
        $authorEntity->email = $user->getEmail();
        $authorEntity->isApi = false;
        $authorEntity->isBackend = true;

        return $authorEntity;
    }

    /**
     * @internal
     */
    protected function getUserByContext(Context $context): UserEntity
    {
        $userId = $context->getSource()->getUserId();

        $user = $this->userRepository
            ->search(new Criteria([$userId]), $context)->first();

        if (!$user) {
            throw new NotFoundException('User not found with the given user id: ' . $userId);
        }

        return $user;
    }
}
