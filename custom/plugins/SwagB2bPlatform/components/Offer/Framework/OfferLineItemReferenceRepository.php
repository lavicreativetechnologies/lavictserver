<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceRepositoryInterface;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Shopware\B2B\ProductName\Framework\ProductNameService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_walk;
use function sprintf;

class OfferLineItemReferenceRepository implements GridRepository
{
    const TABLE_NAME = 'b2b_line_item_reference';

    const TABLE_ALIAS = 'lineItem';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var LineItemReferenceRepositoryInterface
     */
    private $lineItemReferenceRepository;

    /**
     * @var ProductNameService
     */
    private $productNameService;

    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        LineItemReferenceRepositoryInterface $lineItemReferenceRepository,
        ProductNameService $productNameService,
        ProductProviderInterface $productProvider
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->lineItemReferenceRepository = $lineItemReferenceRepository;
        $this->productNameService = $productNameService;
        $this->productProvider = $productProvider;
    }

    /**
     * @return OfferLineItemReferenceEntity[]
     */
    public function fetchAllForList(IdValue $listId, OwnershipContext $ownershipContext): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.list_id = :listId')
            ->orderBy(self::TABLE_ALIAS . '.id')
            ->setParameter('listId', $listId->getStorageValue());

        $this->filterByContextOwner($query, $ownershipContext);

        $lineItemData = $query->execute()->fetchAll();

        $lineItems = [];

        foreach ($lineItemData as $rawLineItem) {
            $lineItem = new OfferLineItemReferenceEntity();
            $lineItem->fromDatabaseArray($rawLineItem);

            $lineItems[] = $lineItem;
        }

        $this->productProvider->setMaxMinAndStepsForItems($lineItems);
        $this->productNameService->translateProductNames($lineItems);

        return $lineItems;
    }

    /**
     * @return OfferLineItemReferenceEntity[]
     */
    public function fetchList(
        IdValue $listId,
        LineItemReferenceSearchStruct $searchStruct,
        OwnershipContext $ownershipContext
    ): array {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('listId', $listId->getStorageValue());

        $this->filterByContextOwner($queryBuilder, $ownershipContext);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = 'coalesce(' . self::TABLE_ALIAS . '.sort, ' . self::TABLE_ALIAS . '.id)';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $queryBuilder);

        $rawItemReferences = $queryBuilder
            ->execute()
            ->fetchAll();

        $offerLineItemReferences = [];
        foreach ($rawItemReferences as $rawItemReference) {
            $offerLineItemReference = (new OfferLineItemReferenceEntity())
                ->fromDatabaseArray($rawItemReference);
            $offerLineItemReferences[] = $offerLineItemReference;
        }

        $this->productProvider->setMaxMinAndStepsForItems($offerLineItemReferences);
        $this->productNameService->translateProductNames($offerLineItemReferences);

        return $offerLineItemReferences;
    }

    /**
     * @return OfferLineItemReferenceEntity[]
     */
    public function fetchListByLineItemList(
        LineItemList $list,
        LineItemReferenceSearchStruct $searchStruct,
        OwnershipContext $ownershipContext
    ): array {
        $offerLineItemReferences = $this->fetchList($list->id, $searchStruct, $ownershipContext);

        $this->updateOfferItemsWithCustomReferenceNumbers($list, $offerLineItemReferences);

        return $offerLineItemReferences;
    }

    /**
     * @param OfferLineItemReferenceEntity[] $offerItems
     * @internal
     */
    protected function updateOfferItemsWithCustomReferenceNumbers(LineItemList $list, array $offerItems): void
    {
        $customReferenceNumbers = [];
        array_walk($list->references, function ($reference) use (&$customReferenceNumbers): void {
            if ($reference->referenceNumber !== $reference->customReferenceNumber) {
                $customReferenceNumbers[$reference->referenceNumber] = $reference->customReferenceNumber;
            }
        });

        if (!$customReferenceNumbers) {
            return;
        }

        foreach ($offerItems as $item) {
            if (!isset($customReferenceNumbers[$item->referenceNumber])) {
                continue;
            }

            $item->customReferenceNumber = $customReferenceNumbers[$item->referenceNumber];
        }
    }

    /**
     * @throws NotFoundException
     */
    public function fetchReferenceById(IdValue $id, OwnershipContext $ownershipContext): OfferLineItemReferenceEntity
    {
        $rawReferenceDataQuery = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue());

        $this->filterByContextOwner($rawReferenceDataQuery, $ownershipContext);

        $rawReferenceData = $rawReferenceDataQuery->execute()->fetch();

        if (!$rawReferenceData) {
            throw new NotFoundException(sprintf('reference with id %s not found', $id->getValue()));
        }

        $offerLineItemReference = new OfferLineItemReferenceEntity();
        $offerLineItemReference->fromDatabaseArray($rawReferenceData);

        $this->productProvider->setMaxMinAndStepsForItems([$offerLineItemReference]);
        $this->productNameService->translateProductNames([$offerLineItemReference]);

        return $offerLineItemReference;
    }

    public function fetchTotalCount(
        IdValue $listId,
        LineItemReferenceSearchStruct $searchStruct,
        OwnershipContext $ownershipContext
    ): int {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('listId', $listId->getStorageValue());

        $this->filterByContextOwner($queryBuilder, $ownershipContext);
        $this->dbalHelper->applyFilters($searchStruct, $queryBuilder);

        return (int) $queryBuilder
            ->execute()
            ->fetchColumn();
    }

    public function hasReference(string $referenceNumber, IdValue $listId): bool
    {
        return $this->lineItemReferenceRepository->hasReference($referenceNumber, $listId);
    }

    /**
     * @throws NotFoundException
     */
    public function fetchReferenceByReferenceNumberAndListIdAndQuantity(
        string $referenceNumber,
        IdValue $listId,
        int $quantity,
        OwnershipContext $ownershipContext
    ): OfferLineItemReferenceEntity {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.reference_number = :referenceNumber')
            ->andWhere(self::TABLE_ALIAS . '.list_id = :listId')
            ->andWhere(self::TABLE_ALIAS . '.quantity = :quantity')
            ->setParameter('referenceNumber', $referenceNumber)
            ->setParameter('listId', $listId->getStorageValue())
            ->setParameter('quantity', $quantity);

        $this->filterByContextOwner($queryBuilder, $ownershipContext);

        $rawReferenceData = $queryBuilder->execute()->fetch();

        if (!$rawReferenceData) {
            throw new NotFoundException(
                "Reference for list with id {$listId} and refrence number {$referenceNumber} not found"
            );
        }

        $lineItemReference = new OfferLineItemReferenceEntity();
        $lineItemReference->fromDatabaseArray($rawReferenceData);

        $this->productProvider->setMaxMinAndStepsForItems([$lineItemReference]);
        $this->productNameService->translateProductNames([$lineItemReference]);

        return $lineItemReference;
    }

    /**
     * @throws NotFoundException
     */
    public function fetchReferenceByReferenceNumberAndListId(
        string $referenceNumber,
        IdValue $listId,
        OwnershipContext $ownershipContext
    ): OfferLineItemReferenceEntity {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.reference_number = :referenceNumber')
            ->andWhere(self::TABLE_ALIAS . '.list_id = :listId')
            ->setParameter('referenceNumber', $referenceNumber)
            ->setParameter('listId', $listId->getStorageValue());

        $this->filterByContextOwner($queryBuilder, $ownershipContext);

        $rawReferenceData = $queryBuilder->execute()->fetch();

        if (!$rawReferenceData) {
            throw new NotFoundException(
                "Reference for list with id {$listId} and refrence number {$referenceNumber} not found"
            );
        }

        $lineItemReference = new OfferLineItemReferenceEntity();
        $lineItemReference->fromDatabaseArray($rawReferenceData);

        $this->productProvider->setMaxMinAndStepsForItems([$lineItemReference]);
        $this->productNameService->translateProductNames([$lineItemReference]);

        return $lineItemReference;
    }

    /**
     * @param LineItemReference|OfferLineItemReferenceEntity $reference
     */
    public function addReference(IdValue $listId, OfferLineItemReferenceEntity $reference): OfferLineItemReferenceEntity
    {
        $lineItem = $this->lineItemReferenceRepository->addReference($listId, $reference);

        $this->productProvider->setMaxMinAndStepsForItems([$lineItem]);
        $this->productNameService->translateProductNames([$lineItem]);

        return $lineItem;
    }

    public function setDefaultPricesForDiscountForLineItemReferenceId(
        IdValue $id,
        OwnershipContext $ownershipContext
    ): OfferLineItemReferenceEntity {
        $lineItemReference = $this->fetchReferenceById($id, $ownershipContext);

        $this->connection->update(
            self::TABLE_NAME,
            [
                'discount_amount_net' => $lineItemReference->amountNet,
                'discount_amount' => $lineItemReference->amount,
            ],
            ['id' => $lineItemReference->id->getStorageValue()]
        );

        $lineItemReference->discountAmount = $lineItemReference->amount;
        $lineItemReference->discountAmountNet = $lineItemReference->amountNet;

        return $lineItemReference;
    }

    public function updateReference(IdValue $listId, LineItemReference $lineItemReference): void
    {
        $this->lineItemReferenceRepository->updateReference($listId, $lineItemReference);
    }

    public function removeReference(IdValue $id): void
    {
        $this->lineItemReferenceRepository->removeReference($id);
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [
            'reference_number',
            'comment',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }

    protected function filterByContextOwner(QueryBuilder $builder, OwnershipContext $ownershipContext): void
    {
        $builder->innerJoin(
            self::TABLE_ALIAS,
            'b2b_line_item_list',
            'lineItemList',
            self::TABLE_ALIAS . '.list_id = lineItemList.id'
        )->andWhere('lineItemList.context_owner_id = :ownerId')
         ->setParameter('ownerId', $ownershipContext->contextOwnerId->getStorageValue());
    }
}
