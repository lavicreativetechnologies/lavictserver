<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\ProductName\Framework\ProductNameAware;
use Shopware\B2B\ProductName\Framework\ProductNameService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OfferLineItemReferenceService
{
    /**
     * @var OfferLineItemReferenceRepository
     */
    private $lineItemReferenceRepository;

    /**
     * @var ProductNameService
     */
    private $productNameService;

    /**
     * @param ProductNameAware $productNameAware
     */
    public function __construct(
        OfferLineItemReferenceRepository $lineItemReferenceRepository,
        ProductNameService $productNameAware
    ) {
        $this->lineItemReferenceRepository = $lineItemReferenceRepository;
        $this->productNameService = $productNameAware;
    }

    /**
     * @return OfferLineItemReferenceEntity[]
     */
    public function fetchLineItemsReferencesWithProductNames(
        IdValue $listId,
        LineItemReferenceSearchStruct $searchStruct,
        OwnershipContext $ownershipContext
    ): array {
        $lineItemReferences = $this->lineItemReferenceRepository
            ->fetchList($listId, $searchStruct, $ownershipContext);

        $this->productNameService->translateProductNames($lineItemReferences);

        return $lineItemReferences;
    }
}
