<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OfferCrudService extends AbstractCrudService
{
    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferValidationService
     */
    private $validationService;

    /**
     * @var OfferLineItemListRepository
     */
    private $offerLineItemListRepository;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var OfferLineItemListService
     */
    private $offerLineItemListService;

    public function __construct(
        OfferRepository $offerRepository,
        OfferValidationService $validationService,
        OfferLineItemListRepository $offerLineItemListRepository,
        OfferService $offerService,
        OfferLineItemListService $offerLineItemListService
    ) {
        $this->offerRepository = $offerRepository;
        $this->validationService = $validationService;
        $this->offerLineItemListRepository = $offerLineItemListRepository;
        $this->offerService = $offerService;
        $this->offerLineItemListService = $offerLineItemListService;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'orderContextId',
                'listId',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'discountValueNet',
                'percentageDiscount',
            ]
        );
    }

    public function update(
        CrudServiceRequest $request,
        CurrencyContext $currencyContext,
        Identity $identity,
        bool $isBackend
    ): OfferEntity {
        $data = $request->getFilteredData();

        $offer = $this->offerRepository->fetchOfferById(
            IdValue::create($data['id']),
            $currencyContext,
            $identity->getOwnershipContext()
        );

        $oldDiscountData = $this->getDiscountAndTypeFromOffer($offer);

        $offer->setData($data);

        $offer->currencyFactor = $currencyContext->currentCurrencyFactor;

        $validation = $this->validationService
            ->createUpdateValidation($offer, $identity->getOwnershipContext());

        $this->testValidation($offer, $validation);

        $ownershipContext = $identity->getOwnershipContext();

        $offer = $this->offerRepository
            ->updateOffer($offer);

        $this->offerLineItemListService
            ->updateListPricesById($offer->listId, $currencyContext, $ownershipContext);

        $list = $this->offerLineItemListRepository
            ->fetchOneListById($offer->listId, $currencyContext, $ownershipContext);

        if ($isBackend) {
            $this->setAdminChange($offer->id);
        } else {
            $this->setUserChange($offer->id);
        }

        $newDiscountData = $this->getDiscountAndTypeFromOffer($offer);

        if (!(
            $oldDiscountData['discountIsAbsolute'] === $newDiscountData['discountIsAbsolute'] &&
            $oldDiscountData['discount'] === $newDiscountData['discount']
        )) {
            $this->offerService->createOfferDiscountLogEntry(
                $offer->orderContextId,
                $identity,
                $newDiscountData['discount'],
                $oldDiscountData['discount'],
                $currencyContext,
                $isBackend,
                $oldDiscountData['discountIsAbsolute'],
                $newDiscountData['discountIsAbsolute']
            );
        }

        return $this->offerService->updateOfferPrices(
            $offer->id,
            $list,
            $currencyContext,
            $identity->getOwnershipContext()
        );
    }

    public function remove(CrudServiceRequest $request, CurrencyContext $currencyContext, OwnershipContext $ownershipContext): OfferEntity
    {
        $data = $request->getFilteredData();

        $offer = $this->offerRepository->fetchOfferById(IdValue::create($data['id']), $currencyContext, $ownershipContext);

        $this->offerRepository
            ->removeOffer($offer, $ownershipContext);

        return $offer;
    }

    /**
     * @internal
     */
    protected function getDiscountAndTypeFromOffer(OfferEntity $offer): array
    {
        $isPercentage = $offer->percentageDiscount !== 0.0 && isset($offer->percentageDiscount);

        return [
            'discount' => (float) ($isPercentage ? $offer->percentageDiscount : $offer->discountValueNet),
            'discountIsAbsolute' => !$isPercentage,
        ];
    }

    /**
     * @internal
     */
    protected function setAdminChange(IdValue $offerId): void
    {
        $offer = new OfferEntity();
        $offer->id = $offerId;
        $offer->updateDates(['changedByAdminAt']);
        $offer->removeDates(['acceptedByUserAt']);

        $this->offerRepository->updateOfferDates($offer);
    }

    /**
     * @internal
     */
    protected function setUserChange(IdValue $offerId): void
    {
        $offer = new OfferEntity();
        $offer->id = $offerId;
        $offer->updateDates(['changedByUserAt']);
        $offer->removeDates(['acceptedByAdminAt']);

        $this->offerRepository->updateOfferDates($offer);
    }
}
