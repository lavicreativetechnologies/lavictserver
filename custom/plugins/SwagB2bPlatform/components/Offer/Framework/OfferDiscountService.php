<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;

class OfferDiscountService
{
    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferCrudService
     */
    private $offerCrudService;

    public function __construct(
        OfferRepository $offerRepository,
        OfferCrudService $offerCrudService
    ) {
        $this->offerRepository = $offerRepository;
        $this->offerCrudService = $offerCrudService;
    }

    public function updateDiscount(
        IdValue $offerId,
        CurrencyContext $currencyContext,
        float $discount,
        Identity $identity,
        bool $isBackend,
        bool $isPercentageDiscount = false
    ): OfferEntity {
        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        $offer->discountValueNet = $discount;
        $offer->percentageDiscount = null;

        if ($isPercentageDiscount) {
            $offer->percentageDiscount = $discount;
            $offer->discountValueNet = null;
        }

        $serviceRequest = $this->offerCrudService
            ->createExistingRecordRequest($offer->toArray());

        return $this->offerCrudService->update($serviceRequest, $currencyContext, $identity, $isBackend);
    }

    public function checkOfferDiscountGreaterThanAmount(
        IdValue $offerId,
        CurrencyContext $currencyContext,
        Identity $identity,
        bool $isBackend = false,
        bool $isPercentageDiscount = false
    ): void {
        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        if ($offer->discountAmountNet > $offer->discountValueNet) {
            return;
        }

        $this->updateDiscount($offerId, $currencyContext, (float) 0, $identity, $isBackend, $isPercentageDiscount);

        throw new DiscountGreaterThanAmountException();
    }
}
