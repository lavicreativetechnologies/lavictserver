<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface OfferCartStateServiceInterface
{
    public function clearOfferByOldState(OfferEntity $offer, OwnershipContext $ownershipContext): void;
}
