<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceValidationService;
use Shopware\B2B\LineItemList\Framework\ProductProviderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OfferLineItemReferenceValidationService
{
    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var OfferLineItemReferenceRepository
     */
    private $referenceRepository;

    /**
     * @var LineItemReferenceValidationService
     */
    private $lineItemReferenceValidationService;

    public function __construct(
        ValidatorInterface $validator,
        ProductProviderInterface $productProvider,
        OfferLineItemReferenceRepository $referenceRepository,
        LineItemReferenceValidationService $lineItemReferenceValidationService
    ) {
        $this->productProvider = $productProvider;
        $this->validator = $validator;
        $this->referenceRepository = $referenceRepository;
        $this->lineItemReferenceValidationService = $lineItemReferenceValidationService;
    }

    /**
     * @internal
     */
    protected function createCrudValidation(OfferLineItemReferenceEntity $lineItemReference): ValidationBuilder
    {
        return $this->lineItemReferenceValidationService->createCrudValidation($lineItemReference)
            ->validateThat('comment', $lineItemReference->comment)
                ->isString()
            ->validateThat('amountNet', $lineItemReference->amountNet)
                ->isNumeric()
            ->validateThat('amount', $lineItemReference->amount)
                ->isNumeric()
            ->validateThat('discountAmountNet', $lineItemReference->discountAmountNet)
                ->isNumeric()
                ->isGreaterEqualThan(0)
            ->validateThat('discountAmount', $lineItemReference->discountAmount)
                ->isNumeric()
                ->isGreaterEqualThan(0);
    }

    public function createCrudValidator(OfferLineItemReferenceEntity $lineItemReference): Validator
    {
        return $this->createCrudValidation($lineItemReference)
            ->validateThat('referenceNumber', $lineItemReference->referenceNumber)
            ->isNotBlank()
            ->withCallback(function ($value = null) {
                return $this->productProvider
                    ->isProduct((string) $value);
            }, 'Missing product %value%', LineItemReferenceValidationService::CAUSE_IS_PRODUCT)->getValidator($this->validator);
    }

    public function createUpdateValidation(OfferLineItemReferenceEntity $lineItemReference, IdValue $offerId): Validator
    {
        return $this->createCrudValidation($lineItemReference)
            ->validateThat('id', $lineItemReference->id)
            ->isIdValue()
            ->validateThat('referenceNumber', $lineItemReference->referenceNumber)
            ->isNotBlank()
            ->withCallback(function ($value = null) {
                return $this->productProvider
                    ->isProduct((string) $value);
            }, 'Missing product %value%', LineItemReferenceValidationService::CAUSE_IS_PRODUCT)
            ->getValidator($this->validator);
    }

    public function createInsertValidation(OfferLineItemReferenceEntity $lineItemReference, IdValue $listId): Validator
    {
        $validation = $this->createCrudValidation($lineItemReference)
            ->validateThat('id', $lineItemReference->id)
            ->isNullIdValue()
            ->validateThat('referenceNumber', $lineItemReference->referenceNumber)
            ->isNotBlank()
            ->isUnique(function () use ($lineItemReference, $listId) {
                return !$this->referenceRepository
                    ->hasReference($lineItemReference->referenceNumber, $listId);
            })
            ->withCallback(function ($value = null) {
                return $this->productProvider
                    ->isProduct((string) $value);
            }, 'Missing product %value%', LineItemReferenceValidationService::CAUSE_IS_PRODUCT)

            ->getValidator($this->validator);

        return $validation;
    }
}
