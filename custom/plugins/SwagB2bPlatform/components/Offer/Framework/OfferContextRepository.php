<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use function array_merge;

class OfferContextRepository
{
    const TABLE_NAME = 'b2b_order_context';
    const TABLE_ALIAS = 'b2bOrder';
    const STATE_OFFER = 'offer';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    public function sendToOfferState(IdValue $orderContextId): void
    {
        $this->setOrderStatusName($orderContextId, self::STATE_OFFER, []);
    }

    /**
     * @internal
     */
    protected function setOrderStatusName(
        IdValue $orderContextId,
        string $status,
        array $additionalData
    ): void {
        $success = (bool) $this->connection->update(
            self::TABLE_NAME,
            array_merge(
                ['state' => $status],
                $additionalData
            ),
            ['id' => $orderContextId->getStorageValue()]
        );

        if (!$success) {
            throw new CanNotUpdateExistingRecordException(
                "Could not update b2b order context status with context id {$orderContextId}"
            );
        }
    }
}
