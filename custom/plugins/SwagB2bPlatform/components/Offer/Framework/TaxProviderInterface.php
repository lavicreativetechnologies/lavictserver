<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Framework;

interface TaxProviderInterface
{
    public function getProductTax(OfferLineItemReferenceEntity $reference): float;
}
