<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Api;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Offer\Framework\OfferLineItemListRepository;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceCrudService;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use function in_array;

class OfferLineItemReferenceController
{
    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OfferLineItemReferenceCrudService
     */
    private $offerLineItemReferenceCrudService;

    /**
     * @var OfferLineItemListRepository
     */
    private $offerLineItemListRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        OfferRepository $offerRepository,
        CurrencyService $currencyService,
        OfferLineItemReferenceCrudService $offerLineItemReferenceCrudService,
        OfferLineItemListRepository $offerLineItemListRepository,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->offerRepository = $offerRepository;
        $this->currencyService = $currencyService;
        $this->offerLineItemReferenceCrudService = $offerLineItemReferenceCrudService;
        $this->offerLineItemListRepository = $offerLineItemListRepository;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function addItemsAction(string $offerId, Request $request): array
    {
        $offerId = IdValue::create($offerId);
        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authStorageAdapter->getIdentity();

        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        $items = $request->getPost();

        $references = [];
        foreach ($items as $item) {
            $crudService = $this->offerLineItemReferenceCrudService->createCreateCrudRequest($item);
            $references[] = $this->offerLineItemReferenceCrudService->addLineItem(
                $offer->listId,
                $offer->id,
                $crudService,
                $currencyContext,
                $identity
            );
        }

        $lineItemList = $this->offerLineItemListRepository->fetchOneListById(
            $offer->listId,
            $currencyContext,
            $identity->getOwnershipContext()
        );

        return ['success' => true, 'offer' => $offer, 'lineItemList' => $lineItemList, 'items' => $references];
    }

    public function removeItemsAction(string $offerId, Request $request): array
    {
        $offerId = IdValue::create($offerId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authStorageAdapter->getIdentity();

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());
        $itemIds = IdValue::createMultiple($request->getPost());

        $items = $this->offerLineItemListRepository
            ->fetchOneListById($offer->listId, $currencyContext, $identity->getOwnershipContext())
            ->references;

        $references = [];
        foreach ($items as $item) {
            if (in_array($item->id, $itemIds, true)) {
                $item->id = IdValue::null();
                $references[] = $item;
            }
        }

        foreach ($itemIds as $itemId) {
            $this->offerLineItemReferenceCrudService->deleteLineItem(
                $offer->id,
                $offer->listId,
                $itemId,
                $currencyContext,
                $identity
            );
        }

        $lineItemList = $this->offerLineItemListRepository->fetchOneListById(
            $offer->listId,
            $currencyContext,
            $identity->getOwnershipContext()
        );

        return ['success' => true, 'orderList' => $offer, 'lineItemList' => $lineItemList, 'items' => $references];
    }

    public function updateItemsAction(string $offerId, Request $request): array
    {
        $offerId = IdValue::create($offerId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authStorageAdapter->getIdentity();

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());
        $items = $request->getPost();

        $references = [];
        foreach ($items as $item) {
            $item['discountAmountNet'] = (float) $item['discountAmountNet'];
            $crudService = $this->offerLineItemReferenceCrudService->createUpdateCrudRequest($item);
            $references[] = $this->offerLineItemReferenceCrudService
                ->updateLineItem($offer->listId, $offer->id, $crudService, $currencyContext, $identity);
        }

        $lineItemList = $this->offerLineItemListRepository->fetchOneListById(
            $offer->listId,
            $currencyContext,
            $identity->getOwnershipContext()
        );

        return ['success' => true, 'offer' => $offer, 'lineItemList' => $lineItemList, 'items' => $references];
    }

    public function getItemsAction(string $offerId): array
    {
        $offerId = IdValue::create($offerId);

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authStorageAdapter->getIdentity()->getOwnershipContext();

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        $lineItemList = $this->offerLineItemListRepository->fetchOneListById(
            $offer->listId,
            $currencyContext,
            $ownershipContext
        );

        return ['success' => true, 'offer' => $offer, 'lineItemList' => $lineItemList, 'items' => $lineItemList->references];
    }
}
