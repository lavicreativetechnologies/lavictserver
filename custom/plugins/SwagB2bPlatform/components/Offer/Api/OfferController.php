<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Api;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Offer\Framework\OfferCrudService;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Offer\Framework\OfferSearchStruct;
use Shopware\B2B\Offer\Framework\OfferService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;

class OfferController
{
    /**
     * @var GridHelper
     */
    private $offerGridHelper;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OfferCrudService
     */
    private $offerCrudService;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        OfferRepository $offerRepository,
        OfferCrudService $offerCrudService,
        GridHelper $offerGridHelper,
        CurrencyService $currencyService,
        OfferService $offerService,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->offerGridHelper = $offerGridHelper;
        $this->offerRepository = $offerRepository;
        $this->currencyService = $currencyService;
        $this->offerCrudService = $offerCrudService;
        $this->offerService = $offerService;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(Request $request): array
    {
        $search = new OfferSearchStruct();
        $ownershipContext = $this->getDebtorOwnershipContext();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $this->offerGridHelper
            ->extractSearchDataInRestApi($request, $search);

        $offerEntities = $this->offerRepository
            ->fetchList($ownershipContext, $search, $currencyContext);

        $totalCount = $this->offerRepository
            ->fetchTotalCount($ownershipContext, $search);

        return ['success' => true, 'offers' => $offerEntities, 'totalCount' => $totalCount];
    }

    public function getAction(string $offerId): array
    {
        $offerId = IdValue::create($offerId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        return ['success' => true, 'offer' => $offer];
    }

    public function removeAction(string $offerId, Request $request): array
    {
        $offerId = IdValue::create($offerId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $post = $request->getPost();
        $post['id'] = $offerId;

        $request = $this->offerCrudService->createExistingRecordRequest($post);
        $offer = $this->offerCrudService->remove($request, $currencyContext, $ownershipContext, true);

        return ['success' => true, 'offer' => $offer];
    }

    public function updateAction(string $offerId, Request $request): array
    {
        $offerId = IdValue::create($offerId);
        $post = $request->getPost();
        $post['id'] = $offerId;

        $debtorIdentity = $this->authStorageAdapter->getIdentity();

        $currencyContext = $this->currencyService->createCurrencyContext();

        $serviceRequest = $this->offerCrudService
            ->createExistingRecordRequest($post);

        $offer = $this->offerCrudService
            ->update($serviceRequest, $currencyContext, $debtorIdentity, true);

        return ['success' => true, 'offer' => $offer];
    }

    public function updateOfferExpiredDateAction(string $offerId, Request $request): array
    {
        $offerId = IdValue::create($offerId);
        $context = $this->currencyService->createCurrencyContext();

        $debtorIdentity = $this->authStorageAdapter->getIdentity();

        $offer = $this->offerRepository->fetchOfferById($offerId, $context, $debtorIdentity->getOwnershipContext());

        $expiredDate = $request->getParam('expiredAt');

        $offer = $this->offerService->updateExpiredDate($expiredDate, $offer, $debtorIdentity);

        return ['success' => true, 'offer' => $offer];
    }

    public function acceptAction(string $offerId): array
    {
        $offerId = IdValue::create($offerId);
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $identity = $this->authStorageAdapter->getIdentity();

        $this->offerService
            ->acceptOffer($offerId, $currencyContext, $identity);

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        return ['success' => true, 'offer' => $offer];
    }

    public function declineOfferAction(string $offerId): array
    {
        $offerId = IdValue::create($offerId);
        $currencyContext = $this->currencyService->createCurrencyContext();

        $identity = $this->authStorageAdapter->getIdentity();

        $this->offerService->declineOffer($offerId, $currencyContext, $identity);

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        return ['success' => true, 'offer' => $offer];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext()
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
