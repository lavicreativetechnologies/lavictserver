<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartFinishSubscriber;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\Order\CartConvertedEvent;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrderFinishSubscriberDecorator extends CartFinishSubscriber
{
    /**
     * @var CartFinishSubscriber
     */
    private $inner;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    public function __construct(
        CartFinishSubscriber $inner,
        OfferRepository $offerRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->inner = $inner;
        $this->offerRepository = $offerRepository;

        parent::__construct($eventDispatcher);
    }

    public function initializeState(CartConvertedEvent $event): void
    {
        $this->cartConvertedEvent = $event;
        $this->inner->initializeState($event);
    }

    /**
     * @internal
     */
    protected function onCartFinish(
        Cart $cart,
        OrderEntity $orderEntity,
        SalesChannelContext $salesChannelContext
    ): ?OrderContext {
        $orderContext = $this->inner->onCartFinish($cart, $orderEntity, $salesChannelContext);
        if ($orderContext === null) {
            return null;
        }

        /** @var OfferDestination $offerDestination */
        $offerDestination = $this->getCartState()->getDestination();
        if (!$offerDestination instanceof OfferDestination) {
            return null;
        }

        $offer = $offerDestination->getOfferEntity();

        $offer->updateDates(['convertedAt']);
        $this->offerRepository->updateOfferDates($offer);

        return $orderContext;
    }
}
