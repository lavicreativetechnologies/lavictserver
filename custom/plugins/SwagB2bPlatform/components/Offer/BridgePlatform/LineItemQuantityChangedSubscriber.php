<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\Event\BeforeLineItemQuantityChangedEvent;
use Shopware\Core\Checkout\Cart\Event\LineItemQuantityChangedEvent;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LineItemQuantityChangedSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            LineItemQuantityChangedEvent::class => 'checkLineItemQuantityChanged',
            BeforeLineItemQuantityChangedEvent::class => 'checkBeforeLineItemQuantityChanged',
        ];
    }

    public function checkLineItemQuantityChanged(LineItemQuantityChangedEvent $event): void
    {
        $this->checkChanged($event->getCart(), $event->getLineItem());
    }

    public function checkBeforeLineItemQuantityChanged(BeforeLineItemQuantityChangedEvent $event): void
    {
        $this->checkChanged($event->getCart(), $event->getLineItem());
    }

    private function checkChanged(Cart $cart, LineItem $lineItem): void
    {
        if (!$cart->hasExtension(CartState::NAME)) {
            return;
        }

        $destination = CartState::extract($cart)->getDestination();
        if (!($destination instanceof OfferDestination)) {
            return;
        }

        throw new QuantityChangeNotAllowedException($lineItem->getId());
    }
}
