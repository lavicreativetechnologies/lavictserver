<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use function sprintf;

class LanguageRepository
{
    /**
     * @var EntityRepositoryInterface
     */
    private $entityRepository;

    public function __construct(
        EntityRepositoryInterface $entityRepository
    ) {
        $this->entityRepository = $entityRepository;
    }

    public function getLocalCodeByLanguageId(IdValue $languageId): string
    {
        $criteria = (new Criteria([$languageId->getValue()]))
            ->addAssociation('locale');

        $languageEntity = $this->entityRepository->search(
            $criteria,
            Context::createDefaultContext()
        )->getEntities()->first();

        if (!$languageEntity) {
            throw new NotFoundException(sprintf('Unable to locate local code for language id "%s"', $languageId->getValue()));
        }

        return $languageEntity->getLocale()->getCode();
    }
}
