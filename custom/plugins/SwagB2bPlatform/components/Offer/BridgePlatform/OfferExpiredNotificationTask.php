<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class OfferExpiredNotificationTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'B2bBudgetNotifyAuthor';
    }

    public static function getDefaultInterval(): int
    {
        return 86400;
    }
}
