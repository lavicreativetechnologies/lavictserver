<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceEntity;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartBehavior;
use Shopware\Core\Checkout\Cart\CartProcessorInterface;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\AbsolutePriceCalculator;
use Shopware\Core\Checkout\Cart\Price\PercentagePriceCalculator;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\AbsolutePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\PercentagePriceDefinition;
use Shopware\Core\Checkout\Cart\Price\Struct\PriceCollection;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\Translation\TranslatorInterface;
use function abs;
use function array_map;
use function md5;

class OfferProcessor implements CartProcessorInterface
{
    private const B2B_DISCOUNT_KEY = 'b2b-discount';
    private const PERCENTAGE_DISCOUNT_NAME = 'b2b.percentage_discount';
    private const ABSOLUTE_DISCOUNT_NAME = 'b2b.absolute_discount';

    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;

    /**
     * @var QuantityPriceCalculator
     */
    private $quantityPriceCalculator;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    /**
     * @var AbsolutePriceCalculator
     */
    private $absolutePriceCalculator;

    /**
     * @var PercentagePriceCalculator
     */
    private $percentagePriceCalculator;

    public function __construct(
        SalesChannelRepositoryInterface $productRepository,
        QuantityPriceCalculator $quantityPriceCalculator,
        TranslatorInterface $translator,
        ContextProvider $salesChannelContextProvider,
        AbsolutePriceCalculator $absolutePriceCalculator,
        PercentagePriceCalculator $percentagePriceCalculator
    ) {
        $this->productRepository = $productRepository;
        $this->quantityPriceCalculator = $quantityPriceCalculator;
        $this->translator = $translator;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
        $this->absolutePriceCalculator = $absolutePriceCalculator;
        $this->percentagePriceCalculator = $percentagePriceCalculator;
    }

    public function process(CartDataCollection $data, Cart $original, Cart $toCalculate, SalesChannelContext $context, CartBehavior $behavior): void
    {
        $this->salesChannelContextProvider->setSalesChannelContext($context);

        if (!$original->hasExtension(CartState::NAME)) {
            return;
        }

        $destination = CartState::extract($original)->getDestination();

        if (!$destination instanceof OfferDestination) {
            return;
        }

        $offer = $destination->getOfferEntity();
        $lineItemList = $destination->getLineItemList();

        $this->overrideLineItemPrices($toCalculate, $lineItemList, $context);

        $prices = $toCalculate->getLineItems()
            ->filterType(LineItem::PRODUCT_LINE_ITEM_TYPE)->getPrices();

        $discountLineItem = $this->createDiscountLineItem(
            $offer,
            $prices,
            $context
        );

        if (!$discountLineItem) {
            return;
        }

        $toCalculate->add($discountLineItem);
    }

    /**
     * @internal
     */
    protected function overrideLineItemPrices(Cart $toCart, LineItemList $lineItemList, SalesChannelContext $context): void
    {
        $cartLineItemCollection = $toCart->getLineItems();

        $referenceNumbers = array_map(function (LineItemReference $reference) {
            return $reference->referenceNumber;
        }, $lineItemList->references);

        $products = $this->getProductsByReferenceNumbers($referenceNumbers, $context);

        foreach ($lineItemList->references as $reference) {
            if (!$reference instanceof OfferLineItemReferenceEntity) {
                continue;
            }

            if (!$reference->discountAmountNet || !isset($products[$reference->referenceNumber])) {
                continue;
            }

            foreach ($cartLineItemCollection->getElements() as $lineItem) {
                if ($products[$reference->referenceNumber]->getId() !== $lineItem->getReferencedId()) {
                    continue;
                }

                if (!($lineItem->getPriceDefinition() instanceof QuantityPriceDefinition)) {
                    continue;
                }

                /** @var QuantityPriceDefinition $definition */
                $definition = $lineItem->getPriceDefinition();

                if (ShopwareVersion::isShopware6_4_X()) {
                    $newQuantityPriceDefinition = new QuantityPriceDefinition(
                        $reference->discountAmountNet,
                        $definition->getTaxRules(),
                        $definition->getQuantity()
                    );
                    $newQuantityPriceDefinition->setIsCalculated(false);
                } else {
                    $newQuantityPriceDefinition = new QuantityPriceDefinition(
                        $reference->discountAmountNet,
                        $definition->getTaxRules(),
                        $context->getContext()->getCurrencyPrecision(),
                        $definition->getQuantity()
                    );
                }

                $lineItem->setPriceDefinition($newQuantityPriceDefinition);

                $price = $this->quantityPriceCalculator->calculate($newQuantityPriceDefinition, $context);
                $lineItem->setPrice($price);
            }
        }
    }

    /**
     * @internal
     */
    protected function createDiscountLineItem(
        OfferEntity $offerEntity,
        PriceCollection $prices,
        SalesChannelContext $context
    ): ?LineItem {
        $discountItem = null;

        if ($offerEntity->discountValueNet) {
            $discountItem = $this->createAbsoluteDiscountItem($offerEntity, $prices, $context);
        }

        if ($offerEntity->percentageDiscount) {
            $discountItem = $this->createPercentageDiscountItem($offerEntity, $prices, $context);
        }

        return $discountItem;
    }

    /**
     * @internal
     */
    protected function createAbsoluteDiscountItem(
        OfferEntity $offerEntity,
        PriceCollection $prices,
        SalesChannelContext $context
    ): LineItem {
        $lineItem = $this->createNewPromotionLineItem();

        $lineItem->setLabel($this->translator->trans(self::ABSOLUTE_DISCOUNT_NAME));
        $lineItem->setDescription($this->translator->trans(self::ABSOLUTE_DISCOUNT_NAME));

        if (ShopwareVersion::isShopware6_4_X()) {
            $absoluteDiscount = new AbsolutePriceDefinition(-abs($offerEntity->discountValueNet));
        } else {
            $absoluteDiscount = new AbsolutePriceDefinition(
                -abs($offerEntity->discountValueNet),
                $context->getContext()->getCurrencyPrecision()
            );
        }

        $lineItem->setPriceDefinition($absoluteDiscount);
        $calculatedAbsoluteDiscount = $this->calculateAbsoluteDiscountPrice($lineItem, $prices, $context);

        $lineItem->setPrice($calculatedAbsoluteDiscount);

        return $lineItem;
    }

    /**
     * @internal
     */
    protected function createPercentageDiscountItem(OfferEntity $offerEntity, PriceCollection $prices, SalesChannelContext $context): LineItem
    {
        $lineItem = $this->createNewPromotionLineItem();

        $lineItem->setLabel($this->translator->trans(self::PERCENTAGE_DISCOUNT_NAME));
        $lineItem->setDescription($this->translator->trans(self::PERCENTAGE_DISCOUNT_NAME));

        if (ShopwareVersion::isShopware6_4_X()) {
            $percentageDiscount = new PercentagePriceDefinition(
                $offerEntity->percentageDiscount
            );
        } else {
            $percentageDiscount = new PercentagePriceDefinition(
                $offerEntity->percentageDiscount,
                $context->getContext()->getCurrencyPrecision()
            );
        }

        $lineItem->setPriceDefinition($percentageDiscount);
        $calculatedPercentageDiscount = $this->calculatePercentageDiscountPrice($lineItem, $prices, $context);

        $lineItem->setPrice($calculatedPercentageDiscount);

        return $lineItem;
    }

    /**
     * @internal
     */
    protected function createNewPromotionLineItem(): LineItem
    {
        $lineItem = new LineItem(
            md5(self::B2B_DISCOUNT_KEY),
            self::B2B_DISCOUNT_KEY,
            ''
        );

        $lineItem->setGood(false);
        $lineItem->setRemovable(false);

        return $lineItem;
    }

    /**
     * @internal
     * @return ProductEntity[]
     */
    protected function getProductsByReferenceNumbers(array $referenceNumbers, SalesChannelContext $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('productNumber', $referenceNumbers));
        $criteria->addAssociation('prices');

        $products = $this->productRepository->search($criteria, $context)->getElements();

        $formattedProducts = [];

        /** @var ProductEntity $product */
        foreach ($products as $product) {
            $formattedProducts[$product->getProductNumber()] = $product;
        }

        return $formattedProducts;
    }

    /**
     * @internal
     */
    protected function calculateAbsoluteDiscountPrice(
        LineItem $lineItem,
        PriceCollection $prices,
        SalesChannelContext $context
    ): CalculatedPrice {
        $priceDefinition = $lineItem->getPriceDefinition();
        $discountPrice = -abs($priceDefinition->getPrice());

        return $this->absolutePriceCalculator
            ->calculate($discountPrice, $prices, $context, $lineItem->getQuantity());
    }

    /**
     * @internal
     */
    protected function calculatePercentageDiscountPrice(
        LineItem $lineItem,
        PriceCollection $prices,
        SalesChannelContext $context
    ): CalculatedPrice {
        $priceDefinition = $lineItem->getPriceDefinition();
        $discountPercentage = -abs($priceDefinition->getPercentage());

        return $this->percentagePriceCalculator
            ->calculate($discountPercentage, $prices, $context);
    }
}
