<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\LineItemList\BridgePlatform\LineItemReferenceMailData;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceEntity;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class OfferLineItemReferenceMailData extends LineItemReferenceMailData
{
    /**
     * @var OfferLineItemReferenceEntity
     */
    private $reference;

    public function __construct(OfferLineItemReferenceEntity $reference)
    {
        $this->reference = $reference;

        parent::__construct($reference);
    }

    public static function getReferenceDataType(): EventDataType
    {
        $referenceData = parent::getReferenceDataType();

        $referenceData->add('discountAmountNet', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $referenceData->add('discountAmount', new ScalarValueType(ScalarValueType::TYPE_FLOAT));

        return $referenceData;
    }

    public function getDiscountAmountNet(): float
    {
        return $this->reference->discountAmountNet;
    }

    public function getDiscountAmount(): float
    {
        return $this->reference->discountAmount;
    }
}
