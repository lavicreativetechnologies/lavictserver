<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartLineItemListConverter;
use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\B2B\Cart\BridgePlatform\ShopCartService;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Offer\Framework\OfferLineItemListRepository;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Offer\Framework\OfferShopWriterServiceInterface;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;

class OfferShopWriterService implements OfferShopWriterServiceInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var ShopCartService
     */
    private $shopCartService;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var CartLineItemListConverter
     */
    private $cartLineItemListConverter;

    /**
     * @var OfferLineItemListRepository
     */
    private $lineItemListRepository;

    public function __construct(
        ContextProvider $contextProvider,
        CartService $cartService,
        ShopCartService $shopCartService,
        OfferRepository $offerRepository,
        CurrencyService $currencyService,
        AuthenticationService $authenticationService,
        CartLineItemListConverter $cartLineItemListConverter,
        OfferLineItemListRepository $lineItemListRepository
    ) {
        $this->contextProvider = $contextProvider;
        $this->cartService = $cartService;
        $this->shopCartService = $shopCartService;
        $this->offerRepository = $offerRepository;
        $this->currencyService = $currencyService;
        $this->authenticationService = $authenticationService;
        $this->cartLineItemListConverter = $cartLineItemListConverter;
        $this->lineItemListRepository = $lineItemListRepository;
    }

    public function sendToCheckout(OrderContext $orderContext): void
    {
        $salesChannelContext = $this->contextProvider
            ->getSalesChannelContext();

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $ownershipContext = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        $offer = $this->offerRepository->fetchOfferByOrderContextId(
            $orderContext->id,
            $currencyContext,
            $ownershipContext
        );

        $lineItemList = $this->lineItemListRepository
            ->fetchOneListById($offer->listId, $currencyContext, $ownershipContext);

        $cart = $this->cartLineItemListConverter
            ->lineItemListToCart($lineItemList);

        $offerDestination = new OfferDestination($offer, $lineItemList);

        CartState::extract($cart)
            ->setDestination($offerDestination);

        $this->cartService
            ->recalculate($cart, $salesChannelContext);
    }

    public function stopCheckout(): void
    {
        $context = $this->contextProvider->getSalesChannelContext();
        $this->shopCartService->clear($context);
    }
}
