<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Debtor\BridgePlatform\DebtorEntityMailData;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\LineItemList\BridgePlatform\LineItemListMailData;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Order\BridgePlatform\OrderContextMailData;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\B2B\StoreFrontAuthentication\BridgePlatform\UserPostalSettingsMailData;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserPostalSettings;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;
use Shopware\Core\Framework\Event\MailActionInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\EventDispatcher\Event;

class OfferStatusNotifyMailEvent extends Event implements MailActionInterface
{
    public const EVENT_NAME = 'b2b.offer.status.notify.mail';

    /**
     * @var DebtorEntityMailData
     */
    private $debtor;

    /**
     * @var SalesChannelContext
     */
    private $context;

    /**
     * @var OrderContextMailData
     */
    private $b2bOrderContext;

    /**
     * @var OfferEntityMailData
     */
    private $offer;

    /**
     * @var LineItemListMailData
     */
    private $lineItemList;

    /**
     * @var UserPostalSettingsMailData
     */
    private $postalSettings;

    /**
     * @var string
     */
    private $translatedOfferStatus;

    public function __construct(
        SalesChannelContext $context,
        OfferEntity $offerEntity,
        LineItemList $lineItemList,
        OrderContext $orderContext,
        DebtorEntity $debtorEntity,
        UserPostalSettings $userPostalSettings,
        string $translatedOfferStatus
    ) {
        $this->lineItemList = new OfferLineItemListMailData($lineItemList);
        $this->offer = new OfferEntityMailData($offerEntity);
        $this->b2bOrderContext = new OrderContextMailData($orderContext);
        $this->debtor = new DebtorEntityMailData($debtorEntity);
        $this->postalSettings = new UserPostalSettingsMailData($userPostalSettings);
        $this->context = $context;
        $this->translatedOfferStatus = $translatedOfferStatus;
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('offer', OfferEntityMailData::getOfferMailDataType())
            ->add('b2bOrderContext', OrderContextMailData::getOrderContextData())
            ->add('lineItemList', OfferLineItemListMailData::getLineItemListDataType())
            ->add('debtor', DebtorEntityMailData::getDebtorEntityDataType())
            ->add('translatedOfferStatus', new ScalarValueType(ScalarValueType::TYPE_STRING))
            ->add('postalSettings', UserPostalSettingsMailData::getPostalSettingsMailDataType());
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        return new MailRecipientStruct([
            $this->postalSettings->getEmail() => $this->postalSettings->getFirstName() . ' ' . $this->postalSettings->getLastName(),
        ]);
    }

    public function getSalesChannelId(): ?string
    {
        return $this->context->getSalesChannel()->getId();
    }

    public function getContext(): Context
    {
        return $this->context->getContext();
    }

    public function getB2bOrderContext(): OrderContextMailData
    {
        return $this->b2bOrderContext;
    }

    public function getOffer(): OfferEntityMailData
    {
        return $this->offer;
    }

    public function getLineItemList(): LineItemListMailData
    {
        return $this->lineItemList;
    }

    public function getDebtor(): DebtorEntityMailData
    {
        return $this->debtor;
    }

    public function getPostalSettings(): UserPostalSettingsMailData
    {
        return $this->postalSettings;
    }

    public function getTranslatedOfferStatus(): string
    {
        return $this->translatedOfferStatus;
    }
}
