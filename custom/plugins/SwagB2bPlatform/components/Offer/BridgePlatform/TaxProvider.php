<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Exception;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceEntity;
use Shopware\B2B\Offer\Framework\TaxProviderInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\Tax\TaxEntity;

class TaxProvider implements TaxProviderInterface
{
    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    /**
     * @var SalesChannelRepository
     */
    private $salesChannelRepository;

    public function __construct(
        ContextProvider $salesChannelContextProvider,
        SalesChannelRepositoryInterface $salesChannelRepository
    ) {
        $this->salesChannelContextProvider = $salesChannelContextProvider;
        $this->salesChannelRepository = $salesChannelRepository;
    }

    public function getDiscountTax(IdValue $lineItemListId, OwnershipContext $ownershipContext): float
    {
        throw new Exception('This discount should not be calculated here. Use the OfferCalculator instead,');
    }

    public function getProductTax(OfferLineItemReferenceEntity $reference): float
    {
        $salesChannelContext = $this->salesChannelContextProvider->getSalesChannelContext();

        if ($reference->referenceNumber === null) {
            throw new Exception('No referenceNumber provided');
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter(
            'productNumber',
            $reference->referenceNumber
        ));

        $entitySearchResult = $this->salesChannelRepository->search($criteria, $salesChannelContext);

        /** @var ProductEntity $product */
        $product = $entitySearchResult->getEntities()->first();

        $tax = $product->getTax();

        if (!($tax instanceof TaxEntity)) {
            throw new Exception('Tax not set');
        }

        return 1.0 + ($tax->getTaxRate() / 100);
    }
}
