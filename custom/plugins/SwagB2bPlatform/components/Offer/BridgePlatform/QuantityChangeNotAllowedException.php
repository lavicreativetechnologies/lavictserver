<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use InvalidArgumentException;
use Shopware\B2B\Common\B2BException;
use function sprintf;

class QuantityChangeNotAllowedException extends InvalidArgumentException implements B2BException
{
    public function __construct(string $lineItemId)
    {
        parent::__construct(sprintf('The quantity of %s could not be changed.', $lineItemId));
    }
}
