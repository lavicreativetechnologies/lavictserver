<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OfferDestination;
use Shopware\B2B\Cart\BridgePlatform\OrderClearanceInterceptDestination;
use Shopware\B2B\Offer\Framework\OfferCartStateServiceInterface;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;

class OfferCartStateService implements OfferCartStateServiceInterface
{
    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        OfferRepository $offerRepository,
        CartService $cartService,
        ContextProvider $contextProvider
    ) {
        $this->offerRepository = $offerRepository;
        $this->cartService = $cartService;
        $this->contextProvider = $contextProvider;
    }

    public function clearOfferByOldState(OfferEntity $offer, OwnershipContext $ownershipContext): void
    {
        $salesChannelContext = $this->contextProvider
            ->getSalesChannelContext();

        $cart = $this->cartService
            ->getCart($salesChannelContext->getToken(), $salesChannelContext);

        $cartState = CartState::extract($cart);
        $orderClearanceDestinationClass = OrderClearanceInterceptDestination::class;

        if ($cartState->hasDestination($orderClearanceDestinationClass)) {
            $this->offerRepository->removeOfferWithoutContext($offer);
        } else {
            $this->offerRepository->removeOffer($offer, $ownershipContext);
        }

        $cartState->removeDestination(OfferDestination::class);
    }
}
