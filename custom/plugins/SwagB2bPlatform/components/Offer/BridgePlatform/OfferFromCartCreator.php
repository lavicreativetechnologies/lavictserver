<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartLineItemListConverter;
use Shopware\B2B\Cart\BridgePlatform\TotalPriceCalculationService;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListService;
use Shopware\B2B\Offer\Framework\OfferContextRepository;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferFromCartCreatorInterface;
use Shopware\B2B\Offer\Framework\OfferService;
use Shopware\B2B\Offer\Framework\OfferStatusChangeNotifierInterface;
use Shopware\B2B\Order\Framework\OrderCheckoutSource;
use Shopware\B2B\Order\Framework\OrderContextService;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class OfferFromCartCreator implements OfferFromCartCreatorInterface
{
    /**
     * @var LineItemListService
     */
    private $lineItemListService;

    /**
     * @var OrderContextService
     */
    private $orderContextService;

    /**
     * @var OfferContextRepository
     */
    private $offerContextRepository;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var ContextProvider
     */
    private $salesChannelContextProvider;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartLineItemListConverter
     */
    private $cartLineItemListConverter;

    /**
     * @var OfferStatusChangeNotifierInterface
     */
    private $offerStatusChangeNotifier;

    /**
     * @var TotalPriceCalculationService
     */
    private $totalPriceCalculationService;

    public function __construct(
        LineItemListService $lineItemListService,
        OrderContextService $orderContextService,
        OfferContextRepository $offerContextRepository,
        OfferService $offerService,
        ContextProvider $salesChannelContextProvider,
        CartService $cartService,
        CartLineItemListConverter $cartLineItemListConverter,
        OfferStatusChangeNotifierInterface $offerStatusChangeNotifier,
        TotalPriceCalculationService $totalPriceCalculationService
    ) {
        $this->lineItemListService = $lineItemListService;
        $this->orderContextService = $orderContextService;
        $this->offerContextRepository = $offerContextRepository;
        $this->offerService = $offerService;
        $this->salesChannelContextProvider = $salesChannelContextProvider;
        $this->cartService = $cartService;
        $this->cartLineItemListConverter = $cartLineItemListConverter;
        $this->offerStatusChangeNotifier = $offerStatusChangeNotifier;
        $this->totalPriceCalculationService = $totalPriceCalculationService;
    }

    public function createOffer(Identity $identity, CurrencyContext $currencyContext): OfferEntity
    {
        $salesChannelContext = $this->salesChannelContextProvider->getSalesChannelContext();
        $ownershipContext = $identity->getOwnershipContext();

        $cart = $this->cartService->getCart($salesChannelContext->getToken(), $salesChannelContext);

        $lineItemList = $this->createListFromCheckout($cart, $identity);

        $orderContext = $this->orderContextService
            ->createContextThroughCheckoutSource(
                $ownershipContext,
                $lineItemList,
                $this->createOrderCheckoutSourceFromCart($cart, $salesChannelContext)
            );

        $offer = $this->offerService->createOfferThroughCheckoutSource(
            $identity,
            $currencyContext,
            $orderContext,
            $lineItemList
        );

        $this->offerContextRepository
            ->sendToOfferState($orderContext->id, $identity->getOwnershipContext());

        $this->deleteCart($salesChannelContext);

        $this->offerStatusChangeNotifier->notify($offer);

        return $offer;
    }

    /**
     * @internal
     */
    protected function deleteCart(SalesChannelContext $context): void
    {
        $this->cartService->deleteCart($context);
    }

    /**
     * @internal
     */
    protected function createListFromCheckout(Cart $cart, Identity $identity): LineItemList
    {
        $lineItemList = $this->cartLineItemListConverter->cartToLineItemList($cart, $identity->getOwnershipContext());

        return $this->lineItemListService->createListThroughListObject($lineItemList, $identity->getOwnershipContext());
    }

    /**
     * @internal
     */
    protected function createOrderCheckoutSourceFromCart(Cart $cart, SalesChannelContext $salesChannelContext): OrderCheckoutSource
    {
        // ToDo set comment and device type
        return new OrderCheckoutSource(
            IdValue::create($salesChannelContext->getCustomer()->getActiveBillingAddress()->getId()),
            IdValue::create($salesChannelContext->getCustomer()->getActiveShippingAddress()->getId()),
            IdValue::create($salesChannelContext->getShippingMethod()->getId()),
            '',
            '',
            $this->totalPriceCalculationService->getTotalAmount($cart->getShippingCosts(), $salesChannelContext),
            $this->totalPriceCalculationService->getTotalAmountNet($cart->getShippingCosts(), $salesChannelContext),
            IdValue::create($salesChannelContext->getPaymentMethod()->getId())
        );
    }
}
