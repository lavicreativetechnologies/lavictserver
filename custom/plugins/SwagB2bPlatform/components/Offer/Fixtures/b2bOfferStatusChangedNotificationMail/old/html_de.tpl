<div style="font-family:arial,sans-serif; font-size:12px;">
    {include file="string:{config name=emailheaderhtml}"}
    <br/>
    <p>
        Hallo {$addressee.name},<br/>
        <br/>
        der Status {if $context.isToEnquirer}deines Angebotes{else}des Angebots von{$offer.enquirer}{/if} bei {config name=shopName} hat sich ge&auml;ndert.<br>
        Er ist jetzt "{s name="{$offer.status}"}{$offer.status}{/s}".<br>
        {if !$offer.finished}Bitte logge dich in deinen {if $context.isToAdmin}Admin-{/if}Account ein und checke das Angebot unter {if $context.isToAdmin}"Kunden" - "Angebote"{else}"Mein Konto" - "Dashboard" - "Angebote"{/if}{/if}
    </p>
    {include file="string:{config name=emailfooterhtml}"}
</div>
