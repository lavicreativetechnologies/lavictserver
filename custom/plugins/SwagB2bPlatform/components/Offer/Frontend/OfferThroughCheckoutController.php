<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Frontend;

use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectException;
use Shopware\B2B\Common\Controller\B2bControllerRedirectToCheckoutConfirm;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Common\Validator\ValidationException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\LineItemList\Framework\LineItemList;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceService;
use Shopware\B2B\LineItemList\Framework\LineItemShopWriterServiceInterface;
use Shopware\B2B\Offer\Framework\DiscountGreaterThanAmountException;
use Shopware\B2B\Offer\Framework\OfferCartStateServiceInterface;
use Shopware\B2B\Offer\Framework\OfferDiscountService;
use Shopware\B2B\Offer\Framework\OfferEntity;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceCrudService;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceRepository;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Offer\Framework\OfferService;
use Shopware\B2B\Offer\Framework\UnexpectedOfferStateException;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderContextService;
use Shopware\B2B\Shop\Framework\StorageInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use function array_merge;
use function count;

class OfferThroughCheckoutController
{
    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferLineItemReferenceRepository
     */
    private $offerLineItemReferenceRepository;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var GridHelper
     */
    private $gridHelper;

    /**
     * @var LineItemReferenceService
     */
    private $offerLineItemReferenceService;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var OfferLineItemReferenceCrudService
     */
    private $offerLineItemReferenceCrudService;

    /**
     * @var StorageInterface
     */
    private $sessionStorage;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var OfferDiscountService
     */
    private $offerDiscountService;

    /**
     * @var LineItemShopWriterServiceInterface
     */
    private $lineItemShopWriterService;

    /**
     * @var OrderContextService
     */
    private $orderContextService;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var OfferCartStateServiceInterface
     */
    private $offerCartStateService;

    public function __construct(
        OfferRepository $offerRepository,
        OfferLineItemReferenceRepository $offerLineItemReferenceRepository,
        LineItemListRepository $lineItemListRepository,
        CurrencyService $currencyService,
        GridHelper $gridHelper,
        OfferService $offerService,
        OfferLineItemReferenceCrudService $offerLineItemReferenceCrudService,
        StorageInterface $sessionStorage,
        OfferDiscountService $offerDiscountService,
        LineItemShopWriterServiceInterface $lineItemShopWriterService,
        AuthenticationService $authenticationService,
        OrderContextService $orderContextService,
        OrderContextRepository $orderContextRepository,
        OfferCartStateServiceInterface $offerCartStateService
    ) {
        $this->offerRepository = $offerRepository;
        $this->offerLineItemReferenceRepository = $offerLineItemReferenceRepository;
        $this->currencyService = $currencyService;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->gridHelper = $gridHelper;
        $this->offerService = $offerService;
        $this->offerLineItemReferenceCrudService = $offerLineItemReferenceCrudService;
        $this->sessionStorage = $sessionStorage;
        $this->offerDiscountService = $offerDiscountService;
        $this->lineItemShopWriterService = $lineItemShopWriterService;
        $this->authenticationService = $authenticationService;
        $this->orderContextService = $orderContextService;
        $this->orderContextRepository = $orderContextRepository;
        $this->offerCartStateService = $offerCartStateService;
    }

    /**
     * @throws UnexpectedOfferStateException
     * @throws B2bControllerRedirectException
     */
    public function indexAction(Request $request): array
    {
        $offerId = $request->getIdValue('offerId');

        $currencyContext = $this->currencyService->createCurrencyContext();

        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        try {
            $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $ownershipContext);
        } catch (NotFoundException $e) {
            throw new B2bControllerRedirectToCheckoutConfirm();
        }

        if ($offer->status !== OfferEntity::STATE_OPEN) {
            throw new UnexpectedOfferStateException('Unsupported Mode');
        }

        return ['offerId' => $offerId->getValue()];
    }

    public function gridAction(Request $request): array
    {
        $offerId = $request->requireIdValue('offerId');

        $currencyContext = $this->currencyService->createCurrencyContext();
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        $lineItemList = $this->lineItemListRepository
            ->fetchOneListById($offer->listId, $currencyContext, $ownershipContext);

        $searchStruct = new LineItemReferenceSearchStruct();

        $this->gridHelper
            ->extractSearchDataInStoreFront($request, $searchStruct);

        $searchStruct->offset = 0;
        $searchStruct->limit = PHP_INT_MAX;

        $items = $this->offerLineItemReferenceRepository
            ->fetchListByLineItemList($lineItemList, $searchStruct, $ownershipContext);

        $totalCount = $this->offerLineItemReferenceRepository
            ->fetchTotalCount($offer->listId, $searchStruct, $ownershipContext);

        $currentPage = $this->gridHelper
            ->getCurrentPage($request);

        $maxPage = $this->gridHelper
            ->getMaxPage($totalCount);

        $gridState = $this->gridHelper
            ->getGridState($request, $searchStruct, $items, $currentPage, $maxPage);

        $validationResponse = $this->gridHelper
            ->getValidationResponse('lineItemReference');

        return array_merge(
            [
                'gridState' => $gridState,
                'offer' => $offer,
                'discountMessage' => (bool) $request->getParam('discountMessage'),
            ],
            $this->getHeaderData($lineItemList),
            $validationResponse
        );
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function updateAction(Request $request): void
    {
        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authenticationService->getIdentity();

        $offerId = $request->requireIdValue('offerId');
        $listId = $request->requireIdValue('listId');

        $post = $request->getPost();
        $post['discountAmountNet'] = (float) $request->getParam('discountAmountNet');

        $crudRequest = $this->offerLineItemReferenceCrudService
            ->createUpdateCrudRequest($post);

        try {
            $this->offerLineItemReferenceCrudService
                ->updateLineItem($listId, $offerId, $crudRequest, $currencyContext, $identity);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
        }

        $discountMessage = false;
        try {
            $this->offerDiscountService->checkOfferDiscountGreaterThanAmount($offerId, $currencyContext, $identity);
        } catch (DiscountGreaterThanAmountException $e) {
            $discountMessage = true;
        }

        throw new B2bControllerForwardException(
            'grid',
            null,
            [
                'offerId' => $offerId->getValue(),
                'discountMessage' => $discountMessage,
            ]
        );
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function removeAction(Request $request): void
    {
        $request->checkPost();

        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authenticationService->getIdentity();

        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $offerId = $request->getIdValue('offerId');
        $lineItemId = $request->getIdValue('lineItemId');

        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        $this->offerLineItemReferenceCrudService
            ->deleteLineItem($offerId, $offer->listId, $lineItemId, $currencyContext, $identity);

        $discountMessage = false;
        try {
            $this->offerDiscountService->checkOfferDiscountGreaterThanAmount($offerId, $currencyContext, $identity);
        } catch (DiscountGreaterThanAmountException $e) {
            $discountMessage = true;
        }

        throw new B2bControllerForwardException(
            'grid',
            null,
            [
                'id' => $offerId->getValue(),
                'discountMessage' => $discountMessage,
            ]
        );
    }

    public function newAction(Request $request): array
    {
        $offerId = $request->requireIdValue('offerId');

        $currencyContext = $this->currencyService->createCurrencyContext();

        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        $validationResponse = $this->gridHelper
            ->getValidationResponse('lineItemReference');

        return array_merge(
            ['offer' => $offer],
            $validationResponse
        );
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function createAction(Request $request): void
    {
        try {
            $this->createLineItemReferenceFromRequest($request);
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
        }

        throw new B2bControllerForwardException('new', null, ['offerId' => $request->requireParam('offerId')]);
    }

    /**
     * @internal
     */
    protected function createLineItemReferenceFromRequest(Request $request): LineItemReference
    {
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $request->checkPost();
        $currencyContext = $this->currencyService->createCurrencyContext();
        $identity = $this->authenticationService->getIdentity();
        $offerId = $request->requireIdValue('offerId');

        $offer = $this->offerRepository
            ->fetchOfferById($offerId, $currencyContext, $ownershipContext);

        $crudRequest = $this->offerLineItemReferenceCrudService
            ->createCreateCrudRequest($request->getPost());

        $reference = $this->offerLineItemReferenceCrudService
            ->addLineItem($offer->listId, $offerId, $crudRequest, $currencyContext, $identity);

        return $reference;
    }

    /**
     * @throws B2bControllerForwardException
     */
    public function updateDiscountAction(Request $request): void
    {
        $request->checkPost();

        $offerId = $request->requireIdValue('offerId');
        $discount = (float) $request->getParam('discount');

        $isPercentageDiscount = (bool) $request->getParam('isPercentageDiscount');

        $currencyContext = $this->currencyService->createCurrencyContext();

        $identity = $this->authenticationService->getIdentity();

        try {
            $this->offerDiscountService->updateDiscount(
                $offerId,
                $currencyContext,
                $discount,
                $identity,
                false,
                $isPercentageDiscount
            );
        } catch (ValidationException $e) {
            $this->gridHelper->pushValidationException($e);
        }

        throw new B2bControllerForwardException('grid', null, ['id' => $offerId->getValue()]);
    }

    /**
     * @internal
     */
    protected function getHeaderData(LineItemList $list): array
    {
        return [
            'itemCount' => count($list->references),
            'amountNet' => $list->amountNet,
            'amount' => $list->amount,
        ];
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function sendOfferAction(Request $request): void
    {
        $offerId = $request->getIdValue('offerId');
        $comment = $request->getParam('comment');

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $identity = $this->authenticationService->getIdentity();

        $this->offerService->sendOfferToAdmin($offerId, $currencyContext, $identity);

        $offer = $this->offerRepository->fetchOfferById($offerId, $currencyContext, $identity->getOwnershipContext());

        $orderContext = $this->orderContextRepository
            ->fetchOneOrderContextById($offer->orderContextId, $identity->getOwnershipContext());

        $this->orderContextService
            ->saveComment((string) $comment, $orderContext);

        $this->sessionStorage->set('showSendToAdminMessage', true);

        throw new B2bControllerRedirectException('index', 'b2boffer');
    }

    /**
     * @throws B2bControllerRedirectException
     */
    public function backToCheckoutAction(Request $request): void
    {
        $offerId = $request->getIdValue('offerId');

        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $ownershipContext = $this->authenticationService
            ->getIdentity()
            ->getOwnershipContext();

        try {
            $offer = $this->offerRepository
                ->fetchOfferById($offerId, $currencyContext, $ownershipContext);

            $list = $this->lineItemListRepository
                ->fetchOneListById($offer->listId, $currencyContext, $ownershipContext);

            $this->lineItemShopWriterService
                ->triggerCart($list, true);

            $this->offerCartStateService->clearOfferByOldState($offer, $ownershipContext);
        } catch (NotFoundException $notFoundException) {
            // nth
        }

        throw new B2bControllerRedirectToCheckoutConfirm();
    }
}
