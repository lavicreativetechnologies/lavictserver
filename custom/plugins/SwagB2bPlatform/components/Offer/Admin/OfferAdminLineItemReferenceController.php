<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Admin;

use InvalidArgumentException;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceSearchStruct;
use Shopware\B2B\Offer\Framework\OfferBackendAuthenticationService;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceRepository;
use Shopware\B2B\Offer\Framework\OfferLineItemReferenceService;
use Shopware\B2B\Offer\Framework\OfferRepository;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use function get_class;
use function is_subclass_of;

/**
 * @RouteScope(scopes={"api"})
 */
class OfferAdminLineItemReferenceController
{
    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @var OfferLineItemReferenceRepository
     */
    private $offerLineItemReferenceRepository;

    /**
     * @var OfferLineItemReferenceService
     */
    private $offerLineItemReferenceService;

    /**
     * @var OfferBackendAuthenticationService
     */
    private $authenticationService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var SalesChannelContextFactory
     */
    private $salesChannelContextFactory;

    public function __construct(
        CurrencyService $currencyService,
        OfferRepository $offerRepository,
        OfferLineItemReferenceRepository $offerLineItemReferenceRepository,
        OfferLineItemReferenceService $offerLineItemReferenceService,
        OfferBackendAuthenticationService $authenticationService,
        AuthStorageAdapterInterface $authStorageAdapter,
        ContextProvider $contextProvider,
        $salesChannelContextFactory
    ) {
        $this->currencyService = $currencyService;
        $this->offerRepository = $offerRepository;
        $this->offerLineItemReferenceRepository = $offerLineItemReferenceRepository;
        $this->offerLineItemReferenceService = $offerLineItemReferenceService;
        $this->authenticationService = $authenticationService;
        $this->authStorageAdapter = $authStorageAdapter;
        $this->contextProvider = $contextProvider;
        $this->salesChannelContextFactory = $salesChannelContextFactory;
    }

    /**
     * @Route(
     *     "/api/v{version}/_action/offer-line-item-reference/{offerId}",
     *     name="api.action.b2bofferlineitemreference.getAllPositions.version",
     *     methods={"GET"}
     * )
     * @Route(
     *     "/api/_action/offer-line-item-reference/{offerId}",
     *     name="api.action.b2bofferlineitemreference.getAllPositions",
     *     methods={"GET"}
     * )
     */
    public function getAllPositionsAction(Request $request): JsonResponse
    {
        $offerId = $request->requireIdValue('offerId');
        $identity = $this->authenticationService->getIdentityByOfferId($offerId);
        $this->authStorageAdapter->setIdentity($identity);
        $this->setSalesChannelContextWithCustomer($identity);

        $context = $this->currencyService->createCurrencyContext();

        $offer = $this->offerRepository->fetchOfferById($offerId, $context, $identity->getOwnershipContext());

        $searchStruct = new LineItemReferenceSearchStruct();
        $searchStruct->orderBy = 'id';

        $references = $this->offerLineItemReferenceService->fetchLineItemsReferencesWithProductNames(
            $offer->listId,
            $searchStruct,
            $identity->getOwnershipContext()
        );

        $count = $this->offerLineItemReferenceRepository->fetchTotalCount($offer->listId, $searchStruct, $identity->getOwnershipContext());

        return new JsonResponse(['success' => true, 'data' => $references, 'count' => $count]);
    }

    /**
     * @internal
     */
    protected function setSalesChannelContextWithCustomer(Identity $identity): void
    {
        $entity = $identity->getEntity();

        if ($entity instanceof DebtorEntity && !is_subclass_of($entity, DebtorEntity::class)) {
            $subshopID = $entity->subshopID;
        } elseif ($entity instanceof ContactEntity && !is_subclass_of($entity, ContactEntity::class)) {
            $subshopID = $entity->debtor->subshopID;
        } else {
            throw new InvalidArgumentException('Unknown identity type "' . get_class($identity) . '"');
        }

        $this->contextProvider->setSalesChannelContext(
            $this->salesChannelContextFactory->create(
                Uuid::randomHex(),
                $subshopID->getValue(),
                [
                    SalesChannelContextService::CUSTOMER_ID => $identity->getOwnershipContext()->shopOwnerUserId->getValue(),
                ]
            )
        );
    }
}
