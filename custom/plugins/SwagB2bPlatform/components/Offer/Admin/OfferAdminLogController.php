<?php declare(strict_types=1);

namespace Shopware\B2B\Offer\Admin;

use Shopware\B2B\AuditLog\Framework\AuditLogEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogRepository;
use Shopware\B2B\AuditLog\Framework\AuditLogSearchStruct;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\Offer\Framework\OfferService;
use Shopware\B2B\Order\Framework\OrderBackendAuthenticationService;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class OfferAdminLogController
{
    /**
     * @var AuditLogRepository
     */
    private $auditLogRepository;

    /**
     * @var GridHelper
     */
    private $auditLogGridHelper;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var OfferService
     */
    private $offerService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var OrderBackendAuthenticationService
     */
    private $orderAuthenticationService;

    public function __construct(
        AuditLogRepository $auditLogRepository,
        GridHelper $auditLogGridHelper,
        OrderContextRepository $orderContextRepository,
        OfferService $offerService,
        CurrencyService $currencyService,
        OrderBackendAuthenticationService $orderAuthenticationService
    ) {
        $this->auditLogRepository = $auditLogRepository;
        $this->auditLogGridHelper = $auditLogGridHelper;
        $this->orderContextRepository = $orderContextRepository;
        $this->offerService = $offerService;
        $this->currencyService = $currencyService;
        $this->orderAuthenticationService = $orderAuthenticationService;
    }

    /**
     * @Route("/api/v{version}/_action/offer-log/{orderContextId}", name="api.action.b2bofferlog.activity.version",
     *     methods={"GET"})
     * @Route("/api/_action/offer-log/{orderContextId}", name="api.action.b2bofferlog.activity",
     *     methods={"GET"})
     */
    public function activityAction(Request $request): JsonResponse
    {
        $orderContextId = $request->requireIdValue('orderContextId');

        $currencyContext = $this->currencyService->createCurrencyContext();

        $searchStruct = new AuditLogSearchStruct();
        $searchStruct->orderBy = 'auditLog.id';

        $this->auditLogGridHelper->extractSearchDataInAdmin($request, $searchStruct);

        $logItems = $this->auditLogRepository
            ->fetchList(OrderContextRepository::TABLE_NAME, $orderContextId, $searchStruct, $currencyContext);

        $totalCount = $this->auditLogRepository
            ->fetchTotalCount(OrderContextRepository::TABLE_NAME, $orderContextId, $searchStruct);

        return new JsonResponse([
            'success' => true,
            'data' => $this->groupLogItemsByAuthorsAndWriteWithId($logItems),
            'count' => $totalCount,
        ]);
    }

    /**
     * @internal
     */
    protected function groupLogItemsByAuthorsAndWriteWithId(array $logItems): array
    {
        $groupedItems = [];

        /** @var AuditLogEntity $logItem */
        foreach ($logItems as $logItem) {
            if (!$logItem->writeWithId instanceof NullIdValue) {
                $activityKey = $logItem->writeWithId . '_' . $logItem->authorHash;
            } else {
                $activityKey = $logItem->id . '_' . $logItem->authorHash;
            }

            if (!isset($groupedItems[$activityKey])) {
                $groupedItems[$activityKey] = [];
            }

            $groupedItems[$activityKey][] = $logItem;
        }

        return $groupedItems;
    }

    /**
     * @Route("/api/v{version}/_action/offer-log/{orderContextId}/comment", name="api.action.b2bofferlog.comment.version",
     *     methods={"POST"})
     * @Route("/api/_action/offer-log/{orderContextId}/comment", name="api.action.b2bofferlog.comment",
     *     methods={"POST"})
     */
    public function commentAction(Request $request): JsonResponse
    {
        $orderContextId = $request->requireIdValue('orderContextId');
        $comment = $request->requireParam('comment');
        $identity = $this->orderAuthenticationService->getIdentityByOrderContextId($orderContextId);

        $orderContext = $this->orderContextRepository
            ->fetchOneOrderContextById($orderContextId, $identity->getOwnershipContext());

        $this->offerService
            ->saveComment($comment, $orderContext, $identity, true);

        return new JsonResponse(['success' => true]);
    }
}
