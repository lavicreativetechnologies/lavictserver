<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Frontend\DependencyInjection;

use Shopware\B2B\Address\Framework\DependencyInjection\AddressFrameworkConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Company\Frontend\DependencyInjection\CompanyFrontendConfiguration;
use Shopware\B2B\Contact\Framework\DependencyInjection\ContactFrameworkConfiguration;
use Shopware\B2B\Role\Framework\DependencyInjection\RoleFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AddressFrontendConfiguration extends DependencyInjectionConfiguration
{
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/frontend-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            new RoleFrameworkConfiguration(),
            new ContactFrameworkConfiguration(),
            new AddressFrameworkConfiguration(),
            new CompanyFrontendConfiguration(),
        ];
    }
}
