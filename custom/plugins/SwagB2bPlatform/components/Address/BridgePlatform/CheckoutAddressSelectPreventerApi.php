<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerRecovery\CustomerRecoveryEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\DataBag;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @RouteScope(scopes={"api"})
 */
class CheckoutAddressSelectPreventerApi extends AccountService
{
    /**
     * @var AccountService
     */
    private $decorated;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AccountService $decorated, AuthenticationService $authenticationService)
    {
        $this->decorated = $decorated;
        $this->authenticationService = $authenticationService;
    }

    public function getCustomerByEmail(string $email, SalesChannelContext $context, bool $includeGuest = false): CustomerEntity
    {
        return $this->decorated->getCustomerByEmail($email, $context, $includeGuest);
    }

    public function getCustomersByEmail(string $email, SalesChannelContext $context, bool $includeGuests = true): EntitySearchResult
    {
        return $this->decorated->getCustomersByEmail($email, $context, $includeGuests);
    }

    public function saveProfile(DataBag $data, SalesChannelContext $context): void
    {
        $this->decorated->saveProfile($data, $context);
    }

    public function saveEmail(DataBag $data, SalesChannelContext $context): void
    {
        $this->decorated->saveEmail($data, $context);
    }

    public function generateAccountRecovery(DataBag $data, SalesChannelContext $context): void
    {
        $this->decorated->generateAccountRecovery($data, $context);
    }

    public function resetPassword(DataBag $data, SalesChannelContext $context): bool
    {
        return $this->decorated->resetPassword($data, $context);
    }

    public function checkHash(string $hash, Context $context): bool
    {
        return $this->decorated->checkHash($hash, $context);
    }

    public function setDefaultBillingAddress(string $addressId, SalesChannelContext $context): void
    {
        if ($this->authenticationService->isB2b()) {
            throw new NotFoundHttpException('Not found');
        }
        $this->decorated->setDefaultBillingAddress($addressId, $context);
    }

    public function setDefaultShippingAddress(string $addressId, SalesChannelContext $context): void
    {
        $this->decorated->setDefaultShippingAddress($addressId, $context);
    }

    public function login(string $email, SalesChannelContext $context, bool $includeGuest = false): string
    {
        return $this->decorated->login($email, $context, $includeGuest);
    }

    public function loginWithPassword(DataBag $data, SalesChannelContext $context): string
    {
        return $this->decorated->loginWithPassword($data, $context);
    }

    public function logout(SalesChannelContext $context): void
    {
        $this->decorated->logout($context);
    }

    public function setNewsletterFlag(CustomerEntity $customer, bool $newsletter, SalesChannelContext $context): void
    {
        $this->decorated->setNewsletterFlag($customer, $newsletter, $context);
    }

    public function changeDefaultPaymentMethod(string $paymentMethodId, RequestDataBag $requestDataBag, CustomerEntity $customer, SalesChannelContext $context): void
    {
        $this->decorated->changeDefaultPaymentMethod($paymentMethodId, $requestDataBag, $customer, $context);
    }

    public function getCustomerByLogin(string $email, string $password, SalesChannelContext $context): CustomerEntity
    {
        return $this->decorated->getCustomerByLogin($email, $password, $context);
    }

    public function getCustomerRecovery(Criteria $criteria, Context $context): ?CustomerRecoveryEntity
    {
        return $this->decorated->getCustomerRecovery($criteria, $context);
    }
}
