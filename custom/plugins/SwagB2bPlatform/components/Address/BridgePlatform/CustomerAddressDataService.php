<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\Core\Checkout\Customer\CustomerEntity;

class CustomerAddressDataService
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    public function __construct(
        AddressRepository $addressRepository
    ) {
        $this->addressRepository = $addressRepository;
    }

    public function synchronizeCustomerAddresses(CustomerEntity $customer): void
    {
        $customerId = IdValue::create($customer->getId());

        try {
            $addressIds = $this->addressRepository
                ->fetchNonB2bCustomerAddressIdsByCustomerId($customerId);
        } catch (NotFoundException $e) {
            return;
        }

        $addresses = $this->createAddresses($addressIds, $customer);

        $this->addressRepository
            ->addAddressesIntoCustomerAddressData($addresses);
    }

    /**
     * @internal
     * @return AddressEntity[]
     */
    protected function createAddresses(
        array $addressIds,
        CustomerEntity $customer
    ): array {
        $defaultShippingId = $customer->getDefaultShippingAddressId();
        $defaultBillingId = $customer->getDefaultBillingAddressId();

        $addresses = [];
        foreach ($addressIds as $addressId) {
            $addresses[] = $this->createAddressEntity(
                $addressId,
                $defaultShippingId,
                $defaultBillingId
            );
        }

        return $addresses;
    }

    /**
     * @internal
     */
    protected function createAddressEntity(
        string $addressId,
        string $defaultShippingId,
        string $defaultBillingId
    ): AddressEntity {
        $address = new AddressEntity();
        $address->id = IdValue::create($addressId);
        $address->type = $this->getB2bType($addressId, $defaultShippingId, $defaultBillingId);

        return $address;
    }

    /**
     * @internal
     */
    protected function getB2bType(
        string $addressId,
        string $defaultShippingId,
        string $defaultBillingId
    ): string {
        switch ($addressId) {
            case $defaultShippingId:
                return AddressEntity::TYPE_SHIPPING;

            case $defaultBillingId:
            default:
                return AddressEntity::TYPE_BILLING;
        }
    }
}
