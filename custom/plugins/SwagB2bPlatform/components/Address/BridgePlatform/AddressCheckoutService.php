<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use BadMethodCallException;
use Shopware\B2B\Address\Framework\AddressCheckoutServiceInterface;
use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextService;

class AddressCheckoutService implements AddressCheckoutServiceInterface
{
    /**
     * @var ContextProvider
     */
    private $channelContextProvider;

    /**
     * @var EntityRepository
     */
    private $addressRepository;

    /**
     * @var SalesChannelContextPersister
     */
    private $contextPersister;

    public function __construct(
        EntityRepository $addressRepository,
        SalesChannelContextPersister $contextPersister,
        ContextProvider $channelContextProvider
    ) {
        $this->contextPersister = $contextPersister;
        $this->addressRepository = $addressRepository;
        $this->channelContextProvider = $channelContextProvider;
    }

    public function updateCheckoutAddress(string $type, AddressEntity $address): void
    {
        $salesChannelContext = $this->channelContextProvider
            ->getSalesChannelContext();

        if (!$salesChannelContext->getCustomer()) {
            throw new BadMethodCallException('Unable to change active address without customer');
        }

        $criteria = new Criteria([$address->id->getValue()]);
        $criteria->addAssociation('country');

        /** @var CustomerAddressEntity $customerAddressEntity */
        $customerAddressEntity = $this->addressRepository
            ->search($criteria, $salesChannelContext->getContext())
            ->first();

        if ($type === AddressEntity::TYPE_BILLING) {
            $paramsToPersist = [SalesChannelContextService::BILLING_ADDRESS_ID => $customerAddressEntity->getId()];

            $salesChannelContext->getCustomer()
                ->setActiveBillingAddress($customerAddressEntity);
        } else {
            $paramsToPersist = [SalesChannelContextService::SHIPPING_ADDRESS_ID => $customerAddressEntity->getId()];

            $salesChannelContext->getCustomer()
                ->setActiveShippingAddress($customerAddressEntity);
        }

        $this->contextPersister
            ->save(
                $salesChannelContext->getToken(),
                $paramsToPersist,
                $salesChannelContext->getSalesChannel()->getId(),
                $salesChannelContext->getCustomer()->getId()
            );
    }
}
