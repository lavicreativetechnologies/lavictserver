<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Address\Framework\ShippingAddressRepositoryInterface;
use Shopware\B2B\Common\IdValue;

class ShippingAddressRepository implements ShippingAddressRepositoryInterface
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    public function fetchShippingIdByUserId(IdValue $customerId): IdValue
    {
        return IdValue::create($this->connection->createQueryBuilder()
            ->select('default_shipping_address_id')
            ->from('customer')
            ->where('id = :customerId')
            ->setParameter('customerId', $customerId->getStorageValue())
            ->execute()
            ->fetchColumn());
    }
}
