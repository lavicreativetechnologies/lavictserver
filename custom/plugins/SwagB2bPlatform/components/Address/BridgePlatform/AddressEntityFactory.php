<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\Address\Framework\AddressEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\Core\Framework\Uuid\Uuid;

class AddressEntityFactory
{
    public function create(array $addressData): AddressEntity
    {
        $address = new AddressEntity();
        $address->id = IdValue::create($addressData['id']);
        $address->user_id = IdValue::create($addressData['customer_id']);
        $address->company = $addressData['company'];
        $address->department = $addressData['department'];
        $address->salutation = $addressData['salutation'];
        $address->title = $addressData['title'];
        $address->firstname = $addressData['first_name'];
        $address->lastname = $addressData['last_name'];
        $address->street = $addressData['street'];
        $address->zipcode = $addressData['zipcode'];
        $address->city = $addressData['city'];
        $address->country_id = IdValue::create($addressData['country_id']);
        $address->state_id = IdValue::create($addressData['country_state_id']);
        $address->ustid = $addressData['vat_id'];
        $address->phone = $addressData['phone_number'];
        $address->additional_address_line1 = $addressData['additional_address_line1'];
        $address->additional_address_line2 = $addressData['additional_address_line2'];
        $address->is_used = (bool) $addressData['is_used'];
        $address->type = $addressData['type'];

        return $address;
    }

    public function toDatabaseArray(AddressEntity $addressEntity): array
    {
        $data['id'] = $addressEntity->id ? $addressEntity->id->getStorageValue() : Uuid::randomBytes();
        $data['customer_id'] = $addressEntity->user_id->getStorageValue();
        $data['country_id'] = $addressEntity->country_id->getStorageValue();
        $data['company'] = $addressEntity->company;
        $data['department'] = $addressEntity->department;
        $data['title'] = $addressEntity->title;
        $data['first_name'] = $addressEntity->firstname;
        $data['last_name'] = $addressEntity->lastname;
        $data['street'] = $addressEntity->street;
        $data['zipcode'] = $addressEntity->zipcode;
        $data['city'] = $addressEntity->city;
        $data['vat_id'] = $addressEntity->ustid;
        $data['phone_number'] = $addressEntity->phone;
        $data['additional_address_line1'] = $addressEntity->additional_address_line1;
        $data['additional_address_line2'] = $addressEntity->additional_address_line2;

        return $data;
    }
}
