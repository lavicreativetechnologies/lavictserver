<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Routing\Annotation\LoginRequired;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\AddressController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteScope(scopes={"storefront"})
 */
class CheckoutAddressSelectPreventerStorefrontV640 extends AddressController
{
    /**
     * @var AddressController
     */
    private $decorated;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AddressController $decorated, AuthenticationService $authenticationService)
    {
        $this->decorated = $decorated;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @LoginRequired()
     */
    public function addressBook(Request $request, RequestDataBag $dataBag, SalesChannelContext $context, CustomerEntity $customer): Response
    {
        if ($this->authenticationService->isB2B()) {
            throw $this->decorated->createNotFoundException();
        }

        return $this->decorated->addressBook($request, $dataBag, $context, $customer);
    }

    /**
     * @LoginRequired()
     */
    public function accountAddressOverview(Request $request, SalesChannelContext $context, CustomerEntity $customer): Response
    {
        return $this->decorated->accountAddressOverview($request, $context, $customer);
    }

    /**
     * @LoginRequired()
     */
    public function accountCreateAddress(Request $request, RequestDataBag $data, SalesChannelContext $context, CustomerEntity $customer): Response
    {
        if ($this->authenticationService->isB2B()) {
            throw $this->decorated->createNotFoundException();
        }

        return $this->decorated->accountCreateAddress($request, $data, $context, $customer);
    }

    /**
     * @LoginRequired()
     */
    public function accountEditAddress(Request $request, SalesChannelContext $context, CustomerEntity $customer): Response
    {
        return $this->decorated->accountEditAddress($request, $context, $customer);
    }

    /**
     * @LoginRequired()
     */
    public function switchDefaultAddress(string $type, string $addressId, SalesChannelContext $context, CustomerEntity $customer): RedirectResponse
    {
        return $this->decorated->switchDefaultAddress($type, $addressId, $context, $customer);
    }

    /**
     * @LoginRequired()
     */
    public function deleteAddress(string $addressId, SalesChannelContext $context, CustomerEntity $customer): Response
    {
        return $this->decorated->deleteAddress($addressId, $context, $customer);
    }

    /**
     * @LoginRequired()
     */
    public function saveAddress(RequestDataBag $data, SalesChannelContext $context, CustomerEntity $customer): Response
    {
        if ($this->authenticationService->isB2B()) {
            throw $this->createNotFoundException();
        }

        return $this->decorated->saveAddress($data, $context, $customer);
    }
}
