<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\AddressController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteScope(scopes={"storefront"})
 */
class CheckoutAddressSelectPreventerStorefront extends AddressController
{
    /**
     * @var AddressController
     */
    private $decorated;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AddressController $decorated, AuthenticationService $authenticationService)
    {
        $this->decorated = $decorated;
        $this->authenticationService = $authenticationService;
    }

    public function addressBook(Request $request, RequestDataBag $dataBag, SalesChannelContext $context): Response
    {
        if ($this->authenticationService->isB2B()) {
            throw $this->decorated->createNotFoundException();
        }

        return $this->decorated->addressBook($request, $dataBag, $context);
    }

    public function accountAddressOverview(Request $request, SalesChannelContext $context): Response
    {
        return $this->decorated->accountAddressOverview($request, $context);
    }

    public function accountCreateAddress(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
        if ($this->authenticationService->isB2B()) {
            throw $this->decorated->createNotFoundException();
        }

        return $this->decorated->accountCreateAddress($request, $data, $context);
    }

    public function accountEditAddress(Request $request, SalesChannelContext $context): Response
    {
        return $this->decorated->accountEditAddress($request, $context);
    }

    public function switchDefaultAddress(string $type, string $addressId, SalesChannelContext $context): RedirectResponse
    {
        return $this->decorated->switchDefaultAddress($type, $addressId, $context);
    }

    public function deleteAddress(string $addressId, SalesChannelContext $context): Response
    {
        return $this->decorated->deleteAddress($addressId, $context);
    }

    public function saveAddress(RequestDataBag $data, SalesChannelContext $context): Response
    {
        if ($this->authenticationService->isB2B()) {
            throw $this->createNotFoundException();
        }

        return $this->decorated->saveAddress($data, $context);
    }
}
