<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Address\Framework\CountryRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\BridgePlatform\TranslatedFieldQueryExtenderTrait;

class CountryRepository implements CountryRepositoryInterface
{
    use TranslatedFieldQueryExtenderTrait;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    public function __construct(
        Connection $connection,
        ContextProvider $contextProvider
    ) {
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
    }

    public function getCountryList(): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('LOWER(HEX(id))')
            ->from('country', 'c')
            ->where('active = 1')
            ->orderBy('name');

        $this->addTranslatedFieldSelect(
            $query,
            'name',
            'name',
            'country_translation',
            'country',
            'c',
            'id',
            $this->contextProvider->getSalesChannelContext()
        );

        return $query->execute()->fetchAll(PDO::FETCH_KEY_PAIR);
    }
}
