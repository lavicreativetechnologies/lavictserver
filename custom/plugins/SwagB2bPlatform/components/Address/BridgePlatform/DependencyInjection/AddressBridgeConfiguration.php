<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform\DependencyInjection;

use Shopware\B2B\Acl\Framework\DependencyInjection\AclFrameworkConfiguration;
use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\Company\Framework\DependencyInjection\CompanyFrameworkConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AddressBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): self
    {
        return new static();
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        $services = [
            __DIR__ . '/bridge-services.xml',
        ];

        if (ShopwareVersion::isShopware6_4_X()) {
            $services[] = __DIR__ . '/bridge-services-v640.xml';
        } elseif (ShopwareVersion::isShopware6_3_2()) {
            $services[] = __DIR__ . '/bridge-services-v632.xml';
        } else {
            $services[] = __DIR__ . '/bridge-services-v6.xml';
        }

        return $services;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompilerPasses(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getDependingConfigurations(): array
    {
        return [
            new AclFrameworkConfiguration(),
            new CompanyFrameworkConfiguration(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
        ];
    }
}
