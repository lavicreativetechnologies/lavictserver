<?php declare(strict_types=1);

namespace Shopware\B2B\Address\BridgePlatform;

use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Customer\SalesChannel\AbstractUpsertAddressRoute;
use Shopware\Core\Checkout\Customer\SalesChannel\UpsertAddressRouteResponse;
use Shopware\Core\Framework\Routing\Annotation\LoginRequired;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @RouteScope(scopes={"api"})
 */
class CheckoutAddressSelectPreventerApiV640 extends AbstractUpsertAddressRoute
{
    /**
     * @var AbstractUpsertAddressRoute
     */
    private $decorated;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    public function __construct(AbstractUpsertAddressRoute $decorated, AuthenticationService $authenticationService)
    {
        $this->decorated = $decorated;
        $this->authenticationService = $authenticationService;
    }

    public function getDecorated(): AbstractUpsertAddressRoute
    {
        return $this->decorated;
    }

    /**
     * @LoginRequired()
     */
    public function upsert(?string $addressId, RequestDataBag $data, SalesChannelContext $context, CustomerEntity $customer): UpsertAddressRouteResponse
    {
        if ($this->authenticationService->isB2b()) {
            throw new NotFoundHttpException('Not found');
        }

        return $this->decorated->upsert($addressId, $data, $context, $customer);
    }
}
