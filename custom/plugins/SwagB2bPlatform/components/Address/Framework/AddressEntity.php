<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use function get_object_vars;
use function property_exists;

/**
 * Represents an address. As an entity this is used in CRUD & listing operations.
 */
class AddressEntity implements CrudEntity
{
    const TYPE_BILLING = 'billing';
    const TYPE_SHIPPING = 'shipping';

    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var IdValue
     */
    public $user_id;

    /**
     * @var string
     */
    public $company;

    /**
     * @var string
     */
    public $department;

    /**
     * @var string
     */
    public $salutation;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $firstname;

    /**
     * @var string
     */
    public $lastname;

    /**
     * @var string
     */
    public $street;

    /**
     * @var string
     */
    public $zipcode;

    /**
     * @var string
     */
    public $city;

    /**
     * @var IdValue
     */
    public $country_id;

    /**
     * @var IdValue
     */
    public $state_id;

    /**
     * @var string
     */
    public $ustid;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $additional_address_line1;

    /**
     * @var string
     */
    public $additional_address_line2;

    /**
     * @var bool
     */
    public $is_used = false;

    /**
     * @var string
     */
    public $type;

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->user_id = IdValue::null();
        $this->country_id = IdValue::null();
        $this->state_id = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->user_id = IdValue::create($this->user_id);
        $this->country_id = IdValue::create($this->country_id);
        $this->state_id = IdValue::create($this->state_id);
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'user_id' => $this->user_id->getStorageValue(),
            'company' => $this->company,
            'department' => $this->department,
            'salutation' => $this->salutation,
            'title' => $this->title,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'street' => $this->street,
            'zipcode' => $this->zipcode,
            'city' => $this->city,
            'country_id' => $this->country_id->getStorageValue(),
            'state_id' => $this->state_id->getStorageValue(),
            'ustid' => $this->ustid,
            'phone' => $this->phone,
            'additional_address_line1' => $this->additional_address_line1,
            'additional_address_line2' => $this->additional_address_line2,
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->user_id = IdValue::create($data['user_id']);
        $this->company = $data['company'];
        $this->department = $data['department'];
        $this->salutation = $data['salutation'];
        $this->title = $data['title'];
        $this->firstname = $data['firstname'];
        $this->lastname = $data['lastname'];
        $this->street = $data['street'];
        $this->zipcode = $data['zipcode'];
        $this->city = $data['city'];
        $this->country_id = IdValue::create($data['country_id']);
        $this->state_id = IdValue::create($data['state_id']);
        $this->ustid = $data['ustid'];
        $this->phone = $data['phone'];
        $this->additional_address_line1 = $data['additional_address_line1'];
        $this->additional_address_line2 = $data['additional_address_line2'];
        $this->is_used = isset($data['is_used']) ? (bool) $data['is_used'] : false;
        $this->type = isset($data['type']) ? $data['type'] : null;

        return $this;
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
