<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Acl\Framework\AclAccessWriterInterface;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Service\AbstractCrudService;
use Shopware\B2B\Common\Service\CrudServiceRequest;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class AddressCrudService extends AbstractCrudService
{
    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var AddressValidationService
     */
    private $validationService;

    /**
     * @var AclAccessWriterInterface
     */
    private $aclAccessWriter;

    public function __construct(
        AddressRepositoryInterface $addressRepository,
        AddressValidationService $validationService,
        AclAccessWriterInterface $aclAccessWriter
    ) {
        $this->addressRepository = $addressRepository;
        $this->validationService = $validationService;
        $this->aclAccessWriter = $aclAccessWriter;
    }

    public function createNewRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'company',
                'department',
                'salutation',
                'ustid',
                'firstname',
                'lastname',
                'street',
                'additional_address_line1',
                'additional_address_line2',
                'zipcode',
                'city',
                'country_id',
                'phone',
                'title',
                'state_id',
            ]
        );
    }

    public function createExistingRecordRequest(array $data): CrudServiceRequest
    {
        return new CrudServiceRequest(
            $data,
            [
                'id',
                'company',
                'department',
                'salutation',
                'ustid',
                'firstname',
                'lastname',
                'street',
                'additional_address_line1',
                'additional_address_line2',
                'zipcode',
                'city',
                'country_id',
                'phone',
            ]
        );
    }

    public function create(
        CrudServiceRequest $request,
        OwnershipContext $ownershipContext,
        string $type,
        AclGrantContext $grantContext
    ): AddressEntity {
        $data = $request->getFilteredData();
        $data['user_id'] = $ownershipContext->shopOwnerUserId;

        $address = new AddressEntity();

        $address->setData($data);

        $validation = $this->validationService
            ->createInsertValidation($address);

        $this->testValidation($address, $validation);

        $address = $this->addressRepository
            ->addAddress($address, $type, $ownershipContext);

        $this->aclAccessWriter->addNewSubject(
            $ownershipContext,
            $grantContext,
            $address->id,
            true
        );

        return $address;
    }

    public function update(CrudServiceRequest $request, OwnershipContext $ownershipContext, string $type): AddressEntity
    {
        $data = $request->getFilteredData();
        $address = new AddressEntity();
        $address->setData($data);
        $address->user_id = $ownershipContext->shopOwnerUserId;

        $this->aclAccessWriter->testUpdateAllowed($ownershipContext, $address->id);

        $validation = $this->validationService
            ->createUpdateValidation($address);

        $this->testValidation($address, $validation);

        return $this->addressRepository
            ->updateAddress($address, $ownershipContext, $type);
    }

    public function remove(CrudServiceRequest $request, OwnershipContext $ownershipContext): AddressEntity
    {
        $data = $request->getFilteredData();
        $address = new AddressEntity();
        $address->setData($data);

        if (!$address->id) {
            throw new CanNotRemoveExistingRecordException();
        }

        $this->aclAccessWriter->testUpdateAllowed($ownershipContext, $address->id);

        $this->addressRepository
            ->removeAddress($address, $ownershipContext);

        return $address;
    }
}
