<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Framework;

use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AddressValidationService
{
    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ConfigServiceInterface
     */
    private $configService;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator,
        ConfigServiceInterface $configService
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
        $this->configService = $configService;
    }

    public function createInsertValidation(AddressEntity $address): Validator
    {
        return $this->createCrudValidation($address)
            ->validateThat('id', $address->id)
            ->isNullIdValue()

            ->getValidator($this->validator);
    }

    public function createUpdateValidation(AddressEntity $address): Validator
    {
        return $this->createCrudValidation($address)
            ->validateThat('id', $address->id)
            ->isIdValue()

            ->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function createCrudValidation(AddressEntity $address): ValidationBuilder
    {
        $requiredFields = $this->configService->getRequiredFieldsByAddress($address);

        $validation = $this->validationBuilder

            ->validateThat('salutation', $address->salutation)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_30)

            ->validateThat('firstName', $address->firstname)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_50)

            ->validateThat('lastName', $address->lastname)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_60)

            ->validateThat('company', $address->company)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255)

            ->validateThat('country', $address->country_id)
                ->isNotBlank()

            ->validateThat('street', $address->street)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255)

            ->validateThat('zipcode', $address->zipcode)
                ->isNotBlank()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_50)

            ->validateThat('city', $address->city)
                ->isNotBlank()
                ->isString()
                ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_70);

        foreach ($requiredFields as $key => $field) {
            $validation->validateThat($key, $field)
                ->isNotBlank()
                ->containsNotOnlyWhitespace();
        }

        return $validation;
    }
}
