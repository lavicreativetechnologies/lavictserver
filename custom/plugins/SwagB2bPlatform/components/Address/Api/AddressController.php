<?php declare(strict_types=1);

namespace Shopware\B2B\Address\Api;

use InvalidArgumentException;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Acl\Framework\AclGrantContextProviderChain;
use Shopware\B2B\Address\Framework\AddressCrudService;
use Shopware\B2B\Address\Framework\AddressRepositoryInterface;
use Shopware\B2B\Address\Framework\AddressSearchStruct;
use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class AddressController
{
    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var AddressCrudService
     */
    private $addressCrudService;

    /**
     * @var AclGrantContextProviderChain
     */
    private $grantContextProviderChain;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        AddressRepositoryInterface $addressRepository,
        GridHelper $requestHelper,
        AddressCrudService $addressCrudService,
        AclGrantContextProviderChain $grantContextProviderChain,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->requestHelper = $requestHelper;
        $this->addressRepository = $addressRepository;
        $this->addressCrudService = $addressCrudService;
        $this->grantContextProviderChain = $grantContextProviderChain;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(string $addressType, Request $request): array
    {
        $search = new AddressSearchStruct();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $ownershipContext = $this->getDebtorOwnershipContext();

        $addresses = $this->addressRepository->fetchList($addressType, $ownershipContext, $search);

        $totalCount = $this->addressRepository
            ->fetchTotalCount($addressType, $ownershipContext, $search);

        return ['success' => true, 'addresses' => $addresses, 'totalCount' => $totalCount];
    }

    public function getAction(string $addressId): array
    {
        $addressId = IdValue::create($addressId);
        $identity = $this->authStorageAdapter->getIdentity();

        $address = $this->addressRepository->fetchOneById($addressId, $identity);

        return ['success' => true, 'address' => $address];
    }

    public function createAction(
        string $addressType,
        Request $request
    ): array {
        $ownershipContext = $this->getDebtorOwnershipContext();

        $aclGrantContext = $this->extractGrantContext($request, $ownershipContext);

        $data = $request->getPost();

        $newRecord = $this->addressCrudService
            ->createNewRecordRequest($data);

        $address = $this->addressCrudService
            ->create($newRecord, $ownershipContext, $addressType, $aclGrantContext);

        return ['success' => true, 'address' => $address];
    }

    public function updateAction(
        string $addressId,
        string $addressType,
        Request $request
    ): array {
        $addressId = IdValue::create($addressId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $data = $request->getPost();
        $data['id'] = $addressId;

        $existingRecord = $this->addressCrudService
            ->createExistingRecordRequest($data);

        $address = $this->addressCrudService
            ->update($existingRecord, $ownershipContext, $addressType);

        return ['success' => true, 'address' => $address];
    }

    public function removeAction(string $addressId): array
    {
        $addressId = IdValue::create($addressId);
        $existingRecord = $this->addressCrudService
            ->createExistingRecordRequest(['id' => $addressId]);

        $ownershipContext = $this->getDebtorOwnershipContext();

        $this->addressCrudService->remove($existingRecord, $ownershipContext);

        return ['success' => true, 'removedAddress' => $addressId];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()
            ->getOwnershipContext();
    }

    /**
     * @internal
     * @throws InvalidArgumentException
     */
    protected function extractGrantContext(Request $request, OwnershipContext $ownershipContext): AclGrantContext
    {
        $grantContextIdentifier = $request->requireParam('grantContextIdentifier');

        return $this->grantContextProviderChain->fetchOneByIdentifier($grantContextIdentifier, $ownershipContext);
    }
}
