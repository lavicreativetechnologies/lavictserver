<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\B2B\Shop\BridgePlatform\TranslatedFieldQueryExtenderTrait;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use function sprintf;

class DebtorRepository implements DebtorRepositoryInterface
{
    use TranslatedFieldQueryExtenderTrait;

    const TABLE_NAME = 'customer';

    const TABLE_ALIAS = 'customer';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var DebtorEntityFactory
     */
    private $debtorEntityFactory;

    public function __construct(
        Connection $connection,
        ContextProvider $contextProvider,
        DebtorEntityFactory $debtorEntityFactory
    ) {
        $this->connection = $connection;
        $this->contextProvider = $contextProvider;
        $this->debtorEntityFactory = $debtorEntityFactory;
    }

    public function fetchOneByEmail(string $email): DebtorEntity
    {
        $debtorCustomerData = $this->createBaseQuery()
            ->andWhere(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $email)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$debtorCustomerData) {
            throw new NotFoundException(sprintf('Debtor not found for %s', $email));
        }

        return $this->debtorEntityFactory->create($debtorCustomerData);
    }

    public function fetchOneByCredentials(AbstractCredentialsEntity $credentialsEntity): DebtorEntity
    {
        $email = $credentialsEntity->email;

        $debtorCustomerData = $this->createBaseQuery()
            ->andWhere(self::TABLE_ALIAS . '.email = :email')
            ->setParameter('email', $email)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$debtorCustomerData) {
            throw new NotFoundException(sprintf('Debtor not found for %s', $email));
        }

        return $this->debtorEntityFactory->create($debtorCustomerData);
    }

    public function fetchOneById(IdValue $id): DebtorEntity
    {
        $debtorCustomerData = $this->createBaseQuery()
            ->andWhere(self::TABLE_ALIAS . '.id = :id')
            ->setParameter('id', $id->getStorageValue())
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$debtorCustomerData) {
            throw new NotFoundException(sprintf('Debtor not found for %s', $id->getValue()));
        }

        return $this->debtorEntityFactory->create($debtorCustomerData);
    }

    /**
     * @return DebtorEntity[]
     */
    public function fetchAllDebtors(): array
    {
        $debtorsCustomerData = $this->createBaseQuery()
            ->execute()
            ->fetchAll();

        if (!$debtorsCustomerData) {
            throw new NotFoundException('No debtor found!');
        }

        $entities = [];
        foreach ($debtorsCustomerData as $debtorCustomerData) {
            $entities[] = $this->debtorEntityFactory->create($debtorCustomerData);
        }

        return $entities;
    }

    /**
     * @throws NotFoundException
     */
    public function fetchIdentityById(IdValue $id, LoginContextService $contextService): Identity
    {
        $entity = $this->fetchOneById($id);

        $authId = $contextService->getAuthId(DebtorRepositoryInterface::class, $entity->id);

        return new DebtorIdentity(
            $authId,
            $entity->id,
            self::TABLE_NAME,
            $entity,
            $contextService->getAvatar($authId)
        );
    }

    public function filterActiveDebtor(QueryBuilder $queryBuilder, string $fromAlias, string $idField): void
    {
        $queryBuilder->innerJoin($fromAlias, 'b2b_customer_data', 'b2bcd', 'UNHEX(' . $idField . ') = b2bcd.customer_id')
            ->where('b2bcd.is_debtor = 1');
    }

    public function getTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function setUserAsDebtor(IdValue $userId): void
    {
        $this->connection->update(
            'b2b_customer_data',
            ['is_debtor' => 1],
            ['customer_id' => $userId->getStorageValue()]
        );
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * @internal
     */
    protected function createBaseQuery(): QueryBuilder
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_customer_data', 'b2bCustomerData', self::TABLE_ALIAS . '.id = b2bCustomerData.customer_id')
            ->innerJoin(self::TABLE_ALIAS, 'salutation', 'salutation', self::TABLE_ALIAS . '.salutation_id = salutation.id')
            ->where('b2bCustomerData.is_debtor = 1');

        $this->addTranslatedFieldSelect(
            $query,
            'display_name',
            'salutation_display_name',
            'salutation_translation',
            'salutation',
            'salutation',
            'id',
            $this->contextProvider->getSalesChannelContext()
        );

        return $query;
    }
}
