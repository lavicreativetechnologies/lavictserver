<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Debtor\Framework\DebtorEntity;

class DebtorEntityFactory
{
    const PASSWORD_DEFAULT_ENCODER = 'bcrypt';

    public function create(array $debtorCustomerData): DebtorEntity
    {
        $debtor = new DebtorEntity();
        $debtor->id = IdValue::create($debtorCustomerData['id']);
        $debtor->subshopID = IdValue::create($debtorCustomerData['sales_channel_id']);
        $debtor->password = $debtorCustomerData['password'] ?? $debtorCustomerData['legacy_password'];
        $debtor->encoder = $debtorCustomerData['legacy_encoder'] ?? self::PASSWORD_DEFAULT_ENCODER;
        $debtor->email = $debtorCustomerData['email'];
        $debtor->active = (bool) $debtorCustomerData['active'];
        $debtor->paymentID = IdValue::create($debtorCustomerData['default_payment_method_id']);
        $debtor->firstlogin = $debtorCustomerData['first_login'];
        $debtor->lastlogin = $debtorCustomerData['last_login'];
        $debtor->newsletter = (int) $debtorCustomerData['newsletter'];
        $debtor->customergroup = IdValue::create($debtorCustomerData['customer_group_id']);
        $debtor->language = IdValue::create($debtorCustomerData['language_id']);
        $debtor->default_billing_address_id = IdValue::create($debtorCustomerData['default_billing_address_id']);
        $debtor->default_shipping_address_id = IdValue::create($debtorCustomerData['default_shipping_address_id']);
        $debtor->title = $debtorCustomerData['title'];
        $debtor->salutation = $debtorCustomerData['salutation_display_name'];
        $debtor->firstName = $debtorCustomerData['first_name'];
        $debtor->lastName = $debtorCustomerData['last_name'];
        $debtor->birthday = $debtorCustomerData['birthday'];
        $debtor->customernumber = $debtorCustomerData['customer_number'];

        return $debtor;
    }
}
