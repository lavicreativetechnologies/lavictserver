<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\BridgePlatform;

use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class DebtorEntityMailData
{
    /**
     * @var DebtorEntity
     */
    private $debtorEntity;

    public function __construct(?DebtorEntity $debtorEntity)
    {
        $this->debtorEntity = $debtorEntity;
    }

    public static function getDebtorEntityDataType(): EventDataType
    {
        $debtor = new ObjectType();
        $debtor->add('id', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('encoder', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('email', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('active', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $debtor->add('paymentID', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('newsletter', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $debtor->add('customergroup', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('language', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('subshopID', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('defaultBillingAddressId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('defaultShippingAddressId', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('title', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('salutation', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('firstName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('lastName', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('birthday', new ScalarValueType(ScalarValueType::TYPE_STRING));
        $debtor->add('customernumber', new ScalarValueType(ScalarValueType::TYPE_STRING));

        return $debtor;
    }

    public function getId(): string
    {
        return $this->debtorEntity ? (string) $this->debtorEntity->id->getValue() : '';
    }

    public function getEncoder(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->encoder : '';
    }

    public function getEmail(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->email : '';
    }

    public function isActive(): bool
    {
        return $this->debtorEntity ? $this->debtorEntity->active : false;
    }

    public function getPaymentID(): string
    {
        return $this->debtorEntity ? (string) $this->debtorEntity->paymentID->getValue() : '';
    }

    public function getNewsletter(): bool
    {
        return $this->debtorEntity ? (bool) $this->debtorEntity->newsletter : false;
    }

    public function getCustomergroup(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->customergroup : '';
    }

    public function getLanguage(): string
    {
        return $this->debtorEntity ? (string) $this->debtorEntity->language->getValue() : '';
    }

    public function getSubshopID(): string
    {
        return $this->debtorEntity ? (string) $this->debtorEntity->subshopID->getValue() : '';
    }

    public function getDefaultBillingAddressId(): string
    {
        return $this->debtorEntity ? (string) $this->debtorEntity->default_billing_address_id->getValue() : '';
    }

    public function getDefaultShippingAddressId(): string
    {
        return $this->debtorEntity ? (string) $this->debtorEntity->default_shipping_address_id->getValue() : '';
    }

    public function getTitle(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->title : '';
    }

    public function getSalutation(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->salutation : '';
    }

    public function getFirstName(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->firstName : '';
    }

    public function getLastName(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->lastName : '';
    }

    public function getBirthday(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->birthday : '';
    }

    public function getCustomernumber(): string
    {
        return $this->debtorEntity ? $this->debtorEntity->customernumber : '';
    }
}
