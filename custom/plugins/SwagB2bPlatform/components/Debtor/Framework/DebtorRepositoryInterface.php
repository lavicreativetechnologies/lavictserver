<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\Framework;

use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\AbstractCredentialsEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;

interface DebtorRepositoryInterface
{
    public function fetchOneByEmail(string $email): DebtorEntity;

    public function fetchOneByCredentials(AbstractCredentialsEntity $credentialsEntity): DebtorEntity;

    public function fetchOneById(IdValue $id): DebtorEntity;

    public function fetchAllDebtors(): array;

    public function fetchIdentityById(IdValue $id, LoginContextService $contextService): Identity;

    public function setUserAsDebtor(IdValue $userId);

    public function filterActiveDebtor(QueryBuilder $queryBuilder, string $fromAlias, string $idField): void;

    public function getTableName(): string;
}
