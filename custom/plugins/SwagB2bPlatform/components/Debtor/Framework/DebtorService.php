<?php declare(strict_types=1);

namespace Shopware\B2B\Debtor\Framework;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationIdentityLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use Shopware\B2B\StoreFrontAuthentication\Framework\StoreFrontAuthenticationEntity;

class DebtorService
{
    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    /**
     * @var AuthenticationIdentityLoaderInterface
     */
    private $contextIdentityLoader;

    /**
     * @var LoginContextService
     */
    private $loginContextService;

    public function __construct(
        AuthStorageAdapterInterface $authStorageAdapter,
        AuthenticationIdentityLoaderInterface $contextIdentityLoader,
        LoginContextService $loginContextService
    ) {
        $this->authStorageAdapter = $authStorageAdapter;
        $this->contextIdentityLoader = $contextIdentityLoader;
        $this->loginContextService = $loginContextService;
    }

    public function authenticateByEmail(string $email): void
    {
        $identity = $this->loadIdentityByEmail($email);
        $this->authStorageAdapter->setIdentity($identity);
    }

    public function authenticateByAuthenticationIdentifier(IdValue $debtorIdentifier): void
    {
        $identity = $this->loadIdentityByAuthentication(IdValue::create($debtorIdentifier));
        $this->authStorageAdapter->setIdentity($identity);
    }

    /**
     * @internal
     */
    protected function loadIdentityByAuthentication(IdValue $authId): Identity
    {
        $debtorEntity = new StoreFrontAuthenticationEntity();
        $debtorEntity->providerKey = DebtorRepositoryInterface::class;
        $debtorEntity->providerContext = $authId;

        $identity = $this->contextIdentityLoader
            ->fetchIdentityByAuthentication($debtorEntity, $this->loginContextService, true);

        return $identity;
    }

    /**
     * @internal
     */
    protected function loadIdentityByEmail(string $debtorEmail): Identity
    {
        return $this->contextIdentityLoader
            ->fetchIdentityByEmail($debtorEmail, $this->loginContextService, true);
    }
}
