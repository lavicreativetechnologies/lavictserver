<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Bridge\CategoryType;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use function array_merge;

class CategoryRuleEntity extends ContingentRuleEntity
{
    /**
     * @var IdValue
     */
    public $categoryId;

    /**
     * @var string
     */
    public $name;

    public function __construct(string $type)
    {
        parent::__construct($type);

        $this->categoryId = IdValue::null();
    }

    public function toDatabaseArray(): array
    {
        return array_merge(
            parent::toDatabaseArray(),
            ['category_id' => $this->categoryId->getStorageValue()]
        );
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->categoryId = IdValue::create($data['category_id']);
        $this->name = (string) $data['category_name'];

        return parent::fromDatabaseArray($data);
    }

    public function fromDatabaseArrayPrefixed(array $data): CrudEntity
    {
        $this->categoryId = IdValue::create($data[$data['type'] . '_category_id']);
        $this->name = (string) $data[$data['type'] . '_category_name'];

        return parent::fromDatabaseArrayPrefixed($data);
    }
}
