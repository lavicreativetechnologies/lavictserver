<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Bridge\CategoryType;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeRepositoryInterface;

class CategoryRepository implements ContingentRuleTypeRepositoryInterface
{
    const TABLE_NAME = 'b2b_contingent_group_rule_category';

    const TABLE_ALIAS = 'categoryType';

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function createSubQuery(): string
    {
        return $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*, category.description as category_name')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 's_categories', 'category', self::TABLE_ALIAS . '.category_id = category.id')
            ->getSQL();
    }

    public function addSelect(QueryBuilder $query, string $prefix): void
    {
        $query->addSelect($prefix . '.contingent_rule_id as ' . $prefix . '_contingent_rule_id')
            ->addSelect($prefix . '.category_id as ' . $prefix . '_category_id')
            ->addSelect($prefix . '.category_name as ' . $prefix . '_category_name');
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
