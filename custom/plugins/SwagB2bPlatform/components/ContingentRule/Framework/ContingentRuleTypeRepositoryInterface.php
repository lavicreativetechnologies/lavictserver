<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

use Doctrine\DBAL\Query\QueryBuilder;

interface ContingentRuleTypeRepositoryInterface
{
    public function getTableName(): string;

    public function createSubQuery(): string;

    public function addSelect(QueryBuilder $query, string $prefix);
}
