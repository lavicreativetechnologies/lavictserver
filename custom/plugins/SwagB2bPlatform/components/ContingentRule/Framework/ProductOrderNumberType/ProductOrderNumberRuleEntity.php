<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductOrderNumberType;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use Shopware\B2B\ProductName\Framework\ProductNameAware;
use function array_merge;

class ProductOrderNumberRuleEntity extends ContingentRuleEntity implements ProductNameAware
{
    /**
     * @var string
     */
    public $productOrderNumber;

    /**
     * @var string
     */
    public $productName;

    /**
     * @var bool
     */
    public $valid;

    public function toDatabaseArray(): array
    {
        return array_merge(
            parent::toDatabaseArray(),
            ['product_order_number' => $this->productOrderNumber]
        );
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->productOrderNumber = (string) $data['product_order_number'];

        return parent::fromDatabaseArray($data);
    }

    public function fromDatabaseArrayPrefixed(array $data): CrudEntity
    {
        $this->productOrderNumber = (string) $data[$data['type'] . '_product_order_number'];

        return parent::fromDatabaseArrayPrefixed($data);
    }

    public function getProductOrderNumber(): string
    {
        return $this->productOrderNumber;
    }

    public function setProductName(string $name = null): void
    {
        $this->productName = $name;
    }
}
