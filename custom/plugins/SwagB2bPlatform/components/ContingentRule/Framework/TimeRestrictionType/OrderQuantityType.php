<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\TimeRestrictionType;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\Cart\Framework\CartHistoryRepository;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleService;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeRepositoryInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeValidationExtender;
use Shopware\B2B\ContingentRule\Framework\UnsupportedContingentRuleEntityTypeException;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class OrderQuantityType implements ContingentRuleTypeInterface
{
    const NAME = 'OrderQuantity';

    /**
     * @var CartHistoryRepository
     */
    private $cartHistoryRepository;

    /**
     * @var ContingentRuleService
     */
    private $contingentRuleService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(
        ContingentRuleService $contingentRuleService,
        CartHistoryRepository $cartHistoryRepository,
        CurrencyService $currencyService
    ) {
        $this->cartHistoryRepository = $cartHistoryRepository;
        $this->contingentRuleService = $contingentRuleService;
        $this->currencyService = $currencyService;
    }

    public function getTypeName(): string
    {
        return self::NAME;
    }

    public function createEntity(): ContingentRuleEntity
    {
        return new TimeRestrictionRuleEntity($this->getTypeName());
    }

    public function createValidationExtender(ContingentRuleEntity $entity): ContingentRuleTypeValidationExtender
    {
        return new TimeRestrictionRuleValidationExtender($entity, $this->contingentRuleService);
    }

    public function createCartAccessStrategy(OwnershipContext $ownershipContext, ContingentRuleEntity $entity): CartAccessStrategyInterface
    {
        if (!$entity instanceof TimeRestrictionRuleEntity) {
            throw new UnsupportedContingentRuleEntityTypeException($entity);
        }

        $currencyContext = $this->currencyService->createCurrencyContext();

        $cartHistory = $this->cartHistoryRepository
            ->fetchHistory(
                $this->contingentRuleService->getTimeRestrictions(),
                $ownershipContext,
                $currencyContext
            );

        return new OrderQuantityAccessStrategy(
            $cartHistory[$entity->timeRestriction],
            (int) $entity->value
        );
    }

    public function getRepository(Connection $connection): ContingentRuleTypeRepositoryInterface
    {
        return new TimeRestrictionRepository($connection);
    }

    public function getRequestKeys(): array
    {
        return [
            'timeRestriction',
            'value',
        ];
    }
}
