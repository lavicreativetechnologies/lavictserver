<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\TimeRestrictionType;

use Shopware\B2B\Cart\Framework\CartAccessContext;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\Cart\Framework\CartHistory;
use function spl_object_hash;

class OrderQuantityAccessStrategy implements CartAccessStrategyInterface
{
    /**
     * @var CartHistory
     */
    private $cartHistory;

    /**
     * @var int
     */
    private $value;

    public function __construct(CartHistory $cartHistory, int $value)
    {
        $this->cartHistory = $cartHistory;
        $this->value = $value;
    }

    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult): void
    {
        if ($this->cartHistory->orderQuantity < $this->value) {
            return;
        }

        $cartAccessResult->addError(
            __CLASS__,
            'OrderQuantityError',
            [
                'allowedValue' => $this->value,
                'appliedValue' => $this->cartHistory->orderQuantity,
                'timeRestriction' => $this->cartHistory->timeRestriction,
                'cartHistory' => $this->cartHistory,
                'identifier' => spl_object_hash($this),
            ]
        );
    }

    public function addInformation(CartAccessResult $cartAccessResult): void
    {
        $cartAccessResult->addInformation(
            __CLASS__,
            'OrderQuantityError',
            [
                'allowedValue' => $this->value,
                'timeRestriction' => $this->cartHistory->timeRestriction,
                'cartHistory' => $this->cartHistory,
                'identifier' => spl_object_hash($this),
            ]
        );
    }
}
