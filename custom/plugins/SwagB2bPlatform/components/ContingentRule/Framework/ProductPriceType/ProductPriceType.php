<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductPriceType;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeRepositoryInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeValidationExtender;
use Shopware\B2B\ContingentRule\Framework\UnsupportedContingentRuleEntityTypeException;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class ProductPriceType implements ContingentRuleTypeInterface
{
    const NAME = 'ProductPrice';

    public function getTypeName(): string
    {
        return self::NAME;
    }

    public function createEntity(): ContingentRuleEntity
    {
        return new ProductPriceRuleEntity($this->getTypeName());
    }

    public function createValidationExtender(ContingentRuleEntity $entity): ContingentRuleTypeValidationExtender
    {
        return new ProductPriceRuleValidationExtender($entity);
    }

    public function createCartAccessStrategy(
        OwnershipContext $ownershipContext,
        ContingentRuleEntity $entity
    ): CartAccessStrategyInterface {
        if (!$entity instanceof ProductPriceRuleEntity) {
            throw new UnsupportedContingentRuleEntityTypeException($entity);
        }

        return new ProductPriceAccessStrategy($entity->productPrice);
    }

    public function getRepository(Connection $connection): ContingentRuleTypeRepositoryInterface
    {
        return new ProductPriceRepository($connection);
    }

    public function getRequestKeys(): array
    {
        return [
            'productPrice',
        ];
    }
}
