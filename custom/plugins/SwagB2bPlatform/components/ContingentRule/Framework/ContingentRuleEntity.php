<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use function array_keys;
use function get_object_vars;
use function in_array;
use function mb_strtolower;
use function preg_replace;

abstract class ContingentRuleEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * foreign key to b2b_contingent_group.id
     *
     * @var IdValue
     */
    public $contingentGroupId;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $templateTypeName;

    public function __construct(string $type)
    {
        $this->type = $type;
        $this->templateTypeName = mb_strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $type));

        $this->id = IdValue::null();
        $this->contingentGroupId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'contingent_group_id' => $this->contingentGroupId->getStorageValue(),
            'type' => $this->type,
        ];
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->id = IdValue::create($data['id']);
        $this->contingentGroupId = IdValue::create($data['contingent_group_id']);
        $this->type = $data['type'];

        return $this;
    }

    public function fromDatabaseArrayPrefixed(array $data): CrudEntity
    {
        return self::fromDatabaseArray($data);
    }

    public function setData(array $data): void
    {
        $properties = array_keys($this->toArray());

        foreach ($data as $key => $value) {
            if (false === in_array($key, $properties, true)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->contingentGroupId = IdValue::create($this->contingentGroupId);
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
