<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductNumberAllowanceType;

use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeValidationExtender;
use function array_filter;
use function is_array;

class ProductNumberAllowanceRuleValidationExtender implements ContingentRuleTypeValidationExtender
{
    const CAUSE = 'NOPRODUCTS';

    const MESSAGE = 'AddProductNumbers';

    /**
     * @var ProductNumberAllowanceRuleEntity
     */
    private $entity;

    public function __construct(ProductNumberAllowanceRuleEntity $entity)
    {
        $this->entity = $entity;
    }

    public function extendValidator(ValidationBuilder $validationBuilder): ValidationBuilder
    {
        return $validationBuilder
            ->validateThat('productNumbers', '')
            ->withCallback(
                function (): bool {
                    if (!is_array($this->entity->productNumbers) || !array_filter($this->entity->productNumbers)) {
                        return false;
                    }

                    return true;
                },
                self::MESSAGE,
                self::CAUSE,
                [],
                true
            );
    }
}
