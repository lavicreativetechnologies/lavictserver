<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Framework\ProductNumberAllowanceType;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleAllowanceEntityInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use function array_filter;
use function array_merge;
use function array_unique;
use function explode;
use function implode;

class ProductNumberAllowanceRuleEntity extends ContingentRuleEntity implements ContingentRuleAllowanceEntityInterface
{
    /**
     * @var string[]
     */
    public $productNumbers;

    public function toDatabaseArray(): array
    {
        return array_merge(
            parent::toDatabaseArray(),
            ['product_numbers' => implode(',', array_filter($this->productNumbers))]
        );
    }

    public function fromDatabaseArray(array $data): CrudEntity
    {
        $this->productNumbers = explode(',', $data['product_numbers']);

        return parent::fromDatabaseArray($data);
    }

    public function fromDatabaseArrayPrefixed(array $data): CrudEntity
    {
        $this->productNumbers = explode(',', $data[$data['type'] . '_product_numbers']);

        return parent::fromDatabaseArrayPrefixed($data);
    }

    public function mergeRule(ContingentRuleAllowanceEntityInterface $rule): ContingentRuleAllowanceEntityInterface
    {
        if (!$rule instanceof self) {
            return $rule;
        }

        $this->productNumbers = array_unique(array_merge($this->productNumbers, $rule->productNumbers));
        $this->contingentGroupId = IdValue::null();

        return $this;
    }
}
