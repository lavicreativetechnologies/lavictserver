<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContingentRuleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_rule/contingent_group/{contingentGroupId}',
                'b2b_contingent_rule.api_rule_controller',
                'getList',
                ['contingentGroupId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_rule/{contingentRuleId}',
                'b2b_contingent_rule.api_rule_controller',
                'get',
                ['contingentRuleId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contingent_rule',
                'b2b_contingent_rule.api_rule_controller',
                'create',
                [],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contingent_rule/{contingentRuleId}',
                'b2b_contingent_rule.api_rule_controller',
                'update',
                ['contingentRuleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contingent_rule/{contingentRuleId}',
                'b2b_contingent_rule.api_rule_controller',
                'remove',
                ['contingentRuleId'],
            ],
        ];
    }
}
