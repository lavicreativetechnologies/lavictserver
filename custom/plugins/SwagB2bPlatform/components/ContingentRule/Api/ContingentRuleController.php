<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\Api;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleCrudService;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleRepository;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleSearchStruct;
use Shopware\B2B\Currency\Framework\CurrencyService;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class ContingentRuleController
{
    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var ContingentRuleRepository
     */
    private $contingentRuleRepository;

    /**
     * @var ContingentRuleCrudService
     */
    private $contingentRuleCrudService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        ContingentRuleRepository $contingentRuleRepository,
        GridHelper $requestHelper,
        ContingentRuleCrudService $contingentRuleCrudService,
        CurrencyService $currencyService,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->requestHelper = $requestHelper;
        $this->contingentRuleRepository = $contingentRuleRepository;
        $this->contingentRuleCrudService = $contingentRuleCrudService;
        $this->currencyService = $currencyService;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(
        string $contingentGroupId,
        Request $request
    ): array {
        $contingentGroupId = IdValue::create($contingentGroupId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $search = new ContingentRuleSearchStruct();
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $rules = $this->contingentRuleRepository
            ->fetchListByContingentGroupId($contingentGroupId, $search, $currencyContext, $ownershipContext);

        $totalCount = $this->contingentRuleRepository
            ->fetchTotalCountByContingentGroupId($contingentGroupId, $search, $ownershipContext);

        return ['success' => true, 'contingentRules' => $rules, 'totalCount' => $totalCount];
    }

    public function getAction(int $contingentRuleId): array
    {
        $contingentRuleId = IdValue::create($contingentRuleId);
        $ownershipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $rule = $this->contingentRuleRepository
            ->fetchOneById($contingentRuleId, $currencyContext, $ownershipContext);

        return ['success' => true, 'contingentRule' => $rule];
    }

    public function createAction(Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService
            ->createCurrencyContext();

        $data = $request->getPost();

        $newRecord = $this->contingentRuleCrudService
            ->createNewRecordRequest($data);

        $rule = $this->contingentRuleCrudService
            ->create($newRecord, $ownershipContext, $currencyContext);

        return ['success' => true, 'contingentRule' => $rule];
    }

    public function updateAction(string $contingentRuleId, Request $request): array
    {
        $contingentRuleId = IdValue::create($contingentRuleId);
        $ownerShipContext = $this->getDebtorOwnershipContext();

        $data = $request->getPost();
        $data['id'] = $contingentRuleId;

        $existingRecord = $this->contingentRuleCrudService
            ->createExistingRecordRequest($data);

        $currencyContext = $this->currencyService->createCurrencyContext();

        $rule = $this->contingentRuleCrudService
            ->update($existingRecord, $ownerShipContext, $currencyContext);

        return ['success' => true, 'contingentRule' => $rule];
    }

    public function removeAction(string $contingentRuleId): array
    {
        $contingentRuleId = IdValue::create($contingentRuleId);
        $ownerShipContext = $this->getDebtorOwnershipContext();
        $currencyContext = $this->currencyService->createCurrencyContext();

        $rule = $this->contingentRuleCrudService
            ->remove($contingentRuleId, $ownerShipContext, $currencyContext);

        return ['success' => true, 'contingentRule' => $rule];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
