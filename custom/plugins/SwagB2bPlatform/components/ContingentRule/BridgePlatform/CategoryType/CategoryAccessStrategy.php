<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentRule\BridgePlatform\CategoryType;

use Shopware\B2B\Cart\Framework\CartAccessContext;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\LineItemList\Framework\LineItemReference;
use Shopware\B2B\Shop\Framework\CategoryRepositoryInterface;
use function array_filter;
use function count;
use function spl_object_hash;

class CategoryAccessStrategy implements CartAccessStrategyInterface
{
    /**
     * @var IdValue
     */
    private $categoryId;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(IdValue $categoryId, CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryId = $categoryId;
        $this->categoryRepository = $categoryRepository;
    }

    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult): void
    {
        $products = $context->orderClearanceEntity
            ->list
            ->references;

        $categoryErrors = array_filter($products, function (LineItemReference $lineItem) {
            return $this->categoryRepository->hasProduct($this->categoryId, $lineItem->referenceNumber);
        });

        if (!count($categoryErrors)) {
            return;
        }

        $category = $this->categoryRepository
            ->fetchNodeById($this->categoryId);

        $cartAccessResult->addError(
            __CLASS__,
            'CategoryError',
            [
                'allowedValue' => $category->name,
                'identifier' => spl_object_hash($this),
            ]
        );
    }

    public function addInformation(CartAccessResult $cartAccessResult): void
    {
        $category = $this->categoryRepository->fetchNodeById($this->categoryId);

        $cartAccessResult->addInformation(
            __CLASS__,
            'CategoryError',
            [
                'allowedValue' => $category->name,
                'identifier' => spl_object_hash($this),
            ]
        );
    }
}
