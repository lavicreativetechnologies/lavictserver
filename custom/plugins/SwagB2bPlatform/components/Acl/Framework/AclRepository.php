<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Doctrine\DBAL\Connection;
use Shopware\B2B\Common\IdValue;
use function array_merge;
use function get_class;
use function implode;

/**
 * IO for ACL records. Contains methods for manipulation and retrieval of acl ownership relationships.
 */
class AclRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var AclTable[]
     */
    private $aclTables;

    /**
     * @param AclTable[] $aclTables
     */
    public function __construct(array $aclTables, Connection $connection)
    {
        $this->connection = $connection;
        $this->aclTables = $aclTables;
    }

    /**
     * @param object $context
     */
    public function allow($context, IdValue $subjectId, bool $grantable = false): void
    {
        $resolver = $this->getAssignableTableResolver($context);

        if ($this->isDirectlyAllowed($context, $subjectId)) {
            $this->connection->update(
                $resolver->getTableName(),
                [
                    'grantable' => (int) $grantable,
                ],
                [
                    'entity_id' => $resolver->getId()->getStorageValue(),
                    'referenced_entity_id' => $subjectId->getStorageValue(),
                ]
            );

            return;
        }

        $this->connection
            ->insert(
                $resolver->getTableName(),
                [
                    'entity_id' => $resolver->getId()->getStorageValue(),
                    'referenced_entity_id' => $subjectId->getStorageValue(),
                    'grantable' => (int) $grantable,
                ]
            );
    }

    /**
     * @param object $context
     */
    public function allowAll($context, array $subjectIds, bool $grantable = false): void
    {
        foreach ($subjectIds as $entityId) {
            $this->allow($context, $entityId, $grantable);
        }
    }

    /**
     * @param object $context
     */
    public function deny($context, IdValue $subjectId): void
    {
        $resolver = $this->getAssignableTableResolver($context);

        $this->connection
            ->delete(
                $resolver->getTableName(),
                [
                    'entity_id' => $resolver->getId()->getStorageValue(),
                    'referenced_entity_id' => $subjectId->getStorageValue(),
                ]
            );
    }

    /**
     * @param object $context
     */
    public function getAllAssignedIdsBySubjectId($context, IdValue $subjectId): array
    {
        $resolver = $this->getAssignableTableResolver($context);
        $query = $this->connection->createQueryBuilder();

        $resolver->getQuery($query);
        $query->select('DISTINCT entity_id')
            ->where('referenced_entity_id = :subjectId')
            ->setParameter('subjectId', $subjectId->getStorageValue());

        $rawResult = $query->execute()->fetchAll();

        $assignedIds = [];
        foreach ($rawResult as $row) {
            $assignedIds[] = IdValue::create($row['entity_id']);
        }

        return $assignedIds;
    }

    /**
     * @param object $context
     */
    public function denyAll($context, array $subjectIds): void
    {
        foreach ($subjectIds as $entityId) {
            $this->deny($context, $entityId);
        }
    }

    /**
     * @param object $context
     */
    public function isAllowed($context, IdValue $subjectId): bool
    {
        $query = $this->getUnionizedSqlQuery($context);

        $statement = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from('(' . $query->sql . ')', 'acl_table')
            ->where('acl_table.referenced_entity_id = :referencedEntityId')
            ->setParameters($query->params)
            ->setParameter('referencedEntityId', $subjectId->getStorageValue())
            ->execute();

        return (bool) $statement->fetchColumn();
    }

    /**
     * @param object $context
     */
    public function isGrantable($context, IdValue $subjectId): bool
    {
        $query = $this->getUnionizedSqlQuery($context);

        $statement = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from('(' . $query->sql . ')', 'acl_table')
            ->where('acl_table.referenced_entity_id = :referencedEntityId')
            ->andWhere('acl_table.grantable = 1')
            ->setParameters($query->params)
            ->setParameter('referencedEntityId', $subjectId->getStorageValue())
            ->execute();

        return (bool) $statement->fetchColumn();
    }

    /**
     * @param object $context
     * @return IdValue[]
     */
    public function getAllAllowedIds($context): array
    {
        $query = $this->getUnionizedSqlQuery($context);

        $statement = $this->connection->createQueryBuilder()
            ->select('DISTINCT acl_table.referenced_entity_id')
            ->from('(' . $query->sql . ')', 'acl_table')
            ->setParameters($query->params)
            ->execute();

        $rawResult = $statement->fetchAll();

        $allowedIds = [];
        foreach ($rawResult as $row) {
            $allowedIds[] = IdValue::create($row['referenced_entity_id']);
        }

        return $allowedIds;
    }

    /**
     * @param object $context
     */
    public function fetchAllGrantableIds($context): array
    {
        $query = $this->getUnionizedSqlQuery($context);

        $statement = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('(' . $query->sql . ')', 'acl_table')
            ->setParameters($query->params)
            ->execute();

        $aclSettings = $statement->fetchAll();

        $result = [];
        foreach ($aclSettings as $setting) {
            $result[$setting['referenced_entity_id']] = (bool) $setting['grantable'];
        }

        return $result;
    }

    /**
     * @param object $context
     */
    public function fetchAllDirectlyIds($context): array
    {
        $query = $this->getDirectAssignableQuery($context);

        $statement = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('(' . $query->sql . ')', 'acl_table')
            ->setParameters($query->params)->execute();

        $aclSettings = $statement->fetchAll();

        $result = [];
        foreach ($aclSettings as $setting) {
            $result[IdValue::create($setting['referenced_entity_id'])->getValue()] = (bool) $setting['grantable'];
        }

        return $result;
    }

    /**
     * @param object $context
     * @throws AclUnsupportedContextException
     */
    public function getUnionizedSqlQuery($context): AclQuery
    {
        $sql = [];
        $params = [];

        foreach ($this->aclTables as $aclTable) {
            try {
                $resolver = $aclTable->getResolver($context);
            } catch (AclUnsupportedContextException $e) {
                continue;
            }

            $query = $resolver->getQuery($this->connection->createQueryBuilder());

            $sql[] = '(' . $query->sql . ')';
            $params = array_merge($params, $query->params);
        }

        if (!$sql) {
            throw new AclUnsupportedContextException('Could not find a single valid query for this context');
        }

        $unionQuery = (new AclQuery())->fromPrimitives(implode(' UNION DISTINCT ', $sql), $params);

        return (new AclQuery())
            ->fromPrimitives('SELECT referenced_entity_id, MAX(grantable) AS grantable
                                FROM (' . $unionQuery->sql . ') AS query
                                GROUP BY referenced_entity_id', $unionQuery->params);
    }

    /**
     * @internal
     * @param object $context
     * @param int|string $subjectId
     */
    protected function isDirectlyAllowed($context, IdValue $subjectId): bool
    {
        $query = $this->getDirectAssignableQuery($context);

        $statement = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from('(' . $query->sql . ')', 'acl_table')
            ->where('acl_table.referenced_entity_id = :referencedEntityId')
            ->setParameters($query->params)
            ->setParameter('referencedEntityId', $subjectId->getStorageValue())
            ->execute();

        return (bool) $statement->fetchColumn();
    }

    /**
     * @param object $context
     * @throws AclUnsupportedContextException
     */
    public function getAssignableTableResolver($context): AclTableResolverFacade
    {
        foreach ($this->aclTables as $table) {
            try {
                return $table->getMainResolver($context);
            } catch (AclUnsupportedContextException $e) {
                // nth
            }
        }

        throw new AclUnsupportedContextException('No applying context found for ' . get_class($context));
    }

    /**
     * @param object $context
     * @throws AclUnsupportedContextException
     */
    public function getDirectAssignableQuery($context): AclQuery
    {
        return $this->getAssignableTableResolver($context)
            ->getQuery($this->connection->createQueryBuilder());
    }
}
