<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\BinaryType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function array_key_exists;

/**
 * Document definition Service. Creates and Drops tables ACL tables.
 */
class AclDdlService
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * Necessary for plugin install
     *
     * @return AclDdlService
     */
    public static function create(ContainerInterface $container): self
    {
        return new self($container->has('dbal_connection') ? $container->get('dbal_connection') : $container->get(Connection::class));
    }

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function createTable(AclTable $table): void
    {
        $contextColumns = $this->connection->getSchemaManager()
            ->listTableColumns($table->getContextTableName());

        $entityColumnType = $this->getIdColumnType($contextColumns);

        $subjectColumns = $this->connection->getSchemaManager()
            ->listTableColumns($table->getSubjectTableName());

        $subjectColumnType = $this->getIdColumnType($subjectColumns);

        $this->connection->exec("
            CREATE TABLE IF NOT EXISTS `{$table->getName()}` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `entity_id` " . $entityColumnType . ' NOT NULL,
                `referenced_entity_id` ' . $subjectColumnType . " NOT NULL,
                `grantable` TINYINT(4) NOT NULL DEFAULT '0',
                UNIQUE INDEX `b2b_unique_{$table->getUid()}_idx` (`entity_id`, `referenced_entity_id`),
                INDEX `PK` (`id`),
                INDEX `b2b_acl_{$table->getUid()}_idx` (`referenced_entity_id`),
                CONSTRAINT `b2b_acl_1_{$table->getUid()}` FOREIGN KEY (`entity_id`) REFERENCES `{$table->getContextTableName()}` (`{$table->getContextPrimaryKeyField()}`) ON DELETE CASCADE,
                CONSTRAINT `b2b_acl_2_{$table->getUid()}` FOREIGN KEY (`referenced_entity_id`) REFERENCES `{$table->getSubjectTableName()}` (`{$table->getSubjectPrimaryKeyField()}`) ON DELETE CASCADE
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
            ;
        ");
    }

    public function dropTable(AclTable $aclTable): void
    {
        $this->connection->exec("
            DROP TABLE `{$aclTable->getName()}`;
        ");
    }

    /**
     * @internal
     * @param Column[] $columns
     */
    protected function getIdColumnType(array $columns): string
    {
        if (!array_key_exists('id', $columns)) {
            return 'INT(11)';
        }

        $type = $columns['id']->getType();
        if ($type instanceof BinaryType) {
            return 'BINARY(16)';
        }

        return 'INT(11)';
    }
}
