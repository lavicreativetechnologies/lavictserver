<?php declare(strict_types=1);

namespace Shopware\B2B\Acl\Framework;

use function array_key_exists;

/**
 * Helper Service to extend Entities with ACL-properties
 */
class AclAccessExtensionService
{
    /**
     * Add fields `foreignAllowed` and `foreignGrantable` in relation to a context identity
     *
     * @param object $context
     */
    public function extendEntitiesWithAssignment(AclRepository $aclRepository, $context, array $entities): void
    {
        $relatedAclSettings = $aclRepository->fetchAllDirectlyIds($context);

        foreach ($entities as $entity) {
            $entity->foreignAllowed = array_key_exists($entity->id->getValue(), $relatedAclSettings);
            $entity->foreignGrantable = false;

            if (!$entity->foreignAllowed) {
                continue;
            }

            $entity->foreignGrantable = $relatedAclSettings[$entity->id->getValue()];
        }
    }

    /**
     * Add `ownerGrantable` to $entities
     *
     * @param object $context
     * @param object[] $entities
     */
    public function extendEntitiesWithIdentityOwnership(AclRepository $aclRepository, $context, array $entities): void
    {
        try {
            $ownerAclSettings = $aclRepository->fetchAllGrantableIds($context);

            foreach ($entities as $entity) {
                $entity->ownerGrantable = false;

                if (!isset($ownerAclSettings[$entity->id->getValue()])) {
                    continue;
                }

                if (!$ownerAclSettings[$entity->id->getValue()]) {
                    continue;
                }

                $entity->ownerGrantable = true;
            }
        } catch (AclUnsupportedContextException $e) {
            foreach ($entities as $entity) {
                $entity->ownerGrantable = true;
            }
        }
    }
}
