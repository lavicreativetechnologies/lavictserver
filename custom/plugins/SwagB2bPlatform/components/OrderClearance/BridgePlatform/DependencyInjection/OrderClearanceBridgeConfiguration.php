<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\LineItemList\Bridge\DependencyInjection\LineItemListBridgeConfiguration;
use Shopware\B2B\Order\Bridge\DependencyInjection\OrderBridgeConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderClearanceBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [];
    }

    public function getDependingConfigurations(): array
    {
        return [
            OrderBridgeConfiguration::create(),
            LineItemListBridgeConfiguration::create(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
        ];
    }
}
