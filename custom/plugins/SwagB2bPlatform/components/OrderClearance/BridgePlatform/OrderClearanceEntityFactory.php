<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntityFactoryInterface;
use Shopware\B2B\OrderClearance\Framework\OrderItemEntity;
use Shopware\B2B\OrderClearance\Framework\OrderItemLoaderInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;

class OrderClearanceEntityFactory implements OrderClearanceEntityFactoryInterface
{
    /**
     * @var OrderItemLoaderInterface[]
     */
    private $itemLoaders;

    public function __construct(OrderItemLoaderInterface ...$itemLoaders)
    {
        $this->itemLoaders = $itemLoaders;
    }

    public function createOrderEntityFromDatabase(array $databaseArray, OwnershipContext $ownershipContext): OrderClearanceEntity
    {
        $entity = $this->fromDatabaseArray($databaseArray);

        return $this->extendStoredOrderWithItems($entity, $ownershipContext);
    }

    /**
     * @internal
     */
    protected function fromDatabaseArray(array $data): OrderClearanceEntity
    {
        $orderClearance = new OrderClearanceEntity();
        $orderClearance->id = IdValue::create($data['id']);
        $orderClearance->listId = IdValue::create($data['list_id']);
        $orderClearance->orderNumber = $data['ordernumber'];
        $orderClearance->createdAt = $data['created_at'];
        $orderClearance->clearedAt = $data['cleared_at'];
        $orderClearance->declinedAt = $data['declined_at'];
        $orderClearance->shippingAddressId = IdValue::create($data['shipping_address_id']);
        $orderClearance->billingAddressId = IdValue::create($data['billing_address_id']);
        $orderClearance->paymentId = IdValue::create($data['payment_id']);
        $orderClearance->shippingId = IdValue::create($data['shipping_id']);
        $orderClearance->comment = $data['comment'];
        $orderClearance->deviceType = $data['device_type'];
        $orderClearance->orderReference = $data['order_reference'];
        $orderClearance->requestedDeliveryDate = $data['requested_delivery_date'];
        $orderClearance->authId = IdValue::create($data['auth_id']);
        $orderClearance->shippingAmount = (float) $data['shipping_amount'];
        $orderClearance->shippingAmountNet = (float) $data['shipping_amount_net'];
        $orderClearance->salesRepresentativeAuthId = IdValue::create($data['sales_representative_auth_id']);
        $orderClearance->status = $data['state'];

        return $orderClearance;
    }

    /**
     * @internal
     */
    protected function extendStoredOrderWithItems(OrderClearanceEntity $order, OwnershipContext $ownershipContext): OrderClearanceEntity
    {
        $items = [];
        /* @var OrderItemEntity $items */
        foreach ($this->itemLoaders as $itemLoader) {
            $items = array_merge($items, $itemLoader->fetchItemsFromStorage($order, $ownershipContext));
        }

        $order->items = $items;

        return $order;
    }
}
