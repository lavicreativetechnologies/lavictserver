<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use Shopware\B2B\Cart\BridgePlatform\CartState;
use Shopware\B2B\Cart\BridgePlatform\OrderClearanceFinishDestination;
use Shopware\B2B\Cart\BridgePlatform\ShopCartService;
use Shopware\B2B\LineItemList\Framework\LineItemShopWriterServiceInterface;
use Shopware\B2B\Order\BridgePlatform\AdditionalDataExtension;
use Shopware\B2B\Order\Framework\OrderContextService;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceShopWriterServiceInterface;
use Shopware\B2B\Shop\BridgePlatform\ContextProvider;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;

class OrderClearanceShopWriterService implements OrderClearanceShopWriterServiceInterface
{
    /**
     * @var ContextProvider
     */
    private $contextProvider;

    /**
     * @var LineItemShopWriterServiceInterface
     */
    private $lineItemShopWriterService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var ShopCartService
     */
    private $shopCartService;

    /**
     * @var OrderContextService
     */
    private $orderContextService;

    public function __construct(
        ContextProvider $contextProvider,
        LineItemShopWriterServiceInterface $lineItemShopWriterService,
        CartService $cartService,
        ShopCartService $shopCartService,
        OrderContextService $orderContextService
    ) {
        $this->contextProvider = $contextProvider;
        $this->lineItemShopWriterService = $lineItemShopWriterService;
        $this->cartService = $cartService;
        $this->shopCartService = $shopCartService;
        $this->orderContextService = $orderContextService;
    }

    public function sendToClearance(OrderClearanceEntity $orderClearance): void
    {
        $this->lineItemShopWriterService
            ->triggerCart($orderClearance->list, true);

        $this->orderContextService
            ->extendCart($orderClearance);

        $this->passAdditionalDataToCart($orderClearance);
        $this->setCartClearanceMode($orderClearance);
    }

    /**
     * @internal
     */
    protected function passAdditionalDataToCart(OrderClearanceEntity $orderClearance): void
    {
        if (!$orderClearance->orderReference && !$orderClearance->requestedDeliveryDate) {
            return;
        }

        $context = $this->contextProvider->getSalesChannelContext();
        $cart = $this->cartService->getCart($context->getToken(), $context);

        $additionalData = new AdditionalDataExtension($orderClearance->orderReference, $orderClearance->requestedDeliveryDate);

        $cart->addExtension(AdditionalDataExtension::NAME, $additionalData);
    }

    public function stopOrderClearance(): OrderClearanceEntity
    {
        $salesChannelContext = $this->contextProvider->getSalesChannelContext();

        $cart = $this->cartService
            ->getCart($salesChannelContext->getToken(), $salesChannelContext);

        $this->shopCartService->clear($salesChannelContext);

        return CartState::extract($cart)->getDestination()->getOrderClearance();
    }

    /**
     * @internal
     */
    protected function setCartClearanceMode(OrderClearanceEntity $orderClearance): void
    {
        $salesChannelContext = $this->contextProvider
            ->getSalesChannelContext();

        $cart = $this->cartService
            ->getCart($salesChannelContext->getToken(), $salesChannelContext);

        CartState::extract($cart)->setDestination(new OrderClearanceFinishDestination($orderClearance));

        $this->cartService->recalculate($cart, $salesChannelContext);
    }
}
