<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use Shopware\B2B\Contact\BridgePlatform\ContactEntityMailData;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Debtor\BridgePlatform\DebtorEntityMailData;
use Shopware\B2B\Debtor\Framework\DebtorEntity;
use Shopware\B2B\Order\BridgePlatform\OrderContextMailData;
use Shopware\B2B\Order\Framework\OrderContext;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\MailActionInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Contracts\EventDispatcher\Event;

class OrderClearanceInquiryMailEvent extends Event implements MailActionInterface
{
    public const EVENT_NAME = 'b2b.order.clearance.inquiry.mail';

    /**
     * @var ContactEntityMailData
     */
    private $contact;

    /**
     * @var DebtorEntityMailData
     */
    private $debtor;

    /**
     * @var SalesChannelContext
     */
    private $context;

    /**
     * @var OrderContextMailData
     */
    private $b2bOrderContext;

    public function __construct(ContactEntity $contact, ?DebtorEntity $debtor, SalesChannelContext $context, OrderContext $orderContext)
    {
        $this->contact = new ContactEntityMailData($contact);
        $this->debtor = new DebtorEntityMailData($debtor);
        $this->context = $context;
        $this->b2bOrderContext = new OrderContextMailData($orderContext);
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('debtor', DebtorEntityMailData::getDebtorEntityDataType())
            ->add('contact', ContactEntityMailData::getContactMailDataType())
            ->add('b2bOrderContext', OrderContextMailData::getOrderContextData());
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        return new MailRecipientStruct([
            $this->debtor->getEmail() => $this->debtor->getFirstName() . ' ' . $this->debtor->getLastName(),
        ]);
    }

    public function getSalesChannelId(): ?string
    {
        return $this->context->getSalesChannel()->getId();
    }

    public function getContext(): Context
    {
        return $this->context->getContext();
    }

    public function getDebtor(): DebtorEntityMailData
    {
        return $this->debtor;
    }

    public function getContact(): ContactEntityMailData
    {
        return $this->contact;
    }

    public function getB2bOrderContext(): OrderContextMailData
    {
        return $this->b2bOrderContext;
    }
}
