<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\BridgePlatform;

use Shopware\B2B\LineItemList\BridgePlatform\LineItemListMailData;
use Shopware\B2B\Order\BridgePlatform\OrderContextMailData;
use Shopware\B2B\OrderClearance\Framework\OrderClearanceEntity;
use Shopware\B2B\StoreFrontAuthentication\BridgePlatform\UserPostalSettingsMailData;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class OrderClearanceEntityMailData extends OrderContextMailData
{
    /**
     * @var OrderClearanceEntity
     */
    private $clearanceEntity;

    public function __construct(OrderClearanceEntity $clearanceEntity)
    {
        $this->clearanceEntity = $clearanceEntity;

        parent::__construct($clearanceEntity);
    }

    public static function getOrderContextData(): EventDataType
    {
        /** @var ObjectType $clearance */
        $clearance = parent::getOrderContextData();
        $clearance->add('list', LineItemListMailData::getLineItemListDataType());
        $clearance->add('postalSettings', UserPostalSettingsMailData::getPostalSettingsMailDataType());
        $clearance->add('clearable', new ScalarValueType(ScalarValueType::TYPE_BOOL));

        return $clearance;
    }

    public function getList(): LineItemListMailData
    {
        return new LineItemListMailData($this->clearanceEntity->list);
    }

    public function getPostalSettings(): UserPostalSettingsMailData
    {
        return new UserPostalSettingsMailData($this->clearanceEntity->userPostalSettings);
    }

    public function getClearable(): bool
    {
        return $this->clearanceEntity->isClearable;
    }
}
