<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

use Shopware\B2B\AuditLog\Framework\AuditLogValueBasicEntity;
use Shopware\B2B\AuditLog\Framework\AuditLogValueDiffEntity;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartService;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\NotAllowedRecordException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemReferenceService;
use Shopware\B2B\Order\Framework\OrderAuditLogService;
use Shopware\B2B\Order\Framework\OrderRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function array_filter;
use function array_values;

class OrderClearanceService
{
    /**
     * @var OrderClearanceRepository
     */
    private $orderClearanceRepository;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var OrderAuditLogService
     */
    private $auditLog;

    /**
     * @var OrderClearanceShopWriterServiceInterface
     */
    private $orderClearanceShopWriterService;

    /**
     * @var LineItemReferenceService
     */
    private $lineItemReferenceService;

    public function __construct(
        OrderClearanceRepository $orderClearanceRepository,
        CartService $cartService,
        OrderAuditLogService $auditLog,
        OrderClearanceShopWriterServiceInterface $orderClearanceShopWriterService,
        LineItemReferenceService $lineItemReferenceService
    ) {
        $this->orderClearanceRepository = $orderClearanceRepository;
        $this->cartService = $cartService;
        $this->auditLog = $auditLog;
        $this->orderClearanceShopWriterService = $orderClearanceShopWriterService;
        $this->lineItemReferenceService = $lineItemReferenceService;
    }

    /**
     * @return OrderClearanceEntity[]
     */
    public function fetchAllOrderClearances(Identity $identity, OrderClearanceSearchStruct $searchStruct, CurrencyContext $currencyContext): array
    {
        $orders = $this->orderClearanceRepository
            ->fetchAllOrderClearances($identity, $searchStruct, $currencyContext);

        return array_values(array_filter($orders, function (OrderClearanceEntity $order) use ($identity): bool {
            try {
                $result = $this->checkAllowed($order, $identity, CartService::ENVIRONMENT_NAME_LISTING);
            } catch (NotAllowedRecordException $e) {
                return false;
            }

            $order->isClearable = $result->isClearable();

            return true;
        }));
    }

    public function acceptOrder(Identity $identity, IdValue $orderContextId, CurrencyContext $currencyContext): void
    {
        $this->checkOrderAccess($identity, $orderContextId, CartService::ENVIRONMENT_NAME_ORDER, $currencyContext);

        $orderClearance = $this->orderClearanceRepository
            ->fetchOneByOrderContextId($orderContextId, $currencyContext, $identity->getOwnershipContext());

        $orderClearance->list = $this->lineItemReferenceService
            ->fetchLineItemListProductNames($orderClearance->list);

        $orderClearance->list->references = $this->lineItemReferenceService
            ->getReferencesWithStock($orderClearance->list->references);

        $this->orderClearanceShopWriterService
            ->sendToClearance($orderClearance);
    }

    public function stopAcceptance(): void
    {
        $orderClearance = $this->orderClearanceShopWriterService
            ->stopOrderClearance();

        if (!$orderClearance instanceof OrderClearanceEntity) {
            return;
        }

        $this->orderClearanceRepository->setStatusToOrderClearance($orderClearance);
    }

    public function createOrderAcceptedStatusChangeLogEntry(IdValue $orderContextId, Identity $identity): void
    {
        $auditLogValue = $this
            ->createAuditLogValue(
                OrderRepository::STATE_ORDER_OPEN,
                OrderClearanceRepository::STATE_CLEARANCE_OPEN
            );

        $this->auditLog->createStatusChangeAuditLog(
            $orderContextId,
            $auditLogValue,
            $identity
        );
    }

    public function declineOrder(Identity $identity, IdValue $orderContextId, string $comment, CurrencyContext $currencyContext): void
    {
        $this->checkOrderAccess($identity, $orderContextId, CartService::ENVIRONMENT_NAME_MODIFY, $currencyContext);

        $auditLogValue = $this
            ->createAuditLogValue(
                OrderClearanceRepository::STATE_CLEARANCE_DENIED,
                OrderClearanceRepository::STATE_CLEARANCE_OPEN
            );

        $this->auditLog->createStatusChangeAuditLog(
            $orderContextId,
            $auditLogValue,
            $identity
        );

        $this->orderClearanceRepository
            ->declineOrderClearance($orderContextId, $comment);
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function deleteOrder(Identity $identity, IdValue $orderContextId, CurrencyContext $currencyContext): void
    {
        $this->checkOrderAccess($identity, $orderContextId, CartService::ENVIRONMENT_NAME_MODIFY, $currencyContext);

        $this->orderClearanceRepository->deleteOrder($orderContextId, $identity->getOwnershipContext());
    }

    /**
     * @internal
     */
    protected function checkOrderAccess(Identity $identity, IdValue $orderContextId, string $environment, CurrencyContext $currencyContext): void
    {
        $this->checkOrderContextIdBelongsToDebtor($identity, $orderContextId);

        $order = $this->orderClearanceRepository
            ->fetchOneByOrderContextId($orderContextId, $currencyContext, $identity->getOwnershipContext());

        $this->checkAllowed($order, $identity, $environment);
    }

    /**
     * @internal
     * @throws NotAllowedRecordException
     */
    protected function checkAllowed(OrderClearanceEntity $order, Identity $identity, string $environment): CartAccessResult
    {
        $result = $this->cartService
            ->computeAccessibility($identity, $order, $environment);

        if ($result->hasErrors()) {
            $message = 'The given cart can not be reviewed by the current identity';
            throw new NotAllowedRecordException(
                $message,
                $message
            );
        }

        return $result;
    }

    /**
     * @internal
     * @throws NotAllowedRecordException
     */
    protected function checkOrderContextIdBelongsToDebtor(Identity $identity, IdValue $orderContextId): void
    {
        if (!$this->orderClearanceRepository->belongsOrderContextIdToDebtor($identity, $orderContextId)) {
            throw new NotAllowedRecordException(
                'The given order context id: ' . $orderContextId . ' belongs to another debtor!',
                'The given order context id: %id% belongs to another debtor!',
                ['%id%' => $orderContextId->getValue()]
            );
        }
    }

    /**
     * @internal
     */
    protected function createAuditLogValue(string $newValue, string $oldValue): AuditLogValueBasicEntity
    {
        $auditLogValue = new AuditLogValueDiffEntity();
        $auditLogValue->newValue = $newValue;
        $auditLogValue->oldValue = $oldValue;

        return $auditLogValue;
    }
}
