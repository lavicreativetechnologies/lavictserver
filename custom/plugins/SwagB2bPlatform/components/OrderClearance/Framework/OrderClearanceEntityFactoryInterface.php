<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

interface OrderClearanceEntityFactoryInterface
{
    public function createOrderEntityFromDatabase(
        array $databaseArray,
        OwnershipContext $ownershipContext
    ): OrderClearanceEntity;
}
