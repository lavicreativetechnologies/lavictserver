<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Framework;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\MysqlRepository;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\LineItemList\Framework\LineItemListRepository;
use Shopware\B2B\Order\Framework\OrderContextRepository;
use Shopware\B2B\Order\Framework\OrderRepository;
use Shopware\B2B\Order\Framework\ShopOrderRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\AclAuthReadHelperLoader;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;

class OrderClearanceRepository implements GridRepository
{
    const STATE_CLEARANCE_OPEN = 'orderclearance_open';

    const STATE_CLEARANCE_DENIED = 'orderclearance_denied';

    const TABLE_NAME = 'b2b_order_context';

    const TABLE_ALIAS = 'b2bOrder';

    const CROSS_TABLE_COLUMNS = [
        'contact' => 'debtorContact.email',
    ];

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var OrderClearanceEntityFactoryInterface
     */
    private $entityFactory;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var ShopOrderRepositoryInterface
     */
    private $shopOrderRepository;

    /**
     * @var OrderContextRepository
     */
    private $orderContextRepository;

    /**
     * @var LineItemListRepository
     */
    private $lineItemListRepository;

    /**
     * @var AclAuthReadHelperLoader
     */
    private $aclAuthReadHelper;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        ShopOrderRepositoryInterface $shopOrderRepository,
        OrderContextRepository $orderContextRepository,
        OrderClearanceEntityFactoryInterface $entityFactory,
        AuthenticationService $authenticationService,
        LineItemListRepository $lineItemListRepository,
        AclAuthReadHelperLoader $aclAuthReadHelper
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->entityFactory = $entityFactory;
        $this->authenticationService = $authenticationService;
        $this->shopOrderRepository = $shopOrderRepository;
        $this->orderContextRepository = $orderContextRepository;
        $this->lineItemListRepository = $lineItemListRepository;
        $this->aclAuthReadHelper = $aclAuthReadHelper;
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    public function getFullTextSearchFields(): array
    {
        return [
            'ordernumber',
            'order_reference',
            'requested_delivery_date',
            'created_at',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [
            '%2$s' => [
                'debtorContact' => 'email',
            ],
        ];
    }

    public function fetchOneByOrderContextId(
        IdValue $orderContextId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OrderClearanceEntity {
        $rawOrder = $this->createBaseQueryBuilder()
            ->addSelect(self::TABLE_ALIAS . '.*')
            ->where(self::TABLE_ALIAS . '.id = :orderContextId')
            ->andWhere(self::TABLE_ALIAS . '.state = :status')
            ->andWhere('auth.context_owner_id = :contextOwner')
            ->setParameter('orderContextId', $orderContextId->getStorageValue())
            ->setParameter('status', self::STATE_CLEARANCE_OPEN)
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue())
            ->execute()
            ->fetch();

        if (!$rawOrder) {
            throw new NotFoundException(
                'Could not find an order with context id "' . $orderContextId->getValue() . '"
                 and status "' . self::STATE_CLEARANCE_OPEN . '"'
            );
        }

        return $this->createOrderClearance(
            $rawOrder,
            IdValue::create($rawOrder['auth_id']),
            $currencyContext,
            $ownershipContext
        );
    }

    /**
     * @return OrderClearanceEntity[]
     */
    public function fetchAllOrderClearances(Identity $identity, OrderClearanceSearchStruct $searchStruct, CurrencyContext $currencyContext): array
    {
        $ownershipContext = $identity->getOwnershipContext();

        $queryBuilder = $this
            ->createBaseQueryBuilder()
            ->addSelect([
                self::TABLE_ALIAS . '.*',
                self::TABLE_ALIAS . '.id as order_context_id',
            ])
            ->andWhere(self::TABLE_ALIAS . '.state = :status')
            ->andWhere('auth.context_owner_id = :contextOwner')
            ->setParameter('status', self::STATE_CLEARANCE_OPEN)
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue())
            ->innerJoin(self::TABLE_ALIAS, 'b2b_debtor_contact', 'debtorContact', self::TABLE_ALIAS . '.auth_id = debtorContact.auth_id');

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        } elseif (isset(self::CROSS_TABLE_COLUMNS[$searchStruct->orderBy])) {
            $searchStruct->orderBy = self::CROSS_TABLE_COLUMNS[$searchStruct->orderBy];
        }

        $this->dbalHelper
            ->applySearchStruct($searchStruct, $queryBuilder);

        $this->aclAuthReadHelper->applyAclVisibility($ownershipContext, $queryBuilder);

        $queryBuilder->addOrderBy(self::TABLE_ALIAS . '.created_at', 'ASC');
        $rawOrderClearances = $queryBuilder->execute()->fetchAll();

        $orders = [];
        foreach ($rawOrderClearances as $rawOrderClearance) {
            $orders[] = $this->createOrderClearance(
                $rawOrderClearance,
                IdValue::create($rawOrderClearance['auth_id']),
                $currencyContext,
                $ownershipContext
            );
        }

        return $orders;
    }

    public function fetchTotalCount(Identity $identity, OrderClearanceSearchStruct $searchStruct): int
    {
        $ownershipContext = $identity->getOwnershipContext();

        $query = $this->createBaseQueryBuilder()
            ->select('COUNT(DISTINCT ' . self::TABLE_ALIAS . '.id)')
            ->where(self::TABLE_ALIAS . '.state = :status')
            ->andWhere('auth.context_owner_id = :contextOwnerId')
            ->setParameter('status', self::STATE_CLEARANCE_OPEN)
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue())
            ->innerJoin(self::TABLE_ALIAS, 'b2b_debtor_contact', 'debtorContact', self::TABLE_ALIAS . '.auth_id = debtorContact.auth_id');

        $this->dbalHelper->applyFilters($searchStruct, $query);

        $this->aclAuthReadHelper->applyAclVisibility($ownershipContext, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn();
    }

    public function belongsOrderContextIdToDebtor(Identity $identity, IdValue $orderContextId): bool
    {
        $contextOwnerId = $identity->getOwnershipContext()->contextOwnerId;

        return (bool) $this->createBaseQueryBuilder()
            ->addSelect('COUNT(*)')
            ->where(self::TABLE_ALIAS . '.id = :orderContextId')
            ->andWhere('auth.context_owner_id = :contextOwnerId')
            ->setParameter('orderContextId', $orderContextId->getStorageValue())
            ->setParameter('contextOwnerId', $contextOwnerId->getStorageValue())
            ->execute()
            ->fetchColumn();
    }

    public function acceptOrderClearance(OrderClearanceEntity $orderClearance, OwnershipContext $clearingUser): void
    {
        $data = $orderClearance->toDatabaseArray();
        $data['cleared_at'] = (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT);
        $data['cleared_by'] = $clearingUser->authId->getStorageValue();

        $this->setOrderStatusName(
            $orderClearance->id,
            OrderRepository::STATE_ORDER_OPEN,
            $data
        );

        $this->shopOrderRepository
            ->testStatusInSync($orderClearance->id, OrderRepository::STATE_ORDER_OPEN);
    }

    public function declineOrderClearance(IdValue $orderContextId, string $comment): void
    {
        $this->setOrderStatusName(
            $orderContextId,
            self::STATE_CLEARANCE_DENIED,
            [
                'comment' => $comment,
                'declined_at' => (new DateTime())->format(MysqlRepository::MYSQL_DATETIME_FORMAT),
            ]
        );
    }

    public function sendToOrderClearance(IdValue $orderContextId, OwnershipContext $ownershipContext = null): void
    {
        $this->setOrderStatusName($orderContextId, self::STATE_CLEARANCE_OPEN, []);
    }

    /**
     * @internal
     */
    protected function setOrderStatusName(
        IdValue $orderContextId,
        string $statusName,
        array $additionalData
    ): void {
        $success = (bool) $this->connection->update(
            self::TABLE_NAME,
            array_merge(
                $additionalData,
                ['state' => $statusName]
            ),
            ['id' => $orderContextId->getStorageValue()]
        );

        if (!$success) {
            throw new CanNotUpdateExistingRecordException(
                "Could not update b2b order context status with context id {$orderContextId->getValue()}"
            );
        }
    }

    public function deleteOrder(IdValue $orderContextId, OwnershipContext $ownershipContext): void
    {
        $orderContext = $this->orderContextRepository
            ->fetchOneOrderContextById($orderContextId, $ownershipContext);

        $this->connection->createQueryBuilder()
            ->delete(self::TABLE_NAME)
            ->where('id = :orderId')
            ->setParameter('orderId', $orderContext->id->getStorageValue())
            ->execute();

        $this->connection->delete(
            'b2b_line_item_list',
            ['id' => $orderContext->listId->getStorageValue()]
        );
    }

    /**
     * @internal
     */
    protected function createBaseQueryBuilder(): QueryBuilder
    {
        $queryBuilder = $this->connection->createQueryBuilder()
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->innerJoin(self::TABLE_ALIAS, 'b2b_store_front_auth', 'auth', self::TABLE_ALIAS . '.auth_id = auth.id');

        return $queryBuilder;
    }

    /**
     * @internal
     */
    protected function createOrderClearance(
        array $data,
        IdValue $authId,
        CurrencyContext $currencyContext,
        OwnershipContext $ownershipContext
    ): OrderClearanceEntity {
        $orderClearance = $this->entityFactory
            ->createOrderEntityFromDatabase($data, $ownershipContext);

        $orderClearance->list = $this->lineItemListRepository
            ->fetchOneListById($orderClearance->listId, $currencyContext, $ownershipContext);

        try {
            $identity = $this->authenticationService->getIdentityByAuthId($authId);
        } catch (NotFoundException $e) {
            return $orderClearance;
        }

        $orderClearance->userPostalSettings = $identity->getPostalSettings();

        return $orderClearance;
    }

    public function setStatusToOrderClearance(OrderClearanceEntity $orderClearance): void
    {
        $this->connection->update(
            self::TABLE_NAME,
            [
                'state' => OrderClearanceRepository::STATE_CLEARANCE_OPEN,
                'auth_id' => $orderClearance->authId,
            ],
            [
                'id' => $orderClearance->id,
            ]
        );
    }
}
