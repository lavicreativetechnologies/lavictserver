<div style="font-family:arial; font-size:12px;">
    {include file="string:{config name=emailheaderhtml}"}
    <br/>
    <p>
        Hallo {$salutation|salutation} {$lastname},<br/>
        <br/>
        Ihre Bestellung mit der Bestellnummer {$orderNumber} wurde akzeptiert!<br/>
        <br/>
        Den aktuellen Status Ihrer Bestellung können Sie auch jederzeit auf unserer Webseite im Bereich "Mein Konto" -
        "Bestellungen" abrufen.
    </p>
    {include file="string:{config name=emailfooterhtml}"}
</div>
