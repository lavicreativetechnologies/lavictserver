<div style="font-family:arial; font-size:12px;">
    {include file="string:{config name=emailheaderhtml}"}
    <br/>
    <p>
        Hello {$salutation|salutation} {$lastname},<br/>
        <br/>
        Your order with the order number {$orderNumber} has been accepted!<br/>
        <br/>
        You can also check the current status of your order on our website under "My Account" - "Orders" at any time.
    </p>
    {include file="string:{config name=emailfooterhtml}"}
</div>
