{include file="string:{config name=emailheaderplain}"}

Hello {$debtorSalutation|salutation} {$debtorLastName},

Your contact "{$contactFirstName} {$contactLastName}" has requested an order clearance!

To edit the order clearance, you can always retrieve them on {config name=shopName} under "My Account" -
"Orders" in the field of order clearances.

In the clearance process, you have the option of fully viewing, editing, accepting and rejecting the order.

{include file="string:{config name=emailfooterplain}"}
