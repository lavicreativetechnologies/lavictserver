<div style="font-family:arial; font-size:12px;">
    {include file="string:{config name=emailheaderhtml}"}
    <br/>
    <p>
        Hello {$debtorSalutation|salutation} {$debtorLastName},<br/>
        <br/>
        Your contact "{$contactFirstName} {$contactLastName}" has requested an order clearance!<br/>
        <br/>
        To edit the order clearance, you can always retrieve them on {config name=shopName} under "My Account" -
        "Orders" in the field of order clearances.<br/>
        <br/>
        In the clearance process, you have the option of fully viewing, editing, accepting and rejecting the order.
    </p>
    {include file="string:{config name=emailfooterhtml}"}
</div>
