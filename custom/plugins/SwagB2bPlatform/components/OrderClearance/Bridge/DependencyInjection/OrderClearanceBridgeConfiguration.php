<?php declare(strict_types=1);

namespace Shopware\B2B\OrderClearance\Bridge\DependencyInjection;

use Shopware\B2B\Common\DependencyInjectionConfiguration;
use Shopware\B2B\Common\Version\ShopwareVersion;
use Shopware\B2B\LineItemList\Bridge\DependencyInjection\LineItemListBridgeConfiguration;
use Shopware\B2B\Order\Bridge\DependencyInjection\OrderBridgeConfiguration;
use Shopware\B2B\OrderClearance\BridgePlatform\DependencyInjection\OrderClearanceBridgeConfiguration as PlatformOrderClearanceBridgeConfiguration;
use Shopware\B2B\StoreFrontAuthentication\Framework\DependencyInjection\StoreFrontAuthenticationFrameworkConfiguration;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderClearanceBridgeConfiguration extends DependencyInjectionConfiguration
{
    public static function create(): DependencyInjectionConfiguration
    {
        if (ShopwareVersion::isPlatform()) {
            return new PlatformOrderClearanceBridgeConfiguration();
        }

        return new self();
    }

    private function __construct()
    {
    }

    public static function createAclTables(): array
    {
        return [];
    }

    public function getServiceFiles(ContainerBuilder $containerBuilder): array
    {
        return [
            __DIR__ . '/bridge-services.xml',
        ];
    }

    public function getCompilerPasses(): array
    {
        return [
            new OrderItemLoaderCollector(),
        ];
    }

    public function getDependingConfigurations(): array
    {
        return [
            OrderBridgeConfiguration::create(),
            LineItemListBridgeConfiguration::create(),
            new StoreFrontAuthenticationFrameworkConfiguration(),
        ];
    }
}
