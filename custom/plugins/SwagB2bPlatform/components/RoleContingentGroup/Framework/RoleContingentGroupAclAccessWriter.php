<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContingentGroup\Framework;

use Shopware\B2B\Acl\Framework\AclAccessWriterInterface;
use Shopware\B2B\Acl\Framework\AclGrantContext;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;

class RoleContingentGroupAclAccessWriter implements AclAccessWriterInterface
{
    /**
     * @var RoleContingentGroupRepository
     */
    private $roleContingentGroupRepository;

    /**
     * @var AclAccessWriterInterface
     */
    private $decoratedService;

    public function __construct(
        AclAccessWriterInterface $decoratedService,
        RoleContingentGroupRepository $roleContingentGroupRepository
    ) {
        $this->decoratedService = $decoratedService;
        $this->roleContingentGroupRepository = $roleContingentGroupRepository;
    }

    public function addNewSubject(
        OwnershipContext $context,
        AclGrantContext $grantContext,
        IdValue $subjectId,
        bool $grantable
    ): void {
        $this->decoratedService->addNewSubject($context, $grantContext, $subjectId, $grantable);

        if ($grantContext->getEntity() instanceof RoleEntity) {
            $this->roleContingentGroupRepository->assignRoleContingentGroup($grantContext->getEntity()->id, $subjectId);
        }
    }

    public function testUpdateAllowed(OwnershipContext $ownershipContext, IdValue $subjectId): void
    {
        $this->decoratedService->testUpdateAllowed($ownershipContext, $subjectId);
    }
}
