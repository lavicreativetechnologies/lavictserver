<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContingentGroup\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class RoleContingentGroupApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/role/{roleId}/contingentgroup',
                'b2b_role_contingent_group.api_role_contingent_group_controller',
                'getList',
                ['roleId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/role/{roleId}/contingentgroup',
                'b2b_role_contingent_group.api_role_contingent_group_controller',
                'create',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/role/{roleId}/contingentgroup',
                'b2b_role_contingent_group.api_role_contingent_group_controller',
                'remove',
                ['roleId'],
            ],
        ];
    }
}
