<?php declare(strict_types=1);

namespace Shopware\B2B\RoleContingentGroup\Api;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\RoleContingentGroup\Framework\RoleContingentGroupAssignmentService;
use Shopware\B2B\RoleContingentGroup\Framework\RoleContingentGroupRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;
use function count;

class RoleContingentGroupController
{
    /**
     * @var RoleContingentGroupAssignmentService
     */
    private $roleContingentGroupAssignmentService;

    /**
     * @var RoleContingentGroupRepository
     */
    private $roleContingentGroupRepository;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        RoleContingentGroupRepository $roleContingentGroupRepository,
        RoleContingentGroupAssignmentService $roleContingentGroupAssignmentService,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->roleContingentGroupAssignmentService = $roleContingentGroupAssignmentService;
        $this->roleContingentGroupRepository = $roleContingentGroupRepository;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(string $roleId): array
    {
        $roleId = IdValue::create($roleId);
        $context = $this->getDebtorOwnershipContext();

        $roles = $this->roleContingentGroupRepository
            ->fetchAllRolesAndCheckForContingentGroupAssignment($roleId, $context);

        $totalCount = count($roles);

        return ['success' => true, 'roles' => $roles, 'totalCount' => $totalCount];
    }

    public function createAction(string $roleId, Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();

        $crudRequest = $this->roleContingentGroupAssignmentService->createAssignRecordRequest(
            array_merge(
                $request->getPost(),
                ['roleId' => $roleId]
            )
        );

        $this->roleContingentGroupAssignmentService
            ->assign($crudRequest, $ownershipContext);

        return ['success' => true];
    }

    public function removeAction(string $roleId, Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();

        $crudRequest = $this->roleContingentGroupAssignmentService->createAssignRecordRequest(
            array_merge(
                $request->getPost(),
                ['roleId' => $roleId]
            )
        );

        $this->roleContingentGroupAssignmentService
            ->removeAssignment($crudRequest, $ownershipContext);

        return ['success' => true];
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
