<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContingentRoleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}',
                'b2b_contingent_group.api_contingent_role_controller',
                'getAllAllowed',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/grant',
                'b2b_contingent_group.api_contingent_role_controller',
                'getAllGrant',
                ['roleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/contingent/{contingentId}',
                'b2b_contingent_group.api_contingent_role_controller',
                'getAllowed',
                ['roleId', 'contingentId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/contingent/{contingentId}',
                'b2b_contingent_group.api_contingent_role_controller',
                'allow',
                ['roleId', 'contingentId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/contingent/{contingentId}/grant',
                'b2b_contingent_group.api_contingent_role_controller',
                'allowGrant',
                ['roleId', 'contingentId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/allow',
                'b2b_contingent_group.api_contingent_role_controller',
                'multipleAllow',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/deny',
                'b2b_contingent_group.api_contingent_role_controller',
                'multipleDeny',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contingent_role/{roleId}/contingent/{contingentId}',
                'b2b_contingent_group.api_contingent_role_controller',
                'deny',
                ['roleId', 'contingentId'],
            ],
        ];
    }
}
