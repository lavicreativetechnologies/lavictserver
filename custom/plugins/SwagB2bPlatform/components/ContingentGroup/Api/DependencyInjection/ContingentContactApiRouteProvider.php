<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class ContingentContactApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}',
                'b2b_contingent_group.api_contingent_contact_controller',
                'getAllAllowed',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/grant',
                'b2b_contingent_group.api_contingent_contact_controller',
                'getAllGrant',
                ['contactEmail'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/contingent/{contingentId}',
                'b2b_contingent_group.api_contingent_contact_controller',
                'getAllowed',
                ['contactEmail', 'contingentId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/contingent/{contingentId}',
                'b2b_contingent_group.api_contingent_contact_controller',
                'allow',
                ['contactEmail', 'contingentId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/contingent/{contingentId}/grant',
                'b2b_contingent_group.api_contingent_contact_controller',
                'allowGrant',
                ['contactEmail', 'contingentId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/allow',
                'b2b_contingent_group.api_contingent_contact_controller',
                'multipleAllow',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/deny',
                'b2b_contingent_group.api_contingent_contact_controller',
                'multipleDeny',
                ['contactEmail'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/contingent_contact/{contactEmail}/contingent/{contingentId}',
                'b2b_contingent_group.api_contingent_contact_controller',
                'deny',
                ['contactEmail', 'contingentId'],
            ],
        ];
    }
}
