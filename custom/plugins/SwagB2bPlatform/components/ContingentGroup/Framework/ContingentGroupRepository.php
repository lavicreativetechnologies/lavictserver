<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Framework;

use Doctrine\DBAL\Connection;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Company\Framework\CompanyFilterHelper;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;
use function sprintf;

class ContingentGroupRepository implements GridRepository
{
    const TABLE_NAME = 'b2b_contingent_group';

    const TABLE_ALIAS = 'contingent_groups';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    /**
     * @var ContingentGroupCompanyAssignmentFilter
     */
    private $contingentGroupCompanyAssignmentFilter;

    /**
     * @var CompanyFilterHelper
     */
    private $companyFilterHelper;

    /**
     * @var ContingentGroupCompanyInheritanceFilter
     */
    private $contingentGroupCompanyInheritanceFilter;

    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        AclReadHelper $aclReadHelper,
        ContingentGroupCompanyAssignmentFilter $contingentGroupCompanyAssignmentFilter,
        CompanyFilterHelper $companyFilterHelper,
        ContingentGroupCompanyInheritanceFilter $contingentGroupCompanyInheritanceFilter
    ) {
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->aclReadHelper = $aclReadHelper;
        $this->contingentGroupCompanyAssignmentFilter = $contingentGroupCompanyAssignmentFilter;
        $this->companyFilterHelper = $companyFilterHelper;
        $this->contingentGroupCompanyInheritanceFilter = $contingentGroupCompanyInheritanceFilter;
    }

    /**
     * @return ContingentGroupEntity[]
     */
    public function fetchList(OwnershipContext $context, ContingentGroupSearchStruct $searchStruct): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select(self::TABLE_ALIAS . '.*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.context_owner_id = :owner')
            ->setParameter('owner', $context->contextOwnerId->getStorageValue());

        $this->aclReadHelper->applyAclVisibility($context, $query);
        $this->companyFilterHelper
            ->applyFilter(
                $this->aclReadHelper,
                $this->contingentGroupCompanyAssignmentFilter,
                $searchStruct,
                $query,
                $this->contingentGroupCompanyInheritanceFilter
            );

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $contingentGroupsData = $query->execute()
            ->fetchAll(PDO::FETCH_ASSOC);

        $contingentGroups = [];
        foreach ($contingentGroupsData as $contingentGroupData) {
            $contingentGroups[] = (new ContingentGroupEntity())->fromDatabaseArray($contingentGroupData);
        }

        return $contingentGroups;
    }

    public function fetchListByContactId(
        OwnershipContext $context,
        ContingentGroupSearchStruct $searchStruct,
        IdValue $contactId
    ): array {
        $query = $this->connection->createQueryBuilder()
            ->select(
                self::TABLE_ALIAS . '.id',
                self::TABLE_ALIAS . '.context_owner_id',
                self::TABLE_ALIAS . '.name',
                self::TABLE_ALIAS . '.description',
                'contactcontingent.id as assignmentId'
            )
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->leftJoin(
                self::TABLE_ALIAS,
                'b2b_contact_contingent_group',
                'contactcontingent',
                self::TABLE_ALIAS . '.id = contactcontingent.contingent_group_id and contactcontingent.contact_id = :contactId'
            )
            ->where(self::TABLE_ALIAS . '.context_owner_id = :owner')
            ->setParameter('owner', $context->contextOwnerId->getStorageValue())
            ->setParameter('contactId', $contactId->getStorageValue());

        $this->aclReadHelper->applyAclVisibility($context, $query);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $statement = $query->execute();

        $contingentGroupsData = $statement
            ->fetchAll(PDO::FETCH_ASSOC);

        $contingentGroups = [];
        foreach ($contingentGroupsData as $contingentGroupData) {
            $contingentGroups[] = (new ContingentGroupAssignmentEntity())->fromDatabaseArray($contingentGroupData);
        }

        return $contingentGroups;
    }

    public function fetchTotalCount(
        OwnershipContext $context,
        ContingentGroupSearchStruct $searchStruct
    ): int {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.context_owner_id = :owner')
            ->setParameter('owner', $context->contextOwnerId->getStorageValue());

        $this->aclReadHelper->applyAclVisibility($context, $query);

        $this->companyFilterHelper
            ->applyFilter(
                $this->aclReadHelper,
                $this->contingentGroupCompanyAssignmentFilter,
                $searchStruct,
                $query,
                $this->contingentGroupCompanyInheritanceFilter
            );

        $this->dbalHelper->applyFilters($searchStruct, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn(0);
    }

    public function fetchOneById(IdValue $id, OwnershipContext $ownershipContext): ContingentGroupEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.id = :id')
            ->andWhere(self::TABLE_ALIAS . '.context_owner_id = :contextOwner')
            ->setParameter('id', $id->getStorageValue())
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue());

        $contingentGroupData = $query->execute()->fetch(PDO::FETCH_ASSOC);

        if (!$contingentGroupData) {
            throw new NotFoundException(sprintf('Contingent Group not found for %s', $id));
        }

        $contingentGroup = new ContingentGroupEntity();

        $contingentGroup->fromDatabaseArray($contingentGroupData);

        return $contingentGroup;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException
     */
    public function addContingentGroup(
        ContingentGroupEntity $contingentGroupEntity,
        OwnershipContext $ownershipContext
    ): ContingentGroupEntity {
        if (!$contingentGroupEntity->isNew()) {
            throw new CanNotInsertExistingRecordException('The Contingent Group provided already exists');
        }

        $this->connection->insert(
            self::TABLE_NAME,
            array_merge(
                $contingentGroupEntity->toDatabaseArray(),
                ['context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue()]
            )
        );

        $contingentGroupEntity->id = IdValue::create($this->connection->lastInsertId());

        return $contingentGroupEntity;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException
     */
    public function updateContingentGroup(
        ContingentGroupEntity $contingentGroupEntity,
        OwnershipContext $ownershipContext
    ): ContingentGroupEntity {
        if ($contingentGroupEntity->isNew()) {
            throw new CanNotUpdateExistingRecordException('The Contingent Group provided does not exist');
        }

        $this->connection->update(
            self::TABLE_NAME,
            $contingentGroupEntity->toDatabaseArray(),
            [
                'id' => $contingentGroupEntity->id->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );

        return $contingentGroupEntity;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotRemoveUsedRecordException
     * @throws \Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException
     */
    public function removeContingentGroup(
        ContingentGroupEntity $contingentGroupEntity,
        OwnershipContext $ownershipContext
    ): ContingentGroupEntity {
        if ($contingentGroupEntity->isNew()) {
            throw new CanNotRemoveExistingRecordException('The Contingent Group provided does not exist');
        }

        $this->connection->delete(
            self::TABLE_NAME,
            [
                'id' => $contingentGroupEntity->id->getStorageValue(),
                'context_owner_id' => $ownershipContext->contextOwnerId->getStorageValue(),
            ]
        );

        $contingentGroupEntity->id = IdValue::null();

        return $contingentGroupEntity;
    }

    /**
     * @return string query alias for filter construction
     */
    public function getMainTableAlias(): string
    {
        return self::TABLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'name',
            'description',
        ];
    }

    /**
     * @return int[]
     */
    public function fetchContingentGroupIdsForContact(IdValue $identityId): array
    {
        $contingentQueryRoles = $this->connection->createQueryBuilder()
            ->select('rcg.contingent_group_id')
            ->from('b2b_role_contingent_group', 'rcg')
            ->innerJoin('rcg', 'b2b_role_contact', 'rc', 'rcg.role_id = rc.role_id')
            ->where('rc.debtor_contact_id = :id')
            ->getSQL();

        $contingentQueryContacts = $this->connection->createQueryBuilder()
            ->select('ccg.contingent_group_id')
            ->from('b2b_contact_contingent_group', 'ccg')
            ->where('ccg.contact_id = :id')
            ->getSQL();

        $sql = "( {$contingentQueryRoles} ) UNION ( {$contingentQueryContacts} )";
        $contingentGroupData = $this->connection
            ->executeQuery($sql, ['id' => $identityId->getStorageValue()])
            ->fetchAll();

        $contingentGroups = [];
        foreach ($contingentGroupData as $group) {
            $contingentGroups[] = IdValue::create($group['contingent_group_id']);
        }

        return $contingentGroups;
    }

    public function fetchRuleTypesFromContingentGroup(IdValue $groupId): array
    {
        $ruleTypes = [];
        $ruleTypesData = $this->connection->createQueryBuilder()
            ->select('DISTINCT(type)')
            ->from('b2b_contingent_group_rule', 'cgr')
            ->where('cgr.contingent_group_id = :id')
            ->setParameter('id', $groupId->getStorageValue())
            ->execute()
            ->fetchAll();

        foreach ($ruleTypesData as $type) {
            $ruleTypes[] = $type['type'];
        }

        return $ruleTypes;
    }

    /**
     * @param $searchStruct
     * @return ContingentGroupAssignmentEntity[]
     */
    public function fetchAllContingentGroupsWithCheckForRoleAssignment(
        OwnershipContext $context,
        ContingentGroupSearchStruct $searchStruct,
        IdValue $roleId
    ): array {
        $query = $this->connection->createQueryBuilder()
            ->select(
                self::TABLE_ALIAS . '.id',
                self::TABLE_ALIAS . '.name',
                self::TABLE_ALIAS . '.description',
                self::TABLE_ALIAS . '.context_owner_id',
                'rc.id as assignmentId'
            )
            ->from(self::TABLE_NAME, self::TABLE_ALIAS)
            ->leftJoin(
                self::TABLE_ALIAS,
                'b2b_role_contingent_group',
                'rc',
                self::TABLE_ALIAS . '.id = rc.contingent_group_id and rc.role_id = :id'
            )
            ->where(self::TABLE_ALIAS . '.context_owner_id = :contextId')
            ->setParameter('id', $roleId->getStorageValue())
            ->setParameter('contextId', $context->contextOwnerId->getStorageValue());

        $this->aclReadHelper->applyAclVisibility($context, $query);

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);

        $statement = $query->execute();
        $groupData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $groups = [];
        foreach ($groupData as $roleData) {
            $groups[] = (new ContingentGroupAssignmentEntity())->fromDatabaseArray($roleData);
        }

        return $groups;
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }
}
