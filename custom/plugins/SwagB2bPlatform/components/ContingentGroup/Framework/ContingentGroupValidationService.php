<?php declare(strict_types=1);

namespace Shopware\B2B\ContingentGroup\Framework;

use Shopware\B2B\Common\Validator\ValidationBuilder;
use Shopware\B2B\Common\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContingentGroupValidationService
{
    /**
     * @var ValidationBuilder
     */
    private $validationBuilder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        ValidationBuilder $validationBuilder,
        ValidatorInterface $validator
    ) {
        $this->validationBuilder = $validationBuilder;
        $this->validator = $validator;
    }

    public function createInsertValidation(ContingentGroupEntity $contingentGroup): Validator
    {
        return $this->createCrudValidation($contingentGroup)
            ->validateThat('id', $contingentGroup->id)
            ->isNullIdValue()
            ->getValidator($this->validator);
    }

    public function createUpdateValidation(ContingentGroupEntity $contingentGroup): Validator
    {
        return $this->createCrudValidation($contingentGroup)
            ->validateThat('id', $contingentGroup->id)
            ->isIdValue()

            ->getValidator($this->validator);
    }

    /**
     * @internal
     */
    protected function createCrudValidation(ContingentGroupEntity $contingentGroup): ValidationBuilder
    {
        return $this->validationBuilder

            ->validateThat('name', $contingentGroup->name)
            ->isStringShorterThan(ValidationBuilder::MAX_STRING_LENGTH_255)
            ->isNotBlank()
            ->isString();
    }
}
