<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\BridgePlatform;

use Shopware\B2B\Budget\Framework\BudgetStatus;
use Shopware\Core\Framework\Event\EventData\EventDataType;
use Shopware\Core\Framework\Event\EventData\ObjectType;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;

class BudgetStatusMailData
{
    /**
     * @var BudgetStatus
     */
    private $budgetStatus;

    public function __construct(BudgetStatus $budgetStatus)
    {
        $this->budgetStatus = $budgetStatus;
    }

    public static function getStatusDataType(): EventDataType
    {
        $status = new ObjectType();
        $status->add('usedBudget', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $status->add('availableBudget', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $status->add('remainingBudget', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $status->add('percentage', new ScalarValueType(ScalarValueType::TYPE_FLOAT));
        $status->add('isSufficient', new ScalarValueType(ScalarValueType::TYPE_BOOL));
        $status->add('currencyFactor', new ScalarValueType(ScalarValueType::TYPE_FLOAT));

        return $status;
    }

    public function getUsedBudget(): float
    {
        return $this->budgetStatus->usedBudget;
    }

    public function getAvailableBudget(): float
    {
        return $this->budgetStatus->availableBudget;
    }

    public function getRemainingBudget(): float
    {
        return $this->budgetStatus->remainingBudget;
    }

    public function getPercentage(): float
    {
        return $this->budgetStatus->percentage;
    }

    public function isSufficient(): bool
    {
        return $this->budgetStatus->isSufficient;
    }

    public function getCurrencyFactor(): float
    {
        return $this->budgetStatus->currencyFactor;
    }
}
