<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\BridgePlatform;

use Shopware\B2B\Budget\Framework\BudgetEntity;
use Shopware\B2B\StoreFrontAuthentication\Framework\UserPostalSettings;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\BusinessEventInterface;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\MailActionInterface;
use Symfony\Contracts\EventDispatcher\Event;

class BudgetNotificationMailEvent extends Event implements BusinessEventInterface, MailActionInterface
{
    public const EVENT_NAME = 'b2b.budget.notifiy.author';

    /**
     * @var BudgetEntityMailData
     */
    private $budget;

    /**
     * @var UserPostalSettings
     */
    private $postalSettings;

    public function __construct(BudgetEntity $budgetEntity, UserPostalSettings $postalSettings)
    {
        $this->budget = new BudgetEntityMailData($budgetEntity);
        $this->postalSettings = $postalSettings;
    }

    public static function getAvailableData(): EventDataCollection
    {
        return (new EventDataCollection())
            ->add('budget', BudgetEntityMailData::getBudgetMailDataType());
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getMailStruct(): MailRecipientStruct
    {
        return new MailRecipientStruct([
            $this->postalSettings->email => $this->postalSettings->firstName . ' ' . $this->postalSettings->lastName,
        ]);
    }

    public function getSalesChannelId(): ?string
    {
        // TODO: ENT-2227 - Send mail while runtime not in scheduled task, and set salesChannelId
        return Defaults::SALES_CHANNEL;
    }

    public function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    public function getBudget(): BudgetEntityMailData
    {
        return $this->budget;
    }
}
