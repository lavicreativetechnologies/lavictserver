<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\BridgePlatform;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class BudgetNotificationTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'B2bBudgetNotifyAuthor';
    }

    /**
     * @return int the default interval this task should run in seconds
     */
    public static function getDefaultInterval(): int
    {
        return 86400;
    }
}
