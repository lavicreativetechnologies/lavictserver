<?php declare(strict_types=1);

namespace Shopware\B2B\Budget\Framework;

use Shopware\B2B\Cart\Framework\CartAccessContext;
use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\CartAccessStrategyInterface;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use function array_walk;
use function count;

class BudgetCartOrderAccessStrategy implements CartAccessStrategyInterface
{
    /**
     * @var Identity
     */
    private $identity;

    /**
     * @var BudgetService
     */
    private $budgetService;

    /**
     * @var CurrencyContext
     */
    private $currencyContext;

    public function __construct(Identity $identity, BudgetService $budgetService, CurrencyContext $currencyContext)
    {
        $this->identity = $identity;
        $this->budgetService = $budgetService;
        $this->currencyContext = $currencyContext;
    }

    public function checkAccess(CartAccessContext $context, CartAccessResult $cartAccessResult): void
    {
        $amountNet = 0;

        if ($context->orderClearanceEntity->list->amountNet) {
            $amountNet = $context->orderClearanceEntity->list->amountNet;
        }

        $budgets = $this->budgetService->getUserSelectableBudgetsWithStatus(
            $this->identity->getOwnershipContext(),
            $amountNet,
            $this->currencyContext
        );

        if (!count($budgets)) {
            $cartAccessResult->addError(
                __CLASS__,
                'BudgetMissingError'
            );

            return;
        }

        $canSpend = false;
        array_walk($budgets, function ($budget) use (&$canSpend): void {
            if ($budget->currentStatus->isSufficient) {
                $canSpend = true;
            }
        });

        if ($canSpend) {
            return;
        }

        $cartAccessResult->addError(
            __CLASS__,
            'BudgetSpendError'
        );
    }

    public function addInformation(CartAccessResult $cartAccessResult): void
    {
        //nth
    }
}
