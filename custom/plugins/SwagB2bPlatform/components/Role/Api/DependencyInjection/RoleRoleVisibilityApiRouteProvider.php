<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class RoleRoleVisibilityApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}',
                'b2b_role.api_role_role_visibility_controller',
                'getAllAllowed',
                ['baseRoleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/grant',
                'b2b_role.api_role_role_visibility_controller',
                'getAllGrant',
                ['baseRoleId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/role/{roleId}',
                'b2b_role.api_role_role_visibility_controller',
                'getAllowed',
                ['baseRoleId', 'roleId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/role/{roleId}',
                'b2b_role.api_role_role_visibility_controller',
                'allow',
                ['baseRoleId', 'roleId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/role/{roleId}/grant',
                'b2b_role.api_role_role_visibility_controller',
                'allowGrant',
                ['baseRoleId', 'roleId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/allow',
                'b2b_role.api_role_role_visibility_controller',
                'multipleAllow',
                ['baseRoleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/deny',
                'b2b_role.api_role_role_visibility_controller',
                'multipleDeny',
                ['baseRoleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/role_role/{baseRoleId}/role/{roleId}',
                'b2b_role.api_role_role_visibility_controller',
                'deny',
                ['baseRoleId', 'roleId'],
            ],
        ];
    }
}
