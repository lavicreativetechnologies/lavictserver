<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Api\DependencyInjection;

use Shopware\B2B\Common\Routing\RouteProvider;

class RoleApiRouteProvider implements RouteProvider
{
    public function getRoutes(): array
    {
        return [
            [
                'GET',
                '/debtor/{debtorIdentifier}/role',
                'b2b_role.api_role_controller',
                'getList',
                [],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/role/{parentId}/children',
                'b2b_role.api_role_controller',
                'getChildren',
                ['parentId'],
            ],
            [
                'GET',
                '/debtor/{debtorIdentifier}/role/{roleId}',
                'b2b_role.api_role_controller',
                'get',
                ['roleId'],
            ],
            [
                'POST',
                '/debtor/{debtorIdentifier}/role',
                'b2b_role.api_role_controller',
                'create',
                [],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/role/{roleId}',
                'b2b_role.api_role_controller',
                'update',
                ['roleId'],
            ],
            [
                'PUT',
                '/debtor/{debtorIdentifier}/role/{roleId}/move',
                'b2b_role.api_role_controller',
                'move',
                ['roleId'],
            ],
            [
                'DELETE',
                '/debtor/{debtorIdentifier}/role/{roleId}',
                'b2b_role.api_role_controller',
                'remove',
                ['roleId'],
            ],
        ];
    }
}
