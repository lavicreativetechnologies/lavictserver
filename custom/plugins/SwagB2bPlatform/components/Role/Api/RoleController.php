<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Api;

use Shopware\B2B\Common\Controller\GridHelper;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Role\Framework\RoleCrudService;
use Shopware\B2B\Role\Framework\RoleEntity;
use Shopware\B2B\Role\Framework\RoleRepository;
use Shopware\B2B\Role\Framework\RoleSearchStruct;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthStorageAdapterInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_merge;

class RoleController
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var GridHelper
     */
    private $requestHelper;

    /**
     * @var RoleCrudService
     */
    private $roleCrudService;

    /**
     * @var AuthStorageAdapterInterface
     */
    private $authStorageAdapter;

    public function __construct(
        RoleRepository $roleRepository,
        GridHelper $requestHelper,
        RoleCrudService $roleCrudService,
        AuthStorageAdapterInterface $authStorageAdapter
    ) {
        $this->roleRepository = $roleRepository;
        $this->requestHelper = $requestHelper;
        $this->roleCrudService = $roleCrudService;
        $this->authStorageAdapter = $authStorageAdapter;
    }

    public function getListAction(Request $request): array
    {
        $search = new RoleSearchStruct();

        $ownershipContext = $this->getDebtorOwnershipContext();

        $this->requestHelper
            ->extractSearchDataInRestApi($request, $search);

        $this->roleRepository
            ->fetchRoot($ownershipContext);

        $roles = $this->roleRepository
            ->fetchList($search, $ownershipContext);

        $totalCount = $this->roleRepository
            ->fetchTotalCount($search, $ownershipContext);

        $this->removeEmptyChildren(...$roles);

        return ['success' => true, 'roles' => $roles, 'totalCount' => $totalCount];
    }

    public function getAction(string $roleId): array
    {
        $roleId = IdValue::create($roleId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $role = $this->roleRepository
            ->fetchOneById($roleId, $ownershipContext);

        $this->removeEmptyChildren($role);

        return ['success' => true, 'role' => $role];
    }

    public function getChildrenAction(string $parentId): array
    {
        $parentId = IdValue::create($parentId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $children = $this->roleRepository->fetchChildren($parentId, $ownershipContext);

        $this->removeEmptyChildren(...$children);

        return ['success' => true, 'children' => $children];
    }

    public function createAction(Request $request): array
    {
        $ownershipContext = $this->getDebtorOwnershipContext();

        $newRecord = $this->roleCrudService
            ->createNewRecordRequest($request->getPost());

        $role = $this->roleCrudService
            ->create($newRecord, $ownershipContext);

        $this->removeEmptyChildren($role);

        return ['success' => true, 'role' => $role];
    }

    public function updateAction(string $roleId, Request $request): array
    {
        $roleId = IdValue::create($roleId);
        $data = array_merge(
            $request->getPost(),
            ['id' => $roleId]
        );

        $ownershipContext = $this->getDebtorOwnershipContext();

        $existingRecord = $this->roleCrudService
            ->createExistingRecordRequest($data);

        $role = $this->roleCrudService
            ->update($existingRecord, $ownershipContext);

        $this->removeEmptyChildren($role);

        return ['success' => true, 'role' => $role];
    }

    public function moveAction(string $roleId, Request $request)
    {
        $roleId = IdValue::create($roleId);
        $ownershipContext = $this->getDebtorOwnershipContext();

        $data = array_merge(
            $request->getPost(),
            ['roleId' => $roleId]
        );

        $moveRecord = $this->roleCrudService->createMoveRecordRequest($data);

        $role = $this->roleCrudService->move($moveRecord, $ownershipContext);

        $this->removeEmptyChildren($role);

        return ['success' => true, 'role' => $role];
    }

    public function removeAction(string $roleId): array
    {
        $roleId = IdValue::create($roleId);
        $data = [
            'id' => $roleId,
        ];

        $ownershipContext = $this->getDebtorOwnershipContext();
        $existingRecord = $this->roleCrudService
            ->createExistingRecordRequest($data);

        $role = $this->roleCrudService
            ->remove($existingRecord, $ownershipContext);

        $this->removeEmptyChildren($role);

        return ['success' => true, 'role' => $role];
    }

    /**
     * @param RoleEntity ...$roles
     */
    protected function removeEmptyChildren(RoleEntity ...$roles): void
    {
        foreach ($roles as $role) {
            if (empty($role->children)) {
                unset($role->children);
            }
        }
    }

    /**
     * @internal
     */
    protected function getDebtorOwnershipContext(): OwnershipContext
    {
        return $this->authStorageAdapter->getIdentity()->getOwnershipContext();
    }
}
