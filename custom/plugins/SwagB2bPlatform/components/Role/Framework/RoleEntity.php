<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use function get_object_vars;
use function property_exists;

class RoleEntity implements CrudEntity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var IdValue
     */
    public $contextOwnerId;

    /**
     * @var int
     */
    public $left;

    /**
     * @var int
     */
    public $right;

    /**
     * @var int
     */
    public $level;

    /**
     * @var bool
     */
    public $hasChildren;

    /**
     * @var array
     */
    public $children = [];

    public function __construct()
    {
        $this->id = IdValue::null();
        $this->contextOwnerId = IdValue::null();
    }

    public function isNew(): bool
    {
        return $this->id instanceof NullIdValue;
    }

    public function toDatabaseArray(): array
    {
        return [
            'id' => $this->id->getStorageValue(),
            'name' => $this->name,
            '`context_owner_id`' => $this->contextOwnerId->getStorageValue(),
        ];
    }

    public function fromDatabaseArray(array $roleData): CrudEntity
    {
        $this->id = IdValue::create($roleData['id']);
        $this->name = (string) $roleData['name'];
        $this->contextOwnerId = IdValue::create($roleData['context_owner_id']);
        $this->left = (int) $roleData['left'];
        $this->right = (int) $roleData['right'];
        $this->level = (int) $roleData['level'];
        $this->hasChildren = (bool) $roleData['hasChildren'];

        return $this;
    }

    public function setData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }

            $this->{$key} = $value;
        }

        $this->id = IdValue::create($this->id);
        $this->contextOwnerId = IdValue::create($this->contextOwnerId);
    }

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $var) {
            if ($var instanceof IdValue) {
                $vars[$key] = $var->getValue();
            }
        }

        return $vars;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
