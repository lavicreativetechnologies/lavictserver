<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Framework;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use PDO;
use Shopware\B2B\Acl\Framework\AclReadHelper;
use Shopware\B2B\Common\Controller\GridRepository;
use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException;
use Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException;
use Shopware\B2B\Common\Repository\DbalHelper;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use Shopware\DbalNestedSet\NestedSetQueryFactory;
use Shopware\DbalNestedSet\NestedSetWriter;
use function array_map;
use function array_merge;
use function sprintf;

class RoleRepository extends AbstractRoleBaseRepository implements GridRepository
{
    /**
     * @var DbalHelper
     */
    private $dbalHelper;

    /**
     * @var NestedSetQueryFactory
     */
    private $nestedSetQueryFactory;

    /**
     * @var NestedSetWriter
     */
    private $nestedSetWriter;

    /**
     * @var AclRoleRemoveDependencyValidator[]
     */
    private $dependencyValidators;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var AclReadHelper
     */
    private $aclReadHelper;

    /**
     * @param AclRoleRemoveDependencyValidator[] $dependencyValidators
     */
    public function __construct(
        Connection $connection,
        DbalHelper $dbalHelper,
        NestedSetQueryFactory $nestedSetQueryFactory,
        NestedSetWriter $nestedSetWriter,
        AclReadHelper $aclReadHelper,
        array $dependencyValidators
    ) {
        parent::__construct($connection, $aclReadHelper);
        $this->connection = $connection;
        $this->dbalHelper = $dbalHelper;
        $this->nestedSetQueryFactory = $nestedSetQueryFactory;
        $this->nestedSetWriter = $nestedSetWriter;
        $this->dependencyValidators = $dependencyValidators;
        $this->aclReadHelper = $aclReadHelper;
    }

    public function fetchList(RoleSearchStruct $searchStruct, OwnershipContext $ownershipContext): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->from(self::TABLE_ROLE_NAME, self::TABLE_ROLE_ALIAS)
            ->where(self::TABLE_ROLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue());

        if (!$searchStruct->orderBy) {
            $searchStruct->orderBy = self::TABLE_ROLE_ALIAS . '.id';
            $searchStruct->orderDirection = 'DESC';
        }

        $this->dbalHelper->applySearchStruct($searchStruct, $query);
        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $statement = $query->execute();
        $rolesData = $statement->fetchAll(PDO::FETCH_ASSOC);

        $roles = [];
        foreach ($rolesData as $roleData) {
            $roles[] = (new RoleEntity())->fromDatabaseArray($roleData);
        }

        return $roles;
    }

    public function fetchTotalCount(RoleSearchStruct $searchStruct, OwnershipContext $ownershipContext): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from(self::TABLE_ROLE_NAME, self::TABLE_ROLE_ALIAS)
            ->where(self::TABLE_ROLE_ALIAS . '.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId);

        $this->dbalHelper->applyFilters($searchStruct, $query);
        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $statement = $query->execute();

        return (int) $statement->fetchColumn();
    }

    public function fetchRoot(OwnershipContext $ownershipContext): RoleEntity
    {
        $rawRole = $this->nestedSetQueryFactory
            ->createFetchRootsQueryBuilder(self::TABLE_ROLE_NAME, self::TABLE_ROLE_ALIAS)
            ->addSelect(self::TABLE_ROLE_ALIAS . '.*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->andWhere('role.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$rawRole) {
            $rootId = $this->insertRoot($ownershipContext);

            return $this->fetchOneById($rootId, $ownershipContext);
        }

        $role = new RoleEntity();

        $role->fromDatabaseArray($rawRole);

        return $role;
    }

    public function insertRoot(OwnershipContext $ownershipContext): IdValue
    {
        return IdValue::create(
            $this->nestedSetWriter
                ->insertRoot(
                    self::TABLE_ROLE_NAME,
                    'context_owner_id',
                    $ownershipContext->contextOwnerId->getStorageValue(),
                    ['name' => 'root']
                )
        );
    }

    /**
     * @return RoleEntity[]
     */
    public function fetchChildren(IdValue $parentId, OwnershipContext $ownershipContext): array
    {
        $query = $this->nestedSetQueryFactory
            ->createChildrenQueryBuilder(self::TABLE_ROLE_NAME, self::TABLE_ROLE_ALIAS, 'context_owner_id', $parentId->getStorageValue())
            ->addSelect(self::TABLE_ROLE_ALIAS . '.*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->andWhere(self::TABLE_ROLE_ALIAS . '.context_owner_id = :contextOwner')
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue());

        return $this->fetchRoles($query);
    }

    /**
     * @param IdValue[] $selectedRoleIds
     */
    public function fetchSubtree(array $selectedRoleIds, OwnershipContext $ownershipContext): array
    {
        $query = $this->nestedSetQueryFactory
            ->createSubtreeThroughMultipleNodesQueryBuilder(
                self::TABLE_ROLE_NAME,
                self::TABLE_ROLE_ALIAS,
                'context_owner_id',
                array_map(function (IdValue $roleId) { return $roleId->getStorageValue(); }, $selectedRoleIds)
            )
            ->addSelect(self::TABLE_ROLE_ALIAS . '.*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->andWhere('role.context_owner_id = :contextOwnerId')
            ->setParameter('contextOwnerId', $ownershipContext->contextOwnerId->getStorageValue());

        return $this->fetchRoles($query);
    }

    /**
     * @return RoleEntity[]
     */
    public function fetchAclRootRoles(OwnershipContext $ownershipContext, bool $withTechnicalRoot): array
    {
        $query = $this->createRootRolesQueryBuilder($ownershipContext, $withTechnicalRoot);

        return $this->fetchRoles($query);
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotRemoveExistingRecordException
     */
    public function removeRole(RoleEntity $roleEntity): RoleEntity
    {
        if ($roleEntity->isNew()) {
            throw new CanNotRemoveExistingRecordException('The role provided does not exist');
        }

        $this->nestedSetWriter->removeNode(
            self::TABLE_ROLE_NAME,
            'context_owner_id',
            $roleEntity->id->getStorageValue()
        );

        $roleEntity->id = IdValue::null();

        return $roleEntity;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotInsertExistingRecordException
     */
    public function addRole(RoleEntity $role, IdValue $parentId): RoleEntity
    {
        if (!$role->isNew()) {
            throw new CanNotInsertExistingRecordException('The role provided already exists');
        }

        $role->id = IdValue::create(
            $this->nestedSetWriter
                ->insertAsLastChild(
                    self::TABLE_ROLE_NAME,
                    'context_owner_id',
                    $parentId->getStorageValue(),
                    $role->toDatabaseArray()
                )
        );

        return $role;
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\CanNotUpdateExistingRecordException
     */
    public function updateRole(RoleEntity $role, OwnershipContext $ownershipContext): RoleEntity
    {
        if ($role->isNew()) {
            throw new CanNotUpdateExistingRecordException('The role provided does not exist');
        }

        $this->connection->update(
            self::TABLE_ROLE_NAME,
            $role->toDatabaseArray(),
            [
                'id' => $role->id->getStorageValue(),
                'context_owner_id' => $role->contextOwnerId->getStorageValue(),
            ]
        );

        return $this->fetchOneById($role->id, $ownershipContext);
    }

    public function moveRoleAsPrevSibling(IdValue $roleId, IdValue $siblingId): void
    {
        $this->nestedSetWriter
            ->moveAsPrevSibling(
                self::TABLE_ROLE_NAME,
                'context_owner_id',
                $siblingId->getStorageValue(),
                $roleId->getStorageValue()
            );
    }

    public function moveRoleAsNextSibling(IdValue $roleId, IdValue $siblingId): void
    {
        $this->nestedSetWriter
            ->moveAsNextSibling(
                self::TABLE_ROLE_NAME,
                'context_owner_id',
                $siblingId->getStorageValue(),
                $roleId->getStorageValue()
            );
    }

    public function moveRoleAsLastChild(IdValue $roleId, IdValue $parentId): void
    {
        $this->nestedSetWriter
            ->moveAsLastChild(
                self::TABLE_ROLE_NAME,
                'context_owner_id',
                $parentId->getStorageValue(),
                $roleId->getStorageValue()
            );
    }

    /**
     * @throws \Shopware\B2B\Common\Repository\NotFoundException
     */
    public function fetchOneById(IdValue $id, OwnershipContext $ownershipContext): RoleEntity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('child.*')
            ->addSelect('(child.left + 1 != child.right) as hasChildren')
            ->from(self::TABLE_ROLE_NAME, 'child')
            ->innerJoin(
                'child',
                self::TABLE_ROLE_NAME,
                self::TABLE_ROLE_ALIAS,
                'child.context_owner_id = ' . self::TABLE_ROLE_ALIAS . '.context_owner_id AND
                child.`left` >= ' . self::TABLE_ROLE_ALIAS . '.`left` AND
                child.`right` <= ' . self::TABLE_ROLE_ALIAS . '.`right`'
            )
            ->where('child.id = :id')
            ->andWhere(self::TABLE_ROLE_ALIAS . '.context_owner_id = :context')
            ->setParameter('id', $id->getStorageValue());

        $this->aclReadHelper->applyAclVisibility($ownershipContext, $query);

        $rootQuery = $this->connection->createQueryBuilder()
            ->select('child2.*')
            ->addSelect('(child2.left + 1 != child2.right) as hasChildren')
            ->from(self::TABLE_ROLE_NAME, 'child2')
            ->where('child2.id = :id')
            ->andWhere('child2.level = 0')
            ->andWhere('child2.context_owner_id = :context');

        $statement = $this->connection->executeQuery(
            $query->getSQL() . ' UNION ' . $rootQuery->getSQL(),
            array_merge(
                $query->getParameters(),
                [
                    'context' => $ownershipContext->contextOwnerId->getStorageValue(),
                ]
            )
        );

        $roleData = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$roleData) {
            throw new NotFoundException(sprintf('Role not found for %s', $id->getValue()));
        }

        $entity = new RoleEntity();
        $entity->fromDatabaseArray($roleData);

        return $entity;
    }

    public function fetchParentByChildId(IdValue $id, OwnershipContext $ownershipContext): RoleEntity
    {
        $roleData = $this->nestedSetQueryFactory
            ->createParentsQueryBuilder(self::TABLE_ROLE_NAME, self::TABLE_ROLE_ALIAS, 'context_owner_id', $id->getStorageValue())
            ->select('*')
            ->addSelect('(role.left + 1 != role.right) as hasChildren')
            ->andWhere(self::TABLE_ROLE_ALIAS . '.context_owner_id = :contextOwner')
            ->setParameter('contextOwner', $ownershipContext->contextOwnerId->getStorageValue())
            ->setMaxResults(1)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        if (!$roleData) {
            throw new NotFoundException(sprintf('Role not found for %s', $id));
        }

        $entity = new RoleEntity();
        $entity->fromDatabaseArray($roleData);

        return $entity;
    }

    public function getMainTableAlias(): string
    {
        return self::TABLE_ROLE_ALIAS;
    }

    /**
     * @return string[]
     */
    public function getFullTextSearchFields(): array
    {
        return [
            'name',
        ];
    }

    public function getAdditionalSearchResourceAndFields(): array
    {
        return [];
    }

    /**
     * @internal
     * @return RoleEntity[]
     */
    protected function fetchRoles(QueryBuilder $query): array
    {
        $rows = $query->execute()->fetchAll();

        $roles = [];
        foreach ($rows as $row) {
            $roles[] = (new RoleEntity())->fromDatabaseArray($row);
        }

        return $roles;
    }

    public function isRoleRemovable(OwnershipContext $ownershipContext, RoleEntity $roleEntity): bool
    {
        $subQuery = $this->nestedSetQueryFactory
            ->createParentAndChildrenQueryBuilder(
                self::TABLE_ROLE_NAME,
                self::TABLE_ROLE_ALIAS,
                'context_owner_id',
                $roleEntity->id->getStorageValue()
            )
            ->select('role.*')
            ->setParameter(self::TABLE_ROLE_ALIAS . 'Root', $ownershipContext->contextOwnerId->getStorageValue());

        $mainQuery = $this->connection->createQueryBuilder()
            ->select('count(*)')
            ->from('(' . $subQuery->getSQL() . ')', self::TABLE_ROLE_ALIAS)
            ->setParameters($subQuery->getParameters())
            ->setParameter('id', $roleEntity->id->getStorageValue());

        $grantContext = new RoleAclGrantContext($roleEntity);

        foreach ($this->dependencyValidators as $key => $dependencyValidator) {
            $tableAlias = 'dependency_' . $key;
            $dependencyValidator->extendRoleRemoveDependencyQuery(
                $mainQuery,
                $grantContext,
                self::TABLE_ROLE_ALIAS,
                $tableAlias
            );
        }

        return !(bool) $mainQuery->execute()->fetch(PDO::FETCH_COLUMN);
    }
}
