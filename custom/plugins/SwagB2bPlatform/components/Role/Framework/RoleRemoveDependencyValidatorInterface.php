<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Framework;

use Doctrine\DBAL\Query\QueryBuilder;

interface RoleRemoveDependencyValidatorInterface
{
    public function extendRoleRemoveDependencyQuery(QueryBuilder $queryBuilder, RoleAclGrantContext $grantContext, string $mainTableAlias, string $alias);
}
