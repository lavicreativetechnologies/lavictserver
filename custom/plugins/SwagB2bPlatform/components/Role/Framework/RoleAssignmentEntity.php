<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Framework;

use Shopware\B2B\Common\CrudEntity;
use Shopware\B2B\Common\IdValue;

class RoleAssignmentEntity extends RoleEntity
{
    /**
     * @var IdValue
     */
    public $assignmentId;

    public function __construct()
    {
        parent::__construct();

        $this->assignmentId = IdValue::null();
    }

    public function toDatabaseArray(): array
    {
        $array = parent::toDatabaseArray();

        $array['assignmentId'] = $this->assignmentId->getStorageValue();

        return $array;
    }

    public function fromDatabaseArray(array $roleData): CrudEntity
    {
        parent::fromDatabaseArray($roleData);

        $this->assignmentId = IdValue::create($roleData['assignmentId']);

        return $this;
    }
}
