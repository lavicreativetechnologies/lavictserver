<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Framework;

use Shopware\B2B\Acl\Framework\AclContextResolverMain;
use Shopware\B2B\Acl\Framework\AclUnsupportedContextException;
use Shopware\B2B\Common\IdValue;
use function get_class;
use function sprintf;

class AclTableRoleContextResolver extends AclContextResolverMain
{
    /**
     * @param object $context
     * @throws AclUnsupportedContextException
     */
    public function extractId($context): IdValue
    {
        if ($context instanceof RoleEntity && !$context->isNew()) {
            return $context->id;
        }

        if ($context instanceof RoleAclGrantContext) {
            return $context->getEntity()->id;
        }

        throw new AclUnsupportedContextException(sprintf('unable to extract id from %s', get_class($context)));
    }
}
