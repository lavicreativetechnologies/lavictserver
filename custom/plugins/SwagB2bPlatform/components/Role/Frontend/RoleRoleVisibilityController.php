<?php declare(strict_types=1);

namespace Shopware\B2B\Role\Frontend;

use Shopware\B2B\Acl\Framework\AclAccessExtensionService;
use Shopware\B2B\Acl\Framework\AclRepository;
use Shopware\B2B\Common\Controller\B2bControllerForwardException;
use Shopware\B2B\Common\MvcExtension\Request;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Role\Framework\RoleRepository;
use Shopware\B2B\StoreFrontAuthentication\Framework\AuthenticationService;

class RoleRoleVisibilityController
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var AclAccessExtensionService
     */
    private $aclAccessExtensionService;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var AclRepository
     */
    private $aclRepository;

    public function __construct(
        AuthenticationService $authenticationService,
        AclRepository $aclRepository,
        RoleRepository $roleRepository,
        AclAccessExtensionService $aclAccessExtensionService
    ) {
        $this->authenticationService = $authenticationService;
        $this->roleRepository = $roleRepository;
        $this->aclAccessExtensionService = $aclAccessExtensionService;
        $this->aclRepository = $aclRepository;
    }

    public function indexAction(Request $request): array
    {
        $baseRoleId = $request->requireIdValue('roleId');

        return [
            'baseRoleId' => $baseRoleId->getValue(),
        ];
    }

    public function treeAction(Request $request): array
    {
        $parentId = $request->getIdValue('parentId');
        $baseRoleId = $request->requireIdValue('baseRoleId');
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $baseRole = $this->roleRepository->fetchOneById($baseRoleId, $ownershipContext);

        if ($parentId instanceof NullIdValue) {
            $roles = $this->roleRepository
                ->fetchAclRootRoles($ownershipContext, false);
        } else {
            $roles = $this->roleRepository
                ->fetchChildren($parentId, $ownershipContext);
        }

        $this->aclAccessExtensionService
            ->extendEntitiesWithAssignment($this->aclRepository, $baseRole, $roles);

        $this->aclAccessExtensionService
            ->extendEntitiesWithIdentityOwnership($this->aclRepository, $ownershipContext, $roles);

        return [
            'roles' => $roles,
            'baseRoleId' => $baseRole->id->getValue(),
        ];
    }

    /**
     * @throws \Shopware\B2B\Common\Controller\B2bControllerForwardException
     */
    public function assignAction(Request $request): void
    {
        $request->checkPost('index', ['roleId' => $request->getParam('baseRoleId')]);

        $roleId = $request->requireIdValue('roleId');
        $baseRoleId = $request->requireIdValue('baseRoleId');
        $grantable = (bool) $request->getParam('grantable', false);
        $ownershipContext = $this->authenticationService->getIdentity()->getOwnershipContext();

        $baseRole = $this->roleRepository->fetchOneById($baseRoleId, $ownershipContext);

        if ($request->getParam('allow', false)) {
            $this->aclRepository->allow($baseRole, $roleId, $grantable);
        } else {
            $this->aclRepository->deny($baseRole, $roleId);
        }

        throw new B2bControllerForwardException('index', null, ['roleId' => $baseRoleId->getValue()]);
    }
}
