<?php declare(strict_types=1);

namespace Shopware\B2B\Dashboard\Backend;

use Shopware\B2B\Common\IdValue;
use Shopware\B2B\Common\NullIdValue;
use Shopware\B2B\Common\Repository\NotFoundException;
use Shopware\B2B\Contact\Framework\ContactEntity;
use Shopware\B2B\Contact\Framework\ContactRepository;
use Shopware\B2B\Contact\Framework\ContactSearchStruct;
use Shopware\B2B\Dashboard\Framework\EmotionRepositoryInterface;
use Shopware\B2B\Debtor\Framework\DebtorIdentity;
use Shopware\B2B\Debtor\Framework\DebtorRepositoryInterface;
use Shopware\B2B\StoreFrontAuthentication\Framework\LoginContextService;
use function array_unshift;

class DebtorBackendExtension
{
    /**
     * @var DebtorRepositoryInterface
     */
    private $debtorRepository;

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var LoginContextService
     */
    private $contextService;

    /**
     * @var EmotionRepositoryInterface
     */
    private $emotionRepository;

    public function __construct(
        DebtorRepositoryInterface $debtorRepository,
        ContactRepository $contactRepository,
        LoginContextService $contextService,
        EmotionRepositoryInterface $emotionRepository
    ) {
        $this->debtorRepository = $debtorRepository;
        $this->contactRepository = $contactRepository;
        $this->contextService = $contextService;
        $this->emotionRepository = $emotionRepository;
    }

    public function getUserList(IdValue $debtorId): array
    {
        /** @var DebtorIdentity $debtor */
        $debtor = $this->debtorRepository->fetchIdentityById($debtorId, $this->contextService);

        $contactSearchStruct = new ContactSearchStruct();
        $contactSearchStruct->orderBy = $this->contactRepository::TABLE_ALIAS . '.id';
        $contactSearchStruct->orderDirection = 'ASC';

        $contacts = $this->contactRepository->fetchList($debtor->getOwnershipContext(), $contactSearchStruct);

        $users = $this->getContactUserData($contacts);
        array_unshift($users, $this->getDebtorUserData($debtor));

        return $users;
    }

    /**
     * @internal
     * @throws NotFoundException
     */
    protected function getDebtorUserData(DebtorIdentity $debtor): array
    {
        try {
            $emotion = $this->emotionRepository->getDirectEmotionIdByAuthId($debtor->getAuthId());
        } catch (NotFoundException $e) {
            $emotion = null;
        }

        $postalSetting = $debtor->getPostalSettings();

        return [
            'id' => $debtor->getAuthId(),
            'name' => $postalSetting->firstName . ' ' . $postalSetting->lastName,
            'email' => $postalSetting->email,
            'type' => 'debtor',
            'emotion' => $emotion,
        ];
    }

    /**
     * @internal
     * @param ContactEntity[] $contacts
     * @throws NotFoundException
     */
    protected function getContactUserData(array $contacts): array
    {
        $userData = [];
        foreach ($contacts as $contact) {
            if ($contact->authId instanceof NullIdValue) {
                continue;
            }

            try {
                $emotion = $this->emotionRepository->getDirectEmotionIdByAuthId($contact->authId);
            } catch (NotFoundException $e) {
                $emotion = null;
            }

            $userData[] = [
                'id' => $contact->authId,
                'name' => $contact->firstName . ' ' . $contact->lastName,
                'email' => $contact->email,
                'type' => 'contact',
                'emotion' => $emotion,
            ];
        }

        return $userData;
    }

    public function updateUser(IdValue $userId, IdValue $emotionId): void
    {
        $this->emotionRepository->updateEmotion($userId, $emotionId);
    }

    /**
     * @return array
     */
    public function getAllEmotions()
    {
        $emotions = $this->emotionRepository->getAllEmotions();

        $emotionArray = [];
        foreach ($emotions as $emotion) {
            $emotionArray[] = [
                'id' => $emotion->getId(),
                'name' => $emotion->getName(),
            ];
        }

        return $emotionArray;
    }
}
