<?php declare(strict_types=1);

namespace Shopware\B2B\Dashboard\Framework;

use Shopware\B2B\Cart\Framework\CartAccessResult;
use Shopware\B2B\Cart\Framework\InformationMessage;
use Shopware\B2B\ContingentGroup\Framework\ContingentGroupRepository;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleAllowanceEntityInterface;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleEntity;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleRepository;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleSearchStruct;
use Shopware\B2B\ContingentRule\Framework\ContingentRuleTypeFactory;
use Shopware\B2B\Currency\Framework\CurrencyContext;
use Shopware\B2B\StoreFrontAuthentication\Framework\Identity;
use Shopware\B2B\StoreFrontAuthentication\Framework\OwnershipContext;
use function array_key_exists;

class InformationService
{
    /**
     * @var ContingentRuleRepository
     */
    private $contingentRuleRepository;

    /**
     * @var ContingentGroupRepository
     */
    private $contingentGroupRepository;

    /**
     * @var ContingentRuleTypeFactory
     */
    private $typeFactory;

    public function __construct(
        ContingentGroupRepository $contingentGroupRepository,
        ContingentRuleRepository $contingentRuleRepository,
        ContingentRuleTypeFactory $typeFactory
    ) {
        $this->contingentRuleRepository = $contingentRuleRepository;
        $this->contingentGroupRepository = $contingentGroupRepository;
        $this->typeFactory = $typeFactory;
    }

    /**
     * @return InformationMessage[]
     */
    public function getInformation(Identity $identity, CurrencyContext $currencyContext): array
    {
        $ownershipContext = $identity->getOwnershipContext();

        $contingentGroupIds = $this->contingentGroupRepository
            ->fetchContingentGroupIdsForContact($ownershipContext->identityId);

        $cartAccessResult = new CartAccessResult();

        $allowanceRules = [];
        foreach ($contingentGroupIds as $groupsId) {
            $rules = $this->contingentRuleRepository
                ->fetchListByContingentGroupId(
                    $groupsId,
                    new ContingentRuleSearchStruct(),
                    $currencyContext,
                    $ownershipContext
                );

            foreach ($rules as $key => $rule) {
                $this->addInformationToCartAccessResult(
                    $rule,
                    $ownershipContext,
                    $cartAccessResult,
                    $allowanceRules
                );
            }
        }

        foreach ($allowanceRules as $rule) {
            $this->typeFactory
                ->createCartAccessStrategy($rule->type, $ownershipContext, $rule)
                ->addInformation($cartAccessResult);
        }

        return $cartAccessResult->information;
    }

    /**
     * @internal
     * @param ContingentRuleAllowanceEntityInterface $allowanceRules
     */
    protected function addInformationToCartAccessResult(
        ContingentRuleEntity $rule,
        OwnershipContext $context,
        CartAccessResult $cartAccessResult,
        array &$allowanceRules
    ): void {
        if ($rule instanceof ContingentRuleAllowanceEntityInterface) {
            if (array_key_exists($rule->type, $allowanceRules)) {
                $rule->mergeRule($allowanceRules[$rule->type]);
            }

            $allowanceRules[$rule->type] = $rule;
        } else {
            $this->typeFactory
                ->createCartAccessStrategy($rule->type, $context, $rule)
                ->addInformation($cartAccessResult);
        }
    }
}
