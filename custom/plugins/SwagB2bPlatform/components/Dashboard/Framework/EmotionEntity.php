<?php declare(strict_types=1);

namespace Shopware\B2B\Dashboard\Framework;

use Shopware\B2B\Common\Entity;
use Shopware\B2B\Common\IdValue;

class EmotionEntity implements Entity
{
    /**
     * @var IdValue
     */
    public $id;

    /**
     * @var int
     */
    public $device;

    /**
     * @var string
     */
    public $name;

    public function getId(): IdValue
    {
        return $this->id;
    }

    public function getDevice(): int
    {
        return $this->device;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
