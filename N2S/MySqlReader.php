<?php

require_once './passwordLib.php';
error_reporting(0);
ini_set('html_errors', false);
//ini_set('max_execution_time', 120);
ini_set('max_execution_time', 0);


try {

    if (!password_verify($_GET['u'] . $_GET['p'], '$2y$10$AH7YUVtTHvcn3qo8pKBUM.ComTCAOJQsIAL7HPWO.fjm3Lr/5NHma')) {
        exit();
    }

    $mySqlReader = new MySqlReader();
    $postData = $mySqlReader->GetPostData();

    $json = $mySqlReader->GetJsonData($postData);



    if ($json == null) {

        $json = 'json was null  ' . json_last_error_msg();
    }

    print $json;
} catch (Exception $e) {
    $mySqlReader->Error = $e->getMessage();
    print json_encode(new MySqlReaderResult("", 999, "MainException " . $e->getMessage()));
    exit();
}

class MySqlReader {

    public function GetPostData() {
        $postData = file_get_contents('php://input');
        if ($postData == NULL || $postData == "") {
            
            $this->Stop(MySqlReaderResult::BuildError(999, "postData Is NULL or Empty"));
        }
        return $postData;
    }

    private $mySqlConnection;

    private function GetMySqlData($mysqlQuerys) {


         $readerResult = new MySqlReaderResult();
         
         foreach ($mysqlQuerys as $mysqlQuery) {

            $results = mysqli_query($this->mySqlConnection, $mysqlQuery);

            if (!$results) {
                
                $nr = mysqli_errno($this->mySqlConnection);
                $text = mysqli_error($this->mySqlConnection);
                $this->Stop(MySqlReaderResult::BuildError($nr, $text));
            }
            
            
            //if (mysqli_affected_rows($results) == 0) {
            //    $readerResult->AddDetaills(MySqlReaderResultDetails::BuildError(0, "No Results for " . $mysqlQuery));
            //    }

            if ($this->IsSelectQuery($mysqlQuery)) {

                while ($result = mysqli_fetch_object($results)) {

                    foreach ($result as $key => $value) {
                        $tmpArray[$key] = $value;
                    }
                    $readerResult->AddDetaills(MySqlReaderResultDetails::BuildResult($tmpArray));
                    
                }
            }else if ($this->IsDescribeQuery($mysqlQuery)) {

                while ($result = mysqli_fetch_object($results)) {

                    foreach ($result as $key => $value) {
                        $tmpArray[$key] = $value;
                    }
                    $readerResult->AddDetaills(MySqlReaderResultDetails::BuildResult($tmpArray));
                    
                }
            } else if ($this->IsShowTablesQuery($mysqlQuery)) {

                while ($result = mysqli_fetch_object($results)) {

                    foreach ($result as $key => $value) {
                        $tmpArray["Tables"] = $value;
                    }
                    $readerResult->AddDetaills(MySqlReaderResultDetails::BuildResult($tmpArray));
                    
                }
            } else if ($this->IsInsertQuery($mysqlQuery)) {
                
                $readerResult->AddDetaills(MySqlReaderResultDetails::BuildLastInsertId(mysqli_insert_id($this->mySqlConnection)));
                
            } else if ($this->IsDeleteQuery($mysqlQuery)) {


                $readerResult->AddDetaills(MySqlReaderResultDetails::BuildRecordsAffected(1));

            }else {
                $readerResult->AddDetaills(MySqlReaderResultDetails::BuildResult(999,"No elseIf for Query " . $mysqlQuery));
                }
        }

        return $readerResult;
    }

    private function OpenDb($mySqlConnInfo) {

        $i = $mySqlConnInfo;

        $this->mySqlConnection = mysqli_connect($i->Server, $i->User, $i->Pass, $i->Db, $i->Port);
        if ($this->mySqlConnection == false) {
            
            $this->Stop(MySqlReaderResult::BuildError(mysqli_connect_errno(), mysqli_connect_error()));
        }
        mysqli_query($this->mySqlConnection, "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
    }

    private function GetMySqlCredentials() {

        //Shopware
        if (file_exists('../config.php')) {
            $config = include ('../config.php');

            $settings = $config['db'];
            $mySqlCredentials = new MySqlCredentials();
            $mySqlCredentials->Server = $settings['host'];
            $mySqlCredentials->User = $settings['username'];
            $mySqlCredentials->Pass = $settings['password'];
            $mySqlCredentials->Db = $settings['dbname'];
            $mySqlCredentials->Port = $settings['port'];
            
        }
        //Gambio
        else if (file_exists('../admin/includes/configure.php')) {
            include "../admin/includes/configure.php";

            $mySqlCredentials = new MySqlConnInfo();
            $mySqlCredentials->Server = DB_SERVER;
            $mySqlCredentials->User = DB_SERVER_USERNAME;
            $mySqlCredentials->Pass = DB_SERVER_PASSWORD;
            $mySqlCredentials->Db = DB_DATABASE;
        }
        //Magento2
        else if (file_exists('../app/etc/env.php')) {
            
            $config = include ('../app/etc/env.php');

            $mySqlCredentials = new MySqlCredentials();
            $mySqlCredentials->Server = $config['db']['connection']['default']['host'];
            $mySqlCredentials->User = $config['db']['connection']['default']['username'];
            $mySqlCredentials->Pass = $config['db']['connection']['default']['password'];
            $mySqlCredentials->Db = $config['db']['connection']['default']['dbname'];

        }
		//Shopware6
		 else if (file_exists('../.env')) 
		{
		     
			$username = "";
			$password = "";
			$host = "";						
			$port = "";
			$dbaName = "";
			
			if ($file = fopen("../.env", "r")) {
			    while(!feof($file)) {
			        $line = fgets($file);
					if(strpos($line, "DATABASE_URL=mysql:") !== false)
					{				
						$line = str_replace("DATABASE_URL=mysql://","",$line);				
						preg_match("/(?<=^)(.*)(?=$)/u", $line, $matches);
						if(count($matches) > 0)
						{
							$userPassHostPOrt = $matches[0];					
							$splittedByAt = explode ("@", $userPassHostPOrt);
							
							if (count($splittedByAt) == 2)
							{
								$userPass = $splittedByAt[0];
								
								$splittedUserPass = explode (":", $userPass);
								if (count($splittedUserPass) == 2)
								{
									$username = $splittedUserPass[0];						
									$password = $splittedUserPass[1];										
								}				
								
								$hostPortDb = $splittedByAt[1];	
								$spittedHostAndDb = explode ("/", $hostPortDb);
								if (count($spittedHostAndDb) == 2)
								{
									$hostPort = $spittedHostAndDb[0];					
									$splittedHostPort = explode (":", $hostPort);
									if (count($splittedHostPort) == 2)
									{
										$host = $splittedHostPort[0];								
										$port = $splittedHostPort[1];	
									}
									$dbaName = $spittedHostAndDb[1];										
								}	
							}			
						}
						break;
					}
			    }
			    fclose($file);
			}    
            $mySqlCredentials = new MySqlCredentials();
            $mySqlCredentials->Server  = $host;
			$mySqlCredentials->Port = $port;
            $mySqlCredentials->User = $username;
            $mySqlCredentials->Pass = $password;
            $mySqlCredentials->Db = $dbaName;
        } 

		 else if (file_exists('../../.env')) 
		{
		     
			$username = "";
			$password = "";
			$host = "";						
			$port = "";
			$dbaName = "";
			
			if ($file = fopen("../../.env", "r")) {
			    while(!feof($file)) {
			        $line = fgets($file);
					if(strpos($line, "DATABASE_URL=mysql:") !== false)
					{				
						$line = str_replace("DATABASE_URL=mysql://","",$line);				
						preg_match("/(?<=^)(.*)(?=$)/u", $line, $matches);
						if(count($matches) > 0)
						{
							$userPassHostPOrt = $matches[0];					
							$splittedByAt = explode ("@", $userPassHostPOrt);
							
							if (count($splittedByAt) == 2)
							{
								$userPass = $splittedByAt[0];
								
								$splittedUserPass = explode (":", $userPass);
								if (count($splittedUserPass) == 2)
								{
									$username = $splittedUserPass[0];						
									$password = $splittedUserPass[1];										
								}				
								
								$hostPortDb = $splittedByAt[1];	
								$spittedHostAndDb = explode ("/", $hostPortDb);
								if (count($spittedHostAndDb) == 2)
								{
									$hostPort = $spittedHostAndDb[0];					
									$splittedHostPort = explode (":", $hostPort);
									if (count($splittedHostPort) == 2)
									{
										$host = $splittedHostPort[0];								
										$port = $splittedHostPort[1];	
									}
									$dbaName = $spittedHostAndDb[1];										
								}	
							}			
						}
						break;
					}
			    }
			    fclose($file);
			}    
            $mySqlCredentials = new MySqlCredentials();
            $mySqlCredentials->Server  = $host;
			$mySqlCredentials->Port = $port;
            $mySqlCredentials->User = $username;
            $mySqlCredentials->Pass = $password;
            $mySqlCredentials->Db = $dbaName;
        } 
		else 
		{            
            $this->Stop(MySqlReaderResult::BuildError(999, "No Configfile"));
        }
        if ($mySqlCredentials == NULL) {

            $this->Stop(MySqlReaderResult::BuildError(999, "MySqlConnectionInfo is NULL"));
        }
        if ($mySqlCredentials->Server == NULL) {

            $this->Stop(MySqlReaderResult::BuildError(999, "MySqlConnectionInfo ServerString is NULL"));
        }
        if ($mySqlCredentials->Server == "") {

            $this->Stop(MySqlReaderResult::BuildError(999, "MySqlConnectionInfo ServerString is String-Empty"));
        }
        return $mySqlCredentials;
    }

    private function CloseDb() {
        mysqli_close($this->mySqlConnection);
    }

    private function IsUpdateQuery($query) {

        if (strtoupper(substr($query, 0, 6)) == "UPDATE") {
            return true;
        }
        return false;
    }

    private function IsDeleteQuery($query) {
        if (strtoupper(substr($query, 0, 6)) == "DELETE") {
            return true;
        }
        return false;
    }

    private function IsInsertQuery($query) {

        if (strtoupper(substr($query, 0, 6)) == "INSERT") {
            return true;
        }
        return false;
    }
	private function IsShowTablesQuery($query) {

        if (strtoupper(substr($query, 0, 11)) == "SHOW TABLES") {
            return true;
        }
        return false;
        return false;
    }
	
    private function IsSelectQuery($query) {

        if (strtoupper(substr($query, 0, 6)) == "SELECT") {
            return true;
        }

        return false;
    }

	private function IsDescribeQuery($query) {

	if (strtoupper(substr($query, 0, 8)) == "DESCRIBE") {
            return true;
        }
        return false;
    }

    public function GetJsonData($postData) {
        
        $mySqlConnInfo = $this->GetMySqlCredentials();
        $this->OpenDb($mySqlConnInfo);
        $postDataAsObject = json_decode($postData);

        $mySqlData = $this->GetMySqlData($postDataAsObject);
		$this->CloseDb();
		$json = json_encode($mySqlData, JSON_INVALID_UTF8_SUBSTITUTE);

        return $json;
    }

    function Stop($mySqlResult) {

        print json_encode($mySqlResult);
        exit();
    }

}

class MySqlCredentials {

    public $Server;
    public $User;
    public $Pass;
    public $Db;
    public $Port = 3306;

}

class MySqlReaderResult {

    
    public static function BuildError($errorNo,$error)
    {
        $mySqlReaderResult = new MySqlReaderResult();
        $mySqlReaderResult->ErrorNo = $errorNo;
        $mySqlReaderResult->Error = $error;
        return $mySqlReaderResult;
    }
    public function AddDetaills($mySqlReaderResultDetails) {
        array_push($this->MySqlReaderResultDetails, $mySqlReaderResultDetails);
    }

    public $ErrorNo = 0;
    public $Error ="";
    public $MySqlReaderResultDetails = [];

}

class MySqlReaderResultDetails {

    
    public $Result ;
    public $RecordsAffected = 0;
    public $LastInsertId = 0;
    public $ErrorNo = 0;
    public $Error ="";
    
    public static function BuildResult($result): MySqlReaderResultDetails
    {
        $m = new MySqlReaderResultDetails();
        $m->Result = $result;
        return $m;
    }
    
     public static function BuildLastInsertId($lastInsertId): MySqlReaderResultDetails {
        $m = new MySqlReaderResultDetails();
        $m->LastInsertId = $lastInsertId;
        return $m;
    }

    public static function BuildRecordsAffected($records): MySqlReaderResultDetails
    {
        $m = new MySqlReaderResultDetails();
        $m->RecordsAffected = $records;
        return $m;
    }

    public static function BuildError($errorNo, $error): MySqlReaderResultDetails
    {
        $m = new MySqlReaderResultDetails();
        $m->Error = $error;
        $m->ErrorNo = $errorNo;
        return $m;
    }
    
    

}
